﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class Licencia
    {
        [Required]
        public string user { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string ClientKey { get; set; }
        [Required]
        public string Release { get; set; }
        [Required]
        public string Plataforma { get; set; }
        public string Version { get; set; }
        public string ClientName { get; set; }
        [Required]
        public string ClientCuit { get; set; }
        public string ClientRazonSocial { get; set; }
        public string Tipo { get; set; }
        public string TerminalName { get; set; }
        public bool Enabled { get; set; }
        public int PointOfSale { get; set; }
    }

    public class ResponcePostLicencia
    {
        public string Key { get; set; }
        public string Resultado { get; set; }
    }

    public class ValidateKey
    {
        public string user { get; set; }
        public string password { get; set; }
        public string Key { get; set; }
        public int PuntoVenta { get; set; }
        public string ClientCuit { get; set; }
    }

    public class ResponseValidateKey
    {
        public string Enabled { get; set; }
    }
}
