﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class RestService
    {
        public static Modelos.Tiquet GetTiquet(string _numSerie, int _numFactura, string _n, SqlConnection _con)
        {
            string _sql = "SELECT SERIE, NUMFACTURA, N, SERIEFISCAL, SERIEFISCAL2, NUMEROFISCAL FROM TIQUETSCAB " +
                    "WHERE SERIE = @numSerie AND NUMERO = @numFactura AND N = @n";

            Modelos.Tiquet _cls = new Modelos.Tiquet();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                _cmd.Parameters.AddWithValue("@n", _n);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.N = _reader["N"].ToString();
                            _cls.NumeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                            _cls.NumFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                            _cls.NumSerie = _reader["SERIE"].ToString();
                            _cls.SerieFiscal1 = _reader["SERIEFISCAL"].ToString();
                            _cls.SerieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                        }
                    }
                }
            }


            return _cls;
        }

        public static Modelos.DatosFactura GetDatosFactura(int _fo, string _serie, int _numero, string _N, int _codVendedor, SqlConnection _Connection)
        {
            string _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.TOTALBRUTO, TIQUETSCAB.TOTALNETO,
                TIQUETSCAB.CODCLIENTE, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.FECHA, TIQUETSCAB.ENTREGADO, TIQUETSCAB.PROPINA,TIQUETSCAB.SERIEFISCAL 
                , TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, VENDEDORES.NOMBREVENDEDOR, TIQUETSCAB.SALA, TIQUETSCAB.MESA, TIQUETSCAB.HORAFIN, TIQUETSCAB.BASEIMP1,
                CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.CIF, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, TIQUETSCAB.CARGO
                FROM TIQUETSCAB inner join VENDEDORES on TIQUETSCAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                LEFT JOIN CLIENTES on TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                WHERE TIQUETSCAB.fo = @fo AND TIQUETSCAB.SERIE = @serie AND TIQUETSCAB.NUMERO = @numero AND TIQUETSCAB.N = @n 
                AND TIQUETSCAB.CODVENDEDOR = @codVendedor";

            Modelos.DatosFactura _df = new Modelos.DatosFactura();

            using (SqlCommand _cmd = new SqlCommand(_sql))
            {
                _cmd.Connection = _Connection;
                _cmd.CommandType = System.Data.CommandType.Text;

                _cmd.Parameters.AddWithValue("@fo", _fo);
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@n", _N);
                _cmd.Parameters.AddWithValue("@numero", _numero);
                _cmd.Parameters.AddWithValue("@codVendedor", _codVendedor);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _df.fo = Convert.ToInt32(_reader["Fo"]);//.GetInt32(0);
                            _df.serie = _reader.GetString(1);
                            _df.numero = _reader.GetInt32(2);
                            _df.n = _reader.GetString(3);
                            _df.totalbruto = _reader.GetDouble(4);
                            _df.totalneto = _reader.GetDouble(5);
                            _df.codcliente = _reader.GetInt32(6);
                            _df.codvendedor = _reader.GetInt32(7);
                            _df.fecha = _reader.GetDateTime(8);
                            _df.entregado = _reader.GetDouble(9);
                            _df.propina = _reader.GetDouble(10);
                            _df.serieFiscal = String.IsNullOrEmpty(_reader["SERIEFISCAL"].ToString()) ? "" : _reader.GetString(11);
                            _df.serieFiscal2 = String.IsNullOrEmpty(_reader["SERIEFISCAL2"].ToString()) ? "" : _reader.GetString(12);
                            _df.numeroFiscal = String.IsNullOrEmpty(_reader["NUMEROFISCAL"].ToString()) ? 0 : _reader.GetInt32(13);
                            _df.nomVendedor = _reader.GetString(14);
                            _df.sala = Convert.ToInt32(_reader[15]);
                            _df.mesa = Convert.ToInt32(_reader[16]);
                            _df.hora = _reader.GetDateTime(17);
                            _df.BaseImp = _reader.GetDouble(18);
                            //Cliente
                            if (_df.codcliente == 0)
                            {
                                _df.clienteNombre = "Consumidor Final";
                                _df.clienteDireccion = ".";
                                _df.clienteDocumento = "0";
                                _df.clienteRegimenFacturacion = 4;
                                _df.cargo = Convert.ToDouble(_reader["CARGO"]);
                            }
                            else
                            {
                                _df.clienteNombre = String.IsNullOrEmpty(_reader["NOMBRECLIENTE"].ToString()) ? "" : _reader["NOMBRECLIENTE"].ToString();
                                _df.clienteDireccion = String.IsNullOrEmpty(_reader["DIRECCION1"].ToString()) ? "." : _reader["DIRECCION1"].ToString() + ", " + _reader["POBLACION"].ToString() + ", " + _reader["PROVINCIA"].ToString();
                                _df.clienteDocumento = String.IsNullOrEmpty(_reader["CIF"].ToString()) ? "0" : _reader["CIF"].ToString();
                                _df.clienteRegimenFacturacion = String.IsNullOrEmpty(_reader["REGIMFACT"].ToString().Trim()) ? 4 : Convert.ToInt32(_reader["REGIMFACT"]);
                                _df.cargo = Convert.ToDouble(_reader["CARGO"]);
                            }
                            //Agrego el tipo de documento.
                            switch (_df.clienteRegimenFacturacion)
                            {
                                case 1:
                                    {
                                        _df.clienteTipoDocumento = 80;
                                        _df.codAfipComprobante = _df.totalneto > 0 ? 1 : 3;
                                        _df.serieFiscal = _df.codAfipComprobante == 1 ? "001_Factura_A" : "003_NCredito_A";
                                        break;
                                    }
                                case 2:
                                    {
                                        _df.clienteTipoDocumento = 80;
                                        _df.codAfipComprobante = _df.totalneto > 0 ? 6 : 8;
                                        _df.serieFiscal = _df.codAfipComprobante == 6 ? "006_Factura_B" : "008_NCredito_B";
                                        break;
                                    }
                                case 4:
                                    {
                                        if (_df.clienteDocumento != "0")
                                            _df.clienteTipoDocumento = 96;
                                        else
                                        {
                                            _df.clienteTipoDocumento = 99;
                                        }
                                        _df.codAfipComprobante = _df.totalneto > 0 ? 6 : 8;
                                        _df.serieFiscal = _df.codAfipComprobante == 6 ? "006_Factura_B" : "008_NCredito_B";
                                        break;
                                    }
                                case 5:
                                    {
                                        //_df.clienteTipoDocumento = 80;
                                        //_df.codAfipComprobante = _df.totalneto > 0 ? 6 : 8;
                                        //_df.serieFiscal = _df.codAfipComprobante == 6 ? "006_Factura_B" : "008_NCredito_B";
                                        _df.clienteTipoDocumento = 80;
                                        _df.codAfipComprobante = _df.totalneto > 0 ? 1 : 3;
                                        _df.serieFiscal = _df.codAfipComprobante == 1 ? "001_Factura_A" : "003_NCredito_A";
                                        break;
                                    }
                                case 6:
                                    {
                                        //_df.clienteTipoDocumento = 80;
                                        //_df.codAfipComprobante = _df.totalneto > 0 ? 6 : 8;
                                        //_df.serieFiscal = _df.codAfipComprobante == 6 ? "006_Factura_B" : "008_NCredito_B";
                                        _df.clienteTipoDocumento = 80;
                                        _df.codAfipComprobante = _df.totalneto > 0 ? 1 : 3;
                                        _df.serieFiscal = _df.codAfipComprobante == 1 ? "001_Factura_A" : "003_NCredito_A";
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
            return _df;
        }

        public static List<Modelos.TiquetsTot> GetTotalesIva(string _Serie, string _N, int _Numero, int _CodDto, SqlConnection _Connexion)
        {
            string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM TIQUETSTOT WHERE IVA >= 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
            //Instanciamos la conexion.                
            SqlCommand _Command = new SqlCommand(strSql);
            _Command.Connection = _Connexion;

            _Command.Parameters.AddWithValue("@CodDto", _CodDto);
            _Command.Parameters.AddWithValue("@Serie", _Serie);
            _Command.Parameters.AddWithValue("@Numero", _Numero);
            _Command.Parameters.AddWithValue("@N", _N);

            List<Modelos.TiquetsTot> _Iva = new List<Modelos.TiquetsTot>();
            try
            {
                using (SqlDataReader reader = _Command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Modelos.TiquetsTot fct = new Modelos.TiquetsTot();
                            fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                            fct.Iva = Convert.ToDecimal(reader["IVA"]);
                            fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                            _Iva.Add(fct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            return _Iva;
        }

        public static List<Modelos.TiquetsTot> GetTotalesIvaNoGravado(string _Serie, string _N, int _Numero, int _CodDto, SqlConnection _Connexion)
        {
            string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM TIQUETSTOT WHERE IVA = 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
            //Instanciamos la conexion.                
            SqlCommand _Command = new SqlCommand(strSql);
            _Command.Connection = _Connexion;

            _Command.Parameters.AddWithValue("@CodDto", _CodDto);
            _Command.Parameters.AddWithValue("@Serie", _Serie);
            _Command.Parameters.AddWithValue("@Numero", _Numero);
            _Command.Parameters.AddWithValue("@N", _N);

            List<Modelos.TiquetsTot> _Iva = new List<Modelos.TiquetsTot>();
            try
            {
                using (SqlDataReader reader = _Command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Modelos.TiquetsTot fct = new Modelos.TiquetsTot();
                            fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                            fct.Iva = Convert.ToDecimal(reader["IVA"]);
                            fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                            _Iva.Add(fct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _Iva;
        }

        public static void ValidarDatosFactura(Modelos.DatosFactura _fc)
        {
            if (String.IsNullOrEmpty(_fc.clienteDocumento))
                throw new Exception("ICG-VAL. El Cliente no posee Nro. de Documento.");

            if (_fc.clienteTipoDocumento == 0)
                throw new Exception("ICG-VAL. El Cliente no posee Tipo de Documento.");

            if (_fc.ptoVta == 0)
                throw new Exception("ICG-VAL. El punto de Vta no esta definido.");

            if (_fc.codAfipComprobante == 0)
                throw new Exception("ICG-VAL. El codigo AFIP no esta definido.");
            //Validamos el CUIT
            if (_fc.clienteTipoDocumento == 80)
            {
                if (!ValidarDigitoCuit(_fc.clienteDocumento.Replace("-","")))
                    throw new Exception("ICG-VAL. El CUIT ingresado no es valido.");
            }

        }

        public static bool ValidarDigitoCuit(string cuit)
        {
            try
            {
                int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                char[] nums = cuit.ToCharArray();
                int total = 0;
                for (int i = 0; i < mult.Length; i++)
                {
                    total += int.Parse(nums[i].ToString()) * mult[i];
                }
                int resto = total % 11;
                int _digito1 = resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
                int _digitorecibido = Convert.ToInt16(cuit.Substring(cuit.Length - 1));

                if (_digito1 == _digitorecibido)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //throw new Exception("ValidarDigitoCuit. Error: " + ex.Message);
                return false;
            }
        }

        public static List<AfipDll.wsAfipCae.AlicIva> ArmarIVAAfip(List<Modelos.TiquetsTot> _Iva)
        {
            List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();

            foreach (Modelos.TiquetsTot itm in _Iva)
            {
                AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                switch (itm.Iva.ToString())
                {
                    case "0":
                        {
                            _al.Id = 3;
                            break;
                        }
                    case "21":
                        {
                            _al.Id = 5;
                            break;
                        }
                    case "10.5":
                    case "10,5":
                        {
                            _al.Id = 4;
                            break;
                        }
                    case "27":
                        {
                            _al.Id = 6;
                            break;
                        }
                }
                _al.BaseImp = Math.Abs(Convert.ToDouble(itm.BaseImponible));
                _al.Importe = Math.Abs(Convert.ToDouble(itm.TotIva));
                _alicIVA.Add(_al);
            }
            return _alicIVA;
        }

        public static List<AfipDll.wsAfipCae.Tributo> ArmaTributos(List<Modelos.TiquetsTot> _Tributos)
        {
            //string strRta = "";
            double totalBaseImponible = 0;
            double totalImpuesto = 0;
            List<AfipDll.wsAfipCae.Tributo> _Rta = new List<AfipDll.wsAfipCae.Tributo>();
            foreach (Modelos.TiquetsTot itm in _Tributos)
            {
                totalBaseImponible = totalBaseImponible + Convert.ToDouble(itm.BaseImponible);
                totalImpuesto = totalImpuesto + Convert.ToDouble(itm.TotIva);

            }

            if (totalImpuesto != 0)
            {
                AfipDll.wsAfipCae.Tributo cls = new AfipDll.wsAfipCae.Tributo();
                cls.Alic = 3;
                if (totalBaseImponible < 0)
                    cls.BaseImp = totalBaseImponible * -1;
                else
                    cls.BaseImp = totalBaseImponible;
                cls.Desc = "Percep IIBB";
                cls.Id = 2;
                if (totalImpuesto < 0)
                    cls.Importe = totalImpuesto * -1;
                else
                    cls.Importe = totalImpuesto;
                _Rta.Add(cls);
            }
            return _Rta;
        }

        public static List<AfipDll.wsAfipCae.CbteAsoc> GetCbteAsocs(int _fo, string _serie, int _numero, string _n, string _cuit, SqlConnection _connection)
        {
            List<AfipDll.wsAfipCae.CbteAsoc> lstCbteAsoc = new List<AfipDll.wsAfipCae.CbteAsoc>();
            string sql = "SELECT DISTINCT TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.FECHA " +
                "FROM TIQUETSLIN inner join TIQUETSCAB on TIQUETSLIN.ABONODE_FO = TIQUETSCAB.FO and TIQUETSLIN.ABONODE_N = TIQUETSCAB.N " +
                "and TIQUETSLIN.ABONODE_NUMERO = TIQUETSCAB.NUMERO and TIQUETSLIN.ABONODE_SERIE = TIQUETSCAB.SERIE " +
                "WHERE TIQUETSLIN.FO = @fo and TIQUETSLIN.SERIE = @serie and TIQUETSLIN.NUMERO = @numero and TIQUETSLIN.N = @n";
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = _connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = sql;

                    cmd.Parameters.AddWithValue("@fo", _fo);
                    cmd.Parameters.AddWithValue("@serie", _serie);
                    cmd.Parameters.AddWithValue("@numero", _numero);
                    cmd.Parameters.AddWithValue("@n", _n);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime _fech = Convert.ToDateTime(reader["FECHA"]);
                                AfipDll.wsAfipCae.CbteAsoc cbteAsoc = new AfipDll.wsAfipCae.CbteAsoc();
                                cbteAsoc.Nro = Convert.ToInt64(reader["NUMEROFISCAL"]);
                                cbteAsoc.PtoVta = Convert.ToInt32(reader["SERIEFISCAL"]);
                                cbteAsoc.Tipo = Convert.ToInt32(reader["SERIEFISCAL2"].ToString().Substring(0, 3));
                                cbteAsoc.CbteFch = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                cbteAsoc.Cuit = _cuit;
                                lstCbteAsoc.Add(cbteAsoc);
                            }
                        }
                    }
                }

                return lstCbteAsoc;
            }
            catch
            {
                return lstCbteAsoc;
            }
        }

        public static int ValidaCbteAsocs(int _fo, string _serie, int _numero, string _n, string _cuit, SqlConnection _connection)
        {
            int _rta = 0;
            string sql = "SELECT DISTINCT TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.FECHA " +
                "FROM TIQUETSLIN inner join TIQUETSCAB on TIQUETSLIN.ABONODE_FO = TIQUETSCAB.FO and TIQUETSLIN.ABONODE_N = TIQUETSCAB.N " +
                "and TIQUETSLIN.ABONODE_NUMERO = TIQUETSCAB.NUMERO and TIQUETSLIN.ABONODE_SERIE = TIQUETSCAB.SERIE " +
                "WHERE TIQUETSLIN.FO = @fo and TIQUETSLIN.SERIE = @serie and TIQUETSLIN.NUMERO = @numero and TIQUETSLIN.N = @n";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = _connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = sql;

                cmd.Parameters.AddWithValue("@fo", _fo);
                cmd.Parameters.AddWithValue("@serie", _serie);
                cmd.Parameters.AddWithValue("@numero", _numero);
                cmd.Parameters.AddWithValue("@n", _n);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int _Fiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                            if (_Fiscal < 0)
                            {
                                _rta = _Fiscal;
                                return _rta;
                            }                            
                        }
                    }
                }
            }

            return _rta;
        }

        public static bool GrabarNumeroTiquetFecha(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, DateTime _fechaCF, string _cae, string _codBarra, string _errorCae,
            string _estado, string _vtocae, string _qr, SqlConnection _con)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, " +
                "NUMEROFISCAL = @numeroFiscal, " +
                "FECHAREAL = @fechaReal " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            string _sql2 = @"IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = 'PTO_VTA'
                AND Object_ID = Object_ID('TIQUETSVENTACAMPOSLIBRES'))
                BEGIN
                    UPDATE TIQUETSVENTACAMPOSLIBRES SET CAE = @cae, COD_BARRAS = @codbarra, ERROR_CAE = @errorcae, 
                        ESTADO_FE = @estado, VTO_CAE = @vtocae, PTO_VTA = @ptovta, CODIGO_QR = @codigoQR
                        WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n
                END
                ELSE
                BEGIN
	                UPDATE TIQUETSVENTACAMPOSLIBRES SET CAE = @cae, COD_BARRAS = @codbarra, ERROR_CAE = @errorcae, 
                        ESTADO_FE = @estado, VTO_CAE = @vtocae 
                        WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n
                END";

            bool _rta = false;
            //aseguro los valores
            _errorCae = _errorCae == null ? "" : _errorCae;
            _cae = _cae == null ? "" : _cae;
            _codBarra = _codBarra == null ? "" : _codBarra;
            _vtocae = _vtocae == null ? "" : _vtocae;
            //Armo los datos para el punto de venta.
            string _ptovta = _serieFiscal1 + "-" + _numeroFiscal.ToString().PadLeft(8, '0');

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);
                    _cmd.Parameters.AddWithValue("@fechaReal", _fechaCF);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                //CamposLibres
                using (SqlCommand _cmd2 = new SqlCommand())
                {
                    _cmd2.Connection = _con;
                    _cmd2.CommandType = System.Data.CommandType.Text;
                    _cmd2.CommandText = _sql2;
                    //update
                    _cmd2.Parameters.AddWithValue("@cae", _cae);
                    _cmd2.Parameters.AddWithValue("@codbarra", _codBarra);
                    _cmd2.Parameters.AddWithValue("@errorcae", _errorCae);
                    _cmd2.Parameters.AddWithValue("@estado", _estado);
                    _cmd2.Parameters.AddWithValue("@vtocae", _vtocae);
                    _cmd2.Parameters.AddWithValue("@ptovta", _ptovta);
                    _cmd2.Parameters.AddWithValue("@codigoQR", _qr);
                    //where
                    _cmd2.Parameters.AddWithValue("@fo", _fo);
                    _cmd2.Parameters.AddWithValue("@serie", _serie);
                    _cmd2.Parameters.AddWithValue("@numero", _numero);
                    _cmd2.Parameters.AddWithValue("@n", _n);


                    int _update = _cmd2.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                return _rta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        public static bool GrabarNumeroTiquetFecha(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, DateTime _fechaCF, string _cae, string _codBarra, string _errorCae,
            string _estado, string _vtocae, string _qr, SqlConnection _con, SqlTransaction _transaction)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, " +
                "NUMEROFISCAL = @numeroFiscal, " +
                "FECHAREAL = @fechaReal " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";
            //Punto de Venta
            string _sql2 = @"IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = 'PTO_VTA'
                AND Object_ID = Object_ID('TIQUETSVENTACAMPOSLIBRES'))
                BEGIN
                    UPDATE TIQUETSVENTACAMPOSLIBRES SET CAE = @cae, COD_BARRAS = @codbarra, ERROR_CAE = @errorcae, 
                        ESTADO_FE = @estado, VTO_CAE = @vtocae, PTO_VTA = @ptovta, CODIGO_QR = @codigoQR
                        WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n
                END
                ELSE
                BEGIN
	                UPDATE TIQUETSVENTACAMPOSLIBRES SET CAE = @cae, COD_BARRAS = @codbarra, ERROR_CAE = @errorcae, 
                        ESTADO_FE = @estado, VTO_CAE = @vtocae 
                        WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n
                END";

            bool _rta = false;
            //aseguro los valores
            _errorCae = _errorCae == null ? "" : _errorCae;
            _cae = _cae == null ? "" : _cae;
            _codBarra = _codBarra == null ? "" : _codBarra;
            _vtocae = _vtocae == null ? "" : _vtocae;
            //Armo los datos para el punto de venta.
            string _ptovta = _serieFiscal1 + "-" + _numeroFiscal.ToString().PadLeft(8, '0');

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transaction;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);
                    _cmd.Parameters.AddWithValue("@fechaReal", _fechaCF);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                //CamposLibres
                using (SqlCommand _cmd2 = new SqlCommand())
                {
                    _cmd2.Connection = _con;
                    _cmd2.CommandType = System.Data.CommandType.Text;
                    _cmd2.CommandText = _sql2;
                    _cmd2.Transaction = _transaction;
                    //update
                    _cmd2.Parameters.AddWithValue("@cae", _cae);
                    _cmd2.Parameters.AddWithValue("@codbarra", _codBarra);
                    _cmd2.Parameters.AddWithValue("@errorcae", _errorCae);
                    _cmd2.Parameters.AddWithValue("@estado", _estado);
                    _cmd2.Parameters.AddWithValue("@vtocae", _vtocae);
                    _cmd2.Parameters.AddWithValue("@ptovta", _ptovta);
                    _cmd2.Parameters.AddWithValue("@codigoQR", _qr);
                    //where
                    _cmd2.Parameters.AddWithValue("@fo", _fo);
                    _cmd2.Parameters.AddWithValue("@serie", _serie);
                    _cmd2.Parameters.AddWithValue("@numero", _numero);
                    _cmd2.Parameters.AddWithValue("@n", _n);


                    int _update = _cmd2.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                return _rta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Transacciones
        public static bool TransactionCAEA(Modelos.DatosFactura _factura, string _caea, string _terminal, int _numeroFiscalCaea, string _codigoBarra,
                string _ptovtacaea, string _cuit, string _pathlog, string _qr, SqlConnection _connection)
        {
            //armo la fecha de vto como la de hoy.
            string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
            //incremento el contador
            _numeroFiscalCaea = _numeroFiscalCaea + 1;
            //instancio la Transaccion
            SqlTransaction _trn;
            _trn = _connection.BeginTransaction();
            try
            {
                GrabarNumeroTiquetFecha(_factura.fo, _factura.serie, _factura.numero, _factura.n, _ptovtacaea, _factura.serieFiscal,
                    _numeroFiscalCaea, _factura.fecha, _caea, _codigoBarra, "", "SININFORMAR", _fechaVto, _qr, _connection, _trn);
                //Inserto en Rem_transacciones.
                DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _factura.serie, _factura.numero, _factura.n, _connection, _trn);
                //Modifico el contador.
                DataAccess.FuncionesVarias.UpdateContador(_ptovtacaea, _factura.serieFiscal.Substring(0, 3), _connection, _trn);
                //Comiteo
                _trn.Commit();
                return true;
            }
            catch (Exception ex)
            {
                //Se produjo un error, Rollback.
                _trn.Rollback();
                //Log
                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathlog), "TransactionCAEA Error: " + ex.Message);
                return false;
            }
        }

        public static bool TransactionManual(Modelos.DatosFactura _factura, string _terminal, int _numeroFiscalManual,
               string _ptovtamanual, string _cuit, string _pathlog, SqlConnection _connection)
        {
            //armo la fecha de vto como la de hoy.
            string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');

            SqlTransaction _trn;
            _trn = _connection.BeginTransaction();
            try
            {
                //Hardcodeo el QR porque no lo necesitamos.
                string _qr = "";
                GrabarNumeroTiquetFecha(_factura.fo, _factura.serie, _factura.numero, _factura.n, _ptovtamanual, _factura.serieFiscal,
                    _numeroFiscalManual, _factura.fecha, "", "", "", "MANUAL", _fechaVto, _qr, _connection, _trn);
                //Inserto en Rem_transacciones.
                DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _factura.serie, _factura.numero, _factura.n, _connection, _trn);
                //Modifico el contador.
                DataAccess.FuncionesVarias.UpdateContador(_ptovtamanual, _factura.serieFiscal.Substring(0, 3), _connection, _trn);
                //Comiteo
                _trn.Commit();
                return true;
            }
            catch (Exception ex)
            {
                //Se produjo un error, Rollback.
                _trn.Rollback();
                //Log
                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathlog), "TransactionManual Error: " + ex.Message);
                return false;
            }
        }

        public static void TransaccionBlack(int _fo, string _serie, int _numero, string _n, string _connectionString)
        {
            //bool _rta = false;
            string _serieFiscal1 = "0000";
            string _serieFiscal2 = "";
            int _numeroFiscal = _serie.Substring(0, 1) == "I" ? -2 : -1;

            if (_n == "N")
            {
                _numeroFiscal = 0;
                _serieFiscal1 = "";
            }

            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                SqlTransaction _tran;
                _connection.Open();
                //Comienzo la transaccion
                _tran = _connection.BeginTransaction();
                try
                {
                    //Inserto TiquetsCab
                    if(GrabarNumeroTiquetFecha( _fo, _serie, _numero, _n, _serieFiscal1, _serieFiscal2, _numeroFiscal,
                        DateTime.Now, "SINCAE", "", "", "FACTURADO", "", "", _connection, _tran))
                    {
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection, _tran);
                    }
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Proceso transaccional de Facturacion de Hotel, se le pone el numero fiscal -9.
        /// Tambien pone el nro de la reserva del hotel en el campo TIQUETSVENTACAMPOSLIBRES.COD_BARRA 
        /// </summary>
        /// <param name="_fo">Numero de FO</param>
        /// <param name="_serie">Serie</param>
        /// <param name="_numero">Numero ICG</param>
        /// <param name="_n">N</param>
        /// <param name="_tipo">tipo de movimiento</param>
        /// <param name="_connectionString">StringConnection</param>
        public static void TransaccionHotel(int _fo, string _serie, int _numero, string _n, string _reserva, string _connectionString)
        {
            //bool _rta = false;
            string _serieFiscal1 = "0000";
            string _serieFiscal2 = "";
            int _numeroFiscal = -9;

            if (_n == "N")
            {
                _numeroFiscal = 0;
                _serieFiscal1 = "";
            }

            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                SqlTransaction _tran;
                _connection.Open();
                //Comienzo la transaccion
                _tran = _connection.BeginTransaction();
                try
                {
                    //Inserto TiquetsCab
                    if (GrabarNumeroTiquetFecha(_fo, _serie, _numero, _n, _serieFiscal1, _serieFiscal2, _numeroFiscal,
                        DateTime.Now, "SINCAE", _reserva, "", "FACTURADO", "", "", _connection, _tran))
                    {
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection, _tran);
                    }
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Proceso transaccional de Facturacion black
        /// </summary>
        /// <param name="_fo">Numero de FO</param>
        /// <param name="_serie">Serie</param>
        /// <param name="_numero">Numero ICG</param>
        /// <param name="_n">N</param>
        /// <param name="_tipo">tipo de movimiento</param>
        /// <param name="_connectionString">StringConnection</param>
        public static void TransaccionBlack2(int _fo, string _serie, int _numero, string _n, int _tipo, string _connectionString)
        {
            //bool _rta = false;
            string _serieFiscal1 = "0000";
            string _serieFiscal2 = "";
            int _numeroFiscal = _serie.Substring(0, 1) == "I" ? -2 : -1;

            if (_n == "N")
            {
                _numeroFiscal = 0;
                _serieFiscal1 = "";
            }

            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                SqlTransaction _tran;
                _connection.Open();
                //Comienzo la transaccion
                _tran = _connection.BeginTransaction();
                string Tipo = "";
                try
                {
                    switch(_tipo)
                    {
                        case 1: { Tipo = "Borrado desde consola"; break; }
                        default: { Tipo = "Sin definir"; break; }
                    }
                    //Inserto TiquetsCab
                    if (GrabarNumeroTiquetFecha(_fo, _serie, _numero, _n, _serieFiscal1, _serieFiscal2, _numeroFiscal,
                        DateTime.Now, "SINCAE", "", Tipo, "FACTURADO", "", "", _connection, _tran))
                    {
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection, _tran);
                    }
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack2 " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Transaccion para cambiar el numero fiscal de un comprobante con CAEA.
        /// </summary>
        /// <param name="_nFactura">N de Factura de ICG</param>
        /// <param name="_nroFiscal">Nuevo numero fiscal para el comprobante.</param>
        /// <param name="_numeroFactura">Numero de Factura de ICG.</param>
        /// <param name="_pathLog">Directorio de LOG.</param>
        /// <param name="_serieFactura">Serie de Factura de ICG.</param>
        /// <param name="_sql">Conexion SQL.</param>
        public static void TransaccionChangeNroFiscal(string _serieFactura, int _numeroFactura, string _nFactura,
            int _nroFiscal, string _pathLog, SqlConnection _sql)
        {
            SqlTransaction _tran;
            //Comienzo la transaccion
            _tran = _sql.BeginTransaction();
            try
            {
                //Modifico el numero Fiscxal
                RestService.TiquetsCab.UpdateNroFiscalTiquetsCab(_serieFactura, _numeroFactura, _nFactura,
                        _nroFiscal, _sql, _tran);
                //Inserto en Rem_transacciones.
                //Grabo RemTransacciones. Enviamos los cambios a central.
                //DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _serieFactura, _numeroFactura, _nFactura, 1, _sql, _tran);
                DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _serieFactura, _numeroFactura, _nFactura, _sql, _tran);
                //Comiteo los cambios
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw new Exception("TransaccionBlack. Error: " + ex.Message);
            }
        }
        #endregion
        public static bool TiquetsVentaCamposLibresInformoCAEA(string _serie, int _numero, string _n, SqlConnection _Connection)
        {
            string strSql = "UPDATE TIQUETSVENTACAMPOSLIBRES SET CAEA_INFORMADO = CONVERT(varchar, getdate(), 22)," +
                " ESTADO_FE = 'FACTURADO' WHERE SERIE = @SERIE AND NUMERO = @NUMERO AND N = @N";
            //Instanciamos la conexion.
            using (SqlCommand _Command = new SqlCommand(strSql))
            {
                _Command.Connection = _Connection;
                _Command.Parameters.AddWithValue("@SERIE", _serie);
                _Command.Parameters.AddWithValue("@NUMERO", _numero);
                _Command.Parameters.AddWithValue("@N", _n);
                try
                {
                    if (_Connection.State == System.Data.ConnectionState.Closed)
                        _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool TiquetsVentaCamposLibresInformoCAEAError(int _fo, string _serie, int _numero, string _n, string _error, SqlConnection _Connection)
        {
            string strSql = "UPDATE TIQUETSVENTACAMPOSLIBRES SET ERROR_CAE = @error WHERE FO = @fo and SERIE = @serie and NUMERO = @numero and N = @N";
            //Instanciamos la conexion.
            using (SqlCommand _Command = new SqlCommand(strSql))
            {
                _Command.Connection = _Connection;
                _Command.Parameters.AddWithValue("@error", _error);
                _Command.Parameters.AddWithValue("@fo", _fo);
                _Command.Parameters.AddWithValue("@SERIE", _serie);
                _Command.Parameters.AddWithValue("@NUMERO", _numero);
                _Command.Parameters.AddWithValue("@N", _n);
                try
                {
                    if (_Connection.State == System.Data.ConnectionState.Closed)
                        _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static List<Modelos.ComprobantesConCAEA> GetComprobantesConCAEA(string _caea, SqlConnection _conexion)
        {
            string _sql = @"select TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.FO, TIQUETSCAB.CODVENDEDOR, TIQUETSVENTACAMPOSLIBRES.CAE, TIQUETSVENTACAMPOSLIBRES.ERROR_CAE,
                    TIQUETSVENTACAMPOSLIBRES.VTO_CAE, TIQUETSVENTACAMPOSLIBRES.ESTADO_FE, TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO, TIQUETSCAB.NUMEROFISCAL
                    from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                    and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                    Where TIQUETSVENTACAMPOSLIBRES.CAE = @CAEA AND TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR'
                    and (TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO is null
                    OR TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO = '') ORDER BY TIQUETSCAB.NUMEROFISCAL";

            List<Modelos.ComprobantesConCAEA> _lst = new List<Modelos.ComprobantesConCAEA>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conexion;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@CAEA", _caea);

                try
                {
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Modelos.ComprobantesConCAEA _cls = new Modelos.ComprobantesConCAEA();
                                _cls.N = reader["N"].ToString();
                                _cls.Numero = Convert.ToInt32(reader["NUMERO"]);
                                _cls.Serie = reader["SERIE"].ToString();
                                _cls.CAE = reader["CAE"].ToString();
                                _cls.CAEA_Informado = reader["CAEA_INFORMADO"].ToString();
                                _cls.Error_CAE = reader["ERROR_CAE"].ToString();
                                _cls.Estado_FE = reader["ESTADO_FE"].ToString();
                                _cls.Fo = Convert.ToInt32(reader["FO"]);
                                _cls.Vto_CAE = reader["VTO_CAE"].ToString();
                                _cls.CodVendedor = Convert.ToInt32(reader["CODVENDEDOR"]);

                                _lst.Add(_cls);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return _lst;
        }

        public static List<AfipDll.wsAfipCae.FECAEADetRequest> CargarDetalleCAEAAfip(Modelos.DatosFactura _Fac, int _NroComprobante, AfipDll.CAEA _caea)
        {
            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();

            try
            {
                AfipDll.wsAfipCae.FECAEADetRequest _det = new AfipDll.wsAfipCae.FECAEADetRequest();
                _det.Concepto = 1;
                _det.DocTipo = Convert.ToInt16(_Fac.clienteTipoDocumento);
                _det.DocNro = Convert.ToInt64(_Fac.clienteDocumento.Replace("-", ""));
                _det.CbteDesde = _NroComprobante;
                _det.CbteHasta = _NroComprobante;
                _det.CbteFch = _Fac.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0');
                _det.ImpTotal = Math.Abs(Convert.ToDouble(_Fac.totalneto));
                _det.ImpTotConc = 0;
                _det.ImpNeto = Math.Abs(Convert.ToDouble(_Fac.totalbruto));
                _det.ImpOpEx = 0;
                _det.ImpIVA = Math.Abs(Convert.ToDouble(_Fac.totIVA));
                _det.ImpTrib = Math.Abs(Convert.ToDouble(_Fac.totTributos));
                _det.FchServDesde = _Fac.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchServHasta = _Fac.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchVtoPago = _Fac.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.MonId = "PES";
                _det.MonCotiz = 1;
                _det.CAEA = _caea.Caea;
                _det.CbteFchHsGen = _Fac.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0') +
                    _Fac.hora.Hour.ToString().PadLeft(2, '0') + _Fac.hora.Minute.ToString().PadLeft(2, '0') + _Fac.hora.Second.ToString().PadLeft(2, '0');

                _detReq.Add(_det);

                return _detReq;
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al CargarDetalle. Error: " + ex.Message);
            }
        }

        public static bool ExistenComprobantesConFechaAnterior(DateTime fechaComprobante, string _caja, SqlConnection conexion)
        {
            bool rta = false;
            string sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, 
                TIQUETSCAB.FECHA
                FROM TIQUETSCAB LEFT JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO and TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE and TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N
                WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = '')
                AND year(TIQUETSCAB.FECHAANULACION) = 1899 AND TIQUETSCAB.N = 'B' AND TIQUETSCAB.SUBTOTAL = 'F' AND TIQUETSCAB.CAJA = @caja
                ORDER BY TIQUETSCAB.FECHA";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conexion;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DateTime fecha = Convert.ToDateTime(reader["FECHA"]);
                            if (fechaComprobante.Date > fecha.Date)
                                return true;
                        }
                    }
                }
            }
            return rta;
        }

        public static void LimpioDatosFiscalesAbono(string pSerie, int pNumero, string pN, SqlConnection pConexion)
        {
            string sql = @"UPDATE TIQUETSCAB SET SERIEFISCAL = '', SERIEFISCAL2 = '', NUMEROFISCAL = 0 
                where SERIE = @serie and NUMERO = @numero and N = @n";
            string sql1 = @"UPDATE TIQUETSVENTACAMPOSLIBRES SET CAE = NULL, VTO_CAE = NULL, COD_BARRAS = NULL, 
                ERROR_CAE = NULL, ESTADO_FE = 'PENDIENTE', Z_NRO = NULL WHERE SERIE = @serie AND NUMERO = @numero AND N = @n";
            try
            {
                //TiqutesCab
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = pConexion;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@serie", pSerie);
                    cmd.Parameters.AddWithValue("@numero", pNumero);
                    cmd.Parameters.AddWithValue("@n", pN);

                    cmd.ExecuteNonQuery();
                }
                //TiquetsVentaCamposLibres
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Connection = pConexion;
                    cmd1.CommandType = System.Data.CommandType.Text;
                    cmd1.CommandText = sql1;
                    cmd1.Parameters.AddWithValue("@serie", pSerie);
                    cmd1.Parameters.AddWithValue("@numero", pNumero);
                    cmd1.Parameters.AddWithValue("@n", pN);

                    cmd1.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error (LimpioDatosFiscalesAbono): " + ex.Message);
            }
        }

        public class TarjetaFidelización
        {
            /// <summary>
            /// Valida si en los pagos existe alguna tarjeta de Fidelizacion.
            /// </summary>
            /// <param name="_fo">Numero de FO</param>
            /// <param name="_serie">Serie el tiquet.</param>
            /// <param name="_numero">Numero del Tiquet</param>
            /// <param name="_N">N del Tiquet.</param>
            /// <param name="_codVendedor">Codigo del vendedor del Tiquet.</param>
            /// <param name="_Connection">Conexion SQL.</param>
            /// <returns>True/False</returns>
            public static bool GetIsTarjetaFidelizacion(int _fo, string _serie, int _numero, string _N, int _codVendedor, SqlConnection _Connection)
            {
                string _sql = @"SELECT TIQUETSPAG.CODFORMAPAGO
                    FROM TIQUETSCAB INNER JOIN TIQUETSPAG on TIQUETSCAB.FO = TIQUETSPAG.FO 
	                    AND TIQUETSCAB.SERIE = TIQUETSPAG.SERIE 
	                    AND TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO
	                    AND TIQUETSCAB.N = TIQUETSPAG.N	
	                WHERE TIQUETSCAB.fo = @fo AND TIQUETSCAB.SERIE = @serie AND TIQUETSCAB.NUMERO = @numero AND TIQUETSCAB.N = @n 
                        AND TIQUETSCAB.CODVENDEDOR = @codVendedor";

                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand(_sql))
                {
                    _cmd.Connection = _Connection;
                    _cmd.CommandType = System.Data.CommandType.Text;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@n", _N);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@codVendedor", _codVendedor);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                if (_reader["CODFORMAPAGO"].ToString() == "-1")
                                {
                                    _rta = true;
                                }
                            }
                        }
                    }
                }
                return _rta;
            }
        }

        public class TiquetsCab
        {
            public static bool UpdateNroFiscalTiquetsCab(string _numSerie, int _numFactura, string _n, int _nroFiscal,
                SqlConnection _Conection, SqlTransaction _tran)
            {
                string strSql = @"UPDATE TIQUETSCAB SET NUMEROFISCAL = @NUMEROFISCAL
                        WHERE SERIE = @NUMSERIE AND NUMERO = @NUMFACTURA  AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Conection;
                _Command.Transaction = _tran;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _numSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _numFactura);
                _Command.Parameters.AddWithValue("@N", _n);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _nroFiscal);

                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al modificar el numero fiscal. Error: " + ex.Message);
                }
            }
        }
        public class Config
        {
            /// <summary>
            /// Retorna el numero de la CAJA del Front.
            /// </summary>
            /// <param name="_connection">Conexion SAL</param>
            /// <returns>Numero de CAJA o Excepción</returns>
            public static int GetCaja(SqlConnection _connection)
            {
                int _caja = 0;

                string _ternimal = Environment.MachineName;

                string _sql = "SELECT TERMINAL, CODFO, CAJA, IDTIPOTERMINAL, VERSIONEXE From TERMINALES WHERE TERMINAL = @terminal";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@terminal", _ternimal);

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    _caja = Convert.ToInt32(_reader["CAJA"]);
                                }
                            }
                        }
                    }
                    return _caja;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static long GetCUIT(SqlConnection _connection)
            {
                long cuit = 0;

                string _sql = "SELECT NOMBRE, NOMBRECOMERCIAL, CIF from empresas";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    cuit = String.IsNullOrEmpty(_reader["CIF"].ToString()) ? 0: Convert.ToInt64(_reader["CIF"].ToString().Replace("-",""));
                                }
                            }
                        }
                    }
                    return cuit;
                }
                catch (Exception ex)
                {
                    throw new Exception("No se pudo recuperar el CUIT. Error :" + ex.Message);
                }
            }

            
            /// <summary>
            /// Recupera una lista con los tipos de compronbantes ya existentes para un pto de venta.
            /// </summary>
            /// <param name="_ptovta">Punto de venta</param>
            /// <param name="_connection">Conexion SQL</param>
            /// <returns>Lista de comprobantes</returns>
            public static List<string> GetSeriesResolucionContadores(string _ptovta, SqlConnection _connection)
            {
                List<string> _rta = new List<string>();

                string _sql = "SELECT NUMRESOL FROM SERIESRESOLUCION WHERE SERIERESOL = @ptovta";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@ptovta", _ptovta);

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    _rta.Add(_reader["NUMRESOL"].ToString());
                                }
                            }
                        }
                    }
                    return _rta;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static void InsertSeriesResolucionContadores(string _ptovta, string _tipoCbte, int _contador, SqlConnection _connection)
            {
                string _sql = @"INSERT INTO SERIESRESOLUCION (SERIERESOL, NUMRESOL, FECHA, NUMEROINICIAL, NUMEROFINAL, ACTIVO, CONTADOR, FECHAINGRESO, FECHAVENCIMIENTO)
                    Values(@ptovta, @tipoCbte, GETDATE(), 1, 99999999, 1, @contador, GETDATE(), CAST('01/01/2099' as datetime))";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@ptovta", _ptovta);
                        _cmd.Parameters.AddWithValue("@tipoCbte", _tipoCbte);
                        _cmd.Parameters.AddWithValue("@contador", _contador);

                        _cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static void UpdateContadorSeriesResolucion(string _ptovta, string _tipoCbte, int _contador, SqlConnection _connection)
            {
                string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = @contador WHERE SERIERESOL = @ptovta AND NUMRESOL = @tipoCbte";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@ptovta", _ptovta);
                        _cmd.Parameters.AddWithValue("@tipoCbte", _tipoCbte);
                        _cmd.Parameters.AddWithValue("@contador", _contador);

                        _cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static bool TengoPendientesDeInformarCAEA(string _caja, SqlConnection _connection)
            {
                string _sql = @"SELECT COUNT(*) as Quanity from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                        and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                        Where (TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR')
                        and(TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO is null
                        OR TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO = '') AND TIQUETSCAB.CAJA = @caja ";
                bool _rta = false;
                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@caja", _caja);
                        
                        var _Q =_cmd.ExecuteScalar();
                        if (Convert.ToInt32(_Q) == 0)
                            _rta = false;
                        else
                            _rta = true;
                    }
                    return _rta;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public class ValidarCamposLibres
        {
            public static bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
            {
                string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlConn;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@table", _table);
                    _cmd.Parameters.AddWithValue("column", _columna);

                    if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                        _rta = true;
                }
                return _rta;
            }

            public static string ValidarColumnasCamposLibres(string _connection)
            {
                string _msj = "";
                try
                {
                    using (SqlConnection _sqlConn = new SqlConnection(_connection))
                    {
                        _sqlConn.Open();

                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "CAE", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }

                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "COD_BARRAS", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna COD_BARRAS en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }

                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "ERROR_CAE", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna ERROR_CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }

                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "ESTADO_FE", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna ESTADO_FE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }

                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "VTO_CAE", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna VTO_CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }                                                
                        
                        if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _sqlConn))
                        {
                            _msj = _msj + "No existe la Columna Z_NRO en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error validando CamposLibres. " + ex.Message);
                }
                return _msj;
            }
            /// <summary>
            /// Funcion que valida la existencia de los campos libres enla tabla TIQUETSVENTACAMPOSLIBRES
            /// </summary>
            /// <param name="_connection">Conexión SQL</param>
            /// <returns>Mensajes de error a mostrar</returns>
            public static string ValidarColumnasCamposLibres(SqlConnection _connection)
            {
                string _msj = "";
                try
                {
                    if (_connection.State == System.Data.ConnectionState.Closed)
                        _connection.Open();

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "COD_BARRAS", _connection))
                    {
                        _msj = _msj + "No existe la Columna COD_BARRAS en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "CODIGO_QR", _connection))
                    {
                        _msj = _msj + "No existe la Columna CODIGO_QR en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "CODIGO_QR", _connection))
                    {
                        _msj = _msj + "No existe la Columna CODIGO_QR en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "ERROR_CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna ERROR_CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "ESTADO_FE", _connection))
                    {
                        _msj = _msj + "No existe la Columna ESTADO_FE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "VTO_CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna VTO_CAE en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _connection))
                    {
                        _msj = _msj + "No existe la Columna Z_NRO en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "PTO_VTA", _connection))
                    {
                        _msj = _msj + "No existe la Columna PTO_VTA en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "CAEA_INFORMADO", _connection))
                    {
                        _msj = _msj + "No existe la Columna CAEA_INFORMADO en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error validando CamposLibres. " + ex.Message);
                }
                return _msj;
            }
        }

        public class Validaciones
        {
            /// <summary>
            /// Valida el ultimo comprobante con Numero en ICG, para un punto de venta y tipo de comprobante.
            /// </summary>
            /// <param name="_ptoVta">Punto de venta</param>
            /// <param name="_numeroFiscal">Numero fiscal a validar</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante</param>
            /// <param name="_connectionString">Conection String</param>
            /// <param name="_ultimoICG">Ultimo numero fiscal en ICG</param>
            /// <returns>True si existe, False en caso contrario.</returns>
            public static bool ValidarUltimoNroComprobante(string _ptoVta, long _numeroFiscal, string _tipoComprobante, string _connectionString, out long _ultimoICG)
            {
                bool _rta = false;
                try
                {
                    string _sql = "SELECT max(NUMEROFISCAL) as ult FROM TIQUETSCAB WHERE SUBSTRING(SERIEFISCAL2,1,3) = @tipoCbte and Cast(SERIEFISCAL as int) = @ptoVta";
                    _ultimoICG = 0;
                    using (SqlConnection _connection = new SqlConnection(_connectionString))
                    {
                        _connection.Open();
                        using (SqlCommand _cmd = new SqlCommand())
                        {
                            _cmd.Connection = _connection;
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.CommandText = _sql;
                            //Parametros.
                            _cmd.Parameters.AddWithValue("@tipoCbte", _tipoComprobante);
                            _cmd.Parameters.AddWithValue("@ptoVta", Convert.ToInt32(_ptoVta));
                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _ultimoICG = String.IsNullOrEmpty(reader["ult"].ToString()) ? 0 : Convert.ToInt32(reader["ult"].ToString());
                                    }
                                }
                            }
                            if (_ultimoICG == _numeroFiscal)
                                _rta = true;
                            else
                                _rta = false;
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("ValidarUltimoNroComprobante Exception = " + ex.Message); }
                return _rta;
            }
            /// <summary>
            /// Valida si existen comprobantes con CAEA sin informar a la AFIP.
            /// </summary>
            /// <param name="_caja">Numero de caja</param>
            /// <param name="_connection">Conexión SQL</param>
            /// <returns>True/False</returns>
            public static bool ValidarComprobantesSinInformarCAEA(int _caja, SqlConnection _connection)            
            {
                bool _rta = false;
                int Cantidad = 0;
                try
                {
                    string _sql = @"SELECT Count(1) Cantidad
                        from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                        and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                        Where (TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR')
                        and(TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO is null
                        OR TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO = '') AND TIQUETSCAB.CAJA = @caja";
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        //Parametros.
                        _cmd.Parameters.AddWithValue("@caja", _caja);
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Cantidad = String.IsNullOrEmpty(reader["Cantidad"].ToString()) ? 0 : Convert.ToInt32(reader["Cantidad"].ToString());
                                }
                            }
                        }
                        if (Cantidad > 0)
                            _rta = true;
                        else
                            _rta = false;
                    }
                }
                catch (Exception ex)
                { throw new Exception("ValidarComprobantesSinInformarCAEA = " + ex.Message); }
                return _rta;
            }
            /// <summary>
            /// valida si hay comprobantes sin fiscalizar
            /// </summary>
            /// <param name="_caja">Numero de Caja</param>
            /// <param name="_connection">Conexión SQL</param>
            /// <returns>True/False</returns>
            public static bool ValidarComprobantesSinFiscalizar(int _caja, SqlConnection _connection)
            {
                bool _rta = false;
                int Cantidad = 0;
                try
                {
                    string _sql = @"SELECT COUNT(1) as Cantidad FROM TIQUETSCAB 
                        WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = 0)                         
                        AND year(TIQUETSCAB.FECHAANULACION) = 1899 AND TIQUETSCAB.N = 'B' AND TIQUETSCAB.CAJA = @caja";
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _connection;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        //Parametros.
                        _cmd.Parameters.AddWithValue("@caja", _caja);
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Cantidad = String.IsNullOrEmpty(reader["Cantidad"].ToString()) ? 0 : Convert.ToInt32(reader["Cantidad"].ToString());
                                }
                            }
                        }
                        if (Cantidad > 0)
                            _rta = true;
                        else
                            _rta = false;
                    }
                }
                catch (Exception ex)
                {
                    //throw new Exception("ValidarComprobantesSinFiscalizar = " + ex.Message);
                    _rta = false;
                }
                return _rta;
            }
        }

        public class Venice
        {
            public static Modelos.FormaPago GetFormaPago(int _fo, string _serie, int _numero, string _N, int _codVendedor, SqlConnection _Connection)
            {
                string _sql = @"SELECT FORMASPAGO.CODFORMAPAGO, FORMASPAGO.DESCRIPCION
                    FROM TIQUETSCAB INNER JOIN TIQUETSPAG on TIQUETSCAB.FO = TIQUETSPAG.FO 
	                    AND TIQUETSCAB.SERIE = TIQUETSPAG.SERIE 
	                    AND TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO
	                    AND TIQUETSCAB.N = TIQUETSPAG.N
	                    INNER JOIN FORMASPAGO on TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO
	                WHERE TIQUETSCAB.fo = @fo AND TIQUETSCAB.SERIE = @serie AND TIQUETSCAB.NUMERO = @numero AND TIQUETSCAB.N = @n 
                        AND TIQUETSCAB.CODVENDEDOR = @codVendedor";

                Modelos.FormaPago _cls = new Modelos.FormaPago();

                using (SqlCommand _cmd = new SqlCommand(_sql))
                {
                    _cmd.Connection = _Connection;
                    _cmd.CommandType = System.Data.CommandType.Text;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@n", _N);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@codVendedor", _codVendedor);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.codFormaPago = Convert.ToInt32(_reader["CODFORMAPAGO"]);
                                _cls.descripcion = _reader["DESCRIPCION"].ToString();
                           }
                        }
                    }
                }
                return _cls;
            }

            public static bool PutFactura(string _key, string _url, string _IcgKey, int _nroHabitacion, double _montoTotal, int _articulo, int _pdv)
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                bool _rta = false;

                Modelos.Venice.VeniceTransaccion _tran = new Modelos.Venice.VeniceTransaccion();
                _tran.article = _articulo;
                _tran.checkNumber = _IcgKey;
                _tran.pdv = _pdv;
                _tran.quantity = 1;
                _tran.reservation = _nroHabitacion;
                _tran.unitPrice = _montoTotal;

                try
                {                    
                    string apiUrl = _url + "/api/transactions/create";
                    var client = new RestClient(apiUrl);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("X-API-KEY", _key);
                    request.AddHeader("X-RESORT-CODE", "1");
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(_tran); ;
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        _rta = true;
                    }
                }
                catch
                {
                    _rta = false;
                }

                return _rta;
            }

            public List<Modelos.Venice.HabitacionesDTO> GetHabitaciones(string _key, string _url)
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                List<Modelos.Venice.HabitacionesDTO> _lst = new List<Modelos.Venice.HabitacionesDTO>();
                string apiUrl = _url + "/api/reservation/inhouse/list";
                var client = new RestClient(apiUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("X-API-KEY", _key);
                request.AddHeader("X-RESORT-CODE", "1");
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _lst = JsonConvert.DeserializeObject<List<Modelos.Venice.HabitacionesDTO>>(response.Content);
                }

                return _lst;
            }
        }

        public class Licencia
        {
            public static void ValidarKey(string _ptoVta, string _pathConsola, string _sql, out string _key, out string _msg)
            {
                _key = "";
                _msg = "";
                try
                {
                    var versionInfo = FileVersionInfo.GetVersionInfo(_pathConsola + "\\IcgRestFCEConsola.exe");
                    string version = versionInfo.FileVersion;
                    using (SqlConnection _connection = new SqlConnection(_sql))
                    {
                        _connection.Open();

                        string sql = "select NOMBRE, NOMBRECOMERCIAL, CIF from empresas";
                        IcgFceDll.Licencia lic = new IcgFceDll.Licencia();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = _connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = sql;

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        if (String.IsNullOrEmpty(reader["NOMBRE"].ToString()) ||
                                            String.IsNullOrEmpty(reader["NOMBRECOMERCIAL"].ToString()) ||
                                            String.IsNullOrEmpty(reader["CIF"].ToString()))
                                        {
                                            throw new Exception("Los siguientes datos de la empresa deben estar completos." + Environment.NewLine +
                                                "NOMBRE, NOMBRE COMERCIAL y CIF.");
                                        }
                                        lic.ClientCuit = reader["CIF"].ToString();
                                        lic.ClientName = reader["NOMBRE"].ToString();
                                        lic.ClientRazonSocial = reader["NOMBRECOMERCIAL"].ToString();
                                    }
                                }
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = _connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "select VALOR from PARAMETROS where TERMINAL = 'VersionBD' and CLAVE = 'VersionBD'";

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        lic.Version = reader["VALOR"].ToString();
                                    }
                                }
                            }
                        }
                        //No Aplico la nueva Key.
                        lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                        lic.password = "Pinsua.2730";
                        lic.Plataforma = "REST";
                        lic.Release = version;
                        lic.user = "pinsua@yahoo.com";
                        lic.Tipo = "FCE Comun";
                        lic.TerminalName = Environment.MachineName;
                        lic.PointOfSale = Convert.ToInt32(_ptoVta);
                        var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                        var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
#else
                            var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");
#endif

                        client.Timeout = -1;
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddParameter("application/json", json, ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            IcgFceDll.ResponcePostLicencia res = JsonConvert.DeserializeObject<IcgFceDll.ResponcePostLicencia>(response.Content);
                            _key = res.Key;
                            _msg = res.Resultado;
                        }
                        else
                        {
                            _key = "";
                            _msg = "-1 Error: ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    _msg = "-1 Error: " + ex.Message;

                }

            }
        }
    }
}
