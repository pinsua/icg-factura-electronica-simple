﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class Irsa
    {
        public class Rest
        {
            public static bool ImprimoCabecera(string _pathOut, string _local, string _contrato,
                string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento, string _rubro,
                double _totalNeto, double _baseImp, int _codVendedor,
                DateTime _fecha, DateTime _hora)
            {
                bool _rta = false;
                string _otrosImp = "0.00";

                string _line = "";

                string fileName = _pathOut + "trancomp.txt";

                try
                {
                    //Calculamos el Neto.
                    double _iva = _totalNeto - _baseImp;
                    string _vendedor = _codVendedor.ToString().Length > 2 ? _codVendedor.ToString().Substring(0, 2) : _codVendedor.ToString().PadLeft(2, '0');
                    string _tipo = "";
                    if (Convert.ToInt32(_tipoComprobante) == 6 || Convert.ToInt32(_tipoComprobante) == 1)
                        _tipo = "D";
                    else
                        _tipo = "C";

                    _line = _local.PadRight(10, ' ');
                    _line = _line + _contrato.PadLeft(10, '0');
                    _line = _line + _pos.PadLeft(2, '0');
                    _line = _line + _fecha.Year.ToString().PadLeft(4, '0') + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                    _line = _line + _hora.Hour.ToString().PadLeft(2, '0') + _hora.Minute.ToString().PadLeft(2, '0') + _hora.Second.ToString().PadLeft(2, '0');
                    _line = _line + _letra;
                    _line = _line + _tipo;
                    _line = _line + Convert.ToInt32(_ptovta).ToString().PadLeft(4, '0');
                    _line = _line + _nrocomprobante.PadLeft(8, '0');
                    _line = _line + _tipoMovimiento;
                    _line = _line + _vendedor;
                    _line = _line + "DNI";          //tipo documento.
                    _line = _line + "000000000";    //Nro de documento.
                    _line = _line + "C";
                    _line = _line + _rubro.PadLeft(4, '0');
                    _line = _line + Math.Round(Math.Abs(_baseImp), 2).ToString("N2").Replace(".", "").Replace(",", ".").PadLeft(9, '0');
                    _line = _line + Math.Round(Math.Abs(_iva), 2).ToString("N2").Replace(".", "").Replace(",", ".").PadLeft(9, '0');
                    _line = _line + _otrosImp.Replace(",", "").PadLeft(9, '0');

                    using (StreamWriter sw = new StreamWriter(fileName, true))
                    {
                        sw.WriteLine(_line);
                        sw.Flush();
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }

            public static bool ImprimoPagos(List<Pagos> _pagos, string _pathOut, string _local, string _contrato,
                string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento, string _rubro, int _codVendedor,
                DateTime _fecha, DateTime _hora)
            {
                bool _rta = false;

                string _line = "";
                string _numerotarjeta = "0";

                string fileName = _pathOut + "trancomp.txt";
                string _tipo = "";
                if (Convert.ToInt32(_tipoComprobante) == 6 || Convert.ToInt32(_tipoComprobante) == 1)
                    _tipo = "D";
                else
                    _tipo = "C";

                try
                {
                    foreach (Pagos pg in _pagos)
                    {
                        string _vendedor = _codVendedor.ToString().Length > 2 ? _codVendedor.ToString().Substring(0, 2) : _codVendedor.ToString().PadLeft(2, '0');
                        //Comun a todos
                        _line = _local.PadRight(10, ' ');
                        _line = _line + _contrato.PadLeft(10, '0');
                        _line = _line + _pos.PadLeft(2, '0');
                        _line = _line + _fecha.Year.ToString().PadLeft(4, '0') + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                        _line = _line + _hora.Hour.ToString().PadLeft(2, '0') + _hora.Minute.ToString().PadLeft(2, '0') + _hora.Second.ToString().PadLeft(2, '0');
                        _line = _line + _letra;
                        _line = _line + _tipo;
                        _line = _line + Convert.ToInt32(_ptovta).ToString().PadLeft(4, '0');
                        _line = _line + _nrocomprobante.PadLeft(8, '0');
                        _line = _line + _tipoMovimiento;
                        _line = _line + _vendedor;
                        _line = _line + "DNI";          //tipo documento.
                        _line = _line + "000000000";    //Nro de documento.
                                                        //Por tipo de pago.
                        _line = _line + "P";
                        _line = _line + pg.FormaPago.PadLeft(1, ' ');
                        _line = _line + pg.CodTarjeta.PadLeft(2, ' ');
                        _line = _line + _numerotarjeta.PadLeft(22, '0');
                        _line = _line + Math.Round(Math.Abs(pg.monto), 2).ToString("N2").Replace(".", "").Replace(",", ".").PadLeft(9, '0');

                        using (StreamWriter sw = new StreamWriter(fileName, true))
                        {
                            sw.WriteLine(_line);
                            sw.Flush();
                        }
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }

            /// <summary>
            /// Lanza el proceso para informar las ventas al host de IRSA
            /// </summary>
            /// <param name="_tipoComprobante">Tipo de Comprobante AFIP</param>
            /// <param name="_nroComprobante">Numero del comprobante PtoVta + Numero</param>
            /// <param name="_conexion">Conexion SQL abierta.</param>
            public static void LanzarTrancomp(InfoIrsa _infoirsa,
                string _tipoComprobante, string _nroFiscal, string _ptoVenta, SqlConnection _conexion)
            {
                try
                {
                    string _letra = "";
                    string _clMov = "N";

                    switch (Convert.ToInt32(_tipoComprobante))
                    {
                        case 6:
                        case 7:
                        case 8:
                            { _letra = "B"; break; }
                        case 1:
                        case 2:
                        case 3:
                            { _letra = "A"; break; }
                    }

                    //Recupero la cabecera
                    Cabecera _cab = GetCabecera(_nroFiscal, _ptoVenta, _conexion);
                    //Recupero los pagos
                    List<Pagos> _pagos = GetPagos(_cab.serie, _cab.numero, _cab.n, _conexion);

                    //Monto Neto > cero implica normal.
                    ImprimoCabecera(_infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                        _tipoComprobante, _ptoVenta, _nroFiscal, _clMov, _infoirsa.rubro,
                        _cab.totalneto, _cab.BaseImp, _cab.codvendedor, _cab.fecha, _cab.hora);

                    ImprimoPagos(_pagos, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                        _tipoComprobante, _ptoVenta, _nroFiscal, _clMov, _infoirsa.rubro,
                        _cab.codvendedor, _cab.fecha, _cab.hora);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static Cabecera GetCabecera(string _nroFiscal, string _ptoVta, SqlConnection _con)
            {
                //Consulta con modificacion
                string _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.TOTALBRUTO, TIQUETSCAB.TOTALNETO, 
                    TIQUETSCAB.CODCLIENTE, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.FECHA, TIQUETSCAB.ENTREGADO, TIQUETSCAB.PROPINA,TIQUETSCAB.SERIEFISCAL 
                    , TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, VENDEDORES.NOMBREVENDEDOR, TIQUETSCAB.SALA, TIQUETSCAB.MESA, TIQUETSCAB.HORAFIN, 
                    TIQUETSCAB.BASEIMP1, TIQUETSCAB.SUPEDIDO, TIQUETSCAB.ALIASESPERA
                    FROM TIQUETSCAB inner join VENDEDORES on TIQUETSCAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                    WHERE TIQUETSCAB.NUMEROFISCAL = @NroFiscal and CAST(TIQUETSCAB.SERIEFISCAL AS int) = @PtoVta";

                Cabecera _cabecera = new Cabecera();

                int _PtoVta = Convert.ToInt32(_ptoVta);

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@NroFiscal", _nroFiscal);
                    _cmd.Parameters.AddWithValue("@PtoVta", _PtoVta);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cabecera.fo = Convert.ToInt32(_reader["Fo"]);//.GetInt32(0);
                                _cabecera.serie = _reader.GetString(1);
                                _cabecera.numero = _reader.GetInt32(2);
                                _cabecera.n = _reader.GetString(3);
                                _cabecera.totalbruto = _reader.GetDouble(4);
                                _cabecera.totalneto = _reader.GetDouble(5);
                                _cabecera.codcliente = _reader.GetInt32(6);
                                _cabecera.codvendedor = _reader.GetInt32(7);
                                _cabecera.fecha = _reader.GetDateTime(8);
                                _cabecera.entregado = _reader.GetDouble(9);
                                _cabecera.propina = _reader.GetDouble(10);
                                _cabecera.serieFiscal = _reader.GetString(11);
                                _cabecera.serieFiscal2 = _reader.GetString(12);
                                _cabecera.numeroFiscal = _reader.GetInt32(13);
                                _cabecera.nomVendedor = _reader.GetString(14);
                                _cabecera.sala = Convert.ToInt32(_reader[15]);
                                _cabecera.mesa = Convert.ToInt32(_reader[16]);
                                _cabecera.hora = _reader.GetDateTime(17);
                                _cabecera.BaseImp = _reader.GetDouble(18);
                                _cabecera.aliasespera = _reader[19] == null ? "" : _reader[19].ToString();
                                _cabecera.supedido = _reader[20] == null ? "" : _reader[20].ToString();
                            }
                        }
                    }
                }

                return _cabecera;
            }

            public static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
            {
                string _sql = "SELECT TIQUETSPAG.IMPORTE, FORMASPAGO.DESCRIPCION as TARJETA, FORMASPAGO.METALICO, TIQUETSPAG.ENTREGADO, " +
                    "FORMASPAGO.TEXTOIMP as CodTarjeta, FORMASPAGO.MARCASTARJETA as FormaPago, FORMASPAGO.ABRIRCAJON " +
                   "FROM TIQUETSPAG INNER JOIN FORMASPAGO ON TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO " +
                   "WHERE TIQUETSPAG.serie = @serie AND TIQUETSPAG.NUMERO = @numero AND TIQUETSPAG.N = @n";

                List<Pagos> _pagos = new List<Pagos>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@n", _N);
                    _cmd.Parameters.AddWithValue("@numero", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                Pagos _cls = new Pagos();
                                _cls.descripcion = _reader["TARJETA"].ToString();
                                _cls.tipopago = _reader["METALICO"].ToString();
                                _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                                _cls.cuotas = "";
                                _cls.cupon = "";
                                _cls.entregado = Convert.ToDecimal(_reader["ENTREGADO"]);
                                _cls.CodTarjeta = _reader["CodTarjeta"] == null ? "" : _reader["CodTarjeta"].ToString();
                                _cls.FormaPago = _reader["FormaPago"] == null ? "" : _reader["FormaPago"].ToString();
                                _cls.AbrirCajon = Convert.ToBoolean(_reader["AbrirCajon"]);

                                _pagos.Add(_cls);
                            }
                        }
                    }
                }

                return _pagos;
            }

            public static List<Regenerar> GetRegenerar(DateTime _desde, DateTime _hasta, SqlConnection _con)
            {
                //Consulta con modificacion
                string _sql = @"select SERIE, NUMERO, N, NUMEROFISCAL, SERIEFISCAL, SERIEFISCAL2 from TIQUETSCAB where CAST(fecha as date ) >= CAST(@desde as date) and CAST(fecha as date) <= CAST(@hasta as date)";

                List<Regenerar> _lst = new List<Regenerar>();
                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@desde", _desde);
                        _cmd.Parameters.AddWithValue("@hasta", _hasta);

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    if (Convert.ToInt32(_reader["NUMEROFISCAL"]) > 0)
                                    {
                                        if (!String.IsNullOrEmpty(_reader["SERIEFISCAL2"].ToString()))
                                        {
                                            if (!String.IsNullOrEmpty(_reader["SERIEFISCAL"].ToString()))
                                            {
                                                Regenerar _cls = new Regenerar();
                                                _cls.N = _reader["N"].ToString();
                                                _cls.Numero = Convert.ToInt32(_reader["NUMERO"]);
                                                _cls.NumeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                                                _cls.PuntoVenta = _reader["SERIEFISCAL"].ToString();
                                                _cls.Serie = _reader["SERIE"].ToString();
                                                _cls.SerieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                                                _lst.Add(_cls);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { }

                return _lst;
            }
        }
        public class Retail
        {
            public static bool ImprimoCabecera(Cabecera _cab, string _pathOut, string _local, string _contrato,
            string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
            string _tipoMovimiento, string _rubro)
            {
                bool _rta = false;
                string _otrosImp = "0.00";

                string _line = "";
                string _tipo = "";

                string fileName = _pathOut + "trancomp.txt";

                try
                {
                    if (Convert.ToInt32(_tipoComprobante) == 6 || Convert.ToInt32(_tipoComprobante) == 1)
                        _tipo = "D";
                    else
                        _tipo = "C";
                    //Calculamos el Neto.
                    double _totalNeto = _cab.totalbruto - _cab.totdtocomercial - _cab.totdtopp;
                    string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');

                    _line = _local.PadRight(10, ' ');
                    _line = _line + _contrato.PadLeft(10, '0');
                    _line = _line + _pos.PadLeft(2, '0');
                    _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                    _line = _line + _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0') + _cab.hora.Second.ToString().PadLeft(2, '0');
                    _line = _line + _letra;
                    _line = _line + _tipo;
                    _line = _line + _ptovta.PadLeft(4, '0');
                    _line = _line + _nrocomprobante.PadLeft(8, '0');
                    _line = _line + _tipoMovimiento;
                    //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                    _line = _line + _vendedor;
                    _line = _line + "DNI";          //tipo documento.
                    _line = _line + "000000000";    //Nro de documento.
                    _line = _line + "C";
                    _line = _line + _rubro.PadLeft(4, '0');
                    //_line = _line + Math.Abs(_cab.totalbruto).ToString().Replace(",", ".").PadLeft(9, '0');
                    _line = _line + Math.Round(Math.Abs(_totalNeto), 2).ToString().Replace(",", ".").PadLeft(9, '0');
                    _line = _line + Math.Round(Math.Abs(_cab.totalimpuestos), 2).ToString().Replace(",", ".").PadLeft(9, '0');
                    _line = _line + _otrosImp.Replace(",", ".").PadLeft(9, '0');

                    using (StreamWriter sw = new StreamWriter(fileName, true))
                    {
                        sw.WriteLine(_line);
                        sw.Flush();
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }

            public static bool ImprimoPagos(Cabecera _cab, List<Pagos> _pagos, string _pathOut, string _local, string _contrato,
                string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento, string _rubro)
            {
                bool _rta = false;

                string _line = "";
                string _numerotarjeta = "0";
                string _tipo = "";

                string fileName = _pathOut + "trancomp.txt";

                if (Convert.ToInt32(_tipoComprobante) == 6 || Convert.ToInt32(_tipoComprobante) == 1)
                    _tipo = "D";
                else
                    _tipo = "C";

                List<Pagos> _new = new List<Pagos>();

                foreach (Pagos x in _pagos)
                {
                    if (x.formapago.ToUpper() != "E")
                    {
                        if (x.descripcion.ToUpper() != "ANTICIPO")
                        {
                            Pagos _p = new Pagos()
                            {
                                descripcion = x.descripcion,
                                tipopago = x.tipopago,
                                monto = x.monto,
                                cuotas = x.cuotas,
                                cupon = x.cupon,
                                formapago = x.formapago,
                                codigotarjeta = x.codigotarjeta
                            };

                            _new.Add(_p);
                        }
                        else
                        {
                            x.formapago = "E";
                        }
                    }
                }
                //Calculamos el total del efectivo.
                decimal _totEfectivo = _pagos.Where(x => x.formapago.ToUpper() == "E").Sum(x => x.monto);
                //vemos si tenemos efectivo.
                if (Math.Abs(_totEfectivo) > 0)
                {
                    Pagos _p = new Pagos()
                    {
                        descripcion = "EFECTIVO",
                        tipopago = "EFECTIVO",
                        monto = Math.Round(Math.Abs(_totEfectivo), 2),
                        cuotas = "",
                        cupon = "",
                        formapago = "E",
                        codigotarjeta = ""
                    };
                    _new.Add(_p);
                }

                try
                {
                    foreach (Pagos pg in _new)
                    {
                        string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');
                        //Comun a todos
                        _line = _local.PadRight(10, ' ');
                        _line = _line + _contrato.PadLeft(10, '0');
                        _line = _line + _pos.PadLeft(2, '0');
                        _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                        _line = _line + _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0') + _cab.hora.Second.ToString().PadLeft(2, '0');
                        _line = _line + _letra;
                        _line = _line + _tipo;
                        _line = _line + _ptovta.PadLeft(4, '0');
                        _line = _line + _nrocomprobante.PadLeft(8, '0');
                        _line = _line + _tipoMovimiento;
                        //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                        _line = _line + _vendedor;
                        _line = _line + "DNI";          //tipo documento.
                        _line = _line + "000000000";    //Nro de documento.
                                                        //Por tipo de pago.
                        _line = _line + "P";
                        _line = _line + pg.formapago.PadLeft(1, ' ');
                        _line = _line + pg.codigotarjeta.PadLeft(2, ' ');
                        _line = _line + _numerotarjeta.PadLeft(22, '0');
                        _line = _line + Math.Round(Math.Abs(pg.monto), 2).ToString("N2").Replace(".", "").Replace(",", ".").PadLeft(9, '0');

                        using (StreamWriter sw = new StreamWriter(fileName, true))
                        {
                            sw.WriteLine(_line);
                            sw.Flush();
                        }
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


                return _rta;
            }

            public static bool ImprimoCancelado(Cabecera _cab, string _pathOut, string _local, string _contrato,
                string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento, string _rubro)
            {
                bool _rta = false;
                string _otrosImp = "0.00";
                string _impNeto = "0.00";
                string _impIVA = "0.00";

                string _line = "";
                string _tipo = "";

                string fileName = _pathOut + "trancomp.txt";

                try
                {
                    if (Convert.ToInt32(_tipoComprobante) == 6 || Convert.ToInt32(_tipoComprobante) == 1)
                        _tipo = "D";
                    else
                        _tipo = "C";

                    //Calculamos el Neto.
                    double _totalNeto = _cab.totalbruto - _cab.totdtocomercial - _cab.totdtopp;
                    string _vendedor = _cab.codvendedor.ToString().Length > 2 ? _cab.codvendedor.ToString().Substring(0, 2) : _cab.codvendedor.ToString().PadLeft(2, '0');

                    _line = _local.PadRight(10, ' ');
                    _line = _line + _contrato.PadLeft(10, '0');
                    _line = _line + _pos.PadLeft(2, '0');
                    _line = _line + _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                    _line = _line + _cab.fecha.Hour.ToString().PadLeft(2, '0') + _cab.fecha.Minute.ToString().PadLeft(2, '0') + _cab.fecha.Second.ToString().PadLeft(2, '0');
                    _line = _line + _letra;
                    _line = _line + _tipo;
                    _line = _line + _ptovta.PadLeft(4, '0');
                    _line = _line + _nrocomprobante.PadLeft(8, '0');
                    _line = _line + _tipoMovimiento;
                    //_line = _line + _cab.codvendedor.ToString().PadLeft(2, '0');
                    _line = _line + _vendedor;
                    _line = _line + "DNI";          //tipo documento.
                    _line = _line + "000000000";    //Nro de documento.
                    _line = _line + "C";
                    _line = _line + _rubro.PadLeft(4, '0');
                    _line = _line + _impNeto.Replace(",", ".").PadLeft(9, '0');
                    _line = _line + _impIVA.Replace(",", ".").PadLeft(9, '0');
                    _line = _line + _otrosImp.Replace(",", ".").PadLeft(9, '0');

                    using (StreamWriter sw = new StreamWriter(fileName, true))
                    {
                        sw.WriteLine(_line);
                        sw.Flush();
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


                return _rta;
            }

            public static void ExisteComprobante(SqlConnection _con,
                Cabecera _cab, string _pathOut, string _local, string _contrato,
                string _pos, string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento, string _rubro)
            {
                string _sql = @"Select NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL from FACTURASVENTASERIESRESOL 
                Where (SERIEFISCAL1 = @PtoVta) And NUMEROFISCAL = (@Numero)";

                int _numero = Convert.ToInt32(_nrocomprobante) - 1;

                Tickets _tck = new Tickets();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros
                    _cmd.Parameters.AddWithValue("@PtoVta", _ptovta);
                    _cmd.Parameters.AddWithValue("@Numero", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _tck.N = _reader["N"].ToString();
                                _tck.NumeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                                _tck.NumFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                                _tck.NumSerie = _reader["NUMSERIE"].ToString();
                                _tck.SerieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                                _tck.SerieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                            }
                        }
                    }
                }
                //Vemos si tenemos algo
                if (_tck.NumSerie == null)
                {
                    ImprimoCancelado(_cab, _pathOut, _local, _contrato, _pos, _letra, _tipoComprobante, _ptovta, _numero.ToString(), _tipoMovimiento, _rubro);
                }

            }

            /// <summary>
            /// Lanza el proceso para informar las ventas al host de IRSA
            /// </summary>
            /// <param name="_cabecera">Cabecera de la venta.</param>
            /// <param name="_pagos">Lista de Pagos</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante a informas N op C</param>
            /// <param name="_nroComprobante">Numero del comprobante PtoVta + Numero</param>
            /// <param name="_conexion">Conexion SQL abierta.</param>
            public static void LanzarTrancomp(string _serie, int _numero, string _n, InfoIrsa _infoirsa,
                string _tipoComprobante, string _nroComprobante, SqlConnection _conexion)
            {
                try
                {
                    string _letra = "";
                    string _ptoVta = "";
                    string _nroFiscal = "";
                    string _clMov = "N";

                    Cabecera _cabecera = Cabecera.GetCabecera(_serie, _numero, _n, _conexion);
                    List<Pagos> _pagos = Pagos.GetPagos(_serie, _numero, _n, _conexion);

                    if (_cabecera.regfacturacioncliente != 1)
                        _letra = "B";
                    else
                        _letra = "A";

                    string[] _nro = _nroComprobante.Split('-');
                    if (_nro.Length == 2)
                    {
                        _ptoVta = Convert.ToInt32(_nro[0]).ToString();
                        _nroFiscal = _nro[1];
                    }

                    ExisteComprobante(_conexion, _cabecera, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                        _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);

                    ImprimoCabecera(_cabecera, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                        _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);

                    ImprimoPagos(_cabecera, _pagos, _infoirsa.pathSalida, _infoirsa.local, _infoirsa.contrato, _infoirsa.pos, _letra,
                        _tipoComprobante, _ptoVta, _nroFiscal, _clMov, _infoirsa.rubro);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            /// <summary>
            /// Metodo que devuelve una lista con la claves de los comprobantes comprendidos entre dos fechas.
            /// </summary>
            /// <param name="Desde">Fecha Desde.</param>
            /// <param name="Hasta">Fecha Hasta.</param>
            /// <param name="_conexion">Conexion SQL ABIERTA.</param>
            /// <returns>Lista</returns>
            public static List<Tickets> GetTikets(DateTime Desde, DateTime Hasta, SqlConnection _conexion)
            {
                List<Tickets> lista = new List<Tickets>();

                string query = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N,
                    FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL
                    FROM FACTURASVENTA INNER JOIN FACTURASVENTASERIESRESOL ON
                    FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    WHERE FACTURASVENTA.FECHA >= @fDesde 
                    AND FACTURASVENTA.FECHA <= @fHasta";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _conexion;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = query;
                        //Parametros
                        _cmd.Parameters.AddWithValue("@fDesde", Desde);
                        _cmd.Parameters.AddWithValue("@fHasta", Hasta);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Tickets unticket = new Tickets();
                                    unticket.N = reader["N"].ToString();
                                    unticket.NumFactura = Convert.ToInt32(reader["NUMFACTURA"]);
                                    unticket.NumSerie = reader["NUMSERIE"].ToString();
                                    unticket.NumeroFiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    unticket.SerieFiscal1 = reader["SERIEFISCAL1"].ToString();
                                    unticket.SerieFiscal2 = reader["SERIEFISCAL2"].ToString();
                                    lista.Add(unticket);
                                }
                            }
                        }
                    }
                    return lista;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            /// <summary>
            /// Clase de información de cabecera del ticket.
            /// </summary>
            public class Cabecera
            {
                public string numserie { get; set; }
                public int numalbaran { get; set; }
                public string n { get; set; }
                public int numfac { get; set; }
                public int codcliente { get; set; }
                public int codvendedor { get; set; }
                public double dtocomercial { get; set; }
                public double totdtocomercial { get; set; }
                public double totalbruto { get; set; }
                public double totalimpuestos { get; set; }
                public double totalneto { get; set; }
                public int tipodoc { get; set; }
                public string nombrecliente { get; set; }
                public string direccioncliente { get; set; }
                public string nrodoccliente { get; set; }
                public string codpostalcliente { get; set; }
                public string ciudadcliente { get; set; }
                public string provinciacliente { get; set; }
                public int regfacturacioncliente { get; set; }
                public string descripcionticket { get; set; }
                public double dtopp { get; set; }
                public double totdtopp { get; set; }
                public double entregado { get; set; }
                public DateTime fecha { get; set; }
                public int z { get; set; }
                public string telefono { get; set; }
                public string nomvendedor { get; set; }
                public DateTime hora { get; set; }

                public static Cabecera GetCabecera(string _serie, int _numero, string _N, SqlConnection _con)
                {
                    string _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC, ALBVENTACAB.CODCLIENTE, ALBVENTACAB.CODVENDEDOR, " +
                        "ALBVENTACAB.DTOCOMERCIAL, ALBVENTACAB.TOTDTOCOMERCIAL, ALBVENTACAB.TOTALBRUTO, ALBVENTACAB.TOTALIMPUESTOS, ALBVENTACAB.TOTALNETO, ALBVENTACAB.TIPODOC, ALBVENTACAB.DTOPP, ALBVENTACAB.TOTDTOPP, " +
                        "CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, " +
                        "TIPOSDOC.DESCRIPCION, FACTURASVENTA.ENTREGADO, ALBVENTACAB.FECHA, ALBVENTACAB.Z, CLIENTES.TELEFONO1, VENDEDORES.NOMVENDEDOR, ALBVENTACAB.HORA " +
                        "FROM FACTURASVENTA INNER JOIN  ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.N " +
                        "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                        "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                        "INNER JOIN VENDEDORES on ALBVENTACAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR " +
                        "WHERE FACTURASVENTA.NUMSERIE = @Numserie AND facturasventa.NUMFACTURA = @NumFact AND FACTURASVENTA.N = @N";

                    Cabecera _cabecera = new Cabecera();

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@Numserie", _serie);
                        _cmd.Parameters.AddWithValue("@N", _N);
                        _cmd.Parameters.AddWithValue("@NumFact", _numero);

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    _cabecera.ciudadcliente = _reader["POBLACION"].ToString();
                                    _cabecera.codcliente = Convert.ToInt32(_reader["CODCLIENTE"]);
                                    _cabecera.codpostalcliente = _reader["CODPOSTAL"].ToString();
                                    _cabecera.codvendedor = Convert.ToInt32(_reader["CODVENDEDOR"]);
                                    _cabecera.descripcionticket = _reader["DESCRIPCION"].ToString();
                                    _cabecera.direccioncliente = _reader["DIRECCION1"].ToString();
                                    _cabecera.dtocomercial = Convert.ToDouble(_reader["DTOCOMERCIAL"]);
                                    _cabecera.n = _reader["N"].ToString();
                                    _cabecera.nombrecliente = _reader["NOMBRECLIENTE"].ToString();
                                    _cabecera.nrodoccliente = _reader["NIF20"].ToString();
                                    _cabecera.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                                    _cabecera.numfac = Convert.ToInt32(_reader["NUMFAC"]);
                                    _cabecera.numserie = _reader["NUMSERIE"].ToString();
                                    _cabecera.provinciacliente = _reader["PROVINCIA"].ToString();
                                    _cabecera.regfacturacioncliente = Convert.ToInt32(_reader["REGIMFACT"]);
                                    _cabecera.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                                    _cabecera.totalbruto = Convert.ToDouble(_reader["TOTALBRUTO"]);
                                    _cabecera.totalimpuestos = Convert.ToDouble(_reader["TOTALIMPUESTOS"]);
                                    _cabecera.totalneto = Convert.ToDouble(_reader["TOTALNETO"]);
                                    _cabecera.totdtocomercial = Convert.ToDouble(_reader["TOTDTOCOMERCIAL"]);
                                    _cabecera.dtopp = Convert.ToDouble(_reader["DTOPP"]);
                                    _cabecera.totdtopp = Convert.ToDouble(_reader["TOTDTOPP"]);
                                    _cabecera.entregado = Convert.ToDouble(_reader["ENTREGADO"]);
                                    _cabecera.fecha = Convert.ToDateTime(_reader["FECHA"]);
                                    _cabecera.z = Convert.ToInt32(_reader["Z"]);
                                    _cabecera.telefono = _reader["TELEFONO1"].ToString();
                                    _cabecera.nomvendedor = _reader["NOMVENDEDOR"].ToString();
                                    _cabecera.hora = Convert.ToDateTime(_reader["HORA"]);
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    return _cabecera;
                }
            }
            /// <summary>
            /// Clase con los datos de pago
            /// </summary>
            public class Pagos
            {
                public string descripcion { get; set; }
                public string tipopago { get; set; }
                public decimal monto { get; set; }
                public string cuotas { get; set; }
                public string cupon { get; set; }
                public string formapago { get; set; }
                public string codigotarjeta { get; set; }
                public string olmosTipoPago { get; set; }
                public string olmosTarjeta { get; set; }

                public static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
                {

                    string _sql = @"SELECT (TESORERIA.IMPORTE * TESORERIA.FACTORMONEDA) as IMPORTE, IsNull(FORMASPAGO.DESCRIPCION, 'Anticipo') as TARJETA, 
                IsNull(TIPOSPAGO.DESCRIPCION, 'Anticipo') as TIPOPAGO, ISNULL(tipospago.MARCASTARJETA,'') as FormaPago, 
                ISNULL(FORMASPAGO.TEXTOIMP,'') as CodigoTarjeta, TESORERIA.COMENTARIOVISIBLE,
                FORMASPAGO.MARCASTARJETA as OlmosTipoPago, FORMASPAGO.CODFORMAPAGOSOBREPAGO as OlmosTarjeta
                FROM TESORERIA 
                LEFT JOIN FORMASPAGO ON TESORERIA.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                LEFT JOIN VENCIMFPAGO ON FORMASPAGO.CODFORMAPAGO = VENCIMFPAGO.CODFORMAPAGO 
                LEFT JOIN TIPOSPAGO ON VENCIMFPAGO.CODTIPOPAGO = TIPOSPAGO.CODTIPOPAGO 
                WHERE TESORERIA.serie = @NumSerie AND TESORERIA.NUMERO = @Numero AND TESORERIA.N = @N";

                    List<Pagos> _pagos = new List<Pagos>();

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@NumSerie", _serie);
                        _cmd.Parameters.AddWithValue("@N", _N);
                        _cmd.Parameters.AddWithValue("@Numero", _numero);

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    Pagos _cls = new Pagos();
                                    _cls.descripcion = _reader["TARJETA"].ToString();
                                    _cls.tipopago = _reader["TIPOPAGO"].ToString();
                                    _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                                    _cls.cuotas = _reader["COMENTARIOVISIBLE"].ToString();
                                    _cls.cupon = "";
                                    _cls.formapago = _reader["FormaPago"].ToString();
                                    _cls.codigotarjeta = _reader["CodigoTarjeta"].ToString();
                                    _cls.olmosTipoPago = _reader["OlmosTipoPago"].ToString();
                                    _cls.olmosTarjeta = _reader["OlmosTarjeta"].ToString();

                                    _pagos.Add(_cls);
                                }
                            }
                        }
                    }

                    return _pagos;
                }                
            }
            /// <summary>
            /// Clase de tickets a procesar.
            /// </summary>
            public class Tickets
            {
                public string NumSerie { get; set; }
                public int NumFactura { get; set; }
                public string N { get; set; }
                public string SerieFiscal1 { get; set; }
                public string SerieFiscal2 { get; set; }
                public int NumeroFiscal { get; set; }
            }
        }
    }
    public class Cabecera
    {
        public int fo { get; set; }
        public string serie { get; set; }
        public int numero { get; set; }
        public string n { get; set; }
        public double totalbruto { get; set; }
        public double totalneto { get; set; }
        public int codcliente { get; set; }
        public int codvendedor { get; set; }
        public DateTime fecha { get; set; }
        public double entregado { get; set; }
        public double propina { get; set; }
        public string serieFiscal { get; set; }
        public string serieFiscal2 { get; set; }
        public int numeroFiscal { get; set; }
        public string nomVendedor { get; set; }
        public int sala { get; set; }
        public int mesa { get; set; }
        public double BaseImp { get; set; }
        public DateTime hora { get; set; }
        public string aliasespera { get; set; }
        public string supedido { get; set; }
    }

    public class Pagos
    {
        public string descripcion { get; set; }
        public string tipopago { get; set; }
        public decimal monto { get; set; }
        public string cuotas { get; set; }
        public string cupon { get; set; }
        public decimal entregado { get; set; }
        public string CodTarjeta { get; set; }
        public string FormaPago { get; set; }
        public bool AbrirCajon { get; set; }
    }

    public class InfoIrsa
    {
        /// <summary>
        /// Contrato de IRSA
        /// </summary>
        public string contrato { get; set; }
        /// <summary>
        /// Numero de Local de IRSA
        /// </summary>
        public string local { get; set; }
        /// <summary>
        /// Directorio / ubicación donde se debe dejar el archivo.
        /// </summary>
        public string pathSalida { get; set; }
        /// <summary>
        /// POS de IRSA.
        /// </summary>
        public string pos { get; set; }
        /// <summary>
        /// Rubro de IRSA.
        /// </summary>
        public string rubro { get; set; }
    }

    public class Regenerar
    {
        public string Serie { get; set; }
        public int Numero { get; set; }
        public string N { get; set; }
        public int NumeroFiscal { get; set; }
        public string PuntoVenta { get; set; }
        public string SerieFiscal2 { get; set; }
    }
}
