﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class QR
    {
        public class QRjson
        {
            public int ver { get; set; }
            public string fecha { get; set; }
            public long cuit { get; set; }
            public int ptoVta { get; set; }
            public int tipoCmp { get; set; }
            public int nroCmp { get; set; }
            public decimal importe { get; set; }
            public string moneda { get; set; }
            public decimal ctz { get; set; }
            public int tipoDocRec { get; set; }
            public long nroDocRec { get; set; }
            public string tipoCodAut { get; set; }
            public long codAut { get; set; }
        }

        /// <summary>
        /// Metodo que retorna el JSON de AFIP ya codificado.
        /// </summary>
        /// <param name="pVersion">Version del Json por default 1</param>
        /// <param name="pFecha">Fecha del comprobante</param>
        /// <param name="pCuit">CUIT del Emisor</param>
        /// <param name="pPtoVta">Punto de Venta</param>
        /// <param name="pTipoComp">Tipo de Comprobante</param>
        /// <param name="pNroCmp">Numero del Comprobante</param>
        /// <param name="pImporte">Importe Total del comprobante</param>
        /// <param name="pMondeda">Moneda del comprobante.</param>
        /// <param name="pCtz">Cotización de la moneda</param>
        /// <param name="pTipoDocRec">Tipo de documento del receptor</param>
        /// <param name="pNroDocRec">Numero de documento del receptor</param>
        /// <param name="pTipoCodAut">Tipo de codigo de autorización A=CAEA, E=CAE</param>
        /// <param name="pCodAut">CAE/CAEA</param>
        /// <returns>Texto para el QR, con url y json codificado.</returns>
        public static string CrearJson(int pVersion, DateTime pFecha, long pCuit, int pPtoVta, int pTipoComp, int pNroCmp,
            decimal pImporte, string pMondeda, decimal pCtz, int pTipoDocRec, long pNroDocRec, string pTipoCodAut, long pCodAut)
        {
            try
            {
                string urlAfip = "https://www.afip.gob.ar/fe/qr/?p=";
                QRjson json = new QRjson();
                json.ver = pVersion;
                json.fecha = pFecha.Year.ToString().PadLeft(4, '0') + "-" + pFecha.Month.ToString().PadLeft(2, '0') + "-" + pFecha.Day.ToString().PadLeft(2, '0');
                json.cuit = pCuit;
                json.ptoVta = pPtoVta;
                json.tipoCmp = pTipoComp;
                json.nroCmp = pNroCmp;
                json.importe = Math.Round(pImporte, 2);
                json.moneda = pMondeda;
                json.ctz = Math.Round(pCtz, 6);
                json.tipoDocRec = pTipoDocRec;
                json.nroDocRec = pNroDocRec;
                json.tipoCodAut = pTipoCodAut;
                json.codAut = pCodAut;
                //Seriealizamos y Convertimos
                string qrStr = JsonConvert.SerializeObject(json);
                string base64EncodedExternalAccount = Convert.ToBase64String(Encoding.UTF8.GetBytes(qrStr));
                //byte[] byteArray = Convert.FromBase64String(base64EncodedExternalAccount);
                //string jsonBack = Encoding.UTF8.GetString(byteArray);
                //Sumamos la Url
                string retorno = urlAfip + base64EncodedExternalAccount;
                //Retornamos el string codificado.
                return retorno;
            }
            catch
            {                
                string retorno = "";
                //Retornamos el string codificado.
                return retorno;
            }            
        }

        /// <summary>
        /// Metodo que retorna el JSON de AFIP ya codificado.
        /// </summary>
        /// <param name="pVersion">Version del Json por default 1</param>
        /// <param name="pFecha">Fecha del comprobante</param>
        /// <param name="pCuit">CUIT del Emisor</param>
        /// <param name="pPtoVta">Punto de Venta</param>
        /// <param name="pTipoComp">Tipo de Comprobante</param>
        /// <param name="pNroCmp">Numero del Comprobante</param>
        /// <param name="pImporte">Importe Total del comprobante</param>
        /// <param name="pMondeda">Moneda del comprobante.</param>
        /// <param name="pCtz">Cotización de la moneda</param>
        /// <param name="pTipoDocRec">Tipo de documento del receptor</param>
        /// <param name="pNroDocRec">Numero de documento del receptor</param>
        /// <param name="pTipoCodAut">Tipo de codigo de autorización A=CAEA, E=CAE</param>
        /// <param name="pCodAut">CAE/CAEA</param>
        /// <param name="pPathLog">Path del archivo de Log.</param>
        /// <returns>Texto para el QR, con url y json codificado.</returns>
        public static string CrearJson(int pVersion, DateTime pFecha, long pCuit, int pPtoVta, int pTipoComp, int pNroCmp,
            decimal pImporte, string pMondeda, decimal pCtz, int pTipoDocRec, long pNroDocRec, string pTipoCodAut, long pCodAut, string pPathLog)
        {
            try
            {
                string urlAfip = "https://www.afip.gob.ar/fe/qr/?p=";
                QRjson json = new QRjson();
                json.ver = pVersion;
                json.fecha = pFecha.Year.ToString().PadLeft(4, '0') + "-" + pFecha.Month.ToString().PadLeft(2, '0') + "-" + pFecha.Day.ToString().PadLeft(2, '0');
                json.cuit = pCuit;
                json.ptoVta = pPtoVta;
                json.tipoCmp = pTipoComp;
                json.nroCmp = pNroCmp;
                json.importe = Math.Round(pImporte, 2);
                json.moneda = pMondeda;
                json.ctz = Math.Round(pCtz, 6);
                json.tipoDocRec = pTipoDocRec;
                json.nroDocRec = pNroDocRec;
                json.tipoCodAut = pTipoCodAut;
                json.codAut = pCodAut;
                //Seriealizamos y Convertimos
                string qrStr = JsonConvert.SerializeObject(json);
                string base64EncodedExternalAccount = Convert.ToBase64String(Encoding.UTF8.GetBytes(qrStr));
                //Sumamos la Url
                string retorno = urlAfip + base64EncodedExternalAccount;
                //Retornamos el string codificado.
                return retorno;
            }
            catch(Exception ex)
            {
                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pPathLog), "CrearJson Error: " + ex.Message);
                string retorno = "";
                //Retornamos el string codificado.
                return retorno;
            }
        }
    }
}
