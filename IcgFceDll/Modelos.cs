﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class Modelos
    {
        public class AfipComprobanteAsoc
        {
            public int Tipo { get; set; }
            public int PtoVta { get; set; }
            public int Nro { get; set; }
            public string Cuit { get; set; }
            public string CbteFch { get; set; }
        }

        public class Tiquet
        {
            public string NumSerie { get; set; }
            public int NumFactura { get; set; }
            public string N { get; set; }
            public string SerieFiscal1 { get; set; }
            public string SerieFiscal2 { get; set; }
            public int NumeroFiscal { get; set; }
        }

        public class DatosFactura
        {
            //Cabecera
            public int fo { get; set; }
            public string serie { get; set; }
            public int numero { get; set; }
            public string n { get; set; }
            public double totalbruto { get; set; }
            public double totalneto { get; set; }
            public int codcliente { get; set; }
            public int codvendedor { get; set; }
            public DateTime fecha { get; set; }
            public double entregado { get; set; }
            public double propina { get; set; }
            public string serieFiscal { get; set; }
            public string serieFiscal2 { get; set; }
            public int numeroFiscal { get; set; }
            public string nomVendedor { get; set; }
            public int sala { get; set; }
            public int mesa { get; set; }
            public double BaseImp { get; set; }
            public DateTime hora { get; set; }
            //Cliente
            public string clienteNombre { get; set; }
            public string clienteDireccion { get; set; }
            public string clienteDocumento { get; set; }
            public int clienteRegimenFacturacion { get; set; }
            public int clienteTipoDocumento { get; set; }
            //Totales
            public decimal totIVA { get; set; }
            public decimal totNoGravado { get; set; }
            public decimal totTributos { get; set; }
            //Pto de venta.
            public int ptoVta { get; set; }
            //
            public int codAfipComprobante { get; set; }
            /// <summary>
            /// Cargo/Descuento
            /// </summary>
            public double cargo { get; set; }
        }

        public class TiquetsTot
        {
            public decimal Iva { get; set; }
            public decimal BaseImponible { get; set; }
            public decimal TotIva { get; set; }
        }

        public class ComprobantesConCAEA
        {
            public string Serie { get; set; }
            public int Numero { get; set; }
            public string N { get; set; }
            public int Fo { get; set; }
            public string CAE { get; set; }
            public string Error_CAE { get; set; }
            public string Vto_CAE { get; set; }
            public string Estado_FE { get; set; }
            public string CAEA_Informado { get; set; }
            public int CodVendedor { get; set; }
        }

        public class FacturasVentasTot
        {
            public decimal Iva { get; set; }
            public decimal BaseImponible { get; set; }
            public decimal TotIva { get; set; }
        }

        public class DatosFacturaExpo
        {
            public string numSerie { get; set; }
            public int numFactura { get; set; }
            public string N { get; set; }
            public DateTime fecha { get; set; }
            public int codCliente { get; set; }
            public decimal totalBruto { get; set; }
            public decimal totalNeto { get; set; }
            public string CodAfip { get; set; }
            public string TipoDoc { get; set; }
            public string PtoVta { get; set; }
            public decimal porcentajeDtoComercial { get; set; }
            public decimal totDtoComercial { get; set; }
            public decimal porcentajeDtoPp { get; set; }
            public decimal totDtoPp { get; set; }
            public string codigoMoneda { get; set; }
            public decimal cotizacion { get; set; }
            public string incoterms { get; set; }
        }

        public class ComprobanteAsociado
        {
            public string numSerie { get; set; }
            public int numAlbaran { get; set; }
            public string n { get; set; }
        }

        public class ValidacionTiquetsSinInformar
        {
            public string numSerie { get; set; }
            public int numFactura { get; set; }
            public string n { get; set; }
            public int fo { get; set; }
            public DateTime fecha { get; set; }
        }

        public class FormaPago
        {
            public int codFormaPago { get; set; }
            public string descripcion { get; set; }
        }

        public class Venice
        {
            public class VeniceTransaccion
            {
                [JsonProperty("reservation")]
                public int reservation { get; set; }
                [JsonProperty("article")]
                public int article { get; set; }
                [JsonProperty("pdv")]
                public int pdv { get; set; }
                [JsonProperty("quantity")]
                public int quantity { get; set; }
                [JsonProperty("unitPrice")]
                public double unitPrice { get; set; }
                [JsonProperty("checkNumber")]
                public string checkNumber { get; set; }
            }

            public class HabitacionesDTO
            {
                [JsonProperty("status")]
                public int Status { get; set; }
                [JsonProperty("arrivalDate")]
                public string ArrivalDate { get; set; }
                [JsonProperty("departureDate")]
                public string departureDate { get; set; }
                [JsonProperty("locator")]
                public string locator { get; set; }
                [JsonProperty("agent")]
                public AgentDto agent { get; set; }
                [JsonProperty("company")]
                public CompanyDto company { get; set; }
                [JsonProperty("profile")]
                public ProfileDto profile { get; set; }
                [JsonProperty("reservationType")]
                public ReservationTypeDto reservationType { get; set; }
                [JsonProperty("id")]
                public int id { get; set; }
                [JsonProperty("room")]
                public RoomDto room { get; set; }
            }

            public class AgentDto
            {
                [JsonProperty("id")]
                public int id { get; set; }
                [JsonProperty("documentNumber")]
                public string documentNomber { get; set; }
                [JsonProperty("name")]
                public string name { get; set; }
                [JsonProperty("surName")]
                public string surName { get; set; }
            }

            public class CompanyDto
            {
                [JsonProperty("id")]
                public int id { get; set; }
                [JsonProperty("documentNumber")]
                public string documentNomber { get; set; }
                [JsonProperty("name")]
                public string name { get; set; }
                [JsonProperty("surName")]
                public string surName { get; set; }
            }

            public class ProfileDto
            {
                [JsonProperty("id")]
                public int id { get; set; }
                [JsonProperty("documentNumber")]
                public string documentNomber { get; set; }
                [JsonProperty("name")]
                public string name { get; set; }
                [JsonProperty("surName")]
                public string surName { get; set; }
            }

            public class ReservationTypeDto
            {
                [JsonProperty("id")]
                public int id { get; set; }
            }

            public class RoomDto
            {
                [JsonProperty("id")]
                public int id { get; set; }
                [JsonProperty("name")]
                public string name { get; set; }
            }

            public class GridDto
            {
                [JsonProperty("nombre")]
                public string Nombre { get; set; }
                [JsonProperty("apellido")]
                public string Apellido { get; set; }
                [JsonProperty("habitacion")]
                public string Habitacion { get; set; }
                [JsonProperty("id")]
                public int Reservation { get; set; }

            }
        }
    }
}
