﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class ShoppingOlmosService
    {
        public class Retail
        {
            public static void LanzarShoppingOlmos(string _serie, int _numero, string _n, string _pathOut,
            string _nroComprobante, int _nroClienteOlmos, int _nroPosOlmos, int _rubroOlmos,
            Int64 _cuit, string _procesoOlmos, bool _EnvioInfo, SqlConnection _conexion)
            {
                try
                {
                    //Recupero los datos de cabecera.
                    Cabecera _cabecera = GetCabecera(_serie, _numero, _n, _conexion);
                    //Recupero los datos de Pagos.
                    List<Pagos> _pagos = GetPagos(_serie, _numero, _n, _conexion);
                    //Recupero los dato de impresion del comprobante.
                    FacturasVentaSerieResol _fsr = GetTiquet(_cabecera.numserie, Convert.ToInt32(_cabecera.numfac), _cabecera.n, _conexion);
                    int _quantityItems = GetItemQuantity(_serie, _numero, _n, _conexion);
                    //Cabecera
                    GenerarDatosCabecera(_fsr, _cabecera, _nroClienteOlmos, _cuit, _nroPosOlmos, _procesoOlmos, _EnvioInfo);
                    //Detalle
                    GenerarDatosDetalle(_fsr, _cabecera, _nroClienteOlmos, _rubroOlmos, _quantityItems, _procesoOlmos, _EnvioInfo);
                    //Pagos
                    GenerarDatosPagos(_fsr, _pagos, _nroClienteOlmos, _rubroOlmos, _procesoOlmos, _EnvioInfo);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            private static void GenerarDatosCabecera(FacturasVentaSerieResol _fvsr, Cabecera _cab, int _nroCliente, Int64 _cuitEmisor, int _posOlmos, string _ProcesoOlmos, bool _envioInfo)
            {
                try
                {
                    string _fecha = _cab.fecha.Year.ToString().PadLeft(4, '0') + _cab.fecha.Month.ToString().PadLeft(2, '0') + _cab.fecha.Day.ToString().PadLeft(2, '0');
                    string _hora = _cab.hora.Hour.ToString().PadLeft(2, '0') + _cab.hora.Minute.ToString().PadLeft(2, '0');

                    string _tipoDocCliente = "";
                    if (_cab.regfacturacioncliente == "1")
                        _tipoDocCliente = "CUIT";
                    else
                        _tipoDocCliente = "DNI";

                    string _datos = "record:C|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                        "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _fecha + "|" + _hora + "|CUIT" +
                        "|" + _cuitEmisor.ToString() + "|" + _tipoDocCliente + "|" + _cab.nrodoccliente + "|N|" + _posOlmos.ToString() + "|" + _fecha;

                    if (_envioInfo)
                    {
                        EnvioInfo(_ProcesoOlmos, _datos);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(_datos))
                        {
                            //Mando la informacion de pago.
                            if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.txt", true))
                            {
                                sw.WriteLine(_datos);
                                sw.Flush();
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("GenerarDatosCabecera - " + ex.Message); }
            }

            private static void GenerarDatosDetalle(FacturasVentaSerieResol _fvsr, Cabecera _cab, int _nroCliente, int _rubro, int _cantidad, string _ProcesoOlmos, bool _envioInfo)
            {
                try
                {
                    double _total = 0;
                    if (_cab.totdtopp < 0)
                        _total = _cab.totalbruto + (_cab.totdtopp * -1);
                    else
                    {
                        _total = _cab.totalbruto;
                    }

                    string _datos = "record:D|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                        "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|21.00|" +
                        Math.Round(Math.Abs(_total), 2).ToString().Replace(',', '.') + "|" + Math.Round(Math.Abs(_cab.totalimpuestos), 2).ToString().Replace(',', '.') +
                        "|0.00|" + _rubro.ToString() + "|" + Math.Abs(_cantidad).ToString();

                    if (_envioInfo)
                    {
                        EnvioInfo(_ProcesoOlmos, _datos);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(_datos))
                        {
                            //Mando la informacion de pago.
                            if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.txt", true))
                            {
                                sw.WriteLine(_datos);
                                sw.Flush();
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("GenerarDatosDetalle - " + ex.Message); }
            }

            private static void GenerarDatosPagos(FacturasVentaSerieResol _fvsr, List<Pagos> _pagos, int _nroCliente, int _rubro, string _procesoOlmos, bool _envioInfo)
            {
                try
                {
                    foreach (Pagos pg in _pagos)
                    {
                        int _formaPago = 0;
                        if (String.IsNullOrEmpty(pg.olmosTipoPago))
                            _formaPago = 0;
                        else
                            _formaPago = Convert.ToInt32(pg.olmosTipoPago);
                        int _codTarjeta = 0;
                        if (String.IsNullOrEmpty(pg.olmosTarjeta))
                            _codTarjeta = 0;
                        else
                            _codTarjeta = Convert.ToInt32(pg.olmosTarjeta);

                        //Cuotas
                        int _cuotas = 0;
                        if (!String.IsNullOrEmpty(pg.cuotas))
                        {
                            string[] _lst = pg.cuotas.Split(' ');
                            _cuotas = Convert.ToInt16(_lst[0]);
                        }

                        string _datos = "";
                        if (_formaPago != 0)
                        {
                            if (_codTarjeta != 0)
                                _datos = "record:P|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                                    "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _formaPago.ToString() +
                                    "|" + _codTarjeta.ToString() + "|" + _cuotas.ToString() + "|" + Math.Round(Math.Abs(pg.monto), 2).ToString().Replace(',', '.');
                            else
                                _datos = "record:P|" + _nroCliente.ToString() + "|" + Convert.ToInt32(_fvsr.serieFiscal2.Substring(0, 3)).ToString() +
                                    "|" + Convert.ToInt32(_fvsr.serieFiscal1).ToString() + "|" + _fvsr.numeroFiscal.ToString() + "|" + _formaPago.ToString() +
                                    "|" + "||" + Math.Round(Math.Abs(pg.monto), 2).ToString().Replace(',', '.');
                        }

                        if (_envioInfo)
                        {
                            EnvioInfo(_procesoOlmos, _datos);
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(_datos))
                            {
                                //Mando la informacion de pago.
                                if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                                {
                                    Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                                }
                                using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.Txt", true))
                                {
                                    sw.WriteLine(_datos);
                                    sw.Flush();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("GenerarDatosPagos - " + ex.Message); }
            }

            private static void EnvioInfo(string _proceso, string _record)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(_proceso, _record);
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false; //No utiliza RunDLL32 para lanzarlo
                                                   //Redirige las salidas y los errores
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                Process proc = Process.Start(startInfo); //Ejecuta el proceso
                proc.WaitForExit(); // Espera a que termine el proceso
                string error = proc.StandardError.ReadToEnd();
                if (error != null && error != "") //Error
                    throw new Exception("Se ha producido un error al ejecutar el proceso '" + _proceso + "'\n" + "Detalles:\n" + "Error: " + error);
                //else //Éxito
                //    return proc.StandardOutput.ReadToEnd(); //Devuelve el resultado 
            }

            /// <summary>
            /// Metodo que recuper la información de cabecera de un comprobante.
            /// </summary>
            /// <param name="_serie">Serie del ticket.</param>
            /// <param name="_numero">Numero del Ticket.</param>
            /// <param name="_N">N del ticket.</param>
            /// <param name="_con">Conexión SQL.</param>
            /// <returns></returns>
            private static Cabecera GetCabecera(string _serie, int _numero, string _N, SqlConnection _con)
            {
                string _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC, ALBVENTACAB.CODCLIENTE, ALBVENTACAB.CODVENDEDOR, " +
                    "ALBVENTACAB.DTOCOMERCIAL, ALBVENTACAB.TOTDTOCOMERCIAL, ALBVENTACAB.TOTALBRUTO, ALBVENTACAB.TOTALIMPUESTOS, ALBVENTACAB.TOTALNETO, ALBVENTACAB.TIPODOC, ALBVENTACAB.DTOPP, ALBVENTACAB.TOTDTOPP, " +
                    "CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, " +
                    "TIPOSDOC.DESCRIPCION, FACTURASVENTA.ENTREGADO, FACTURASVENTA.FECHA, ALBVENTACAB.Z, CLIENTES.TELEFONO1, VENDEDORES.NOMVENDEDOR, ALBVENTACAB.HORA  " +
                    "FROM FACTURASVENTA INNER JOIN  ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.N " +
                    "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN VENDEDORES ON FACTURASVENTA.CODVENDEDOR = VENDEDORES.CODVENDEDOR " +
                    "WHERE FACTURASVENTA.NUMSERIE = @Numserie AND facturasventa.NUMFACTURA = @NumFact AND FACTURASVENTA.N = @N";

                Cabecera _cabecera = new Cabecera();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _serie);
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@NumFact", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cabecera.ciudadcliente = _reader["POBLACION"].ToString();
                                _cabecera.codcliente = Convert.ToInt32(_reader["CODCLIENTE"]);
                                _cabecera.codpostalcliente = _reader["CODPOSTAL"].ToString();
                                _cabecera.codvendedor = Convert.ToInt32(_reader["CODVENDEDOR"]);
                                _cabecera.descripcionticket = _reader["DESCRIPCION"].ToString();
                                _cabecera.direccioncliente = _reader["DIRECCION1"].ToString();
                                _cabecera.dtocomercial = Convert.ToDouble(_reader["DTOCOMERCIAL"]);
                                _cabecera.n = _reader["N"].ToString();
                                _cabecera.nombrecliente = _reader["NOMBRECLIENTE"].ToString();
                                _cabecera.nrodoccliente = _reader["NIF20"].ToString();
                                _cabecera.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                                _cabecera.numfac = Convert.ToInt32(_reader["NUMFAC"]);
                                _cabecera.numserie = _reader["NUMSERIE"].ToString();
                                _cabecera.provinciacliente = _reader["PROVINCIA"].ToString();
                                _cabecera.regfacturacioncliente = _reader["REGIMFACT"].ToString();
                                _cabecera.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                                _cabecera.totalbruto = Convert.ToDouble(_reader["TOTALBRUTO"]);
                                _cabecera.totalimpuestos = Convert.ToDouble(_reader["TOTALIMPUESTOS"]);
                                _cabecera.totalneto = Convert.ToDouble(_reader["TOTALNETO"]);
                                _cabecera.totdtocomercial = Convert.ToDouble(_reader["TOTDTOCOMERCIAL"]);
                                _cabecera.dtopp = Convert.ToDouble(_reader["DTOPP"]);
                                _cabecera.totdtopp = Convert.ToDouble(_reader["TOTDTOPP"]);
                                _cabecera.entregado = Convert.ToDouble(_reader["ENTREGADO"]);
                                _cabecera.fecha = Convert.ToDateTime(_reader["FECHA"]);
                                _cabecera.z = Convert.ToInt32(_reader["Z"]);
                                _cabecera.telefono = _reader["TELEFONO1"].ToString();
                                _cabecera.nomvendedor = _reader["NOMVENDEDOR"].ToString();
                                _cabecera.hora = Convert.ToDateTime(_reader["HORA"]);
                            }
                        }
                    }
                }

                return _cabecera;
            }
            /// <summary>
            /// Metodo que recupera la información de pago de un ticket.
            /// </summary>
            /// <param name="_serie">Serie del ticket.</param>
            /// <param name="_numero">Numero del ticket.</param>
            /// <param name="_N">N del Ticket.</param>
            /// <param name="_con">Conexion SQL.</param>
            /// <returns></returns>
            private static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
            {

                string _sql = @"SELECT (TESORERIA.IMPORTE * TESORERIA.FACTORMONEDA) as IMPORTE, IsNull(FORMASPAGO.DESCRIPCION, 'Anticipo') as TARJETA, 
                IsNull(TIPOSPAGO.DESCRIPCION, 'Anticipo') as TIPOPAGO, ISNULL(tipospago.MARCASTARJETA,'') as FormaPago, 
                ISNULL(FORMASPAGO.TEXTOIMP,'') as CodigoTarjeta, TESORERIA.COMENTARIOVISIBLE,
                FORMASPAGO.MARCASTARJETA as OlmosTipoPago, FORMASPAGO.CODFORMAPAGOSOBREPAGO as OlmosTarjeta
                FROM TESORERIA 
                LEFT JOIN FORMASPAGO ON TESORERIA.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                LEFT JOIN VENCIMFPAGO ON FORMASPAGO.CODFORMAPAGO = VENCIMFPAGO.CODFORMAPAGO 
                LEFT JOIN TIPOSPAGO ON VENCIMFPAGO.CODTIPOPAGO = TIPOSPAGO.CODTIPOPAGO 
                WHERE TESORERIA.serie = @NumSerie AND TESORERIA.NUMERO = @Numero AND TESORERIA.N = @N";

                List<Pagos> _pagos = new List<Pagos>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@NumSerie", _serie);
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@Numero", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                Pagos _cls = new Pagos();
                                _cls.descripcion = _reader["TARJETA"].ToString();
                                _cls.tipopago = _reader["TIPOPAGO"].ToString();
                                _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                                _cls.cuotas = _reader["COMENTARIOVISIBLE"].ToString();
                                _cls.cupon = "";
                                _cls.formapago = _reader["FormaPago"].ToString();
                                _cls.codigotarjeta = _reader["CodigoTarjeta"].ToString();
                                _cls.olmosTipoPago = _reader["OlmosTipoPago"].ToString();
                                _cls.olmosTarjeta = _reader["OlmosTarjeta"].ToString();

                                _pagos.Add(_cls);
                            }
                        }
                    }
                }
                return _pagos;
            }

            /// <summary>
            /// Metodo que devuelve la cantidad de articulos vendidos
            /// </summary>
            /// <param name="_serie">Serie de la Venta.</param>
            /// <param name="_numero">Numero de la Venta.</param>
            /// <param name="_N">N de la Venta.</param>
            /// <param name="_con">Conexión SQL.</param>
            /// <returns>Integer</returns>
            private static int GetItemQuantity(string _serie, int _numero, string _N, SqlConnection _con)
            {

                string _sql = @"SELECT SUM(ALBVENTALIN.UNIDADESTOTAL) AS CANTIDAD
                FROM FACTURASVENTA INNER JOIN ALBVENTACAB ON FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.NFAC
                INNER JOIN ALBVENTALIN ON ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE AND ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN AND ALBVENTACAB.N = ALBVENTALIN.N
                WHERE FACTURASVENTA.NUMSERIE = @serie AND FACTURASVENTA.NUMFACTURA = @numero AND FACTURASVENTA.N = @N";

                int _quantity = 0;
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@n", _N);
                    _cmd.Parameters.AddWithValue("@numero", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _quantity = Convert.ToInt32(_reader["CANTIDAD"]);                                
                            }
                        }
                    }
                }
                return _quantity;
            }
            private static FacturasVentaSerieResol GetTiquet(string _numSerie, int _numFactura, string _n, SqlConnection _con)
            {
                string _sql = "SELECT NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL FROM FACTURASVENTASERIESRESOL " +
                        "WHERE NUMSERIE = @numSerie AND NUMFACTURA = @numFactura AND N = @n";

                FacturasVentaSerieResol _cls = new FacturasVentaSerieResol();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.n = _reader["N"].ToString();
                                _cls.numeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                                _cls.numFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                                _cls.numSerie = _reader["NUMSERIE"].ToString();
                                _cls.serieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                                _cls.serieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                            }
                        }
                    }
                }
                return _cls;
            }
            /// <summary>
            /// Clase de cabecera.
            /// </summary>
            private class Cabecera
            {
                public string numserie { get; set; }
                public int numalbaran { get; set; }
                public string n { get; set; }
                public int numfac { get; set; }
                public int codcliente { get; set; }
                public int codvendedor { get; set; }
                public double dtocomercial { get; set; }
                public double totdtocomercial { get; set; }
                public double totalbruto { get; set; }
                public double totalimpuestos { get; set; }
                public double totalneto { get; set; }
                public int tipodoc { get; set; }
                public string nombrecliente { get; set; }
                public string direccioncliente { get; set; }
                public string nrodoccliente { get; set; }
                public string codpostalcliente { get; set; }
                public string ciudadcliente { get; set; }
                public string provinciacliente { get; set; }
                public string regfacturacioncliente { get; set; }
                public string descripcionticket { get; set; }
                public double dtopp { get; set; }
                public double totdtopp { get; set; }
                public double entregado { get; set; }
                public DateTime fecha { get; set; }
                public int z { get; set; }
                public string telefono { get; set; }
                public string nomvendedor { get; set; }
                public DateTime hora { get; set; }
            }
            /// <summary>
            /// Clase del pagos.
            /// </summary>
            private class Pagos
            {
                public string descripcion { get; set; }
                public string tipopago { get; set; }
                public decimal monto { get; set; }
                public string cuotas { get; set; }
                public string cupon { get; set; }
                public string formapago { get; set; }
                public string codigotarjeta { get; set; }
                public string olmosTipoPago { get; set; }
                public string olmosTarjeta { get; set; }
            }
            /// <summary>
            /// Clase de facturasventaserieresol
            /// </summary>
            private class FacturasVentaSerieResol
            {
                public string numSerie { get; set; }
                public int numFactura { get; set; }
                public string n { get; set; }
                public string serieFiscal1 { get; set; }
                public string serieFiscal2 { get; set; }
                public int numeroFiscal { get; set; }
            }
        }
    }
}
