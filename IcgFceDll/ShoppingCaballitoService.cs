﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgFceDll
{
    public class ShoppingCaballitoService
    {
        public class Retail
        {
            /// <summary>
            /// Metodo que genera la información a enviar al Shopping Caballito
            /// </summary>
            /// <param name="_serie">Serie del ticket.</param>
            /// <param name="_numero">Numero del ticket.</param>
            /// <param name="_n">N del Ticket.</param>
            /// <param name="_connection">Conexión SQL a la Base</param>
            /// <param name="_path">Path donde se dejará la información</param>
            /// <param name="_nroComprobante">Numero del comprobante a informar, formateado en 8 digitos..</param>
            /// <param name="_ptoVta">Punto de venta. Formateado en 4 digitos</param>
            public static void GenerarShoppingCaballito(string _serie, string _numero, string _n, SqlConnection _connection, 
                string _path, string _nroComprobante, string _ptoVta)
            {
                try
                {
                    Cabecera _cab = GetCabecera(_serie, Convert.ToInt32(_numero), _n, _connection);
                    List<Pagos> _pagos = GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);
                    LanzarShoppingCaballito(_cab, _pagos, _path, "V", _nroComprobante, _ptoVta, _connection);
                }
                catch (Exception ex1)
                {
                    if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\LOG\icgCaballito.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex1.Message);
                        sw.Flush();
                    }
                }
            }

            /// <summary>
            /// Lanza el proceso para informar las ventas al host de IRSA
            /// </summary>
            /// <param name="_cabecera">Cabecera de la venta.</param>
            /// <param name="_pagos">Lista de Pagos</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante a informas N op C</param>
            /// <param name="_nroComprobante">Numero del comprobante PtoVta + Numero</param>
            /// <param name="_pathOut">Path del directorio de salida.</param>
            /// <param name="_PtoVta">Punto de venta.</param>
            /// <param name="_conexion">Conexion SQL abierta.</param>
            private static void LanzarShoppingCaballito(Cabecera _cabecera, List<Pagos> _pagos, string _pathOut,
                string _tipoComprobante, string _nroComprobante, string _PtoVta, SqlConnection _conexion)
            {
                try
                {
                    string _letra = "";
                    //string _ptoVta = "";
                    //string _nroFiscal = "";
                    string _clMov = "N";
                    if (_cabecera.regfacturacioncliente != "N")
                    {
                        if (_cabecera.regfacturacioncliente != "1")
                            _letra = "B";
                        else
                            _letra = "A";

                        //string[] _nro = _nroComprobante.Split('-');
                        //if (_nro.Length == 2)
                        //{
                        //    _ptoVta = _nro[0];
                        //    _nroFiscal = _nro[1];
                        //}

                        ImprimoPagos(_cabecera, _pagos, _pathOut, _letra,
                            _tipoComprobante, _PtoVta, _nroComprobante, _clMov);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            /// <summary>
            /// Metodo que Genera la información del Pago.
            /// </summary>
            /// <param name="_cab">Clase de la Cabecera</param>
            /// <param name="_pagos">Clase del PAgo</param>
            /// <param name="_pathOut">Path de salida de la información</param>
            /// <param name="_letra">Letra del comprobante</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante.</param>
            /// <param name="_ptovta">Punto de Venta.</param>
            /// <param name="_nrocomprobante">Numero de Comprobante.</param>
            /// <param name="_tipoMovimiento">Tipo de movimiento.</param>
            /// <returns></returns>
            private static bool ImprimoPagos(Cabecera _cab, List<Pagos> _pagos, string _pathOut,
                string _letra, string _tipoComprobante, string _ptovta, string _nrocomprobante,
                string _tipoMovimiento)
            {
                bool _rta = false;

                List<Pagos> _new = new List<Pagos>();

                foreach (Pagos x in _pagos)
                {
                    if (x.formapago.ToUpper() != "E")
                    {
                        if (x.descripcion.ToUpper() != "ANTICIPO")
                        {
                            Pagos _p = new Pagos()
                            {
                                descripcion = x.descripcion,
                                tipopago = x.tipopago,
                                monto = x.monto,
                                cuotas = x.cuotas,
                                cupon = x.cupon,
                                formapago = x.formapago,
                                codigotarjeta = x.codigotarjeta
                            };

                            _new.Add(_p);
                        }
                        else
                        {
                            x.formapago = "E";
                        }
                    }
                }
                //Calculamos el total del efectivo.
                decimal _totEfectivo = _pagos.Where(x => x.formapago.ToUpper() == "E").Sum(x => x.monto);
                //vemos si tenemos efectivo.
                if (Math.Abs(_totEfectivo) > 0)
                {
                    Pagos _p = new Pagos()
                    {
                        descripcion = "EFECTIVO",
                        tipopago = "EFECTIVO",
                        monto = Math.Round(Math.Abs(_totEfectivo), 2),
                        cuotas = "",
                        cupon = "",
                        formapago = "E",
                        codigotarjeta = ""
                    };
                    _new.Add(_p);
                }

                try
                {
                    foreach (Pagos pg in _new)
                    {
                        string _tipopag = pg.tipopago.Length > 5 ? pg.tipopago.Substring(0, 5) : pg.tipopago.PadLeft(5, ' ');
                        string _formapago = pg.formapago == "E" ? "01" : "03";
                        string _monto = Math.Round(Math.Abs(pg.monto), 2).ToString("N2").Replace(",", "").Replace(",", ".").PadLeft(7, '0');
                        if (Math.Abs(pg.monto) > 9999)
                        {
                            decimal _importe = Math.Round(Math.Abs(pg.monto), 2);
                            //
                            while (_importe > 9999)
                            {
                                _importe = _importe - 9999;
                                ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, "9999.00", _formapago, _tipopag, _pathOut);
                            }
                            if (_importe > 0)
                            {
                                string _importe2 = Math.Round(Math.Abs(_importe), 2).ToString("N2").Replace(",", "").Replace(",", ".").PadLeft(7, '0');
                                ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, _importe2, _formapago, _tipopag, _pathOut);
                            }
                        }
                        else
                        {
                            ImprimoLinea(_tipoComprobante, _letra, _ptovta, _nrocomprobante, _cab.fecha, _cab.hora, _monto, _formapago, _tipopag, _pathOut);
                        }

                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }
            /// <summary>
            /// Metodo que Genera la información de la linea
            /// </summary>
            /// <param name="_tipocomprobante">Tipo de comprobante.</param>
            /// <param name="_letra">Letra del comprobante.</param>
            /// <param name="_ptoVta">Punto de Venta.</param>
            /// <param name="_nroComprobante">Numero de comprobante.</param>
            /// <param name="_fecha">Fecha del comprobante.</param>
            /// <param name="_hora">Hora del comprobante.</param>
            /// <param name="_monto">Monto del comprobante.</param>
            /// <param name="_formaPago">Forma de pago.</param>
            /// <param name="_tipoPago">Tipo de pago.</param>
            /// <param name="_path">Path de salida de la información.</param>
            private static void ImprimoLinea(string _tipocomprobante, string _letra, string _ptoVta, string _nroComprobante,
                DateTime _fecha, DateTime _hora, string _monto, string _formaPago, string _tipoPago, string _path)
            {
                string _line = "";
                //Comun a todos
                _line = _tipocomprobante;
                _line = _line + _letra;
                _line = _line + _ptoVta.PadLeft(4, '0');
                _line = _line + _nroComprobante.PadLeft(8, '0');
                _line = _line + _fecha.Day.ToString().PadLeft(2, '0') + "/" + _fecha.Month.ToString().PadLeft(2, '0') + "/" + _fecha.Year.ToString().Substring(2, 2).PadLeft(2, '0');
                _line = _line + _hora.Hour.ToString().PadLeft(2, '0') + ":" + _hora.Minute.ToString().PadLeft(2, '0') + ":" + _hora.Second.ToString().PadLeft(2, '0');
                _line = _line + "1";
                _line = _line + _monto.PadLeft(7, '0');  //
                                                         //Por tipo de pago.
                _line = _line + _formaPago;
                _line = _line + _tipoPago.PadLeft(5, ' ');

                using (StreamWriter sw = new StreamWriter(_path, true))
                {
                    sw.WriteLine(_line);
                    sw.Flush();
                }
            }
            /// <summary>
            /// Metodo que recuper la información de cabecera de un comprobante.
            /// </summary>
            /// <param name="_serie">Serie del ticket.</param>
            /// <param name="_numero">Numero del Ticket.</param>
            /// <param name="_N">N del ticket.</param>
            /// <param name="_con">Conexión SQL.</param>
            /// <returns></returns>
            private static Cabecera GetCabecera(string _serie, int _numero, string _N, SqlConnection _con)
            {
                string _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC, ALBVENTACAB.CODCLIENTE, ALBVENTACAB.CODVENDEDOR, " +
                    "ALBVENTACAB.DTOCOMERCIAL, ALBVENTACAB.TOTDTOCOMERCIAL, ALBVENTACAB.TOTALBRUTO, ALBVENTACAB.TOTALIMPUESTOS, ALBVENTACAB.TOTALNETO, ALBVENTACAB.TIPODOC, ALBVENTACAB.DTOPP, ALBVENTACAB.TOTDTOPP, " +
                    "CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, " +
                    "TIPOSDOC.DESCRIPCION, FACTURASVENTA.ENTREGADO, FACTURASVENTA.FECHA, ALBVENTACAB.Z, CLIENTES.TELEFONO1, VENDEDORES.NOMVENDEDOR, ALBVENTACAB.HORA  " +
                    "FROM FACTURASVENTA INNER JOIN  ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.N " +
                    "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN VENDEDORES ON FACTURASVENTA.CODVENDEDOR = VENDEDORES.CODVENDEDOR " +
                    "WHERE FACTURASVENTA.NUMSERIE = @Numserie AND facturasventa.NUMFACTURA = @NumFact AND FACTURASVENTA.N = @N";

                Cabecera _cabecera = new Cabecera();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _serie);
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@NumFact", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cabecera.ciudadcliente = _reader["POBLACION"].ToString();
                                _cabecera.codcliente = Convert.ToInt32(_reader["CODCLIENTE"]);
                                _cabecera.codpostalcliente = _reader["CODPOSTAL"].ToString();
                                _cabecera.codvendedor = Convert.ToInt32(_reader["CODVENDEDOR"]);
                                _cabecera.descripcionticket = _reader["DESCRIPCION"].ToString();
                                _cabecera.direccioncliente = _reader["DIRECCION1"].ToString();
                                _cabecera.dtocomercial = Convert.ToDouble(_reader["DTOCOMERCIAL"]);
                                _cabecera.n = _reader["N"].ToString();
                                _cabecera.nombrecliente = _reader["NOMBRECLIENTE"].ToString();
                                _cabecera.nrodoccliente = _reader["NIF20"].ToString();
                                _cabecera.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                                _cabecera.numfac = Convert.ToInt32(_reader["NUMFAC"]);
                                _cabecera.numserie = _reader["NUMSERIE"].ToString();
                                _cabecera.provinciacliente = _reader["PROVINCIA"].ToString();
                                _cabecera.regfacturacioncliente = _reader["REGIMFACT"].ToString();
                                _cabecera.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                                _cabecera.totalbruto = Convert.ToDouble(_reader["TOTALBRUTO"]);
                                _cabecera.totalimpuestos = Convert.ToDouble(_reader["TOTALIMPUESTOS"]);
                                _cabecera.totalneto = Convert.ToDouble(_reader["TOTALNETO"]);
                                _cabecera.totdtocomercial = Convert.ToDouble(_reader["TOTDTOCOMERCIAL"]);
                                _cabecera.dtopp = Convert.ToDouble(_reader["DTOPP"]);
                                _cabecera.totdtopp = Convert.ToDouble(_reader["TOTDTOPP"]);
                                _cabecera.entregado = Convert.ToDouble(_reader["ENTREGADO"]);
                                _cabecera.fecha = Convert.ToDateTime(_reader["FECHA"]);
                                _cabecera.z = Convert.ToInt32(_reader["Z"]);
                                _cabecera.telefono = _reader["TELEFONO1"].ToString();
                                _cabecera.nomvendedor = _reader["NOMVENDEDOR"].ToString();
                                _cabecera.hora = Convert.ToDateTime(_reader["HORA"]);
                            }
                        }
                    }
                }

                return _cabecera;
            }
            /// <summary>
            /// Metodo que recupera la información de pago de un ticket.
            /// </summary>
            /// <param name="_serie">Serie del ticket.</param>
            /// <param name="_numero">Numero del ticket.</param>
            /// <param name="_N">N del Ticket.</param>
            /// <param name="_con">Conexion SQL.</param>
            /// <returns></returns>
            private static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
            {
                string _sql = "SELECT (TESORERIA.IMPORTE * TESORERIA.FACTORMONEDA) as IMPORTE, FORMASPAGO.DESCRIPCION as TARJETA, TIPOSPAGO.DESCRIPCION as TIPOPAGO, " +
                     "ISNULL(tipospago.MARCASTARJETA,'') as FormaPago, ISNULL(FORMASPAGO.TEXTOIMP, '') as CodigoTarjeta, FORMASPAGO.Abrircajon " +
                     "FROM TESORERIA INNER JOIN FORMASPAGO ON TESORERIA.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO " +
                     "INNER JOIN VENCIMFPAGO ON FORMASPAGO.CODFORMAPAGO = VENCIMFPAGO.CODFORMAPAGO " +
                     "INNER JOIN TIPOSPAGO ON VENCIMFPAGO.CODTIPOPAGO = TIPOSPAGO.CODTIPOPAGO " +
                     "INNER JOIN FACTURASVENTA on TESORERIA.SERIE = FACTURASVENTA.NUMSERIE AND TESORERIA.NUMERO = FACTURASVENTA.NUMFACTURA and TESORERIA.N = FACTURASVENTA.N " +
                     "WHERE TESORERIA.serie = @NumSerie AND TESORERIA.NUMERO = @Numero AND TESORERIA.N = @N";

                List<Pagos> _pagos = new List<Pagos>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@NumSerie", _serie);
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@Numero", _numero);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                Pagos _cls = new Pagos();
                                _cls.descripcion = _reader["TARJETA"].ToString();
                                _cls.tipopago = _reader["TIPOPAGO"].ToString();
                                _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                                _cls.cuotas = "";
                                _cls.cupon = "";
                                _cls.formapago = _reader["FormaPago"].ToString();
                                _cls.codigotarjeta = _reader["CodigoTarjeta"].ToString();
                                _cls.AbrirCajon = Convert.ToBoolean(_reader["Abrircajon"]);

                                _pagos.Add(_cls);
                            }
                        }
                    }
                }

                return _pagos;
            }
            /// <summary>
            /// Clase de cabecera.
            /// </summary>
            private class Cabecera
            {
                public string numserie { get; set; }
                public int numalbaran { get; set; }
                public string n { get; set; }
                public int numfac { get; set; }
                public int codcliente { get; set; }
                public int codvendedor { get; set; }
                public double dtocomercial { get; set; }
                public double totdtocomercial { get; set; }
                public double totalbruto { get; set; }
                public double totalimpuestos { get; set; }
                public double totalneto { get; set; }
                public int tipodoc { get; set; }
                public string nombrecliente { get; set; }
                public string direccioncliente { get; set; }
                public string nrodoccliente { get; set; }
                public string codpostalcliente { get; set; }
                public string ciudadcliente { get; set; }
                public string provinciacliente { get; set; }
                public string regfacturacioncliente { get; set; }
                public string descripcionticket { get; set; }
                public double dtopp { get; set; }
                public double totdtopp { get; set; }
                public double entregado { get; set; }
                public DateTime fecha { get; set; }
                public int z { get; set; }
                public string telefono { get; set; }
                public string nomvendedor { get; set; }
                public DateTime hora { get; set; }
            }
            /// <summary>
            /// Clase del pagos.
            /// </summary>
            private class Pagos
            {
                public string descripcion { get; set; }
                public string tipopago { get; set; }
                public decimal monto { get; set; }
                public string cuotas { get; set; }
                public string cupon { get; set; }
                public string formapago { get; set; }
                public string codigotarjeta { get; set; }
                public bool AbrirCajon { get; set; }
            }
        }
    }
}
