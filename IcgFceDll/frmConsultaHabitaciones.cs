﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using IcgFceDll;

namespace IcgFceDll
{
    public partial class frmConsultaHabitaciones : Form
    {
        string _apiUrl;
        string _apiKey;

        public int _reservation = 0;
        public frmConsultaHabitaciones(string _Key, string _Url)
        {
            InitializeComponent();
            _apiKey = _Key;
            _apiUrl = _Url;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                //Recupero las habitaciones.
                RestService.Venice venice = new RestService.Venice();
                List <Modelos.Venice.HabitacionesDTO> _lst = venice.GetHabitaciones(_apiKey, _apiUrl);
                List<Modelos.Venice.GridDto> _grid = new List<Modelos.Venice.GridDto>();
                foreach (Modelos.Venice.HabitacionesDTO hab in _lst)
                {
                    Modelos.Venice.GridDto _cls = new Modelos.Venice.GridDto();
                    _cls.Apellido = hab.profile.surName;
                    _cls.Nombre = hab.profile.name;
                    _cls.Habitacion = hab.room.name;
                    _cls.Reservation = hab.id;
                    _grid.Add(_cls);
                }
                var source = new BindingSource();
                source.DataSource = _grid;
                gridHabitacion.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //Recupero las habitaciones.
                RestService.Venice venice = new RestService.Venice();
                List<Modelos.Venice.HabitacionesDTO> _lst = venice.GetHabitaciones(_apiKey, _apiUrl);
                List<Modelos.Venice.GridDto> _grid = new List<Modelos.Venice.GridDto>();
                foreach (Modelos.Venice.HabitacionesDTO hab in _lst)
                {
                    Modelos.Venice.GridDto _cls = new Modelos.Venice.GridDto();
                    _cls.Apellido = hab.profile.surName;
                    _cls.Nombre = hab.profile.name;
                    _cls.Habitacion = hab.room.name;
                    _cls.Reservation = hab.id;
                    _grid.Add(_cls);
                }
                var source = new BindingSource();
                source.DataSource = _grid;
                gridHabitacion.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (gridHabitacion.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in gridHabitacion.SelectedRows)
                {
                    _reservation = Convert.ToInt32(dr.Cells["Reservation"].Value);
                }
                this.Close();
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "Debe seleccionar una habitación.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
