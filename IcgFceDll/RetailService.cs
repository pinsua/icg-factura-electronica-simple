﻿using AfipDll;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net;
using static IcgFceDll.DataAccess;
using System.Data;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.Win32;

namespace IcgFceDll
{
    public class RetailService
    {
        
        #region SQL
        public static List<Modelos.FacturasVentasTot> GetTotalesTributos(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion, string _Neto)
        {
            //string strSql = "SELECT IVA, IMPORTE, 0 as TOTIVA FROM FACTURASVENTADTOS WHERE CODDTO = @CodDto AND NUMSERIE = @Serie AND NUMERO = @Numero AND N = @N";
            string strSql = @"SELECT FACTURASVENTADTOS.IVA, FACTURASVENTADTOS.IMPORTE, 0 as TOTIVA, FACTURASVENTADTOS.DTOCARGO, CARGOSDTOS.NOMBRE, FACTURASVENTADTOS.BASE
                    FROM FACTURASVENTADTOS inner join CARGOSDTOS on FACTURASVENTADTOS.CODDTO = CARGOSDTOS.CODIGO
                    WHERE CARGOSDTOS.CODIGOFISCAL = @CodDto AND
                    FACTURASVENTADTOS.NUMSERIE = @Serie AND FACTURASVENTADTOS.NUMERO = @Numero AND FACTURASVENTADTOS.N = @N";
            //Instanciamos la conexion.
            SqlCommand _Command = new SqlCommand(strSql);
            _Command.Connection = _Connexion;
            _Command.Parameters.AddWithValue("@CodDto", _CodDto);
            _Command.Parameters.AddWithValue("@Serie", _Serie);
            _Command.Parameters.AddWithValue("@Numero", _Numero);
            _Command.Parameters.AddWithValue("@N", _N);

            List<Modelos.FacturasVentasTot> _Iva = new List<Modelos.FacturasVentasTot>();

            decimal _neto = Convert.ToDecimal(_Neto);
            try
            {
                using (SqlDataReader reader = _Command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Modelos.FacturasVentasTot fct = new Modelos.FacturasVentasTot();
                            fct.BaseImponible = Convert.ToDecimal(reader["BASE"]);
                            //fct.BaseImponible = _neto;
                            fct.Iva = _neto / Convert.ToDecimal(reader["IMPORTE"]);
                            fct.TotIva = Convert.ToDecimal(reader["IMPORTE"]);
                            _Iva.Add(fct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _Iva;
        }

        public static List<Modelos.FacturasVentasTot> GetTotalesIva(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion)
        {
            string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE IVA >= 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
            //Instanciamos la conexion.                
            SqlCommand _Command = new SqlCommand(strSql);
            _Command.Connection = _Connexion;

            _Command.Parameters.AddWithValue("@CodDto", _CodDto);
            _Command.Parameters.AddWithValue("@Serie", _Serie);
            _Command.Parameters.AddWithValue("@Numero", _Numero);
            _Command.Parameters.AddWithValue("@N", _N);

            List<Modelos.FacturasVentasTot> _Iva = new List<Modelos.FacturasVentasTot>();
            try
            {
                using (SqlDataReader reader = _Command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Modelos.FacturasVentasTot fct = new Modelos.FacturasVentasTot();
                            fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                            fct.Iva = Convert.ToDecimal(reader["IVA"]);
                            fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                            _Iva.Add(fct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _Iva;
        }

        public static List<Modelos.FacturasVentasTot> GetTotalesNoGravado(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion)
        {
            string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE CODDTO = @CodDto AND IVA = 0 AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
            //Instanciamos la conexion.
            SqlCommand _Command = new SqlCommand(strSql);
            _Command.Connection = _Connexion;

            _Command.Parameters.AddWithValue("@CodDto", _CodDto);
            _Command.Parameters.AddWithValue("@Serie", _Serie);
            _Command.Parameters.AddWithValue("@Numero", _Numero);
            _Command.Parameters.AddWithValue("@N", _N);

            List<Modelos.FacturasVentasTot> _Iva = new List<Modelos.FacturasVentasTot>();
            try
            {
                using (SqlDataReader reader = _Command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Modelos.FacturasVentasTot fct = new Modelos.FacturasVentasTot();
                            fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                            fct.Iva = Convert.ToDecimal(reader["IVA"]);
                            fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                            _Iva.Add(fct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _Iva;
        }

        public static Modelos.DatosFacturaExpo GetDatosFacturaExpo(string pnumSerie, string pnumFactura, string pN, string _Conection, string _pathLog)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los datos del comprobante");
            Modelos.DatosFacturaExpo _dfe = new Modelos.DatosFacturaExpo();
            try
            {

                string _sql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, FACTURASVENTA.CODCLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.TOTALNETO, " +
                    "SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTA.DTOCOMERCIAL, FACTURASVENTA.DTOPP, FACTURASVENTA.TOTDTOCOMERCIAL, FACTURASVENTA.TOTDTOPP, " +
                    "MONEDAS.CODIGOISONUM, FACTURASVENTA.FACTORMONEDA, FACTURASVENTACAMPOSLIBRES.INCOTERMS " +
                    "FROM FACTURASVENTA inner Join TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "WHERE FACTURASVENTA.NUMSERIE = @NumSerie AND FACTURASVENTA.NUMFACTURA = @NumFactura AND FACTURASVENTA.N = @N";

                using (SqlConnection _conn = new SqlConnection(_Conection))
                {
                    _conn.Open();

                    using (SqlCommand _cmd = new SqlCommand(_sql))
                    {
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.Connection = _conn;

                        _cmd.Parameters.AddWithValue("@NumSerie", pnumSerie);
                        _cmd.Parameters.AddWithValue("@NumFactura", pnumFactura);
                        _cmd.Parameters.AddWithValue("@N", pN);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    _dfe.numSerie = reader.GetString(0);
                                    _dfe.numFactura = reader.GetInt32(1);
                                    _dfe.N = reader.GetString(2);
                                    _dfe.fecha = reader.GetDateTime(3);
                                    _dfe.codCliente = reader.GetInt32(4);
                                    _dfe.totalBruto = Math.Abs(Convert.ToDecimal(reader[5], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.totalNeto = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.TipoDoc = reader[7] == null ? "" : reader.GetString(7);
                                    _dfe.PtoVta = reader[8] == null ? "" : reader.GetString(8);
                                    _dfe.porcentajeDtoComercial = Math.Abs(Convert.ToDecimal(reader[9], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.porcentajeDtoPp = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.totDtoComercial = Math.Abs(Convert.ToDecimal(reader[11], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.totDtoPp = Math.Abs(Convert.ToDecimal(reader[12], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.codigoMoneda = String.IsNullOrEmpty(reader["CODIGOISONUM"].ToString()) ? "" : reader.GetString(13);
                                    _dfe.cotizacion = Math.Abs(Convert.ToDecimal(reader[14], System.Globalization.CultureInfo.InvariantCulture));
                                    _dfe.incoterms = String.IsNullOrEmpty(reader["INCOTERMS"].ToString()) ? "" : reader.GetString(15);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al recuperar los datos de la factura de exportación. Error: " + ex.Message);
            }
            return _dfe;
        }

        public static Modelos.DatosFacturaExpo GetDatosFactura(DataAccess.DatosFactura _dtos, SqlConnection _Conexion, string _pathLog)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los datos del comprobante");
            Modelos.DatosFacturaExpo _dfe = new Modelos.DatosFacturaExpo();
            try
            {

                string _sql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, FACTURASVENTA.CODCLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.TOTALNETO, " +
                    "SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTA.DTOCOMERCIAL, FACTURASVENTA.DTOPP, FACTURASVENTA.TOTDTOCOMERCIAL, FACTURASVENTA.TOTDTOPP, " +
                    "MONEDAS.CODIGOISONUM, FACTURASVENTA.FACTORMONEDA, FACTURASVENTACAMPOSLIBRES.INCOTERMS " +
                    "FROM FACTURASVENTA inner Join TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "WHERE FACTURASVENTA.NUMSERIE = @NumSerie AND FACTURASVENTA.NUMFACTURA = @NumFactura AND FACTURASVENTA.N = @N";


                using (SqlCommand _cmd = new SqlCommand(_sql))
                {
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.Connection = _Conexion;

                    _cmd.Parameters.AddWithValue("@NumSerie", _dtos.NumSerie);
                    _cmd.Parameters.AddWithValue("@NumFactura", _dtos.NumFactura);
                    _cmd.Parameters.AddWithValue("@N", _dtos.N);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                _dfe.numSerie = reader.GetString(0);
                                _dfe.numFactura = reader.GetInt32(1);
                                _dfe.N = reader.GetString(2);
                                _dfe.fecha = reader.GetDateTime(3);
                                _dfe.codCliente = reader.GetInt32(4);
                                _dfe.totalBruto = Math.Abs(Convert.ToDecimal(reader[5], System.Globalization.CultureInfo.InvariantCulture));
                                _dfe.totalNeto = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                _dfe.TipoDoc = reader[7] == DBNull.Value ? _dtos.cCodAfip : reader.GetString(7);
                                _dfe.PtoVta = reader[8] == DBNull.Value ? _dtos.PtoVta : reader.GetString(8);
                                //_dfe.porcentajeDtoComercial = Math.Abs(Convert.ToDecimal(reader[9], System.Globalization.CultureInfo.InvariantCulture));
                                //_dfe.porcentajeDtoPp = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));
                                //_dfe.totDtoComercial = Math.Abs(Convert.ToDecimal(reader[11], System.Globalization.CultureInfo.InvariantCulture));
                                //_dfe.totDtoPp = Math.Abs(Convert.ToDecimal(reader[12], System.Globalization.CultureInfo.InvariantCulture));
                                _dfe.porcentajeDtoComercial = Convert.ToDecimal(reader[9], System.Globalization.CultureInfo.InvariantCulture);
                                _dfe.porcentajeDtoPp = Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture);                                
                                _dfe.codigoMoneda = String.IsNullOrEmpty(reader["CODIGOISONUM"].ToString()) ? "" : reader.GetString(13);
                                _dfe.cotizacion = Math.Abs(Convert.ToDecimal(reader[14], System.Globalization.CultureInfo.InvariantCulture));
                                _dfe.incoterms = String.IsNullOrEmpty(reader["INCOTERMS"].ToString()) ? "" : reader.GetString(15);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al recuperar los datos de la factura de exportación. Error: " + ex.Message);
            }
            return _dfe;
        }

        public static List<AfipDll.wsMtxca.ItemType> GetDatosItemsMtxca(string pnumserie, int pnumalbaran, string pn, int _codAfip,
                    Modelos.DatosFacturaExpo _cabecera, SqlConnection _conexion, string _pathLog)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los Items del comprobante");
            
            string _sql = @"SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN, ALBVENTALIN.REFERENCIA, 
                        ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL AS UNID1, ALBVENTALIN.PRECIO, ALBVENTALIN.TOTAL, ARTICULOS.CODIGOADUANA, ALBVENTALIN.DTO,
                        ArticulosLin.codbarras3, ArticulosLin.CODBARRAS, ALBVENTALIN.PRECIOIVA, ARTICULOSCAMPOSLIBRES.MTX, ALBVENTALIN.IVA/100  as IVA 
                        FROM FACTURASVENTA INNER JOIN ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC and FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC and FACTURASVENTA.N = ALBVENTACAB.NFAC
                        INNER JOIN ALBVENTALIN on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N
                        INNER JOIN ARTICULOSLIN on albventalin.codarticulo = articulosLIN.codarticulo and albventalin.talla = articulosLIN.talla and albventalin.color = articulosLIN.color
                        inner join articulos on articuloslin.codarticulo = ARTICULOS.codarticulo
                        INNER JOIN ARTICULOSCAMPOSLIBRES ON ARTICULOS.CODARTICULO = ARTICULOSCAMPOSLIBRES.CODARTICULO
                        WHERE FACTURASVENTA.NUMSERIE = @NumSerie and FACTURASVENTA.NUMFACTURA = @NumAlbaran and FACTURASVENTA.N = @N";

            List<AfipDll.wsMtxca.ItemType> _dfe = new List<AfipDll.wsMtxca.ItemType>();
            try
            {
                using (SqlCommand _cmd = new SqlCommand(_sql))
                {
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.Connection = _conexion;

                    _cmd.Parameters.AddWithValue("@NumSerie", pnumserie);
                    _cmd.Parameters.AddWithValue("@NumAlbaran", pnumalbaran);
                    _cmd.Parameters.AddWithValue("@N", pn);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                AfipDll.wsMtxca.ItemType _it = new AfipDll.wsMtxca.ItemType();
                                decimal _Iva = Convert.ToDecimal(reader["IVA"]);
                                decimal _Dto = Convert.ToDecimal(reader["DTO"]);
                                _it.cantidad = Math.Abs(Math.Round(Convert.ToDecimal(reader["UNID1"], System.Globalization.CultureInfo.InvariantCulture), 2));
                                _it.unidadesMtx = Math.Abs(Convert.ToInt32(reader["UNID1"], System.Globalization.CultureInfo.InvariantCulture));
                                _it.codigo = String.IsNullOrEmpty(reader["CODBARRAS"].ToString()) ? "" : reader["CODBARRAS"].ToString();
                                _it.codigoMtx = String.IsNullOrEmpty(reader["codbarras3"].ToString()) ? "" : reader["codbarras3"].ToString();
                                //Solo para las facturas y las lineas positivas o negativas.
                                if (_codAfip == 1 || _codAfip == 6 || _codAfip == 11)
                                {
                                    if (Convert.ToInt32(reader["UNID1"]) > 0)
                                        _it.codigoUnidadMedida = String.IsNullOrEmpty(reader["MTX"].ToString()) ? Convert.ToInt16(7) : Convert.ToInt16(reader["MTX"].ToString().Split('-')[0]);
                                    else
                                    {
                                        //Vemos si tenemos un 100 de bonificación.
                                        if(_Dto == 100)
                                            //Si tenemos 100 de Bonificacion no lo mandamos como Devolución porque da error.
                                            _it.codigoUnidadMedida = String.IsNullOrEmpty(reader["MTX"].ToString()) ? Convert.ToInt16(7) : Convert.ToInt16(reader["MTX"].ToString().Split('-')[0]);
                                        else
                                            _it.codigoUnidadMedida = 95;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(reader["UNID1"]) < 0)
                                        _it.codigoUnidadMedida = String.IsNullOrEmpty(reader["MTX"].ToString()) ? Convert.ToInt16(7) : Convert.ToInt16(reader["MTX"].ToString().Split('-')[0]);
                                    else
                                        _it.codigoUnidadMedida = 95;
                                }
                                _it.descripcion = String.IsNullOrEmpty(reader["DESCRIPCION"].ToString()) ? "" : reader["DESCRIPCION"].ToString();

                                switch (_Iva.ToString())
                                {
                                    case "0":
                                        {
                                            _it.codigoCondicionIVA = 3;
                                            break;
                                        }
                                    case "0,105":
                                    case "0.105":
                                        {
                                            _it.codigoCondicionIVA = 4;
                                            break;
                                        }
                                    case "0,21":
                                    case "0.21":
                                        {
                                            _it.codigoCondicionIVA = 5;
                                            break;
                                        }
                                    case "0,27":
                                    case "0.27":
                                        {
                                            _it.codigoCondicionIVA = 6;
                                            break;
                                        }
                                    case "0.05":
                                    case "0,05":
                                        {
                                            _it.codigoCondicionIVA = 8;
                                            break;
                                        }
                                    case "0,025":
                                    case "0.025":
                                        {
                                            _it.codigoCondicionIVA = 9;
                                            break;
                                        }
                                }
                                //Precio unitario
                                if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                          || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                {
                                    if (_cabecera.porcentajeDtoPp < 0)
                                    {
                                        _it.precioUnitario = Math.Round(Convert.ToDecimal(reader["PRECIOIVA"]), 2);
                                        
                                        //Es un recargo, no se debe enviar la bonificación, y se debe incorporar al precio el recargo.
                                        decimal _bonifPp = _it.precioUnitario * Math.Abs(_cabecera.porcentajeDtoPp) / 100;

                                        _it.precioUnitario = Math.Round(_it.precioUnitario + _bonifPp, 3);

                                    }
                                    else
                                    {
                                        _it.precioUnitario = Math.Round(Convert.ToDecimal(reader["PRECIOIVA"]), 3);
                                    }

                                    _it.importeBonificacion = Convert.ToDecimal(reader["DTO"]) == 0 ? 0 :
                                        Math.Round((Math.Abs(_it.precioUnitario * _it.cantidad) * Convert.ToDecimal(reader["DTO"]) / 100), 3);
                                    _it.importeIVA = Math.Round(((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva), 2);
                                    if (_it.codigoUnidadMedida == 95)
                                        _it.importeItem = Math.Round((((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion)) * -1, 2);
                                    else
                                        _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion), 2);
                                }
                                else
                                {
                                    if (_cabecera.porcentajeDtoPp < 0)
                                    {
                                        _it.precioUnitario = Convert.ToDecimal(reader["PRECIO"]);
                                        //Es un recargo, no se debe enviar la bonificación, y se debe incorporar al precio el recargo.
                                        decimal _bonifPp = _it.precioUnitario * Math.Abs(_cabecera.porcentajeDtoPp) / 100;
                                        _it.precioUnitario = Math.Round(_it.precioUnitario + _bonifPp, 3);
                                    }
                                    else
                                        _it.precioUnitario = Math.Round(Convert.ToDecimal(reader["PRECIO"]), 3);
                                    _it.importeBonificacion = Convert.ToDecimal(reader["DTO"]) == 0 ? 0 :
                                        Math.Round((Math.Abs(_it.precioUnitario * _it.cantidad) * Convert.ToDecimal(reader["DTO"]) / 100),3);
                                    _it.importeIVA = Math.Round(((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva), 2);
                                    _it.importeItem = Math.Round((((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva)), 2);

                                }
                                //
                                //Vemos el dto comercial
                                if (_cabecera.porcentajeDtoComercial > 0)
                                {
                                    decimal _precioComercial = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion), 3);
                                    decimal _bonifComercial = Math.Round((_precioComercial * _cabecera.porcentajeDtoComercial / 100), 3);
                                    decimal _totalBonifComercial = Math.Round(_it.importeBonificacion + _bonifComercial, 3);
                                    _it.importeBonificacion = Math.Round(_totalBonifComercial, 3);
                                    _it.importeIVA = Math.Round((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva, 2);
                                    if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                          || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                    {
                                        if (_it.codigoUnidadMedida == 95)
                                            _it.importeItem = Math.Round((((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion)) * -1, 2);
                                        else
                                            _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion), 2);
                                    }
                                    else
                                        _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva), 2);
                                }
                                //Vemos el dto PP
                                if (_cabecera.porcentajeDtoPp > 0)
                                {
                                    decimal _precioPp = (_it.precioUnitario * _it.cantidad) - _it.importeBonificacion;
                                    decimal _bonifPp = _precioPp * _cabecera.porcentajeDtoPp / 100;
                                    decimal _totalBonifComercial = _it.importeBonificacion + _bonifPp;
                                    _it.importeBonificacion = Math.Round(_totalBonifComercial, 3);
                                    _it.importeIVA = Math.Round((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva, 2);

                                    if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                          || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                    {
                                        if (_it.codigoUnidadMedida == 95)
                                            _it.importeItem = Math.Round((((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion)) * -1, 2);
                                        else
                                            _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion), 2);
                                    }
                                    else
                                        _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva), 2);
                                }
                                
                                //Cambio para asegurarnos cuando es 95
                                if (_it.codigoUnidadMedida == 95)
                                {                                    
                                    if (_it.importeItem > 0)
                                        _it.importeItem = _it.importeItem * (-1);
                                    if (_it.importeIVA > 0)
                                        _it.importeIVA = _it.importeIVA * (-1);                                    
                                }

                                _dfe.Add(_it);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
                throw new Exception("Error al recuperar los datos de los items de la factura. Error: " + ex.Message);
            }
            return _dfe;
        }

        public static List<Modelos.ComprobanteAsociado> GetComprobantesAsociados(string _numserie, int _numfac, string _n, SqlConnection _conexion)
        {
            string _sql = @"select distinct ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, ALBVENTALIN.ABONODE_N
                    from ALBVENTACAB inner join albventalin on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN 
                    and ALBVENTACAB.N = ALBVENTALIN.N WHERE ALBVENTACAB.NUMSERIEFAC = @NumSerie AND ALBVENTACAB.NUMFAC = @NumFac AND ALBVENTACAB.NFAC = @N";

            List<Modelos.ComprobanteAsociado> _lst = new List<Modelos.ComprobanteAsociado>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                if (_conexion.State == System.Data.ConnectionState.Closed)
                    _conexion.Open();

                _cmd.Connection = _conexion;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros.
                _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                _cmd.Parameters.AddWithValue("@NumFac", _numfac);
                _cmd.Parameters.AddWithValue("@N", _n);

                try
                {
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Modelos.ComprobanteAsociado _cls = new Modelos.ComprobanteAsociado();
                                _cls.n = reader["ABONODE_N"].ToString();
                                _cls.numAlbaran = Convert.ToInt32(reader["ABONODE_NUMALBARAN"]);
                                _cls.numSerie = reader["ABONODE_NUMSERIE"].ToString();

                                _lst.Add(_cls);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return _lst;
        }

        public static AfipDll.wsMtxca.ComprobanteAsociadoType GetAfipComprobanteAsocMTXCA(string _numserie, int _numfac, string _n, string _cuit, int _tipoComprobante, SqlConnection _conexion)
        {
            string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

            AfipDll.wsMtxca.ComprobanteAsociadoType _cls = new AfipDll.wsMtxca.ComprobanteAsociadoType();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conexion;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros.
                _cmd.Parameters.AddWithValue("@numserie", _numserie);
                _cmd.Parameters.AddWithValue("@numfac", _numfac);
                _cmd.Parameters.AddWithValue("@n", _n);

                try
                {
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime _fech = Convert.ToDateTime(reader["FECHA"]);

                                _cls.fechaEmision = _fech;
                                _cls.fechaEmisionSpecified = true;
                                if (_tipoComprobante == 203 || _tipoComprobante == 208 || _tipoComprobante == 213
                                        || _tipoComprobante == 202 || _tipoComprobante == 207 || _tipoComprobante == 212)
                                {
                                    _cls.cuit = Convert.ToInt64(_cuit);
                                    _cls.cuitSpecified = true;
                                }

                                _cls.numeroComprobante = Convert.ToInt64(reader["NUMEROFISCAL"]);
                                _cls.numeroPuntoVenta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                _cls.codigoTipoComprobante = Convert.ToInt16(reader["DESCRIPCION"].ToString().Substring(0, 3));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return _cls;
        }

        #endregion
        public static List<AfipDll.wsAfipCae.AlicIva> ArmarIVAAfip(List<DataAccess.FacturasVentasTot> _Iva)
        {
            List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();

            foreach (DataAccess.FacturasVentasTot itm in _Iva)
            {
                AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                switch (itm.Iva.ToString())
                {
                    case "0":
                        {
                            _al.Id = 3;
                            break;
                        }
                    case "21":
                        {
                            _al.Id = 5;
                            break;
                        }
                    case "10.5":
                    case "10,5":
                        {
                            _al.Id = 4;
                            break;
                        }
                    case "27":
                        {
                            _al.Id = 6;
                            break;
                        }
                }
                _al.BaseImp = Math.Abs(Convert.ToDouble(itm.BaseImponible));
                _al.Importe = Math.Abs(Convert.ToDouble(itm.TotIva));
                _alicIVA.Add(_al);
            }
            return _alicIVA;
        }

        public static List<wsAfip.clsIVA> ArmarIVA(List<Modelos.FacturasVentasTot> _Iva)
        {
            List<AfipDll.wsAfip.clsIVA> _Rta = new List<AfipDll.wsAfip.clsIVA>();
            foreach (Modelos.FacturasVentasTot itm in _Iva)
            {
                AfipDll.wsAfip.clsIVA cls = new AfipDll.wsAfip.clsIVA();
                switch (itm.Iva.ToString())
                {
                    case "0":
                        {
                            cls.ID = 3;
                            break;
                        }
                    case "21":
                        {
                            cls.ID = 5;
                            break;
                        }
                    case "10.5":
                    case "10,5":
                        {
                            cls.ID = 4;
                            break;
                        }
                    case "27":
                        {
                            cls.ID = 6;
                            break;
                        }
                }
                if (itm.BaseImponible < 0)
                    cls.BaseImp = Convert.ToDouble(itm.BaseImponible) * -1;
                else
                    cls.BaseImp = Convert.ToDouble(itm.BaseImponible);
                if (itm.TotIva < 0)
                    cls.Importe = Convert.ToDouble(itm.TotIva) * -1;
                else
                    cls.Importe = Convert.ToDouble(itm.TotIva);
                _Rta.Add(cls);
            }
            return _Rta;
        }

        public static List<AfipDll.wsAfipCae.Tributo> ArmaTributos(List<DataAccess.FacturasVentasTot> _Tributos)
        {
            //string strRta = "";
            double totalBaseImponible = 0;
            double totalImpuesto = 0;
            List<AfipDll.wsAfipCae.Tributo> _Rta = new List<AfipDll.wsAfipCae.Tributo>();
            foreach (DataAccess.FacturasVentasTot itm in _Tributos)
            {
                totalBaseImponible = totalBaseImponible + Convert.ToDouble(itm.BaseImponible);
                totalImpuesto = totalImpuesto + Convert.ToDouble(itm.TotIva);

            }

            if (totalImpuesto != 0)
            {
                AfipDll.wsAfipCae.Tributo cls = new AfipDll.wsAfipCae.Tributo();
                cls.Alic = 3;
                if (totalBaseImponible < 0)
                    cls.BaseImp = totalBaseImponible * -1;
                else
                    cls.BaseImp = totalBaseImponible;
                cls.Desc = "Percep IIBB";
                cls.Id = 2;
                if (totalImpuesto < 0)
                    cls.Importe = totalImpuesto * -1;
                else
                    cls.Importe = totalImpuesto;
                _Rta.Add(cls);
            }
            return _Rta;
        }

        public static List<wsAfip.clsTributos> ArmaTributos(List<Modelos.FacturasVentasTot> _Tributos)
        {
            double totalBaseImponible = 0;
            double totalImpuesto = 0;
            List<wsAfip.clsTributos> _Rta = new List<wsAfip.clsTributos>();
            foreach (Modelos.FacturasVentasTot itm in _Tributos)
            {
                totalBaseImponible = totalBaseImponible + Convert.ToDouble(itm.BaseImponible);
                totalImpuesto = totalImpuesto + Convert.ToDouble(itm.TotIva);

            }
            if (totalImpuesto != 0)
            {
                wsAfip.clsTributos cls = new wsAfip.clsTributos();
                cls.Alic = 3;
                if (totalBaseImponible < 0)
                    cls.BaseImp = totalBaseImponible * -1;
                else
                    cls.BaseImp = totalBaseImponible;
                cls.Desc = "Percep IIBB";
                cls.ID = 2;
                if (totalImpuesto < 0)
                    cls.Importe = totalImpuesto * -1;
                else
                    cls.Importe = totalImpuesto;
                _Rta.Add(cls);
            }
            return _Rta;
        }

        public static List<AfipDll.wsMtxca.ComprobanteAsociadoType> ArmarCbteAsocMtxca(DataAccess.DatosFactura fc, int _tipoComprobante,
            string _pathLog, string _cuit, SqlConnection _sql)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los comprobantes Asociados");
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lst = new List<AfipDll.wsMtxca.ComprobanteAsociadoType>();
            try
            {
                List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _sql);

                foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                {
                    AfipDll.wsMtxca.ComprobanteAsociadoType _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocMTXCA(_ca.numSerie, _ca.numAlbaran, _ca.n, _cuit, _tipoComprobante, _sql);
                    _lst.Add(_cls);
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error buscando los comprobantes Asociados. Error: " + ex.Message);
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        public static List<AfipDll.wsAfipCae.FECAEADetRequest> CargarDetalleCAEAAfip(DataAccess.DatosFactura _Fac, int _NroComprobante, AfipDll.CAEA _caea)
        {
            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();

            try
            {
                AfipDll.wsAfipCae.FECAEADetRequest _det = new AfipDll.wsAfipCae.FECAEADetRequest();
                _det.Concepto = 2;
                _det.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
                _det.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
                _det.CbteDesde = _NroComprobante;
                _det.CbteHasta = _NroComprobante;
                _det.CbteFch = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0');
                _det.ImpTotal = Math.Abs(Convert.ToDouble(_Fac.TotalNeto));
                _det.ImpTotConc = 0;
                _det.ImpNeto = Math.Abs(Convert.ToDouble(_Fac.TotalBruto));
                _det.ImpOpEx = 0;
                _det.ImpIVA = Math.Abs(Convert.ToDouble(_Fac.TotIva));
                _det.ImpTrib = Math.Abs(Convert.ToDouble(_Fac.totTributos));
                _det.FchServDesde = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchServHasta = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchVtoPago = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.MonId = "PES";
                _det.MonCotiz = 1;
                _det.CAEA = _caea.Caea;
                _det.CbteFchHsGen = _Fac.CompFechaHoraGen;

                _detReq.Add(_det);

                return _detReq;
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al CargarDetalle. Error: " + ex.Message);
            }
        }

        public static List<AfipDll.wsAfipCae.FECAEADetRequest> CargarDetalleCAEAAfip(DataAccess.DatosFactura _Fac, int _NroComprobante, string _caea)
        {
            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();

            try
            {
                AfipDll.wsAfipCae.FECAEADetRequest _det = new AfipDll.wsAfipCae.FECAEADetRequest();
                _det.Concepto = 2;
                _det.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
                _det.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
                _det.CbteDesde = _NroComprobante;
                _det.CbteHasta = _NroComprobante;
                _det.CbteFch = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0');
                _det.ImpTotal = Math.Abs(Convert.ToDouble(_Fac.TotalNeto));
                _det.ImpTotConc = 0;
                _det.ImpNeto = Math.Abs(Convert.ToDouble(_Fac.TotalBruto));
                _det.ImpOpEx = 0;
                _det.ImpIVA = Math.Abs(Convert.ToDouble(_Fac.TotIva));
                _det.ImpTrib = Math.Abs(Convert.ToDouble(_Fac.totTributos));
                _det.FchServDesde = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchServHasta = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.FchVtoPago = _Fac.strFecha; //.fecha.Year.ToString().PadLeft(4, '0') + _Fac.fecha.Month.ToString().PadLeft(2, '0') + _Fac.fecha.Day.ToString().PadLeft(2, '0'); ;
                _det.MonId = "PES";
                _det.MonCotiz = 1;
                _det.CAEA = _caea;
                _det.CbteFchHsGen = _Fac.CompFechaHoraGen;

                _detReq.Add(_det);

                return _detReq;
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al CargarDetalle. Error: " + ex.Message);
            }
        }

        private List<AfipDll.wsMtxca.ComprobanteAsociadoType> ArmarCbteAsocMtxca(DataAccess.DatosFactura fc, int _tipoComprobante, string _pathLog, string _cuit, string _conection)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los comprobantes Asociados");
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lst = new List<AfipDll.wsMtxca.ComprobanteAsociadoType>();
            try
            {
                using (SqlConnection _conexion = new SqlConnection(_conection))
                {
                    List<Modelos.ComprobanteAsociado> _icgCompAsoc = GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _conexion);

                    foreach (Modelos.ComprobanteAsociado _ca in _icgCompAsoc)
                    {
                        AfipDll.wsMtxca.ComprobanteAsociadoType _cls = GetAfipComprobanteAsocMTXCA(_ca.numSerie, _ca.numAlbaran, _ca.n, _cuit, _tipoComprobante, _conexion);
                        _lst.Add(_cls);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        /// <summary>
        /// Servicios de MTXCA
        /// </summary>
        public class Mtxca
        {
            /// <summary>
            /// Metodo para fiscalizar un comprobante con MTXCA
            /// </summary>
            /// <param name="datosFactura">Clase DataAccess.DatosFactura, con los datos del Comprobante.</param>
            /// <param name="codigoIVA">Código de IVA</param>
            /// <param name="ptovtaCAE">Punto de venta de CAE</param>
            /// <param name="password">Password del certificado</param>
            /// <param name="soloCAEA">Es Solo CAEA</param>
            /// <param name="esTest">Ambiente de Test o Producción</param>
            /// <param name="generaXML">Genera XML del request y response</param>
            /// <param name="pathLog">Path del Log</param>
            /// <param name="pathCertificado">Path del certificado</param>
            /// <param name="pathTA">Path del ticket de acceso</param>
            /// <param name="Cuit">Cuit</param>
            /// <param name="connection">Conexión SQL</param>
            /// <returns>TRUE/FALSE</returns>
            public static bool Fiscalizar(DataAccess.DatosFactura datosFactura, int codigoIVA, string ptovtaCAE, string password, 
                bool soloCAEA, bool esTest, bool generaXML, string pathLog, string pathCertificado, string pathTA, 
                string Cuit, SqlConnection connection)
            {
                //string _dummy = "FALSE";
                WsMTXCA.Dummy _dummy = new WsMTXCA.Dummy();
                List<Modelos.FacturasVentasTot> _Iva = RetailService.GetTotalesIva(datosFactura.NumSerie, datosFactura.N, 
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.TotIva = _Iva.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(datosFactura.NumSerie, datosFactura.N, 
                    datosFactura.NumFactura, codigoIVA, connection, datosFactura.TotalBruto);
                datosFactura.totTributos = _Tributos.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _NoGravado = RetailService.GetTotalesNoGravado(datosFactura.NumSerie, datosFactura.N, 
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                //Asigno el punto de venta de CAE.
                datosFactura.PtoVta = ptovtaCAE;
                //Validamos los datos de la FC.
                DataAccess.FuncionesVarias.ValidarDatosFactura(datosFactura);
                //Armamos la password como segura
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (soloCAEA)
                {
                    _dummy.Autorizaciones = "False";
                    _dummy.BaseDatos = "False";
                    _dummy.Server = "False";
                }
                else
                {
                    //Vemos si esta disponible el servicio de AFIP.
                    _dummy = WsMTXCA.TestDummy(pathLog, esTest);
                }

                if (_dummy.Autorizaciones == "OK" && _dummy.BaseDatos == "OK" && _dummy.Server == "OK")
                {
                    AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                    //_comproFc.cbu = _cbu;
                    _comproFc.codigoMoneda = "PES";
                    _comproFc.concepto = 2; //TODO de donde lo saco???
                    _comproFc.cotizacionMoneda = 1;
                    _comproFc.cuit = Cuit.Replace("-", "");
                    _comproFc.fechaDesde = datosFactura.fecha;
                    _comproFc.fechaEmision = datosFactura.fecha;  // dttFecha;
                    _comproFc.fechaHasta = datosFactura.fecha;    // dttFecha;
                    _comproFc.fechaPago = datosFactura.fecha;     // dttFecha;
                    _comproFc.fechaVto = datosFactura.fecha.AddDays(10);      // dttFecha.AddDays(10);
                    _comproFc.importeExento = 0;
                    _comproFc.importeGravado = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                    _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                    _comproFc.importeSubTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                    _comproFc.importeTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalNeto));
                    _comproFc.importeTributos = Math.Abs(datosFactura.totTributos);
                    _comproFc.NcRechazo = "SI"; //Armar como MiPyme.
                    _comproFc.numeroComprobante = 0; //Lo pongo luego de consultar el ultimo
                    _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                    _comproFc.numeroDocumento = Convert.ToInt64(datosFactura.ClienteNroDoc);
                    _comproFc.obervaciones = "";
                    _comproFc.ptoVta = Convert.ToInt32(datosFactura.PtoVta);
                    _comproFc.tipoComprobante = Convert.ToInt16(datosFactura.cCodAfip);
                    _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                    _comproFc.tipoDocumento = Convert.ToInt16(datosFactura.ClienteTipoDoc);
                    List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                    //Recupero el nro del ultimo comprobante.
                    Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(pathCertificado, pathTA, pathLog,
                        Convert.ToInt32(datosFactura.PtoVta), Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest,
                        strPasswordSecureString, false, out _lstError);
                    //Vemos si tenemos algun error
                    if (_lstError.Count() == 0)
                    {
                        //incremento el nro de comprobante.
                        _comproFc.numeroComprobante = _ultimoComprobante + 1;
                        //Recupero los datos de la fc.
                        Modelos.DatosFacturaExpo _datosFcExpo = RetailService.GetDatosFactura(datosFactura, connection, pathLog);

                        //Armo el string de IVA.
                        List<AfipDll.wsAfip.clsIVA> _IvaRequest = RetailService.ArmarIVA(_Iva);
                        //Items
                        List<AfipDll.wsMtxca.ItemType> _icgItems = RetailService.GetDatosItemsMtxca(datosFactura.NumSerie,
                            Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, Convert.ToInt32(datosFactura.cCodAfip),
                            _datosFcExpo, connection, pathLog);
                        //Armos el string de Tributos.
                        List<AfipDll.wsAfip.clsTributos> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                        //Armo los comprobantes asociados.
                        List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = RetailService.ArmarCbteAsocMtxca(datosFactura,
                            _comproFc.tipoComprobante, pathLog, Cuit, connection);
                        //Verifico que si es una NC/ND.
                        if (Convert.ToInt32(datosFactura.cCodAfip) == 203 || Convert.ToInt32(datosFactura.cCodAfip) == 208 || Convert.ToInt32(datosFactura.cCodAfip) == 213
                            || Convert.ToInt32(datosFactura.cCodAfip) == 202 || Convert.ToInt32(datosFactura.cCodAfip) == 207 || Convert.ToInt32(datosFactura.cCodAfip) == 212
                            || Convert.ToInt32(datosFactura.cCodAfip) == 3 || Convert.ToInt32(datosFactura.cCodAfip) == 8 || Convert.ToInt32(datosFactura.cCodAfip) == 13
                            || Convert.ToInt32(datosFactura.cCodAfip) == 2 || Convert.ToInt32(datosFactura.cCodAfip) == 7 || Convert.ToInt32(datosFactura.cCodAfip) == 12)
                        {
                            //Verifico que tengo por lo menos 1 comprobante asociado.
                            if (_compAsoc[0].numeroComprobante == 0)
                            {
                                frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_comproFc.tipoComprobante, Cuit, true);
                                _frm.ShowDialog();
                                _compAsoc = _frm._lstNew;
                                _frm.Dispose();
                                if (_compAsoc.Count == 0)
                                {
                                    //Insertamos en FacturasCamposLibres
                                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, "Error: No posee comprobante asociado", null, null, connection);
                                    return false;
                                }
                            }
                        }
                        try
                        {
                            AfipDll.WsMTXCA.ObtenerCAE(pathCertificado, pathTA, pathLog, Convert.ToInt32(datosFactura.PtoVta),
                                Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest, strPasswordSecureString, generaXML, _IvaRequest, _icgItems, _TributosRequest,
                                _comproFc, _compAsoc, "ADC", out _lstError);
                            string _rta = "";
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstError)
                            {
                                switch (_er.Code)
                                {
                                    case 1:
                                        {
                                            //Resultado.
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Resultado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Resultado: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 2:
                                        {
                                            //Observaciones.
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Observación: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Observación: " + _er.Msg + Environment.NewLine;
                                            //Insertamos en FacturasCamposLibres
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, _er.Msg, null, null, connection);
                                            break;
                                        }
                                    case 3:
                                        {
                                            //Errores.
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Error: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Error: " + _er.Msg + Environment.NewLine;
                                            //Insertamos en FacturasCamposLibres
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, _er.Msg, null, null, connection);
                                            //Mostramos el error devuelto por la AFIP.
                                            MessageBox.Show(new Form { TopMost = true }, "El servicio de la AFIP reporto el siguiente error." + Environment.NewLine +
                                                _er.Msg, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            break;
                                        }
                                    case 4:
                                        {
                                            //Evento.
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Evento: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Evento: " + _er.Msg + Environment.NewLine;
                                            //Insertamos en FacturasCamposLibres
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, 
                                                null, _er.Msg, null, null, null, pathLog, null, connection);
                                            break;
                                        }
                                    case 0:
                                        {
                                            //Respuesta con CAE.
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            string[] _Resultado = _er.Msg.Split('|');
                                            //Genero codigo de barra
                                            string _codBarra = "";
                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(Cuit, datosFactura.cCodAfip, datosFactura.PtoVta.ToString(), _Resultado[0], _Resultado[1], out _codBarra);
                                            //Genero el codigo del QR.
                                            string _qr = QR.CrearJson(1, datosFactura.fecha, Convert.ToInt64(Cuit), _comproFc.ptoVta, _comproFc.tipoComprobante,
                                                Convert.ToInt32(_Resultado[2]), _comproFc.importeTotal, _comproFc.codigoMoneda, _comproFc.cotizacionMoneda,
                                                _comproFc.tipoDocumento, _comproFc.numeroDocumento, "E", Convert.ToInt64(_Resultado[0]));
                                            //Insertamos en FacturasVentasSeriesResol
                                            DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(datosFactura, Convert.ToInt32(_Resultado[2]), pathLog, connection);
                                            //Insertamos en FacturasCamposLibres
                                            string clPtoVta = datosFactura.PtoVta.ToString().PadLeft(5, '0') + "-" + _Resultado[2].ToString().PadLeft(8, '0');
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura.NumSerie, datosFactura.NumFactura,
                                                datosFactura.N, _Resultado[0], _er.Msg, _Resultado[1], _codBarra, _qr, pathLog, clPtoVta, connection);

                                            break;
                                        }
                                }
                            }
                            AfipDll.LogFile.ErrorLog(LogFile.CreatePath(pathLog), _rta);
                            return true;
                        }
                        catch (Exception ex)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "MTXCA. Fiscalizar. " + ex.Message);
                            MessageBox.Show(new Form { TopMost = true }, ex.Message);
                            //Recupero el nro del ultimo comprobante.
                            Int64 _ultimoComprobanteError = AfipDll.WsMTXCA.RecuperoUltimoComprobante(pathCertificado, pathTA, pathLog,
                                Convert.ToInt32(datosFactura.PtoVta), Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest, strPasswordSecureString, false, out _lstError);
                            //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                            if (_ultimoComprobante == _ultimoComprobanteError)
                            {
                                //Guardamos el numero y ponemos el estado en Recuperar.
                                //Modificamos el registro en FacturasCamposLibres.
                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, _ultimoComprobante, ex.Message, connection);
                            }
                            return false;
                        }
                    }
                    else
                    {
                        string _error = "";
                        foreach (AfipDll.WsMTXCA.Errors er in _lstError)
                        {
                            _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                        }
                        //Error al recuperar el ultimo numero de factura de la AFIP
                        MessageBox.Show(new Form { TopMost = true }, "hay error en la busqueda del ultimo");
                        //Error al recuperar el ultimo numero de factura de la AFIP
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, "", _error, "", "", connection);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "El servicio de la AFIP no esta disponible en este momento. Intente mas tarde." + Environment.NewLine +
                        "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                        "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                        "Servicio de Servidor AFIP: " + _dummy.Server,
                               "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    return false;
                }
            }

            /// <summary>
            /// Metodo para fiscalizar un comprobante con MTXCA
            /// </summary>
            /// <param name="datosFactura">Clase DataAccess.DatosFactura, con los datos del Comprobante.</param>
            /// <param name="codigoIVA">Código de IVA</param>
            /// <param name="ptovtaCAE">Punto de venta de CAE</param>
            /// <param name="codigoIIBB">Codigo de IIBB</param>
            /// <param name="codVendedor">Codigo de Vendedor</param>
            /// <param name="ptoVtaCAEA">Punto de Venta CAEA</param>
            /// <param name="password">Password del certificado</param>
            /// <param name="soloCAEA">Es Solo CAEA</param>
            /// <param name="esTest">Ambiente de Test o Producción</param>
            /// <param name="generaXML">Genera XML del request y response</param>
            /// <param name="pathLog">Path del Log</param>
            /// <param name="pathCertificado">Path del certificado</param>
            /// <param name="pathTA">Path del ticket de acceso</param>
            /// <param name="Cuit">Cuit</param>
            /// <param name="connection">Conexión SQL</param>
            /// <returns>TRUE/FALSE</returns>
            public static bool Fiscalizar(DataAccess.DatosFactura datosFactura, int codigoIVA, int codigoIIBB, string ptovtaCAE, string ptoVtaCAEA, string password,
                bool soloCAEA, bool esTest, bool generaXML, string pathLog, string pathCertificado, string pathTA, string Cuit, 
                string codVendedor, SqlConnection connection)
            {
                //string _dummy = "FALSE";
                WsMTXCA.Dummy _dummy = new WsMTXCA.Dummy();
                List<Modelos.FacturasVentasTot> _Iva = RetailService.GetTotalesIva(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.TotIva = _Iva.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIIBB, connection, datosFactura.TotalBruto);
                datosFactura.totTributos = _Tributos.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _NoGravado = RetailService.GetTotalesNoGravado(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                //Asigno el punto de venta de CAE.
                datosFactura.PtoVta = ptovtaCAE;
                //Validamos los datos de la FC.
                DataAccess.FuncionesVarias.ValidarDatosFactura(datosFactura);
                //Armamos la password como segura
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (soloCAEA)
                {
                    _dummy.Autorizaciones = "False";
                    _dummy.BaseDatos = "False";
                    _dummy.Server = "False";
                }
                else
                {
                    //Vemos si esta disponible el servicio de AFIP.
                    _dummy = WsMTXCA.TestDummy(pathLog, esTest);
                }

                if (_dummy.Autorizaciones == "OK" && _dummy.BaseDatos == "OK" && _dummy.Server == "OK")
                {
                    try
                    {
                        AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                        //_comproFc.cbu = _cbu;
                        _comproFc.codigoMoneda = "PES";
                        _comproFc.concepto = 2; //TODO de donde lo saco???
                        _comproFc.cotizacionMoneda = 1;
                        _comproFc.cuit = Cuit.Replace("-", "");
                        _comproFc.fechaDesde = datosFactura.fecha;
                        _comproFc.fechaEmision = datosFactura.fecha;  // dttFecha;
                        _comproFc.fechaHasta = datosFactura.fecha;    // dttFecha;
                        _comproFc.fechaPago = datosFactura.fecha;     // dttFecha;
                        _comproFc.fechaVto = datosFactura.fecha.AddDays(10);      // dttFecha.AddDays(10);
                        _comproFc.importeExento = 0;
                        _comproFc.importeGravado = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                        _comproFc.importeSubTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalNeto));
                        _comproFc.importeTributos = Math.Abs(datosFactura.totTributos);
                        _comproFc.NcRechazo = "SI"; //Armar como MiPyme.
                        _comproFc.numeroComprobante = 0; //Lo pongo luego de consultar el ultimo
                        _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                        _comproFc.numeroDocumento = Convert.ToInt64(datosFactura.ClienteNroDoc);
                        _comproFc.obervaciones = datosFactura.NumSerie + "-" + datosFactura.NumFactura + "-" + datosFactura.N;
                        _comproFc.ptoVta = Convert.ToInt32(datosFactura.PtoVta);
                        _comproFc.tipoComprobante = Convert.ToInt16(datosFactura.cCodAfip);
                        _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                        _comproFc.tipoDocumento = Convert.ToInt16(datosFactura.ClienteTipoDoc);
                        List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                        //Recupero el nro del ultimo comprobante.
                        Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(pathCertificado, pathTA, pathLog,
                            Convert.ToInt32(datosFactura.PtoVta), Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest,
                            strPasswordSecureString, false, out _lstError);

                        //Vemos si tenemos algun error
                        if (_lstError.Count() == 0)
                        {
                            //incremento el nro de comprobante.
                            _comproFc.numeroComprobante = _ultimoComprobante + 1;
                            //Recupero los datos de la fc.
                            Modelos.DatosFacturaExpo _datosFcExpo = RetailService.GetDatosFactura(datosFactura, connection, pathLog);

                            //Armo el string de IVA.
                            List<AfipDll.wsAfip.clsIVA> _IvaRequest = RetailService.ArmarIVA(_Iva);
                            //Items
                            List<AfipDll.wsMtxca.ItemType> _icgItems = RetailService.GetDatosItemsMtxca(datosFactura.NumSerie,
                                Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, Convert.ToInt32(datosFactura.cCodAfip),
                                _datosFcExpo, connection, pathLog);
                            //Armos el string de Tributos.
                            List<AfipDll.wsAfip.clsTributos> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                            //Armo los comprobantes asociados.
                            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = RetailService.ArmarCbteAsocMtxca(datosFactura,
                                _comproFc.tipoComprobante, pathLog, Cuit, connection);
                            //Verifico que si es una NC/ND.
                            if (Convert.ToInt32(datosFactura.cCodAfip) == 203 || Convert.ToInt32(datosFactura.cCodAfip) == 208 || Convert.ToInt32(datosFactura.cCodAfip) == 213
                                || Convert.ToInt32(datosFactura.cCodAfip) == 202 || Convert.ToInt32(datosFactura.cCodAfip) == 207 || Convert.ToInt32(datosFactura.cCodAfip) == 212
                                || Convert.ToInt32(datosFactura.cCodAfip) == 3 || Convert.ToInt32(datosFactura.cCodAfip) == 8 || Convert.ToInt32(datosFactura.cCodAfip) == 13
                                || Convert.ToInt32(datosFactura.cCodAfip) == 2 || Convert.ToInt32(datosFactura.cCodAfip) == 7 || Convert.ToInt32(datosFactura.cCodAfip) == 12)
                            {
                                //Verifico que tengo por lo menos 1 comprobante asociado.
                                if (_compAsoc[0].numeroComprobante == 0)
                                {
                                    frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_comproFc.tipoComprobante, Cuit, true);
                                    _frm.ShowDialog();
                                    _compAsoc = _frm._lstNew;
                                    _frm.Dispose();
                                    if (_compAsoc.Count == 0)
                                    {
                                        //Insertamos en FacturasCamposLibres
                                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, "Error: No posee comprobante asociado", null, null, connection);
                                        return false;
                                    }
                                }
                            }
                            try
                            {
                                AfipDll.WsMTXCA.ObtenerCAE(pathCertificado, pathTA, pathLog, Convert.ToInt32(datosFactura.PtoVta),
                                    Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest, strPasswordSecureString, generaXML, _IvaRequest, _icgItems, _TributosRequest,
                                    _comproFc, _compAsoc, "ADC", out _lstError);
                                string _rta = "";
                                foreach (AfipDll.WsMTXCA.Errors _er in _lstError.OrderByDescending(x => x.Code))
                                {
                                    switch (_er.Code)
                                    {
                                        case 1:
                                            {
                                                if (String.IsNullOrEmpty(_rta))
                                                    _rta = "Resultado: " + _er.Msg + Environment.NewLine;
                                                else
                                                    _rta = _rta + "Resultado: " + _er.Msg + Environment.NewLine;
                                                break;
                                            }
                                        case 2:
                                            {
                                                if (String.IsNullOrEmpty(_rta))
                                                    _rta = "Observación: " + _er.Msg + Environment.NewLine;
                                                else
                                                    _rta = _rta + "Observación: " + _er.Msg + Environment.NewLine;
                                                //Insertamos en FacturasCamposLibres
                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, _er.Msg, null, null, connection);
                                                break;
                                            }
                                        case 3:
                                            {
                                                if (String.IsNullOrEmpty(_rta))
                                                    _rta = "Error: " + _er.Msg + Environment.NewLine;
                                                else
                                                    _rta = _rta + "Error: " + _er.Msg + Environment.NewLine;
                                                //Insertamos en FacturasCamposLibres
                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, _er.Msg, null, null, connection);
                                                //Mostramos el error devuelto por la AFIP.
                                                MessageBox.Show(new Form { TopMost = true }, "El servicio de la AFIP reporto el siguiente error." + Environment.NewLine +
                                                    _er.Msg, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                break;
                                            }
                                        case 4:
                                            {
                                                if (String.IsNullOrEmpty(_rta))
                                                    _rta = "Evento: " + _er.Msg + Environment.NewLine;
                                                else
                                                    _rta = _rta + "Evento: " + _er.Msg + Environment.NewLine;
                                                //Insertamos en FacturasCamposLibres
                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, null, _er.Msg, null, null, connection);
                                                break;
                                            }
                                        case 0:
                                            {
                                                if (String.IsNullOrEmpty(_rta))
                                                    _rta = "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                                else
                                                    _rta = _rta + "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                                string[] _Resultado = _er.Msg.Split('|');
                                                //Genero codigo de barra
                                                string _codBarra = "";
                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(Cuit, datosFactura.cCodAfip, datosFactura.PtoVta.ToString(), _Resultado[0], _Resultado[1], out _codBarra);
                                                //Genero el codigo del QR.
                                                string _qr = QR.CrearJson(1, datosFactura.fecha, Convert.ToInt64(Cuit), _comproFc.ptoVta, _comproFc.tipoComprobante,
                                                    Convert.ToInt32(_Resultado[2]), _comproFc.importeTotal, _comproFc.codigoMoneda, _comproFc.cotizacionMoneda,
                                                    _comproFc.tipoDocumento, _comproFc.numeroDocumento, "E", Convert.ToInt64(_Resultado[0]));
                                                //Insertamos en FacturasVentasSeriesResol
                                                DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(datosFactura, Convert.ToInt32(_Resultado[2]), connection);
                                                string clPtoVta = datosFactura.PtoVta.ToString().PadLeft(5, '0') + "-" + _Resultado[2].ToString().PadLeft(8, '0');
                                                //Insertamos en FacturasCamposLibres                                                
                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura.NumSerie, datosFactura.NumFactura,
                                                    datosFactura.N, _Resultado[0], _er.Msg, _Resultado[1], _codBarra, _qr, pathLog, clPtoVta, connection);

                                                break;
                                            }
                                    }
                                }
                                AfipDll.LogFile.ErrorLog(LogFile.CreatePath(pathLog), _rta);
                                return true;
                            }
                            catch (Exception ex)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pathLog), "MTXCA. Error al Fiscalizar. " + ex.Message);
                                //Recupero el nro del ultimo comprobante.
                                Int64 _ultimoComprobanteError = AfipDll.WsMTXCA.RecuperoUltimoComprobante(pathCertificado, pathTA, pathLog,
                                    Convert.ToInt32(datosFactura.PtoVta), Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest, strPasswordSecureString, false, out _lstError);
                                //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                                if (_ultimoComprobante == _ultimoComprobanteError)
                                {
                                    //Guardamos el numero y ponemos el estado en Recuperar.
                                    //Modificamos el registro en FacturasCamposLibres.
                                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, _ultimoComprobante, ex.Message, connection);
                                    return false;
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(ptoVtaCAEA))
                                    {
                                        //Si poseemos Punto de Venta para CAEA, no hacemos la consulta y fiscalizamos directamtente.
                                        //Grabo todo con una transaccion.
                                        if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la información en el sistema. Consulte el log.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return false;
                                        }

                                        return true;
                                    }
                                    else
                                    {
                                        MessageBox.Show(new Form { TopMost = true }, "Hubo un error al conectar con el servicio de la AFIP. Intente mas tarde." + Environment.NewLine +
                                            "Error: " + ex, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }
                                }

                            }
                        }
                        else
                        {
                            string _error = "";
                            foreach (AfipDll.WsMTXCA.Errors er in _lstError)
                            {
                                _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                            }
                            //Error al recuperar el ultimo numero de factura de la AFIP
                            MessageBox.Show(new Form { TopMost = true }, "hay error en la busqueda del ultimo");
                            //Error al recuperar el ultimo numero de factura de la AFIP
                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, "", _error, "", "", connection);
                            return false;
                        }
                    }
                    catch (Exception _ex)
                    {
                        //Servicio no disponible.
                        if (!String.IsNullOrEmpty(ptoVtaCAEA))
                        {
                            //Si poseemos Punto de Venta para CAEA, no hacemos la consulta y fiscalizamos directamtente.
                            //Grabo todo con una transaccion.
                            if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la información en el sistema. Consulte el log.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }

                            return true;
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Hubo un error al conectar con el servicio de la AFIP. Intente mas tarde." + Environment.NewLine +
                                "Error: " + _ex, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }
                else
                {
                    //Servicio no disponible.
                    if (!String.IsNullOrEmpty(ptoVtaCAEA))
                    {
                        //Si poseemos Punto de Venta para CAEA, no hacemos la consulta y fiscalizamos directamtente.
                        //Grabo todo con una transaccion.
                        if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la información en el sistema. Consulte el log.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        return true;
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "El servicio de la AFIP no esta disponible en este momento. Intente mas tarde." + Environment.NewLine +
                            "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                            "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                            "Servicio de Servidor AFIP: " + _dummy.Server,
                                   "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }

            /// <summary>
            /// Metodo para fiscalizar un comprobante con MTXCA
            /// </summary>
            /// <param name="datosFactura">Clase DataAccess.DatosFactura, con los datos del Comprobante.</param>
            /// <param name="codigoIVA">Código de IVA</param>
            /// <param name="ptovtaCAE">Punto de venta de CAE</param>
            /// <param name="codigoIIBB">Codigo de IIBB</param>
            /// <param name="codVendedor">Codigo de Vendedor</param>
            /// <param name="ptoVtaCAEA">Punto de Venta CAEA</param>
            /// <param name="password">Password del certificado</param>
            /// <param name="soloCAEA">Es Solo CAEA</param>
            /// <param name="esTest">Ambiente de Test o Producción</param>
            /// <param name="generaXML">Genera XML del request y response</param>
            /// <param name="pathLog">Path del Log</param>
            /// <param name="pathCertificado">Path del certificado</param>
            /// <param name="pathTA">Path del ticket de acceso</param>
            /// <param name="Cuit">Cuit</param>
            /// <param name="connection">Conexión SQL</param>
            /// <param name="msj">mensaje de error.</param>
            /// <returns>TRUE/FALSE</returns>
            public static bool Fiscalizar(DataAccess.DatosFactura datosFactura, int codigoIVA, int codigoIIBB, string ptovtaCAE, string ptoVtaCAEA, string password,
                bool soloCAEA, bool esTest, bool generaXML, string pathLog, string pathCertificado, string pathTA, string Cuit,
                string codVendedor, SqlConnection connection, out string msj)
            {
                //string _dummy = "FALSE";
                WsMTXCA.Dummy _dummy = new WsMTXCA.Dummy();
                List<Modelos.FacturasVentasTot> _Iva = RetailService.GetTotalesIva(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.TotIva = _Iva.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIIBB, connection, datosFactura.TotalBruto);
                datosFactura.totTributos = _Tributos.Sum(x => x.TotIva);
                List<Modelos.FacturasVentasTot> _NoGravado = RetailService.GetTotalesNoGravado(datosFactura.NumSerie, datosFactura.N,
                    datosFactura.NumFactura, codigoIVA, connection);
                datosFactura.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                //Asigno el punto de venta de CAE.
                datosFactura.PtoVta = ptovtaCAE;
                //Validamos los datos de la FC.
                DataAccess.FuncionesVarias.ValidarDatosFactura(datosFactura);
                //Armamos la password como segura
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (soloCAEA)
                {
                    _dummy.Autorizaciones = "False";
                    _dummy.BaseDatos = "False";
                    _dummy.Server = "False";
                }
                else
                {
                    //Vemos si esta disponible el servicio de AFIP.
                    _dummy = WsMTXCA.TestDummy(pathLog, esTest);
                }

                if (_dummy.Autorizaciones == "OK" && _dummy.BaseDatos == "OK" && _dummy.Server == "OK")
                {
                    try
                    {
                        AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                        //_comproFc.cbu = _cbu;
                        _comproFc.codigoMoneda = "PES";
                        _comproFc.concepto = 3; //TODO de donde lo saco???
                        _comproFc.cotizacionMoneda = 1;
                        _comproFc.cuit = Cuit.Replace("-", "");
                        _comproFc.fechaDesde = datosFactura.fecha;
                        _comproFc.fechaEmision = datosFactura.fecha;  // dttFecha;
                        _comproFc.fechaHasta = datosFactura.fecha;    // dttFecha;
                        _comproFc.fechaPago = datosFactura.fecha;     // dttFecha;
                        _comproFc.fechaVto = datosFactura.fecha.AddDays(10);      // dttFecha.AddDays(10);
                        _comproFc.importeExento = 0;
                        _comproFc.importeGravado = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                        _comproFc.importeSubTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalNeto));
                        _comproFc.importeTributos = Math.Abs(datosFactura.totTributos);
                        _comproFc.NcRechazo = "SI"; //Armar como MiPyme.
                        _comproFc.numeroComprobante = 0; //Lo pongo luego de consultar el ultimo
                        _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                        _comproFc.numeroDocumento = Convert.ToInt64(datosFactura.ClienteNroDoc);
                        _comproFc.obervaciones = datosFactura.NumSerie + "-" + datosFactura.NumFactura + "-" + datosFactura.N;
                        _comproFc.ptoVta = Convert.ToInt32(datosFactura.PtoVta);
                        _comproFc.tipoComprobante = Convert.ToInt16(datosFactura.cCodAfip);
                        _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                        _comproFc.tipoDocumento = Convert.ToInt16(datosFactura.ClienteTipoDoc);
                        List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                        //Recupero el nro del ultimo comprobante.
                        Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(pathCertificado, pathTA, pathLog,
                            Convert.ToInt32(datosFactura.PtoVta), Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest,
                            strPasswordSecureString, false, out _lstError);

                        //Vemos si tenemos algun error
                        if (_lstError.Count() == 0)
                        {
                            //incremento el nro de comprobante.
                            _comproFc.numeroComprobante = _ultimoComprobante + 1;
                            //Recupero los datos de la fc.
                            Modelos.DatosFacturaExpo _datosFcExpo = RetailService.GetDatosFactura(datosFactura, connection, pathLog);

                            //Armo el string de IVA.
                            List<AfipDll.wsAfip.clsIVA> _IvaRequest = RetailService.ArmarIVA(_Iva);
                            //Items
                            List<AfipDll.wsMtxca.ItemType> _icgItems = RetailService.GetDatosItemsMtxca(datosFactura.NumSerie,
                                Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, Convert.ToInt32(datosFactura.cCodAfip),
                                _datosFcExpo, connection, pathLog);
                            //Armos el string de Tributos.
                            List<AfipDll.wsAfip.clsTributos> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                            //Armo los comprobantes asociados.
                            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = RetailService.ArmarCbteAsocMtxca(datosFactura,
                                _comproFc.tipoComprobante, pathLog, Cuit, connection);
                            //Verifico que si es una NC/ND.
                            if (Convert.ToInt32(datosFactura.cCodAfip) == 203 || Convert.ToInt32(datosFactura.cCodAfip) == 208 || Convert.ToInt32(datosFactura.cCodAfip) == 213
                                || Convert.ToInt32(datosFactura.cCodAfip) == 202 || Convert.ToInt32(datosFactura.cCodAfip) == 207 || Convert.ToInt32(datosFactura.cCodAfip) == 212
                                || Convert.ToInt32(datosFactura.cCodAfip) == 3 || Convert.ToInt32(datosFactura.cCodAfip) == 8 || Convert.ToInt32(datosFactura.cCodAfip) == 13
                                || Convert.ToInt32(datosFactura.cCodAfip) == 2 || Convert.ToInt32(datosFactura.cCodAfip) == 7 || Convert.ToInt32(datosFactura.cCodAfip) == 12)
                            {
                                //Verifico que tengo por lo menos 1 comprobante asociado.
                                if (_compAsoc[0].numeroComprobante == 0)
                                {
                                    frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_comproFc.tipoComprobante, Cuit, true);
                                    _frm.ShowDialog();
                                    _compAsoc = _frm._lstNew;
                                    _frm.Dispose();
                                    if (_compAsoc.Count == 0)
                                    {
                                        //Insertamos en FacturasCamposLibres
                                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, "Error: No posee comprobante asociado", null, null, connection);
                                        msj = "Error|Error: No posee comprobante asociado";
                                        return false;
                                    }
                                }
                            }
                            try
                            {
                                WsMTXCA.ObternerCaeResponse _rtdo = new WsMTXCA.ObternerCaeResponse();
                                AfipDll.WsMTXCA.ObtenerCAE(pathCertificado, pathTA, pathLog, Convert.ToInt32(datosFactura.PtoVta),
                                    Convert.ToInt32(datosFactura.cCodAfip), Cuit, esTest, strPasswordSecureString, generaXML, _IvaRequest, _icgItems, _TributosRequest,
                                    _comproFc, _compAsoc, out _rtdo);
                                string _rta = "";
                                switch (_rtdo.respuesta.ToUpper())
                                {
                                    case "A":
                                    case "O":
                                        {
                                            //Genero codigo de barra
                                            string _codBarra = "";
                                            string _fechaVto = _rtdo.fechaVto.Year.ToString() + _rtdo.fechaVto.Month.ToString().PadLeft(2, '0') + _rtdo.fechaVto.Day.ToString().PadLeft(2, '0');
                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(Cuit, datosFactura.cCodAfip, datosFactura.PtoVta.ToString(), _rtdo.cae.ToString(), _fechaVto, out _codBarra);
                                            //Genero el codigo del QR.
                                            string _qr = QR.CrearJson(1, datosFactura.fecha, Convert.ToInt64(Cuit), _comproFc.ptoVta, _comproFc.tipoComprobante,
                                                Convert.ToInt32(_rtdo.numeroCbte), _comproFc.importeTotal, _comproFc.codigoMoneda, _comproFc.cotizacionMoneda,
                                                _comproFc.tipoDocumento, _comproFc.numeroDocumento, "E", _rtdo.cae);
                                            //Insertamos en FacturasVentasSeriesResol
                                            DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(datosFactura, Convert.ToInt32(_rtdo.numeroCbte), pathLog, connection);
                                            //Insertamos en FacturasCamposLibres
                                            string clPtoVta = datosFactura.PtoVta.ToString().PadLeft(5, '0') + "-" + _rtdo.numeroCbte.ToString().PadLeft(8, '0');
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura.NumSerie, datosFactura.NumFactura,
                                                datosFactura.N, _rtdo.cae.ToString(), _rtdo.observacion, _fechaVto, _codBarra, _qr, pathLog, clPtoVta , connection);
                                            //Insertamos en TransaccionesAFIP
                                            IcgFceDll.CommonService.InfoTransaccionAFIP _afip = new CommonService.InfoTransaccionAFIP();
                                            _afip.cae = _rtdo.cae.ToString();
                                            _afip.codigobara = _codBarra;
                                            _afip.codigoqr = _qr;
                                            _afip.fechavto = _fechaVto;
                                            _afip.n = datosFactura.N;
                                            _afip.numero = Convert.ToInt32(datosFactura.NumFactura);
                                            _afip.serie = datosFactura.NumSerie;
                                            _afip.tipocae = "CAE";
                                            _afip.nrofiscal = Convert.ToInt32(_rtdo.numeroCbte);
                                            CommonService.TransaccionAFIP.InsertTransaccionAFIP(_afip, connection);
                                            _rta = "Observacion|" + _rtdo.observacion;
                                            if(_rtdo.respuesta.ToUpper() == "A")
                                                LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Aprovado AFIP: " + _rta);
                                            else
                                                LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Observado AFIP: " + _rta);
                                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Codigo QR: " + _qr);
                                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Codigo Barra: " + _codBarra);
                                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Resultado AFIP: CAE: " + _rtdo.cae.ToString() + " Vto: " + _fechaVto);
                                            break;
                                        }
                                    case "R":
                                        {
                                            _rta = "Error|Error AFIP: " + _rtdo.error;
                                            if (_rtdo.observacion != null)
                                                _rta = _rta + Environment.NewLine + "Error AFIP: " + _rtdo.observacion;
                                            //Insertamos en FacturasCamposLibres
                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, _rta, null, null, connection);
                                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Rechazo AFIP: " + _rta);
                                            break;
                                        }
                                }
                                msj = _rta;
                                return true;
                            }
                            catch (WebException ex)
                            {
                                if (ex.Status == WebExceptionStatus.Timeout)
                                { 
                                    LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Error: " + ex.Message); 
                                }
                                LogFile.ErrorLog(LogFile.CreatePath(pathLog), "Error: " + ex.Message);
                                //Si se produjo una Excepción, es que hubo un problema y lo sacamos por CAEA.
                                if (!String.IsNullOrEmpty(ptoVtaCAEA))
                                {
                                    //Grabo todo con una transaccion.
                                    if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                                    {
                                        msj = "Error|Se produjo un error al grabar la información en el sistema, y no se pudo otorgar CAEA";
                                        //Insertamos en FacturasCamposLibres
                                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                                        return false;
                                    }
                                    msj = "OK";
                                    return true;
                                }
                                else
                                {
                                    msj = "Error|Error: Hubo un error al conectar con el servicio de la AFIP. Intente mas tarde." + Environment.NewLine +
                                        "Error: " + ex.Message;
                                    //Insertamos en FacturasCamposLibres
                                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            string _error = "";
                            foreach (AfipDll.WsMTXCA.Errors er in _lstError)
                            {
                                _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                            }
                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, "", _error, "", "", connection);
                            msj = "Error|Error AFIP: Ocurrío un error en la AFIP al buscar el último comprobante. AFIP: " + _error;
                            return false;
                        }
                    }
                    catch (Exception _ex)
                    {
                        //Servicio no disponible.
                        if (!String.IsNullOrEmpty(ptoVtaCAEA))
                        {
                            //Grabo todo con una transaccion.
                            if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                            {
                                msj = "Error|Se produjo un error al grabar la información en el sistema, y no se pudo otorgar CAEA";
                                //Insertamos en FacturasCamposLibres
                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                                return false;
                            }
                            msj = "OK";
                            return true;
                        }
                        else
                        {
                            msj = "Error| Hubo un error al conectar con el servicio de la AFIP. Intente mas tarde." + Environment.NewLine +
                                "Error: " + _ex.Message;
                            //Insertamos en FacturasCamposLibres
                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                            return false;
                        }
                    }
                }
                else
                {
                    //Servicio no disponible.
                    if (!String.IsNullOrEmpty(ptoVtaCAEA))
                    {
                        //Si poseemos Punto de Venta para CAEA, no hacemos la consulta y fiscalizamos directamtente.
                        //Grabo todo con una transaccion.
                        if (!DataAccess.Transactions.TransactionCAEA2(datosFactura.NumSerie, datosFactura.NumFactura, datosFactura.N, codVendedor, ptoVtaCAEA, Cuit, pathLog, connection))
                        {
                            msj = "Error|Se produjo un error al grabar la información en el sistema, y no se pudo otorgar CAEA";
                            //Insertamos en FacturasCamposLibres
                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                            return false;
                        }
                        msj = "OK";
                        return true;
                    }
                    else
                    {
                        msj = "Error| El servicio de la AFIP no esta disponible en este momento. Intente mas tarde." + Environment.NewLine +
                            "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                            "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                            "Servicio de Servidor AFIP: " + _dummy.Server;
                        //Insertamos en FacturasCamposLibres
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, msj, null, null, connection);
                        return false;
                    }
                }
            }

            public static bool InformarComprobanteCAEA(DataAccess.DatosFactura datosFactura, int codigoIVA, string ptovtaCAE, string password,
                bool soloCAEA, bool esTest, bool generaXML, string pathLog, string pathCertificado, string pathTA, 
                string Cuit, SqlConnection connection)
            {
                try
                {
                    //string _dummy = "FALSE";
                    WsMTXCA.Dummy _dummy = new WsMTXCA.Dummy();

                    List<Modelos.FacturasVentasTot> _Iva = RetailService.GetTotalesIva(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection);
                    datosFactura.TotIva = _Iva.Sum(x => x.TotIva);
                    List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection, datosFactura.TotalBruto);
                    datosFactura.totTributos = _Tributos.Sum(x => x.TotIva);
                    List<Modelos.FacturasVentasTot> _NoGravado = RetailService.GetTotalesNoGravado(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection);
                    datosFactura.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                    //Asigno el punto de venta de CAE.
                    datosFactura.PtoVta = ptovtaCAE;
                    //Validamos los datos de la FC.
                    DataAccess.FuncionesVarias.ValidarDatosFactura(datosFactura);
                    //Armamos la password como segura
                    SecureString strPasswordSecureString = new SecureString();

                    foreach (char c in password) strPasswordSecureString.AppendChar(c);
                    strPasswordSecureString.MakeReadOnly();

                    //Vemos si esta disponible el servicio de AFIP.
                    _dummy = WsMTXCA.TestDummy(pathLog, esTest);
                    
                    if (_dummy.Autorizaciones == "OK" && _dummy.BaseDatos == "OK" && _dummy.Server == "OK")
                    {
                        AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                        _comproFc.codigoMoneda = "PES";
                        _comproFc.concepto = 3; //TODO de donde lo saco???
                        _comproFc.cotizacionMoneda = 1;
                        _comproFc.cuit = Cuit.Replace("-", "");
                        _comproFc.fechaDesde = datosFactura.fecha;
                        _comproFc.fechaEmision = datosFactura.fecha;  // dttFecha;
                        _comproFc.fechaHasta = datosFactura.fecha;    // dttFecha;
                        _comproFc.fechaPago = datosFactura.fecha;     // dttFecha;
                        _comproFc.fechaVto = datosFactura.fecha.AddDays(10);      // dttFecha.AddDays(10);
                        _comproFc.importeExento = 0;
                        _comproFc.importeGravado = Math.Round(Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto)), 2);
                        _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                        _comproFc.importeSubTotal = Math.Round(Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto)), 2);
                        _comproFc.importeTotal = Math.Round(Math.Abs(Convert.ToDecimal(datosFactura.TotalNeto)), 2);
                        _comproFc.importeTributos = Math.Round(Math.Abs(datosFactura.totTributos), 2);
                        _comproFc.NcRechazo = "SI"; //Armar como MiPyme.
                        _comproFc.numeroComprobante = datosFactura.NroFiscal; //Lo pongo luego de consultar el ultimo
                        _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                        _comproFc.numeroDocumento = Convert.ToInt64(datosFactura.ClienteNroDoc);
                        _comproFc.obervaciones = "";
                        _comproFc.ptoVta = Convert.ToInt16(datosFactura.PtoVta);
                        _comproFc.tipoComprobante = Convert.ToInt16(datosFactura.cCodAfip);
                        _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                        _comproFc.tipoDocumento = Convert.ToInt16(datosFactura.ClienteTipoDoc);                        

                        List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                        AfipDll.WsMTXCA.CaeaSolicitar _resultado = new WsMTXCA.CaeaSolicitar();
                        //Recupero los datos de la fc.
                        Modelos.DatosFacturaExpo _datosFcExpo = RetailService.GetDatosFactura(datosFactura, connection, pathLog);

                        //Armo el string de IVA.
                        List<AfipDll.wsAfip.clsIVA> _IvaRequest = RetailService.ArmarIVA(_Iva);
                        //Items
                        List<AfipDll.wsMtxca.ItemType> _icgItems = RetailService.GetDatosItemsMtxca(datosFactura.NumSerie,
                            Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, Convert.ToInt32(datosFactura.cCodAfip),
                            _datosFcExpo, connection, pathLog);
                        //Armos el string de Tributos.
                        List<AfipDll.wsAfip.clsTributos> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                        //Armo los comprobantes asociados.
                        List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = RetailService.ArmarCbteAsocMtxca(datosFactura,
                            _comproFc.tipoComprobante, pathLog, Cuit, connection);
                        //Verifico que si es una NC/ND.
                        if (Convert.ToInt32(datosFactura.cCodAfip) == 203 || Convert.ToInt32(datosFactura.cCodAfip) == 208 || Convert.ToInt32(datosFactura.cCodAfip) == 213
                            || Convert.ToInt32(datosFactura.cCodAfip) == 202 || Convert.ToInt32(datosFactura.cCodAfip) == 207 || Convert.ToInt32(datosFactura.cCodAfip) == 212
                            || Convert.ToInt32(datosFactura.cCodAfip) == 3 || Convert.ToInt32(datosFactura.cCodAfip) == 8 || Convert.ToInt32(datosFactura.cCodAfip) == 13
                            || Convert.ToInt32(datosFactura.cCodAfip) == 2 || Convert.ToInt32(datosFactura.cCodAfip) == 7 || Convert.ToInt32(datosFactura.cCodAfip) == 12)
                        {
                            //Verifico que tengo por lo menos 1 comprobante asociado.
                            if (_compAsoc[0].numeroComprobante == 0)
                            {
                                frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_comproFc.tipoComprobante, Cuit, true);
                                _frm.ShowDialog();
                                _compAsoc = _frm._lstNew;
                                _frm.Dispose();
                                if (_compAsoc.Count == 0)
                                {
                                    //Insertamos en FacturasCamposLibres
                                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, "Error: No posee comprobante asociado", null, null, connection);
                                    return false;
                                }
                            }
                        }
                        try
                        {
                            AfipDll.WsMTXCA.InformarComprobantesCAEA(pathCertificado, pathTA, pathLog, esTest, strPasswordSecureString,
                                _IvaRequest, _icgItems, _TributosRequest, _comproFc, datosFactura.CAE, _compAsoc,
                                true, out _lstError, out _resultado);
                            string _rta = "";
                            switch (_resultado.Resultado)
                            {
                                case "A":
                                    {
                                        //Grabo el dato en facturascamposlibres.
                                        bool _rtaFVCL = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        if (!_rtaFVCL)
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                        }
                                        //Inserto en Rem_transacciones.
                                        DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        break;
                                    }
                                case "O":
                                    {
                                        bool _rtaFVCL = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        if (!_rtaFVCL)
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                        }
                                        else
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                    ") se informo con la observacion: " + _resultado.Descripcion);
                                        }
                                        //Inserto en Rem_transacciones.
                                        DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        break;
                                    }
                                case "R":
                                    {
                                        //Informo.
                                        MessageBox.Show(new Form { TopMost = true }, "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N + Environment.NewLine +
                                            "Fue rechazado por AFIP. Error: " + _resultado.Descripcion,
                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                            ") fue rechazado por la AFIP el ser informado. ERROR: " + _resultado.Descripcion);
                                        break;
                                    }
                            }
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstError)
                            {
                                switch (_er.Code)
                                {
                                    case 1:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Resultado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Resultado: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 2:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Observación: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Observación: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 3:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Error: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Error: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 4:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Evento: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Evento: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 0:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            string[] _Resultado = _er.Msg.Split('|');
                                            break;
                                        }
                                }
                            }
                            AfipDll.LogFile.ErrorLog(LogFile.CreatePath(pathLog), _rta);
                            return true;
                        }
                        catch (Exception ex)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "MTXCA. InformarComprobanteCAEA. " + ex.Message);
                            MessageBox.Show(new Form { TopMost = true }, ex.Message);
                            return false;
                        }

                    }
                    else
                    {
                        //Servicio no disponible.
                        MessageBox.Show(new Form { TopMost = true }, "El servicio de la AFIP no esta disponible en este momento. Intente mas tarde." + Environment.NewLine +
                            "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                            "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                            "Servicio de Servidor AFIP: " + _dummy.Server,
                                   "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pathLog), "MTXCA. InformarComprobanteCAEA. " + ex.Message);
                    throw new Exception(ex.Message);
                }
            }

            public static bool InformarComprobanteCAEAPorServicio(DataAccess.DatosFactura datosFactura, int codigoIVA, string ptovtaCAEA, string password,
                bool esTest, string pathLog, string pathCertificado, string pathTA, string Cuit,
                System.Diagnostics.EventLog _Evento, SqlConnection connection, out string error)
            {
                error = "";
                try
                {
                    //string _dummy = "FALSE";
                    WsMTXCA.Dummy _dummy = new WsMTXCA.Dummy();

                    List<Modelos.FacturasVentasTot> _Iva = RetailService.GetTotalesIva(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection);
                    datosFactura.TotIva = _Iva.Sum(x => x.TotIva);
                    List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection, datosFactura.TotalBruto);
                    datosFactura.totTributos = _Tributos.Sum(x => x.TotIva);
                    List<Modelos.FacturasVentasTot> _NoGravado = RetailService.GetTotalesNoGravado(datosFactura.NumSerie, datosFactura.N,
                        datosFactura.NumFactura, codigoIVA, connection);
                    datosFactura.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                    //Asigno el punto de venta de CAE.
                    datosFactura.PtoVta = ptovtaCAEA;
                    //Validamos los datos de la FC.
                    DataAccess.FuncionesVarias.ValidarDatosFactura(datosFactura);
                    //Armamos la password como segura
                    SecureString strPasswordSecureString = new SecureString();

                    foreach (char c in password) strPasswordSecureString.AppendChar(c);
                    strPasswordSecureString.MakeReadOnly();

                    //Vemos si esta disponible el servicio de AFIP.
                    _dummy = WsMTXCA.TestDummy(pathLog, esTest);

                    if (_dummy.Autorizaciones == "OK" && _dummy.BaseDatos == "OK" && _dummy.Server == "OK")
                    {
                        AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                        //_comproFc.cbu = _cbu;
                        _comproFc.codigoMoneda = "PES";
                        _comproFc.concepto = 3; //TODO de donde lo saco???
                        _comproFc.cotizacionMoneda = 1;
                        _comproFc.cuit = Cuit.Replace("-", "");
                        _comproFc.fechaDesde = datosFactura.fecha;
                        _comproFc.fechaEmision = datosFactura.fecha;  // dttFecha;
                        _comproFc.fechaHasta = datosFactura.fecha;    // dttFecha;
                        _comproFc.fechaPago = datosFactura.fecha;     // dttFecha;
                        _comproFc.fechaVto = datosFactura.fecha.AddDays(10);      // dttFecha.AddDays(10);
                        _comproFc.importeExento = 0;
                        _comproFc.importeGravado = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                        _comproFc.importeSubTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalBruto));
                        _comproFc.importeTotal = Math.Abs(Convert.ToDecimal(datosFactura.TotalNeto));
                        _comproFc.importeTributos = Math.Abs(datosFactura.totTributos);
                        _comproFc.NcRechazo = "SI"; //Armar como MiPyme.
                        _comproFc.numeroComprobante = datosFactura.NroFiscal; //Lo pongo luego de consultar el ultimo
                        _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                        _comproFc.numeroDocumento = Convert.ToInt64(datosFactura.ClienteNroDoc);
                        _comproFc.obervaciones = "";
                        _comproFc.ptoVta = Convert.ToInt16(datosFactura.PtoVta);
                        _comproFc.tipoComprobante = Convert.ToInt16(datosFactura.cCodAfip);
                        _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                        _comproFc.tipoDocumento = Convert.ToInt16(datosFactura.ClienteTipoDoc);

                        List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                        AfipDll.WsMTXCA.CaeaSolicitar _resultado = new WsMTXCA.CaeaSolicitar();
                        //Recupero los datos de la fc.
                        Modelos.DatosFacturaExpo _datosFcExpo = RetailService.GetDatosFactura(datosFactura, connection, pathLog);

                        //Armo el string de IVA.
                        List<AfipDll.wsAfip.clsIVA> _IvaRequest = RetailService.ArmarIVA(_Iva);
                        //Items
                        List<AfipDll.wsMtxca.ItemType> _icgItems = RetailService.GetDatosItemsMtxca(datosFactura.NumSerie,
                            Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, Convert.ToInt32(datosFactura.cCodAfip),
                            _datosFcExpo, connection, pathLog);
                        //Armos el string de Tributos.
                        List<AfipDll.wsAfip.clsTributos> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                        //Armo los comprobantes asociados.
                        List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = RetailService.ArmarCbteAsocMtxca(datosFactura,
                            _comproFc.tipoComprobante, pathLog, Cuit, connection);
                        //Verifico que si es una NC/ND.
                        if (Convert.ToInt32(datosFactura.cCodAfip) == 203 || Convert.ToInt32(datosFactura.cCodAfip) == 208 || Convert.ToInt32(datosFactura.cCodAfip) == 213
                            || Convert.ToInt32(datosFactura.cCodAfip) == 202 || Convert.ToInt32(datosFactura.cCodAfip) == 207 || Convert.ToInt32(datosFactura.cCodAfip) == 212
                            || Convert.ToInt32(datosFactura.cCodAfip) == 3 || Convert.ToInt32(datosFactura.cCodAfip) == 8 || Convert.ToInt32(datosFactura.cCodAfip) == 13
                            || Convert.ToInt32(datosFactura.cCodAfip) == 2 || Convert.ToInt32(datosFactura.cCodAfip) == 7 || Convert.ToInt32(datosFactura.cCodAfip) == 12)
                        {
                            //Verifico que tengo por lo menos 1 comprobante asociado.
                            if (_compAsoc[0].numeroComprobante == 0)
                            {
                                //Insertamos en FacturasCamposLibres
                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(datosFactura, null, "Error: No posee comprobante asociado", null, null, connection);
                                return false;
                            }
                        }
                        try
                        {
                            AfipDll.WsMTXCA.InformarComprobantesCAEA(pathCertificado, pathTA, pathLog, esTest, strPasswordSecureString,
                                _IvaRequest, _icgItems, _TributosRequest, _comproFc, datosFactura.CAE, _compAsoc,
                                true, out _lstError, out _resultado);
                            string _rta = "";
                            switch (_resultado.Resultado)
                            {
                                case "A":
                                    {
                                        //Grabo el dato en facturascamposlibres.
                                        bool _rtaFVCL = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        if (!_rtaFVCL)
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                        }
                                        //Inserto en Rem_transacciones.
                                        DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        break;
                                    }
                                case "O":
                                    {
                                        bool _rtaFVCL = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        if (!_rtaFVCL)
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                        }
                                        else
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                                    ") se informo con la observacion: " + _resultado.Descripcion);
                                        }
                                        //Inserto en Rem_transacciones.
                                        DataAccess.RemTransacciones.InsertRemTransacciones(Environment.MachineName, datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, connection);
                                        break;
                                    }
                                case "R":
                                    {
                                        //Insertamos en FacturasCamposLibres
                                        DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEARechazo(datosFactura.NumSerie, Convert.ToInt32(datosFactura.NumFactura), datosFactura.N, _resultado.Descripcion, connection);
                                        //Informo.
                                        _Evento.WriteEntry("Informar Comprobantes con CAEA. "+ Environment.NewLine 
                                            +"El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N + Environment.NewLine +
                                            "Fue rechazado por AFIP. Error: " + _resultado.Descripcion);
                                        AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(pathLog), "Informar Comprobantes con CAEA. El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N +
                                            ") fue rechazado por la AFIP el ser informado. ERROR: " + _resultado.Descripcion);
                                        error = "Informar Comprobantes con CAEA. " + Environment.NewLine
                                            + "El comprobante (SERIE: " + datosFactura.NumSerie + ", NUMERO: " + datosFactura.NumFactura + ", N: " + datosFactura.N + Environment.NewLine +
                                            "Fue rechazado por AFIP. Error: " + _resultado.Descripcion;
                                        break;
                                    }
                            }
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstError)
                            {
                                switch (_er.Code)
                                {
                                    case 1:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Resultado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Resultado: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 2:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Observación: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Observación: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 3:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Error: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Error: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 4:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "Evento: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "Evento: " + _er.Msg + Environment.NewLine;
                                            break;
                                        }
                                    case 0:
                                        {
                                            if (String.IsNullOrEmpty(_rta))
                                                _rta = "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            else
                                                _rta = _rta + "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                            string[] _Resultado = _er.Msg.Split('|');
                                            break;
                                        }
                                }
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pathLog), "MTXCA. InformarComprobanteCAEA. Error: " + ex.Message);
                            _Evento.WriteEntry("Se produjo el siguiente error al intentar informar un comprobante." + Environment.NewLine +
                                "Error: " + ex.Message);
                            error = "Se produjo el siguiente error al intentar informar un comprobante." + Environment.NewLine +
                                "Error: " + ex.Message;
                            return false;
                        }

                    }
                    else
                    {
                        //Servicio no disponible.
                        _Evento.WriteEntry("El servicio de la AFIP para informar CAEA no esta disponible en este momento." + Environment.NewLine +
                            "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                            "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                            "Servicio de Servidor AFIP: " + _dummy.Server);
                        LogFile.ErrorLog(LogFile.CreatePath(pathLog), "El servicio de la AFIP para informar CAEA no esta disponible en este momento." + Environment.NewLine +
                            "Servicio de Autorizaciones AFIP: " + _dummy.Autorizaciones + Environment.NewLine +
                            "Servicio de Base de Datos AFIP: " + _dummy.BaseDatos + Environment.NewLine +
                            "Servicio de Servidor AFIP: " + _dummy.Server);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    _Evento.WriteEntry("MTXCA. InformarComprobanteCAEA. Error: " + ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }
        /// <summary>
        /// Servicios Comunes.
        /// </summary>
        public class Common
        {
            /// <summary>
            /// Valida el ultimo comprobante con Numero en ICG, para un punto de venta y tipo de comprobante.
            /// </summary>
            /// <param name="_ptoVta">Punto de venta</param>
            /// <param name="_numeroFiscal">Numero fiscal a validar</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante</param>
            /// <param name="_con">Conexion SQL</param>
            /// <param name="_ultimoICG">Ultimo numero fiscal en ICG</param>
            /// <returns>True si existe, False en caso contrario.</returns>
            public static bool ValidarUltimoNroComprobante(string _ptoVta, long _numeroFiscal, string _tipoComprobante, SqlConnection _con, out long _ultimoICG)
            {
                bool _rta = false;
                try {
                    string _sql = "SELECT max(NUMEROFISCAL) as ult FROM FACTURASVENTASERIESRESOL WHERE SUBSTRING(SERIEFISCAL2,1,3) = @tipoCbte and Cast(SERIEFISCAL1 as int) = @ptoVta";
                    _ultimoICG = 0;
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        //Parametros.
                        _cmd.Parameters.AddWithValue("@tipoCbte", _tipoComprobante);
                        //_cmd.Parameters.AddWithValue("@ptoVta", _ptoVta.PadLeft(5,'0'));
                        _cmd.Parameters.AddWithValue("@ptoVta", Convert.ToInt32(_ptoVta));
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    _ultimoICG = String.IsNullOrEmpty(reader["ult"].ToString()) ? 0 : Convert.ToInt32(reader["ult"].ToString());
                                }
                            }
                        }
                        if (_ultimoICG == _numeroFiscal)
                            _rta =true;
                        else
                            _rta = false;
                    }
                }
                catch (Exception ex)
                { throw new Exception("ValidarUltimoNroComprobante Exception = " + ex.Message); }
                return _rta;
            }
            /// <summary>
            /// Valida el ultimo comprobante con Numero en ICG, para un punto de venta y tipo de comprobante.
            /// </summary>
            /// <param name="_ptoVta">Punto de venta</param>
            /// <param name="_numeroFiscal">Numero fiscal a validar</param>
            /// <param name="_tipoComprobante">Tipo de Comprobante</param>
            /// <param name="_connectionString">Conection String</param>
            /// <param name="_ultimoICG">Ultimo numero fiscal en ICG</param>
            /// <returns>True si existe, False en caso contrario.</returns>
            public static bool ValidarUltimoNroComprobante(string _ptoVta, long _numeroFiscal, string _tipoComprobante, string _connectionString, out long _ultimoICG)
            {
                bool _rta = false;
                try
                {
                    string _sql = "SELECT max(NUMEROFISCAL) as ult FROM FACTURASVENTASERIESRESOL WHERE SUBSTRING(SERIEFISCAL2,1,3) = @tipoCbte and Cast(SERIEFISCAL1 as int) = @ptoVta";
                    _ultimoICG = 0;
                    using (SqlConnection _connection = new SqlConnection(_connectionString))
                    {
                        _connection.Open();
                        using (SqlCommand _cmd = new SqlCommand())
                        {
                            _cmd.Connection = _connection;
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.CommandText = _sql;
                            //Parametros.
                            _cmd.Parameters.AddWithValue("@tipoCbte", _tipoComprobante);
                            _cmd.Parameters.AddWithValue("@ptoVta", _ptoVta);
                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _ultimoICG = String.IsNullOrEmpty(reader["ult"].ToString()) ? 0 : Convert.ToInt32(reader["ult"].ToString());
                                    }
                                }
                            }
                            if (_ultimoICG == _numeroFiscal)
                                _rta = true;
                            else
                                _rta = false;
                        }
                    }
                }
                catch (Exception ex)
                { throw new Exception("ValidarUltimoNroComprobante Exception = " + ex.Message); }
                return _rta;
            }
            /// <summary>
            /// Funcion que valida la existencia de los campos libres enla tabla TIQUETSVENTACAMPOSLIBRES
            /// </summary>
            /// <param name="_connection">Conexión SQL</param>
            /// <returns>Mensajes de error a mostrar</returns>
            public static string ValidarColumnasCamposLibres(SqlConnection _connection)
            {
                string _msj = "";
                try
                {
                    if (_connection.State == System.Data.ConnectionState.Closed)
                        _connection.Open();

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "COD_BARRAS", _connection))
                    {
                        _msj = _msj + "No existe la Columna COD_BARRAS en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "CODIGO_QR", _connection))
                    {
                        _msj = _msj + "No existe la Columna CODIGO_QR en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "CODIGO_QR", _connection))
                    {
                        _msj = _msj + "No existe la Columna CODIGO_QR en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "ERROR_CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna ERROR_CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "ESTADO_FE", _connection))
                    {
                        _msj = _msj + "No existe la Columna ESTADO_FE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "VTO_CAE", _connection))
                    {
                        _msj = _msj + "No existe la Columna VTO_CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_NRO", _connection))
                    {
                        _msj = _msj + "No existe la Columna Z_NRO en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "PTO_VTA", _connection))
                    {
                        _msj = _msj + "No existe la Columna PTO_VTA en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "CAEA_INFORMADO", _connection))
                    {
                        _msj = _msj + "No existe la Columna CAEA_INFORMADO en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error validando CamposLibres. " + ex.Message);
                }
                return _msj;
            }
            public static bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
            {
                string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlConn;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@table", _table);
                    _cmd.Parameters.AddWithValue("column", _columna);

                    if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                        _rta = true;
                }
                return _rta;
            }

            public static bool ExistenComprobantesConFechaAnterior(DateTime fechaComprobante, string _caja, SqlConnection conexion)
            {
                bool rta = false;
                string sql = @"SELECT FACTURASVENTA.FECHA
                    from FACTURASVENTA 
                    inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    inner join ALBVENTACAB on FACTURASVENTACAMPOSLIBRES.NUMSERIE = ALBVENTACAB.NUMSERIEFAC AND 
                    FACTURASVENTACAMPOSLIBRES.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTACAMPOSLIBRES.N = ALBVENTACAB.NFAC 
                    left join FACTURASVENTASERIESRESOL on ALBVENTACAB.NUMSERIEFAC = FACTURASVENTASERIESRESOL.NUMSERIE 
                    and ALBVENTACAB.NUMFAC = FACTURASVENTASERIESRESOL.NUMFACTURA
                    and ALBVENTACAB.NFAC = FACTURASVENTASERIESRESOL.N
                    INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC 
                    INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE 
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTACAMPOSLIBRES.NUMSERIE = SERIESCAMPOSLIBRES.SERIE 
                    WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND (FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') 
                    AND (FACTURASVENTA.CAJA = @caja)
                    AND (FACTURASVENTACAMPOSLIBRES.CAE is null) AND CLIENTES.REGIMFACT <> 'N' AND ALBVENTACAB.N = 'B' 
                    ORDER BY FACTURASVENTA.FECHA";

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conexion;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@caja", _caja);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime fecha = Convert.ToDateTime(reader["FECHA"]);
                                if (fechaComprobante.Date > fecha.Date)
                                    return true;
                            }
                        }
                    }
                }
                return rta;
            }

            /// <summary>
            /// Transaccion para cambiar el numero fiscal de un comprobante con CAEA.
            /// </summary>
            /// <param name="_nFactura">N de Factura de ICG</param>
            /// <param name="_nroFiscal">Nuevo numero fiscal para el comprobante.</param>
            /// <param name="_numeroFactura">Numero de Factura de ICG.</param>
            /// <param name="_pathLog">Directorio de LOG.</param>
            /// <param name="_serieFactura">Serie de Factura de ICG.</param>
            /// <param name="_sql">Conexion SQL.</param>
            public static void TransaccionChangeNroFiscal(string _serieFactura, int _numeroFactura, string _nFactura,
                int _nroFiscal, string _pathLog, SqlConnection _sql)
            {
                SqlTransaction _tran;
                //Comienzo la transaccion
                _tran = _sql.BeginTransaction();
                try
                {
                    //Grabo FacturasVentaSerieResol.
                    FacturasVentaSeriesResol.UpdateNroFiscalFacturasVentaSeriesResol(_serieFactura, _numeroFactura, _nFactura, _nroFiscal, _pathLog, _sql, _tran);
                    //Inserto en Rem_transacciones.
                    //Grabo RemTransacciones. Enviamos los cambios a central.
                    RemTransacciones.InsertRemTransacciones(Environment.MachineName, _serieFactura, _numeroFactura, _nFactura, 1, _sql, _tran);
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack. Error: " + ex.Message);
                }
            }

            public static void GraboSaltoCleanCashControlCode(string _serieFactura, int _numeroFactura, string _textoFaltante, SqlConnection _connection)
            {
                
            }
        }
        /// <summary>
        /// Metodos para la facturación comun
        /// </summary>
        public class FceComun
        {
            public class Resultado
            {
                public bool afipOK { get; set; }
                public string cae { get; set; }
                public string fechaVto { get; set; }
                public List<string> message { get; set; }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="_pathCertificado">Directorio del cerificado.</param>
            /// <param name="_pathTa">Directorio del Ticket de Acceso.</param>
            /// <param name="_pathLog">Directorio del LOG</param>
            /// <param name="_cuit">CUIT del Emisor.</param>
            /// <param name="_Fc">Datos de la Factura.</param>
            /// <param name="_iva">Lista de IVA.</param>
            /// <param name="_tributos">Lista de Tributos.</param>
            /// <param name="_esTest">Boleano para indicar si es TEST o Produccion.</param>
            /// <param name="_passwordSecureString">Password.</param>
            /// <param name="_connection">Conexion SQL.</param>
            public static void FiscalizarAFIP(string _pathCertificado, string _pathTa, string _pathLog,string _cuit,
                DataAccess.DatosFactura _Fc, 
                List<DataAccess.FacturasVentasTot> _iva, List<Modelos.FacturasVentasTot> _tributos,
                bool _esTest,
                SecureString _passwordSecureString,
                SqlConnection _connection,
                out Resultado _resultado)
            {
                _resultado = new Resultado();
                int intNroComprobante = 0;
                string _Error;
                List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                //Recupero el nro del ultimo comprobante.
                wsAfip.UltimoComprobante _ultimo = wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTa, _pathLog,
                    Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), _cuit, _esTest, _passwordSecureString, out _lstErrores);
                if (_lstErrores.Count() == 0)
                {
                    long _ultimoICG = 0;
                    bool _estoyOkAfip = RetailService.Common.ValidarUltimoNroComprobante(_Fc.PtoVta, _ultimo.CbteNro, _Fc.cCodAfip, _connection, out _ultimoICG);
                    if (!_estoyOkAfip)
                    {
                        string _error = "VAL-ICG: El último Número de AFIP no se corresponde con el ultimo en ICG." + Environment.NewLine +
                            "Ultimo informado por AFIP: " + _ultimo.CbteNro.ToString() +
                            Environment.NewLine + "Ultimo informado por ICG: " + _ultimoICG.ToString() +
                            Environment.NewLine + "Por favor Verifique que el comprobante no este anulado";
                        MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Último AFIP ->" + _ultimo.CbteNro.ToString());
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Último ICG ->" + _ultimoICG.ToString());
                        string texto = "Último AFIP ->" + _ultimo.CbteNro.ToString() + " - Último ICG ->" + _ultimoICG.ToString();
                        if (FacturasVenta.UpdateCleanCashControlCode(_Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), texto, _connection))
                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al grabar el CleanCashControlCode1");
                    }
                    //incremento el nro de comprobante.
                    intNroComprobante = _ultimo.CbteNro + 1;
                    //Armo el string de la cabecera.
                    wsAfip.clsCabReq _cabecera = new wsAfip.clsCabReq();
                    _cabecera.CantReg = 1;
                    _cabecera.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                    _cabecera.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                    string strCabReg = "1|" + _Fc.cCodAfip + "|" + _Fc.PtoVta;
                    //Cargamos la clase Detalle.
                    wsAfip.clsDetReq _detalle = DataAccess.FuncionesVarias.CargarDetalle(_Fc, intNroComprobante);
                    //Armo el string de IVA.
                    List<wsAfip.clsIVA> _IvaRequest = DataAccess.FuncionesVarias.ArmarIVA(_iva);
                    //Armos el string de Tributos.
                    List<wsAfip.clsTributos> _TributosRequest = DataAccess.FuncionesVarias.ArmaTributos(_tributos);
                    //Armamos la lista de comprobantes Asociados.
                    List<AfipDll.wsAfip.ComprobanteAsoc> _ComprobantesAsociados = ArmarCbteAsoc(_Fc, _cuit, _connection);
                    //Verifico que si es una NC/ND.
                    if (Convert.ToInt32(_Fc.cCodAfip) == 203 || Convert.ToInt32(_Fc.cCodAfip) == 208 || Convert.ToInt32(_Fc.cCodAfip) == 213
                        || Convert.ToInt32(_Fc.cCodAfip) == 202 || Convert.ToInt32(_Fc.cCodAfip) == 207 || Convert.ToInt32(_Fc.cCodAfip) == 212
                        || Convert.ToInt32(_Fc.cCodAfip) == 3 || Convert.ToInt32(_Fc.cCodAfip) == 8 || Convert.ToInt32(_Fc.cCodAfip) == 13
                        || Convert.ToInt32(_Fc.cCodAfip) == 2 || Convert.ToInt32(_Fc.cCodAfip) == 7 || Convert.ToInt32(_Fc.cCodAfip) == 12)
                    {
                        //Verifico que tengo por lo menos 1 comprobante asociado.
                        if (_ComprobantesAsociados.Count() == 0)
                        {
                            frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(Convert.ToInt32(_Fc.cCodAfip), _cuit, false);
                            _frm.ShowDialog();
                            _ComprobantesAsociados = _frm._lstNewComun;
                            _frm.Dispose();
                            if (_ComprobantesAsociados.Count == 0)
                            {
                                //Insertamos en FacturasCamposLibres
                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc, null, "Error: No posee comprobante asociado", null, null, _connection);
                                //MessageBox.Show(new Form { TopMost = true }, "No posee comprobante asociado, debe ingresar uno." + Environment.NewLine +
                                //    "Fiscalicelo desde la consola e ingrese el comprobante asociado.",
                                //        "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                List<string> _msg = new List<string>();
                                _msg.Add("Error: No posee comprobante asociado");
                                _resultado = new Resultado { afipOK = true, cae = "", fechaVto = "", message = _msg };
                                return;
                            }
                        }
                    }

                    //Armamos los Opcionales.
                    List<AfipDll.wsAfip.Opcionales> _opcionales = new List<AfipDll.wsAfip.Opcionales>();

                    try
                    {
                        wsAfip.clsCaeResponse _Respuesta = wsAfip.ObtenerDatosCAE(_cabecera, _detalle, _TributosRequest, _IvaRequest,
                            _ComprobantesAsociados, _pathCertificado, _pathTa, _pathLog, _cuit, _esTest);
                        //
                        string strCodBarra = "";
                        string _codigoQR = "";
                        string _fechaVto = "";
                        switch (_Respuesta.Resultado)
                        {
                            case "A":
                            case "P":
                                {
                                    DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _cabecera.CbteTipo.ToString(), _cabecera.PtoVta.ToString(), _Respuesta.Cae, _Respuesta.FechaVto, out strCodBarra);
                                    _codigoQR = QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit), _cabecera.PtoVta, _cabecera.CbteTipo, Convert.ToInt32(_detalle.CbteDesde), Convert.ToDecimal(_detalle.ImpTotal), _detalle.MonID, Convert.ToDecimal(_detalle.MonCotiz), _detalle.DocTipo, _detalle.DocNro, "E", Convert.ToInt64(_Respuesta.Cae));
                                    _fechaVto = _Respuesta.FechaVto;
                                    //Insertamos en TransaccionesAFIP
                                    IcgFceDll.CommonService.InfoTransaccionAFIP _afip = new CommonService.InfoTransaccionAFIP();
                                    _afip.cae = _Respuesta.Cae.ToString();
                                    _afip.codigobara = strCodBarra;
                                    _afip.codigoqr = _codigoQR;
                                    _afip.fechavto = _fechaVto;
                                    _afip.n = _Fc.N;
                                    _afip.numero = Convert.ToInt32(_Fc.NumFactura);
                                    _afip.serie = _Fc.NumSerie;
                                    _afip.tipocae = "CAE";
                                    _afip.nrofiscal = Convert.ToInt32(_detalle.CbteDesde);
                                    _afip.ptoVta = _cabecera.PtoVta.ToString().PadLeft(5, '0');
                                    CommonService.TransaccionAFIP.InsertTransaccionAFIP(_afip, _connection);
                                    List<string> _msg = new List<string>();
                                    _resultado = new Resultado { afipOK = true, cae = _Respuesta.Cae, fechaVto = _Respuesta.FechaVto, message = _msg };
                                    break;
                                }
                            case "R":
                                {
                                    List<string> _msg = new List<string>();
                                    if (_Respuesta.ErrorCode == "10040")
                                    {
                                        //Limpio el dato del Abono.
                                        DataAccess.ComprobanteAsociado.DeleteComprobantesAsociados(_Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _Fc.N, _connection);
                                        _msg.Add(_Respuesta.ErrorCode + "|La Tipo factura asociada no se corresponde con el Tipo de nota de Credito. Por favor fiscalicela de nuevo." );
                                        _resultado = new Resultado { afipOK = true, cae = "", fechaVto = "", message = _msg };
                                        break;
                                    }                                    
                                    _msg.Add(_Respuesta.ErrorCode + "|" + _Respuesta.ErrorMsj);
                                    _resultado = new Resultado { afipOK = true, cae = "", fechaVto = "", message = _msg };
                                    break;
                                }
                        }
                        //Insertamos en FacturasCamposLibres
                        string clPtoVta = _cabecera.PtoVta.ToString().PadLeft(5, '0') + "-" + _detalle.CbteDesde.ToString().PadLeft(8, '0');
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc.NumSerie, _Fc.NumFactura,
                            _Fc.N, _Respuesta.Cae.ToString(), _Respuesta.ErrorMsj, _fechaVto, strCodBarra, _codigoQR, _pathLog, clPtoVta, _connection);
                        //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.                                            
                        if (!String.IsNullOrEmpty(_Respuesta.Cae))
                        {
                            DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_Fc, intNroComprobante, _connection);
                        }
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.Retail_InsertRemTransacciones(_terminal, _Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _Fc.N, 1, _connection);
                    }
                    catch (Exception ex)
                    {
                        //Recupero el nro del ultimo comprobante.
                        int intUltimoNro = wsAfip.RecuperoUltimoComprobante(_pathCertificado, _pathTa, _pathLog, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), _cuit, _esTest, out _Error);
                        //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                        if (intUltimoNro == intNroComprobante)
                        {
                            //Guardamos el numero y ponemos el estado en Recuperar.
                            //Modificamos el registro en FacturasCamposLibres.
                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc, intNroComprobante, ex.Message, _connection);
                        }
                        else
                        {
                            //if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                            //{
                            //    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_Fc.NumSerie, _Fc.NumFactura, _Fc.N, _codVendedor, _connection);
                            //    string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                            //    string _codBarra;
                            //    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _tipoComprobante.Substring(0, 3), _connection);
                            //    //Obtengo el CAEA para el periodo en curso.
                            //    CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                            //    //Genero el codigo de barras.
                            //    DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _cabecera.CbteTipo.ToString(), _cabecera.PtoVta.ToString(), _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                            //    //Grabo todo con una transaccion.
                            //    if (!DataAccess.Transactions.TransactionCAEA2(_Fc.NumSerie, _Fc.NumFactura, _Fc.N, _codVendedor, _PtoVtaCAEA, _cuit, _pathLog, _connection))
                            //        MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la informacion en el sistema. Consulte el log.",
                            //            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                            //}

                        }
                        List<string> _msg = new List<string>();
                        _msg.Add(ex.Message);
                        _resultado = new Resultado { afipOK = false, cae = "", fechaVto = "", message = _msg };
                    }
                }
                else
                {
                    //MessageBox.Show(new Form { TopMost = true }, "hay error en la busqueda del ultimo");
                    //Error al recuperar el ultimo numero de factura de la AFIP
                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc, "", DataAccess.FuncionesVarias.GetStringFromErrors(_lstErrores), "", "", _connection);
                    _resultado = new Resultado { afipOK = false, cae = "", fechaVto = "", message = DataAccess.FuncionesVarias.GetListFromErrors(_lstErrores) };
                }
            }
            /// <summary>
            /// Realiza la transaccion con CAEA
            /// </summary>
            /// <param name="_serie">Serie</param>
            /// <param name="_numero">Numero</param>
            /// <param name="_n">N</param>
            /// <param name="_codvendedor">Codigo del vendedor</param>
            /// <param name="_ptovtacaea">Punto de venta para CAEA</param>
            /// <param name="_cuit">CUIT del emisor</param>
            /// <param name="_pathlog">Directorio del Log</param>
            /// <param name="_Fc">Clase de los datos de la factura.</param>
            /// <param name="_connection">Conexion SQL</param>
            /// <param name="_message">Mensaje de error</param>
            /// <returns>True/False</returns>
            public static bool TransactionCAEA(string _serie, string _numero, string _n, string _codvendedor,
                string _ptovtacaea, string _cuit, string _pathlog, 
                DataAccess.DatosFactura _Fc, 
                SqlConnection _connection, 
                out string _message)
            {
                _message = "";
                try
                {
                    if (_connection.State == System.Data.ConnectionState.Closed)
                        _connection.Open();
                    //Recupero el tipo de comprobante.
                    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codvendedor, _connection);
                    //Valido los datos del comprobante.
                    if (String.IsNullOrEmpty(_tipoComprobante))
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No está definido el tipo de comprobante en TIPOSDOC.DESCRIPCION.");
                        _message = "No está definido el tipo de comprobante en TIPOSDOC.DESCRIPCION." + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        return false;
                    }

                    string _codBarra;
                    //Recupero el contador.
                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection);
                    //Valido que tengo el Contador de CAEA.
                    if (_dtoContador.serieresol == null)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No se encontró un contador definido para el punto de Venta: " + _ptovtacaea);
                        _message = "No se encontró un contador definido para el punto de Venta: " + _ptovtacaea + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        return false;
                    }
                    //Obtengo el CAEA para el periodo en curso.
                    CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                    if (String.IsNullOrEmpty(_caeaEnCurso.Caea))
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No se encontró un CAEA para el periodo del comprobante.");
                        _message = "No se encontró un CAEA para el periodo del comprobante." + Environment.NewLine +
                            "Tramite uno desde la consola y luego intente fiscalizarlo." + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        return false;
                    }
                    //Armo la fecha
                    string _fechaVto = _caeaEnCurso.FechaTope.Year.ToString().PadLeft(4, '0') + _caeaEnCurso.FechaTope.Month.ToString().PadLeft(2, '0') + _caeaEnCurso.FechaTope.Day.ToString().PadLeft(2, '0');
                    //Genero el codigo de barras.
                    DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _tipoComprobante.Substring(0, 3), _ptovtacaea, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                    //Genero el codigo del QR.
                    string _qr = QR.CrearJson(1, DateTime.Now, Convert.ToInt64(_cuit), Convert.ToInt32(_ptovtacaea),
                        Convert.ToInt32(_tipoComprobante.Substring(0, 3)),
                        _dtoContador.contador + 1, Convert.ToDecimal(_Fc.TotalNeto), "PES", 1,
                        Convert.ToInt32(_Fc.ClienteTipoDoc), Convert.ToInt64(_Fc.ClienteNroDoc), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                    //
                    string clPtoVta = _ptovtacaea.PadLeft(5, '0') + "-" + (_dtoContador.contador + 1).ToString().PadLeft(8, '0');
                    SqlTransaction _trn;
                    _trn = _connection.BeginTransaction();
                    try
                    {
                        //Modificamos el registro en FacturasCamposLibres.
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serie, _numero, _n, _caeaEnCurso.Caea,
                            "CAEA", _fechaVto, _codBarra, _qr, clPtoVta, _connection, _trn);
                        //insertamos el comprobante de FacturasVentasSeriesResol
                        DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_serie, _numero, _n, _ptovtacaea, _tipoComprobante, _dtoContador.contador + 1, _connection, _trn);
                        //Insertamos en TransaccionesAFIP
                        CommonService.InfoTransaccionAFIP _afip = new CommonService.InfoTransaccionAFIP();
                        _afip.cae = _caeaEnCurso.Caea;
                        _afip.codigobara = _codBarra;
                        _afip.codigoqr = _qr;
                        _afip.fechavto = _fechaVto;
                        _afip.n = _n;
                        _afip.numero = Convert.ToInt32(_numero);
                        _afip.serie = _serie;
                        _afip.tipocae = "CAEA";
                        _afip.nrofiscal = _dtoContador.contador + 1;
                        if (!CommonService.TransaccionAFIP.InsertTransaccionAFIP(_afip, _connection, _trn))
                            LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "InsertTransaccionAFIP con CAEA, no se ejecuto correctamente. No grabo.");
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, 1, _connection, _trn);
                        //Modifico el contador.
                        DataAccess.FuncionesVarias.UpdateContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection, _trn);
                        //Comiteo, limpio el mensaje y retorno
                        _trn.Commit();
                        _message = "";
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //Se produjo un error, Rollback.
                        _trn.Rollback();
                        //Log
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: " + ex.Message);
                        _message = "Se produjo el siguiente Error: " + ex.Message;
                        return false;
                    }
                }
                catch (Exception e)
                {
                    //Log
                    LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: " + e.Message);
                    _message = "Se produjo el siguiente Error: " + e.Message;
                    return false;
                }
            }
            /// <summary>
            /// Transaccion para las ventas en negro
            /// </summary>
            /// <param name="_cab">Clase con los datos de la Factura.</param>
            /// <param name="_sql">Conexion SQL.</param>
            public static void TransaccionBlack(DatosFactura _cab, SqlConnection _sql)
            {
                string _serieFiscal1 = "0000";
                string _serieFiscal2 = "";
                int _numeroFiscal = -1;

                SqlTransaction _tran;
                //Comienzo la transaccion
                _tran = _sql.BeginTransaction();
                try
                {
                    FacturasVentaSeriesResol _fvsr = new FacturasVentaSeriesResol
                    {
                        N = _cab.N,
                        NumSerie = _cab.NumSerie,
                        NumFactura = Convert.ToInt32(_cab.NumFactura),
                        NumeroFiscal = _numeroFiscal,
                        SerieFiscal1 = _serieFiscal1,
                        SerieFiscal2 = _serieFiscal2
                    };
                    //Grabo FacturasVentaSerieResol.
                    FacturasVentaSeriesResol.GrabarTiquetNew(_fvsr, _sql, _tran);
                    //Inserto en Rem_transacciones.
                    //Grabo RemTransacciones. Enviamos los cambios a central.
                    RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.NumSerie, Convert.ToInt32(_cab.NumFactura), _cab.N, 1, _sql, _tran);
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack. Error: " + ex.Message);
                }
            }

            /// <summary>
            /// Transaccion de Venta Manual
            /// </summary>
            /// <param name="_serie">Serie</param>
            /// <param name="_numero">Numero</param>
            /// <param name="_n">N</param>
            /// <param name="_codvendedor">Codigo del vendendor.</param>
            /// <param name="_ptovta">Punto de venta Manual</param>
            /// <param name="_nroCbte">Nro de comprobante.</param>
            /// <param name="_cuit">Cuit del Emisor.</param>
            /// <param name="_pathlog">Directorio del Log.</param>
            /// <param name="_connection">Conexión SQL.</param>
            /// <returns>True/False</returns>
            public static bool TransactionManual(string _serie, string _numero, string _n, string _codvendedor,
                string _ptovta, int _nroCbte, string _cuit, string _pathlog, SqlConnection _connection)
            {
                //obtengo el tipo de comprobante.
                string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codvendedor, _connection);
                //armo la fecha de vto como la de hoy.
                string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                //Recupero el contador.
                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_ptovta, _tipoComprobante.Substring(0, 3), _connection);
                //
                string clPtoVta = _ptovta.PadLeft(5, '0') + "-" + (_dtoContador.contador + 1).ToString().PadLeft(8, '0');
                SqlTransaction _trn;
                _trn = _connection.BeginTransaction();
                try
                {
                    //Modificamos el registro en FacturasCamposLibres.
                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serie, _numero, _n, "MANUAL", "", _fechaVto, "", "", clPtoVta, _connection, _trn);
                    //insertamos el comprobante de FacturasVentasSeriesResol
                    DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_serie, _numero, _n, _ptovta, _tipoComprobante, _nroCbte, _connection, _trn);
                    //Recupero el nombre de la terminal.
                    string _terminal = Environment.MachineName;
                    //Inserto en Rem_transacciones.
                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, _connection, _trn);
                    //Modifico el contador.
                    DataAccess.FuncionesVarias.UpdateContador(_ptovta, _tipoComprobante.Substring(0, 3), _nroCbte, _connection, _trn);
                    //Comiteo
                    _trn.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    //Se produjo un error, Rollback.
                    _trn.Rollback();
                    //Log
                    LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionManual Error: " + ex.Message);
                    return false;
                    //throw new Exception(ex.Message);
                }
            }
            /// <summary>
            /// Metodo para informar los Comprobantes con CAEA.
            /// </summary>
            /// <param name="_serieComp">Serie</param>
            /// <param name="_numeroComp">Numero</param>
            /// <param name="_nComp">N</param>
            /// <param name="_codigoIVA">Codigo de IVA</param>
            /// <param name="_codigoIIBB">Codigo de IIBB</param>
            /// <param name="_ptoVtaCAEA">Punto de venta de CAEA.</param>
            /// <param name="_password">Password del certificado.</param>
            /// <param name="_cuit">CUIT del Emisor.</param>
            /// <param name="_pathLog">Directorio del Log</param>
            /// <param name="_pathCertificado">Directorio del Certificado.</param>
            /// <param name="_pathTaFC">Directorio del Tiquet de Acceso</param>
            /// <param name="_esTest">True/False para indicar si se trabaja con el ambiente de homologacion o Produccion</param>
            /// <param name="_isService">True/False para indicar si el metodo es invocado desde un servicio.</param>
            /// <param name="_connection">Conección SQL.</param>
            /// <returns></returns>
            public static bool InformarComprobantesCAEA(string _serieComp, int _numeroComp, string _nComp,
                int _codigoIVA, int _codigoIIBB, string _ptoVtaCAEA, string _password, string _cuit,
                string _pathLog, string _pathCertificado, string _pathTaFC,
                bool _esTest, bool _isService,
                SqlConnection _connection)
            {
                bool _rta = false;
                //Factura Electronica comun.
                //DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(_serieComp, _numeroComp, _nComp, _connection);
                DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFacturaRetail(_serieComp, _numeroComp, _nComp, _ptoVtaCAEA, _connection);
                // Array de IVA
                List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                //Array de tributos
                List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIIBB, _connection, _Fc.TotalBruto);
                _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                //Array de Importes no gravados.
                List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                //Asigno el punto de venta de CAE.
                _Fc.PtoVta = _ptoVtaCAEA;
                //Validamos los datos de la FC.
                DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);
                //Armamos la password como segura
                SecureString strPasswordSecureString = new SecureString();
                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();
                //Vemos si esta disponible el servicio de AFIP.
                string _dummy = wsAfip.TestDummy(_pathLog, _esTest);
                if (_dummy.ToUpper() == "OK")
                {
                    List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                    //Armo el string de la cabecera.
                    AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                    _cabReq.CantReg = 1;
                    _cabReq.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                    _cabReq.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                    string strCabReg = "1|" + _Fc.cCodAfip.ToString() + "|" + _Fc.PtoVta.ToString();
                    //Cargamos la clase Detalle.
                    List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = RetailService.CargarDetalleCAEAAfip(_Fc, _Fc.NroFiscal, _Fc.CAE);
                    //Armo el string de IVA.
                    List<AfipDll.wsAfipCae.AlicIva> _lstIva = RetailService.ArmarIVAAfip(_Iva);
                    //Armos el string de Tributos.
                    List<AfipDll.wsAfipCae.Tributo> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                    //Verifico que si es una NC/ND.
                    if (Convert.ToInt32(_Fc.cCodAfip) == 203 || Convert.ToInt32(_Fc.cCodAfip) == 208 || Convert.ToInt32(_Fc.cCodAfip) == 213
                        || Convert.ToInt32(_Fc.cCodAfip) == 202 || Convert.ToInt32(_Fc.cCodAfip) == 207 || Convert.ToInt32(_Fc.cCodAfip) == 212
                        || Convert.ToInt32(_Fc.cCodAfip) == 3 || Convert.ToInt32(_Fc.cCodAfip) == 8 || Convert.ToInt32(_Fc.cCodAfip) == 13
                        || Convert.ToInt32(_Fc.cCodAfip) == 2 || Convert.ToInt32(_Fc.cCodAfip) == 7 || Convert.ToInt32(_Fc.cCodAfip) == 12)
                    {

                        //Armamos la lista de comprobantes Asociados.
                        List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RetailService.ArmarCbteAsocCAEA(_Fc, _cuit, _connection);
                        //Verifico que tengo por lo menos 1 comprobante asociado.
                        if (_ComprobantesAsociados.Count() == 0)
                        {
                            DateTime _dtDesde = _Fc.fecha.AddDays(-30);
                            string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                            string _hasta = _Fc.fecha.Year.ToString().PadLeft(2, '0') + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                            AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                            _detReq[0].PeriodoAsoc = _cls;
                        }
                        else
                        {
                            _detReq[0].CbtesAsoc = _ComprobantesAsociados.ToArray();
                        }
                    }
                    //Agregamos el IVA
                    _detReq[0].Iva = _lstIva.ToArray();
                    //Agregamos los tributos.
                    if (_TributosRequest.Count > 0)
                        _detReq[0].Tributos = _TributosRequest.ToArray();
                    //Armamos el Request.
                    AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                    _req.FeCabReq = _cabReq;
                    _req.FeDetReq = _detReq.ToArray();
                    try
                    {
                        List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest, strPasswordSecureString, _req, true, out _lstErrores);

                        if (_Respuesta.Count() > 0)
                        {
                            //Recupero el nombre de la terminal.
                            string _terminal = Environment.MachineName;
                            foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                            {
                                switch (cri.Resultado)
                                {
                                    case "A":
                                    case "P":
                                        {
                                            //Grabo el dato en facturascamposlibres.
                                            bool _rtado = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(_serieComp, _numeroComp, _nComp, _connection);
                                            if (!_rtado)
                                            {
                                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp +
                                                    ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                            }
                                            else
                                            {
                                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp +
                                                        ") se informo con la observacion: " + cri.Observaciones);
                                            }
                                            //Inserto en Rem_transacciones.
                                            DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serieComp, _numeroComp, _nComp, _connection);
                                            _rta = true;
                                            break;
                                        }
                                    case "R":
                                        {
                                            //Informo.
                                            if (!_isService)
                                            {
                                                MessageBox.Show("El comprobante (SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp + Environment.NewLine +
                                                    "Fue rechazado por AFIP. Error" + cri.Observaciones,
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                            }
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp +
                                                ") fue rechazado por la AFIP el ser infomado.");
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Error: " + cri.Observaciones);
                                            _rta = false;
                                            break;
                                        }
                                }
                            }
                        }
                        else
                        {
                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                if (!_isService)
                                {
                                    MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                        "Descripcion: " + _er.Msg, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                }
                            }
                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp +
                                                ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                            _rta = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + _serieComp + ", NUMERO: " + _numeroComp.ToString() + ", N: " + _nComp +
                                                ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                        _rta = false;
                    }
                }
                return _rta;
            }
            /// <summary>
            /// Transaccion para cambiar el numero fiscal de un comprobante con CAEA.
            /// </summary>
            /// <param name="_nFactura">N de Factura de ICG</param>
            /// <param name="_nroFiscal">Nuevo numero fiscal para el comprobante.</param>
            /// <param name="_numeroFactura">Numero de Factura de ICG.</param>
            /// <param name="_pathLog">Directorio de LOG.</param>
            /// <param name="_serieFactura">Serie de Factura de ICG.</param>
            /// <param name="_sql">Conexion SQL.</param>
            public static void TransaccionChangeNroFiscal(string _serieFactura, int _numeroFactura, string _nFactura,
                int _nroFiscal, string _pathLog, SqlConnection _sql)
            {
                SqlTransaction _tran;
                //Comienzo la transaccion
                _tran = _sql.BeginTransaction();
                try
                {
                    //Grabo FacturasVentaSerieResol.
                    FacturasVentaSeriesResol.UpdateNroFiscalFacturasVentaSeriesResol(_serieFactura, _numeroFactura, _nFactura, _nroFiscal, _pathLog, _sql, _tran);
                    //Inserto en Rem_transacciones.
                    //Grabo RemTransacciones. Enviamos los cambios a central.
                    RemTransacciones.InsertRemTransacciones(Environment.MachineName, _serieFactura, _numeroFactura, _nFactura, 1, _sql, _tran);
                    //Comiteo los cambios
                    _tran.Commit();
                }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionBlack. Error: " + ex.Message);
                }
            }
        }

        public class Licencia
        {
            public static void GetKey(string _server, string _database, string _cuit, string _ptoVta,
                out string _key, out string _msj)
            {
                _key = "";
                _msj = "";
                string sql = @"select TITULO, NOMBRECOMERCIAL as Nombre, TITULO as RazonSocial, CIF as CUIT from [GENERAL].dbo.EMPRESAS where CODEMPRESA = @codEmp";
                //Armamos el stringConnection.
                string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=ICGAdmin;Password=masterkey;";

                IcgFceDll.Licencia lic = new IcgFceDll.Licencia();
                try
                {
                    RegistryKey _keyReg = Registry.CurrentUser;
                    string _databaseNro = _keyReg.OpenSubKey(@"SOFTWARE\ICG\RetailSQL2007").GetValue("EMPGESTFRONT").ToString();

                    using (SqlConnection _connection = new SqlConnection(strConnection))
                    {
                        _connection.Open();

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = _connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = sql;

                            //cmd.Parameters.AddWithValue("@cuit", _cuit);
                            cmd.Parameters.AddWithValue("@codEmp", _databaseNro);

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        if (String.IsNullOrEmpty(reader["NOMBRE"].ToString()) ||
                                            String.IsNullOrEmpty(reader["RazonSocial"].ToString()) ||
                                            String.IsNullOrEmpty(reader["CUIT"].ToString()))
                                        {
                                            throw new Exception("Los siguientes datos de la empresa deben estar completos." + Environment.NewLine +
                                                "NOMBRE, NOMBRE COMERCIAL y CIF.");
                                        }
                                        lic.ClientCuit = reader["CUIT"].ToString();
                                        lic.ClientName = reader["NOMBRE"].ToString();
                                        lic.ClientRazonSocial = reader["RazonSocial"].ToString();
                                    }
                                }
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = _connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "SELECT VERSION from [VERSION]";
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        lic.Version = reader["VERSION"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    //No Aplico la nueva Key.
                    lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                    lic.password = "Pinsua.2730";
                    lic.Plataforma = "RETAIL";
                    lic.Release = Application.ProductVersion;
                    lic.user = "pinsua@yahoo.com";
                    lic.Tipo = "FCE";
                    lic.TerminalName = Environment.MachineName;
                    lic.PointOfSale = Convert.ToInt32(_ptoVta);
                    //
                    var json = JsonConvert.SerializeObject(lic);

                    var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");

                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", json, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        IcgFceDll.ResponcePostLicencia res = JsonConvert.DeserializeObject<IcgFceDll.ResponcePostLicencia>(response.Content);
                        if (String.IsNullOrEmpty(res.Key))
                        {
                            _key = "";
                            _msj = "Licencia NO ACTIVA.";
                        }
                        else
                        {
                            _key = res.Key;
                            _msj = "";
                        }
                    }
                    else
                    {
                        _key = "";
                        //_msj = response.Content;
                        _msj = "-1 Error: El servicio contesto con error";
                    }
                }
                catch (Exception ex)
                {
                    _key = "";
                    _msj = "-1 Error: " + ex.Message;
                }
                return;
            }
        }

        public static List<AfipDll.wsAfip.ComprobanteAsoc> ArmarCbteAsoc(DataAccess.DatosFactura fc, string strCUIT, SqlConnection _connection)
        {
            List<AfipDll.wsAfip.ComprobanteAsoc> _lst = new List<AfipDll.wsAfip.ComprobanteAsoc>();
            try
            {

                List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _connection);

                foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                {
                    AfipDll.wsAfip.ComprobanteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocNew(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _connection);
                    _lst.Add(_cls);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        public static List<AfipDll.wsAfipCae.CbteAsoc> ArmarCbteAsocCAEA(DataAccess.DatosFactura fc, string strCUIT, SqlConnection _connection)
        {
            List<AfipDll.wsAfipCae.CbteAsoc> _lst = new List<AfipDll.wsAfipCae.CbteAsoc>();
            try
            {

                List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _connection);

                foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                {
                    AfipDll.wsAfipCae.CbteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocNewCAEA(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _connection);
                    _lst.Add(_cls);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
    }
}
