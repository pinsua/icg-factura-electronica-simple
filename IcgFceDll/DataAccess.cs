﻿using AfipDll;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace IcgFceDll
{
    public class DataAccess
    {
        public class TiposDoc
        {
            public int TipoDoc { get; set; }
            public string Descripcion { get; set; }

            public static List<TiposDoc> GetTiposDoc(string _connexion)
            {
                //Instanciamos la conexion.
                string strSql = "SELECT DISTINCT TIPOSDOC.TIPODOC, TIPOSDOC.DESCRIPCION FROM TIPOSDOC, SERIESCAMPOSLIBRES, SERIESDOC " +
                    "WHERE SERIESCAMPOSLIBRES.SERIE = SERIESDOC.SERIE AND SERIESDOC.TIPODOC = TIPOSDOC.TIPODOC AND SERIESCAMPOSLIBRES.ELECTRONICA = 'T' " +
                    "ORDER BY DESCRIPCION";
                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<TiposDoc> _TipoDoc = new List<TiposDoc>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TiposDoc _td = new TiposDoc();
                                _td.TipoDoc = Convert.ToInt32(reader["TipoDoc"]);
                                _td.Descripcion = reader["Descripcion"].ToString();
                                _TipoDoc.Add(_td);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _TipoDoc;
            }
        }

        public class Clientes
        {
            public int CodCliente { get; set; }
            public string NombreCliente { get; set; }

            public static List<Clientes> GetClientes(string _connexion)
            {
                //Instanciamos la conexion.
                string strSql = "select CODCLIENTE, NOMBRECLIENTE from CLIENTES";
                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Clientes> _Clientes = new List<Clientes>();

                try
                {
                    _Connection.Open();
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Clientes _Cliente = new Clientes();
                                _Cliente.CodCliente = Convert.ToInt32(reader["CODCLIENTE"]);
                                _Cliente.NombreCliente = reader["NOMBRECLIENTE"].ToString();
                                _Clientes.Add(_Cliente);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Clientes;
            }

        }

        public class Facturas
        {
            public string NumSerie { get; set; }
            public int NumFactura { get; set; }
            public string N { get; set; }
            public DateTime Fecha { get; set; }
            public string NombreCliente { get; set; }
            public decimal TotalBruto { get; set; }
            public decimal TotalNeto { get; set; }
            public decimal TotalImpuestos { get; set; }
            public string DescTipoDoc { get; set; }
            public int NumeroFiscal { get; set; }
            public string SerieFiscal { get; set; }
            public string Estado { get; set; }
            public string TipoDoc { get; set; }
            public string PuntoVenta { get; set; }
            public string Cae { get; set; }
            public string ErrorCae { get; set; }
            public string ClienteDoc { get; set; }
            public string ClienteNroDoc { get; set; }
            public string DescripMoneda { get; set; }

            public static List<Facturas> GetFacturas(string _feDesde, string _feHasta, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, string _connexion)
            {
                //Seteamos el formato americano para ver si así trae las FC.
                //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);
                //Instanciamos la conexion.

                string strSql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                    "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, " +
                    "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA " +
                    "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N " +
                    "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                    "AND FACTURASVENTA.FECHA >= '" + _feDesde + "' AND FACTURASVENTA.FECHA <= '" + _feHasta + "'";

                if (!String.IsNullOrEmpty(strTiposDoc))
                    strSql = strSql + " AND FACTURASVENTA.TIPODOC in(" + strTiposDoc + ")";
                if (!String.IsNullOrEmpty(strClientes))
                    strSql = strSql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Facturas> _Facturas = new List<Facturas>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Facturas _fc = new Facturas();
                                _fc.Cae = reader["Cae"].ToString();
                                _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                _fc.ErrorCae = reader["Error_Cae"].ToString();

                                _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                _fc.N = reader["N"].ToString();
                                _fc.NombreCliente = reader["NombreCliente"].ToString();
                                if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                    _fc.NumeroFiscal = 0;
                                else
                                    _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                _fc.NumSerie = reader["NumSerie"].ToString();
                                _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                _fc.Estado = reader["ESTADO_FE"].ToString();
                                _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();

                                //if (String.IsNullOrEmpty(_fc.Cae))
                                //{
                                //    if (String.IsNullOrEmpty(_fc.ErrorCae))
                                //        _fc.Estado = "Pendiente";
                                //    else
                                //        _fc.Estado = "Error";
                                //}
                                //else
                                //    _fc.Estado = "Facturado";

                                _Facturas.Add(_fc);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Facturas;
            }

            public static List<Facturas> GetFacturas(DateTime _feDesde, DateTime _feHasta, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, string _connexion)
            {

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);

                string strSql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                    "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, " +
                    "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA " +
                    "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N " +
                    "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                    "AND FACTURASVENTA.FECHA >= @desde  AND FACTURASVENTA.FECHA <= @hasta";

                if (!String.IsNullOrEmpty(strTiposDoc))
                    strSql = strSql + " AND FACTURASVENTA.TIPODOC in(" + strTiposDoc + ")";
                if (!String.IsNullOrEmpty(strClientes))
                    strSql = strSql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                List<Facturas> _Facturas = new List<Facturas>();

                using (SqlConnection _Connection = new SqlConnection(_connexion))
                {
                    using (SqlCommand _Command = new SqlCommand(strSql))
                    {
                        _Command.Connection = _Connection;
                        _Command.CommandType = System.Data.CommandType.Text;
                        _Command.Parameters.AddWithValue("@desde", _feDesde.Date);
                        _Command.Parameters.AddWithValue("@hasta", _feHasta.Date);

                        try
                        {
                            _Connection.Open();

                            using (SqlDataReader reader = _Command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        Facturas _fc = new Facturas();
                                        _fc.Cae = reader["Cae"].ToString();
                                        _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                        _fc.ErrorCae = reader["Error_Cae"].ToString();

                                        _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                        _fc.N = reader["N"].ToString();
                                        _fc.NombreCliente = reader["NombreCliente"].ToString();
                                        if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                            _fc.NumeroFiscal = 0;
                                        else
                                            _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                        _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                        _fc.NumSerie = reader["NumSerie"].ToString();
                                        _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                        //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                        _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                        _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                        _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                        _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                        _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                        _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                        _fc.Estado = reader["ESTADO_FE"].ToString();
                                        _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();

                                        _Facturas.Add(_fc);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                            //System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                return _Facturas;
            }

            public static List<Facturas> GetFacturas(DateTime _feDesde, DateTime _feHasta, int _empresa, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, string _connexion)
            {

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);

                string strSql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                    "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, " +
                    "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA " +
                    "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N " +
                    "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                    "AND FACTURASVENTA.ENLACE_EMPRESA = @Empresa " +
                    "AND FACTURASVENTA.FECHA >= @desde  AND FACTURASVENTA.FECHA <= @hasta";

                if (!String.IsNullOrEmpty(strTiposDoc))
                    strSql = strSql + " AND FACTURASVENTA.TIPODOC in(" + strTiposDoc + ")";
                if (!String.IsNullOrEmpty(strClientes))
                    strSql = strSql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                List<Facturas> _Facturas = new List<Facturas>();

                using (SqlConnection _Connection = new SqlConnection(_connexion))
                {
                    using (SqlCommand _Command = new SqlCommand(strSql))
                    {
                        _Command.Connection = _Connection;
                        _Command.CommandType = System.Data.CommandType.Text;
                        _Command.Parameters.AddWithValue("@Empresa", _empresa);
                        _Command.Parameters.AddWithValue("@desde", _feDesde.Date);
                        _Command.Parameters.AddWithValue("@hasta", _feHasta.Date);

                        try
                        {
                            _Connection.Open();

                            using (SqlDataReader reader = _Command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        Facturas _fc = new Facturas();
                                        _fc.Cae = reader["Cae"].ToString();
                                        _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                        _fc.ErrorCae = reader["Error_Cae"].ToString();

                                        _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                        _fc.N = reader["N"].ToString();
                                        _fc.NombreCliente = reader["NombreCliente"].ToString();
                                        if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                            _fc.NumeroFiscal = 0;
                                        else
                                            _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                        _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                        _fc.NumSerie = reader["NumSerie"].ToString();
                                        _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                        //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                        _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                        _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                        _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                        _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                        _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                        _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                        _fc.Estado = reader["ESTADO_FE"].ToString();
                                        _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();

                                        _Facturas.Add(_fc);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                            //System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                return _Facturas;
            }

            private static string GetStringForTiposDoc(List<TiposDoc> _lstDoc)
            {
                string strRta = "";
                foreach (TiposDoc td in _lstDoc)
                {
                    if (String.IsNullOrEmpty(strRta))
                        strRta = td.TipoDoc.ToString();
                    else
                        strRta = strRta + ", " + td.TipoDoc.ToString();
                }
                return strRta;
            }

            private static string GetStringForCliente(List<Clientes> _lstCli)
            {
                string strRta = "";
                foreach (Clientes cli in _lstCli)
                {
                    if (String.IsNullOrEmpty(strRta))
                        strRta = cli.CodCliente.ToString();
                    else
                        strRta = strRta + ", " + cli.CodCliente.ToString();
                }
                return strRta;
            }

            public static bool ValidoCAE(string _N, string _NumSerie, int _NumFactura, string _stringConnection)
            {
                bool booRta = false;
                object objRta;

                string strSql = "SELECT CAE FROM FACTURASVENTACAMPOSLIBRES WHERE N = @B AND NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura";

                SqlConnection _connection = new SqlConnection(_stringConnection);
                SqlCommand _command = new SqlCommand(strSql);
                _command.Connection = _connection;
                _command.CommandType = System.Data.CommandType.Text;
                _command.CommandText = strSql;

                _command.Parameters.AddWithValue("@B", _N);
                _command.Parameters.AddWithValue("@NumSerie", _NumSerie);
                _command.Parameters.AddWithValue("@NumFactura", _NumFactura);
                try
                {
                    //Abrimos la conexion.
                    _connection.Open();

                    objRta = _command.ExecuteScalar();

                    if (objRta == DBNull.Value)
                        objRta = false;
                    else
                        objRta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                    //booRta = false;
                }
                finally
                {
                    if (_connection.State == System.Data.ConnectionState.Open)
                        _connection.Close();
                }

                return booRta;
            }
        }

        public class FacturasVenta
        {
            public static bool UpdateCleanCashControlCode(string _serie, int _numero, string _texto, SqlConnection _connection)
            {
                string _sentencia = "UPDATE FACTURASVENTA SET CLEANCASHCONTROLCODE2 = @texto Where NUMSERIE = @Serie and NUMFACTURA = @Numero and N = 'B'";

                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(_sentencia);
                _Command.Connection = _connection;

                _Command.Parameters.AddWithValue("@Serie", _serie);
                _Command.Parameters.AddWithValue("@Numero", _numero);
                _Command.Parameters.AddWithValue("@texto", _texto);
                try
                {
                    if (_connection.State == System.Data.ConnectionState.Closed)
                        _connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
        public class FacturasVentasTot
        {
            public decimal Iva { get; set; }
            public decimal BaseImponible { get; set; }
            public decimal TotIva { get; set; }
            public string Nombre { get; set; }

            public static List<FacturasVentasTot> GetTotalesIva(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE IVA >= 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.                
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesIva2(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _connexion)
            {
                string strSql = "SELECT IMPUESTOS.DESCRIPCION, FACTURASVENTATOT.BASEIMPONIBLE, FACTURASVENTATOT.IVA, FACTURASVENTATOT.TOTIVA " +
                    "FROM FACTURASVENTATOT INNER JOIN IMPUESTOS ON FACTURASVENTATOT.TIPOIVA = IMPUESTOS.TIPOIVA " +
                    "WHERE FACTURASVENTATOT.IVA >= 0 AND FACTURASVENTATOT.CODDTO = @CodDto AND FACTURASVENTATOT.SERIE = @Serie AND FACTURASVENTATOT.NUMERO = @Numero AND FACTURASVENTATOT.N = @N";
                //Instanciamos la conexion.                
                //Instanciamos la conexion.                
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _connexion;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();

                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                fct.Nombre = reader["DESCRIPCION"].ToString();
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                return _Iva;
            }
            public static List<FacturasVentasTot> GetTotalesIva(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE IVA >= 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.                
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connexion;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }

                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesTributos(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion, string _Neto)
            {
                //string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                string strSql = "SELECT IVA, IMPORTE, 0 as TOTIVA FROM FACTURASVENTADTOS WHERE CODDTO = @CodDto AND NUMSERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();

                decimal _neto = Convert.ToDecimal(_Neto);
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                //fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.BaseImponible = _neto;
                                fct.Iva = _neto / Convert.ToDecimal(reader["IMPORTE"]);
                                fct.TotIva = Convert.ToDecimal(reader["IMPORTE"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesTributos(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion, string _Neto)
            {
                string strSql = "SELECT IVA, IMPORTE, 0 as TOTIVA FROM FACTURASVENTADTOS WHERE CODDTO = @CodDto AND NUMSERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);

                _Command.Connection = _Connexion;
                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();

                decimal _neto = Convert.ToDecimal(_Neto);
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                //fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.BaseImponible = _neto;
                                fct.Iva = _neto / Convert.ToDecimal(reader["IMPORTE"]);
                                fct.TotIva = Convert.ToDecimal(reader["IMPORTE"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }

                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesNoGravado(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE CODDTO = @CodDto AND IVA = 0 AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesNoGravado(string _Serie, string _N, string _Numero, int _CodDto, SqlConnection _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE CODDTO = @CodDto AND IVA = 0 AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connexion;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                }

                return _Iva;
            }
        }

        public class FacturasVentaCamposLibres : IDisposable
        {
            public string NumeroSerie { get; set; }
            public int NumeroFactura { get; set; }
            public string N { get; set; }
            public string Cae { get; set; }
            public string VtoCae { get; set; }
            public string CodBarras { get; set; }
            public string ErrorCae { get; set; }
            public string Estado { get; set; }
            public string CodigoQr { get; set; }
            public string PtoVta { get; set; }

            public void Dispose()
            {
                //Dispose(true);
                GC.SuppressFinalize(this);
            }

            public bool UpdateFacturasVentasCamposLibres(FacturasVentaCamposLibres _FcLibres, string _Connexion)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ";
                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    strSql = strSql + "CAE = @CAE,  VTO_CAE = @VTOCAE, COD_BARRAS = @CODBARRAS, ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                else
                    strSql = strSql + "ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                strSql = strSql + " WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                {
                    _Command.Parameters.AddWithValue("@CAE", _FcLibres.Cae);
                    _Command.Parameters.AddWithValue("@VTOCAE", _FcLibres.VtoCae);
                    _Command.Parameters.AddWithValue("@CODBARRAS", _FcLibres.CodBarras);
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                }
                else
                {
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                }
                _Command.Parameters.AddWithValue("@ERRORCAE", _FcLibres.ErrorCae.Length > 100 ? _FcLibres.ErrorCae.Substring(0, 100) : _FcLibres.ErrorCae);
                _Command.Parameters.AddWithValue("@NUMSERIE", _FcLibres.NumeroSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _FcLibres.NumeroFactura);
                _Command.Parameters.AddWithValue("@N", _FcLibres.N);
                try
                {
                    _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public bool UpdateFacturasVentasCamposLibres(FacturasVentaCamposLibres _FcLibres, SqlConnection _Connexion)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ";
                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    strSql = strSql + "CAE = @CAE,  VTO_CAE = @VTOCAE, COD_BARRAS = @CODBARRAS, ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO, CODIGO_QR = @QR, PTO_VTA = @ptoVta ";
                else
                    strSql = strSql + "ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                strSql = strSql + " WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connexion;

                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                {
                    _Command.Parameters.AddWithValue("@CAE", _FcLibres.Cae);
                    _Command.Parameters.AddWithValue("@VTOCAE", _FcLibres.VtoCae);
                    _Command.Parameters.AddWithValue("@CODBARRAS", _FcLibres.CodBarras);
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                    _Command.Parameters.AddWithValue("@QR", _FcLibres.CodigoQr);
                    _Command.Parameters.AddWithValue("@ptoVta", _FcLibres.PtoVta);
                }
                else
                {
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                }
                _Command.Parameters.AddWithValue("@ERRORCAE", _FcLibres.ErrorCae.Length > 100 ? _FcLibres.ErrorCae.Substring(0, 100) : _FcLibres.ErrorCae);
                _Command.Parameters.AddWithValue("@NUMSERIE", _FcLibres.NumeroSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _FcLibres.NumeroFactura);
                _Command.Parameters.AddWithValue("@N", _FcLibres.N);
                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public bool UpdateFacturasVentasCamposLibres(FacturasVentaCamposLibres _FcLibres, string _pathLog, SqlConnection _Connexion)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ";
                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    strSql = strSql + "CAE = @CAE,  VTO_CAE = @VTOCAE, COD_BARRAS = @CODBARRAS, ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO, CODIGO_QR = @QR ";
                else
                    strSql = strSql + "ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                strSql = strSql + " WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                try
                {
                    //Instanciamos la conexion.
                    SqlCommand _Command = new SqlCommand(strSql);
                    _Command.Connection = _Connexion;

                    if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    {
                        _Command.Parameters.AddWithValue("@CAE", _FcLibres.Cae);
                        _Command.Parameters.AddWithValue("@VTOCAE", _FcLibres.VtoCae);
                        _Command.Parameters.AddWithValue("@CODBARRAS", _FcLibres.CodBarras);
                        _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                        _Command.Parameters.AddWithValue("@QR", _FcLibres.CodigoQr);
                    }
                    else
                    {
                        _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                    }
                    _Command.Parameters.AddWithValue("@ERRORCAE", _FcLibres.ErrorCae.Length > 100 ? _FcLibres.ErrorCae.Substring(0, 100) : _FcLibres.ErrorCae);
                    _Command.Parameters.AddWithValue("@NUMSERIE", _FcLibres.NumeroSerie);
                    _Command.Parameters.AddWithValue("@NUMFACTURA", _FcLibres.NumeroFactura);
                    _Command.Parameters.AddWithValue("@N", _FcLibres.N);

                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch(Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "UpdateFacturasVentasCamposLibres Error: " + ex.Message);
                    return false;
                }
            }

            public bool UpdateFacturasVentasCamposLibres(FacturasVentaCamposLibres _FcLibres, SqlConnection _Connexion, SqlTransaction _tran)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ";
                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    strSql = strSql + "CAE = @CAE,  VTO_CAE = @VTOCAE, COD_BARRAS = @CODBARRAS, ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO, CODIGO_QR = @QR, PTO_VTA = @ptoVta ";
                else
                    strSql = strSql + "ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                strSql = strSql + " WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connexion;
                _Command.Transaction = _tran;

                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                {
                    _Command.Parameters.AddWithValue("@CAE", _FcLibres.Cae);
                    _Command.Parameters.AddWithValue("@VTOCAE", _FcLibres.VtoCae);
                    _Command.Parameters.AddWithValue("@CODBARRAS", _FcLibres.CodBarras);
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                    _Command.Parameters.AddWithValue("@QR", _FcLibres.CodigoQr);
                    _Command.Parameters.AddWithValue("@ptoVta", _FcLibres.PtoVta);
                }
                else
                {
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                }
                _Command.Parameters.AddWithValue("@ERRORCAE", _FcLibres.ErrorCae.Length > 100 ? _FcLibres.ErrorCae.Substring(0, 100) : _FcLibres.ErrorCae);
                _Command.Parameters.AddWithValue("@NUMSERIE", _FcLibres.NumeroSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _FcLibres.NumeroFactura);
                _Command.Parameters.AddWithValue("@N", _FcLibres.N);
                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public static FacturasVentaCamposLibres GetRecord(string _conexion, DatosFactura _cls)
            {
                string strSql = "SELECT CAE, VTO_CAE, COD_BARRAS, ERROR_CAE FROM FACTURASVENTACAMPOSLIBRES " +
                    "WHERE NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_conexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);

                FacturasVentaCamposLibres fvcl = new FacturasVentaCamposLibres();

                try
                {
                    _Connection.Open();
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                fvcl.Cae = reader["Cae"].ToString();
                                fvcl.CodBarras = reader["Cod_Barras"].ToString();
                                fvcl.ErrorCae = reader["Error_Cae"].ToString();
                                fvcl.Estado = "";
                                fvcl.N = _cls.N;
                                fvcl.NumeroFactura = Convert.ToInt32(_cls.NumFactura);
                                fvcl.NumeroSerie = _cls.NumSerie;
                                fvcl.VtoCae = reader["Vto_Cae"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error buscando FacturasVentaCamposLibres. Error: " + ex.Message);
                }
                return fvcl;
            }

            public static FacturasVentaCamposLibres GetRecord(SqlConnection _conn, DatosFactura _cls)
            {
                string strSql = "SELECT CAE, VTO_CAE, COD_BARRAS, ERROR_CAE FROM FACTURASVENTACAMPOSLIBRES " +
                    "WHERE NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _conn;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);

                FacturasVentaCamposLibres fvcl = new FacturasVentaCamposLibres();

                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                fvcl.Cae = reader["Cae"].ToString();
                                fvcl.CodBarras = reader["Cod_Barras"].ToString();
                                fvcl.ErrorCae = reader["Error_Cae"].ToString();
                                fvcl.Estado = "";
                                fvcl.N = _cls.N;
                                fvcl.NumeroFactura = Convert.ToInt32(_cls.NumFactura);
                                fvcl.NumeroSerie = _cls.NumSerie;
                                fvcl.VtoCae = reader["Vto_Cae"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error buscando FacturasVentaCamposLibres. Error: " + ex.Message);
                }
                return fvcl;
            }

            public static void ModificoFacturasVentaCampoLibres(DatosFactura _Fc, string _Cae, string _Error, string _FeVto, 
                string _CodBarra, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                    }
                    
                    fcLib.N = _Fc.N;
                    fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                    fcLib.NumeroSerie = _Fc.NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, string _Cae, string _Error, string _FeVto, string _CodBarra, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                        fcLib.VtoCae = "";
                    }

                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, 
                string _Cae, string _Error, string _FeVto, string _CodBarra, string _QR, 
                string _PathLog, string _PtoVta, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    fcLib.CodigoQr = _QR;
                    fcLib.PtoVta = _PtoVta;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.VtoCae = _FeVto.Length == 8 ?  _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4) : _FeVto;
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                        fcLib.VtoCae = "";
                    }

                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, 
                string _Cae, string _Error, string _FeVto, string _CodBarra, SqlConnection _conn, SqlTransaction _tran)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        if (_Error == "CAEA")
                        {
                            fcLib.ErrorCae = "";
                            fcLib.Estado = "SININFORMAR";
                            fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        }
                        else
                        {
                            fcLib.ErrorCae = "";
                            fcLib.Estado = "FACTURADO";
                            fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        }
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                        fcLib.VtoCae = "";
                    }

                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn, _tran);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, 
                string _Cae, string _Error, string _FeVto, string _CodBarra, string _QR, string _PtoVta,
                SqlConnection _conn, SqlTransaction _tran)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    fcLib.CodigoQr = _QR;
                    fcLib.PtoVta = _PtoVta;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        if (_Error == "CAEA")
                        {
                            fcLib.ErrorCae = "";
                            fcLib.Estado = "SININFORMAR";
                            fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        }
                        else
                        {
                            fcLib.ErrorCae = "";
                            fcLib.Estado = "FACTURADO";
                            fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        }
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                        fcLib.VtoCae = "";
                    }

                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn, _tran);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, string _Cae, string _Error, string _FeVto, string _CodBarra, string _connectionString)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                        fcLib.VtoCae = "";
                    }

                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _connectionString);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, int _NroComprobante, string _Error, string _connectionString)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = "";
                    fcLib.CodBarras = _NroComprobante.ToString();
                    fcLib.ErrorCae = "";
                    fcLib.Estado = "RECUPERAR";
                    fcLib.ErrorCae = _Error;
                    fcLib.VtoCae = "";
                    fcLib.N = _N;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _connectionString);
                }
            }

            public static void CAEAFacturasVentaCampoLibres(DatosFactura _Fc, string _Cae, string _Error, string _FeVto, string _CodBarra, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = _Cae;
                    fcLib.CodBarras = _CodBarra;
                    fcLib.ErrorCae = "";
                    fcLib.Estado = "FACTURADO";
                    fcLib.VtoCae = DateTime.Now.Day.ToString().PadLeft(2,'0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                    fcLib.N = _Fc.N;
                    fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                    fcLib.NumeroSerie = _Fc.NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(DatosFactura _Fc, int _NroComprobante, string _Error, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = "";
                    fcLib.CodBarras = _NroComprobante.ToString();
                    fcLib.ErrorCae = "";
                    fcLib.Estado = "RECUPERAR";
                    fcLib.ErrorCae = _Error;
                    fcLib.VtoCae = "";
                    fcLib.N = _Fc.N;
                    fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                    fcLib.NumeroSerie = _Fc.NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(DatosFactura _Fc, long _NroComprobante, string _Error, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    fcLib.Cae = "";
                    fcLib.CodBarras = _NroComprobante.ToString();
                    fcLib.ErrorCae = "";
                    fcLib.Estado = "RECUPERAR";
                    fcLib.ErrorCae = _Error;
                    fcLib.VtoCae = "";
                    fcLib.N = _Fc.N;
                    fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                    fcLib.NumeroSerie = _Fc.NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _conn);
                }
            }

            public static bool FacturasVentasCamposLibresInformoCAEA(string _serie, int _numero, string _n, SqlConnection _Connection)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET CAEA_INFORMADO = CONVERT(varchar, getdate(), 22)," +
                    " ESTADO_FE = 'FACTURADO' WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                using (SqlCommand _Command = new SqlCommand(strSql))
                {
                    _Command.Connection = _Connection;
                    _Command.Parameters.AddWithValue("@NUMSERIE", _serie);
                    _Command.Parameters.AddWithValue("@NUMFACTURA", _numero);
                    _Command.Parameters.AddWithValue("@N", _n);
                    try
                    {
                        if (_Connection.State == System.Data.ConnectionState.Closed)
                            _Connection.Open();
                        int intOK = _Command.ExecuteNonQuery();
                        if (intOK > 0)
                            return true;
                        else
                            return false;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }

            public static bool FacturasVentasCamposLibresInformoCAEARechazo(string _serie, int _numero, string _n, string _error, SqlConnection _Connection)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ERROR_CAE = @error, ESTADO_FE = 'SININFORMAR', CAEA_INFORMADO = null WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                using (SqlCommand _Command = new SqlCommand(strSql))
                {
                    _Command.Connection = _Connection;
                    _Command.Parameters.AddWithValue("@NUMSERIE", _serie);
                    _Command.Parameters.AddWithValue("@NUMFACTURA", _numero);
                    _Command.Parameters.AddWithValue("@N", _n);
                    _Command.Parameters.AddWithValue("@error", _error);
                    try
                    {
                        if (_Connection.State == System.Data.ConnectionState.Closed)
                            _Connection.Open();
                        int intOK = _Command.ExecuteNonQuery();
                        if (intOK > 0)
                            return true;
                        else
                            return false;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }

        public class FacturasVentaSeriesResol : IDisposable
        {
            public string NumSerie { get; set; }
            public int NumFactura { get; set; }
            public string N { get; set; }
            public string SerieFiscal1 { get; set; }
            public string SerieFiscal2 { get; set; }
            public int NumeroFiscal { get; set; }

            public void Dispose()
            {
                //Dispose(true);
                GC.SuppressFinalize(this);
            }

            public static bool InsertFacturasVentaSeriesResol(FacturasVentaSeriesResol _cls, string _Conection)
            {
                string strSql = "INSERT INTO FACTURASVENTASERIESRESOL " +
                    "(NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES (@NUMSERIE, @NUMFACTURA, @N, @SERIEFISCAL1, @SERIEFISCAL2, @NUMEROFISCAL)";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Conection);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);
                _Command.Parameters.AddWithValue("@SERIEFISCAL1", _cls.SerieFiscal1);
                _Command.Parameters.AddWithValue("@SERIEFISCAL2", _cls.SerieFiscal2);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _cls.NumeroFiscal);

                try
                {
                    _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public static bool InsertFacturasVentaSeriesResol(FacturasVentaSeriesResol _cls, SqlConnection _Conection)
            {
                string strSql = "INSERT INTO FACTURASVENTASERIESRESOL " +
                    "(NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES (@NUMSERIE, @NUMFACTURA, @N, @SERIEFISCAL1, @SERIEFISCAL2, @NUMEROFISCAL)";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Conection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);
                _Command.Parameters.AddWithValue("@SERIEFISCAL1", _cls.SerieFiscal1);
                _Command.Parameters.AddWithValue("@SERIEFISCAL2", _cls.SerieFiscal2);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _cls.NumeroFiscal);

                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public static bool InsertFacturasVentaSeriesResol(FacturasVentaSeriesResol _cls, SqlConnection _Conection, SqlTransaction _tran)
            {
                string strSql = "INSERT INTO FACTURASVENTASERIESRESOL " +
                    "(NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES (@NUMSERIE, @NUMFACTURA, @N, @SERIEFISCAL1, @SERIEFISCAL2, @NUMEROFISCAL)";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Conection;
                _Command.Transaction = _tran;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);
                _Command.Parameters.AddWithValue("@SERIEFISCAL1", _cls.SerieFiscal1);
                _Command.Parameters.AddWithValue("@SERIEFISCAL2", _cls.SerieFiscal2);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _cls.NumeroFiscal);

                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }

            public static bool Update_Insert_FacturasVentaSeriesResol(FacturasVentaSeriesResol _cls, string _pathLog, SqlConnection _Conection)
            {
                string strSql = @"IF(SELECT isnull(NUMEROFISCAL, 0) FROM FACTURASVENTASERIESRESOL WHERE numserie = @NUMSERIE and numfactura = @NUMFACTURA and N = @N) > 0
                    BEGIN
                        UPDATE FACTURASVENTASERIESRESOL SET SERIEFISCAL1 = @SERIEFISCAL1, SERIEFISCAL2 = @SERIEFISCAL2, NUMEROFISCAL = @NUMEROFISCAL
                        WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA  AND N = @N
                    END
                    ELSE
                    BEGIN
                         INSERT INTO FACTURASVENTASERIESRESOL(NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL)

                         VALUES(@NUMSERIE, @NUMFACTURA, @N, @SERIEFISCAL1, @SERIEFISCAL2, @NUMEROFISCAL)
                    END";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Conection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);
                _Command.Parameters.AddWithValue("@SERIEFISCAL1", _cls.SerieFiscal1);
                _Command.Parameters.AddWithValue("@SERIEFISCAL2", _cls.SerieFiscal2);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _cls.NumeroFiscal);

                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch(Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Update_Insert_FacturasVentaSeriesResol Error: " + ex.Message);
                    return false;
                }
            }

            public static bool UpdateNroFiscalFacturasVentaSeriesResol(string _numSerie, int _numFactura, string _n, int _nroFiscal, 
                string _pathLog, SqlConnection _Conection, SqlTransaction transaction)
            {
                string strSql = @"UPDATE FACTURASVENTASERIESRESOL SET NUMEROFISCAL = @NUMEROFISCAL
                        WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA  AND N = @N";
                //Instanciamos la conexion.
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Conection;
                _Command.Transaction = transaction;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _numSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _numFactura);
                _Command.Parameters.AddWithValue("@N", _n);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _nroFiscal);

                try
                {
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Update_Insert_FacturasVentaSeriesResol Error: " + ex.Message);
                    return false;
                }
            }

            public static void Insertar_FacturasVentaSerieResol(DatosFactura _Fc, int _NroComprobante, SqlConnection _conn)
            {
                using (FacturasVentaSeriesResol _fcResol = new FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _Fc.NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_Fc.NumFactura);
                    _fcResol.N = _Fc.N;
                    _fcResol.SerieFiscal1 = _Fc.PtoVta;
                    _fcResol.SerieFiscal2 = _Fc.strTipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    InsertFacturasVentaSeriesResol(_fcResol, _conn);
                }
            }

            public static void Grabar_FacturasVentaSerieResol(DatosFactura _Fc, int _NroComprobante, string _PathLog, SqlConnection _conn)
            {
                using (FacturasVentaSeriesResol _fcResol = new FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _Fc.NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_Fc.NumFactura);
                    _fcResol.N = _Fc.N;
                    _fcResol.SerieFiscal1 = _Fc.PtoVta.PadLeft(5,'0');
                    _fcResol.SerieFiscal2 = _Fc.strTipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    Update_Insert_FacturasVentaSeriesResol(_fcResol, _PathLog, _conn);
                }
            }

            public static void Insertar_FacturasVentaSerieResol(string _NumSerie, string _NumFactura, string _N, string _PtoVta, string _TipoDoc, int _NroComprobante, SqlConnection _conn)
            {
                using (DataAccess.FacturasVentaSeriesResol _fcResol = new DataAccess.FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_NumFactura);
                    _fcResol.N = _N;
                    _fcResol.SerieFiscal1 = _PtoVta;
                    _fcResol.SerieFiscal2 = _TipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    DataAccess.FacturasVentaSeriesResol.InsertFacturasVentaSeriesResol(_fcResol, _conn);
                }
            }

            public static void Insertar_FacturasVentaSerieResol(string _NumSerie, string _NumFactura, string _N, string _PtoVta, string _TipoDoc, int _NroComprobante, SqlConnection _conn, SqlTransaction _tran)
            {
                using (DataAccess.FacturasVentaSeriesResol _fcResol = new DataAccess.FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_NumFactura);
                    _fcResol.N = _N;
                    _fcResol.SerieFiscal1 = _PtoVta;
                    _fcResol.SerieFiscal2 = _TipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    InsertFacturasVentaSeriesResol(_fcResol, _conn, _tran);
                }
            }

            public static void Insertar_FacturasVentaSerieResol(string _NumSerie, string _NumFactura, string _N, string _PtoVta, string _TipoDoc, int _NroComprobante, string _connectionString)
            {
                using (DataAccess.FacturasVentaSeriesResol _fcResol = new DataAccess.FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_NumFactura);
                    _fcResol.N = _N;
                    _fcResol.SerieFiscal1 = _PtoVta;
                    _fcResol.SerieFiscal2 = _TipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    DataAccess.FacturasVentaSeriesResol.InsertFacturasVentaSeriesResol(_fcResol, _connectionString);
                }
            }

            public static FacturasVentaSeriesResol GetTiquet(string _numSerie, int _numFactura, string _n, SqlConnection _con)
            {
                string _sql = "SELECT NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL FROM FACTURASVENTASERIESRESOL " +
                        "WHERE NUMSERIE = @numSerie AND NUMFACTURA = @numFactura AND N = @n";

                FacturasVentaSeriesResol _cls = new FacturasVentaSeriesResol();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.N = _reader["N"].ToString();
                                _cls.NumeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                                _cls.NumFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                                _cls.NumSerie = _reader["NUMSERIE"].ToString();
                                _cls.SerieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                                _cls.SerieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                            }
                        }
                        else
                        {
                            //No tengo nada devuelvo en cero.
                            _cls.N = _n;
                            _cls.NumeroFiscal = 0;
                            _cls.NumFactura = _numFactura;
                            _cls.NumSerie = _numSerie;
                            _cls.SerieFiscal1 = "";
                            _cls.SerieFiscal2 = "";
                        }
                    }
                }


                return _cls;
            }

            public static void GrabarTiquetNew(FacturasVentaSeriesResol _fvsr, SqlConnection _con, SqlTransaction _tran)
            {
                string _sql = "INSERT INTO FACTURASVENTASERIESRESOL ( NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                        "VALUES(@numSerie, @numFactura, @n, @serieFiscal1, @serieFiscal2, @numeroFiscal)";

                try
                {
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        _cmd.Transaction = _tran;

                        _cmd.Parameters.AddWithValue("@Numserie", _fvsr.NumSerie);
                        _cmd.Parameters.AddWithValue("@numFactura", _fvsr.NumFactura);
                        _cmd.Parameters.AddWithValue("@n", _fvsr.N);
                        _cmd.Parameters.AddWithValue("@serieFiscal1", _fvsr.SerieFiscal1);
                        _cmd.Parameters.AddWithValue("@serieFiscal2", _fvsr.SerieFiscal2);
                        _cmd.Parameters.AddWithValue("@numeroFiscal", _fvsr.NumeroFiscal);

                        int _insert = _cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        public class DatosFactura
        {
            public string NumSerie { get; set; }
            public string NumFactura { get; set; }
            public string N { get; set; }
            public string TotalBruto { get; set; }
            public string TotalNeto { get; set; }
            public string TotalCosteIva { get; set; }
            public decimal TotIva { get; set; }
            public decimal totTributos { get; set; }
            public decimal totNoGravado { get; set; }
            public string cCodAfip { get; set; }
            public string strTipoDoc { get; set; }
            public string PtoVta { get; set; }
            public string ClienteTipoDoc { get; set; }
            public string ClienteNroDoc { get; set; }
            public string strFecha { get; set; }
            public string Estado { get; set; }
            public string regfacturacioncliente { get; set; }
            public string CompFechaHoraGen { get; set; }
            public DateTime fecha { get; set; }
            public int NroFiscal { get; set; }
            public string CAE { get; set; }
            public int codigoCliente { get; set; }
            public string Comprobante { get; set; }

            public static DatosFactura GetDatosFactura(string _serie, int _numfactura, string _n, SqlConnection _Connection)
            {
                string _sql = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, 
                    (FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO,
                     FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, 
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, 
                    FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA, CLIENTES.REGIMFACT, 
                    FACTURASVENTA.HORA, FACTURASVENTA.CODCLIENTE
                    FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE
                    INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE
                    INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                    WHERE FACTURASVENTA.NUMSERIE = @NumSeria
                    and FACTURASVENTA.NUMFACTURA = @NumFactura
                    and FACTURASVENTA.N = @N";
                //sacamo esto porque no tiene el datos cargado. SERIESCAMPOSLIBRES.ELECTRONICA = 'T' 

                DatosFactura _df = new DatosFactura();

                using (SqlCommand _Command = new SqlCommand(_sql))
                {
                    _Command.Connection = _Connection;
                    _Command.CommandType = System.Data.CommandType.Text;
                    _Command.Parameters.AddWithValue("@NumSeria", _serie);
                    _Command.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _Command.Parameters.AddWithValue("@N", _n);

                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime dttFecha = Convert.ToDateTime(reader[3].ToString());
                                DateTime dttHora = Convert.ToDateTime(reader["HORA"].ToString());
                                string _fecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                string _hora = dttHora.Hour.ToString().PadLeft(2, '0') + dttHora.Minute.ToString().PadLeft(2, '0') + dttHora.Second.ToString().PadLeft(2, '0');
                                _df.NumSerie = reader[0].ToString();
                                _df.NumFactura = reader[1].ToString();
                                _df.N = reader[2].ToString();
                                _df.strFecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                _df.TotalBruto = reader["TotalBruto"].ToString();
                                _df.TotalNeto = reader["TOTALNETO"].ToString();
                                _df.TotalCosteIva = reader["TOTALIMPUESTOS"].ToString();
                                _df.strTipoDoc = reader["DESCTIPODOC"].ToString();
                                _df.cCodAfip = FuncionesVarias.GetCodAfip(reader["DESCTIPODOC"].ToString());
                                _df.PtoVta = reader["PUNTO_DE_VENTA"].ToString();
                                _df.ClienteNroDoc = reader["NIF20"].ToString();
                                _df.ClienteTipoDoc = FuncionesVarias.GetCodClienteDoc(reader["CLIENTEDOC"].ToString());
                                _df.Estado = reader["ESTADO_FE"].ToString();
                                _df.regfacturacioncliente = reader["REGIMFACT"].ToString();
                                _df.CompFechaHoraGen = _fecha + _hora;
                                _df.fecha = dttFecha;
                                _df.NroFiscal = String.IsNullOrEmpty(reader["NUMEROFISCAL"].ToString()) ? 0 : Convert.ToInt32(reader["NUMEROFISCAL"]);
                                _df.CAE = reader["CAE"].ToString();
                                _df.codigoCliente = String.IsNullOrEmpty(reader["CODCLIENTE"].ToString()) ? 0 : Convert.ToInt32(reader["CODCLIENTE"]);

                                //Vemos el cliente.
                                //Cliente
                                if (_df.codigoCliente == 0)
                                {
                                    _df.ClienteNroDoc = "0";
                                    _df.regfacturacioncliente = "4";
                                }
                                else
                                {
                                    _df.regfacturacioncliente = String.IsNullOrEmpty(reader["REGIMFACT"].ToString().Trim()) ? "4" : reader["REGIMFACT"].ToString();
                                }
                                //Agrego el tipo de documento.
                                switch (_df.regfacturacioncliente)
                                {
                                    case "1":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "2":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "4":
                                        {
                                            if (_df.ClienteNroDoc != "0")
                                                _df.ClienteTipoDoc = "96";
                                            else
                                            {
                                                _df.ClienteTipoDoc = "99";
                                            }
                                            break;
                                        }
                                    case "5":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "6":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
                return _df;
            }
            /// <summary>
            /// Recupera los datos de la Factura
            /// </summary>
            /// <param name="_serie">Serie</param>
            /// <param name="_numfactura">Numero</param>
            /// <param name="_n">N</param>
            /// <param name="_ptovta">Punto de Venta</param>
            /// <param name="_Connection">Sql Connection</param>
            /// <returns>DatosFactura</returns>
            public static DatosFactura GetDatosFactura(string _serie, int _numfactura, string _n, string _ptovta, SqlConnection _Connection)
            {
                string _sql = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, 
                    (FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.CODCLIENTE,
                     FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, 
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, 
                    FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA, CLIENTES.REGIMFACT, FACTURASVENTA.HORA, FACTURASVENTA.REGIMFACT as RegimenFacturacion   
                    FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE
                    INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE
                    INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                    WHERE FACTURASVENTA.NUMSERIE = @NumSeria
                    and FACTURASVENTA.NUMFACTURA = @NumFactura
                    and FACTURASVENTA.N = @N";
                //sacamo esto porque no tiene el datos cargado. SERIESCAMPOSLIBRES.ELECTRONICA = 'T' 

                DatosFactura _df = new DatosFactura();

                using (SqlCommand _Command = new SqlCommand(_sql))
                {
                    _Command.Connection = _Connection;
                    _Command.CommandType = System.Data.CommandType.Text;
                    _Command.Parameters.AddWithValue("@NumSeria", _serie);
                    _Command.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _Command.Parameters.AddWithValue("@N", _n);

                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime dttFecha = Convert.ToDateTime(reader[3].ToString());
                                DateTime dttHora = Convert.ToDateTime(reader["HORA"].ToString());
                                string _fecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                string _hora = dttHora.Hour.ToString().PadLeft(2, '0') + dttHora.Minute.ToString().PadLeft(2, '0') + dttHora.Second.ToString().PadLeft(2, '0');
                                _df.NumSerie = reader[0].ToString();
                                _df.NumFactura = reader[1].ToString();
                                _df.N = reader[2].ToString();
                                _df.strFecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                _df.TotalBruto = reader["TotalBruto"].ToString();
                                _df.TotalNeto = reader["TOTALNETO"].ToString();
                                _df.TotalCosteIva = reader["TOTALIMPUESTOS"].ToString();
                                _df.strTipoDoc = reader["DESCTIPODOC"].ToString();
                                _df.cCodAfip = FuncionesVarias.GetCodAfip(reader["DESCTIPODOC"].ToString());
                                _df.PtoVta = _ptovta;
                                _df.ClienteNroDoc = reader["NIF20"].ToString();
                                _df.ClienteTipoDoc = FuncionesVarias.GetCodClienteDoc(reader["CLIENTEDOC"].ToString());
                                _df.Estado = reader["ESTADO_FE"].ToString();
                                //A pedido de Zumbo. Se toma el regimen de la tabla FacturasVenta.
                                //_df.regfacturacioncliente = reader["REGIMFACT"].ToString(); 
                                _df.regfacturacioncliente = reader["RegimenFacturacion"].ToString();
                                _df.CompFechaHoraGen = _fecha + _hora;
                                _df.fecha = dttFecha;
                                _df.NroFiscal = String.IsNullOrEmpty(reader["NUMEROFISCAL"].ToString()) ? 0 : Convert.ToInt32(reader["NUMEROFISCAL"]);
                                _df.CAE = reader["CAE"].ToString();
                                _df.codigoCliente = Convert.ToInt32(reader["CODCLIENTE"]);

                                //Vemos el cliente.
                                //Cliente
                                if (_df.codigoCliente == 0)
                                {
                                    if (_df.regfacturacioncliente != "N")
                                    {
                                        _df.ClienteNroDoc = "0";
                                        _df.regfacturacioncliente = "4";
                                    }
                                }
                                else
                                {
                                    _df.regfacturacioncliente = String.IsNullOrEmpty(reader["RegimenFacturacion"].ToString().Trim()) ? "4" : reader["RegimenFacturacion"].ToString();
                                }
                                //Agrego el tipo de documento.
                                switch (_df.regfacturacioncliente)
                                {
                                    case "1":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "2":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "4":
                                        {
                                            if (_df.ClienteNroDoc != "0")
                                                _df.ClienteTipoDoc = "96";
                                            else
                                            {
                                                _df.ClienteTipoDoc = "99";
                                            }
                                            break;
                                        }
                                    case "5":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "6":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
                return _df;
            }
            /// <summary>
            /// Recupera los datos de la Factura
            /// </summary>
            /// <param name="_serie">Serie</param>
            /// <param name="_numfactura">Numero</param>
            /// <param name="_n">N</param>
            /// <param name="_ptovta">Punto de Venta</param>
            /// <param name="_Connection">Sql Connection</param>
            /// <returns>DatosFactura</returns>
            public static DatosFactura GetDatosFacturaRetail(string _serie, int _numfactura, string _n, string _ptovta, SqlConnection _Connection)
            {
                string _sql = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, 
                    (FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.CODCLIENTE,
                     FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, 
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, 
                    FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA, CLIENTES.REGIMFACT, 
                    FACTURASVENTA.HORA, FACTURASVENTA.REGIMFACT as RegimenFacturacion, FACTURASVENTACAMPOSLIBRES.PTO_VTA
                    FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE
                    INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE
                    INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                    WHERE FACTURASVENTA.NUMSERIE = @NumSeria
                    and FACTURASVENTA.NUMFACTURA = @NumFactura
                    and FACTURASVENTA.N = @N";
                //sacamo esto porque no tiene el datos cargado. SERIESCAMPOSLIBRES.ELECTRONICA = 'T' 

                DatosFactura _df = new DatosFactura();

                using (SqlCommand _Command = new SqlCommand(_sql))
                {
                    _Command.Connection = _Connection;
                    _Command.CommandType = System.Data.CommandType.Text;
                    _Command.Parameters.AddWithValue("@NumSeria", _serie);
                    _Command.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _Command.Parameters.AddWithValue("@N", _n);

                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DateTime dttFecha = Convert.ToDateTime(reader[3].ToString());
                                DateTime dttHora = Convert.ToDateTime(reader["HORA"].ToString());
                                string _fecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                string _hora = dttHora.Hour.ToString().PadLeft(2, '0') + dttHora.Minute.ToString().PadLeft(2, '0') + dttHora.Second.ToString().PadLeft(2, '0');
                                _df.NumSerie = reader[0].ToString();
                                _df.NumFactura = reader[1].ToString();
                                _df.N = reader[2].ToString();
                                _df.strFecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                _df.TotalBruto = reader["TotalBruto"].ToString();
                                _df.TotalNeto = reader["TOTALNETO"].ToString();
                                _df.TotalCosteIva = reader["TOTALIMPUESTOS"].ToString();
                                _df.strTipoDoc = reader["DESCTIPODOC"].ToString();
                                _df.cCodAfip = FuncionesVarias.GetCodAfip(reader["DESCTIPODOC"].ToString());
                                _df.PtoVta = _ptovta;
                                _df.ClienteNroDoc = reader["NIF20"].ToString();
                                _df.ClienteTipoDoc = FuncionesVarias.GetCodClienteDoc(reader["CLIENTEDOC"].ToString());
                                _df.Estado = reader["ESTADO_FE"].ToString();
                                //A pedido de Zumbo. Se toma el regimen de la tabla FacturasVenta.
                                //_df.regfacturacioncliente = reader["REGIMFACT"].ToString(); 
                                _df.regfacturacioncliente = reader["RegimenFacturacion"].ToString();
                                _df.CompFechaHoraGen = _fecha + _hora;
                                _df.fecha = dttFecha;
                                _df.NroFiscal = String.IsNullOrEmpty(reader["NUMEROFISCAL"].ToString()) ? 0 : Convert.ToInt32(reader["NUMEROFISCAL"]);
                                _df.CAE = reader["CAE"].ToString();
                                _df.codigoCliente = Convert.ToInt32(reader["CODCLIENTE"]);
                                _df.Comprobante = reader["PTO_VTA"].ToString();

                                //Vemos el cliente.
                                //Cliente
                                if (_df.codigoCliente == 0)
                                {
                                    if (_df.regfacturacioncliente != "N")
                                    {
                                        _df.ClienteNroDoc = "0";
                                        _df.regfacturacioncliente = "4";
                                    }
                                }
                                else
                                {
                                    _df.regfacturacioncliente = String.IsNullOrEmpty(reader["RegimenFacturacion"].ToString().Trim()) ? "4" : reader["RegimenFacturacion"].ToString();
                                }
                                //Agrego el tipo de documento.
                                switch (_df.regfacturacioncliente)
                                {
                                    case "1":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "2":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "4":
                                        {
                                            if (_df.ClienteNroDoc != "0")
                                                _df.ClienteTipoDoc = "96";
                                            else
                                            {
                                                _df.ClienteTipoDoc = "99";
                                            }
                                            break;
                                        }
                                    case "5":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                    case "6":
                                        {
                                            _df.ClienteTipoDoc = "80";
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
                return _df;
            }
        }

        public class DatosFacturaExpo : IDisposable
        {
            public string numSerie { get; set; }
            public int numFactura { get; set; }
            public string N { get; set; }
            public DateTime fecha { get; set; }
            public int codCliente { get; set; }
            public decimal totalBruto { get; set; }
            public decimal totalNeto { get; set; }
            public string CodAfip { get; set; }
            public string TipoDoc { get; set; }
            public string PtoVta { get; set; }
            public decimal porcentajeDtoComercial { get; set; }
            public decimal totDtoComercial { get; set; }
            public decimal porcentajeDtoPp { get; set; }
            public decimal totDtoPp { get; set; }
            public string codigoMoneda { get; set; }
            public decimal cotizacion { get; set; }
            public string incoterms { get; set; }


            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }

            public static DatosFacturaExpo GetDatosFacturaExpo(string pnumSerie, string pnumFactura, string pN, string _Conection, string _pathLog)
            {
                //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los datos del comprobante");
                DatosFacturaExpo _dfe = new DatosFacturaExpo();
                try
                {

                    string _sql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, FACTURASVENTA.CODCLIENTE, " +
                        "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.TOTALNETO, " +
                        "SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                        "FACTURASVENTA.DTOCOMERCIAL, FACTURASVENTA.DTOPP, FACTURASVENTA.TOTDTOCOMERCIAL, FACTURASVENTA.TOTDTOPP, " +
                        "MONEDAS.CODIGOISONUM, FACTURASVENTA.FACTORMONEDA, FACTURASVENTACAMPOSLIBRES.INCOTERMS " +
                        "FROM FACTURASVENTA inner Join TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                        "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                        "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                        "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                        "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                        "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                        "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                        "WHERE FACTURASVENTA.NUMSERIE = @NumSerie AND FACTURASVENTA.NUMFACTURA = @NumFactura AND FACTURASVENTA.N = @N";

                    using (SqlConnection _conn = new SqlConnection(_Conection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@NumSerie", pnumSerie);
                            _cmd.Parameters.AddWithValue("@NumFactura", pnumFactura);
                            _cmd.Parameters.AddWithValue("@N", pN);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _dfe.numSerie = reader.GetString(0);
                                        _dfe.numFactura = reader.GetInt32(1);
                                        _dfe.N = reader.GetString(2);
                                        _dfe.fecha = reader.GetDateTime(3);
                                        _dfe.codCliente = reader.GetInt32(4);
                                        _dfe.totalBruto = Math.Abs(Convert.ToDecimal(reader[5], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totalNeto = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.TipoDoc = reader[7] == null ? "" : reader.GetString(7);
                                        _dfe.PtoVta = reader[8] == null ? "" : reader.GetString(8);
                                        _dfe.porcentajeDtoComercial = Math.Abs(Convert.ToDecimal(reader[9], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.porcentajeDtoPp = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totDtoComercial = Math.Abs(Convert.ToDecimal(reader[11], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totDtoPp = Math.Abs(Convert.ToDecimal(reader[12], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.codigoMoneda = String.IsNullOrEmpty(reader["CODIGOISONUM"].ToString()) ? "" : reader.GetString(13);
                                        _dfe.cotizacion = Math.Abs(Convert.ToDecimal(reader[14], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.incoterms = String.IsNullOrEmpty(reader["INCOTERMS"].ToString()) ? "" : reader.GetString(15);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al recuperar los datos de la factura de exportación. Error: " + ex.Message);
                }
                return _dfe;
            }
        }

        public class DatosClienteExpo
        {
            public int _codCliente { get; set; }
            public string _razonSocial { get; set; }
            public string _cuit { get; set; }
            public string _domicilio { get; set; }
            public string _idImpositivo { get; set; }
            public string _tipoExpo { get; set; }
            public string _paisDestino { get; set; }

            public static DatosClienteExpo GetDatosClienteExpo(int pCodClinte, string _Conection)
            {
                string _sql = "SELECT CLIENTES.CODCLIENTE, CLIENTES.NOMBRECLIENTE, CLIENTES.NIF20, CLIENTES.DIRECCION1, CLIENTESCAMPOSLIBRES.TIPO_EXPO, PAISES.ZONA " +
                    "FROM CLIENTES inner join CLIENTESCAMPOSLIBRES on CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN PAISES ON CLIENTES.CODPAIS = PAISES.CODPAIS " +
                    "WHERE CLIENTES.CODCLIENTE = @CodCliente";

                DatosClienteExpo _dfe = new DatosClienteExpo();

                try
                {
                    using (SqlConnection _conn = new SqlConnection(_Conection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@CodCliente", pCodClinte);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _dfe._codCliente = reader.GetInt32(0);
                                        _dfe._razonSocial = reader[1] == null ? "" : reader.GetString(1);
                                        _dfe._cuit = reader[2] == null ? "" : reader.GetString(2);
                                        _dfe._domicilio = reader[3] == null ? "" : reader.GetString(3);
                                        _dfe._tipoExpo = reader[4] == null ? "" : reader.GetString(4);
                                        _dfe._paisDestino = reader[5] == null ? "" : reader.GetString(5);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al recuperar los datos del cliente. Erro: " + ex.Message);
                }

                return _dfe;
            }
        }

        public class DatosItemsExpo
        {
            public string numserie { get; set; }
            public int numalbaran { get; set; }
            public string n { get; set; }
            public int numlin { get; set; }
            public string referencia { get; set; }
            public string descripcion { get; set; }
            public decimal unidades { get; set; }
            public decimal precio { get; set; }
            public decimal total { get; set; }
            public string codigoaduana { get; set; }
            public decimal dto { get; set; }

            public static List<DatosItemsExpo> GetDatosItemsExpo(string pnumserie, int pnumalbaran, string pn, string pconnection)
            {

                string _sql = "SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN, ALBVENTALIN.REFERENCIA, " +
                    "ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL AS UNID1, ALBVENTALIN.PRECIO, ALBVENTALIN.TOTAL, ARTICULOS.CODIGOADUANA, ALBVENTALIN.DTO " +
                    "FROM FACTURASVENTA INNER JOIN ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC and FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC and FACTURASVENTA.N = ALBVENTACAB.NFAC " +
                    "INNER JOIN ALBVENTALIN on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N " +
                    "INNER JOIN ARTICULOS on albventalin.codarticulo = articulos.codarticulo " +
                    "WHERE FACTURASVENTA.NUMSERIE = @NumSerie and FACTURASVENTA.NUMFACTURA = @NumAlbaran and FACTURASVENTA.N = @N";

                List<DatosItemsExpo> _dfe = new List<DatosItemsExpo>();
                try
                {
                    using (SqlConnection _conn = new SqlConnection(pconnection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@NumSerie", pnumserie);
                            _cmd.Parameters.AddWithValue("@NumAlbaran", pnumalbaran);
                            _cmd.Parameters.AddWithValue("@N", pn);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        DatosItemsExpo _it = new DatosItemsExpo();

                                        _it.numserie = reader.GetString(0);
                                        _it.numalbaran = reader.GetInt32(1);
                                        _it.n = reader.GetString(2);
                                        _it.numlin = reader.GetInt32(3);
                                        _it.referencia = reader[4] == null ? "" : reader.GetString(4);
                                        _it.descripcion = reader[5] == null ? "" : reader.GetString(5);
                                        _it.unidades = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.precio = Math.Abs(Convert.ToDecimal(reader[7], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.total = Math.Abs(Convert.ToDecimal(reader[8], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.codigoaduana = reader[9] == null ? "" : reader[9].ToString();
                                        _it.dto = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));

                                        _dfe.Add(_it);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al recuperar los datos de los items de la factura. Error: " + ex.Message);
                }
                return _dfe;
            }
        }

        public class RemTransacciones
        {
            public static void GraboRemTransacciones(string _terminal, string _caja, string _z, int _tipo, int _accion,
            string _serie, string _numero, string _N, SqlConnection _con)
            {
                try
                {
                    string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                        "VALUES(@terminal, @caja, 0, @Z, @tipo, @accion, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

                    using (SqlCommand _cmd = new SqlCommand(_sql, _con))
                    {
                        _cmd.Parameters.AddWithValue("@terminal", _terminal);
                        _cmd.Parameters.AddWithValue("@caja", _caja);
                        _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                        _cmd.Parameters.AddWithValue("@tipo", _tipo);
                        _cmd.Parameters.AddWithValue("@accion", _accion);
                        _cmd.Parameters.AddWithValue("@serie", _serie);
                        _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                        _cmd.Parameters.AddWithValue("@N", _N);
                        _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                        _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                        int rta = _cmd.ExecuteNonQuery();
                    }
                }
                catch { }
            }

            public static bool InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, SqlConnection _con)
            {
                bool _rta = false;
                try
                {
                    string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.SERIE, t.NUMERO, t.N, '0', 1
                from TIQUETSCAB t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                where t.SERIE = @Serie and t.NUMERO = @Numero and t.N = @N";

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;

                        _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                        _cmd.Parameters.AddWithValue("@Serie", _serie);
                        _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                        _cmd.Parameters.AddWithValue("@N", _N);

                        _cmd.ExecuteNonQuery();
                    }
                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }

            public static void Retail_InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, int _accion, SqlConnection _con)
            {
                try
                {
                    string _sql = @"INSERT INTO rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                    SELECT @Terminal, 0, @Accion, t.CAJA, CASE WHEN isnull(t.Z, 0) = 0 THEN z.Z + 1 ELSE t.Z END, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                    FROM FACTURASVENTA t LEFT JOIN (SELECT Caja, max(Numero) Z FROM ARQUEOS WHERE Arqueo = 'Z' GROUP BY Caja) z on z.Caja = t.CAJA
                    WHERE t.NUMSERIE = @Serie AND t.NUMFACTURA = @Numero AND t.N = @N";

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        
                        _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                        _cmd.Parameters.AddWithValue("@Accion", _accion);
                        _cmd.Parameters.AddWithValue("@Serie", _serie);
                        _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                        _cmd.Parameters.AddWithValue("@N", _N);

                        _cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("InsertRemTransacciones. Error: " + ex.Message);
                }

            }

            public static bool InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, SqlConnection _con, SqlTransaction _trn)
            {
                bool _rta = false;
                try
                {
                    string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.SERIE, t.NUMERO, t.N, '0', 1
                from TIQUETSCAB t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                where t.SERIE = @Serie and t.NUMERO = @Numero and t.N = @N";

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        _cmd.Transaction = _trn;

                        _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                        _cmd.Parameters.AddWithValue("@Serie", _serie);
                        _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                        _cmd.Parameters.AddWithValue("@N", _N);

                        _cmd.ExecuteNonQuery();
                    }
                    _rta = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _rta;
            }

            public static void InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, int _accion, SqlConnection _con, SqlTransaction _tran)
            {
                try
                {
                    string _sql = @"INSERT INTO rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                    SELECT @Terminal, 0, @Accion, t.CAJA, CASE WHEN isnull(t.Z, 0) = 0 THEN z.Z + 1 ELSE t.Z END, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                    FROM FACTURASVENTA t LEFT JOIN (SELECT Caja, max(Numero) Z FROM ARQUEOS WHERE Arqueo = 'Z' GROUP BY Caja) z on z.Caja = t.CAJA
                    WHERE t.NUMSERIE = @Serie AND t.NUMFACTURA = @Numero AND t.N = @N";

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = _con;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.CommandText = _sql;
                        _cmd.Transaction = _tran;

                        _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                        _cmd.Parameters.AddWithValue("@Accion", _accion);
                        _cmd.Parameters.AddWithValue("@Serie", _serie);
                        _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                        _cmd.Parameters.AddWithValue("@N", _N);

                        _cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("InsertRemTransacciones. Error: " + ex.Message);
                }

            }
        }

        public class FuncionesVarias
        {
            public static bool GenerarCodigoBarra(string _Cuit, string _TipoComprobate, string _PtoVta, string _CAE, string _Vto, out string _CodigoBarra)
            {
                bool _rta = false;
                //Armo el string de todo.
                int _tipoComprobante = Convert.ToInt32(_TipoComprobate);
                string strTodo = _Cuit + _tipoComprobante.ToString().PadLeft(3, '0') + _PtoVta.PadLeft(5, '0') + _CAE + _Vto;
                //LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "CUIT: " + _Cuit + ", TipoComprobante: " + _tipoComprobante.ToString().PadLeft(2, '0') + ", PtoVta: " + _PtoVta.PadLeft(4, '0') + ", CAE: " + _CAE + ", FVTO: " + _Vto);
                try
                {
                    int intTamaño = strTodo.Length;
                    int[] a = new int[intTamaño];
                    int pares = 0, impares = 0;
                    //Cargamos el array con los digitos.
                    for (int i = 0; i < intTamaño; i++)
                    {
                        a[i] = int.Parse(strTodo[i].ToString());
                    }
                    //Sumamos las posiciones pares y las impares por separado.
                    for (int i = 0; i < 10; i++)
                    {
                        if ((i % 2) == 0)
                            pares += a[i];
                        else
                            impares += a[i];
                    }
                    //Dividimos y sumamos pares
                    int intT = (impares / 3) + pares;
                    //Recupero el ultimo digito de la suma.
                    int intUlimoDigito = int.Parse(intT.ToString().Substring(intT.ToString().Length - 1, 1));
                    //Vemos si el ultimo digito es 0
                    if (intUlimoDigito == 0)
                        strTodo = strTodo + intUlimoDigito.ToString();
                    else
                    {
                        intUlimoDigito = 10 - intUlimoDigito;
                        strTodo = strTodo + intUlimoDigito.ToString();
                    }

                    _rta = true;
                }
                catch(Exception sx)
                {
                    _rta = false;
                    strTodo = "";
                }
                _CodigoBarra = strTodo;

                return _rta;
            }
            public static bool GenerarCodigoBarra(string _Cuit, string _TipoComprobate, string _PtoVta, string _CAE, string _Vto, string _PathLog, out string _CodigoBarra)
            {
                bool _rta = false;
                //Armo el string de todo.
                int _tipoComprobante = Convert.ToInt32(_TipoComprobate);
                string strTodo = _Cuit + _tipoComprobante.ToString().PadLeft(3, '0') + _PtoVta.PadLeft(5, '0') + _CAE + _Vto;
                try
                {
                    int intTamaño = strTodo.Length;
                    int[] a = new int[intTamaño];
                    int pares = 0, impares = 0;
                    //Cargamos el array con los digitos.
                    for (int i = 0; i < intTamaño; i++)
                    {
                        a[i] = int.Parse(strTodo[i].ToString());
                    }
                    //Sumamos las posiciones pares y las impares por separado.
                    for (int i = 0; i < 10; i++)
                    {
                        if ((i % 2) == 0)
                            pares += a[i];
                        else
                            impares += a[i];
                    }
                    //Dividimos y sumamos pares
                    int intT = (impares / 3) + pares;
                    //Recupero el ultimo digito de la suma.
                    int intUlimoDigito = int.Parse(intT.ToString().Substring(intT.ToString().Length - 1, 1));
                    //Vemos si el ultimo digito es 0
                    if (intUlimoDigito == 0)
                        strTodo = strTodo + intUlimoDigito.ToString();
                    else
                    {
                        intUlimoDigito = 10 - intUlimoDigito;
                        strTodo = strTodo + intUlimoDigito.ToString();
                    }

                    _rta = true;
                }
                catch (Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "GenerarCodigoBarra Error: " + ex.Message);
                    _rta = false;
                    strTodo = "";
                }
                _CodigoBarra = strTodo;

                return _rta;
            }

            /// <summary>
            /// Calcula el dígito verificador dado un CUIT completo o sin él.
            /// </summary>
            /// <param name="cuit">El CUIT como String sin guiones</param>
            /// <returns>El valor del dígito verificador calculado.</returns>
            public static bool ValidarDigitoCuit(string cuit)
            {
                try
                {
                    int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                    char[] nums = cuit.ToCharArray();
                    int total = 0;
                    for (int i = 0; i < mult.Length; i++)
                    {
                        total += int.Parse(nums[i].ToString()) * mult[i];
                    }
                    int resto = total % 11;
                    int _digito1 = resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
                    int _digitorecibido = Convert.ToInt16(cuit.Substring(cuit.Length - 1));

                    if (_digito1 == _digitorecibido)
                        return true;
                    else
                        return false;
                }
                catch(Exception ex)
                {
                    throw new Exception("ValidarDigitoCuit. Error: " + ex.Message);
                }
            }

            public static string GetCodAfip(string p)
            {
                return p.Split(' ')[0].ToString();
            }

            public static string GetCodClienteDoc(string p)
            {
                return p.Split('_')[0].ToString();
            }

            public static void ValidarDatosFactura(DataAccess.DatosFactura _fc)
            {
                if (String.IsNullOrEmpty(_fc.ClienteNroDoc))
                    throw new Exception("ICG-VAL. El Cliente no posee Nro. de Documento.");

                if (String.IsNullOrEmpty(_fc.ClienteTipoDoc))
                    throw new Exception("ICG-VAL. El Cliente no posee Tipo de Documento.");

                if (String.IsNullOrEmpty(_fc.PtoVta))
                    throw new Exception("ICG-VAL. El punto de Vta no esta definido.");

                if (String.IsNullOrEmpty(_fc.cCodAfip))
                    throw new Exception("ICG-VAL. El codigo AFIP no esta definido.");
                //Validamos el CUIT
                string _CodAfipCliente = GetCodClienteDoc(_fc.ClienteTipoDoc);
                if (_CodAfipCliente == "80")
                {
                    if (!ValidarDigitoCuit(_fc.ClienteNroDoc.Replace("-", "")))
                        throw new Exception("ICG-VAL. El CUIT ingresado no es valido.");
                }
                else
                {
                    try { Convert.ToInt64(_fc.ClienteNroDoc); }
                    catch (Exception ex) { throw new Exception("ICG-VAL. El Nro de documento del cliente ingresado no es valido."); }
                }
            }

            public static void ValidarCarpetas(string _nombreCertificado,
                out string _pathApp, out string _pathCerificado, out string _pathLog, out string _pathTaFC)
            {
                try
                {
                    //recupero la ubicacion del ejecutable.
                    _pathApp = System.Windows.Forms.Application.StartupPath.ToString();
                    //Cargo la ubicaciones.
                    _pathCerificado = _pathApp + "\\Certificado";
                    _pathLog = _pathApp + "\\Log";
                    _pathTaFC = _pathApp + "\\TAFC";

                    //Validamos que existan los Directorios
                    if (!Directory.Exists(_pathCerificado))
                    {
                        //Si no existe la creamos.
                        Directory.CreateDirectory(_pathCerificado);
                        //Informamos que debe instalar el certificado en la ruta.
                        throw new Exception("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCerificado + "). Por favor coloque el certificado en este directorio para poder continuar.");
                    }
                    else
                    {
                        if (!File.Exists(_pathCerificado + "\\" + _nombreCertificado))
                        {
                            throw new Exception("El certificado no se encuentra en la directorio " + _pathCerificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        }
                        else
                            _pathCerificado = _pathCerificado + "\\" + _nombreCertificado;
                    }
                    //Validamos el log.
                    if (!Directory.Exists(_pathLog))
                        Directory.CreateDirectory(_pathLog);
                    //Validamos el FC
                    if (!Directory.Exists(_pathTaFC))
                        Directory.CreateDirectory(_pathTaFC);
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error: " + ex.Message + " validando los directorios.");
                }
            }

            public static wsAfip.clsDetReq CargarDetalle(DataAccess.DatosFactura _Fac, int _NroComprobante)
            {
                try
                {
                    wsAfip.clsDetReq _detalle = new wsAfip.clsDetReq();
                    _detalle.Concepto = 2;
                    _detalle.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
                    _detalle.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
                    _detalle.CbteDesde = _NroComprobante;
                    _detalle.CbteHasta = _NroComprobante;
                    _detalle.CbteFch = _Fac.strFecha;
                    //if (_Fac.TotalNeto.StartsWith("-"))
                    //    _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto) * -1);
                    //else
                    //    _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto);
                    _detalle.ImpTotal = Math.Round(Math.Abs(Convert.ToDouble(_Fac.TotalNeto)), 2);
                    _detalle.ImpTotConc = 0;
                    //if (_Fac.TotalBruto.StartsWith("-"))
                    //    _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto) * -1;
                    //else
                    //    _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto);
                    _detalle.ImpNeto = Math.Round(Math.Abs(Convert.ToDouble(_Fac.TotalBruto)), 2);
                    _detalle.ImpOpEx = 0;
                    //if (_Fac.TotIva < 0)
                    //    _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva) * -1;
                    //else
                    //    _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva);
                    _detalle.ImpIVA = Math.Round(Math.Abs(Convert.ToDouble(_Fac.TotIva)), 2);
                    //if (_Fac.totTributos < 0)
                    //    _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos) * -1;
                    //else
                    //    _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos);
                    _detalle.ImpTrib = Math.Round(Math.Abs(Convert.ToDouble(_Fac.totTributos)), 2);
                    _detalle.FchServDesde = _Fac.strFecha;
                    _detalle.FchServHasta = _Fac.strFecha;
                    _detalle.FchVtoPago = _Fac.strFecha;
                    _detalle.MonID = "PES";
                    _detalle.MonCotiz = 1;

                    return _detalle;
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al CargarDetalle. Error: " + ex.Message);
                }
            }

            public static List<AfipDll.wsAfipCae.FECAEADetRequest> CargarDetalleCAEAAfip(DataAccess.DatosFactura _Fac, int _NroComprobante, AfipDll.CAEA _caea)
            {
                List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();
                
                try
                {
                    AfipDll.wsAfipCae.FECAEADetRequest _det = new AfipDll.wsAfipCae.FECAEADetRequest();
                    _det.Concepto = 2;
                    _det.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
                    _det.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
                    _det.CbteDesde = _NroComprobante;
                    _det.CbteHasta = _NroComprobante;
                    _det.CbteFch = _Fac.strFecha;
                    _det.ImpTotal = Math.Abs(Convert.ToDouble(_Fac.TotalNeto));
                    _det.ImpTotConc = 0;
                    _det.ImpNeto = Math.Abs(Convert.ToDouble(_Fac.TotalBruto));
                    _det.ImpOpEx = 0;
                    _det.ImpIVA = Math.Abs(Convert.ToDouble(_Fac.TotIva));
                    _det.ImpTrib = Math.Abs(Convert.ToDouble(_Fac.totTributos));
                    _det.FchServDesde = _Fac.strFecha;
                    _det.FchServHasta = _Fac.strFecha;
                    _det.FchVtoPago = _Fac.strFecha;
                    _det.MonId = "PES";
                    _det.MonCotiz = 1;
                    _det.CAEA = _caea.Caea;

                    _detReq.Add(_det);

                    return _detReq;
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al CargarDetalle. Error: " + ex.Message);
                }
            }

            public static List<wsAfip.clsIVA> ArmarIVA(List<DataAccess.FacturasVentasTot> _Iva)
            {
                List<AfipDll.wsAfip.clsIVA> _Rta = new List<AfipDll.wsAfip.clsIVA>();
                foreach (DataAccess.FacturasVentasTot itm in _Iva)
                {
                    AfipDll.wsAfip.clsIVA cls = new AfipDll.wsAfip.clsIVA();
                    switch (itm.Iva.ToString())
                    {
                        case "0":
                            {
                                cls.ID = 3;
                                break;
                            }
                        case "21":
                            {
                                cls.ID = 5;
                                break;
                            }
                        case "10.5":
                        case "10,5":
                            {
                                cls.ID = 4;
                                break;
                            }
                        case "27":
                            {
                                cls.ID = 6;
                                break;
                            }
                    }
                    if (itm.BaseImponible < 0)
                        cls.BaseImp = Convert.ToDouble(itm.BaseImponible) * -1;
                    else
                        cls.BaseImp = Convert.ToDouble(itm.BaseImponible);
                    if (itm.TotIva < 0)
                        cls.Importe = Convert.ToDouble(itm.TotIva) * -1;
                    else
                        cls.Importe = Convert.ToDouble(itm.TotIva);
                    _Rta.Add(cls);
                }
                return _Rta;
            }
            public static List<AfipDll.wsAfip.clsIVA> ArmarIVA2(List<DataAccess.FacturasVentasTot> _Iva)
            {
                List<AfipDll.wsAfip.clsIVA> _Rta = new List<AfipDll.wsAfip.clsIVA>();
                foreach (DataAccess.FacturasVentasTot itm in _Iva)
                {
                    if (!itm.Nombre.ToUpper().Contains("EXENTO") && !itm.Nombre.ToUpper().Contains("NO GRAVADO"))
                    {
                        AfipDll.wsAfip.clsIVA cls = new AfipDll.wsAfip.clsIVA();
                        switch (itm.Iva.ToString())
                        {
                            case "0":
                                {
                                    cls.ID = 3;
                                    break;
                                }
                            case "21":
                                {
                                    cls.ID = 5;
                                    break;
                                }
                            case "10.5":
                            case "10,5":
                                {
                                    cls.ID = 4;
                                    break;
                                }
                            case "27":
                                {
                                    cls.ID = 6;
                                    break;
                                }
                        }
                        if (itm.BaseImponible < 0)
                            cls.BaseImp = Convert.ToDouble(itm.BaseImponible) * -1;
                        else
                            cls.BaseImp = Convert.ToDouble(itm.BaseImponible);
                        if (itm.TotIva < 0)
                            cls.Importe = Convert.ToDouble(itm.TotIva) * -1;
                        else
                            cls.Importe = Convert.ToDouble(itm.TotIva);
                        if (cls.ID == 3 || cls.ID == 4 || cls.ID == 5 || cls.ID == 6)
                            _Rta.Add(cls);
                    }
                }
                return _Rta;
            }

            public static List<AfipDll.wsAfipCae.AlicIva> ArmarIVAAfip(List<DataAccess.FacturasVentasTot> _Iva)
            {
                List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();
                
                foreach (DataAccess.FacturasVentasTot itm in _Iva)
                {
                    AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                    switch (itm.Iva.ToString())
                    {
                        case "0":
                            {
                                _al.Id = 3;
                                break;
                            }
                        case "21":
                            {
                                _al.Id = 5;
                                break;
                            }
                        case "10.5":
                        case "10,5":
                            {
                                _al.Id = 4;
                                break;
                            }
                        case "27":
                            {
                                _al.Id = 6;
                                break;
                            }
                    }
                    _al.BaseImp = Math.Abs(Convert.ToDouble(itm.BaseImponible));
                    _al.Importe = Math.Abs(Convert.ToDouble(itm.TotIva));
                    _alicIVA.Add(_al);
                }
                return _alicIVA;
            }

            public static List<wsAfip.clsTributos> ArmaTributos(List<Modelos.FacturasVentasTot> _Tributos)
            {
                //string strRta = "";
                double totalBaseImponible = 0;
                double totalImpuesto = 0;
                List<wsAfip.clsTributos> _Rta = new List<wsAfip.clsTributos>();
                foreach (Modelos.FacturasVentasTot itm in _Tributos)
                {
                    totalBaseImponible = totalBaseImponible + Convert.ToDouble(itm.BaseImponible);
                    totalImpuesto = totalImpuesto + Convert.ToDouble(itm.TotIva);

                }
                //if (String.IsNullOrEmpty(strRta))
                //    strRta = "NT";
                if (totalImpuesto != 0)
                {
                    wsAfip.clsTributos cls = new wsAfip.clsTributos();
                    cls.Alic = 3;
                    if (totalBaseImponible < 0)
                        cls.BaseImp = totalBaseImponible * -1;
                    else
                        cls.BaseImp = totalBaseImponible;
                    cls.Desc = "Percep IIBB";
                    cls.ID = 2;
                    if (totalImpuesto < 0)
                        cls.Importe = Math.Round(totalImpuesto * -1, 2);
                    else
                        cls.Importe = Math.Round(totalImpuesto, 2);
                    _Rta.Add(cls);
                    //strRta = "02|Percep IIBB|"+ totalBaseImponible.ToString("G") + "|3|" + totalImpuesto.ToString("G");
                }
                return _Rta;
            }

            public static List<AfipDll.wsAfipCae.Tributo> ArmaTributosAfip(List<DataAccess.FacturasVentasTot> _Tributos)
            {
                double totalBaseImponible = 0;
                double totalImpuesto = 0;

                List<AfipDll.wsAfipCae.Tributo> _rta = new List<AfipDll.wsAfipCae.Tributo>();

                foreach (DataAccess.FacturasVentasTot itm in _Tributos)
                {
                    totalBaseImponible = totalBaseImponible + Convert.ToDouble(itm.BaseImponible);
                    totalImpuesto = totalImpuesto + Convert.ToDouble(itm.TotIva);

                }

                if (totalImpuesto != 0)
                {
                    AfipDll.wsAfipCae.Tributo cls = new AfipDll.wsAfipCae.Tributo();

                    cls.Alic = 3;
                    if (totalBaseImponible < 0)
                        cls.BaseImp = totalBaseImponible * -1;
                    else
                        cls.BaseImp = totalBaseImponible;
                    cls.Desc = "Percep IIBB";
                    cls.Id = 2;
                    if (totalImpuesto < 0)
                        cls.Importe = totalImpuesto * -1;
                    else
                        cls.Importe = totalImpuesto;
                    _rta.Add(cls);
                }
                return _rta;
            }

            public static string GetStringFromErrors(List<wsAfip.Errors> _list)
            {
                string _rta = "";

                foreach (wsAfip.Errors _er in _list)
                {
                    _rta = _rta + "! Codigo: " + _er.Code + ". Descripción: " + _er.Msg;
                }

                return _rta;
            }

            public static List<string> GetListFromErrors(List<wsAfip.Errors> _list)
            {
                List<string> _rta = new List<string>();                

                foreach (wsAfip.Errors _er in _list)
                {
                    string _msg = "! Codigo: " + _er.Code + ". Descripción: " + _er.Msg;
                    _rta.Add(_msg);
                }

                return _rta;
            }

            public static string QueComprobanteEs(string serie, string numero, string n, string vendedor, SqlConnection _con)
            {
                string _sql = @"select  TIPOSDOC.DESCRIPCION from FACTURASVENTA inner join TIPOSDOC on FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE FACTURASVENTA.NUMSERIE = @serie AND FACTURASVENTA.NUMFACTURA = @numero AND FACTURASVENTA.N = @n";
                    //AND FACTURASVENTA.CODVENDEDOR = @codVendedor";

                string _rta = "";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    //_cmd.Parameters.AddWithValue("@fo", fo);
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@n", n);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@codVendedor", vendedor);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _rta = _reader["DESCRIPCION"].ToString();
                            }
                        }
                    }
                }

                return _rta;
            }

            public static SeriesResol GetContador(string serieresol, string numresol, SqlConnection _con)
            {
                string _sql = @"select SERIERESOL,NUMRESOL, FECHA, NUMEROINICIAL, NUMEROFINAL, ACTIVO, CONTADOR, FECHAINGRESO, FECHAVENCIMIENTO 
                        from SERIESRESOLUCION Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

                SeriesResol _cls = new SeriesResol();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                    _cmd.Parameters.AddWithValue("@NumResol", numresol);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.activo = Convert.ToInt32(_reader["ACTIVO"]);
                                _cls.contador = _reader["CONTADOR"] == DBNull.Value ? 0 : Convert.ToInt32(_reader["CONTADOR"]);
                                _cls.fecha = _reader["FECHA"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHA"]);
                                _cls.fechaingreso = _reader["FECHAINGRESO"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHAINGRESO"]);
                                _cls.fechavencimiento = _reader["FECHAVENCIMIENTO"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(_reader["FECHAVENCIMIENTO"]);
                                _cls.numerofinal = Convert.ToInt32(_reader["NUMEROFINAL"]);
                                _cls.numeroinicial = Convert.ToInt32(_reader["NUMEROINICIAL"]);
                                _cls.numresol = _reader["NUMRESOL"].ToString();
                                _cls.serieresol = _reader["SERIERESOL"].ToString();
                            }
                        }
                    }
                }

                return _cls;
            }

            public static bool UpdateContador(string serieresol, string numresol, SqlConnection _con)
            {
                string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = CONTADOR + 1 
                            Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                    _cmd.Parameters.AddWithValue("@NumResol", numresol);

                    try
                    {
                        _cmd.ExecuteNonQuery();
                        _rta = true;
                    }
                    catch
                    {
                        _rta = false;
                    }
                }

                return _rta;
            }

            public static bool UpdateContador(string serieresol, string numresol, SqlConnection _con, SqlTransaction _tran)
            {
                string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = CONTADOR + 1 
                            Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                    _cmd.Parameters.AddWithValue("@NumResol", numresol);

                    try
                    {
                        _cmd.ExecuteNonQuery();
                        _rta = true;
                    }
                    catch
                    {
                        _rta = false;
                    }
                }

                return _rta;
            }

            public static bool UpdateContador(string serieresol, string numresol, int _numero, SqlConnection _con, SqlTransaction _tran)
            {
                string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = @Numero 
                            Where SERIERESOL = @SerieResol and NUMRESOL = @NumResol";

                bool _rta = false;

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@SerieResol", serieresol);
                    _cmd.Parameters.AddWithValue("@NumResol", numresol);
                    _cmd.Parameters.AddWithValue("@Numero", _numero);

                    try
                    {
                        _cmd.ExecuteNonQuery();
                        _rta = true;
                    }
                    catch
                    {
                        _rta = false;
                    }
                }

                return _rta;
            }
        }

        public class Empresas
        {
            public int codigoEmpresa { get; set; }
            public string nombreEmpresa { get; set; }
            public string razonSocial { get; set; }
            public string cuitEmpresa { get; set; }
            public string nombreCertificado { get; set; }

            public static List<Empresas> GetEmpresas(string _connexion)
            {
                string strSql = "SELECT Empresa, Nombre, RazonSocial, CUIT, Web FROM v_XsEmpresas";

                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Empresas> _lst = new List<Empresas>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Empresas _rw = new Empresas();

                                _rw.codigoEmpresa = reader.GetInt32(0);
                                _rw.nombreEmpresa = reader["Nombre"] == DBNull.Value ? "" : reader.GetString(1);
                                _rw.razonSocial = reader["RazonSocial"] == DBNull.Value ? "" : reader.GetString(2);
                                _rw.cuitEmpresa = reader["CUIT"] == DBNull.Value ? "" : reader.GetString(3);
                                _rw.nombreCertificado = reader["Web"] == DBNull.Value ? "" : reader.GetString(4);

                                _lst.Add(_rw);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _lst;
            }
        }

        public class SeriesResol
        {
            public string serieresol { get; set; }
            public string numresol { get; set; }
            public DateTime? fecha { get; set; }
            public int numeroinicial { get; set; }
            public int numerofinal { get; set; }
            public int activo { get; set; }
            public int contador { get; set; }
            public DateTime? fechaingreso { get; set; }
            public DateTime? fechavencimiento { get; set; }
        }

        public class IcgCAEA
        {
            public static List<ComprobantesConCAEA> GetComprobantesConCAEA(string _caea, SqlConnection _conexion)
            {
                string _sql = @"select FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTACAMPOSLIBRES.CAE, 
                FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, 
                FACTURASVENTACAMPOSLIBRES.VTO_CAE, FACTURASVENTA.CODVENDEDOR
                FROM FACTURASVENTA inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE
                and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA
                and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                Where FACTURASVENTACAMPOSLIBRES.CAE = @CAEA AND FACTURASVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR'
                and (FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO is null
                OR FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO = '')";

                List<ComprobantesConCAEA> _lst = new List<ComprobantesConCAEA>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@CAEA", _caea);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ComprobantesConCAEA _cls = new ComprobantesConCAEA();
                                    _cls.N = reader["N"].ToString();
                                    _cls.Numfactura = Convert.ToInt32(reader["NUMFACTURA"]);
                                    _cls.Numserie = reader["NUMSERIE"].ToString();
                                    _cls.CAE = reader["CAE"].ToString();
                                    _cls.CAEA_Informado = reader["CAEA_INFORMADO"] == null ? "" : reader["CAEA_INFORMADO"].ToString();
                                    _cls.CodVendedor = Convert.ToInt32(reader["CODVENDEDOR"]);
                                    _cls.Error_CAE = reader["ERROR_CAE"].ToString();
                                    _cls.Estado_FE = reader["ESTADO_FE"].ToString();
                                    //_cls.Fo = Convert.ToInt32(reader["FO"]);

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        throw new Exception(ex.Message);
                    }
                }

                return _lst;
            }

            public static AfipDll.CAEA GetCAEAEnCurso(SqlConnection _connection)
            {
                try
                {
                    int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                    int _quincena;

                    if (DateTime.Now.Day > 15)
                        _quincena = 2;
                    else
                        _quincena = 1;

                    //2- recupero el CAEA de la base de datos
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);

                    return _caea;
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public class ComprobantesConCAEA
        {
            public string Numserie { get; set; }
            public int Numfactura { get; set; }
            public string N { get; set; }

            public int Fo { get; set; }
            public string CAE { get; set; }
            public string Error_CAE { get; set; }
            public string Vto_CAE { get; set; }
            public string Estado_FE { get; set; }
            public string CAEA_Informado { get; set; }
            public int CodVendedor { get; set; }
        }

        public class Transactions
        {
            public static bool TransactionCAEA(string _serie, string _numero, string _n, string _codvendedor, 
                string _ptovtacaea, string _cuit, string _pathlog, SqlConnection _connection)
            {
                //obtengo el tipo de comprobante.
                string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codvendedor, _connection);
                //armo la fecha de vto como la de hoy.
                string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                string _codBarra;
                //Recupero el contador.
                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection);
                //Obtengo el CAEA para el periodo en curso.
                CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                //Genero el codigo de barras.
                DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _tipoComprobante.Substring(0, 3), _ptovtacaea, _caeaEnCurso.Caea, _fechaVto, out _codBarra);

                SqlTransaction _trn;
                _trn = _connection.BeginTransaction();
                try
                {
                    //Modificamos el registro en FacturasCamposLibres.
                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serie, _numero, _n, _caeaEnCurso.Caea, "", _fechaVto, _codBarra, _connection, _trn);
                    //insertamos el comprobante de FacturasVentasSeriesResol
                    DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_serie, _numero, _n, _ptovtacaea, _tipoComprobante, _dtoContador.contador + 1, _connection, _trn);
                    //Recupero el nombre de la terminal.
                    string _terminal = Environment.MachineName;
                    //Inserto en Rem_transacciones.
                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, _connection, _trn);
                    //Modifico el contador.
                    DataAccess.FuncionesVarias.UpdateContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection, _trn);
                    //Comiteo
                    _trn.Commit();
                    return true;
                }
                catch(Exception ex)
                {
                    //Se produjo un error, Rollback.
                    _trn.Rollback();
                    //Log
                    LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA Error: " + ex.Message);
                    return false;
                    //throw new Exception(ex.Message);
                }
            }

            public static bool TransactionCAEA2(string _serie, string _numero, string _n, string _codvendedor,
                string _ptovtacaea, string _cuit, string _pathlog, SqlConnection _connection)
            {
                try
                {
                    if (_connection.State == System.Data.ConnectionState.Closed)
                        _connection.Open();
                    //Recupero el tipo de comprobante.
                    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codvendedor, _connection);
                    //Valido los datos del comprobante.
                    if (String.IsNullOrEmpty(_tipoComprobante))
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No está definido el tipo de comprobante en TIPOSDOC.DESCRIPCION.");
                        string _rta = "No está definido el tipo de comprobante en TIPOSDOC.DESCRIPCION." + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        return false;
                    }

                    //Recupero los datos de la FC.
                    //Factura Electronica comun.
                    DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(_serie, Convert.ToInt32(_numero), _n, _connection);
                    
                    string _codBarra;
                    //Recupero el contador.
                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection);
                    //Valido que tengo el Contador de CAEA.
                    if(_dtoContador.serieresol == null)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No se encontró un contador definido para el punto de Venta: " + _ptovtacaea);
                        string _rta = "No se encontró un contador definido para el punto de Venta: " + _ptovtacaea + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        return false;
                    }
                    //Obtengo el CAEA para el periodo en curso.
                    CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                    if (String.IsNullOrEmpty(_caeaEnCurso.Caea))
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: No se encontró un CAEA para el periodo del comprobante.");
                        string _rta = "No se encontró un CAEA para el periodo del comprobante." + Environment.NewLine +
                            "Tramite uno desde la consola y luego intente fiscalizarlo." + Environment.NewLine +
                                                    "Por favor comuníquese con ICG Argentina.";
                        MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        return false;
                    }
                    //Armo la fecha
                    string _fechaVto = _caeaEnCurso.FechaTope.Year.ToString().PadLeft(4, '0') + _caeaEnCurso.FechaTope.Month.ToString().PadLeft(2, '0') + _caeaEnCurso.FechaTope.Day.ToString().PadLeft(2, '0');
                    //Genero el codigo de barras.
                    DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _tipoComprobante.Substring(0, 3), _ptovtacaea, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                    //Genero el codigo del QR.
                    string _qr = QR.CrearJson(1, DateTime.Now, Convert.ToInt64(_cuit), Convert.ToInt32(_ptovtacaea),
                        Convert.ToInt32(_tipoComprobante.Substring(0, 3)),
                        _dtoContador.contador + 1, Convert.ToDecimal(_Fc.TotalNeto), "PES", 1,
                        Convert.ToInt32(_Fc.ClienteTipoDoc), Convert.ToInt64(_Fc.ClienteNroDoc), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                    //
                    string clPtoVta = _ptovtacaea.PadLeft(5, '0') + "-" + (_dtoContador.contador + 1).ToString().PadLeft(8, '0');
                    SqlTransaction _trn;
                    _trn = _connection.BeginTransaction();
                    try
                    {
                        //Modificamos el registro en FacturasCamposLibres.
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serie, _numero, _n, _caeaEnCurso.Caea, 
                            "CAEA", _fechaVto, _codBarra, _qr, clPtoVta, _connection, _trn);
                        //insertamos el comprobante de FacturasVentasSeriesResol
                        DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_serie, _numero, _n, _ptovtacaea, _tipoComprobante, _dtoContador.contador + 1, _connection, _trn);
                        //Insertamos en TransaccionesAFIP
                        CommonService.InfoTransaccionAFIP _afip = new CommonService.InfoTransaccionAFIP();
                        _afip.cae = _caeaEnCurso.Caea;
                        _afip.codigobara = _codBarra;
                        _afip.codigoqr = _qr;
                        _afip.fechavto = _fechaVto;
                        _afip.n = _n;
                        _afip.numero = Convert.ToInt32(_numero);
                        _afip.serie = _serie;
                        _afip.tipocae = "CAEA";
                        _afip.nrofiscal = _dtoContador.contador + 1;
                        if (!CommonService.TransaccionAFIP.InsertTransaccionAFIP(_afip, _connection, _trn))
                            LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "InsertTransaccionAFIP con CAEA, no se ejecuto correctamente. No grabo.");
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, 1, _connection, _trn);
                        //Modifico el contador.
                        DataAccess.FuncionesVarias.UpdateContador(_ptovtacaea, _tipoComprobante.Substring(0, 3), _connection, _trn);
                        //Comiteo
                        _trn.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //Se produjo un error, Rollback.
                        _trn.Rollback();
                        //Log
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: " + ex.Message);
                        return false;
                    }
                }
                catch(Exception e)
                {
                    //Log
                    LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionCAEA2 Error: " + e.Message);
                    return false;
                }
            }

            public class Retail
            {
                public static void TransaccionBlack(DatosFactura _cab, SqlConnection _sql)
                {
                    string _serieFiscal1 = "0000";
                    string _serieFiscal2 = "";
                    int _numeroFiscal = -1;

                    SqlTransaction _tran;
                    //Comienzo la transaccion
                    _tran = _sql.BeginTransaction();
                    try
                    {
                        FacturasVentaSeriesResol _fvsr = new FacturasVentaSeriesResol
                        {
                            N = _cab.N,
                            NumSerie = _cab.NumSerie,
                            NumFactura = Convert.ToInt32(_cab.NumFactura),
                            NumeroFiscal = _numeroFiscal,
                            SerieFiscal1 = _serieFiscal1,
                            SerieFiscal2 = _serieFiscal2
                        };
                        //Grabo FacturasVentaSerieResol.
                        FacturasVentaSeriesResol.GrabarTiquetNew(_fvsr, _sql, _tran);
                        //Inserto en Rem_transacciones.
                        //Grabo RemTransacciones. Enviamos los cambios a central.
                        RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.NumSerie, Convert.ToInt32(_cab.NumFactura), _cab.N, 1, _sql, _tran);
                        //Comiteo los cambios
                        _tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        _tran.Rollback();
                        throw new Exception("TransaccionBlack. Error: " + ex.Message);
                    }
                }

                public static void TransaccionBlack2(string _n, string _numSerie, int _numero, SqlConnection _sql)
                {
                    string _serieFiscal1 = "0000";
                    string _serieFiscal2 = "";
                    int _numeroFiscal = -1;

                    SqlTransaction _tran;
                    //Comienzo la transaccion
                    _tran = _sql.BeginTransaction();
                    try
                    {
                        FacturasVentaSeriesResol _fvsr = new FacturasVentaSeriesResol
                        {
                            N = _n,
                            NumSerie = _numSerie,
                            NumFactura = _numero,
                            NumeroFiscal = _numeroFiscal,
                            SerieFiscal1 = _serieFiscal1,
                            SerieFiscal2 = _serieFiscal2
                        };
                        //Grabo FacturasVentaSerieResol.
                        FacturasVentaSeriesResol.GrabarTiquetNew(_fvsr, _sql, _tran);
                        //Inserto en Rem_transacciones.
                        //Grabo RemTransacciones. Enviamos los cambios a central.
                        RemTransacciones.InsertRemTransacciones(Environment.MachineName, _numSerie, _numero, _n, 1, _sql, _tran);
                        //Comiteo los cambios
                        _tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        _tran.Rollback();
                        throw new Exception("TransaccionBlack. Error: " + ex.Message);
                    }
                }

                public static bool TransactionManual(string _serie, string _numero, string _n, string _codvendedor,
               string _ptovta, int _nroCbte, string _cuit, string _pathlog, SqlConnection _connection)
                {
                    //obtengo el tipo de comprobante.
                    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codvendedor, _connection);
                    //armo la fecha de vto como la de hoy.
                    string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                    //Recupero el contador.
                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_ptovta, _tipoComprobante.Substring(0, 3), _connection);
                    //
                    string clPtoVta = _ptovta.PadLeft(5, '0') + "-" + (_dtoContador.contador + 1).ToString().PadLeft(8, '0');
                    SqlTransaction _trn;
                    _trn = _connection.BeginTransaction();
                    try
                    {
                        //Modificamos el registro en FacturasCamposLibres.
                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serie, _numero, _n, "MANUAL", "", _fechaVto, "", "", clPtoVta, _connection, _trn);
                        //insertamos el comprobante de FacturasVentasSeriesResol
                        DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_serie, _numero, _n, _ptovta, _tipoComprobante, _nroCbte, _connection, _trn);
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, _connection, _trn);
                        //Modifico el contador.
                        DataAccess.FuncionesVarias.UpdateContador(_ptovta, _tipoComprobante.Substring(0, 3), _nroCbte, _connection, _trn);
                        //Comiteo
                        _trn.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //Se produjo un error, Rollback.
                        _trn.Rollback();
                        //Log
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "TransactionManual Error: " + ex.Message);
                        return false;
                        //throw new Exception(ex.Message);
                    }
                }
            }
        }

        public class ComprobanteAsociado
        {
            public string numSerie { get; set; }
            public int numAlbaran { get; set; }
            public string n { get; set; }

            public static List<ComprobanteAsociado> GetComprobantesAsociados(string _numserie, int _numfac, string _n,  SqlConnection _conexion)
            {
                string _sql = @"select distinct ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, ALBVENTALIN.ABONODE_N
                    from ALBVENTACAB inner join albventalin on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN 
                    and ALBVENTACAB.N = ALBVENTALIN.N WHERE ALBVENTACAB.NUMSERIEFAC = @NumSerie AND ALBVENTACAB.NUMFAC = @NumFac AND ALBVENTACAB.NFAC = @N";

                List<ComprobanteAsociado> _lst = new List<ComprobanteAsociado>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFac", _numfac);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if (!String.IsNullOrEmpty(reader["ABONODE_NUMSERIE"].ToString()))
                                    {
                                        ComprobanteAsociado _cls = new ComprobanteAsociado();
                                        _cls.n = reader["ABONODE_N"].ToString();
                                        _cls.numAlbaran = Convert.ToInt32(reader["ABONODE_NUMALBARAN"]);
                                        _cls.numSerie = reader["ABONODE_NUMSERIE"].ToString();

                                        _lst.Add(_cls);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _lst;
            }

            public static void DeleteComprobantesAsociados(string _numserie, int _numfac, string _n, SqlConnection _conexion)
            {
                string _sql = @"SET ALBVENTALIN.ABONODE_NUMSERIE = '', ALBVENTALIN.ABONODE_NUMALBARAN = -1, ALBVENTALIN.ABONODE_N = ''
                    from  ALBVENTACAB inner join albventalin on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN 
                    and ALBVENTACAB.N = ALBVENTALIN.N 
                    WHERE ALBVENTACAB.NUMSERIEFAC = @NumSerie AND ALBVENTACAB.NUMFAC = @NumFac AND ALBVENTACAB.NFAC = @NFac";

                List<ComprobanteAsociado> _lst = new List<ComprobanteAsociado>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFac", _numfac);
                    _cmd.Parameters.AddWithValue("@NFac", _n);

                    try
                    {
                        _cmd.ExecuteNonQuery();                        
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return;
            }

            public static List<Modelos.AfipComprobanteAsoc> GetAfipComprobanteAsoc(string _numserie, int _numfac, string _n, string _cuit, SqlConnection _conexion)
            {
                string _sql = @"select distinct ALBVENTACAB.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    from ALBVENTACAB inner join albventalin on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N
                    inner join FACTURASVENTASERIESRESOL on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE 
                    AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N 
                    inner join TIPOSDOC on ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIEFAC = @numserie AND ALBVENTACAB.NUMFAC = @numfac AND ALBVENTACAB.NFAC = @n";

                List<Modelos.AfipComprobanteAsoc> _lst = new List<Modelos.AfipComprobanteAsoc>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["ABONODE_N"]);
                                    Modelos.AfipComprobanteAsoc _cls = new Modelos.AfipComprobanteAsoc();
                                    _cls.CbteFch = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                    _cls.Cuit = _cuit;
                                    _cls.Nro = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    _cls.PtoVta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.Tipo = Convert.ToInt32(reader["DESCRIPCION"].ToString().Substring(0, 3));

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _lst;
            }
            public static AfipDll.wsAfip.ComprobanteAsoc GetAfipComprobanteAsocNew(string _numserie, int _numfac, string _n, string _cuit, SqlConnection _conexion)
            {
                string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

                AfipDll.wsAfip.ComprobanteAsoc _cls = new AfipDll.wsAfip.ComprobanteAsoc();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["FECHA"]);

                                    _cls.CbteFch = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                    _cls.Cuit = _cuit;
                                    _cls.Nro = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    _cls.PtoVta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.Tipo = Convert.ToInt32(reader["DESCRIPCION"].ToString().Substring(0, 3));

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _cls;
            }

            public static AfipDll.wsAfipCae.CbteAsoc GetAfipComprobanteAsocNewCAEA(string _numserie, int _numfac, string _n, string _cuit, SqlConnection _conexion)
            {
                string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

                AfipDll.wsAfipCae.CbteAsoc _cls = new AfipDll.wsAfipCae.CbteAsoc();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["FECHA"]);

                                    _cls.CbteFch = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                    _cls.Cuit = _cuit;
                                    _cls.Nro = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    _cls.PtoVta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.Tipo = Convert.ToInt32(reader["DESCRIPCION"].ToString().Substring(0, 3));

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _cls;
            }

            public static AfipDll.wsMtxca.ComprobanteAsociadoType GetAfipComprobanteAsocMTXCA(string _numserie, int _numfac, string _n, string _cuit, int _tipoComprobante, SqlConnection _conexion)
            {
                string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

                AfipDll.wsMtxca.ComprobanteAsociadoType _cls = new AfipDll.wsMtxca.ComprobanteAsociadoType();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["FECHA"]);

                                    _cls.fechaEmision = _fech;
                                    _cls.fechaEmisionSpecified = true;
                                    if (_tipoComprobante == 203 || _tipoComprobante == 208 || _tipoComprobante == 213
                                            || _tipoComprobante == 202 || _tipoComprobante == 207 || _tipoComprobante == 212)
                                    {
                                        _cls.cuit = Convert.ToInt64(_cuit);
                                        _cls.cuitSpecified = true;
                                    }

                                    _cls.numeroComprobante = Convert.ToInt64(reader["NUMEROFISCAL"]);
                                    _cls.numeroPuntoVenta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.codigoTipoComprobante = Convert.ToInt16(reader["DESCRIPCION"].ToString().Substring(0, 3));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _cls;
            }
        }

        public class MTXCA
        {
            public class DatosItemsMtxca
            {
                public string numserie { get; set; }
                public int numalbaran { get; set; }
                public string n { get; set; }
                public int numlin { get; set; }
                public string codigo { get; set; }
                public string descripcion { get; set; }
                public decimal cantidad { get; set; }
                public int codidoUnidadMedida { get; set; }
                public int codigoCondicionIva { get; set; }
                public decimal importeIva { get; set; }
                public decimal unidadesMTx { get; set; }
                public decimal precio { get; set; }
                public decimal total { get; set; }
                public string codigoMtx { get; set; }
                public decimal dto { get; set; }

                public static List<AfipDll.wsMtxca.ItemType> GetDatosItemsMtxca(string pnumserie, int pnumalbaran, string pn, int _codAfip, 
                    DataAccess.DatosFacturaExpo _cabecera, string pconnection, string _pathLog)
                {
                    //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los Items del comprobante");
                    string _sql = @"SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN, ALBVENTALIN.REFERENCIA, 
                        ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL AS UNID1, ALBVENTALIN.PRECIO, ALBVENTALIN.TOTAL, ARTICULOS.CODIGOADUANA, ALBVENTALIN.DTO,
                        ArticulosLin.codbarras3, ArticulosLin.CODBARRAS, ALBVENTALIN.PRECIOIVA, ARTICULOSCAMPOSLIBRES.MTX, ALBVENTALIN.IVA/100  as IVA 
                        FROM FACTURASVENTA INNER JOIN ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC and FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC and FACTURASVENTA.N = ALBVENTACAB.NFAC
                        INNER JOIN ALBVENTALIN on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N
                        INNER JOIN ARTICULOSLIN on albventalin.codarticulo = articulosLIN.codarticulo and albventalin.talla = articulosLIN.talla and albventalin.color = articulosLIN.color
                        inner join articulos on articuloslin.codarticulo = ARTICULOS.codarticulo
                        INNER JOIN ARTICULOSCAMPOSLIBRES ON ARTICULOS.CODARTICULO = ARTICULOSCAMPOSLIBRES.CODARTICULO
                        WHERE FACTURASVENTA.NUMSERIE = @NumSerie and FACTURASVENTA.NUMFACTURA = @NumAlbaran and FACTURASVENTA.N = @N";

                    List<AfipDll.wsMtxca.ItemType> _dfe = new List<AfipDll.wsMtxca.ItemType>();
                    try
                    {
                        using (SqlConnection _conn = new SqlConnection(pconnection))
                        {
                            _conn.Open();

                            using (SqlCommand _cmd = new SqlCommand(_sql))
                            {
                                _cmd.CommandType = System.Data.CommandType.Text;
                                _cmd.Connection = _conn;

                                _cmd.Parameters.AddWithValue("@NumSerie", pnumserie);
                                _cmd.Parameters.AddWithValue("@NumAlbaran", pnumalbaran);
                                _cmd.Parameters.AddWithValue("@N", pn);

                                using (SqlDataReader reader = _cmd.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            AfipDll.wsMtxca.ItemType _it = new AfipDll.wsMtxca.ItemType();
                                            decimal _Iva = Convert.ToDecimal(reader["IVA"]);
                                            _it.cantidad = Math.Abs(Math.Round(Convert.ToDecimal(reader["UNID1"], System.Globalization.CultureInfo.InvariantCulture),2));
                                            _it.unidadesMtx = Math.Abs(Convert.ToInt32(reader[6], System.Globalization.CultureInfo.InvariantCulture)); 
                                            _it.codigo = reader["CODBARRAS"] == null ? "" : reader["CODBARRAS"].ToString();
                                            _it.codigoMtx = reader["codbarras3"] == null ? "" : reader["codbarras3"].ToString();
                                            _it.codigoUnidadMedida = reader["MTX"] == null ? Convert.ToInt16(7) : Convert.ToInt16(reader["MTX"].ToString().Split('-')[0]);
                                            _it.descripcion = reader[5] == null ? "" : reader["DESCRIPCION"].ToString();

                                            switch (_Iva.ToString())
                                            {
                                                case "0":                                                
                                                    {
                                                        _it.codigoCondicionIVA = 3;
                                                        break;
                                                    }
                                                case "0,105":
                                                case "0.105":
                                                    {
                                                        _it.codigoCondicionIVA = 4;
                                                        break;
                                                    }
                                                case "0,21":
                                                case "0.21":
                                                    {
                                                        _it.codigoCondicionIVA = 5;
                                                        break;
                                                    }
                                                case "0,27":
                                                case "0.27":
                                                    {
                                                        _it.codigoCondicionIVA = 6;
                                                        break;
                                                    }
                                                case "0.05":
                                                case "0,05":
                                                    {
                                                        _it.codigoCondicionIVA = 8;
                                                        break;
                                                    }
                                                case "0,025":
                                                case "0.025":
                                                    {
                                                        _it.codigoCondicionIVA = 9;
                                                        break;
                                                    }
                                            }
                                            if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                                      || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                            {
                                                //_it.precioUnitario = Math.Round(Convert.ToDecimal(reader["PRECIOIVA"]),2);
                                                _it.precioUnitario = Math.Round(Convert.ToDecimal(reader["PRECIO"]) * (1 + _Iva), 2);
                                                _it.importeBonificacion = Convert.ToDecimal(reader["DTO"]) == 0 ? 0 :
                                                    Math.Round((Math.Abs(_it.precioUnitario * _it.cantidad) * Convert.ToDecimal(reader["DTO"]) / 100),2);
                                                _it.importeIVA = Math.Round(((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva),2);
                                                _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion), 2);
                                            }
                                            else
                                            {
                                                _it.precioUnitario = Convert.ToDecimal(reader["PRECIO"]);
                                                _it.importeBonificacion = Convert.ToDecimal(reader["DTO"]) == 0 ? 0 :
                                                    Math.Round((Math.Abs(_it.precioUnitario * _it.cantidad) * Convert.ToDecimal(reader["DTO"]) / 100),2);
                                                _it.importeIVA = Math.Round(((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva),2);
                                                _it.importeItem = Math.Round((((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva)),2);
                                            }
                                            //
                                            //Vemos el dto comercial
                                            if (_cabecera.porcentajeDtoComercial > 0)
                                            {
                                                decimal _precioComercial = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion),2);
                                                decimal _bonifComercial = Math.Round((_precioComercial * _cabecera.porcentajeDtoComercial / 100),2);
                                                decimal _totalBonifComercial = Math.Round(_it.importeBonificacion + _bonifComercial,2);
                                                _it.importeBonificacion = Math.Round(_totalBonifComercial, 2);
                                                _it.importeIVA = Math.Round((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva,2);
                                                if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                                      || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                                    _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion),2);
                                                else
                                                    _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva),2);
                                            }
                                            //Vemos el dto PP
                                            if (_cabecera.porcentajeDtoPp > 0)
                                            {
                                                decimal _precioPp = (_it.precioUnitario * _it.cantidad) - _it.importeBonificacion;
                                                decimal _bonifPp = _precioPp * _cabecera.porcentajeDtoPp / 100;
                                                //decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                                                decimal _totalBonifComercial = _it.importeBonificacion + _bonifPp;
                                                _it.importeBonificacion = Math.Round(_totalBonifComercial, 2);
                                                _it.importeIVA = Math.Round((_it.precioUnitario * _it.cantidad - _it.importeBonificacion) * _Iva,2);

                                                if (_codAfip == 6 || _codAfip == 7 || _codAfip == 8
                                                                      || _codAfip == 206 || _codAfip == 207 || _codAfip == 208)
                                                    _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion),2);
                                                else
                                                    _it.importeItem = Math.Round(((_it.precioUnitario * _it.cantidad) - _it.importeBonificacion) * (1 + _Iva),2);
                                            }
                                            //

                                            _dfe.Add(_it);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error al recuperar los datos de los items de la factura. Error: " + ex.Message);
                    }
                    return _dfe;
                }
            }
        }
    }
}
