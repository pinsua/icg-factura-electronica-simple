﻿using AfipDll.wsMtxca;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using static IcgFceDll.SiTefService.Common;

namespace IcgFceDll
{
    public class CommonService
    {
        public class Email
        {
            /// <summary>
            /// Metodo para enviar un mail
            /// </summary>
            /// <param name="pAdjunto">Archivo adjunto con Path completo.</param>
            /// <param name="pDestinatario">Desinatario de mail, pueden ir varios.</param>
            /// <param name="pEnviador">Direccion de envio del mail.</param>
            /// <param name="pNicEnvaidor">Nick o texto a mostrar en el enviador.</param>
            /// <param name="pAsunto">Asunto del mail.</param>
            /// <param name="pBody">Texto del Mail.</param>
            /// <param name="pCredentials">Booleano que indica si se utilizan las credenciales.</param>
            /// <param name="pPuerto">Puerto de envio del mail.</param>
            /// <param name="pServidor">Servidor SMTP de salida.</param>
            /// <param name="pPassword">Password de la cuenta de envio.</param>
            /// <param name="pEnableSSL">Indica si el servidor utiliza SSL con Encriptacion.</param>
            /// <returns></returns>
            public static bool Enviar(string pAdjunto, string pDestinatario, string pDestinCopia, string pEnviador, string pNicEnvaidor,
                string pAsunto, string pBody, bool pCredentials, string pPassword, int pPuerto, string pServidor,
                bool pEnableSSL)
            {
                //Ruta de archivo adjunto (Si lo lleva)
                string PathFile = pAdjunto;

                //Creo el mensaje del mail.
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                //Agrego los destinatarios, pueden ir varios
                msg.To.Add(pDestinatario);

                if (pDestinCopia.Trim().Length > 0)
                {
                    //Agrego los destinatarios de Copia, pueden ir varios.
                    msg.CC.Add(pDestinCopia);
                }
                //Agrego la direccion del que manda el email, puede ser falsa o verdadera, 
                //pero si es falsa puede que el servidor de correo lo detecte como spam, 
                //tambien depende de las credenciales que se ponen mas abajo
                msg.From = new MailAddress(pEnviador, pNicEnvaidor, System.Text.Encoding.UTF8);

                //Pongo el asunto
                msg.Subject = pAsunto;

                //El tipo de codificacion del Asunto 
                msg.SubjectEncoding = System.Text.Encoding.UTF8;

                //Escribo el mensaje Y su codificacion
                msg.Body = pBody;
                msg.BodyEncoding = System.Text.Encoding.UTF8;

                //Especifico si va ha ser interpertado con HTML
                msg.IsBodyHtml = true;

                //Agrego el archivo que puse en la ruta anterior "PathFile", y su tipo.
                if (PathFile.Trim().Length > 0)
                {
                    string[] _adjuntos = PathFile.Split(';');
                    foreach (string _at in _adjuntos)
                    {
                        Attachment attachment = new Attachment(_at, MediaTypeNames.Application.Octet);
                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(_at);
                        disposition.ModificationDate = File.GetLastWriteTime(_at);
                        disposition.ReadDate = File.GetLastAccessTime(_at);
                        disposition.FileName = Path.GetFileName(_at);
                        disposition.Size = new FileInfo(_at).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        msg.Attachments.Add(attachment);
                    }
                }
                //Creo un objeto de tipo cliente de correo (Por donde se enviara el correo)
                SmtpClient client = new SmtpClient();

                //Si no voy a usar credenciales pongo false, Pero la mayoria de servidores exigen las credenciales para evitar el spam
                client.UseDefaultCredentials = pCredentials;
                if (client.UseDefaultCredentials)
                {
                    //Como voy a utilizar credenciales las pongo
                    client.Credentials = new System.Net.NetworkCredential(pEnviador, pPassword);
                }
                //Si fuera gmail seria 587 el puerto, si es un servidor outlook casi siempre el puerto 25, yo utilizo un servidor propio de correo
                client.Port = pPuerto;

                //identifico el cliente que voy a utilizar
                client.Host = pServidor;

                //Si fuera a utilizar gmail esto deberia ir en true, esto es un certificado de seguridad
                client.EnableSsl = pEnableSSL;


                try
                {
                    //Envio el mensaje
                    client.Send(msg);
                    return true;
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    throw new Exception("Se produjo el siguiente Error al intentar enviar el mail. Error: " + ex.Message);
                }
            }
        }

        public class TransaccionAFIP
        {
            /// <summary>
            /// Metodo para la creación de la tabla de transacciones con AFIP.
            /// </summary>
            /// <param name="_connection">Conexión SQL</param>
            public static string CrearTablaTransaccionAFIP(SqlConnection _connection)
            {
                string _rta = "";
                string _sql = @"IF(NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'
                    AND TABLE_NAME = 'TransaccionAfip'))
                    BEGIN
                        CREATE TABLE [dbo].[TransaccionAfip](
	                        [Id] [int] IDENTITY(1,1) NOT NULL,
	                        [Serie] [nvarchar](10) NOT NULL,
	                        [Numero] [int] NOT NULL,
	                        [N] [nvarchar](5) NOT NULL,
	                        [CAE] [nvarchar](50) NULL,
	                        [FechaVto] [nvarchar](10) NULL,
	                        [CodigoQr] [nvarchar](350) NULL,
	                        [CodigoBarra] [nvarchar](250) NULL,
	                        [TipoCAE] [nvarchar](10) NULL,
                            [NroFiscal] [int] NULL,
                         CONSTRAINT [PK_TransaccionAfip] PRIMARY KEY CLUSTERED 
                        (
	                        [Id] ASC
                        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                        ) ON [PRIMARY]
                    END";
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = _connection;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = _sql;

                        cmd.ExecuteNonQuery();
                    }
                    _rta = "La tabla TransaccionAfip, se creo correctamente.";
                }
                catch (Exception ex)
                {                    
                    _rta ="Se produjo el siguiente error al crear la tabla TransaccionAfip: " + ex.Message;
                }
                return _rta;
            }
            /// <summary>
            /// Metodo para la inserción de un registro
            /// </summary>
            /// <param name="_cae">Clase con la información del registro</param>
            /// <param name="_connection">Conexión SQL</param>
            /// <returns>True/false</returns>
            public static bool InsertTransaccionAFIP(InfoTransaccionAFIP _cae, SqlConnection _connection)
            {
                bool _rta = false;

                string _sql = @"INSERT INTO [dbo].[TransaccionAfip]([Serie],[Numero],[N],[CAE],[FechaVto],[CodigoQr],[CodigoBarra],[TipoCAE], [NroFiscal])
                VALUES(@Serie,@Numero,@N,@CAE,@FechaVto,@CodigoQr,@CodigoBarra,@TipoCAE, @NroFiscal)";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _connection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@Serie", _cae.serie);
                    _cmd.Parameters.AddWithValue("@Numero", _cae.numero);
                    _cmd.Parameters.AddWithValue("@N", _cae.n);
                    _cmd.Parameters.AddWithValue("@CAE", _cae.cae);
                    _cmd.Parameters.AddWithValue("@FechaVto", _cae.fechavto);
                    _cmd.Parameters.AddWithValue("@CodigoQr", _cae.codigoqr);
                    _cmd.Parameters.AddWithValue("@CodigoBarra", _cae.codigobara);
                    _cmd.Parameters.AddWithValue("@TipoCAE", _cae.tipocae);
                    _cmd.Parameters.AddWithValue("@NroFiscal", _cae.nrofiscal);

                    try
                    {
                        int _insert = _cmd.ExecuteNonQuery();

                        if (_insert > 0)
                            _rta = true;
                        else
                            _rta = false;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _rta;
            }
            /// <summary>
            /// Metodo para la inserción de un registro
            /// </summary>
            /// <param name="_cae">Clase con la información del registro</param>
            /// <param name="_connection">Conexión SQL</param>
            /// <param name="_transac">Transacción SQL</param>
            /// <returns>True/false</returns>
            public static bool InsertTransaccionAFIP(InfoTransaccionAFIP _cae, SqlConnection _connection, SqlTransaction _transac)
            {
                bool _rta = false;

                string _sql = @"INSERT INTO [dbo].[TransaccionAfip]([Serie],[Numero],[N],[CAE],[FechaVto],[CodigoQr],[CodigoBarra],[TipoCAE],[NroFiscal])
                VALUES(@Serie,@Numero,@N,@CAE,@FechaVto,@CodigoQr,@CodigoBarra,@TipoCAE, @NroFiscal)";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _connection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@Serie", _cae.serie);
                    _cmd.Parameters.AddWithValue("@Numero", _cae.numero);
                    _cmd.Parameters.AddWithValue("@N", _cae.n);
                    _cmd.Parameters.AddWithValue("@CAE", _cae.cae);
                    _cmd.Parameters.AddWithValue("@FechaVto", _cae.fechavto);
                    _cmd.Parameters.AddWithValue("@CodigoQr", _cae.codigoqr);
                    _cmd.Parameters.AddWithValue("@CodigoBarra", _cae.codigobara);
                    _cmd.Parameters.AddWithValue("@TipoCAE", _cae.tipocae);
                    _cmd.Parameters.AddWithValue("@NroFiscal", _cae.nrofiscal);
                    //ExecuteNonQuery.
                    int _insert = _cmd.ExecuteNonQuery();

                    if (_insert > 0)
                        _rta = true;
                    else
                        _rta = false;

                }

                return _rta;
            }

            public static InfoTransaccionAFIP GetTransaccionAFIP(string serie, int numero, SqlConnection _connection)
            {
                string _sql = @"select Serie, Numero, N, CAE, FechaVto, CodigoQr, CodigoBarra, TipoCAE, NroFiscal from TransaccionAfip where Serie = @Serie and Numero = @Numero  and N = 'B'";

                InfoTransaccionAFIP rta = new InfoTransaccionAFIP();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _connection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //_cmd.Transaction = _transac;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@Serie", serie);
                    _cmd.Parameters.AddWithValue("@Numero", numero);
                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    rta.cae = reader["CAE"].ToString();
                                    //rta.ptoVta = ptovta;
                                    rta.n = "B";
                                    rta.codigobara = reader["CodigoBarra"].ToString();
                                    rta.codigoqr = reader["CodigoQr"].ToString();
                                    rta.fechavto = reader["FechaVto"].ToString();
                                    rta.nrofiscal = Convert.ToInt32(reader["NroFiscal"]);
                                    rta.tipocae = reader["TipoCAE"].ToString();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }

                }

                return rta;
            }

        }

        public class InfoTransaccionAFIP
        {
            public string serie { get; set; }
            public int numero { get; set; }
            public string n { get; set; }
            public string cae { get; set; }
            public string fechavto { get; set; }
            public string codigoqr { get; set; }
            public string codigobara { get; set; }
            public string tipocae { get; set; }
            public int nrofiscal { get; set; }
            public string ptoVta { get; set; }

        }

        public class FcCAEA
        {
            public static string CreateTableFcCAEA(SqlConnection _connection)
            {
                string _rta = "";
                string _sql = @"IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo'
                    AND TABLE_NAME = 'FCCAEA'))
                    BEGIN
	                    CREATE TABLE [dbo].[FCCAEA](
		                    [ID] [int] IDENTITY(1,1) NOT NULL,
		                    [CAEA] [nvarchar](50) NOT NULL,
		                    [FechaProceso] [datetime] NULL,
		                    [FechaDesde] [datetime] NULL,
		                    [FechaHasta] [datetime] NULL,
		                    [FechaTope] [datetime] NULL,
		                    [Periodo] [int] NULL,
		                    [Quincena] [int] NULL,
	                     CONSTRAINT [PK_FCCAEA] PRIMARY KEY CLUSTERED 
	                    (
		                    [ID] ASC
	                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	                    ) ON [PRIMARY]
                    END";
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = _connection;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = _sql;

                        cmd.ExecuteNonQuery();
                    }
                    _rta = "La tabla FCCAEA se creo correctamente.";
                }
                catch (Exception ex)
                {
                    _rta = "Se produjo el siguiente error al crear la tabla FCCAEA: " + ex.Message;
                }

                return _rta;
            }
        }

        public class Validaciones
        {
            public static bool ValidarMontoMaximoConsumidorFinal(decimal MontoMaximo, DataAccess.DatosFactura FC)
            {
                bool _rta = false;
                if (FC.cCodAfip == "006" || FC.cCodAfip == "007" || FC.cCodAfip == "008")
                {
                    if (Math.Round(Math.Abs(Convert.ToDecimal(FC.TotalNeto)), 2) > Math.Round(MontoMaximo, 2))
                    {
                        //No puede ser 99, porque no esta identificado.
                        if (FC.ClienteTipoDoc == "99")
                        {
                            _rta = false;
                        }
                        else
                        {
                            if (FC.ClienteTipoDoc == "96")
                            {
                                if (String.IsNullOrEmpty(FC.ClienteNroDoc))
                                    _rta = false;
                                else
                                {
                                    if (Convert.ToInt64(FC.ClienteNroDoc) <= 0)
                                        _rta = false;
                                    else
                                        _rta = true;
                                }
                            }
                            if(FC.ClienteTipoDoc == "80")
                            {
                                if (String.IsNullOrEmpty(FC.ClienteNroDoc))
                                    _rta = false;
                                else
                                {
                                    if (Convert.ToInt64(FC.ClienteNroDoc) <= 0)
                                        _rta = false;
                                    else
                                        _rta = true;
                                }
                            }
                        }
                    }
                    else
                        _rta = true;
                }
                else
                    _rta = true;
                return _rta;
            }
        }
    }
}
