﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HerramientasAFIP
{
    public partial class fmObtenerUltimoNro : Form
    {
        private string _cuit;
        private string _certificado;
        private string _password;
        private string _pathCertificado;
        private bool _esTest;
        private string _pathLog;
        private string _pathTaFC;
        public fmObtenerUltimoNro(string cuit, string certificado, string password,
            string pathcertificado, string pathlog, string pathtafc, bool esTest)
        {
            InitializeComponent();
            _cuit = cuit;
            _certificado = certificado;
            _password = password;
            _pathCertificado = pathcertificado;
            _esTest = esTest;
            _pathLog = pathlog;
            _pathTaFC = pathtafc;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(_cuit))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(_certificado))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(_password))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
            {
                MessageBox.Show("Debe ingresar el PtoVta.");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCertificado.Contains(_certificado))
                    _pathCertificado = _pathCertificado+ @"\" + _certificado;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
                int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));

                AfipDll.wsAfip.UltimoComprobante call = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                    _tipoConpro, _cuit, _esTest, strPasswordSecureString, out _lstErrores);

                if (call.CbteNro > 0)
                {
                    MessageBox.Show("Nro. Comprobante: " + call.CbteNro + Environment.NewLine +
                        "Comprobante Tipo: " + call.CbteTipo + Environment.NewLine +
                        "Punto de Venta: " + call.PtoVta);
                }
                else
                {
                    MessageBox.Show("Nro. Comprobante: " + call.CbteNro);

                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
