﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HerramientasAFIP
{
    public partial class fmObtenerDatosComprobante : Form
    {
        private string _cuit;
        private string _certificado;
        private string _password;
        private string _pathCertificado;
        private bool _esTest;
        private string _pathLog;
        private string _pathTaFC;
        public fmObtenerDatosComprobante(string cuit, string certificado, string password,
            string pathcertificado, string pathlog, string pathtafc, bool esTest)
        {
            InitializeComponent();
            _cuit = cuit;
            _certificado = certificado;
            _password = password;
            _pathCertificado = pathcertificado;
            _esTest = esTest;
            _pathLog = pathlog;
            _pathTaFC = pathtafc;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(_cuit))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(_certificado))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(_password))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
            {
                MessageBox.Show("Debe ingresar el PtoVta.");
                return;
            }

            if (String.IsNullOrEmpty(txtNroComprobante.Text))
            {
                MessageBox.Show("Debe ingresar el Nro del comprobante.");
                return;
            }



            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCertificado.Contains(_certificado))
                    _pathCertificado = _pathCertificado + @"\" + _certificado;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
                
                int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));
                int _Nro = Convert.ToInt32(txtNroComprobante.Text);

                string call = AfipDll.wsAfip.ConsultaComprobante2New(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                    _tipoConpro, _cuit, _Nro, _esTest, strPasswordSecureString);

                MessageBox.Show(call);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
