﻿
namespace HerramientasAFIP
{
    partial class fmExcelCAEA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbExcel = new System.Windows.Forms.GroupBox();
            this.lblExcel = new System.Windows.Forms.Label();
            this.txtPathExcel = new System.Windows.Forms.TextBox();
            this.btSeleccionarExcel = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.gbAcciones = new System.Windows.Forms.GroupBox();
            this.gbDatos = new System.Windows.Forms.GroupBox();
            this.dgDatos = new System.Windows.Forms.DataGridView();
            this.btLoadExcel = new System.Windows.Forms.Button();
            this.btInformar = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.gbExcel.SuspendLayout();
            this.gbAcciones.SuspendLayout();
            this.gbDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // gbExcel
            // 
            this.gbExcel.Controls.Add(this.btSeleccionarExcel);
            this.gbExcel.Controls.Add(this.txtPathExcel);
            this.gbExcel.Controls.Add(this.lblExcel);
            this.gbExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbExcel.Location = new System.Drawing.Point(0, 0);
            this.gbExcel.Name = "gbExcel";
            this.gbExcel.Size = new System.Drawing.Size(1221, 112);
            this.gbExcel.TabIndex = 0;
            this.gbExcel.TabStop = false;
            this.gbExcel.Text = "Excel";
            // 
            // lblExcel
            // 
            this.lblExcel.AutoSize = true;
            this.lblExcel.Location = new System.Drawing.Point(74, 41);
            this.lblExcel.Name = "lblExcel";
            this.lblExcel.Size = new System.Drawing.Size(127, 20);
            this.lblExcel.TabIndex = 0;
            this.lblExcel.Text = "Excel a Procesar";
            // 
            // txtPathExcel
            // 
            this.txtPathExcel.Location = new System.Drawing.Point(207, 35);
            this.txtPathExcel.Name = "txtPathExcel";
            this.txtPathExcel.Size = new System.Drawing.Size(545, 26);
            this.txtPathExcel.TabIndex = 1;
            // 
            // btSeleccionarExcel
            // 
            this.btSeleccionarExcel.Location = new System.Drawing.Point(778, 31);
            this.btSeleccionarExcel.Name = "btSeleccionarExcel";
            this.btSeleccionarExcel.Size = new System.Drawing.Size(194, 35);
            this.btSeleccionarExcel.TabIndex = 2;
            this.btSeleccionarExcel.Text = "Seleccionar Archivo";
            this.btSeleccionarExcel.UseVisualStyleBackColor = true;
            this.btSeleccionarExcel.Click += new System.EventHandler(this.btSeleccionarExcel_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // gbAcciones
            // 
            this.gbAcciones.Controls.Add(this.btClose);
            this.gbAcciones.Controls.Add(this.btInformar);
            this.gbAcciones.Controls.Add(this.btLoadExcel);
            this.gbAcciones.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbAcciones.Location = new System.Drawing.Point(0, 526);
            this.gbAcciones.Name = "gbAcciones";
            this.gbAcciones.Size = new System.Drawing.Size(1221, 100);
            this.gbAcciones.TabIndex = 1;
            this.gbAcciones.TabStop = false;
            this.gbAcciones.Text = "Acciones";
            // 
            // gbDatos
            // 
            this.gbDatos.Controls.Add(this.dgDatos);
            this.gbDatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDatos.Location = new System.Drawing.Point(0, 112);
            this.gbDatos.Name = "gbDatos";
            this.gbDatos.Size = new System.Drawing.Size(1221, 414);
            this.gbDatos.TabIndex = 2;
            this.gbDatos.TabStop = false;
            this.gbDatos.Text = "Datos a Procesar";
            // 
            // dgDatos
            // 
            this.dgDatos.AllowUserToAddRows = false;
            this.dgDatos.AllowUserToDeleteRows = false;
            this.dgDatos.AllowUserToOrderColumns = true;
            this.dgDatos.AllowUserToResizeRows = false;
            this.dgDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDatos.Location = new System.Drawing.Point(3, 22);
            this.dgDatos.Name = "dgDatos";
            this.dgDatos.ReadOnly = true;
            this.dgDatos.RowHeadersWidth = 62;
            this.dgDatos.RowTemplate.Height = 28;
            this.dgDatos.Size = new System.Drawing.Size(1215, 389);
            this.dgDatos.TabIndex = 0;
            // 
            // btLoadExcel
            // 
            this.btLoadExcel.Location = new System.Drawing.Point(63, 36);
            this.btLoadExcel.Name = "btLoadExcel";
            this.btLoadExcel.Size = new System.Drawing.Size(194, 35);
            this.btLoadExcel.TabIndex = 3;
            this.btLoadExcel.Text = "Leer Excel";
            this.btLoadExcel.UseVisualStyleBackColor = true;
            this.btLoadExcel.Click += new System.EventHandler(this.btLoadExcel_Click);
            // 
            // btInformar
            // 
            this.btInformar.Location = new System.Drawing.Point(447, 33);
            this.btInformar.Name = "btInformar";
            this.btInformar.Size = new System.Drawing.Size(194, 35);
            this.btInformar.TabIndex = 4;
            this.btInformar.Text = "Infomar CAEA AFIP";
            this.btInformar.UseVisualStyleBackColor = true;
            this.btInformar.Click += new System.EventHandler(this.btInformar_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(789, 36);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(194, 35);
            this.btClose.TabIndex = 5;
            this.btClose.Text = "Salir";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // fmExcelCAEA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 626);
            this.Controls.Add(this.gbDatos);
            this.Controls.Add(this.gbAcciones);
            this.Controls.Add(this.gbExcel);
            this.Name = "fmExcelCAEA";
            this.Text = "Informar CAEA a AFIP";
            this.gbExcel.ResumeLayout(false);
            this.gbExcel.PerformLayout();
            this.gbAcciones.ResumeLayout(false);
            this.gbDatos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDatos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbExcel;
        private System.Windows.Forms.Button btSeleccionarExcel;
        private System.Windows.Forms.TextBox txtPathExcel;
        private System.Windows.Forms.Label lblExcel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox gbAcciones;
        private System.Windows.Forms.GroupBox gbDatos;
        private System.Windows.Forms.Button btLoadExcel;
        private System.Windows.Forms.DataGridView dgDatos;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btInformar;
    }
}