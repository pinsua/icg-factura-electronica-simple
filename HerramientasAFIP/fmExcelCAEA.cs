﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AfipDll;
using OfficeOpenXml;

namespace HerramientasAFIP
{
    public partial class fmExcelCAEA : Form
    {
        private string _pathcertificado;
        private string _pathTafc;
        private string _pathlog;
        private string _cuit;
        private bool _istest;
        private string _password;

        public fmExcelCAEA(string pathCertificado, string pathtafc, string pathlog, string cuit, string password, bool istest)
        {
            InitializeComponent();
            _pathcertificado = pathCertificado;
            _pathTafc = pathtafc;
            _pathlog = pathlog;
            _cuit = cuit;
            _istest = istest;
            _password = password;
        }

        private void btSeleccionarExcel_Click(object sender, EventArgs e)
        {
            //configuracion  de algunos parametros del openFileDialog
            // directorio inicial donde se abrira
            openFileDialog1.InitialDirectory = "C:\\";
            // filtro de archivos.
            openFileDialog1.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"; 

            // codigo para abrir el cuadro de dialogo
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string str_RutaArchivo = openFileDialog1.FileName;
                    txtPathExcel.Text = str_RutaArchivo;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void btLoadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                FileInfo excelFile = new FileInfo(txtPathExcel.Text);
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[0];
                    //add column header
                    foreach (var firstRC in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                    {
                        dt.Columns.Add(firstRC.Text);
                    }

                    // add rows
                    for (int rN = 2; rN <= workSheet.Dimension.End.Row; rN++)
                    {
                        ExcelRange row = workSheet.Cells[rN, 1, rN, workSheet.Dimension.End.Column];
                        DataRow newR = dt.NewRow();
                        foreach (var cell in row)
                        {
                            newR[cell.Start.Column - 1] = cell.Text;
                        }

                        dt.Rows.Add(newR);
                    }
                }
                //
                dgDatos.DataSource = dt;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btInformar_Click(object sender, EventArgs e)
        {
            try
            {
                //Armamos la password como segura
                SecureString strPasswordSecureString = new SecureString();
                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();
                //Recorremos el grid.
                foreach (DataGridViewRow dr in dgDatos.Rows)
                {
                    //Armo el string de la cabecera.
                    AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                    _cabReq.CantReg = 1;
                    _cabReq.CbteTipo = Convert.ToInt16(dr.Cells["CbteTipo"].Value);
                    _cabReq.PtoVta = Convert.ToInt16(dr.Cells["PtoVta"].Value);
                    //Cargamos la clase Detalle.
                    AfipDll.wsAfipCae.FECAEADetRequest _detReq = new AfipDll.wsAfipCae.FECAEADetRequest();
                    _detReq.CAEA = dr.Cells["CAEA"].Value.ToString();
                    _detReq.CbteDesde = Convert.ToInt64(dr.Cells["CbteNro"].Value);
                    _detReq.CbteHasta = Convert.ToInt64(dr.Cells["CbteNro"].Value);
                    DateTime _fecha = Convert.ToDateTime(dr.Cells["CbteFecha"].Value);
                    _detReq.CbteFch = _fecha.Year.ToString() + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                    _detReq.Concepto = Convert.ToInt16(dr.Cells["Concepto"].Value);
                    _detReq.DocNro = Convert.ToInt64(dr.Cells["DocNro"].Value);
                    _detReq.DocTipo = Convert.ToInt32(dr.Cells["DocTipo"].Value);
                    _detReq.FchServDesde = _fecha.Year.ToString() + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                    _detReq.FchServHasta = _fecha.Year.ToString() + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                    _detReq.FchVtoPago = _fecha.Year.ToString() + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                    _detReq.ImpIVA = Convert.ToDouble(dr.Cells["ImpIva"].Value);
                    _detReq.ImpNeto = Convert.ToDouble(dr.Cells["ImpNeto"].Value);
                    _detReq.ImpOpEx = Convert.ToDouble(dr.Cells["ImpOpEx"].Value);
                    _detReq.ImpTotal = Convert.ToDouble(dr.Cells["ImpTotal"].Value);
                    _detReq.ImpTotConc = Convert.ToDouble(dr.Cells["ImpTotConc"].Value);
                    _detReq.ImpTrib = Convert.ToDouble(dr.Cells["ImpTrib"].Value);
                    _detReq.MonCotiz = Convert.ToDouble(dr.Cells["MonCotiz"].Value);
                    _detReq.MonId = dr.Cells["MonId"].Value.ToString();
                    //Armo el string de IVA.
                    List<AfipDll.wsAfipCae.AlicIva> _lstIva = new List<AfipDll.wsAfipCae.AlicIva>();
                    AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                    _al.Id = 5;
                    _al.BaseImp = _detReq.ImpNeto;
                    _al.Importe = _detReq.ImpIVA;
                    _lstIva.Add(_al);
                    //Agregamos el IVA
                    _detReq.Iva = _lstIva.ToArray();
                    //Verifico que si es una NC/ND.
                    if (_cabReq.CbteTipo == 203 || _cabReq.CbteTipo == 208 || _cabReq.CbteTipo == 213
                        || _cabReq.CbteTipo == 202 || _cabReq.CbteTipo == 207 || _cabReq.CbteTipo == 212
                        || _cabReq.CbteTipo == 3 || _cabReq.CbteTipo == 8 || _cabReq.CbteTipo == 13
                        || _cabReq.CbteTipo == 2 || _cabReq.CbteTipo == 7 || _cabReq.CbteTipo == 12)
                    {
                        //Genero el periodo asociado
                        DateTime _dtDesde = _fecha.AddDays(-30);
                        string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                        string _hasta = _fecha.Year.ToString().PadLeft(2, '0') + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
                        AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                        _detReq.PeriodoAsoc = _cls;
                    }
                    //Obtengo la hora
                    string _tiempo = dr.Cells["Hora"].Value.ToString();
                    int _hour = Convert.ToInt32(_tiempo.Split(':')[0]);
                    int _minute = Convert.ToInt32(_tiempo.Split(':')[1]);
                    int _second = Convert.ToInt32(_tiempo.Split(':')[2]);
                    _detReq.CbteFchHsGen = _fecha.Year.ToString().PadLeft(4, '0') + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0') +
                    _hour.ToString().PadLeft(2, '0') + _minute.ToString().PadLeft(2, '0') + _second.ToString().PadLeft(2, '0');
                    //Armamos el Request.
                    List<AfipDll.wsAfipCae.FECAEADetRequest> _lstReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();
                    _lstReq.Add(_detReq);
                    AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                    _req.FeCabReq = _cabReq;
                    _req.FeDetReq = _lstReq.ToArray();

                    try
                    {
                        List<wsAfip.Errors> _lstErroresAFIP = new List<wsAfip.Errors>();
                        //Informo a AFIP
                        List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathcertificado, _pathTafc, _pathlog, _cuit, _istest, strPasswordSecureString, _req, true, out _lstErroresAFIP);

                        if (_Respuesta.Count() > 0)
                        {
                            //Recupero el nombre de la terminal.
                            string _terminal = Environment.MachineName;
                            foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                            {
                                switch (cri.Resultado)
                                {
                                    case "A":
                                    case "P":
                                        {
                                            //todo OK
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "El comprobante NUMERO: " + _detReq.CbteDesde.ToString() + 
                                                ", Punto de venta: " + _cabReq.PtoVta +
                                                " se informo correctamente a la AFIP.");
                                            break;
                                        }
                                    case "R":
                                        {
                                            //Error
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "El comprobante NUMERO: " + _detReq.CbteDesde.ToString() +
                                        ", Punto de venta: " + _cabReq.PtoVta + " fue rechazado por la AFIP. Error: " + cri.Observaciones);
                                            break; ;
                                        }
                                }
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "El comprobante NUMERO: " + _detReq.CbteDesde.ToString() +
                                                ", Punto de venta: " + _cabReq.PtoVta + " fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                            foreach (AfipDll.wsAfip.Errors _er in _lstErroresAFIP)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathlog), "El comprobante NUMERO: " + _detReq.CbteDesde.ToString() +
                                                ", Punto de venta: " + _cabReq.PtoVta + " fue rechazado por la AFIP el ser infomado."
                        + Environment.NewLine + "Error: " + ex.Message);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
