﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HerramientasAFIP
{
    public partial class fmMain : Form
    {
        public static string _nombreCertificado;
        public static string strConnectionString;
        public static string _cuit;
        public static bool _EsTest = false;
        public static string _pathApp;
        public static string _pathLog;
        public static string _pathCerificado;
        public static string _pathTaFC;
        public static string _pathTaFCE;
        public static string _pathTaFCMTXCA;
        public static string _appName;
        public static int _codigoEmpresa;
        public static string _razonSocial;

        public static string _certificado;
        public static string _password;
        public static string _pruebaCert;
        public static string _ptovta;
        public fmMain()
        {
            InitializeComponent();
            ValidarCarpetas();
        }

        private void btSave_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            //if (String.IsNullOrEmpty(txtPtoVta.Text))
            //{
            //    MessageBox.Show("Debe ingresar el PtoVta.");
            //    return;
            //}
            try
            {
                _cuit = txtCuit.Text;
                _certificado = txtCertificado.Text;
                _password = txtPassword.Text;
                //_ptovta = txtPtoVta.Text;
                _EsTest = chkIsTest.Checked;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void obtenerPuntosDeVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))
                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

            List<AfipDll.wsAfip.PtoVenta> _ptoVta = AfipDll.wsAfip.ConsultarPuntosVentaNew(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text, _EsTest,
                strPasswordSecureString, false, out _lstErrores);

            if (_ptoVta.Count > 0)
            {
                foreach (AfipDll.wsAfip.PtoVenta _pv in _ptoVta)
                {
                    MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                        "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                        "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                        "Fecha de Baja: " + _pv.FechaBaja);
                }
            }
            else
            {
                MessageBox.Show("No se recivieron puntos de venta");

                foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                {
                    MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                        "Descripcion: " + _er.Msg);
                }
            }
        }

        #region privados
        private void ValidarCarpetas()
        {
            try
            {
                //recupero la ubicacion del ejecutable.
                _pathApp = Application.StartupPath.ToString();
                //Cargo la ubicaciones.
                _pathCerificado = _pathApp + "\\Certificado";
                _pathLog = _pathApp + "\\Log";
                _pathTaFC = _pathApp + "\\TAFC";
                _pathTaFCE = _pathApp + "\\TAFCE";
                _pathTaFCMTXCA = _pathApp + "\\TAFCMTXCA";

                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCerificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCerificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCerificado + "). Por favor coloque el certificado en este directorio para poder continuar.");
                }
                else
                {
                    if (!String.IsNullOrEmpty(_nombreCertificado))
                    {
                        if (!File.Exists(_pathCerificado + "\\" + _nombreCertificado))
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCerificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        else
                            _pathCerificado = _pathCerificado + "\\" + _nombreCertificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);
                //Validamos el FCEMTXCA.
                if (!Directory.Exists(_pathTaFCMTXCA))
                    Directory.CreateDirectory(_pathTaFCMTXCA);
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error: " + ex.Message + " validando los directorios.");
            }
        }
        #endregion

        private void obtenerUltimoNroUtilizadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmObtenerUltimoNro frm = new fmObtenerUltimoNro(_cuit, _certificado, _password, _pathCerificado, _pathLog, _pathTaFC, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void procesarExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _pathCerificado = _pathCerificado + "\\" + _certificado;
            fmExcelCAEA frm = new fmExcelCAEA(_pathCerificado, _pathTaFC, _pathLog, _cuit, _password, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            fmObtenerDatosComprobante frm = new fmObtenerDatosComprobante(_cuit, _certificado, _password, _pathCerificado, _pathLog, _pathTaFC, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void obtenerCAEAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmObtenerCAEA frm = new fmObtenerCAEA(_cuit, _certificado, _password, _pathCerificado, _pathLog, _pathTaFC, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void consultarCAEAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmConsultarCAEA frm = new fmConsultarCAEA(_cuit, _certificado, _password, _pathCerificado, _pathLog, _pathTaFC, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void informarCAEASinMovimientoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmInformarCAEASinMovimiento frm = new fmInformarCAEASinMovimiento(_cuit, _certificado, _password, _pathCerificado, _pathLog, _pathTaFC, _EsTest);
            frm.ShowDialog(this);
            frm.Dispose();
        }
    }
}
