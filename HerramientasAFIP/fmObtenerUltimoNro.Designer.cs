﻿
namespace HerramientasAFIP
{
    partial class fmObtenerUltimoNro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboNroCompro = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboNroCompro
            // 
            this.cboNroCompro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNroCompro.FormattingEnabled = true;
            this.cboNroCompro.Items.AddRange(new object[] {
            "001 - Factura A",
            "002 - Nota Debito A",
            "003 - Nota Credito A",
            "006 - Factura B",
            "007 - Nota Debito B",
            "008 - Nota Credito B",
            "011 - Factura C",
            "012 - Nota Debito C",
            "013 - Nota Credito C",
            "201 - Factura de Credito A",
            "202 - Nota Debito Credito A",
            "203 - Nota Credito Credito A",
            "206 - Factura de Credito B",
            "207 - Nota Debito Credito B",
            "208 - Nota Credito Credito B",
            "113- Tiquet Nota de Credito B"});
            this.cboNroCompro.Location = new System.Drawing.Point(162, 32);
            this.cboNroCompro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboNroCompro.Name = "cboNroCompro";
            this.cboNroCompro.Size = new System.Drawing.Size(337, 28);
            this.cboNroCompro.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 37);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 20);
            this.label7.TabIndex = 37;
            this.label7.Text = "TipoComprobante";
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Location = new System.Drawing.Point(627, 34);
            this.txtPtoVta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.Size = new System.Drawing.Size(148, 26);
            this.txtPtoVta.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(546, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 35;
            this.label2.Text = "PtoVta";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(269, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(210, 38);
            this.button1.TabIndex = 39;
            this.button1.Text = "Obtener Ultimo Número";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fmObtenerUltimoNro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 212);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cboNroCompro);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPtoVta);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fmObtenerUltimoNro";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Obtener Ultimo Numero";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboNroCompro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}