﻿
namespace HerramientasAFIP
{
    partial class fmObtenerDatosComprobante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNroComprobante = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboNroCompro = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNroComprobante
            // 
            this.txtNroComprobante.Location = new System.Drawing.Point(189, 120);
            this.txtNroComprobante.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNroComprobante.Name = "txtNroComprobante";
            this.txtNroComprobante.Size = new System.Drawing.Size(148, 26);
            this.txtNroComprobante.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 123);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 20);
            this.label8.TabIndex = 41;
            this.label8.Text = "Nro. Comprobante";
            // 
            // cboNroCompro
            // 
            this.cboNroCompro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNroCompro.FormattingEnabled = true;
            this.cboNroCompro.Items.AddRange(new object[] {
            "001 - Factura A",
            "002 - Nota Debito A",
            "003 - Nota Credito A",
            "006 - Factura B",
            "007 - Nota Debito B",
            "008 - Nota Credito B",
            "011 - Factura C",
            "012 - Nota Debito C",
            "013 - Nota Credito C",
            "051 - Factura M",
            "053- Nota credito M",
            "201 - Factura de Credito A",
            "202 - Nota Debito Credito A",
            "203 - Nota Credito Credito A",
            "206 - Factura de Credito B",
            "207 - Nota Debito Credito B",
            "208 - Nota Credito Credito B",
            "113- Tiquet Nota de Credito B"});
            this.cboNroCompro.Location = new System.Drawing.Point(189, 39);
            this.cboNroCompro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboNroCompro.Name = "cboNroCompro";
            this.cboNroCompro.Size = new System.Drawing.Size(337, 28);
            this.cboNroCompro.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 20);
            this.label7.TabIndex = 39;
            this.label7.Text = "TipoComprobante";
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Location = new System.Drawing.Point(189, 80);
            this.txtPtoVta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.Size = new System.Drawing.Size(148, 26);
            this.txtPtoVta.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 83);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "PtoVta";
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(199, 184);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(268, 39);
            this.btSave.TabIndex = 43;
            this.btSave.Text = "Obtener Información";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // fmObtenerDatosComprobante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 293);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.txtNroComprobante);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboNroCompro);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPtoVta);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fmObtenerDatosComprobante";
            this.Text = "Obtener Datos Comprobante";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNroComprobante;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboNroCompro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btSave;
    }
}