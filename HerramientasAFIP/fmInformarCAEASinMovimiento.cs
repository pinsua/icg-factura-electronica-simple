﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HerramientasAFIP
{
    public partial class fmInformarCAEASinMovimiento : Form
    {
        private string _cuit;
        private string _certificado;
        private string _password;
        private string _pathCertificado;
        private bool _esTest;
        private string _pathLog;
        private string _pathTaFC;

        public fmInformarCAEASinMovimiento(string cuit, string certificado, string password,
            string pathcertificado, string pathlog, string pathtafc, bool esTest)
        {
            InitializeComponent();

            _cuit = cuit;
            _certificado = certificado;
            _password = password;
            _pathCertificado = pathcertificado;
            _esTest = esTest;
            _pathLog = pathlog;
            _pathTaFC = pathtafc;
        }

        private void btnInformarCAEA_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(_cuit))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(_certificado))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(_password))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
            {
                MessageBox.Show("Debe ingresar el punto de venta.");
                return;
            }

            if (String.IsNullOrEmpty(txtCaea.Text))
            {
                MessageBox.Show("Debe ingresar el CAEA.");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCertificado.Contains(_certificado))
                    _pathCertificado = _pathCertificado + @"\" + _certificado;

                AfipDll.CAEA _caea = new AfipDll.CAEA();

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                int _ptovta = Convert.ToInt32(txtPtoVta.Text);

                string _rta = AfipDll.wsAfip.InformarCAEASinMovimiento(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest, strPasswordSecureString, _ptovta, txtCaea.Text, out _lstErrores);

                if (_rta == "A")
                {
                    MessageBox.Show("CAEA Informado Correctamente.");
                }
                else
                {
                    MessageBox.Show("CAEA RECHAZADO.");
                    string _error = "";
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        _error = "Codigo de error: " + _er.Code + "Descripcion: " + _er.Msg + Environment.NewLine;
                    }

                    MessageBox.Show(_error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
