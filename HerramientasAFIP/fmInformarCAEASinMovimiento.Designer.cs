﻿
namespace HerramientasAFIP
{
    partial class fmInformarCAEASinMovimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInformarCAEA = new System.Windows.Forms.Button();
            this.txtCaea = new System.Windows.Forms.TextBox();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInformarCAEA
            // 
            this.btnInformarCAEA.Location = new System.Drawing.Point(82, 113);
            this.btnInformarCAEA.Name = "btnInformarCAEA";
            this.btnInformarCAEA.Size = new System.Drawing.Size(397, 39);
            this.btnInformarCAEA.TabIndex = 15;
            this.btnInformarCAEA.Text = "Informar CAEA";
            this.btnInformarCAEA.UseVisualStyleBackColor = true;
            this.btnInformarCAEA.Click += new System.EventHandler(this.btnInformarCAEA_Click);
            // 
            // txtCaea
            // 
            this.txtCaea.Location = new System.Drawing.Point(182, 69);
            this.txtCaea.Name = "txtCaea";
            this.txtCaea.Size = new System.Drawing.Size(361, 26);
            this.txtCaea.TabIndex = 14;
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Location = new System.Drawing.Point(182, 27);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.Size = new System.Drawing.Size(126, 26);
            this.txtPtoVta.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "CAEA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Punto de Venta";
            // 
            // fmInformarCAEASinMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 215);
            this.Controls.Add(this.btnInformarCAEA);
            this.Controls.Add(this.txtCaea);
            this.Controls.Add(this.txtPtoVta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fmInformarCAEASinMovimiento";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Informar CAEA Sin Movimiento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInformarCAEA;
        private System.Windows.Forms.TextBox txtCaea;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}