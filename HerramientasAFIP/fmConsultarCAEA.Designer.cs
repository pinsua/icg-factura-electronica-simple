﻿
namespace HerramientasAFIP
{
    partial class fmConsultarCAEA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCaea = new System.Windows.Forms.GroupBox();
            this.txtFechaTope = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFechaProceso = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFechaHasta = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFechaDesde = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCAEA = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConsultarCAEA = new System.Windows.Forms.Button();
            this.txtOrden = new System.Windows.Forms.TextBox();
            this.txtPeriodo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbCaea.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCaea
            // 
            this.gbCaea.Controls.Add(this.txtFechaTope);
            this.gbCaea.Controls.Add(this.label7);
            this.gbCaea.Controls.Add(this.txtFechaProceso);
            this.gbCaea.Controls.Add(this.label6);
            this.gbCaea.Controls.Add(this.txtFechaHasta);
            this.gbCaea.Controls.Add(this.label5);
            this.gbCaea.Controls.Add(this.txtFechaDesde);
            this.gbCaea.Controls.Add(this.label4);
            this.gbCaea.Controls.Add(this.txtCAEA);
            this.gbCaea.Controls.Add(this.label3);
            this.gbCaea.Location = new System.Drawing.Point(12, 161);
            this.gbCaea.Name = "gbCaea";
            this.gbCaea.Size = new System.Drawing.Size(515, 240);
            this.gbCaea.TabIndex = 11;
            this.gbCaea.TabStop = false;
            this.gbCaea.Text = "CAEA Obtenido.";
            // 
            // txtFechaTope
            // 
            this.txtFechaTope.Location = new System.Drawing.Point(149, 187);
            this.txtFechaTope.Name = "txtFechaTope";
            this.txtFechaTope.ReadOnly = true;
            this.txtFechaTope.Size = new System.Drawing.Size(313, 26);
            this.txtFechaTope.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Fecha Tope";
            // 
            // txtFechaProceso
            // 
            this.txtFechaProceso.Location = new System.Drawing.Point(149, 152);
            this.txtFechaProceso.Name = "txtFechaProceso";
            this.txtFechaProceso.ReadOnly = true;
            this.txtFechaProceso.Size = new System.Drawing.Size(313, 26);
            this.txtFechaProceso.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fecha Proceso";
            // 
            // txtFechaHasta
            // 
            this.txtFechaHasta.Location = new System.Drawing.Point(149, 112);
            this.txtFechaHasta.Name = "txtFechaHasta";
            this.txtFechaHasta.ReadOnly = true;
            this.txtFechaHasta.Size = new System.Drawing.Size(313, 26);
            this.txtFechaHasta.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Fecha Hasta";
            // 
            // txtFechaDesde
            // 
            this.txtFechaDesde.Location = new System.Drawing.Point(149, 76);
            this.txtFechaDesde.Name = "txtFechaDesde";
            this.txtFechaDesde.ReadOnly = true;
            this.txtFechaDesde.Size = new System.Drawing.Size(313, 26);
            this.txtFechaDesde.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Fecha Desde";
            // 
            // txtCAEA
            // 
            this.txtCAEA.Location = new System.Drawing.Point(149, 33);
            this.txtCAEA.Name = "txtCAEA";
            this.txtCAEA.ReadOnly = true;
            this.txtCAEA.Size = new System.Drawing.Size(313, 26);
            this.txtCAEA.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "CAEA";
            // 
            // btnConsultarCAEA
            // 
            this.btnConsultarCAEA.Location = new System.Drawing.Point(144, 107);
            this.btnConsultarCAEA.Name = "btnConsultarCAEA";
            this.btnConsultarCAEA.Size = new System.Drawing.Size(252, 39);
            this.btnConsultarCAEA.TabIndex = 10;
            this.btnConsultarCAEA.Text = "Consultar CAEA";
            this.btnConsultarCAEA.UseVisualStyleBackColor = true;
            this.btnConsultarCAEA.Click += new System.EventHandler(this.btnConsultarCAEA_Click);
            // 
            // txtOrden
            // 
            this.txtOrden.Location = new System.Drawing.Point(270, 63);
            this.txtOrden.Name = "txtOrden";
            this.txtOrden.Size = new System.Drawing.Size(126, 26);
            this.txtOrden.TabIndex = 9;
            // 
            // txtPeriodo
            // 
            this.txtPeriodo.Location = new System.Drawing.Point(270, 21);
            this.txtPeriodo.Name = "txtPeriodo";
            this.txtPeriodo.Size = new System.Drawing.Size(127, 26);
            this.txtPeriodo.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Quincena (1, 2)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Periodo (MMAA)";
            // 
            // fmConsultarCAEA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 422);
            this.Controls.Add(this.gbCaea);
            this.Controls.Add(this.btnConsultarCAEA);
            this.Controls.Add(this.txtOrden);
            this.Controls.Add(this.txtPeriodo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fmConsultarCAEA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar CAEA";
            this.gbCaea.ResumeLayout(false);
            this.gbCaea.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCaea;
        private System.Windows.Forms.TextBox txtFechaTope;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFechaProceso;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFechaHasta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFechaDesde;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCAEA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnConsultarCAEA;
        private System.Windows.Forms.TextBox txtOrden;
        private System.Windows.Forms.TextBox txtPeriodo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}