﻿
namespace HerramientasAFIP
{
    partial class fmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.facturaComúnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obtenerPuntosDeVentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obtenerUltimoNroUtilizadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cAEAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obtenerCAEAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarCAEAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturaMTXCAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btSave = new System.Windows.Forms.Button();
            this.chkIsTest = new System.Windows.Forms.CheckBox();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCertificado = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.informarCAEASinMovimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.facturaComúnToolStripMenuItem,
            this.facturaMTXCAToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1222, 36);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // facturaComúnToolStripMenuItem
            // 
            this.facturaComúnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.obtenerPuntosDeVentaToolStripMenuItem,
            this.obtenerUltimoNroUtilizadoToolStripMenuItem,
            this.toolStripMenuItem1,
            this.cAEAToolStripMenuItem});
            this.facturaComúnToolStripMenuItem.Name = "facturaComúnToolStripMenuItem";
            this.facturaComúnToolStripMenuItem.Size = new System.Drawing.Size(147, 30);
            this.facturaComúnToolStripMenuItem.Text = "Factura Común";
            // 
            // obtenerPuntosDeVentaToolStripMenuItem
            // 
            this.obtenerPuntosDeVentaToolStripMenuItem.Name = "obtenerPuntosDeVentaToolStripMenuItem";
            this.obtenerPuntosDeVentaToolStripMenuItem.Size = new System.Drawing.Size(373, 34);
            this.obtenerPuntosDeVentaToolStripMenuItem.Text = "Obtener puntos de Venta";
            this.obtenerPuntosDeVentaToolStripMenuItem.Click += new System.EventHandler(this.obtenerPuntosDeVentaToolStripMenuItem_Click);
            // 
            // obtenerUltimoNroUtilizadoToolStripMenuItem
            // 
            this.obtenerUltimoNroUtilizadoToolStripMenuItem.Name = "obtenerUltimoNroUtilizadoToolStripMenuItem";
            this.obtenerUltimoNroUtilizadoToolStripMenuItem.Size = new System.Drawing.Size(373, 34);
            this.obtenerUltimoNroUtilizadoToolStripMenuItem.Text = "Obtener ultimo nro utilizado";
            this.obtenerUltimoNroUtilizadoToolStripMenuItem.Click += new System.EventHandler(this.obtenerUltimoNroUtilizadoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(373, 34);
            this.toolStripMenuItem1.Text = "Obtener Datos del comprobante";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // cAEAToolStripMenuItem
            // 
            this.cAEAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.procesarExcelToolStripMenuItem,
            this.obtenerCAEAToolStripMenuItem,
            this.consultarCAEAToolStripMenuItem,
            this.informarCAEASinMovimientoToolStripMenuItem});
            this.cAEAToolStripMenuItem.Name = "cAEAToolStripMenuItem";
            this.cAEAToolStripMenuItem.Size = new System.Drawing.Size(373, 34);
            this.cAEAToolStripMenuItem.Text = "CAEA";
            // 
            // procesarExcelToolStripMenuItem
            // 
            this.procesarExcelToolStripMenuItem.Name = "procesarExcelToolStripMenuItem";
            this.procesarExcelToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.procesarExcelToolStripMenuItem.Text = "Procesar Excel";
            this.procesarExcelToolStripMenuItem.Click += new System.EventHandler(this.procesarExcelToolStripMenuItem_Click);
            // 
            // obtenerCAEAToolStripMenuItem
            // 
            this.obtenerCAEAToolStripMenuItem.Name = "obtenerCAEAToolStripMenuItem";
            this.obtenerCAEAToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.obtenerCAEAToolStripMenuItem.Text = "Obtener CAEA";
            this.obtenerCAEAToolStripMenuItem.Click += new System.EventHandler(this.obtenerCAEAToolStripMenuItem_Click);
            // 
            // consultarCAEAToolStripMenuItem
            // 
            this.consultarCAEAToolStripMenuItem.Name = "consultarCAEAToolStripMenuItem";
            this.consultarCAEAToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.consultarCAEAToolStripMenuItem.Text = "Consultar CAEA";
            this.consultarCAEAToolStripMenuItem.Click += new System.EventHandler(this.consultarCAEAToolStripMenuItem_Click);
            // 
            // facturaMTXCAToolStripMenuItem
            // 
            this.facturaMTXCAToolStripMenuItem.Name = "facturaMTXCAToolStripMenuItem";
            this.facturaMTXCAToolStripMenuItem.Size = new System.Drawing.Size(148, 29);
            this.facturaMTXCAToolStripMenuItem.Text = "Factura MTXCA";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.chkIsTest);
            this.groupBox1.Controls.Add(this.txtCuit);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCertificado);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1222, 218);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de conexión a AFIP";
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(428, 157);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(268, 39);
            this.btSave.TabIndex = 38;
            this.btSave.Text = "Guardar configuración";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // chkIsTest
            // 
            this.chkIsTest.AutoSize = true;
            this.chkIsTest.Location = new System.Drawing.Point(485, 96);
            this.chkIsTest.Name = "chkIsTest";
            this.chkIsTest.Size = new System.Drawing.Size(137, 24);
            this.chkIsTest.TabIndex = 37;
            this.chkIsTest.Text = "Homologación";
            this.chkIsTest.UseVisualStyleBackColor = true;
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(84, 43);
            this.txtCuit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.Size = new System.Drawing.Size(194, 26);
            this.txtCuit.TabIndex = 22;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(862, 43);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(148, 26);
            this.txtPassword.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "CUIT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(776, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Password";
            // 
            // txtCertificado
            // 
            this.txtCertificado.Location = new System.Drawing.Point(400, 43);
            this.txtCertificado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCertificado.Name = "txtCertificado";
            this.txtCertificado.Size = new System.Drawing.Size(328, 26);
            this.txtCertificado.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(307, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 20);
            this.label3.TabIndex = 28;
            this.label3.Text = "Certificado";
            // 
            // informarCAEASinMovimientoToolStripMenuItem
            // 
            this.informarCAEASinMovimientoToolStripMenuItem.Name = "informarCAEASinMovimientoToolStripMenuItem";
            this.informarCAEASinMovimientoToolStripMenuItem.Size = new System.Drawing.Size(362, 34);
            this.informarCAEASinMovimientoToolStripMenuItem.Text = "Informar CAEA Sin Movimiento";
            this.informarCAEASinMovimientoToolStripMenuItem.Click += new System.EventHandler(this.informarCAEASinMovimientoToolStripMenuItem_Click);
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 254);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "fmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Herramientas AFIP";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem facturaComúnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obtenerPuntosDeVentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obtenerUltimoNroUtilizadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturaMTXCAToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCertificado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.CheckBox chkIsTest;
        private System.Windows.Forms.ToolStripMenuItem cAEAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesarExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obtenerCAEAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarCAEAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem informarCAEASinMovimientoToolStripMenuItem;
    }
}

