﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HerramientasAFIP
{
    public partial class fmObtenerCAEA : Form
    {
        private string _cuit;
        private string _certificado;
        private string _password;
        private string _pathCertificado;
        private bool _esTest;
        private string _pathLog;
        private string _pathTaFC;

        public fmObtenerCAEA(string cuit, string certificado, string password,
            string pathcertificado, string pathlog, string pathtafc, bool esTest)
        {
            InitializeComponent();

            _cuit = cuit;
            _certificado = certificado;
            _password = password;
            _pathCertificado = pathcertificado;
            _esTest = esTest;
            _pathLog = pathlog;
            _pathTaFC = pathtafc;
        }

        private void btnObtenerCAEA_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(_cuit))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(_certificado))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(_password))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            txtCAEA.Text = "";
            txtFechaDesde.Text = "";
            txtFechaHasta.Text = "";
            txtFechaProceso.Text = "";
            txtFechaTope.Text = "";

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCertificado.Contains(_certificado))
                    _pathCertificado = _pathCertificado + @"\" + _certificado;

                AfipDll.CAEA _caea = new AfipDll.CAEA();

                int _periodo = Convert.ToInt32(txtPeriodo.Text);
                int _quincena = Convert.ToInt32(txtOrden.Text);

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _esTest, out _lstErrores);

                if (_caea.Caea != null)
                {
                    txtCAEA.Text = _caea.Caea;
                    txtFechaDesde.Text = _caea.FechaDesde.ToString();
                    txtFechaHasta.Text = _caea.FechaHasta.ToString();
                    txtFechaProceso.Text = _caea.FechaProceso.ToString();
                    txtFechaTope.Text = _caea.FechaTope.ToString();
                }
                else
                {
                    string _error = "";
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        _error = "Codigo de error: " + _er.Code + "Descripcion: " + _er.Msg + Environment.NewLine;
                    }

                    MessageBox.Show(_error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
