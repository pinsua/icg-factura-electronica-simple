﻿namespace IcgPlugInFacturacion
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            Janus.Windows.GridEX.GridEXLayout gridEXLayout1 = new Janus.Windows.GridEX.GridEXLayout();
            this.pnlSeleccion = new System.Windows.Forms.Panel();
            this.gbSeleccion = new System.Windows.Forms.GroupBox();
            this.cbEmpresas = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.lsClientes = new System.Windows.Forms.ListBox();
            this.lsTipoDoc = new System.Windows.Forms.ListBox();
            this.btnSearchClientes = new System.Windows.Forms.Button();
            this.btnSearchTipoDoc = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.lblSerie = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblTipoDoc = new System.Windows.Forms.Label();
            this.lblFechaHasta = new System.Windows.Forms.Label();
            this.lblFechaDesde = new System.Windows.Forms.Label();
            this.pnlCliente = new System.Windows.Forms.Panel();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.grdGrid = new Janus.Windows.GridEX.GridEX();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.btnBuscarCae = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSeleccion.SuspendLayout();
            this.gbSeleccion.SuspendLayout();
            this.pnlCliente.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGrid)).BeginInit();
            this.pnlFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSeleccion
            // 
            this.pnlSeleccion.Controls.Add(this.gbSeleccion);
            this.pnlSeleccion.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSeleccion.Location = new System.Drawing.Point(0, 0);
            this.pnlSeleccion.Name = "pnlSeleccion";
            this.pnlSeleccion.Size = new System.Drawing.Size(1028, 180);
            this.pnlSeleccion.TabIndex = 0;
            // 
            // gbSeleccion
            // 
            this.gbSeleccion.Controls.Add(this.cbEmpresas);
            this.gbSeleccion.Controls.Add(this.lblEmpresa);
            this.gbSeleccion.Controls.Add(this.dtpHasta);
            this.gbSeleccion.Controls.Add(this.dtpDesde);
            this.gbSeleccion.Controls.Add(this.btnAceptar);
            this.gbSeleccion.Controls.Add(this.lsClientes);
            this.gbSeleccion.Controls.Add(this.lsTipoDoc);
            this.gbSeleccion.Controls.Add(this.btnSearchClientes);
            this.gbSeleccion.Controls.Add(this.btnSearchTipoDoc);
            this.gbSeleccion.Controls.Add(this.textBox3);
            this.gbSeleccion.Controls.Add(this.lblSerie);
            this.gbSeleccion.Controls.Add(this.lblCliente);
            this.gbSeleccion.Controls.Add(this.lblTipoDoc);
            this.gbSeleccion.Controls.Add(this.lblFechaHasta);
            this.gbSeleccion.Controls.Add(this.lblFechaDesde);
            this.gbSeleccion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSeleccion.Location = new System.Drawing.Point(0, 0);
            this.gbSeleccion.Name = "gbSeleccion";
            this.gbSeleccion.Size = new System.Drawing.Size(1028, 180);
            this.gbSeleccion.TabIndex = 0;
            this.gbSeleccion.TabStop = false;
            this.gbSeleccion.Text = "Selección";
            this.gbSeleccion.Enter += new System.EventHandler(this.gbSeleccion_Enter);
            // 
            // cbEmpresas
            // 
            this.cbEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEmpresas.FormattingEnabled = true;
            this.cbEmpresas.Location = new System.Drawing.Point(177, 23);
            this.cbEmpresas.Name = "cbEmpresas";
            this.cbEmpresas.Size = new System.Drawing.Size(435, 21);
            this.cbEmpresas.TabIndex = 18;
            this.cbEmpresas.SelectedIndexChanged += new System.EventHandler(this.cbEmpresas_SelectedIndexChanged);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(36, 26);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(125, 13);
            this.lblEmpresa.TabIndex = 17;
            this.lblEmpresa.Text = "Seleccione una Empresa";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(304, 59);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(101, 20);
            this.dtpHasta.TabIndex = 16;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(113, 59);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(97, 20);
            this.dtpDesde.TabIndex = 15;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(873, 59);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 14;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // lsClientes
            // 
            this.lsClientes.DisplayMember = "NombreCliente";
            this.lsClientes.FormattingEnabled = true;
            this.lsClientes.Location = new System.Drawing.Point(113, 90);
            this.lsClientes.Name = "lsClientes";
            this.lsClientes.Size = new System.Drawing.Size(291, 69);
            this.lsClientes.TabIndex = 13;
            // 
            // lsTipoDoc
            // 
            this.lsTipoDoc.DisplayMember = "Descripcion";
            this.lsTipoDoc.FormattingEnabled = true;
            this.lsTipoDoc.Location = new System.Drawing.Point(496, 90);
            this.lsTipoDoc.Name = "lsTipoDoc";
            this.lsTipoDoc.Size = new System.Drawing.Size(310, 69);
            this.lsTipoDoc.TabIndex = 12;
            // 
            // btnSearchClientes
            // 
            this.btnSearchClientes.AutoSize = true;
            this.btnSearchClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchClientes.Image")));
            this.btnSearchClientes.Location = new System.Drawing.Point(410, 93);
            this.btnSearchClientes.Name = "btnSearchClientes";
            this.btnSearchClientes.Size = new System.Drawing.Size(22, 23);
            this.btnSearchClientes.TabIndex = 11;
            this.btnSearchClientes.UseVisualStyleBackColor = true;
            this.btnSearchClientes.Click += new System.EventHandler(this.btnSearchClientes_Click);
            // 
            // btnSearchTipoDoc
            // 
            this.btnSearchTipoDoc.AutoSize = true;
            this.btnSearchTipoDoc.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTipoDoc.Image")));
            this.btnSearchTipoDoc.Location = new System.Drawing.Point(812, 90);
            this.btnSearchTipoDoc.Name = "btnSearchTipoDoc";
            this.btnSearchTipoDoc.Size = new System.Drawing.Size(22, 23);
            this.btnSearchTipoDoc.TabIndex = 10;
            this.btnSearchTipoDoc.UseVisualStyleBackColor = true;
            this.btnSearchTipoDoc.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(496, 59);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(310, 20);
            this.textBox3.TabIndex = 9;
            // 
            // lblSerie
            // 
            this.lblSerie.AutoSize = true;
            this.lblSerie.Location = new System.Drawing.Point(459, 62);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(31, 13);
            this.lblSerie.TabIndex = 4;
            this.lblSerie.Text = "Serie";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Location = new System.Drawing.Point(68, 94);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(39, 13);
            this.lblCliente.TabIndex = 3;
            this.lblCliente.Text = "Cliente";
            // 
            // lblTipoDoc
            // 
            this.lblTipoDoc.AutoSize = true;
            this.lblTipoDoc.Location = new System.Drawing.Point(436, 93);
            this.lblTipoDoc.Name = "lblTipoDoc";
            this.lblTipoDoc.Size = new System.Drawing.Size(54, 13);
            this.lblTipoDoc.TabIndex = 2;
            this.lblTipoDoc.Text = "Tipo Doc.";
            // 
            // lblFechaHasta
            // 
            this.lblFechaHasta.AutoSize = true;
            this.lblFechaHasta.Location = new System.Drawing.Point(230, 62);
            this.lblFechaHasta.Name = "lblFechaHasta";
            this.lblFechaHasta.Size = new System.Drawing.Size(68, 13);
            this.lblFechaHasta.TabIndex = 1;
            this.lblFechaHasta.Text = "Fecha Hasta";
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.AutoSize = true;
            this.lblFechaDesde.Location = new System.Drawing.Point(36, 62);
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.Size = new System.Drawing.Size(71, 13);
            this.lblFechaDesde.TabIndex = 0;
            this.lblFechaDesde.Text = "Fecha Desde";
            // 
            // pnlCliente
            // 
            this.pnlCliente.Controls.Add(this.pnlGrid);
            this.pnlCliente.Controls.Add(this.pnlFooter);
            this.pnlCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCliente.Location = new System.Drawing.Point(0, 180);
            this.pnlCliente.Name = "pnlCliente";
            this.pnlCliente.Size = new System.Drawing.Size(1028, 323);
            this.pnlCliente.TabIndex = 1;
            // 
            // pnlGrid
            // 
            this.pnlGrid.Controls.Add(this.grdGrid);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 0);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(1028, 273);
            this.pnlGrid.TabIndex = 1;
            // 
            // grdGrid
            // 
            gridEXLayout1.LayoutString = resources.GetString("gridEXLayout1.LayoutString");
            this.grdGrid.DesignTimeLayout = gridEXLayout1;
            this.grdGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdGrid.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.grdGrid.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.grdGrid.FilterRowFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdGrid.GroupByBoxVisible = false;
            this.grdGrid.HideSelection = Janus.Windows.GridEX.HideSelection.Highlight;
            this.grdGrid.Location = new System.Drawing.Point(0, 0);
            this.grdGrid.Name = "grdGrid";
            this.grdGrid.RecordNavigator = true;
            this.grdGrid.Size = new System.Drawing.Size(1028, 273);
            this.grdGrid.TabIndex = 0;
            this.grdGrid.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdGrid.SelectionChanged += new System.EventHandler(this.grdGrid_SelectionChanged);
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnBuscarCae);
            this.pnlFooter.Controls.Add(this.btnSalir);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 273);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Size = new System.Drawing.Size(1028, 50);
            this.pnlFooter.TabIndex = 0;
            // 
            // btnBuscarCae
            // 
            this.btnBuscarCae.Location = new System.Drawing.Point(856, 15);
            this.btnBuscarCae.Name = "btnBuscarCae";
            this.btnBuscarCae.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarCae.TabIndex = 1;
            this.btnBuscarCae.Text = "&Buscar CAE";
            this.btnBuscarCae.UseVisualStyleBackColor = true;
            this.btnBuscarCae.Click += new System.EventHandler(this.btnBuscarCae_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(937, 15);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "&Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 1000;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 500;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.toolTip1.ToolTipTitle = "Error CAE";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 503);
            this.Controls.Add(this.pnlCliente);
            this.Controls.Add(this.pnlSeleccion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Facturación Electrónica";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.pnlSeleccion.ResumeLayout(false);
            this.gbSeleccion.ResumeLayout(false);
            this.gbSeleccion.PerformLayout();
            this.pnlCliente.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGrid)).EndInit();
            this.pnlFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSeleccion;
        private System.Windows.Forms.GroupBox gbSeleccion;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblTipoDoc;
        private System.Windows.Forms.Label lblFechaHasta;
        private System.Windows.Forms.Label lblFechaDesde;
        private System.Windows.Forms.Button btnSearchTipoDoc;
        private System.Windows.Forms.Button btnSearchClientes;
        private System.Windows.Forms.ListBox lsTipoDoc;
        private System.Windows.Forms.ListBox lsClientes;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Panel pnlCliente;
        private System.Windows.Forms.Panel pnlGrid;
        private Janus.Windows.GridEX.GridEX grdGrid;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Button btnBuscarCae;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.ComboBox cbEmpresas;
        private System.Windows.Forms.Label lblEmpresa;
    }
}