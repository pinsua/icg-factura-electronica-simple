﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IcgPlugInFacturacion
{
    public partial class frmFcExportacion : Form
    {
        public string _numSerie;
        public string _numFactura;
        public string _n;
        public string _codAfip;
        public string _ptoVta;
        public DataAccess.DatosFacturaExpo _datosFcExpo = new DataAccess.DatosFacturaExpo();

        public frmFcExportacion(string pNumSerie, string pNumFactura, string pN, string pcodAfip, string pptaVta)
        {
            InitializeComponent();
            _numSerie = pNumSerie;
            _numFactura = pNumFactura;
            _n = pN;
            _codAfip = pcodAfip;
            _ptoVta = pptaVta;
        }

        private void frmFcExportacion_Load(object sender, EventArgs e)
        {
            try
            {
                _datosFcExpo = DataAccess.DatosFacturaExpo.GetDatosFacturaExpo(_numSerie, _numFactura, _n, frmMain.strConnectionString);
                _datosFcExpo.CodAfip = _codAfip;
                txtFecha.Text = _datosFcExpo.fecha.ToShortDateString();
                txtNroComprobante.Text = _datosFcExpo.numFactura.ToString(); //TODO: ver este tema.
                txtPtoVta.Text = _datosFcExpo.PtoVta;
                txtTipoComprobante.Text = _datosFcExpo.CodAfip;
                txtImporteTotal.Text = _datosFcExpo.totalBruto.ToString();

                List<Monedas> _monedas = Monedas.GetMonedas();
                cboMoneda.DataSource = _monedas;
                cboMoneda.DisplayMember = "_nombre";
                cboMoneda.ValueMember = "_codigo";
                cboMoneda.SelectedIndex = -1;

                List<TiposExportacion> _tiposExpo = TiposExportacion.GetTiposExportacion();
                cboTiposExportacion.DataSource = _tiposExpo;
                cboTiposExportacion.DisplayMember = "_nombre";
                cboTiposExportacion.ValueMember = "_codigo";
                cboTiposExportacion.SelectedIndex = -1;

                List<Paises> _paises = Paises.GetPaises();
                cboPaises.DataSource = _paises;
                cboPaises.DisplayMember = "_nombre";
                cboPaises.ValueMember = "_codigo";
                cboPaises.SelectedIndex = -1;

                List<Incoterms> _incoterms = Incoterms.GetIncoterms()
;
                cboIncoterms.DataSource = _incoterms;
                cboIncoterms.DisplayMember = "_nombre";
                cboIncoterms.ValueMember = "_codigo";
                cboIncoterms.SelectedIndex = -1;

                DataAccess.DatosClienteExpo _cliente = DataAccess.DatosClienteExpo.GetDatosClienteExpo(_datosFcExpo.codCliente, frmMain.strConnectionString);
                txtCliente.Text = _cliente._razonSocial;
                txtCuit.Text = _cliente._cuit;
                txtDomicilio.Text = _cliente._domicilio;

                List<DataAccess.DatosItemsExpo> _items = DataAccess.DatosItemsExpo.GetDatosItemsExpo(_numSerie, Convert.ToInt32(_numFactura), _n, frmMain.strConnectionString);
                dgvItems.DataSource = _items;

                dgvItems.Columns[0].Visible = false;
                dgvItems.Columns[1].Visible = false;
                dgvItems.Columns[2].Visible = false;
                dgvItems.Columns[3].Visible = false;
                dgvItems.Columns[5].Width = 250;
                dgvItems.Columns[7].DefaultCellStyle.Format = "c";
                dgvItems.Columns[8].DefaultCellStyle.Format = "c";

            }
            catch(Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message);
                this.Close();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCae_Click(object sender, EventArgs e)
        {
            List<string> _Validaciones = ValidarDatos();

            //if(_Validaciones.Count > 0)
            //{
            //    string _mensaje = "";
            //    foreach(string st in _Validaciones)
            //    {
            //        _mensaje = _mensaje + st + Environment.NewLine;
            //    }

            //    MessageBox.Show("Por favor revise los mensajes y corrijalos." + 
            //        Environment.NewLine + Environment.NewLine + _mensaje, 
            //        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //else
            //{
            //    //Proceso.
            //    string _rtdo;
            //    string _error;
            //    //string _ptoVta;
            //    //Recupero el ultimo numero enviado.
            //    int _ultimoNro = wsAfipExpo.GetLastCmp(frmMain._pathCerificado, frmMain._pathTaFCE, frmMain.strCUIT, frmMain._EsTest, _ptoVta, Convert.ToInt32(_codAfip), out _rtdo, out _error);
            //    //Si tengo nro proceso.
            //    if(_rtdo.ToUpper() ==  "OK")
            //    {
            //        string _nroCae = "";
            //        string _rtdoCae = "";
            //        string _fechaVtoCae = "";
            //        string _errorCae = "";

            //        _ultimoNro = _ultimoNro + 1;

            //        string _nroComprobante = "E" +_ptoVta + _ultimoNro.ToString().PadLeft(8, '0');
            //        int _concepto = Convert.ToInt32(cboTiposExportacion.SelectedValue);
            //        DateTime _fecha = Convert.ToDateTime(txtFecha.Text);
            //        //Cargamos la cabecera.
            //        wsAfipExpo.Cabecera _cab = CargoCabecera(_ultimoNro, _ptoVta);
            //        //Cargmos los detalles.
            //        List<wsAfipExpo.Detalle> _det = CargoDetalles();
            //        //Recupero los comprobantes asociados.
            //        List<wsExportacion.Cmp_asoc> _ComprobantesAsociados = ArmarCbteAsocExpo(_datosFcExpo);

            //        try
            //        {
            //            bool _todoOk = false;
            //            //tenemos todoa vamos por el CAE.
            //            bool _caeOk = wsAfipExpo.AutorizarExpo(frmMain._pathCerificado, frmMain._pathTaFCE, frmMain._pathLog, frmMain.strCUIT, frmMain._EsTest,
            //                _cab, _det, out _rtdoCae, out _nroCae, out _fechaVtoCae, out _errorCae);


            //            if (_caeOk)
            //            {
            //                string _codBarra = "";
            //                switch (_rtdoCae.ToUpper())
            //                {
            //                    case "A":
            //                        {
            //                            //Aprobado.
            //                            DataAccess.FuncionesVarias.GenerarCodigoBarra(frmMain.strCUIT, _codAfip, _cab.ptovta.ToString(), _nroCae, _fechaVtoCae, out _codBarra);
            //                            _todoOk = true;
            //                            break;
            //                        }
            //                    case "R":
            //                        {
            //                            //Rechazado.

            //                            break;
            //                        }
            //                    case "P":
            //                        {
            //                            //Parcial con Observaciones.
            //                            DataAccess.FuncionesVarias.GenerarCodigoBarra(frmMain.strCUIT, _codAfip, _cab.ptovta.ToString(), _nroCae, _fechaVtoCae, out _codBarra);
            //                            _todoOk = true;
            //                            break;
            //                        }
            //                    default:
            //                        {
            //                            //Cualquier otro.
            //                            break;
            //                        }
            //                }
            //                //Modificamos el registro en FacturasCamposLibres.
            //                DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(_numSerie, _numFactura, _n, _nroCae, _errorCae, _fechaVtoCae, _codBarra, frmMain.strConnectionString);

            //                //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.                                            
            //                if (!String.IsNullOrEmpty(_nroCae))
            //                {
            //                    DataAccess.FuncionesVarias.Insertar_FacturasVentaSerieResol(_numSerie, _numFactura, _n, _ptoVta, _codAfip, _ultimoNro, frmMain.strConnectionString);
            //                }

            //                if (_todoOk)
            //                    this.Close();
            //            }
            //            else
            //            {
            //                //Error al recuperar el ultimo numero de factura de la AFIP
            //                DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(_numSerie, _numFactura, _n, "", _errorCae, "", "",frmMain.strConnectionString);
            //            }
            //        }
            //        catch(Exception ex)
            //        {
            //            //Recupero el nro del ultimo comprobante.
            //            int _ultimoNroNuevo = wsAfipExpo.GetLastId(frmMain._pathCerificado, frmMain._pathTaFCE, frmMain.strCUIT, frmMain._EsTest, out _rtdo, out _error, out _ptoVta);
            //            //Vemos si el ultimo comprobante coincide con el ultimo enviado.
            //            if (_ultimoNro == _ultimoNroNuevo)
            //            {
            //                //Guardamos el numero y ponemos el estado en Recuperar.
            //                //Modificamos el registro en FacturasCamposLibres.
            //                DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(_numSerie,_numFactura,_n, _ultimoNro, ex.Message, frmMain.strConnectionString);
            //            }

            //            this.Close();
            //        }

            //    }
            //    else
            //    {
            //        MessageBox.Show("No se pudo obtener comunicación con la AFIP. Por favor intente nuevamente.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}
        }

        private List<string> ValidarDatos()
        {
            List<string> _rta = new List<string>();
            //Validamos los combos.
            if (cboMoneda.SelectedIndex == -1)
                _rta.Add("Debe seleccionar una Moneda.");
            if (cboPaises.SelectedIndex == -1)
                _rta.Add("Debe seleccionar un Pais.");
            if (cboTiposExportacion.SelectedIndex == -1)
                _rta.Add("Debe seleccionar un Tipo de Exportación");

            //Cotizacion.
            if (String.IsNullOrEmpty(txtCotizacion.Text))
                _rta.Add("Debe ingresar la cotización de la moneda.");
            try
            { decimal pp = Convert.ToDecimal(txtCotizacion.Text); }
            catch
            { _rta.Add("El valor de la cotización de la moneda debe ser decimal."); }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
                _rta.Add("No hay punto de venta declarado. Por favor configurelo y luego vuelva a solicitar CAE.");

            if (String.IsNullOrEmpty(txtCliente.Text))
                _rta.Add("No hay Razon Social. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            if (String.IsNullOrEmpty(txtCuit.Text))
                _rta.Add("No hay CUIT en el cliente. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            if(String.IsNullOrEmpty(txtDomicilio.Text))
                _rta.Add("No hay Domicilio en el cliente. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            //Items.
            if (dgvItems.Rows.Count == 0)
                _rta.Add("No hay Items en la factura.");

            decimal _total = 0;
            foreach(DataGridViewRow _rw in dgvItems.Rows)
            {
                decimal _ct = Convert.ToDecimal(_rw.Cells["TOTAL"].Value);               
                _total = _total + _ct;
            }

            decimal _txtTotal = Convert.ToDecimal(txtImporteTotal.Text);
            //Vemos dto comercial
            if (_datosFcExpo.totDtoComercial > 0)
                _total = _total - _datosFcExpo.totDtoComercial;
            //Vemos dto Pp
            if (_datosFcExpo.totDtoPp > 0)
                _total = _total - _datosFcExpo.totDtoPp;

            if (_txtTotal != _total)
                _rta.Add("La sumatoria de los totales de Items no coincide con el total de la Factura.");

            return _rta;
        }

        //private wsAfipExpo.Cabecera CargoCabecera(int ultimoNro, string ptoVta)
        //{
        //    string _nroComprobante = "E" + ptoVta + ultimoNro.ToString().PadLeft(8, '0');
        //    int _concepto = Convert.ToInt32(cboTiposExportacion.SelectedValue);
        //    DateTime _fecha = Convert.ToDateTime(txtFecha.Text);
        //    //Cargamos la cabecera.
        //    wsAfipExpo.Cabecera _cab = new wsAfipExpo.Cabecera();
        //    _cab.ape = "S";
        //    _cab.comprobante = _nroComprobante;
        //    _cab.concepto = _concepto;
        //    _cab.cuitemisor = frmMain.strCUIT;
        //    _cab.destinocmp = cboPaises.SelectedValue.ToString();
        //    _cab.detalles = "";
        //    _cab.domicilioreceptor = txtDomicilio.Text;
        //    _cab.fechaemision = _fecha.Year.ToString() + _fecha.Month.ToString().PadLeft(2, '0') + _fecha.Day.ToString().PadLeft(2, '0');
        //    _cab.formaspago = "Contado";
        //    _cab.idimpositivoreceptor = txtCuit.Text;
        //    _cab.idioma = 1;
        //    _cab.importetotal = txtImporteTotal.Text.Replace(',','.');
        //    _cab.incoterms = cboIncoterms.SelectedValue.ToString();
        //    _cab.moneda = cboMoneda.SelectedValue.ToString();
        //    _cab.nrodocreceptor = "";
        //    _cab.otrosdatosgenerales = _numFactura;
        //    _cab.ptovta = ptoVta;
        //    _cab.receptor = txtCliente.Text;
        //    _cab.Tipo = Convert.ToInt32(_codAfip);
        //    _cab.tipocambio = txtCotizacion.Text;
        //    _cab.tipodocrecptor = "";

        //    return _cab;
        //}

        //private List<wsAfipExpo.Detalle> CargoDetalles()
        //{
        //    List<wsAfipExpo.Detalle> _det = new List<wsAfipExpo.Detalle>();
        //    //Recorro el grid.
        //    foreach (DataGridViewRow _rw in dgvItems.Rows)
        //    {

        //        wsAfipExpo.Detalle _dt = new wsAfipExpo.Detalle();
        //        _dt.cant = _rw.Cells["unidades"].Value.ToString().Replace(',','.');
        //        _dt.cod = _rw.Cells["referencia"].Value.ToString();
        //        _dt.descrip = _rw.Cells["descripcion"].Value.ToString();
        //        _dt.importe = Math.Round(Convert.ToDecimal(_rw.Cells["total"].Value, System.Globalization.CultureInfo.InvariantCulture),2).ToString().Replace(',', '.');
        //        _dt.preciounit = Math.Round(Convert.ToDecimal(_rw.Cells["precio"].Value, System.Globalization.CultureInfo.InvariantCulture),2).ToString().Replace(',', '.');
        //        _dt.unimed = _rw.Cells["codigoaduana"].Value.ToString();
        //        _dt.dto = _rw.Cells["dto"].Value.ToString().Replace(',', '.');

        //        if (Convert.ToDecimal(_dt.dto) > 0)
        //        {
        //            //Pro_qty * (Pro_precio_uni - Pro_bonificacion)
        //            decimal _bonif = (Convert.ToDecimal(_dt.dto) * Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture)) / 100;
        //            decimal _totBonif = _bonif * Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture);
        //            _dt.dto = Math.Round(_totBonif,2).ToString().Replace(',', '.');
        //        }
        //        //Vemos el dto comercial
        //        if(_datosFcExpo.porcentajeDtoComercial > 0)
        //        {
        //            decimal _precioComercial = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
        //            decimal _bonifComercial = _precioComercial * _datosFcExpo.porcentajeDtoComercial / 100;
        //            decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
        //            decimal _totalBonifComercial = _dto + _bonifComercial;
        //            _dt.dto = Math.Round(_totalBonifComercial,2).ToString().Replace(',', '.');
        //        }
        //        //Vemos el dto PP
        //        if(_datosFcExpo.porcentajeDtoPp > 0)
        //        {
        //            decimal _precioPp = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
        //            decimal _bonifPp = _precioPp * _datosFcExpo.porcentajeDtoPp / 100;
        //            decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
        //            decimal _totalBonifComercial = _dto + _bonifPp;
        //            _dt.dto = Math.Round(_totalBonifComercial,2).ToString().Replace(',', '.');
        //        }
        //        decimal _importeTotal = Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture) * (Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture));
        //        _dt.importe = Math.Round( _importeTotal,2).ToString().Replace(',', '.');
                
        //        _det.Add(_dt);
        //    }

        //    return _det;
        //}
    }
}
