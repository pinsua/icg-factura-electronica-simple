﻿namespace IcgPlugInFacturacion
{
    partial class frmFcExportacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDatosFactura = new System.Windows.Forms.GroupBox();
            this.cboIncoterms = new System.Windows.Forms.ComboBox();
            this.lblIncoterms = new System.Windows.Forms.Label();
            this.txtCotizacion = new System.Windows.Forms.TextBox();
            this.cboPaises = new System.Windows.Forms.ComboBox();
            this.cboTiposExportacion = new System.Windows.Forms.ComboBox();
            this.cboMoneda = new System.Windows.Forms.ComboBox();
            this.txtImporteTotal = new System.Windows.Forms.TextBox();
            this.txtNroComprobante = new System.Windows.Forms.TextBox();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.txtTipoComprobante = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.lblImporteTotal = new System.Windows.Forms.Label();
            this.lblMonedCotiz = new System.Windows.Forms.Label();
            this.lblMoneda = new System.Windows.Forms.Label();
            this.lblDest_comp = new System.Windows.Forms.Label();
            this.lblTipoExpo = new System.Windows.Forms.Label();
            this.lblNroCpte = new System.Windows.Forms.Label();
            this.lblPtoVta = new System.Windows.Forms.Label();
            this.lblTipoComprobante = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.gbCliente = new System.Windows.Forms.GroupBox();
            this.txtDomicilio = new System.Windows.Forms.TextBox();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.lblDomicilio = new System.Windows.Forms.Label();
            this.lblCuitCliente = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.gbFinal = new System.Windows.Forms.GroupBox();
            this.btnCae = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.gbItems = new System.Windows.Forms.GroupBox();
            this.dgvItems = new System.Windows.Forms.DataGridView();
            this.gbDatosFactura.SuspendLayout();
            this.gbCliente.SuspendLayout();
            this.gbFinal.SuspendLayout();
            this.gbItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // gbDatosFactura
            // 
            this.gbDatosFactura.Controls.Add(this.cboIncoterms);
            this.gbDatosFactura.Controls.Add(this.lblIncoterms);
            this.gbDatosFactura.Controls.Add(this.txtCotizacion);
            this.gbDatosFactura.Controls.Add(this.cboPaises);
            this.gbDatosFactura.Controls.Add(this.cboTiposExportacion);
            this.gbDatosFactura.Controls.Add(this.cboMoneda);
            this.gbDatosFactura.Controls.Add(this.txtImporteTotal);
            this.gbDatosFactura.Controls.Add(this.txtNroComprobante);
            this.gbDatosFactura.Controls.Add(this.txtPtoVta);
            this.gbDatosFactura.Controls.Add(this.txtTipoComprobante);
            this.gbDatosFactura.Controls.Add(this.txtFecha);
            this.gbDatosFactura.Controls.Add(this.lblImporteTotal);
            this.gbDatosFactura.Controls.Add(this.lblMonedCotiz);
            this.gbDatosFactura.Controls.Add(this.lblMoneda);
            this.gbDatosFactura.Controls.Add(this.lblDest_comp);
            this.gbDatosFactura.Controls.Add(this.lblTipoExpo);
            this.gbDatosFactura.Controls.Add(this.lblNroCpte);
            this.gbDatosFactura.Controls.Add(this.lblPtoVta);
            this.gbDatosFactura.Controls.Add(this.lblTipoComprobante);
            this.gbDatosFactura.Controls.Add(this.lblFecha);
            this.gbDatosFactura.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbDatosFactura.Location = new System.Drawing.Point(0, 0);
            this.gbDatosFactura.Name = "gbDatosFactura";
            this.gbDatosFactura.Size = new System.Drawing.Size(858, 146);
            this.gbDatosFactura.TabIndex = 0;
            this.gbDatosFactura.TabStop = false;
            this.gbDatosFactura.Text = "Datos Factura";
            // 
            // cboIncoterms
            // 
            this.cboIncoterms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIncoterms.FormattingEnabled = true;
            this.cboIncoterms.Location = new System.Drawing.Point(128, 114);
            this.cboIncoterms.Name = "cboIncoterms";
            this.cboIncoterms.Size = new System.Drawing.Size(228, 21);
            this.cboIncoterms.TabIndex = 19;
            // 
            // lblIncoterms
            // 
            this.lblIncoterms.AutoSize = true;
            this.lblIncoterms.Location = new System.Drawing.Point(18, 119);
            this.lblIncoterms.Name = "lblIncoterms";
            this.lblIncoterms.Size = new System.Drawing.Size(53, 13);
            this.lblIncoterms.TabIndex = 18;
            this.lblIncoterms.Text = "Incoterms";
            // 
            // txtCotizacion
            // 
            this.txtCotizacion.Location = new System.Drawing.Point(371, 86);
            this.txtCotizacion.Name = "txtCotizacion";
            this.txtCotizacion.Size = new System.Drawing.Size(171, 20);
            this.txtCotizacion.TabIndex = 17;
            // 
            // cboPaises
            // 
            this.cboPaises.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaises.FormattingEnabled = true;
            this.cboPaises.Location = new System.Drawing.Point(659, 52);
            this.cboPaises.Name = "cboPaises";
            this.cboPaises.Size = new System.Drawing.Size(154, 21);
            this.cboPaises.TabIndex = 16;
            // 
            // cboTiposExportacion
            // 
            this.cboTiposExportacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTiposExportacion.FormattingEnabled = true;
            this.cboTiposExportacion.Location = new System.Drawing.Point(371, 52);
            this.cboTiposExportacion.Name = "cboTiposExportacion";
            this.cboTiposExportacion.Size = new System.Drawing.Size(171, 21);
            this.cboTiposExportacion.TabIndex = 15;
            // 
            // cboMoneda
            // 
            this.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMoneda.FormattingEnabled = true;
            this.cboMoneda.Location = new System.Drawing.Point(128, 85);
            this.cboMoneda.Name = "cboMoneda";
            this.cboMoneda.Size = new System.Drawing.Size(121, 21);
            this.cboMoneda.TabIndex = 14;
            // 
            // txtImporteTotal
            // 
            this.txtImporteTotal.Location = new System.Drawing.Point(659, 86);
            this.txtImporteTotal.Name = "txtImporteTotal";
            this.txtImporteTotal.ReadOnly = true;
            this.txtImporteTotal.Size = new System.Drawing.Size(154, 20);
            this.txtImporteTotal.TabIndex = 13;
            // 
            // txtNroComprobante
            // 
            this.txtNroComprobante.Location = new System.Drawing.Point(128, 53);
            this.txtNroComprobante.Name = "txtNroComprobante";
            this.txtNroComprobante.ReadOnly = true;
            this.txtNroComprobante.Size = new System.Drawing.Size(121, 20);
            this.txtNroComprobante.TabIndex = 12;
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Location = new System.Drawing.Point(659, 24);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.ReadOnly = true;
            this.txtPtoVta.Size = new System.Drawing.Size(154, 20);
            this.txtPtoVta.TabIndex = 11;
            // 
            // txtTipoComprobante
            // 
            this.txtTipoComprobante.Location = new System.Drawing.Point(371, 24);
            this.txtTipoComprobante.Name = "txtTipoComprobante";
            this.txtTipoComprobante.ReadOnly = true;
            this.txtTipoComprobante.Size = new System.Drawing.Size(171, 20);
            this.txtTipoComprobante.TabIndex = 10;
            // 
            // txtFecha
            // 
            this.txtFecha.Location = new System.Drawing.Point(128, 24);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.ReadOnly = true;
            this.txtFecha.Size = new System.Drawing.Size(121, 20);
            this.txtFecha.TabIndex = 9;
            // 
            // lblImporteTotal
            // 
            this.lblImporteTotal.AutoSize = true;
            this.lblImporteTotal.Location = new System.Drawing.Point(562, 89);
            this.lblImporteTotal.Name = "lblImporteTotal";
            this.lblImporteTotal.Size = new System.Drawing.Size(69, 13);
            this.lblImporteTotal.TabIndex = 8;
            this.lblImporteTotal.Text = "Importe Total";
            // 
            // lblMonedCotiz
            // 
            this.lblMonedCotiz.AutoSize = true;
            this.lblMonedCotiz.Location = new System.Drawing.Point(262, 89);
            this.lblMonedCotiz.Name = "lblMonedCotiz";
            this.lblMonedCotiz.Size = new System.Drawing.Size(98, 13);
            this.lblMonedCotiz.TabIndex = 7;
            this.lblMonedCotiz.Text = "Cotizacion Moneda";
            // 
            // lblMoneda
            // 
            this.lblMoneda.AutoSize = true;
            this.lblMoneda.Location = new System.Drawing.Point(18, 89);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(46, 13);
            this.lblMoneda.TabIndex = 6;
            this.lblMoneda.Text = "Moneda";
            // 
            // lblDest_comp
            // 
            this.lblDest_comp.AutoSize = true;
            this.lblDest_comp.Location = new System.Drawing.Point(562, 56);
            this.lblDest_comp.Name = "lblDest_comp";
            this.lblDest_comp.Size = new System.Drawing.Size(43, 13);
            this.lblDest_comp.TabIndex = 5;
            this.lblDest_comp.Text = "Destino";
            // 
            // lblTipoExpo
            // 
            this.lblTipoExpo.AutoSize = true;
            this.lblTipoExpo.Location = new System.Drawing.Point(262, 56);
            this.lblTipoExpo.Name = "lblTipoExpo";
            this.lblTipoExpo.Size = new System.Drawing.Size(87, 13);
            this.lblTipoExpo.TabIndex = 4;
            this.lblTipoExpo.Text = "Tipo Exportación";
            // 
            // lblNroCpte
            // 
            this.lblNroCpte.AutoSize = true;
            this.lblNroCpte.Location = new System.Drawing.Point(18, 56);
            this.lblNroCpte.Name = "lblNroCpte";
            this.lblNroCpte.Size = new System.Drawing.Size(90, 13);
            this.lblNroCpte.TabIndex = 3;
            this.lblNroCpte.Text = "Nro Comprobante";
            // 
            // lblPtoVta
            // 
            this.lblPtoVta.AutoSize = true;
            this.lblPtoVta.Location = new System.Drawing.Point(562, 27);
            this.lblPtoVta.Name = "lblPtoVta";
            this.lblPtoVta.Size = new System.Drawing.Size(81, 13);
            this.lblPtoVta.TabIndex = 2;
            this.lblPtoVta.Text = "Punto de Venta";
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.AutoSize = true;
            this.lblTipoComprobante.Location = new System.Drawing.Point(262, 27);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(94, 13);
            this.lblTipoComprobante.TabIndex = 1;
            this.lblTipoComprobante.Text = "Tipo Comprobante";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(18, 27);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(37, 13);
            this.lblFecha.TabIndex = 0;
            this.lblFecha.Text = "Fecha";
            // 
            // gbCliente
            // 
            this.gbCliente.Controls.Add(this.txtDomicilio);
            this.gbCliente.Controls.Add(this.txtCuit);
            this.gbCliente.Controls.Add(this.txtCliente);
            this.gbCliente.Controls.Add(this.lblDomicilio);
            this.gbCliente.Controls.Add(this.lblCuitCliente);
            this.gbCliente.Controls.Add(this.lblCliente);
            this.gbCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbCliente.Location = new System.Drawing.Point(0, 146);
            this.gbCliente.Name = "gbCliente";
            this.gbCliente.Size = new System.Drawing.Size(858, 100);
            this.gbCliente.TabIndex = 1;
            this.gbCliente.TabStop = false;
            this.gbCliente.Text = "Datos Cliente";
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.Location = new System.Drawing.Point(128, 72);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.ReadOnly = true;
            this.txtDomicilio.Size = new System.Drawing.Size(604, 20);
            this.txtDomicilio.TabIndex = 5;
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(128, 46);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.ReadOnly = true;
            this.txtCuit.Size = new System.Drawing.Size(121, 20);
            this.txtCuit.TabIndex = 4;
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(128, 19);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.ReadOnly = true;
            this.txtCliente.Size = new System.Drawing.Size(364, 20);
            this.txtCliente.TabIndex = 3;
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.AutoSize = true;
            this.lblDomicilio.Location = new System.Drawing.Point(18, 75);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(49, 13);
            this.lblDomicilio.TabIndex = 2;
            this.lblDomicilio.Text = "Domicilio";
            // 
            // lblCuitCliente
            // 
            this.lblCuitCliente.AutoSize = true;
            this.lblCuitCliente.Location = new System.Drawing.Point(18, 49);
            this.lblCuitCliente.Name = "lblCuitCliente";
            this.lblCuitCliente.Size = new System.Drawing.Size(32, 13);
            this.lblCuitCliente.TabIndex = 1;
            this.lblCuitCliente.Text = "CUIT";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Location = new System.Drawing.Point(18, 26);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(39, 13);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.Text = "Cliente";
            // 
            // gbFinal
            // 
            this.gbFinal.Controls.Add(this.btnCae);
            this.gbFinal.Controls.Add(this.btnSalir);
            this.gbFinal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbFinal.Location = new System.Drawing.Point(0, 486);
            this.gbFinal.Name = "gbFinal";
            this.gbFinal.Size = new System.Drawing.Size(858, 66);
            this.gbFinal.TabIndex = 2;
            this.gbFinal.TabStop = false;
            // 
            // btnCae
            // 
            this.btnCae.Location = new System.Drawing.Point(647, 26);
            this.btnCae.Name = "btnCae";
            this.btnCae.Size = new System.Drawing.Size(75, 23);
            this.btnCae.TabIndex = 1;
            this.btnCae.Text = "Buscar CAE";
            this.btnCae.UseVisualStyleBackColor = true;
            this.btnCae.Click += new System.EventHandler(this.btnCae_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(756, 26);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gbItems
            // 
            this.gbItems.Controls.Add(this.dgvItems);
            this.gbItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbItems.Location = new System.Drawing.Point(0, 246);
            this.gbItems.Name = "gbItems";
            this.gbItems.Size = new System.Drawing.Size(858, 240);
            this.gbItems.TabIndex = 3;
            this.gbItems.TabStop = false;
            this.gbItems.Text = "Items Factura";
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.AllowUserToDeleteRows = false;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItems.Location = new System.Drawing.Point(3, 16);
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.ReadOnly = true;
            this.dgvItems.Size = new System.Drawing.Size(852, 221);
            this.dgvItems.TabIndex = 0;
            // 
            // frmFcExportacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 552);
            this.Controls.Add(this.gbItems);
            this.Controls.Add(this.gbFinal);
            this.Controls.Add(this.gbCliente);
            this.Controls.Add(this.gbDatosFactura);
            this.Name = "frmFcExportacion";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmFcExportacion";
            this.Load += new System.EventHandler(this.frmFcExportacion_Load);
            this.gbDatosFactura.ResumeLayout(false);
            this.gbDatosFactura.PerformLayout();
            this.gbCliente.ResumeLayout(false);
            this.gbCliente.PerformLayout();
            this.gbFinal.ResumeLayout(false);
            this.gbItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatosFactura;
        private System.Windows.Forms.TextBox txtNroComprobante;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.TextBox txtTipoComprobante;
        private System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.Label lblImporteTotal;
        private System.Windows.Forms.Label lblMonedCotiz;
        private System.Windows.Forms.Label lblMoneda;
        private System.Windows.Forms.Label lblDest_comp;
        private System.Windows.Forms.Label lblTipoExpo;
        private System.Windows.Forms.Label lblNroCpte;
        private System.Windows.Forms.Label lblPtoVta;
        private System.Windows.Forms.Label lblTipoComprobante;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.TextBox txtImporteTotal;
        private System.Windows.Forms.ComboBox cboMoneda;
        private System.Windows.Forms.ComboBox cboTiposExportacion;
        private System.Windows.Forms.ComboBox cboPaises;
        private System.Windows.Forms.TextBox txtCotizacion;
        private System.Windows.Forms.GroupBox gbCliente;
        private System.Windows.Forms.TextBox txtDomicilio;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label lblDomicilio;
        private System.Windows.Forms.Label lblCuitCliente;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.GroupBox gbFinal;
        private System.Windows.Forms.GroupBox gbItems;
        private System.Windows.Forms.DataGridView dgvItems;
        private System.Windows.Forms.Button btnCae;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ComboBox cboIncoterms;
        private System.Windows.Forms.Label lblIncoterms;
    }
}