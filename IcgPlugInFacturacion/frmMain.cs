﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security;
using AfipDll;
using System.Data.SqlClient;
using Janus.Windows.GridEX;
using static IcgFceDll.RetailService;

namespace IcgPlugInFacturacion
{
    public partial class frmMain : Form
    {
        List<DataAccess.TiposDoc> lstTipoDoc = new List<DataAccess.TiposDoc>();
        List<DataAccess.Clientes> lstClientes = new List<DataAccess.Clientes>();
        int intCodigoIVA;
        int intCodigoIIBB;
        public static string _nombreCertificado;
        public static string strConnectionString;
        public static string strCUIT;
        public static bool _EsTest = true;
        public static string _pathApp;
        public static string _pathLog;
        public static string _pathCerificado;
        public static string _pathTaFC;
        public static string _pathTaFCE;
        public static string _appName;
        public static int _codigoEmpresa;
        public static string _razonSocial;
        public static string _cbu;
        public static string _password;
        public static bool _tieneComputoIva;
        public static bool _Mtxca;
        public static bool _generaXML;
        public static string _tipoFcMiPyme;

        #region Formulario
        public frmMain()
        {
            InitializeComponent();

            intCodigoIVA = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["CodigoIVA"]);
            intCodigoIIBB = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["CodigoIIBB"]);
            strConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["IcgConnection"].ConnectionString;
            _tieneComputoIva = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["TieneComputoIVA"]);
            _Mtxca = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MTXCA"]);
            _EsTest = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsTest"]);
            _generaXML = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["GeneraXml"]);
            _tipoFcMiPyme = System.Configuration.ConfigurationManager.AppSettings["TipoFcMiPyme"].ToString();
            _appName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            _password = "pk.2639";
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSearchTipoDoc frm = new frmSearchTipoDoc();
            frm.ShowDialog(this);
            lstTipoDoc = frm.TiposDocumentos;
            frm.Dispose();
            lsTipoDoc.DataSource = lstTipoDoc;
        }

        private void btnSearchClientes_Click(object sender, EventArgs e)
        {
            frmSearchClientes frm = new frmSearchClientes();
            frm.ShowDialog(this);
            lstClientes = frm._Seleccion;
            frm.Dispose();
            lsClientes.DataSource = lstClientes;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //Cargo los datos.
            //if (cbEmpresas.Items.Count == 1)
            //    _codigoEmpresa = 0;
            LoadFacturas();
        }        

        private void frmMain_Load(object sender, EventArgs e)
        {
            DateTime fechaprimera = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime fechaSegunda = DateTime.Now;
            dtpDesde.Value = fechaprimera;
            dtpHasta.Value = fechaSegunda;

            string _valcamposlibres = ValidarCamposLibres.ValidarColumnasCamposLibres(strConnectionString);
            if (!String.IsNullOrEmpty(_valcamposlibres))
                MessageBox.Show(_valcamposlibres, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            LoadEmpresas();
        }

        private void ValidarCarpetas()
        {
            try
            {
                //recupero la ubicacion del ejecutable.
                _pathApp = Application.StartupPath.ToString();
                //Cargo la ubicaciones.
                _pathCerificado = _pathApp + "\\Certificado";
                _pathLog = _pathApp + "\\Log";
                _pathTaFC = _pathApp + "\\TAFC";
                _pathTaFCE = _pathApp + "\\TAFCE";

                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCerificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCerificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCerificado + "). Por favor coloque el certificado en este directorio para poder continuar.");
                }
                else
                {
                    if (!String.IsNullOrEmpty(_nombreCertificado))
                    {
                        if (!File.Exists(_pathCerificado + "\\" + _nombreCertificado))
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCerificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        else
                            _pathCerificado = _pathCerificado + "\\" + _nombreCertificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error: " +ex.Message +" validando los directorios.");
            }
        }

        private void btnBuscarCae_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            foreach (char c in _password) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            if (!String.IsNullOrEmpty(strCUIT))
            {
                if (File.Exists(_pathCerificado))
                {
                    this.Cursor = Cursors.WaitCursor;
                    foreach (Janus.Windows.GridEX.GridEXRow rw in grdGrid.GetCheckedRows())
                    {
                        try
                        {
                            if (rw.Cells["Estado"].Value.ToString() != "FACTURADO")
                            {
                                //Aca validar si la no esta ya facturada.
                                if (!DataAccess.Facturas.ValidoCAE(rw.Cells["N"].Value.ToString(), rw.Cells["NumSerie"].Value.ToString(),
                                    Convert.ToInt32(rw.Cells["NumFactura"].Value.ToString()), strConnectionString))
                                {
                                    //Recupero el codigo Afip para ver que tipo de documento es.
                                    string _codigoAfip = GetCodAfip(rw.Cells["DescTipoDoc"].Value.ToString());
                                    if (_codigoAfip == "019" || _codigoAfip == "020" || _codigoAfip == "021")
                                    {
                                        //Factura Electronica de Exportacion.
                                        //Recupero los valores.
                                        string _numSerie = rw.Cells["NumSerie"].Value.ToString();
                                        string _numFactura = rw.Cells["NumFactura"].Value.ToString();
                                        string _n = rw.Cells["N"].Value.ToString();
                                        string _ptoVta = rw.Cells["PuntoVenta"].Value.ToString();

                                        BuscarCaeDeExportacion(_numSerie, _numFactura, _n, _codigoAfip, _ptoVta, strPasswordSecureString, _generaXML);
                                    }
                                    else
                                    {
                                        if (_Mtxca)
                                        {
                                            BuscarCaeMTXCA(rw, strPasswordSecureString, _generaXML);
                                        }
                                        else
                                        {
                                            if (_codigoAfip == "201" || _codigoAfip == "203" || _codigoAfip == "202"
                                                || _codigoAfip == "206" || _codigoAfip == "208" || _codigoAfip == "207"
                                                || _codigoAfip == "211" || _codigoAfip == "213" || _codigoAfip == "212")
                                            {
                                                BuscarCaeMiPyme(rw, strPasswordSecureString);
                                            }
                                            else
                                            {
                                                //1, 2, 3, 4, 5, 34, 39, 60, 63
                                                if (!String.IsNullOrEmpty(rw.Cells["EXEPCION_COMPUTO_IVA"].Value.ToString()))
                                                {
                                                    int _cod = Convert.ToInt32(_codigoAfip);
                                                    int[] numbers = { 1, 2, 3, 4, 5, 34, 39, 60, 63 };
                                                    if (!numbers.Contains(_cod))
                                                        throw new Exception("Excepción Computo IVA solo se encuentra disponible para los comprobantes de tipo A (1, 2, 3, 4, 5, 34, 39, 60, 63)");
                                                }
                                                AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                                                //validamos que tenemos el TA.xml
                                                if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCerificado, _pathTaFC, _pathLog, _wsaa, strCUIT))
                                                    _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCerificado, _pathTaFC, _pathLog, _EsTest, strCUIT, strPasswordSecureString);
                                                if (_wsaa.Token != null)
                                                {
                                                    //Factura Electronica comun.
                                                    DataAccess.DatosFactura _Fc = new DataAccess.DatosFactura();
                                                    _Fc.NumSerie = rw.Cells["NumSerie"].Value.ToString();
                                                    _Fc.NumFactura = rw.Cells["NumFactura"].Value.ToString();
                                                    _Fc.N = rw.Cells["N"].Value.ToString();
                                                    _Fc.TotalBruto = rw.Cells["TotalBruto"].Value.ToString();
                                                    _Fc.TotalNeto = rw.Cells["TotalNeto"].Value.ToString();
                                                    _Fc.TotalCosteIva = rw.Cells["TotalImpuestos"].Value.ToString();
                                                    List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                                                    _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                                    List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, strConnectionString, _Fc.TotalBruto);
                                                    _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                                    List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                                                    _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                                    _Fc.cCodAfip = GetCodAfip(rw.Cells["DescTipoDoc"].Value.ToString());
                                                    _Fc.strTipoDoc = rw.Cells["DescTipoDoc"].Value.ToString();
                                                    _Fc.PtoVta = rw.Cells["PuntoVenta"].Value.ToString();
                                                    _Fc.ClienteTipoDoc = GetCodClienteDoc(rw.Cells["ClienteDoc"].Value.ToString());
                                                    _Fc.ClienteNroDoc = rw.Cells["ClienteNroDoc"].Value.ToString();
                                                    if (!String.IsNullOrEmpty(rw.Cells["EXEPCION_COMPUTO_IVA"].Value.ToString()))
                                                        _Fc.ExcepcionComputoIva = rw.Cells["EXEPCION_COMPUTO_IVA"].Value.ToString().Substring(0, 2);
                                                    else
                                                        _Fc.ExcepcionComputoIva = "";
                                                    DateTime dttFecha = Convert.ToDateTime(rw.Cells["Fecha"].Value);
                                                    _Fc.strFecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                                                    _Fc.NcRechazada = rw.Cells["NcRechazada"].Value.ToString();
                                                    //Recupero el estado de la Factura.
                                                    string estadoFc = DataAccess.Facturas.GetEstadoFC(_Fc.N, _Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _pathLog, strConnectionString);
                                                    switch (estadoFc.ToUpper())
                                                    {
                                                        case "PENDIENTE":
                                                        case "ERROR":
                                                            {
                                                                string _Error;
                                                                List<AfipDll.wsAfip.Errors> _lstError = new List<AfipDll.wsAfip.Errors>();
                                                                //Recupero el nro del ultimo comprobante.
                                                                AfipDll.wsAfip.UltimoComprobante _ultimoComprobante = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCerificado, _pathTaFC, _pathLog, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, _EsTest, strPasswordSecureString, out _lstError);
                                                                //Vemos si tenemos algun error
                                                                if (_lstError.Count() == 0)
                                                                {
                                                                    long _ultimoICG = 0;
                                                                    //if (IcgFceDll.RetailService.Common.ValidarUltimoNroComprobante(_ultimoComprobante.PtoVta.ToString(),
                                                                    //    _ultimoComprobante.CbteNro, _ultimoComprobante.CbteTipo.ToString().PadLeft(3, '0'), strConnectionString, out _ultimoICG))
                                                                    if(_ultimoICG == 0)
                                                                    {
                                                                        //incremento el nro de comprobante.
                                                                        int _NroComprobante = _ultimoComprobante.CbteNro + 1;
                                                                        //Armo el string de la cabecera.
                                                                        AfipDll.wsAfip.clsCabReq _cabecera = new AfipDll.wsAfip.clsCabReq();
                                                                        _cabecera.CantReg = 1;
                                                                        _cabecera.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                                                                        _cabecera.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                                                                        string strCabReg = "1|" + _Fc.cCodAfip + "|" + _Fc.PtoVta;
                                                                        //Cargamos la clase Detalle.
                                                                        AfipDll.wsAfip.clsDetReq _detalle = CargarDetalle3(_Fc,_cabecera.CbteTipo, _NroComprobante, _Iva);
                                                                        //Armo el string de IVA.
                                                                        List<AfipDll.wsAfip.clsIVA> _IvaRequest = ArmarIVA2(_Iva);
                                                                        //Armos el string de Tributos.
                                                                        List<AfipDll.wsAfip.clsTributos> _TributosRequest = ArmaTributos(_Tributos);
                                                                        //Armamos la lista de comprobantes Asociados.
                                                                        List<AfipDll.wsAfip.ComprobanteAsoc> _ComprobantesAsociados = ArmarCbteAsoc(_Fc);
                                                                        //Verifico que tengo por lo menos 1 comprobante asociado.
                                                                        if (Convert.ToInt32(_Fc.cCodAfip) == 2 || Convert.ToInt32(_Fc.cCodAfip) == 3
                                                                            || Convert.ToInt32(_Fc.cCodAfip) == 7 || Convert.ToInt32(_Fc.cCodAfip) == 8
                                                                            || Convert.ToInt32(_Fc.cCodAfip) == 12 || Convert.ToInt32(_Fc.cCodAfip) == 13)
                                                                        {
                                                                            if (_ComprobantesAsociados[0].Nro == 0)
                                                                            {
                                                                                frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_cabecera.CbteTipo, strCUIT, _Mtxca);
                                                                                _frm.ShowDialog(this);
                                                                                _ComprobantesAsociados = _frm._lstNewComun;
                                                                                _frm.Dispose();
                                                                                if (_ComprobantesAsociados.Count == 0)
                                                                                {
                                                                                    //Insertamos en FacturasCamposLibres
                                                                                    ModificoFacturasVentaCampoLibres(_Fc, null, "Error: No posee comprobante asociado", null, null, null);
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        //Armamos los Opcionales.
                                                                        List<AfipDll.wsAfip.Opcionales> _opcionales = new List<AfipDll.wsAfip.Opcionales>();
                                                                        if (!String.IsNullOrEmpty(_Fc.ExcepcionComputoIva))
                                                                        {
                                                                            AfipDll.wsAfip.Opcionales _opc = new AfipDll.wsAfip.Opcionales();
                                                                            _opc.Id = "5";
                                                                            _opc.Valor = _Fc.ExcepcionComputoIva;

                                                                            _opcionales.Add(_opc);
                                                                        }
                                                                        try
                                                                        {
                                                                            AfipDll.wsAfip.clsCaeResponse _Respuesta = AfipDll.wsAfip.ObtenerDatosCAE(_cabecera, _detalle, _TributosRequest,
                                                                                _IvaRequest, _opcionales, _ComprobantesAsociados, _pathCerificado, _pathTaFC, _pathLog, strCUIT, _EsTest, _wsaa, _cbu, _generaXML);
                                                                            //
                                                                            string strCodBarra = "";
                                                                            string qrEncode = "";
                                                                            switch (_Respuesta.Resultado)
                                                                            {
                                                                                case "A":
                                                                                    {
                                                                                        GenerarCodigoBarra(strCUIT, _cabecera.CbteTipo.ToString(), _cabecera.PtoVta.ToString(), _Respuesta.Cae, _Respuesta.FechaVto, out strCodBarra);
                                                                                        //Genero el los datos del QR.
                                                                                        qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                                                            _cabecera.PtoVta, _cabecera.CbteTipo, Convert.ToInt32(_detalle.CbteDesde),
                                                                                            Convert.ToDecimal(_detalle.ImpTotal), _detalle.MonID, Convert.ToDecimal(_detalle.MonCotiz),
                                                                                            _detalle.DocTipo, Convert.ToInt64(_detalle.DocNro), "E", Convert.ToInt64(_Respuesta.Cae));
                                                                                        break;
                                                                                    }
                                                                                case "P":
                                                                                    {
                                                                                        GenerarCodigoBarra(strCUIT, _cabecera.CbteTipo.ToString(), _cabecera.PtoVta.ToString(), _Respuesta.Cae, _Respuesta.FechaVto, out strCodBarra);
                                                                                        //Genero el los datos del QR.
                                                                                        qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                                                            _cabecera.PtoVta, _cabecera.CbteTipo, Convert.ToInt32(_detalle.CbteDesde),
                                                                                            Convert.ToDecimal(_detalle.ImpTotal), _detalle.MonID, Convert.ToDecimal(_detalle.MonCotiz),
                                                                                            _detalle.DocTipo, Convert.ToInt64(_detalle.DocNro), "E", Convert.ToInt64(_Respuesta.Cae));
                                                                                        break;
                                                                                    }
                                                                                case "R":
                                                                                    {
                                                                                        break;
                                                                                    }
                                                                            }
                                                                            //Modificamos el registro en FacturasCamposLibres.
                                                                            ModificoFacturasVentaCampoLibres(_Fc, _Respuesta.Cae, _Respuesta.ErrorMsj, _Respuesta.FechaVto, strCodBarra, qrEncode);

                                                                            //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.                                            
                                                                            if (!String.IsNullOrEmpty(_Respuesta.Cae))
                                                                            {
                                                                                Insertar_FacturasVentaSerieResol(_Fc, _NroComprobante);
                                                                            }
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            //Recupero el nro del ultimo comprobante.
                                                                            int intUltimoNro = wsAfip.RecuperoUltimoComprobante(_pathCerificado, _pathTaFC, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, _EsTest, _pathLog, out _Error);
                                                                            //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                                                                            if (intUltimoNro == _NroComprobante)
                                                                            {
                                                                                //Guardamos el numero y ponemos el estado en Recuperar.
                                                                                //Modificamos el registro en FacturasCamposLibres.
                                                                                ModificoFacturasVentaCampoLibres(_Fc, _NroComprobante, ex.Message);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        MessageBox.Show("El último comprobante informado en AFIP, no corresponde con el último de ICG" +
                                                                            Environment.NewLine + "Ultimo Nro AFIP: " + _ultimoComprobante.CbteNro.ToString() +
                                                                            Environment.NewLine + "Ultimo Nro ICG: " + _ultimoICG.ToString() +
                                                                            Environment.NewLine + "Por favor Verifique que el comprobante no este anulado",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string _error = "";
                                                                    foreach (AfipDll.wsAfip.Errors er in _lstError)
                                                                    {
                                                                        _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                                                                    }
                                                                    //Error al recuperar el ultimo numero de factura de la AFIP
                                                                    ModificoFacturasVentaCampoLibres(_Fc, "", _error, "", "", "");
                                                                }
                                                                break;
                                                            }
                                                        case "RECUPERAR":
                                                            {
                                                                //Recupero el nro grabado como pendiente.
                                                                DataAccess.FacturasVentaCamposLibres _fvcl = DataAccess.FacturasVentaCamposLibres.GetRecord(strConnectionString, _Fc);
                                                                if (String.IsNullOrEmpty(_fvcl.CodBarras))
                                                                    _fvcl.CodBarras = "0";
                                                                if (Convert.ToInt32(_fvcl.CodBarras) >= 0)
                                                                {
                                                                    //Recuperamos el CAE.
                                                                    wsAfip.clsCaeRecupero _rec = wsAfip.ConsultaComprobante(_pathCerificado, _pathTaFC, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, Convert.ToInt32(_fvcl.CodBarras), _EsTest, _pathLog);
                                                                    //Analizamos la respuesta.
                                                                    switch (_rec.Resultado.ToUpper())
                                                                    {
                                                                        case "A":
                                                                            {
                                                                                string strCodBarra;
                                                                                GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _rec.Cae, _rec.FechaVto, out strCodBarra);
                                                                                //Genero el los datos del QR.
                                                                                string qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                                                    Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), Convert.ToInt32(_rec.CbteDesde),
                                                                                    Convert.ToDecimal(_rec.ImporteTotal), _rec.MonId, Convert.ToDecimal(_rec.MonCtz),
                                                                                    _rec.DocTipo, Convert.ToInt64(_rec.DocNro), "E", Convert.ToInt64(_rec.Cae));
                                                                                //Modificamos el registro en FacturasCamposLibres.
                                                                                ModificoFacturasVentaCampoLibres(_Fc, _rec.Cae, _rec.ErrorMsj, _rec.FechaVto, strCodBarra, qrEncode);
                                                                                //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.
                                                                                if (!String.IsNullOrEmpty(_rec.Cae))
                                                                                {
                                                                                    Insertar_FacturasVentaSerieResol(_Fc, Convert.ToInt32(_rec.CbteDesde));
                                                                                }

                                                                                break;
                                                                            }
                                                                        case "R":
                                                                            {
                                                                                if (_rec.ErrorMsj == "No existen datos en nuestros registros para los parametros ingresados."
                                                                                    || _rec.ErrorMsj == "Campo CbteNro no lo ingreso o supera el tamaño permitido de 8 caracteres numericos.")
                                                                                {
                                                                                    //Modificamos el registro en FacturasCamposLibres.
                                                                                    ModificoFacturasVentaCampoLibres(_Fc, "", "", "", "", "");
                                                                                }
                                                                                break;
                                                                            }
                                                                    }
                                                                }
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                break;
                                                            }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //ModificoFacturasVentaCampoLibres(_Fc, intNroComprobante, "Ya posee CAE");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Cursor = Cursors.Default;
                            MessageBox.Show("Se produjo el siguiente error: " + ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No Existe el certificado por favor coloquelo en " + _pathCerificado + " para continuar.");
                }
            }
            else
            {
                MessageBox.Show("Por favor ingrese el CUIT de la Empresa de Facturación.");
            }
            //Vuelvo a cargar los datos.
            LoadFacturas();
            this.Cursor = Cursors.Default;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (grdGrid.GetRow().RowType == Janus.Windows.GridEX.RowType.Record)
            {
                if (grdGrid.GetRow().Cells[16].Value.ToString() == "Error")
                {
                    string strError = grdGrid.GetRow().Cells[15].Value.ToString();
                    toolTip1.SetToolTip(grdGrid, strError);
                }
            }
        }
        #endregion

        #region MiPyme
        private void BuscarCaeMiPyme(GridEXRow rw, SecureString _secureString)
        {
            try
            {                
                int _ptoVta = Convert.ToInt32(rw.Cells["PuntoVenta"].Value);
                string _codAfip = GetCodAfip(rw.Cells["DescTipoDoc"].Value.ToString());

                //Factura Electronica comun.
                DataAccess.DatosFactura _Fc = new DataAccess.DatosFactura();
                _Fc.NumSerie = rw.Cells["NumSerie"].Value.ToString();
                _Fc.NumFactura = rw.Cells["NumFactura"].Value.ToString();
                _Fc.N = rw.Cells["N"].Value.ToString();
                _Fc.TotalBruto = rw.Cells["TotalBruto"].Value.ToString();
                _Fc.TotalNeto = rw.Cells["TotalNeto"].Value.ToString();
                _Fc.TotalCosteIva = rw.Cells["TotalImpuestos"].Value.ToString();
                List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, strConnectionString, _Fc.TotalBruto);
                _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                _Fc.cCodAfip = _codAfip;
                _Fc.strTipoDoc = rw.Cells["DescTipoDoc"].Value.ToString();
                _Fc.PtoVta = rw.Cells["PuntoVenta"].Value.ToString();
                _Fc.ClienteTipoDoc = GetCodClienteDoc(rw.Cells["ClienteDoc"].Value.ToString());
                _Fc.ClienteNroDoc = rw.Cells["ClienteNroDoc"].Value.ToString();
                DateTime dttFecha = Convert.ToDateTime(rw.Cells["Fecha"].Value);
                _Fc.strFecha = dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0') + dttFecha.Day.ToString().PadLeft(2, '0');
                _Fc.NcRechazada = rw.Cells["NcRechazada"].Value.ToString();

                switch (rw.Cells["Estado"].Value.ToString().ToUpper())
                {
                    case "PENDIENTE":
                    case "ERROR":
                        {
                            string _Error;
                            List<AfipDll.wsAfip.Errors> _lstError = new List<AfipDll.wsAfip.Errors>();
                            //Recupero el nro del ultimo comprobante.
                            AfipDll.wsAfip.UltimoComprobante _ultimoComprobante = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCerificado, 
                                _pathTaFC, _pathLog, _ptoVta, Convert.ToInt32(_codAfip), strCUIT, _EsTest, _secureString, out _lstError);

                            //Vemos si tenemos algun error
                            if (_lstError.Count() == 0)
                            {
                                //incremento el nro de comprobante.
                                int _NroComprobante = _ultimoComprobante.CbteNro + 1;

                                //Armo el string de IVA.
                                List<AfipDll.wsAfip.clsIVA> _IvaRequest = ArmarIVA2(_Iva);
                                //Parseo a la clase de AFIP.
                                List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();
                                foreach (AfipDll.wsAfip.clsIVA _iva in _IvaRequest)
                                {
                                    AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                                    _al.BaseImp = _iva.BaseImp;
                                    _al.Id = _iva.ID;
                                    _al.Importe = _iva.Importe;
                                    _alicIVA.Add(_al);
                                }
                                
                                //Opcionales
                                List<AfipDll.wsAfipCae.Opcional> _opcional = new List<AfipDll.wsAfipCae.Opcional>();
                                //Si es Factura ponemos el CBU
                                if (_codAfip == "201" || _codAfip == "206" || _codAfip == "211")
                                {
                                    //Tipo Fc MiPyme
                                    AfipDll.wsAfipCae.Opcional _opFc1 = new AfipDll.wsAfipCae.Opcional();
                                    _opFc1.Id = "27";
                                    _opFc1.Valor = _tipoFcMiPyme;
                                    _opcional.Add(_opFc1);
                                    //
                                    AfipDll.wsAfipCae.Opcional _opFc = new AfipDll.wsAfipCae.Opcional();
                                    _opFc.Id = "2101";
                                    _opFc.Valor = _cbu;
                                    _opcional.Add(_opFc);
                                }

                                //Notas de credito MiPyme.
                                List<AfipDll.wsAfipCae.CbteAsoc> _lstCA = new List<AfipDll.wsAfipCae.CbteAsoc>();
                                if (_codAfip == "203" || _codAfip == "208" || _codAfip == "213"
                                    || _codAfip == "202" || _codAfip == "207" || _codAfip == "212")
                                {
                                    //Recupero los Comprobantes Asociados.
                                    List<AfipDll.wsAfip.ComprobanteAsoc> _ComprobantesAsociados = ArmarCbteAsoc(_Fc);
                                    //Verifico que tengo por lo menos 1 comprobante asociado.
                                    if (_ComprobantesAsociados[0].Nro == 0)
                                    {
                                        frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(Convert.ToInt32(_codAfip), strCUIT, _Mtxca);
                                        _frm.ShowDialog(this);
                                        _ComprobantesAsociados = _frm._lstNewComun;
                                        _frm.Dispose();
                                        if (_ComprobantesAsociados.Count == 0)
                                        {
                                            //Insertamos en FacturasCamposLibres
                                            ModificoFacturasVentaCampoLibres(_Fc, null, "Error: No posee comprobante asociado", null, null, null);
                                            break;
                                        }
                                    }
                                    //Recorro los comprobantes.
                                    foreach (AfipDll.wsAfip.ComprobanteAsoc _ca in _ComprobantesAsociados)
                                    {
                                        AfipDll.wsAfipCae.CbteAsoc _cls = new AfipDll.wsAfipCae.CbteAsoc();
                                        _cls.CbteFch = _ca.CbteFch;
                                        _cls.Cuit = _ca.Cuit;
                                        _cls.Nro = _ca.Nro;
                                        _cls.PtoVta = _ca.PtoVta;
                                        _cls.Tipo = _ca.Tipo;
                                        _lstCA.Add(_cls);
                                    }
                                    //Cargo el opcional como rechazado para sacar la NC.
                                    AfipDll.wsAfipCae.Opcional _opNcRech = new AfipDll.wsAfipCae.Opcional();
                                    _opNcRech.Id = "22";
                                    if (_Fc.NcRechazada == "T")
                                        _opNcRech.Valor = "S";
                                    else
                                        _opNcRech.Valor = "N";
                                    _opcional.Add(_opNcRech);
                                }

                                //Tributos.
                                //Armos el string de Tributos.
                                List<AfipDll.wsAfip.clsTributos> _TributosRequest = ArmaTributos(_Tributos);
                                List<AfipDll.wsAfipCae.Tributo> _tributos = new List<AfipDll.wsAfipCae.Tributo>();
                                foreach(AfipDll.wsAfip.clsTributos _tr in _TributosRequest)
                                {
                                    AfipDll.wsAfipCae.Tributo _t = new AfipDll.wsAfipCae.Tributo();
                                    _t.Alic = _tr.Alic;
                                    _t.BaseImp = _tr.BaseImp;
                                    _t.Desc = _tr.Desc;
                                    _t.Id = Convert.ToInt16(_tr.ID);
                                    _t.Importe = _tr.Importe;
                                    _tributos.Add(_t);
                                }                              

                                //Armos el detalle
                                List<AfipDll.wsAfipCae.FECAEDetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEDetRequest>();
                                AfipDll.wsAfipCae.FECAEDetRequest _det = new AfipDll.wsAfipCae.FECAEDetRequest();
                                _det.CbteDesde = _NroComprobante;
                                _det.CbteFch = _Fc.strFecha; 
                                _det.CbteHasta = _NroComprobante;
                                _det.Concepto = 2;
                                _det.DocNro = Convert.ToInt64(_Fc.ClienteNroDoc.Replace("-", "")); 
                                _det.DocTipo = Convert.ToInt16(_Fc.ClienteTipoDoc); 
                                _det.FchServDesde = _Fc.strFecha; 
                                _det.FchServHasta = _Fc.strFecha;
                                DateTime _dtt = dttFecha.AddDays(10);
                                _det.FchVtoPago = GetFechaVto(_Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _Fc.N); // _dtt.Year.ToString() + _dtt.Month.ToString().PadLeft(2, '0') + _dtt.Day.ToString().PadLeft(2, '0');
                                //IVA;
                                if (_Fc.TotIva < 0)
                                    _det.ImpIVA = Convert.ToDouble(_Fc.TotIva) * -1;
                                else
                                    _det.ImpIVA = Convert.ToDouble(_Fc.TotIva);
                                //Neto
                                if (_Fc.TotalBruto.StartsWith("-"))
                                    _det.ImpNeto = Convert.ToDouble(_Fc.TotalBruto) * -1;
                                else
                                    _det.ImpNeto = Convert.ToDouble(_Fc.TotalBruto);
                                //Total
                                if (_Fc.TotalNeto.StartsWith("-"))
                                    _det.ImpTotal = Convert.ToDouble(_Fc.TotalNeto) * -1;
                                else
                                    _det.ImpTotal = Convert.ToDouble(_Fc.TotalNeto);
                                //Tributos;
                                if (_Fc.totTributos < 0)
                                    _det.ImpTrib = Convert.ToDouble(_Fc.totTributos) * -1;
                                else
                                    _det.ImpTrib = Convert.ToDouble(_Fc.totTributos);
                                _det.ImpOpEx = 0;                                
                                _det.ImpTotConc = 0;
                                //Array de IVA
                                _det.Iva = _alicIVA.ToArray();
                                //Array de Opcionales.
                                _det.Opcionales = _opcional.ToArray();
                                //Array de tributos
                                if(_det.ImpTrib > 0)
                                    _det.Tributos = _tributos.ToArray();
                                //Moneda
                                _det.MonCotiz = 1;
                                _det.MonId = "PES";
                                //Nota credito MiPyme                                
                                if (_codAfip == "203" || _codAfip == "208" || _codAfip == "213")
                                {
                                    _det.FchVtoPago = "";
                                    _det.CbtesAsoc = _lstCA.ToArray();
                                }
                                //
                                _detReq.Add(_det);

                                //Armo el string de la cabecera.
                                AfipDll.wsAfipCae.FECAECabRequest _cabReq = new AfipDll.wsAfipCae.FECAECabRequest();
                                _cabReq.CantReg = 1;
                                _cabReq.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);// 201;
                                _cabReq.PtoVta = Convert.ToInt16(_Fc.PtoVta);

                                //Armo el request
                                AfipDll.wsAfipCae.FECAERequest _req = new AfipDll.wsAfipCae.FECAERequest();
                                //Asigno la cabecera
                                _req.FeCabReq = _cabReq;
                                //Asigno el detalle.
                                _req.FeDetReq = _detReq.ToArray();
                                try
                                {
                                    //instanciamos la clase.
                                    AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                                    //validamos que tenemos el TA.xml
                                    if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCerificado, _pathTaFC, _pathLog, _wsaa, strCUIT))
                                        _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCerificado, _pathTaFC, _pathLog, _EsTest, strCUIT, _secureString);
                                    //invocamos el WS.
                                    AfipDll.wsAfip.clsCaeResponse _call = AfipDll.wsAfip.ObtenerDatosCAENew(_req, _pathCerificado, _pathTaFC, _pathLog, strCUIT,
                                        _EsTest, _secureString, _wsaa);

                                    if (!String.IsNullOrEmpty(_call.Cae))
                                    {
                                        MessageBox.Show("CAE: " + _call.Cae + Environment.NewLine +
                                            "Fecha Vto.: " + _call.FechaVto + Environment.NewLine +
                                            "Resultado: " + _call.Resultado);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Codigo de error: " + _call.ErrorCode + Environment.NewLine +
                                            "Descripcion: " + _call.ErrorMsj);
                                    }
                                    //
                                    string strCodBarra = "";
                                    string qrEncode = "";
                                    switch (_call.Resultado)
                                    {
                                        case "A":
                                            {
                                                GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _call.Cae, _call.FechaVto, out strCodBarra);
                                                //Genero el los datos del QR.
                                                qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                    _req.FeCabReq.PtoVta, _req.FeCabReq.CbteTipo, Convert.ToInt32(_req.FeDetReq[0].CbteDesde),
                                                    Convert.ToDecimal(_req.FeDetReq[0].ImpTotal), _req.FeDetReq[0].MonId, Convert.ToDecimal(_req.FeDetReq[0].MonCotiz),
                                                    _req.FeDetReq[0].DocTipo, Convert.ToInt64(_req.FeDetReq[0].DocNro), "E", Convert.ToInt64(_call.Cae));
                                                break;
                                            }
                                        case "P":
                                            {
                                                GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _call.Cae, _call.FechaVto, out strCodBarra);
                                                //Genero el los datos del QR.
                                                qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                    _req.FeCabReq.PtoVta, _req.FeCabReq.CbteTipo, Convert.ToInt32(_req.FeDetReq[0].CbteDesde),
                                                    Convert.ToDecimal(_req.FeDetReq[0].ImpTotal), _req.FeDetReq[0].MonId, Convert.ToDecimal(_req.FeDetReq[0].MonCotiz),
                                                    _req.FeDetReq[0].DocTipo, Convert.ToInt64(_req.FeDetReq[0].DocNro), "E", Convert.ToInt64(_call.Cae));
                                                break;
                                            }
                                        case "R":
                                            {
                                                break;
                                            }
                                    }
                                    //Modificamos el registro en FacturasCamposLibres.
                                    ModificoFacturasVentaCampoLibres(_Fc, _call.Cae, _call.ErrorMsj, _call.FechaVto, strCodBarra, qrEncode);

                                    //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.                                            
                                    if (!String.IsNullOrEmpty(_call.Cae))
                                    {
                                        Insertar_FacturasVentaSerieResol(_Fc, _NroComprobante);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //Recupero el nro del ultimo comprobante.
                                    int intUltimoNro = wsAfip.RecuperoUltimoComprobante(_pathCerificado, _pathTaFC, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, _EsTest, _pathLog, out _Error);
                                    //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                                    if (intUltimoNro == _NroComprobante)
                                    {
                                        //Guardamos el numero y ponemos el estado en Recuperar.
                                        //Modificamos el registro en FacturasCamposLibres.
                                        ModificoFacturasVentaCampoLibres(_Fc, _NroComprobante, ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                string _error = "";
                                foreach (AfipDll.wsAfip.Errors er in _lstError)
                                {
                                    _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                                }
                                //Error al recuperar el ultimo numero de factura de la AFIP
                                ModificoFacturasVentaCampoLibres(_Fc, "", _error, "", "", "");
                            }
                            break;
                        }
                    case "RECUPERAR":
                        {
                            //Recupero el nro grabado como pendiente.
                            DataAccess.FacturasVentaCamposLibres _fvcl = DataAccess.FacturasVentaCamposLibres.GetRecord(strConnectionString, _Fc);
                            if (String.IsNullOrEmpty(_fvcl.CodBarras))
                                _fvcl.CodBarras = "0";
                            if (Convert.ToInt32(_fvcl.CodBarras) >= 0)
                            {
                                //Recuperamos el CAE.
                                wsAfip.clsCaeRecupero _rec = wsAfip.ConsultaComprobante(_pathCerificado, _pathTaFC, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, Convert.ToInt32(_fvcl.CodBarras), _EsTest, _pathLog);
                                //Analizamos la respuesta.
                                switch (_rec.Resultado.ToUpper())
                                {
                                    case "A":
                                        {
                                            string strCodBarra;
                                            string qrEncode;
                                            GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _rec.Cae, _rec.FechaVto, out strCodBarra);
                                            //Genero el los datos del QR.
                                            qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                Convert.ToInt32(_Fc.PtoVta), _rec.CbtTipo, Convert.ToInt32(_rec.CbteDesde),
                                                Convert.ToDecimal(_rec.ImporteTotal), _rec.MonId, Convert.ToDecimal(_rec.MonCtz),
                                                _rec.DocTipo, Convert.ToInt64(_rec.DocNro), "E", Convert.ToInt64(_rec.Cae));
                                            //Modificamos el registro en FacturasCamposLibres.
                                            ModificoFacturasVentaCampoLibres(_Fc, _rec.Cae, _rec.ErrorMsj, _rec.FechaVto, strCodBarra, qrEncode);
                                            //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.
                                            if (!String.IsNullOrEmpty(_rec.Cae))
                                            {
                                                Insertar_FacturasVentaSerieResol(_Fc, Convert.ToInt32(_fvcl.CodBarras.Trim()));
                                            }

                                            break;
                                        }
                                    case "R":
                                        {
                                            if (_rec.ErrorMsj == "No existen datos en nuestros registros para los parametros ingresados."
                                                || _rec.ErrorMsj == "Campo CbteNro no lo ingreso o supera el tamaño permitido de 8 caracteres numericos.")
                                            {
                                                //Modificamos el registro en FacturasCamposLibres.
                                                ModificoFacturasVentaCampoLibres(_Fc, "", "", "", "", "");
                                            }
                                            break;
                                        }
                                }
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private string GetFechaVto(string _serie, int _numero, string _n)
        {
            string _sql = "select max(fechavencimiento) as FECHA from tesoreria where serie = @serie and numero = @numero and N = @n";
            string _fecha = "";
            using (SqlConnection _conection = new SqlConnection(strConnectionString))
            {
                _conection.Open();
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = reader["FECHA"] == DBNull.Value ? DateTime.Now.AddDays(10) : Convert.ToDateTime(reader["FECHA"]);
                                    if (_fech < DateTime.Now)
                                        DateTime.Now.AddDays(10);
                                    _fecha = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        _fecha = DateTime.Now.AddDays(10).Year.ToString() + DateTime.Now.AddDays(10).Month.ToString().PadLeft(2, '0') + DateTime.Now.AddDays(10).Day.ToString().PadLeft(2, '0');
                    }
                }
            }
            return _fecha;
        }
        #endregion

        #region MTXCA
        private void BuscarCaeMTXCA(GridEXRow rw, SecureString _secureString, bool _generaXML)
        {            
            try
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Comenzamos MTXCA.");
                int _ptoVta = Convert.ToInt32(rw.Cells["PuntoVenta"].Value);
                string _codAfip = GetCodAfip(rw.Cells["DescTipoDoc"].Value.ToString());

                //Factura Electronica comun.
                DataAccess.DatosFactura _Fc = new DataAccess.DatosFactura();
                _Fc.NumSerie = rw.Cells["NumSerie"].Value.ToString();
                _Fc.NumFactura = rw.Cells["NumFactura"].Value.ToString();
                _Fc.N = rw.Cells["N"].Value.ToString();
                _Fc.TotalBruto = rw.Cells["TotalBruto"].Value.ToString();
                _Fc.TotalNeto = rw.Cells["TotalNeto"].Value.ToString();
                _Fc.TotalCosteIva = rw.Cells["TotalImpuestos"].Value.ToString();
                List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                //Recupero los tributos.
                List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, strConnectionString, _Fc.TotalBruto);
                //obtengo el total de los tributos.
                _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                //Recupero los Impuestos Internos parte de los tributos
                List<DataAccess.FacturasVentasTot> _ImpInternos = DataAccess.FacturasVentasTot.GetImpuestosInternos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, strConnectionString);
                //sumo los imp Internos a los tributos.
                _Fc.totTributos = _Fc.totTributos + _ImpInternos.Sum(i => i.TotIva);
                List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, strConnectionString);
                _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                _Fc.cCodAfip = _codAfip;
                _Fc.strTipoDoc = rw.Cells["DescTipoDoc"].Value.ToString();
                _Fc.PtoVta = rw.Cells["PuntoVenta"].Value.ToString();
                _Fc.ClienteTipoDoc = GetCodClienteDoc(rw.Cells["ClienteDoc"].Value.ToString());
                _Fc.ClienteNroDoc = rw.Cells["ClienteNroDoc"].Value.ToString().Replace("-","");
                DateTime dttFecha = Convert.ToDateTime(rw.Cells["Fecha"].Value);
                _Fc.strFecha = dttFecha.ToShortDateString();
                _Fc.NcRechazada = rw.Cells["NcRechazada"].Value.ToString();

                AfipDll.WsMTXCA.Comprobante _comproFc = new WsMTXCA.Comprobante();
                _comproFc.cbu = _cbu;
                _comproFc.codigoMoneda = "PES";
                _comproFc.concepto = 2; //TODO de donde lo saco???
                _comproFc.cotizacionMoneda = 1;
                _comproFc.cuit = strCUIT.Replace("-", "");
                _comproFc.fechaDesde = dttFecha;
                _comproFc.fechaEmision = dttFecha;
                _comproFc.fechaHasta = dttFecha;
                _comproFc.fechaPago = dttFecha;
                _comproFc.fechaVto = dttFecha.AddDays(10);
                _comproFc.importeExento = 0;
                _comproFc.importeGravado = Math.Abs(Convert.ToDecimal(_Fc.TotalBruto));
                _comproFc.importeNoGravado = 0; // Math.Abs(_Fc.totNoGravado);
                _comproFc.importeSubTotal = Math.Abs(Convert.ToDecimal(_Fc.TotalBruto));
                _comproFc.importeTotal = Math.Abs(Convert.ToDecimal(_Fc.TotalNeto));
                _comproFc.importeTributos = Math.Abs(_Fc.totTributos);
                _comproFc.NcRechazo = rw.Cells["NcRechazada"].Value.ToString(); // "SI"; //Armar como MiPyme.
                _comproFc.numeroComprobante = 0; //Lo pongo luego de consultar el ultimo
                _comproFc.numeroComprobanteAsoc = ""; //Armar como MiPyme.
                _comproFc.numeroDocumento = Convert.ToInt64(_Fc.ClienteNroDoc);
                _comproFc.obervaciones = _Fc.NumSerie + "-" + _Fc.NumFactura + "-" + _Fc.N;
                _comproFc.ptoVta = _ptoVta;
                _comproFc.tipoComprobante = Convert.ToInt16(_codAfip);
                _comproFc.tipoComprobanteAsoc = 0; //armar com MiPyme
                _comproFc.tipoDocumento = Convert.ToInt16(_Fc.ClienteTipoDoc);
                //Recupero el estado de la FC.
                string estodFC = DataAccess.Facturas.GetEstadoFC(_Fc.N, _Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _pathLog, strConnectionString);

                //switch (rw.Cells["Estado"].Value.ToString().ToUpper())
                switch (estodFC.ToUpper())
                {
                    case "PENDIENTE":
                    case "ERROR":
                        {
                            long _ultimoCbteICG = 0;
                            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Estado en el grid PENDIENTE");
                            List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                            //Recupero el nro del ultimo comprobante.
                            Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCerificado, _pathTaFC, _pathLog, _ptoVta, Convert.ToInt32(_codAfip), 
                                strCUIT, _EsTest, _secureString, false, out _lstError);                            
                            //Vemos si tenemos algun error
                            if (_lstError.Count() == 0)
                            {
                                //Validamos si esta el ultimo
                                if (Common.ValidarUltimoNroComprobante(_Fc.PtoVta, _ultimoComprobante, _Fc.cCodAfip, strConnectionString, out _ultimoCbteICG))
                                {
                                    //incremento el nro de comprobante.
                                    _comproFc.numeroComprobante = _ultimoComprobante + 1;
                                    //Recupero los datos de la fc.
                                    IcgFceDll.DataAccess.DatosFacturaExpo _datosFcExpo = IcgFceDll.DataAccess.DatosFacturaExpo.GetDatosFacturaExpo(_Fc.NumSerie,
                                        _Fc.NumFactura, _Fc.N, strConnectionString, _pathLog);

                                    //Armo el string de IVA.
                                    List<AfipDll.wsAfip.clsIVA> _IvaRequest = ArmarIVA2(_Iva);
                                    //Items
                                    List<AfipDll.wsMtxca.ItemType> _icgItems = IcgFceDll.DataAccess.MTXCA.DatosItemsMtxca.GetDatosItemsMtxca(_Fc.NumSerie,
                                        Convert.ToInt32(_Fc.NumFactura), _Fc.N, Convert.ToInt32(_codAfip), _datosFcExpo, strConnectionString, _pathLog);
                                    //Armos el string de Tributos.
                                    List<AfipDll.wsAfip.clsTributos> _TributosRequest = ArmaTributos(_Tributos);
                                    //Armamos los Impuestos Internos
                                    _TributosRequest = ArmaImpuestosInternos(_ImpInternos, _TributosRequest);
                                    //Armo los comprobantes asociados.
                                    List<AfipDll.wsMtxca.ComprobanteAsociadoType> _compAsoc = ArmarCbteAsocMtxca(_Fc, _comproFc.tipoComprobante);
                                    //Verifico que si es una NC/ND.
                                    if (Convert.ToInt32(_codAfip) == 203 || Convert.ToInt32(_codAfip) == 208 || Convert.ToInt32(_codAfip) == 213
                                        || Convert.ToInt32(_codAfip) == 202 || Convert.ToInt32(_codAfip) == 207 || Convert.ToInt32(_codAfip) == 212
                                        || Convert.ToInt32(_codAfip) == 3 || Convert.ToInt32(_codAfip) == 8 || Convert.ToInt32(_codAfip) == 13
                                        || Convert.ToInt32(_codAfip) == 2 || Convert.ToInt32(_codAfip) == 7 || Convert.ToInt32(_codAfip) == 12)
                                    {
                                        //Verifico que tengo por lo menos 1 comprobante asociado.
                                        if (_compAsoc[0].numeroComprobante == 0)
                                        {
                                            frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_comproFc.tipoComprobante, strCUIT, _Mtxca);
                                            _frm.ShowDialog(this);
                                            _compAsoc = _frm._lstNew;
                                            _frm.Dispose();
                                            if (_compAsoc.Count == 0)
                                            {
                                                //Insertamos en FacturasCamposLibres
                                                ModificoFacturasVentaCampoLibres(_Fc, null, "Error: No posee comprobante asociado", null, null, null);
                                                break;
                                            }
                                        }
                                    }
                                    try
                                    {
                                        AfipDll.WsMTXCA.ObtenerCAE(_pathCerificado, _pathTaFC, _pathLog, _ptoVta,
                                            Convert.ToInt32(_codAfip), strCUIT, _EsTest, _secureString, _generaXML, _IvaRequest, _icgItems, _TributosRequest,
                                            _comproFc, _compAsoc, _tipoFcMiPyme, out _lstError);
                                        string _rta = "";
                                        foreach (AfipDll.WsMTXCA.Errors _er in _lstError)
                                        {
                                            switch (_er.Code)
                                            {
                                                case 1:
                                                    {
                                                        if (String.IsNullOrEmpty(_rta))
                                                            _rta = "Resultado: " + _er.Msg + Environment.NewLine;
                                                        else
                                                            _rta = _rta + "Resultado: " + _er.Msg + Environment.NewLine;
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        if (String.IsNullOrEmpty(_rta))
                                                            _rta = "Observación: " + _er.Msg + Environment.NewLine;
                                                        else
                                                            _rta = _rta + "Observación: " + _er.Msg + Environment.NewLine;
                                                        //Insertamos en FacturasCamposLibres
                                                        //ModificoFacturasVentaCampoLibres(_Fc, null, _er.Msg, null, null, null);
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        if (String.IsNullOrEmpty(_rta))
                                                            _rta = "Error: " + _er.Msg + Environment.NewLine;
                                                        else
                                                            _rta = _rta + "Error: " + _er.Msg + Environment.NewLine;
                                                        //Insertamos en FacturasCamposLibres
                                                        ModificoFacturasVentaCampoLibres(_Fc, null, _er.Msg, null, null, null);
                                                        break;
                                                    }
                                                case 4:
                                                    {
                                                        if (String.IsNullOrEmpty(_rta))
                                                            _rta = "Evento: " + _er.Msg + Environment.NewLine;
                                                        else
                                                            _rta = _rta + "Evento: " + _er.Msg + Environment.NewLine;
                                                        //Insertamos en FacturasCamposLibres
                                                        DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(_Fc.NumSerie, _Fc.NumFactura, _Fc.N, null, _er.Msg, null, null, strConnectionString, _pathLog);
                                                        break;
                                                    }
                                                case 0:
                                                    {
                                                        if (String.IsNullOrEmpty(_rta))
                                                            _rta = "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                                        else
                                                            _rta = _rta + "CAE Otorgado: " + _er.Msg + Environment.NewLine;
                                                        string[] _Resultado = _er.Msg.Split('|');
                                                        //Genero codigo de barra
                                                        string _codBarra = "";
                                                        string qrEncode = "";
                                                        DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _codAfip, _ptoVta.ToString(), _Resultado[0], _Resultado[1], _pathLog, out _codBarra);
                                                        //Genero el los datos del QR.
                                                        qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                            _ptoVta, Convert.ToInt32(_codAfip), Convert.ToInt32(_comproFc.numeroComprobante),
                                                            _comproFc.importeTotal, _comproFc.codigoMoneda, _comproFc.cotizacionMoneda,
                                                            _comproFc.tipoDocumento, Convert.ToInt64(_comproFc.numeroDocumento), "E", Convert.ToInt64(_Resultado[0]));
                                                        //Ponemos los datos en el log.
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Código de Barra -> " + _codBarra);
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Código QR -> " + qrEncode);
                                                        //Insertamos en FacturasVentasSeriesResol
                                                        Insertar_FacturasVentaSerieResol(_Fc, Convert.ToInt32(_Resultado[2]));
                                                        //Insertamos en FacturasCamposLibres
                                                        ModificoFacturasVentaCampoLibres(_Fc, _Resultado[0], null, _Resultado[1], _codBarra, qrEncode);

                                                        break;
                                                    }
                                            }
                                        }
                                        AfipDll.LogFile.ErrorLog(LogFile.CreatePath(_pathLog), _rta);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), ex.Message);
                                        MessageBox.Show("Se produjo un error al intentar fiscalizar el comprobante."+
                                            Environment.NewLine + "Error: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Se ha encontrado una diferencia entre el último comprobate en AFIP e ICG" +
                                    Environment.NewLine + "Último en AFIP: " + _ultimoComprobante.ToString() +
                                    Environment.NewLine + "Último en ICG: " + _ultimoCbteICG +
                                    Environment.NewLine + "Por favor Verifique que el comprobante no este anulado"+
                                    Environment.NewLine + "Comuníquese con ICG para solucinarlo", 
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                string _error = "";
                                foreach (AfipDll.WsMTXCA.Errors er in _lstError)
                                {
                                    _error = _error + "Codigo: " + er.Code + " Detalle: " + er.Msg;
                                }
                                MessageBox.Show("Se produjo el siguiente error al consultar el último comprobante en la AFIP" +
                                    Environment.NewLine + "Error: " + _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            break;
                        }
                    case "RECUPERAR":
                        {
                            //Recupero el nro grabado como pendiente.
                            DataAccess.FacturasVentaCamposLibres _fvcl = DataAccess.FacturasVentaCamposLibres.GetRecord(strConnectionString, _Fc);
                            if (String.IsNullOrEmpty(_fvcl.CodBarras))
                                _fvcl.CodBarras = "0";
                            if (Convert.ToInt32(_fvcl.CodBarras) >= 0)
                            {
                                //Recuperamos el CAE.
                                wsAfip.clsCaeRecupero _rec = wsAfip.ConsultaComprobante(_pathCerificado, _pathTaFC, Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, Convert.ToInt32(_fvcl.CodBarras), _EsTest, _pathLog);
                                //Analizamos la respuesta.
                                switch (_rec.Resultado.ToUpper())
                                {
                                    case "A":
                                        {
                                            string strCodBarra;
                                            string qrEncode;
                                            GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _rec.Cae, _rec.FechaVto, out strCodBarra);
                                            qrEncode = IcgFceDll.QR.CrearJson(1, dttFecha, Convert.ToInt64(strCUIT),
                                                Convert.ToInt32(_Fc.PtoVta), _rec.CbtTipo, Convert.ToInt32(_rec.CbteDesde),
                                                Convert.ToDecimal(_rec.ImporteTotal), _rec.MonId, Convert.ToDecimal(_rec.MonCtz),
                                                _rec.DocTipo, Convert.ToInt64(_rec.DocNro), "E", Convert.ToInt64(_rec.Cae));
                                            //Ponemos los datos en el log.
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Código de Barra -> " + strCodBarra);
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Código QR -> " + qrEncode);
                                            //Modificamos el registro en FacturasCamposLibres.
                                            ModificoFacturasVentaCampoLibres(_Fc, _rec.Cae, _rec.ErrorMsj, _rec.FechaVto, strCodBarra, qrEncode);
                                            //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.
                                            if (!String.IsNullOrEmpty(_rec.Cae))
                                            {
                                                Insertar_FacturasVentaSerieResol(_Fc, Convert.ToInt32(_fvcl.CodBarras.Trim()));
                                            }

                                            break;
                                        }
                                    case "R":
                                        {
                                            if (_rec.ErrorMsj == "No existen datos en nuestros registros para los parametros ingresados."
                                                || _rec.ErrorMsj == "Campo CbteNro no lo ingreso o supera el tamaño permitido de 8 caracteres numericos.")
                                            {
                                                //Modificamos el registro en FacturasCamposLibres.
                                                ModificoFacturasVentaCampoLibres(_Fc, "", "", "", "", "");
                                            }
                                            break;
                                        }
                                }
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Metodos Propios
        private List<AfipDll.wsAfip.clsTributos> ArmaTributos(List<DataAccess.FacturasVentasTot> _Tributos)
        {
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los tributos");
            //string strRta = "";
            double totalBaseImponible = 0;
            double totalImpuesto = 0;
            List<AfipDll.wsAfip.clsTributos> _Rta = new List<AfipDll.wsAfip.clsTributos>();

            foreach (DataAccess.FacturasVentasTot itm in _Tributos)
            {
                AfipDll.wsAfip.clsTributos cls = new AfipDll.wsAfip.clsTributos();
                cls.Alic = Convert.ToDouble(itm.Alicuota);
                totalBaseImponible = Convert.ToDouble(itm.BaseImponible);
                totalImpuesto = Convert.ToDouble(itm.TotIva);
                if (totalBaseImponible < 0)
                    cls.BaseImp = totalBaseImponible * -1;
                else
                    cls.BaseImp = totalBaseImponible;
                cls.Desc = itm.Nombre;
                if (cls.Desc.Contains("IVA"))
                    cls.ID = 1;
                else
                    cls.ID = 2;
                if (totalImpuesto < 0)
                    cls.Importe = totalImpuesto * -1;
                else
                    cls.Importe = totalImpuesto;
                _Rta.Add(cls);

            }

            return _Rta;
        }

        private List<AfipDll.wsAfip.clsTributos> ArmaImpuestosInternos(List<DataAccess.FacturasVentasTot> _impInt, List<AfipDll.wsAfip.clsTributos> tributos)
        {            
            foreach (DataAccess.FacturasVentasTot itm in _impInt)
            {
                AfipDll.wsAfip.clsTributos cls = new AfipDll.wsAfip.clsTributos();
                double baseImponible = Convert.ToDouble(itm.BaseImponible);
                double alicuota = Convert.ToDouble(itm.Alicuota);
                double totIva = Convert.ToDouble(itm.TotIva);
                if (baseImponible > 0)
                {
                    cls.Alic = alicuota;
                    cls.BaseImp = baseImponible;
                    cls.Importe = totIva;
                }
                else
                {
                    cls.Alic = alicuota * -1;
                    cls.BaseImp = baseImponible * -1;
                    cls.Importe = totIva * -1;
                }
                cls.Desc = itm.Nombre;
                cls.ID = 4;
                
                tributos.Add(cls);

            }

            return tributos;
        }

        private List<AfipDll.wsAfip.clsIVA> ArmarIVA2(List<DataAccess.FacturasVentasTot> _Iva)
        {
            List<AfipDll.wsAfip.clsIVA> _Rta = new List<AfipDll.wsAfip.clsIVA>();
            foreach (DataAccess.FacturasVentasTot itm in _Iva)
            {
                if (!itm.Nombre.ToUpper().Contains("EXENTO") && !itm.Nombre.ToUpper().Contains("NO GRAVADO"))
                {
                    AfipDll.wsAfip.clsIVA cls = new AfipDll.wsAfip.clsIVA();
                    switch (itm.Iva.ToString())
                    {
                        case "0":
                            {
                                cls.ID = 3;
                                break;
                            }
                        case "21":
                            {
                                cls.ID = 5;
                                break;
                            }
                        case "10.5":
                        case "10,5":
                            {
                                cls.ID = 4;
                                break;
                            }
                        case "27":
                            {
                                cls.ID = 6;
                                break;
                            }
                    }
                    if (itm.BaseImponible < 0)
                        cls.BaseImp = Convert.ToDouble(itm.BaseImponible) * -1;
                    else
                        cls.BaseImp = Convert.ToDouble(itm.BaseImponible);
                    if (itm.TotIva < 0)
                        cls.Importe = Convert.ToDouble(itm.TotIva) * -1;
                    else
                        cls.Importe = Convert.ToDouble(itm.TotIva);
                    if (cls.ID == 3 || cls.ID == 4 || cls.ID == 5 || cls.ID == 6)
                    {
                        if (_Rta.Where(x => x.ID == cls.ID).Count() > 0)
                        {
                            //list.Where(w => w.Name == "height").ToList().ForEach(s => s.Value = 30);
                            _Rta.Where(x => x.ID == cls.ID).ToList().ForEach(x => x.Importe = x.Importe + cls.Importe);
                            _Rta.Where(x => x.ID == cls.ID).ToList().ForEach(x => x.BaseImp = x.BaseImp + cls.BaseImp);
                        }
                        else
                        {
                            _Rta.Add(cls);
                        }
                    }
                }
            }
            return _Rta;
        }

        private List<AfipDll.wsAfip.ComprobanteAsoc> ArmarCbteAsoc(DataAccess.DatosFactura fc)
        {
            List<AfipDll.wsAfip.ComprobanteAsoc> _lst = new List<AfipDll.wsAfip.ComprobanteAsoc>();
            try
            {
                using (SqlConnection _conexion = new SqlConnection(strConnectionString))
                {
                    List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _conexion);

                    foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                    {
                        AfipDll.wsAfip.ComprobanteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsoc(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _conexion);
                        bool _exist = _lst.Any(c => c.PtoVta == _cls.PtoVta && c.Nro == _cls.Nro);
                        if(!_exist)
                            _lst.Add(_cls);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        private List<AfipDll.wsMtxca.ComprobanteAsociadoType> ArmarCbteAsocMtxca(DataAccess.DatosFactura fc, int _tipoComprobante)
        {
            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos los comprobantes Asociados");
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lst = new List<AfipDll.wsMtxca.ComprobanteAsociadoType>();
            try
            {
                using (SqlConnection _conexion = new SqlConnection(strConnectionString))
                {
                    List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _conexion);

                    foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                    {
                        AfipDll.wsMtxca.ComprobanteAsociadoType _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocMTXCA(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _tipoComprobante, _conexion);
                        bool _exist = _lst.Any(c => c.numeroPuntoVenta == _cls.numeroPuntoVenta && c.numeroComprobante == _cls.numeroComprobante);
                        if (!_exist)
                            _lst.Add(_cls);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        private string GetCodClienteDoc(string p)
        {
            try
            {
                return p.Split('_')[0].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Codigo Tipo Documento Cliente mal definido. " + ex.Message);
            }
        }

        private string GetCodAfip(string p)
        {
            try
            {
                return p.Split(' ')[0].ToString();
            }
            catch(Exception ex)
            {
                throw new Exception("Codigo AFIP mal definido. " + ex.Message);
            }
        }

        private void GenerarCodigoBarra(string _Cuit, string _TipoComprobate, string _PtoVta, string _CAE, string _Vto, out string _CodigoBarra)
        {
            int _tipoComprobante = Convert.ToInt32(_TipoComprobate);
            //Armo el string de todo.
            string strTodo = _Cuit + _tipoComprobante.ToString().PadLeft(3,'0') + _PtoVta.PadLeft(5,'0') + _CAE + _Vto;
            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "CUIT: " + _Cuit + ", TipoComprobante: " + _tipoComprobante.ToString().PadLeft(3, '0') + ", PtoVta: " + _PtoVta.PadLeft(5, '0') + ", CAE: " + _CAE + ", FVTO: " + _Vto);
            try
            {
                int intTamaño = strTodo.Length;
                int[] a = new int[intTamaño];
                int pares = 0, impares = 0;
                //Cargamos el array con los digitos.
                for (int i = 0; i < intTamaño; i++)
                {
                    a[i] = int.Parse(strTodo[i].ToString());
                }
                //Sumamos las posiciones pares y las impares por separado.
                for (int i = 0; i < 10; i++)
                {
                    if ((i % 2) == 0)
                        pares += a[i];
                    else
                        impares += a[i];
                }
                //Dividimos y sumamos pares
                int intT = (impares / 3) + pares;
                //Recupero el ultimo digito de la suma.
                int intUlimoDigito = int.Parse(intT.ToString().Substring(intT.ToString().Length - 1, 1));
                //Vemos si el ultimo digito es 0
                if (intUlimoDigito == 0)
                    strTodo = strTodo + intUlimoDigito.ToString();
                else
                {
                    intUlimoDigito = 10 - intUlimoDigito;
                    strTodo = strTodo + intUlimoDigito.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al calcular el digito verificador." + ex.Message);
                strTodo = "";
            }
            _CodigoBarra = strTodo;
        }

        private wsAfip.clsDetReq CargarDetalle(DataAccess.DatosFactura _Fac, int _NroComprobante)
        {
            wsAfip.clsDetReq _detalle = new wsAfip.clsDetReq();
            _detalle.Concepto = 2;
            _detalle.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
            _detalle.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
            _detalle.CbteDesde = _NroComprobante;
            _detalle.CbteHasta = _NroComprobante;
            _detalle.CbteFch = _Fac.strFecha;
            if (_Fac.TotalNeto.StartsWith("-"))
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto) * -1;
            else
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto);
            _detalle.ImpTotConc = 0;
            if (_Fac.TotalBruto.StartsWith("-"))
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto) * -1;
            else
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto);
            _detalle.ImpOpEx = 0;
            if (_Fac.TotIva < 0)
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva) * -1;
            else
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva);
            if (_Fac.totTributos < 0)
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos) * -1;
            else
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos);
            _detalle.FchServDesde = _Fac.strFecha;
            _detalle.FchServHasta = _Fac.strFecha;
            _detalle.FchVtoPago = _Fac.strFecha;
            _detalle.MonID = "PES";
            _detalle.MonCotiz = 1;

            return _detalle;
        }

        private AfipDll.wsAfip.clsDetReq CargarDetalle2(DataAccess.DatosFactura _Fac, int _NroComprobante)
        {
            AfipDll.wsAfip.clsDetReq _detalle = new AfipDll.wsAfip.clsDetReq();
            _detalle.Concepto = 2;
            _detalle.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
            _detalle.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
            _detalle.CbteDesde = _NroComprobante;
            _detalle.CbteHasta = _NroComprobante;
            _detalle.CbteFch = _Fac.strFecha;
            if (_Fac.TotalNeto.StartsWith("-"))
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto) * -1;
            else
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto);
            _detalle.ImpTotConc = 0;
            if (_Fac.TotalBruto.StartsWith("-"))
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto) * -1;
            else
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto);
            _detalle.ImpOpEx = 0;
            if (_Fac.TotIva < 0)
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva) * -1;
            else
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva);
            if (_Fac.totTributos < 0)
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos) * -1;
            else
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos);
            _detalle.FchServDesde = _Fac.strFecha;
            _detalle.FchServHasta = _Fac.strFecha;
            _detalle.FchVtoPago = _Fac.strFecha;
            _detalle.MonID = "PES";
            _detalle.MonCotiz = 1;

            return _detalle;
        }

        private double GetImporteExento(List<DataAccess.FacturasVentasTot> _IvaFcTot)
        {
            try
            {
                double importe = 0;
                foreach(DataAccess.FacturasVentasTot _rw in _IvaFcTot)
                {
                    if (_rw.Iva == 0)
                    {
                        if (_rw.Nombre.ToUpper().Contains("EXENTO"))
                            importe = importe + Convert.ToDouble(_rw.BaseImponible);
                    }
                }
                return importe;
            }
            catch(Exception ex)
            {
                throw new Exception("Error en GetImporteExento: " + ex.Message);
            }
        }
        private double GetImporteNoGravado(List<DataAccess.FacturasVentasTot> _IvaFcTot)
        {
            try
            {
                double importe = 0;
                foreach (DataAccess.FacturasVentasTot _rw in _IvaFcTot)
                {
                    if (_rw.Iva == 0)
                    {
                        if (_rw.Nombre.ToUpper().Contains("NO GRAVADO"))
                            importe = importe + Convert.ToDouble(_rw.BaseImponible);
                    }
                }
                return importe;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en GetImporteExento: " + ex.Message);
            }
        }
        private AfipDll.wsAfip.clsDetReq CargarDetalle3(DataAccess.DatosFactura _Fac, int _tipoCbte, int _NroComprobante, List<DataAccess.FacturasVentasTot> _IvaFcTot)
        {

            AfipDll.wsAfip.clsDetReq _detalle = new AfipDll.wsAfip.clsDetReq();
            _detalle.Concepto = 2;
            _detalle.DocTipo = Convert.ToInt16(_Fac.ClienteTipoDoc);
            _detalle.DocNro = Convert.ToInt64(_Fac.ClienteNroDoc.Replace("-", ""));
            _detalle.CbteDesde = _NroComprobante;
            _detalle.CbteHasta = _NroComprobante;
            _detalle.CbteFch = _Fac.strFecha;
            if (_Fac.TotalNeto.StartsWith("-"))
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto) * -1;
            else
                _detalle.ImpTotal = Convert.ToDouble(_Fac.TotalNeto);
            _detalle.ImpTotConc = GetImporteNoGravado(_IvaFcTot);
            if (Convert.ToDouble(_Fac.TotalBruto) < 0)
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto) * -1;
            else
                _detalle.ImpNeto = Convert.ToDouble(_Fac.TotalBruto);
            if (_tipoCbte == 11 || _tipoCbte == 13)
                _detalle.ImpOpEx = 0;
            else
                _detalle.ImpOpEx = Math.Abs(GetImporteExento(_IvaFcTot));
            if (_Fac.TotIva < 0)
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva) * -1;
            else
                _detalle.ImpIVA = Convert.ToDouble(_Fac.TotIva);
            if (_Fac.totTributos < 0)
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos) * -1;
            else
                _detalle.ImpTrib = Convert.ToDouble(_Fac.totTributos);
            _detalle.FchServDesde = _Fac.strFecha;
            _detalle.FchServHasta = _Fac.strFecha;
            _detalle.FchVtoPago = _Fac.strFecha;
            _detalle.MonID = "PES";
            _detalle.MonCotiz = 1;
            //
            _detalle.ImpNeto = _detalle.ImpNeto - _detalle.ImpOpEx - _detalle.ImpTotConc;

            return _detalle;
        }
        #endregion

        #region ICG
        private void ModificoFacturasVentaCampoLibres(DataAccess.DatosFactura _Fc, string _Cae, string _Error, string _FeVto, 
            string _CodBarra, string _qr)
        {
            try
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.Cae = _Cae;
                        fcLib.CodBarras = _CodBarra;
                        fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        fcLib.qrEncoce = _qr;
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                    }

                    fcLib.N = _Fc.N;
                    fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                    fcLib.NumeroSerie = _Fc.NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _pathLog, strConnectionString);
                }
            }
            catch(Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "ModificoFacturasVentaCampoLibres. Error: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void ModificoFacturasVentaCampoLibres(DataAccess.DatosFactura _Fc, int _NroComprobante, string _Error)
        {
            using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
            {
                fcLib.Cae = "";
                fcLib.CodBarras = _NroComprobante.ToString();
                fcLib.ErrorCae = "";
                fcLib.Estado = "RECUPERAR";
                fcLib.ErrorCae = _Error;
                fcLib.VtoCae = "";
                fcLib.N = _Fc.N;
                fcLib.NumeroFactura = Convert.ToInt32(_Fc.NumFactura);
                fcLib.NumeroSerie = _Fc.NumSerie;

                fcLib.UpdateFacturasVentasCamposLibres(fcLib, _pathLog, strConnectionString);
            }
        }

        private void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _n, string _Cae, string _Error, 
            string _FeVto, string _CodBarra, string _Qr)
        {
            try
            {
                using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                {
                    if (!String.IsNullOrEmpty(_Cae))
                    {
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "FACTURADO";
                        fcLib.Cae = _Cae;
                        fcLib.CodBarras = _CodBarra;
                        fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        fcLib.qrEncoce = _Qr;
                    }
                    else
                    {
                        fcLib.ErrorCae = _Error;
                        fcLib.Estado = "ERROR";
                    }

                    fcLib.N = _n;
                    fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                    fcLib.NumeroSerie = _NumSerie;

                    fcLib.UpdateFacturasVentasCamposLibres(fcLib, _pathLog, strConnectionString);
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "ModificoFacturasVentaCampoLibres. Error: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Insertar_FacturasVentaSerieResol(DataAccess.DatosFactura _Fc, int _NroComprobante)
        {
            try
            {
                using (DataAccess.FacturasVentaSeriesResol _fcResol = new DataAccess.FacturasVentaSeriesResol())
                {
                    _fcResol.NumSerie = _Fc.NumSerie;
                    _fcResol.NumFactura = Convert.ToInt32(_Fc.NumFactura);
                    _fcResol.N = _Fc.N;
                    _fcResol.SerieFiscal1 = _Fc.PtoVta;
                    _fcResol.SerieFiscal2 = _Fc.strTipoDoc;
                    _fcResol.NumeroFiscal = _NroComprobante;

                    DataAccess.FacturasVentaSeriesResol.InsertFacturasVentaSeriesResol(_fcResol, strConnectionString);
                }
            }
            catch(Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Insertar_FacturasVentaSerieResol. Error: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void LoadFacturas()
        {
            try
            {
                string strFechaDesde = dtpDesde.Value.ToShortDateString();
                string strFechaHasta = dtpHasta.Value.ToShortDateString();
                grdGrid.SetDataBinding(null, "");
                List<DataAccess.Facturas> _Facturas = DataAccess.Facturas.GetFacturas(dtpDesde.Value, dtpHasta.Value, _codigoEmpresa, lstTipoDoc, lstClientes, _tieneComputoIva, strConnectionString);
                grdGrid.SetDataBinding(_Facturas, "");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message);
            }
        }
        #endregion

        #region Exportacion
        private void BuscarCaeDeExportacion(string numSerie, string numFactura, string n, string codigoAfip, 
            string ptoVta, SecureString password, bool generaXml)
        {
            try
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Comenzamos con la busqueda");
                //Recupero los datos de la fc.
                DataAccess.DatosFacturaExpo _datosFcExpo = DataAccess.DatosFacturaExpo.GetDatosFacturaExpo(numSerie, numFactura, n, strConnectionString);
                //Recuperamos los datos del cliente.
                DataAccess.DatosClienteExpo _cliente = DataAccess.DatosClienteExpo.GetDatosClienteExpo(_datosFcExpo.codCliente, strConnectionString);
                //Recuperamos los Items de la FC.
                List<DataAccess.DatosItemsExpo> _items = DataAccess.DatosItemsExpo.GetDatosItemsExpo(numSerie, Convert.ToInt32(numFactura), n, strConnectionString);
                //Recupero los comprobantes asociados.
                List<AfipDll.wsExportacion.Cmp_asoc> _ComprobantesAsociados = ArmarCbteAsocExpo(_datosFcExpo);
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Recuperamos los datos y vamos a validar");
                //Validamos los datos recuperados.
                List<string> _Validaciones = ValidarDatos(_datosFcExpo, _cliente, _items, ptoVta);

                if (_Validaciones.Count > 0)
                {
                    string _mensaje = "";
                    foreach (string st in _Validaciones)
                    {
                        _mensaje = _mensaje + st + Environment.NewLine;
                    }

                    MessageBox.Show("Por favor revise los mensajes y corrijalos." +
                        Environment.NewLine + Environment.NewLine + _mensaje,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Validacion OK");
                    //Proceso.
                    string _rtdo;
                    string _error;
                    //string _ptoVta;
                    //Recupero el ultimo numero enviado.
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Buscamos el ultimo en AFIP");
                    int _ultimoNro = AfipDll.wsAfipExpo.GetLastCmp(_pathCerificado, _pathTaFCE, _pathLog, strCUIT, _EsTest, ptoVta, Convert.ToInt32(codigoAfip), password, out _rtdo, out _error);
                    //Si tengo nro proceso.
                    if (_rtdo.ToUpper() == "OK")
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Tenemos el ultimo.");
                        string _nroCae = "";
                        string _rtdoCae = "";
                        string _fechaVtoCae = "";
                        string _errorCae = "";

                        _ultimoNro = _ultimoNro + 1;

                        string _nroComprobante = "E" + ptoVta + _ultimoNro.ToString().PadLeft(8, '0');
                        int _concepto = GetTipoExpo(_cliente._tipoExpo);
                        DateTime _fecha = _datosFcExpo.fecha;
                        //Cargamos la cabecera.
                        string _otrosDatos = numSerie + "-" + numFactura + "-" + n;
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Cargamos cabecera");
                        AfipDll.wsAfipExpo.Cabecera _cab = CargoCabeceraExpoNew(_ultimoNro, ptoVta, _datosFcExpo, _cliente, _concepto, _otrosDatos, codigoAfip);
                        //Cargmos los detalles.
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Cargamos detalles");
                        List<AfipDll.wsAfipExpo.Detalle> _det = CargoDetallesExpoNew(_items,_datosFcExpo);

                        try
                        {
                            //tenemos todo vamos por el CAE.
                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Fiscalizamos");
                            bool _caeOk = AfipDll.wsAfipExpo.AutorizarExpo(_pathCerificado, _pathTaFCE, _pathLog, strCUIT, _EsTest,
                                _cab, _det, _ComprobantesAsociados, password, _generaXML,
                                out _rtdoCae, out _nroCae, out _fechaVtoCae, out _errorCae);

                            if (_caeOk)
                            {
                                string _codBarra = "";
                                string qrEncode = "";
                                switch (_rtdoCae.ToUpper())
                                {
                                    case "A":
                                        {
                                            //Aprobado.
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Fiscalizacion OK");
                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, codigoAfip, _cab.ptovta.ToString(), _nroCae, _fechaVtoCae, _pathLog, out _codBarra);
                                            //QR
                                            int tipoDocumentoReceptor = String.IsNullOrEmpty(_cab.tipodocrecptor) ? 0 : Convert.ToInt32(_cab.tipodocrecptor);
                                            long nroDocumentoReceptor = String.IsNullOrEmpty(_cab.nrodocreceptor) ? 0 : Convert.ToInt64(_cab.nrodocreceptor);
                                            qrEncode = IcgFceDll.QR.CrearJson(1, _datosFcExpo.fecha, Convert.ToInt64(strCUIT),
                                                Convert.ToInt32(_cab.ptovta), _cab.Tipo, _ultimoNro, Convert.ToDecimal(_cab.importetotal),
                                                _cab.moneda, Convert.ToDecimal(_cab.tipocambio), tipoDocumentoReceptor,
                                                nroDocumentoReceptor, "E", Convert.ToInt64(_nroCae));
                                            //_todoOk = true;
                                            break;
                                        }
                                    case "R":
                                        {
                                            //Rechazado.
                                            //_todoOk = true;
                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Fiscalizacion RECHAZADA");
                                            break;
                                        }
                                    case "P":
                                        {
                                            //Parcial con Observaciones.
                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, codigoAfip, _cab.ptovta.ToString(), _nroCae, _fechaVtoCae, _pathLog, out _codBarra);
                                            //QR
                                            int tipoDocumentoReceptor = String.IsNullOrEmpty(_cab.tipodocrecptor) ? 0 : Convert.ToInt32(_cab.tipodocrecptor);
                                            long nroDocumentoReceptor = String.IsNullOrEmpty(_cab.nrodocreceptor) ? 0 : Convert.ToInt64(_cab.nrodocreceptor);
                                            qrEncode = IcgFceDll.QR.CrearJson(1, _datosFcExpo.fecha, Convert.ToInt64(strCUIT),
                                                Convert.ToInt32(_cab.ptovta), _cab.Tipo, _ultimoNro, Convert.ToDecimal(_cab.importetotal),
                                                _cab.moneda, Convert.ToDecimal(_cab.tipocambio), tipoDocumentoReceptor,
                                                nroDocumentoReceptor, "E", Convert.ToInt64(_nroCae));
                                            //_todoOk = true;
                                            break;
                                        }
                                    default:
                                        {
                                            //Cualquier otro.
                                            //_todoOk = true;
                                            break;
                                        }
                                }
                                //Modificamos el registro en FacturasCamposLibres.
                                ModificoFacturasVentaCampoLibres(numSerie, numFactura, n, _nroCae, _errorCae, _fechaVtoCae, _codBarra, qrEncode);
                                //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.                                            
                                if (!String.IsNullOrEmpty(_nroCae))
                                {
                                    DataAccess.FuncionesVarias.Insertar_FacturasVentaSerieResol(numSerie, numFactura, n, ptoVta, codigoAfip, _ultimoNro, 
                                        strConnectionString, _pathLog);
                                }
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error a fiscalizar, " + _errorCae);
                                //Error al recuperar el ultimo numero de factura de la AFIP
                                //DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(numSerie, numFactura, n, "", _errorCae, "", "", strConnectionString, _pathLog);
                                ModificoFacturasVentaCampoLibres(numSerie, numFactura, n, "", _errorCae, "", "", "");
                            }
                        }
                        catch (Exception ex)
                        {

                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error: " + ex.Message);
                            if(!String.IsNullOrEmpty(ex.StackTrace))
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Trace: " + ex.StackTrace);
                            }
                            //Recupero el nro del ultimo comprobante.
                            int _ultimoNroNuevo = AfipDll.wsAfipExpo.GetLastId(_pathCerificado, _pathTaFCE, _pathLog, strCUIT, _EsTest, password, out _rtdo, out _error, out ptoVta);
                            //Vemos si el ultimo comprobante coincide con el ultimo enviado.
                            if (_ultimoNro == _ultimoNroNuevo)
                            {
                                //Guardamos el numero y ponemos el estado en Recuperar.
                                //Modificamos el registro en FacturasCamposLibres.
                                DataAccess.FuncionesVarias.ModificoFacturasVentaCampoLibres(numSerie, numFactura, n, _ultimoNro, ex.Message, strConnectionString, _pathLog);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se pudo obtener comunicación con la AFIP. Por favor intente nuevamente.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch(Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error: " + ex.Message);
                if (!String.IsNullOrEmpty(ex.StackTrace))
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Trace: " + ex.StackTrace);
                }
                MessageBox.Show("Se produjo el siguiente ERROR: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<string> ValidarDatos(DataAccess.DatosFacturaExpo facturaExpo, DataAccess.DatosClienteExpo clienteExpo, 
            List<DataAccess.DatosItemsExpo> itemsExpo, string ptoVta)
        {
            List<string> _rta = new List<string>();
            //Codigo Moneda.
            if(String.IsNullOrEmpty(facturaExpo.codigoMoneda))
                _rta.Add("No existe el código de la Moneda.");
            //Pais destino.
            if(String.IsNullOrEmpty(clienteExpo._paisDestino))
                _rta.Add("No existe el país destino.");
            //Tipo Exportacion.
            if (String.IsNullOrEmpty(clienteExpo._tipoExpo))
                _rta.Add("Debe seleccionar un Tipo de Exportación");
            
            //Cotizacion.
            if (facturaExpo.cotizacion <= 0)
                _rta.Add("Debe ingresar la cotización de la moneda.");
            
            //
            if (String.IsNullOrEmpty(ptoVta))
                _rta.Add("No hay punto de venta declarado. Por favor configurelo y luego vuelva a solicitar CAE.");

            if (String.IsNullOrEmpty(clienteExpo._razonSocial))
                _rta.Add("No hay Razon Social. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            if (String.IsNullOrEmpty(clienteExpo._cuit))
                _rta.Add("No hay CUIT en el cliente. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            if (String.IsNullOrEmpty(clienteExpo._domicilio))
                _rta.Add("No hay Domicilio en el cliente. Por favor ingreselo en el cliente y luego vuelva a solicitar CAE.");

            if(String.IsNullOrEmpty(facturaExpo.incoterms))
                _rta.Add("No se definio el valor para Incoterms. Por favor ingreselo y luego vuelva a solicitar CAE.");

            //Items.
            if (itemsExpo.Count == 0)
                _rta.Add("No hay Items en la factura.");
            
            decimal _total = 0;
            foreach (DataAccess.DatosItemsExpo _rw in itemsExpo)
            {
                decimal _ct = _rw.total;
                _total = _total + _ct;
            }

            decimal _txtTotal = facturaExpo.totalBruto;
            //Vemos dto comercial
            if (facturaExpo.totDtoComercial > 0)
                _total = _total - facturaExpo.totDtoComercial;
            //Vemos dto Pp
            if (facturaExpo.totDtoPp > 0)
                _total = _total - facturaExpo.totDtoPp;

            if (_txtTotal != _total)
                _rta.Add("La sumatoria de los totales de Items no coincide con el total de la Factura.");

            return _rta;
        }

        private int GetTipoExpo(string p)
        {
            int _rta = 0;
            string _cod = p.Split(' ')[0].ToString();
            try
            { _rta = Convert.ToInt32(_cod); }
            catch
            { throw new Exception("Error al convertir el campo Tipo de Exportación."); }
            return _rta;
        }

        private string GetIncoterms(string p)
        {
            try
            {
                return p.Split(' ')[0].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Codigo Incoterms mal definido. " + ex.Message);
            }
        }

        private wsAfipExpo.Cabecera CargoCabeceraExpo(int ultimoNro, string ptoVta,
            DataAccess.DatosFacturaExpo facturaExpo, DataAccess.DatosClienteExpo clienteExpo, int concepto, string otrosDatos, string codAfip)
        {
            string _nroComprobante = "E" + ptoVta + ultimoNro.ToString().PadLeft(8, '0');
            //Cargamos la cabecera.
            wsAfipExpo.Cabecera _cab = new wsAfipExpo.Cabecera();
            _cab.ape = "S";
            _cab.comprobante = _nroComprobante;
            _cab.concepto = concepto;
            _cab.cuitemisor = strCUIT;
            _cab.destinocmp = clienteExpo._paisDestino;
            _cab.detalles = "";
            _cab.domicilioreceptor = clienteExpo._domicilio;
            _cab.fechaemision = facturaExpo.fecha.Year.ToString() + facturaExpo.fecha.Month.ToString().PadLeft(2, '0') + facturaExpo.fecha.Day.ToString().PadLeft(2, '0');
            _cab.formaspago = "Contado";
            _cab.idimpositivoreceptor = clienteExpo._cuit;
            _cab.idioma = 1;
            _cab.importetotal = facturaExpo.totalBruto.ToString().Replace(',', '.');
            _cab.incoterms = GetIncoterms(facturaExpo.incoterms);
            _cab.moneda = facturaExpo.codigoMoneda;
            _cab.nrodocreceptor = "";
            _cab.otrosdatosgenerales = otrosDatos;
            _cab.ptovta = ptoVta;
            _cab.receptor = clienteExpo._razonSocial;
            _cab.Tipo = Convert.ToInt32(codAfip);
            _cab.tipocambio = facturaExpo.cotizacion.ToString().Replace(',','.');
            _cab.tipodocrecptor = "";
            _cab.iD = facturaExpo.numFactura;
            _cab.fechavto = facturaExpo.fechaVto.Year.ToString() + facturaExpo.fechaVto.Month.ToString().PadLeft(2, '0') + facturaExpo.fechaVto.Day.ToString().PadLeft(2, '0');

            return _cab;
        }

        private AfipDll.wsAfipExpo.Cabecera CargoCabeceraExpoNew(int ultimoNro, string ptoVta,
            DataAccess.DatosFacturaExpo facturaExpo, DataAccess.DatosClienteExpo clienteExpo, int concepto, string otrosDatos, string codAfip)
        {
            string _nroComprobante = "E" + ptoVta + ultimoNro.ToString().PadLeft(8, '0');
            //Cargamos la cabecera.
            AfipDll.wsAfipExpo.Cabecera _cab = new AfipDll.wsAfipExpo.Cabecera();
            _cab.ape = "S";
            _cab.comprobante = _nroComprobante;
            _cab.concepto = concepto;
            _cab.cuitemisor = strCUIT;
            _cab.destinocmp = clienteExpo._paisDestino;
            _cab.detalles = "";
            _cab.domicilioreceptor = clienteExpo._domicilio;
            _cab.fechaemision = facturaExpo.fecha.Year.ToString() + facturaExpo.fecha.Month.ToString().PadLeft(2, '0') + facturaExpo.fecha.Day.ToString().PadLeft(2, '0');
            _cab.formaspago = "Contado";
            _cab.idimpositivoreceptor = clienteExpo._cuit;
            _cab.idioma = 1;
            _cab.importetotal = facturaExpo.totalBruto.ToString().Replace(',', '.');
            _cab.incoterms = GetIncoterms(facturaExpo.incoterms);
            _cab.moneda = facturaExpo.codigoMoneda;
            _cab.nrodocreceptor = "";
            _cab.otrosdatosgenerales = otrosDatos;
            _cab.ptovta = ptoVta;
            _cab.receptor = clienteExpo._razonSocial;
            _cab.Tipo = Convert.ToInt32(codAfip);
            _cab.tipocambio = facturaExpo.cotizacion.ToString().Replace(',', '.');
            _cab.tipodocrecptor = "";
            _cab.iD = facturaExpo.numFactura;
            _cab.fechavto = facturaExpo.fechaVto.Year.ToString() + facturaExpo.fechaVto.Month.ToString().PadLeft(2, '0') + facturaExpo.fechaVto.Day.ToString().PadLeft(2, '0');

            return _cab;
        }

        private List<wsAfipExpo.Detalle> CargoDetallesExpo(List<DataAccess.DatosItemsExpo> items,
            DataAccess.DatosFacturaExpo facturaExpo)
        {
            List<wsAfipExpo.Detalle> _det = new List<wsAfipExpo.Detalle>();
            //Recorro la lista.
            foreach (DataAccess.DatosItemsExpo _rw in items)
            {

                wsAfipExpo.Detalle _dt = new wsAfipExpo.Detalle();
                _dt.cant = _rw.unidades.ToString().Replace(',', '.');
                _dt.cod = _rw.referencia.ToString();
                _dt.descrip = _rw.descripcion.ToString();
                _dt.importe = Math.Round(Convert.ToDecimal(_rw.total, System.Globalization.CultureInfo.InvariantCulture), 2).ToString().Replace(',', '.');
                _dt.preciounit = Math.Round(Convert.ToDecimal(_rw.precio, System.Globalization.CultureInfo.InvariantCulture), 2).ToString().Replace(',', '.');
                _dt.unimed = _rw.codigoaduana.ToString();
                _dt.dto = _rw.dto.ToString().Replace(',', '.');

                if (Convert.ToDecimal(_dt.dto) > 0)
                {
                    //Pro_qty * (Pro_precio_uni - Pro_bonificacion)
                    decimal _bonif = (Convert.ToDecimal(_dt.dto) * Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture)) / 100;
                    decimal _totBonif = _bonif * Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture);
                    _dt.dto = Math.Round(_totBonif, 2).ToString().Replace(',', '.');
                }
                //Vemos el dto comercial
                if (facturaExpo.porcentajeDtoComercial > 0)
                {
                    decimal _precioComercial = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _bonifComercial = _precioComercial * facturaExpo.porcentajeDtoComercial / 100;
                    decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _totalBonifComercial = _dto + _bonifComercial;
                    _dt.dto = Math.Round(_totalBonifComercial, 2).ToString().Replace(',', '.');
                }
                //Vemos el dto PP
                if (facturaExpo.porcentajeDtoPp > 0)
                {
                    decimal _precioPp = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _bonifPp = _precioPp * facturaExpo.porcentajeDtoPp / 100;
                    decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _totalBonifComercial = _dto + _bonifPp;
                    _dt.dto = Math.Round(_totalBonifComercial, 2).ToString().Replace(',', '.');
                }
                decimal _importeTotal = Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture) * (Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture));
                _dt.importe = Math.Round(_importeTotal, 2).ToString().Replace(',', '.');

                _det.Add(_dt);
            }

            return _det;
        }

        private List<AfipDll.wsAfipExpo.Detalle> CargoDetallesExpoNew(List<DataAccess.DatosItemsExpo> items,
            DataAccess.DatosFacturaExpo facturaExpo)
        {
            List<AfipDll.wsAfipExpo.Detalle> _det = new List<AfipDll.wsAfipExpo.Detalle>();
            //Recorro la lista.
            foreach (DataAccess.DatosItemsExpo _rw in items)
            {

                AfipDll.wsAfipExpo.Detalle _dt = new AfipDll.wsAfipExpo.Detalle();
                _dt.cant = _rw.unidades.ToString().Replace(',', '.');
                _dt.cod = _rw.referencia.ToString();
                _dt.descrip = _rw.descripcion.ToString();
                _dt.importe = Math.Round(Convert.ToDecimal(_rw.total, System.Globalization.CultureInfo.InvariantCulture), 2).ToString().Replace(',', '.');
                _dt.preciounit = Math.Round(Convert.ToDecimal(_rw.precio, System.Globalization.CultureInfo.InvariantCulture), 2).ToString().Replace(',', '.');
                _dt.unimed = _rw.codigoaduana.ToString();
                _dt.dto = _rw.dto.ToString().Replace(',', '.');

                if (Convert.ToDecimal(_dt.dto) > 0)
                {
                    //Pro_qty * (Pro_precio_uni - Pro_bonificacion)
                    decimal _bonif = (Convert.ToDecimal(_dt.dto) * Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture)) / 100;
                    decimal _totBonif = _bonif * Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture);
                    _dt.dto = Math.Round(_totBonif, 2).ToString().Replace(',', '.');
                }
                //Vemos el dto comercial
                if (facturaExpo.porcentajeDtoComercial > 0)
                {
                    decimal _precioComercial = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _bonifComercial = _precioComercial * facturaExpo.porcentajeDtoComercial / 100;
                    decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _totalBonifComercial = _dto + _bonifComercial;
                    _dt.dto = Math.Round(_totalBonifComercial, 2).ToString().Replace(',', '.');
                }
                //Vemos el dto PP
                if (facturaExpo.porcentajeDtoPp > 0)
                {
                    decimal _precioPp = Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _bonifPp = _precioPp * facturaExpo.porcentajeDtoPp / 100;
                    decimal _dto = Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                    decimal _totalBonifComercial = _dto + _bonifPp;
                    _dt.dto = Math.Round(_totalBonifComercial, 2).ToString().Replace(',', '.');
                }
                decimal _importeTotal = (Convert.ToDecimal(_dt.cant, System.Globalization.CultureInfo.InvariantCulture) * Convert.ToDecimal(_dt.preciounit, System.Globalization.CultureInfo.InvariantCulture)) - Convert.ToDecimal(_dt.dto, System.Globalization.CultureInfo.InvariantCulture);
                _dt.importe = Math.Round(_importeTotal, 2).ToString().Replace(',', '.');

                _det.Add(_dt);
            }

            return _det;
        }

        private List<AfipDll.wsExportacion.Cmp_asoc> ArmarCbteAsocExpo(DataAccess.DatosFacturaExpo fc)
        {
            List<AfipDll.wsExportacion.Cmp_asoc> _lst = new List<AfipDll.wsExportacion.Cmp_asoc>();
            try
            {
                using (SqlConnection _conexion = new SqlConnection(strConnectionString))
                {
                    List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.numSerie, fc.numFactura, fc.N, _conexion);

                    foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                    {
                        AfipDll.wsAfip.ComprobanteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsoc(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _conexion);

                        if (_cls.Cuit != null)
                        {                            
                            AfipDll.wsExportacion.Cmp_asoc _caEx = new AfipDll.wsExportacion.Cmp_asoc();
                            _caEx.Cbte_cuit = Convert.ToInt64(_cls.Cuit);
                            _caEx.Cbte_nro = _cls.Nro;
                            _caEx.Cbte_punto_vta = Convert.ToInt16(_cls.PtoVta);
                            _caEx.Cbte_tipo = Convert.ToInt16(_cls.Tipo);
                            bool _exist = _lst.Any(c => c.Cbte_punto_vta == _caEx.Cbte_punto_vta && c.Cbte_nro == _caEx.Cbte_nro);
                            if (!_exist)
                                _lst.Add(_caEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
        #endregion

        #region Empresas
        private void cbEmpresas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataAccess.Empresas vrow = (DataAccess.Empresas)cbEmpresas.SelectedItem;

                _codigoEmpresa = vrow.codigoEmpresa;
                strCUIT = vrow.cuitEmpresa.Replace("-","");
                _nombreCertificado = vrow.nombreCertificado;
                _razonSocial = vrow.razonSocial;
                _cbu = vrow.CBU;
                _password = vrow.Password;

                grdGrid.SetDataBinding(null, "");

                ValidarCarpetas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error seleccionando la Empresa: " + ex.Message);
            }
        }

        private void LoadEmpresas()
        {
            try
            {
                List<DataAccess.Empresas> _empresas = DataAccess.Empresas.GetEmpresas(strConnectionString);
                cbEmpresas.DataSource = _empresas;
                cbEmpresas.DisplayMember = "razonSocial";
                cbEmpresas.ValueMember = "codigoEmpresa";
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error cargando las Empresas: " + ex.Message);
            }

        }

        #endregion

        private void gbSeleccion_Enter(object sender, EventArgs e)
        {

        }
    }
}
