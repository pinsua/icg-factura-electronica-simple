﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using AfipDll;

namespace IcgPlugInFacturacion
{
    public class DataAccess
    {
        public class TiposDoc
        {
            public int TipoDoc { get; set; }
            public string Descripcion { get; set; }

            public static List<TiposDoc> GetTiposDoc(string _connexion)
            {
                //Instanciamos la conexion.
                string strSql = "SELECT DISTINCT TIPOSDOC.TIPODOC, TIPOSDOC.DESCRIPCION FROM TIPOSDOC, SERIESCAMPOSLIBRES, SERIESDOC "+
                    "WHERE SERIESCAMPOSLIBRES.SERIE = SERIESDOC.SERIE AND SERIESDOC.TIPODOC = TIPOSDOC.TIPODOC AND SERIESCAMPOSLIBRES.ELECTRONICA = 'T' "+
                    "ORDER BY DESCRIPCION";
                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<TiposDoc> _TipoDoc = new List<TiposDoc>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TiposDoc _td = new TiposDoc();
                                _td.TipoDoc = Convert.ToInt32(reader["TipoDoc"]);
                                _td.Descripcion = reader["Descripcion"].ToString();
                                _TipoDoc.Add(_td);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _TipoDoc;
            }
        }

        public class Clientes
        {
            public int CodCliente { get; set; }
            public string NombreCliente { get; set; }

            public static List<Clientes> GetClientes(string _connexion)
            {
                //Instanciamos la conexion.
                string strSql = "select CODCLIENTE, NOMBRECLIENTE from CLIENTES";
                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Clientes> _Clientes = new List<Clientes>();
                
                try
                {
                    _Connection.Open();
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Clientes _Cliente = new Clientes();
                                _Cliente.CodCliente = Convert.ToInt32(reader["CODCLIENTE"]);
                                _Cliente.NombreCliente = reader["NOMBRECLIENTE"].ToString();
                                _Clientes.Add(_Cliente);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Clientes;
            }

        }

        public class Facturas
        {
            public string NumSerie { get; set; }
            public int NumFactura { get; set; }
            public string N { get; set; }
            public DateTime Fecha { get; set; }
            public string NombreCliente { get; set; }
            public decimal TotalBruto {get;set;}
            public decimal TotalNeto {get;set;}
            public decimal TotalImpuestos { get; set; }
            public string DescTipoDoc { get; set; }            
            public int NumeroFiscal { get; set; }
            public string SerieFiscal { get; set; }
            public string Estado { get; set; }
            public string TipoDoc { get; set; }
            public string PuntoVenta { get; set; }
            public string Cae { get; set; }
            public string ErrorCae { get; set; }
            public string ClienteDoc { get; set; }
            public string ClienteNroDoc { get; set; }
            public string DescripMoneda { get; set; }    
            public string EXEPCION_COMPUTO_IVA { get; set; }
            public string NcRechazada { get; set; }

            public static List<Facturas> GetFacturas(string _feDesde, string _feHasta, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, string _connexion)
            {
                //Seteamos el formato americano para ver si así trae las FC.
                //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);
                //Instanciamos la conexion.

                string strSql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, "+
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                    "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, "+
                    "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, "+
                    "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA " +
                    "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE "+
	                "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE "+
	                "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC "+
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA "+
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
	                "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE "+
		            "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA "+
		            "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N "+
	                "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA "+
		            "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N "+
		            "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE "+
                    "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                    "AND FACTURASVENTA.FECHA >= '" + _feDesde + "' AND FACTURASVENTA.FECHA <= '" + _feHasta + "'";

                if(!String.IsNullOrEmpty(strTiposDoc))
                    strSql = strSql + " AND FACTURASVENTA.TIPODOC in("+strTiposDoc+ ")";
                if(!String.IsNullOrEmpty(strClientes))
                    strSql = strSql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Facturas> _Facturas = new List<Facturas>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Facturas _fc = new Facturas();
                                _fc.Cae = reader["Cae"].ToString();
                                _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                _fc.ErrorCae = reader["Error_Cae"].ToString();
                                
                                _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                _fc.N = reader["N"].ToString();
                                _fc.NombreCliente = reader["NombreCliente"].ToString();
                                if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                    _fc.NumeroFiscal = 0;
                                else
                                    _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                _fc.NumSerie = reader["NumSerie"].ToString();
                                _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                _fc.Estado = reader["ESTADO_FE"].ToString();
                                _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();

                                //if (String.IsNullOrEmpty(_fc.Cae))
                                //{
                                //    if (String.IsNullOrEmpty(_fc.ErrorCae))
                                //        _fc.Estado = "Pendiente";
                                //    else
                                //        _fc.Estado = "Error";
                                //}
                                //else
                                //    _fc.Estado = "Facturado";

                                _Facturas.Add(_fc);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Facturas;
            }

            public static List<Facturas> GetFacturas(DateTime _feDesde, DateTime _feHasta, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, string _connexion)
            {

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);
                
                string strSql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, " +
                    "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                    "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                    "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, " +
                    "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA " +
                    "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                    "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                    "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                    "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                    "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                    "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                    "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N " +
                    "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                    "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                    "AND FACTURASVENTA.FECHA >= @desde  AND FACTURASVENTA.FECHA <= @hasta";

                if (!String.IsNullOrEmpty(strTiposDoc))
                    strSql = strSql + " AND FACTURASVENTA.TIPODOC in(" + strTiposDoc + ")";
                if (!String.IsNullOrEmpty(strClientes))
                    strSql = strSql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                List<Facturas> _Facturas = new List<Facturas>();

                using (SqlConnection _Connection = new SqlConnection(_connexion))
                {
                    using (SqlCommand _Command = new SqlCommand(strSql))
                    {
                        _Command.Connection = _Connection;
                        _Command.CommandType = System.Data.CommandType.Text;
                        _Command.Parameters.AddWithValue("@desde", _feDesde.Date);
                        _Command.Parameters.AddWithValue("@hasta", _feHasta.Date);
                        
                        try
                        {
                            _Connection.Open();

                            using (SqlDataReader reader = _Command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        Facturas _fc = new Facturas();
                                        _fc.Cae = reader["Cae"].ToString();
                                        _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                        _fc.ErrorCae = reader["Error_Cae"].ToString();

                                        _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                        _fc.N = reader["N"].ToString();
                                        _fc.NombreCliente = reader["NombreCliente"].ToString();
                                        if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                            _fc.NumeroFiscal = 0;
                                        else
                                            _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                        _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                        _fc.NumSerie = reader["NumSerie"].ToString();
                                        _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                        //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                        _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                        _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                        _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                        _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                        _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                        _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                        _fc.Estado = reader["ESTADO_FE"].ToString();
                                        _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();

                                        _Facturas.Add(_fc);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                return _Facturas;
            }

            public static List<Facturas> GetFacturas(DateTime _feDesde, DateTime _feHasta, int _empresa, List<TiposDoc> _lstDoc, List<Clientes> _lstCli, bool _tieneComputoIva, string _connexion)
            {

                string strTiposDoc = GetStringForTiposDoc(_lstDoc);
                string strClientes = GetStringForCliente(_lstCli);
                string _sql;

                if (_tieneComputoIva)
                {
                    _sql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, " +
                        "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, " +
                        "FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                        "FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, " +
                        "FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA, " +
                        "FACTURASVENTACAMPOSLIBRES.EXEPCION_COMPUTO_IVA, FACTURASVENTACAMPOSLIBRES.NC_RECHAZADA " +
                        "FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE " +
                        "INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                        "INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                        "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                        "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                        "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                        "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA " +
                        "AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                        "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                        "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N " +
                        "AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                        "WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' " +
                        "AND FACTURASVENTA.ENLACE_EMPRESA = @Empresa " +
                        "AND FACTURASVENTA.FECHA >= @desde  AND FACTURASVENTA.FECHA <= @hasta";
                }
                else
                {
                    _sql = @"SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, CLIENTES.NOMBRECLIENTE, 
                    (FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL -FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, 
                    FACTURASVENTA.TOTALNETO, FACTURASVENTA.TOTALIMPUESTOS, TIPOSDOC.DESCRIPCION as DESCTIPODOC, SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, 
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE, CLIENTESCAMPOSLIBRES.TIPO_DOC as CLIENTEDOC, CLIENTES.NIF20, 
                    FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTASERIESRESOL.NUMEROFISCAL, MONEDAS.DESCRIPCION as DESCRIPMONEDA, 
                    null as EXEPCION_COMPUTO_IVA, FACTURASVENTACAMPOSLIBRES.NC_RECHAZADA 
                    FROM FACTURASVENTA inner Join CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE 
                    INNER JOIN CLIENTESCAMPOSLIBRES ON CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC 
                    INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA 
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE 
                    INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE 
                    AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA 
                    AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N 
                    LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA 
                    AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N 
                    AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE 
                    WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND  FACTURASVENTA.N = 'B' 
                    AND FACTURASVENTA.ENLACE_EMPRESA = @Empresa 
                    AND FACTURASVENTA.FECHA >= @desde  AND FACTURASVENTA.FECHA <= @hasta";
                }

                if (!String.IsNullOrEmpty(strTiposDoc))
                    _sql = _sql + " AND FACTURASVENTA.TIPODOC in(" + strTiposDoc + ")";
                if (!String.IsNullOrEmpty(strClientes))
                    _sql = _sql + " AND FACTURASVENTA.CODCLIENTE in(" + strClientes + ")";

                List<Facturas> _Facturas = new List<Facturas>();

                using (SqlConnection _Connection = new SqlConnection(_connexion))
                {
                    using (SqlCommand _Command = new SqlCommand(_sql))
                    {
                        _Command.Connection = _Connection;
                        _Command.CommandType = System.Data.CommandType.Text;
                        _Command.Parameters.AddWithValue("@Empresa", _empresa);
                        _Command.Parameters.AddWithValue("@desde", _feDesde.Date);
                        _Command.Parameters.AddWithValue("@hasta", _feHasta.Date);

                        try
                        {
                            _Connection.Open();

                            using (SqlDataReader reader = _Command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        Facturas _fc = new Facturas();
                                        _fc.Cae = reader["Cae"].ToString();
                                        _fc.DescTipoDoc = reader["DescTipoDoc"].ToString();
                                        _fc.ErrorCae = reader["Error_Cae"].ToString();

                                        _fc.Fecha = Convert.ToDateTime(reader["Fecha"]);
                                        _fc.N = reader["N"].ToString();
                                        _fc.NombreCliente = reader["NombreCliente"].ToString();
                                        if (String.IsNullOrEmpty(reader["NumeroFiscal"].ToString()))
                                            _fc.NumeroFiscal = 0;
                                        else
                                            _fc.NumeroFiscal = Convert.ToInt32(reader["NumeroFiscal"]);
                                        _fc.NumFactura = Convert.ToInt32(reader["NumFactura"]);
                                        _fc.NumSerie = reader["NumSerie"].ToString();
                                        _fc.PuntoVenta = reader["Punto_de_Venta"].ToString();
                                        //_fc.SerieFiscal = reader["SerieFiscal1"].ToString();
                                        _fc.TipoDoc = reader["Tipo_Doc"].ToString();
                                        _fc.TotalBruto = Convert.ToDecimal(reader["TotalBruto"]);
                                        _fc.TotalImpuestos = Convert.ToDecimal(reader["TotalImpuestos"]);
                                        _fc.TotalNeto = Convert.ToDecimal(reader["TotalNeto"]);
                                        _fc.ClienteDoc = reader["CLIENTEDOC"].ToString();
                                        _fc.ClienteNroDoc = reader["NIF20"].ToString();
                                        _fc.Estado = reader["ESTADO_FE"].ToString();
                                        _fc.DescripMoneda = reader["DESCRIPMONEDA"].ToString();
                                        _fc.EXEPCION_COMPUTO_IVA = reader["EXEPCION_COMPUTO_IVA"].ToString();
                                        _fc.NcRechazada = reader["NC_RECHAZADA"].ToString();

                                        _Facturas.Add(_fc);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                }
                return _Facturas;
            }

            private static string GetStringForTiposDoc(List<TiposDoc> _lstDoc)
            {
                string strRta = "";
                foreach (TiposDoc td in _lstDoc)
                {
                    if (String.IsNullOrEmpty(strRta))
                        strRta = td.TipoDoc.ToString();
                    else
                        strRta = strRta + ", " + td.TipoDoc.ToString();
                }
                return strRta;
            }

            private static string GetStringForCliente(List<Clientes> _lstCli)
            {
                string strRta = "";
                foreach (Clientes cli in _lstCli)
                {
                    if (String.IsNullOrEmpty(strRta))
                        strRta = cli.CodCliente.ToString();
                    else
                        strRta = strRta + ", " + cli.CodCliente.ToString();
                }
                return strRta;
            }

            public static bool ValidoCAE(string _N, string _NumSerie, int _NumFactura, string _stringConnection)
            {
                bool booRta = false;
                //object objRta;

                string strSql = "SELECT CAE FROM FACTURASVENTACAMPOSLIBRES WHERE N = @B AND NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura";

                //SqlConnection _connection = new SqlConnection(_stringConnection);
                //SqlCommand _command = new SqlCommand(strSql);
                //_command.Connection = _connection;
                //_command.CommandType = System.Data.CommandType.Text;
                //_command.CommandText = strSql;

                //_command.Parameters.AddWithValue("@B", _N);
                //_command.Parameters.AddWithValue("@NumSerie", _NumSerie);
                //_command.Parameters.AddWithValue("@NumFactura", _NumFactura);
                try
                {
                    using (SqlConnection _connection = new SqlConnection(_stringConnection))
                    {
                        //Abrimos la conexion.
                        _connection.Open();
                        using (SqlCommand _command = new SqlCommand(strSql))
                        {
                            _command.Connection = _connection;
                            _command.CommandType = System.Data.CommandType.Text;
                            _command.CommandText = strSql;

                            _command.Parameters.AddWithValue("@B", _N);
                            _command.Parameters.AddWithValue("@NumSerie", _NumSerie);
                            _command.Parameters.AddWithValue("@NumFactura", _NumFactura);
                            int count = 0;
                            using (SqlDataReader reader = _command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        count++;
                                        if(booRta == false)
                                        booRta = String.IsNullOrEmpty(reader["CAE"].ToString()) ? false : true;
                                    }
                                }
                            }

                            if (count > 1)
                                booRta = true;
                            //objRta = _command.ExecuteScalar();

                            //if (objRta == DBNull.Value)
                            //    objRta = false;
                            //else
                            //    objRta = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    booRta = true;
                }
                //finally
                //{
                //    if (_connection.State == System.Data.ConnectionState.Open)
                //        _connection.Close();
                //}

                return booRta;
            }

            public static string GetEstadoFC(string _N, string _NumSerie, int _NumFactura, string _pathLog, string _stringConnection)
            {
                string rta = "";
                
                string strSql = "SELECT ESTADO_FE FROM FACTURASVENTACAMPOSLIBRES WHERE N = @B AND NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura";

                try
                {
                    using (SqlConnection _connection = new SqlConnection(_stringConnection))
                    {
                        //Abrimos la conexion.
                        _connection.Open();
                        using (SqlCommand _command = new SqlCommand(strSql))
                        {
                            _command.Connection = _connection;
                            _command.CommandType = System.Data.CommandType.Text;
                            _command.CommandText = strSql;

                            _command.Parameters.AddWithValue("@B", _N);
                            _command.Parameters.AddWithValue("@NumSerie", _NumSerie);
                            _command.Parameters.AddWithValue("@NumFactura", _NumFactura);
                            int count = 0;
                            using (SqlDataReader reader = _command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        count++;
                                        rta = String.IsNullOrEmpty(reader["ESTADO_FE"].ToString()) ? rta = "NO FISCALIZAR" : rta = reader["ESTADO_FE"].ToString();
                                    }
                                }
                            }

                            if (count > 1)
                                rta = "NO FISCALIZAR";                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Error GetEstadoFC. Revisar la consistencia de los datos. ERROR: " + ex.Message);
                    rta = "NO FISCALIZAR";
                }


                return rta;
            }
        }

        public class FacturasVentasTot
        {
            public decimal Iva { get; set; }
            public decimal BaseImponible { get; set; }
            public decimal TotIva { get; set; }
            public decimal Alicuota { get; set; }
            public string Nombre { get; set; }

            public static List<FacturasVentasTot> GetTotalesIva(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE IVA >= 0 AND CODDTO = @CodDto AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //string strSql = "SELECT IMPUESTOS.DESCRIPCION, FACTURASVENTATOT.BASEIMPONIBLE, FACTURASVENTATOT.IVA, FACTURASVENTATOT.TOTIVA " +
                //    "FROM FACTURASVENTATOT INNER JOIN IMPUESTOS ON FACTURASVENTATOT.TIPOIVA = IMPUESTOS.TIPOIVA " +
                //    "WHERE FACTURASVENTATOT.IVA >= 0 AND FACTURASVENTATOT.CODDTO = @CodDto AND FACTURASVENTATOT.SERIE = @Serie AND FACTURASVENTATOT.NUMERO = @Numero AND FACTURASVENTATOT.N = @N";
                //Instanciamos la conexion.                
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TOTIVA"]);
                                fct.Nombre = "IVA"; //reader["DESCRIPCION"].ToString();
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesTributos(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion, string _Neto)
            {
                //string strSql = "SELECT IVA, IMPORTE, 0 as TOTIVA FROM FACTURASVENTADTOS WHERE CODDTO = @CodDto AND NUMSERIE = @Serie AND NUMERO = @Numero AND N = @N";
                string strSql = @"SELECT FACTURASVENTADTOS.IVA, FACTURASVENTADTOS.IMPORTE, 0 as TOTIVA, FACTURASVENTADTOS.DTOCARGO, CARGOSDTOS.NOMBRE, FACTURASVENTADTOS.BASE
                    FROM FACTURASVENTADTOS inner join CARGOSDTOS on FACTURASVENTADTOS.CODDTO = CARGOSDTOS.CODIGO
                    WHERE CARGOSDTOS.CODIGOFISCAL = @CodDto AND
                    FACTURASVENTADTOS.NUMSERIE = @Serie AND FACTURASVENTADTOS.NUMERO = @Numero AND FACTURASVENTADTOS.N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();

                decimal _neto = Convert.ToDecimal(_Neto);
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                //fct.BaseImponible = _neto;
                                fct.BaseImponible = Convert.ToDecimal(reader["BASE"]);
                                //fct.Iva = _neto / Convert.ToDecimal(reader["IMPORTE"]);
                                fct.Iva = fct.BaseImponible / Convert.ToDecimal(reader["IMPORTE"]);
                                fct.TotIva = Convert.ToDecimal(reader["IMPORTE"]);
                                fct.Alicuota = Convert.ToDecimal(reader["DTOCARGO"]);
                                fct.Nombre = reader["NOMBRE"].ToString();
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetTotalesNoGravado(string _Serie, string _N, string _Numero, int _CodDto, string _Connexion)
            {
                string strSql = "SELECT IVA, BASEIMPONIBLE, TOTIVA FROM FACTURASVENTATOT WHERE CODDTO = @CodDto AND IVA = 0 AND SERIE = @Serie AND NUMERO = @Numero AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                _Command.Parameters.AddWithValue("@CodDto", _CodDto);
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["IVA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TotIva"]);
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }

            public static List<FacturasVentasTot> GetImpuestosInternos(string _Serie, string _N, string _Numero, string _Connexion)
            {
                string strSql = @"select FACTURASVENTATOT.BASEIMPONIBLE, FACTURASVENTATOT.REQ as ALICUOTA, FACTURASVENTATOT.TOTREQ as TOTAL, IMPUESTOS.DESCRIPCION
                    from FACTURASVENTATOT inner join IMPUESTOS on FACTURASVENTATOT.TIPOIVA = IMPUESTOS.TIPOIVA
                    where FACTURASVENTATOT.NUMERO = @Numero and FACTURASVENTATOT.SERIE = @Serie and FACTURASVENTATOT.N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                _Command.Parameters.AddWithValue("@Serie", _Serie);
                _Command.Parameters.AddWithValue("@Numero", _Numero);
                _Command.Parameters.AddWithValue("@N", _N);

                List<FacturasVentasTot> _Iva = new List<FacturasVentasTot>();
                _Connection.Open();

                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                FacturasVentasTot fct = new FacturasVentasTot();
                                fct.BaseImponible = Convert.ToDecimal(reader["BASEIMPONIBLE"]);
                                fct.Iva = Convert.ToDecimal(reader["ALICUOTA"]);
                                fct.TotIva = Convert.ToDecimal(reader["TOTAL"]);
                                fct.Alicuota = Convert.ToDecimal(reader["ALICUOTA"]);
                                fct.Nombre = reader["DESCRIPCION"].ToString();
                                _Iva.Add(fct);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _Iva;
            }
        }

        public class FacturasVentaCamposLibres: IDisposable
        {
            public string NumeroSerie {get;set;}
            public int NumeroFactura {get;set;}
            public string N {get;set;}
            public string Cae {get;set;}
            public string VtoCae{get;set;}
            public string CodBarras{get;set;}
            public string ErrorCae{get;set;}
            public string Estado { get; set; }
            public string qrEncoce { get; set; }

            public void Dispose()
            {
                //Dispose(true);
                GC.SuppressFinalize(this);
            }

            public bool UpdateFacturasVentasCamposLibres(FacturasVentaCamposLibres _FcLibres, string _PathLog, string _Connexion)
            {
                string strSql = "UPDATE FACTURASVENTACAMPOSLIBRES SET ";
                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                    strSql = strSql + "CAE = @CAE,  VTO_CAE = @VTOCAE, COD_BARRAS = @CODBARRAS, ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO, CODIGO_QR = @QR ";
                else
                    strSql = strSql + "ERROR_CAE = @ERRORCAE, ESTADO_FE = @ESTADO ";
                strSql = strSql +" WHERE NUMSERIE = @NUMSERIE AND NUMFACTURA = @NUMFACTURA AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;

                if (!String.IsNullOrEmpty(_FcLibres.Cae))
                {
                    _Command.Parameters.AddWithValue("@CAE", _FcLibres.Cae);
                    _Command.Parameters.AddWithValue("@VTOCAE", _FcLibres.VtoCae);
                    _Command.Parameters.AddWithValue("@CODBARRAS", _FcLibres.CodBarras);
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);
                    _Command.Parameters.AddWithValue("@QR", _FcLibres.qrEncoce);
                }
                else
                {                    
                    _Command.Parameters.AddWithValue("@ESTADO", _FcLibres.Estado);                                        
                }
                _Command.Parameters.AddWithValue("@ERRORCAE", _FcLibres.ErrorCae.Length > 100 ? _FcLibres.ErrorCae.Substring(0,100) : _FcLibres.ErrorCae);
                _Command.Parameters.AddWithValue("@NUMSERIE", _FcLibres.NumeroSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _FcLibres.NumeroFactura);
                _Command.Parameters.AddWithValue("@N", _FcLibres.N);
                try
                {
                    _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch(Exception ex) 
                {
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_PathLog), "Error FacturasVenasCamposLibre: " + ex.Message);
                    return false;
                }
            }

            public static FacturasVentaCamposLibres GetRecord(string _conexion, DatosFactura _cls)
            {
                string strSql = "SELECT CAE, VTO_CAE, COD_BARRAS, ERROR_CAE FROM FACTURASVENTACAMPOSLIBRES " +
                    "WHERE NUMSERIE = @NumSerie AND NUMFACTURA = @NumFactura AND N = @N";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_conexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA", _cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);                

                FacturasVentaCamposLibres fvcl = new FacturasVentaCamposLibres();

                try
                {
                    _Connection.Open();
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                fvcl.Cae = reader["Cae"].ToString();
                                fvcl.CodBarras = reader["Cod_Barras"].ToString();
                                fvcl.ErrorCae = reader["Error_Cae"].ToString();
                                fvcl.Estado = "";
                                fvcl.N = _cls.N;
                                fvcl.NumeroFactura = Convert.ToInt32(_cls.NumFactura);
                                fvcl.NumeroSerie = _cls.NumSerie;
                                fvcl.VtoCae = reader["Vto_Cae"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error buscando FacturasVentaCamposLibres. Error: " + ex.Message);
                }
                return fvcl;
            }
        }

        public class FacturasVentaSeriesResol: IDisposable
        {
            public string NumSerie { get; set; }
            public int NumFactura { get; set; }
            public string N { get; set; }
            public string SerieFiscal1 { get; set; }
            public string SerieFiscal2 { get; set; }
            public int NumeroFiscal { get; set; }

            public void Dispose()
            {
                //Dispose(true);
                GC.SuppressFinalize(this);
            }

            public static bool InsertFacturasVentaSeriesResol(FacturasVentaSeriesResol _cls, string _Conection)
            {
                string strSql = "INSERT INTO FACTURASVENTASERIESRESOL "+
                    "(NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES (@NUMSERIE, @NUMFACTURA, @N, @SERIEFISCAL1, @SERIEFISCAL2, @NUMEROFISCAL)";
                //Instanciamos la conexion.
                SqlConnection _Connection = new SqlConnection(_Conection);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                //Pasamos los parametros.
                _Command.Parameters.AddWithValue("@NUMSERIE", _cls.NumSerie);
                _Command.Parameters.AddWithValue("@NUMFACTURA",_cls.NumFactura);
                _Command.Parameters.AddWithValue("@N", _cls.N);
                _Command.Parameters.AddWithValue("@SERIEFISCAL1", _cls.SerieFiscal1);
                _Command.Parameters.AddWithValue("@SERIEFISCAL2", _cls.SerieFiscal2);
                _Command.Parameters.AddWithValue("@NUMEROFISCAL", _cls.NumeroFiscal);

                try
                {
                    _Connection.Open();
                    int intOK = _Command.ExecuteNonQuery();
                    if (intOK > 0)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public class DatosFactura
        {
            public string NumSerie { get; set; }
            public string NumFactura { get; set; }
            public string N { get; set; }
            public string TotalBruto { get; set; }
            public string TotalNeto { get; set; }
            public string TotalCosteIva { get; set; }
            public decimal TotIva { get; set; }
            public decimal totTributos { get; set; }
            public decimal totNoGravado { get; set; }
            public string cCodAfip { get; set; }
            public string strTipoDoc { get; set; }
            public string PtoVta { get; set; }
            public string ClienteTipoDoc { get; set; }
            public string ClienteNroDoc { get; set; }
            public string strFecha { get; set; }
            public string ExcepcionComputoIva { get; set; }
            public string NcRechazada { get; set; }
        }

        public class DatosFacturaExpo: IDisposable
        {
            public string numSerie { get; set; }
            public int numFactura { get; set; }
            public string N { get; set; }
            public DateTime fecha { get; set; }
            public int codCliente { get; set; }
            public decimal totalBruto { get; set; }
            public decimal totalNeto { get; set; }
            public string CodAfip { get; set; }
            public string TipoDoc { get; set; }
            public string PtoVta { get; set; }
            public decimal porcentajeDtoComercial { get; set; }
            public decimal totDtoComercial { get; set; }
            public decimal porcentajeDtoPp { get; set; }
            public decimal totDtoPp { get; set; }
            public string codigoMoneda { get; set; }
            public decimal cotizacion { get; set; }
            public string incoterms { get; set; }
            public DateTime fechaVto { get; set; }
            

            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }

            public static DatosFacturaExpo GetDatosFacturaExpo(string pnumSerie, string pnumFactura, string pN, string _Conection)
            {
                DatosFacturaExpo _dfe = new DatosFacturaExpo();

                try
                {

                    string _sql = "SELECT FACTURASVENTA.NUMSERIE, FACTURASVENTA.NUMFACTURA, FACTURASVENTA.N, FACTURASVENTA.FECHA, FACTURASVENTA.CODCLIENTE, " +
                        "(FACTURASVENTA.TOTALBRUTO - FACTURASVENTA.TOTDTOCOMERCIAL - FACTURASVENTA.TOTDTOPP) AS TOTALBRUTO, FACTURASVENTA.TOTALNETO, " +
                        "SERIESCAMPOSLIBRES.TIPO_DOC, SERIESCAMPOSLIBRES.PUNTO_DE_VENTA, " +
                        "FACTURASVENTA.DTOCOMERCIAL, FACTURASVENTA.DTOPP, FACTURASVENTA.TOTDTOCOMERCIAL, FACTURASVENTA.TOTDTOPP, " +
                        "MONEDAS.CODIGOISONUM, FACTURASVENTA.FACTORMONEDA, FACTURASVENTACAMPOSLIBRES.INCOTERMS, TESORERIA.FECHAVENCIMIENTO " +
                        "FROM FACTURASVENTA inner Join TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC " +
                        "INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE " +
                        "INNER JOIN FACTURASVENTACAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE " +
                        "AND FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA AND FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N " +
                        "LEFT JOIN FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA " +
                        "AND FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N AND FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE " +
                        "INNER JOIN MONEDAS ON FACTURASVENTA.CODMONEDA = MONEDAS.CODMONEDA " +
                        "INNER JOIN TESORERIA ON FACTURASVENTA.NUMFACTURA = TESORERIA.NUMERO AND FACTURASVENTA.N = TESORERIA.N AND FACTURASVENTA.NUMSERIE = TESORERIA.SERIE " +
                        "WHERE FACTURASVENTA.NUMSERIE = @NumSerie AND FACTURASVENTA.NUMFACTURA = @NumFactura AND FACTURASVENTA.N = @N";                    

                    using (SqlConnection _conn = new SqlConnection(_Conection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@NumSerie", pnumSerie);
                            _cmd.Parameters.AddWithValue("@NumFactura", pnumFactura);
                            _cmd.Parameters.AddWithValue("@N", pN);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _dfe.numSerie = reader.GetString(0);
                                        _dfe.numFactura = reader.GetInt32(1);
                                        _dfe.N = reader.GetString(2);
                                        _dfe.fecha = reader.GetDateTime(3);
                                        _dfe.codCliente = reader.GetInt32(4);
                                        _dfe.totalBruto = Math.Abs(Convert.ToDecimal(reader[5], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totalNeto = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.TipoDoc = reader[7] == null ? "" : reader.GetString(7);
                                        _dfe.PtoVta = reader[8] == null ? "" : reader.GetString(8);
                                        _dfe.porcentajeDtoComercial = Math.Abs(Convert.ToDecimal(reader[9], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.porcentajeDtoPp = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totDtoComercial = Math.Abs(Convert.ToDecimal(reader[11], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.totDtoPp = Math.Abs(Convert.ToDecimal(reader[12], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.codigoMoneda = reader[13] == null ? "" : reader.GetString(13);
                                        _dfe.cotizacion = Math.Abs(Convert.ToDecimal(reader[14], System.Globalization.CultureInfo.InvariantCulture));
                                        _dfe.incoterms = String.IsNullOrEmpty(reader[15].ToString()) ? "" : reader.GetString(15);
                                        _dfe.fechaVto = reader.GetDateTime(16);
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al recuperar los datos de la factura de exportación. Error: " + ex.Message);
                }
                return _dfe;
            }
        }

        public class DatosClienteExpo
        {
            public int _codCliente { get; set; }
            public string _razonSocial { get; set; }
            public string _cuit { get; set; }
            public string _domicilio { get; set; }
            public string _idImpositivo { get; set; }
            public string _tipoExpo { get; set; }
            public string _paisDestino { get; set; }

            public static DatosClienteExpo GetDatosClienteExpo(int pCodClinte, string _Conection)
            {
                string _sql = "SELECT CLIENTES.CODCLIENTE, CLIENTES.NOMBRECLIENTE, CLIENTES.NIF20, CLIENTES.DIRECCION1, CLIENTESCAMPOSLIBRES.TIPO_EXPO, PAISES.ZONA " +
                    "FROM CLIENTES inner join CLIENTESCAMPOSLIBRES on CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                    "INNER JOIN PAISES ON CLIENTES.CODPAIS = PAISES.CODPAIS " +
                    "WHERE CLIENTES.CODCLIENTE = @CodCliente";

                DatosClienteExpo _dfe = new DatosClienteExpo();

                try
                {
                    using (SqlConnection _conn = new SqlConnection(_Conection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@CodCliente", pCodClinte);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _dfe._codCliente = reader.GetInt32(0);
                                        _dfe._razonSocial = reader[1] == null ? "" : reader.GetString(1);
                                        _dfe._cuit = reader[2] == null ? "" : reader.GetString(2);
                                        _dfe._domicilio = reader[3] == null ? "" : reader.GetString(3);
                                        _dfe._tipoExpo = reader[4] == null ? "" : reader.GetString(4);
                                        _dfe._paisDestino = reader[5] == null ? "" : reader.GetString(5);
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception("Se produjo el siguiente error al recuperar los datos del cliente. Erro: " + ex.Message);
                }

                return _dfe;
            }
        }

        public class DatosItemsExpo
        {
            public string numserie { get; set; }
            public int numalbaran { get; set; }
            public string n { get; set; }
            public int numlin { get; set; }
            public string referencia { get; set; }
            public string descripcion { get; set; }
            public decimal unidades { get; set; }
            public decimal precio { get; set; }
            public decimal total { get; set; }
            public string codigoaduana { get; set; }
            public decimal dto { get; set; }

            public static List<DatosItemsExpo> GetDatosItemsExpo(string pnumserie, int pnumalbaran, string pn, string pconnection)
            {

                string _sql = "SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN, ALBVENTALIN.REFERENCIA, " +
                    "ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL AS UNID1, ALBVENTALIN.PRECIO, ALBVENTALIN.TOTAL, ARTICULOS.CODIGOADUANA, ALBVENTALIN.DTO " +
                    "FROM FACTURASVENTA INNER JOIN ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC and FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC and FACTURASVENTA.N = ALBVENTACAB.NFAC " +
                    "INNER JOIN ALBVENTALIN on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N " +
                    "INNER JOIN ARTICULOS on albventalin.codarticulo = articulos.codarticulo " +
                    "WHERE FACTURASVENTA.NUMSERIE = @NumSerie and FACTURASVENTA.NUMFACTURA = @NumAlbaran and FACTURASVENTA.N = @N";

                List<DatosItemsExpo> _dfe = new List<DatosItemsExpo>();
                try
                {
                    using (SqlConnection _conn = new SqlConnection(pconnection))
                    {
                        _conn.Open();

                        using (SqlCommand _cmd = new SqlCommand(_sql))
                        {
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            _cmd.Parameters.AddWithValue("@NumSerie", pnumserie);
                            _cmd.Parameters.AddWithValue("@NumAlbaran", pnumalbaran);
                            _cmd.Parameters.AddWithValue("@N", pn);

                            using (SqlDataReader reader = _cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        DatosItemsExpo _it = new DatosItemsExpo();

                                        _it.numserie = reader.GetString(0);
                                        _it.numalbaran = reader.GetInt32(1);
                                        _it.n = reader.GetString(2);
                                        _it.numlin = reader.GetInt32(3);
                                        _it.referencia = reader[4] == null ? "" : reader.GetString(4);
                                        _it.descripcion = reader[5] == null ? "" : reader.GetString(5);
                                        _it.unidades = Math.Abs(Convert.ToDecimal(reader[6], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.precio = Math.Abs(Convert.ToDecimal(reader[7], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.total = Math.Abs(Convert.ToDecimal(reader[8], System.Globalization.CultureInfo.InvariantCulture));
                                        _it.codigoaduana = reader[9] == null ? "" : reader[9].ToString();
                                        _it.dto = Math.Abs(Convert.ToDecimal(reader[10], System.Globalization.CultureInfo.InvariantCulture));

                                        _dfe.Add(_it);
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception("Error al recuperar los datos de los items de la factura. Error: " + ex.Message);
                }
                return _dfe;
            }
        }

        public class FuncionesVarias
        {
            public static bool GenerarCodigoBarra(string _Cuit, string _TipoComprobate, string _PtoVta, string _CAE, string _Vto, string _PathLog, out string _CodigoBarra)
            {
                bool _rta = false;
                //Armo el string de todo.
                int _tipoComprobante = Convert.ToInt32(_TipoComprobate);
                string strTodo = _Cuit + _tipoComprobante.ToString().PadLeft(3, '0') + _PtoVta.PadLeft(5, '0') + _CAE + _Vto;
                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_PathLog), "CUIT: " + _Cuit + ", TipoComprobante: " + _tipoComprobante.ToString().PadLeft(2, '0') + ", PtoVta: " + _PtoVta.PadLeft(5, '0') + ", CAE: " + _CAE + ", FVTO: " + _Vto);
                try
                {
                    int intTamaño = strTodo.Length;
                    int[] a = new int[intTamaño];
                    int pares = 0, impares = 0;
                    //Cargamos el array con los digitos.
                    for (int i = 0; i < intTamaño; i++)
                    {
                        a[i] = int.Parse(strTodo[i].ToString());
                    }
                    //Sumamos las posiciones pares y las impares por separado.
                    for (int i = 0; i < 10; i++)
                    {
                        if ((i % 2) == 0)
                            pares += a[i];
                        else
                            impares += a[i];
                    }
                    //Dividimos y sumamos pares
                    int intT = (impares / 3) + pares;
                    //Recupero el ultimo digito de la suma.
                    int intUlimoDigito = int.Parse(intT.ToString().Substring(intT.ToString().Length - 1, 1));
                    //Vemos si el ultimo digito es 0
                    if (intUlimoDigito == 0)
                        strTodo = strTodo + intUlimoDigito.ToString();
                    else
                    {
                        intUlimoDigito = 10 - intUlimoDigito;
                        strTodo = strTodo + intUlimoDigito.ToString();
                    }

                    _rta = true;
                }
                catch
                {
                    _rta = false;
                    strTodo = "";
                }
                _CodigoBarra = strTodo;

                return _rta;
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, string _Cae, string _Error, string _FeVto, 
                string _CodBarra, string _connectionString, string _PathLog)
            {
                try
                {
                    using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                    {
                        fcLib.Cae = _Cae;
                        fcLib.CodBarras = _CodBarra;
                        if (!String.IsNullOrEmpty(_Cae))
                        {
                            fcLib.ErrorCae = "";
                            fcLib.Cae = _Cae;
                            fcLib.Estado = "FACTURADO";
                            fcLib.VtoCae = _FeVto.Substring(6, 2) + "/" + _FeVto.Substring(4, 2) + "/" + _FeVto.Substring(0, 4);
                        }
                        else
                        {
                            fcLib.ErrorCae = _Error;
                            fcLib.Estado = "ERROR";
                            fcLib.VtoCae = "";
                        }

                        fcLib.N = _N;
                        fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                        fcLib.NumeroSerie = _NumSerie;

                        fcLib.UpdateFacturasVentasCamposLibres(fcLib, _connectionString, _PathLog);
                    }
                }
                catch (Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "ModificoFacturasVentaCampoLibres Error: " + ex.Message);
                    throw new Exception(ex.Message);
                }
            }

            public static void Insertar_FacturasVentaSerieResol(string _NumSerie, string _NumFactura, string _N, string _PtoVta, string _TipoDoc, 
                int _NroComprobante, string _connectionString, string _PathLog)
            {
                try
                {
                    using (DataAccess.FacturasVentaSeriesResol _fcResol = new DataAccess.FacturasVentaSeriesResol())
                    {
                        _fcResol.NumSerie = _NumSerie;
                        _fcResol.NumFactura = Convert.ToInt32(_NumFactura);
                        _fcResol.N = _N;
                        _fcResol.SerieFiscal1 = _PtoVta;
                        _fcResol.SerieFiscal2 = _TipoDoc;
                        _fcResol.NumeroFiscal = _NroComprobante;

                        DataAccess.FacturasVentaSeriesResol.InsertFacturasVentaSeriesResol(_fcResol, _connectionString);
                    }
                }
                catch (Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Insertar_FacturasVentaSerieResol Error: " + ex.Message);
                    throw new Exception(ex.Message);
                }
            }

            public static void ModificoFacturasVentaCampoLibres(string _NumSerie, string _NumFactura, string _N, int _NroComprobante, string _Error, 
                string _connectionString, string _PathLog)
            {
                try
                {
                    using (DataAccess.FacturasVentaCamposLibres fcLib = new DataAccess.FacturasVentaCamposLibres())
                    {
                        fcLib.Cae = "";
                        fcLib.CodBarras = _NroComprobante.ToString();
                        fcLib.ErrorCae = "";
                        fcLib.Estado = "RECUPERAR";
                        fcLib.ErrorCae = _Error;
                        fcLib.VtoCae = "";
                        fcLib.N = _N;
                        fcLib.NumeroFactura = Convert.ToInt32(_NumFactura);
                        fcLib.NumeroSerie = _NumSerie;

                        fcLib.UpdateFacturasVentasCamposLibres(fcLib, _PathLog, _connectionString);
                    }
                }
                catch(Exception ex)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "ModificoFacturasVentaCampoLibres Error: " + ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }

        public class Empresas
        {
            public int codigoEmpresa { get; set; }
            public string nombreEmpresa { get; set; }
            public string razonSocial { get; set; }
            public string cuitEmpresa { get; set; }
            public string nombreCertificado { get; set; }
            public string CBU { get; set; }
            public string Password { get; set; }

            public static List<Empresas> GetEmpresas(string _connexion)
            {
                string strSql = @"select c.Codigo as Empresa, c.Descripcion as Nombre, c.NombreComercial as RazonSocial, 
                    c.NIF as CUIT, c.Web as Web, c.LOCALIDADREGISTRO as CBU, c.IDENTIFICACIONACREEDOR as Pass
                    from[GENERAL].[dbo].[EmpresasContables]
                    c join(select Codigo, max(Ejercicio) Ejercicio from[GENERAL].[dbo].[EmpresasContables] group by Codigo
                    ) m on m.Codigo = c.Codigo and m.Ejercicio = c.Ejercicio";

                SqlConnection _Connection = new SqlConnection(_connexion);
                SqlCommand _Command = new SqlCommand(strSql);
                _Command.Connection = _Connection;
                List<Empresas> _lst = new List<Empresas>();
                _Connection.Open();
                try
                {
                    using (SqlDataReader reader = _Command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Empresas _rw = new Empresas();

                                _rw.codigoEmpresa = reader.GetInt32(0);
                                //_rw.codigoEmpresa = 1;
                                _rw.nombreEmpresa = reader["Nombre"] == DBNull.Value ? "" : reader.GetString(1);
                                _rw.razonSocial = reader["RazonSocial"] == DBNull.Value ? "" : reader.GetString(2);
                                _rw.cuitEmpresa = reader["CUIT"] == DBNull.Value ? "" : reader.GetString(3);
                                _rw.nombreCertificado = reader["Web"] == DBNull.Value ? "" : reader.GetString(4);
                                _rw.CBU = reader["CBU"] == DBNull.Value ? "" : reader.GetString(5);
                                _rw.Password = reader["Pass"] == DBNull.Value ? "" : reader.GetString(6);

                                _lst.Add(_rw);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (_Connection.State == System.Data.ConnectionState.Open)
                        _Connection.Close();
                }
                return _lst;
            }
        }

        public class ComprobanteAsociado
        {
            public string numSerie { get; set; }
            public int numAlbaran { get; set; }
            public string n { get; set; }

            public static List<ComprobanteAsociado> GetComprobantesAsociados(string _numserie, int _numfac, string _n, SqlConnection _conexion)
            {
                string _sql = @"select distinct ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, ALBVENTALIN.ABONODE_N
                    from ALBVENTACAB inner join albventalin on ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN 
                    and ALBVENTACAB.N = ALBVENTALIN.N WHERE ALBVENTACAB.NUMSERIEFAC = @NumSerie AND ALBVENTACAB.NUMFAC = @NumFac AND ALBVENTACAB.NFAC = @N";

                List<ComprobanteAsociado> _lst = new List<ComprobanteAsociado>();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    if (_conexion.State == System.Data.ConnectionState.Closed)
                        _conexion.Open();

                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFac", _numfac);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    ComprobanteAsociado _cls = new ComprobanteAsociado();
                                    _cls.n = reader["ABONODE_N"].ToString();
                                    _cls.numAlbaran = Convert.ToInt32(reader["ABONODE_NUMALBARAN"]);
                                    _cls.numSerie = reader["ABONODE_NUMSERIE"].ToString();

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _lst;
            }

            public static AfipDll.wsAfip.ComprobanteAsoc GetAfipComprobanteAsoc(string _numserie, int _numfac, string _n, string _cuit, SqlConnection _conexion)
            {
                string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

                AfipDll.wsAfip.ComprobanteAsoc _cls = new AfipDll.wsAfip.ComprobanteAsoc();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["FECHA"]);
                                    
                                    _cls.CbteFch = _fech.Year.ToString() + _fech.Month.ToString().PadLeft(2, '0') + _fech.Day.ToString().PadLeft(2, '0');
                                    _cls.Cuit = _cuit;
                                    _cls.Nro = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    _cls.PtoVta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.Tipo = Convert.ToInt32(reader["DESCRIPCION"].ToString().Substring(0, 3));

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _cls;
            }

            public static AfipDll.wsMtxca.ComprobanteAsociadoType GetAfipComprobanteAsocMTXCA(string _numserie, int _numfac, string _n, string _cuit, int _tipoComprobante, SqlConnection _conexion)
            {
                string _sql = @"SELECT DISTINCT FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION
                    FROM ALBVENTACAB INNER JOIN FACTURASVENTA 
	                    ON ALBVENTACAB.NUMSERIEFAC = FACTURASVENTA.NUMSERIE AND ALBVENTACAB.NUMFAC = FACTURASVENTA.NUMFACTURA AND ALBVENTACAB.N = FACTURASVENTA.N
                    INNER JOIN FACTURASVENTASERIESRESOL 
	                    ON FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTA.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTA.NUMFACTURA AND FACTURASVENTASERIESRESOL.N = FACTURASVENTA.N 
                    INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE ALBVENTACAB.NUMSERIE = @numserie AND ALBVENTACAB.NUMALBARAN = @numfac AND ALBVENTACAB.N = @n";

                AfipDll.wsMtxca.ComprobanteAsociadoType _cls = new AfipDll.wsMtxca.ComprobanteAsociadoType();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _conexion;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //Parametros.
                    _cmd.Parameters.AddWithValue("@numserie", _numserie);
                    _cmd.Parameters.AddWithValue("@numfac", _numfac);
                    _cmd.Parameters.AddWithValue("@n", _n);

                    try
                    {
                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime _fech = Convert.ToDateTime(reader["FECHA"]);

                                    _cls.fechaEmision = _fech;
                                    _cls.fechaEmisionSpecified = true;
                                    if (_tipoComprobante == 203 || _tipoComprobante == 208 || _tipoComprobante == 213
                                            || _tipoComprobante == 202 || _tipoComprobante == 207 || _tipoComprobante == 212)
                                    {
                                        _cls.cuit = Convert.ToInt64(_cuit);
                                        _cls.cuitSpecified = true;
                                    }

                                    _cls.numeroComprobante = Convert.ToInt64(reader["NUMEROFISCAL"]);
                                    _cls.numeroPuntoVenta = Convert.ToInt32(reader["SERIEFISCAL1"]);
                                    _cls.codigoTipoComprobante = Convert.ToInt16(reader["DESCRIPCION"].ToString().Substring(0, 3));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                return _cls;
            }
        }
    }
}
