﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IcgPlugInFacturacion
{
    public partial class frmSearchTipoDoc : Form
    {
        public List<DataAccess.TiposDoc> TiposDocumentos = new List<DataAccess.TiposDoc>();
        public frmSearchTipoDoc()
        {
            InitializeComponent();
        }

        private void frmSearchTipoDoc_Load(object sender, EventArgs e)
        {
            List<DataAccess.TiposDoc> _tipodoc = DataAccess.TiposDoc.GetTiposDoc(frmMain.strConnectionString);
            grdTipoDoc.DataSource = _tipodoc;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            foreach (Janus.Windows.GridEX.GridEXRow rw in grdTipoDoc.GetCheckedRows())
            {
                DataAccess.TiposDoc _Doc = new DataAccess.TiposDoc();
                _Doc.TipoDoc = Convert.ToInt32(rw.Cells["TipoDoc"].Value);
                _Doc.Descripcion = rw.Cells["Descripcion"].Value.ToString();
                TiposDocumentos.Add(_Doc);
            }
            this.Close();
        }
    }
}
