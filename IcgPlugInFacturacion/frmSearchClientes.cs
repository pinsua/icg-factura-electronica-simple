﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace IcgPlugInFacturacion
{
    public partial class frmSearchClientes : Form
    {
        public List<DataAccess.Clientes> _Clientes;
        public List<DataAccess.Clientes> _Seleccion;
        public frmSearchClientes()
        {
            InitializeComponent();
        }

        private void frmSearchClientes_Load(object sender, EventArgs e)
        {
            _Clientes = DataAccess.Clientes.GetClientes(frmMain.strConnectionString);
            //grdClientes.DataSource = _Clientes;
            grdClientes.SetDataBinding(_Clientes, "");
            _Seleccion = new List<DataAccess.Clientes>();
            //grdSeleccion.DataSource = _Seleccion;            
            grdSeleccion.SetDataBinding(_Seleccion, "");
        }

        private void btnPasar_Click(object sender, EventArgs e)
        {
            foreach (Janus.Windows.GridEX.GridEXRow rw in grdClientes.GetRows())
            {
                if (rw.Selected)
                {
                    DataAccess.Clientes _cliente = new DataAccess.Clientes();
                    _cliente.CodCliente = Convert.ToInt32(rw.Cells["CodCliente"].Value);
                    _cliente.NombreCliente = rw.Cells["NombreCliente"].Value.ToString();
                    _Seleccion.Add(_cliente);
                    var item = _Clientes.SingleOrDefault(x => x.CodCliente == _cliente.CodCliente);
                    _Clientes.Remove(item);
                }
            }
            grdClientes.SetDataBinding(null, "");
            grdClientes.SetDataBinding(_Clientes, "");
            grdSeleccion.SetDataBinding(null, "");
            grdSeleccion.SetDataBinding(_Seleccion, "");
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            foreach (Janus.Windows.GridEX.GridEXRow rw in grdSeleccion.GetRows())
            {
                if (rw.Selected)
                {
                    DataAccess.Clientes _cliente = new DataAccess.Clientes();
                    _cliente.CodCliente = Convert.ToInt32(rw.Cells["CodCliente"].Value);
                    _cliente.NombreCliente = rw.Cells["NombreCliente"].Value.ToString();
                    _Clientes.Add(_cliente);
                    var item = _Seleccion.SingleOrDefault(x => x.CodCliente == _cliente.CodCliente);
                    _Seleccion.Remove(item);
                }
            }
            grdClientes.SetDataBinding(null, "");
            grdClientes.SetDataBinding(_Clientes, "");
            grdSeleccion.SetDataBinding(null, "");
            grdSeleccion.SetDataBinding(_Seleccion, "");
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            _Seleccion.Clear();
            this.Close();
        }
    }
}
