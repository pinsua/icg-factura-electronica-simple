﻿using System;
using System.Collections.Generic;
using System.Text;
using AfipDll;

namespace IcgPlugInFacturacion
{
    class wsAfip
    {
        /// <summary>
        /// Clase que define la cabecera.
        /// </summary>
        public class clsCabReq
        {
            /// <summary>
            /// Cantidad de facturas.
            /// </summary>
            public int CantReg
            {
                get;
                set;
            }
            /// <summary>
            /// Tipo de FC a imprimir.
            /// </summary>
            public int CbteTipo
            {
                get;
                set;
            }
            /// <summary>
            /// Punto de Venta del que se van imprimir las FC.
            /// </summary>
            public int PtoVta
            {
                get;
                set;
            }
        }

        public class clsDetReq
        {
            /// <summary>
            /// Concepto de la FC.
            /// </summary>
            public int Concepto
            {
                get;
                set;
            }
            /// <summary>
            /// Codigo del docuento del comprador.
            /// </summary>
            public int DocTipo
            {
                get;
                set;
            }
            /// <summary>
            /// Numero del documento del comprador.
            /// </summary>
            public long DocNro
            {
                get;
                set;
            }
            /// <summary>
            /// Nro. del comprobante desde.
            /// </summary>
            public long CbteDesde
            {
                get;
                set;
            }
            /// <summary>
            /// Nro. del comprobante hasta.
            /// </summary>
            public long CbteHasta
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha del comprobante.
            /// </summary>
            public string CbteFch
            {
                get;
                set;
            }
            /// <summary>
            /// Importe Total del comprobante.
            /// </summary>
            public double ImpTotal
            {
                get;
                set;
            }
            /// <summary>
            /// Importe noto no agravado.
            /// </summary>
            public double ImpTotConc
            {
                get;
                set;
            }
            /// <summary>
            /// Importe neto agravado.
            /// </summary>
            public double ImpNeto
            {
                get;
                set;
            }
            /// <summary>
            /// Importe de exentos.
            /// </summary>
            public double ImpOpEx
            {
                get;
                set;
            }
            /// <summary>
            /// Importe total de IVA.
            /// </summary>
            public double ImpIVA
            {
                get;
                set;
            }
            /// <summary>
            /// Importe Total de tributos.
            /// </summary>
            public double ImpTrib
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de inicio del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchServDesde
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de finalizacion del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchServHasta
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de vencimiento del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchVtoPago
            {
                get;
                set;
            }
            /// <summary>
            /// Codigo de la moneda utilizada en el comprobante.
            /// </summary>
            public string MonID
            {
                get;
                set;
            }
            /// <summary>
            /// Cotizacion de la moneda informada. Para PES = 1.
            /// </summary>
            public double MonCotiz
            {
                get;
                set;
            }
        }

        public class clsIVA
        {
            /// <summary>
            /// Codigo del tipo de IVA.
            /// </summary>
            public int ID
            {
                get;
                set;
            }
            /// <summary>
            /// Base imponible para el codigo de IVA.
            /// </summary>
            public double BaseImp
            {
                get;
                set;
            }
            /// <summary>
            /// Sumatoria del importe de IVA para la alicuota informada.
            /// </summary>
            public double Importe
            {
                get;
                set;
            }
        }

        public class clsTributos
        {
            /// <summary>
            /// Codigo identificatorio del tributo.
            /// </summary>
            public int ID
            {
                get;
                set;
            }
            /// <summary>
            /// Descripcion del tributo.
            /// </summary>
            public string Desc
            {
                get;
                set;
            }
            /// <summary>
            /// Base imponible para el tributo
            /// </summary>
            public double BaseImp
            {
                get;
                set;
            }
            public double Alic
            {
                get;
                set;
            }
            public double Importe
            {
                get;
                set;
            }
        }

        public class clsCaeResponse
        {
            public string Resultado { get; set; }
            public string Cae { get; set; }
            public string FechaVto { get; set; }
            public string ErrorCode { get; set; }
            public string ErrorMsj { get; set; }
        }

        public class clsCaeRecupero
        {
            public string Resultado { get; set; }
            public string Cae { get; set; }
            public string FechaVto { get; set; }
            public double ImporteTotal { get; set; }
            public string ErrorMsj { get; set; }
            public string MonId { get; set; }
            public double MonCtz { get; set; }
            public int CbtTipo { get; set; }
            public int DocTipo { get; set; }
            public long DocNro { get; set; }
            public long CbteDesde { get; set; }

        }

        /// <summary>
        /// URL servicio Wasa de Test.
        /// </summary>
        const string TEST_URLWSAAWSDL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
        /// <summary>
        /// URL serviciop Waa de Produccion.
        /// </summary>
        const string DEFAULT_URLWSAAWSDL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";
        const string DEFAULT_SERVICIO = "wsfe";
        const bool DEFAULT_VERBOSE = true;
        /// <summary>
        /// URL servicio FE de Test.
        /// </summary>
        const string TEST_URLWSFEV1 = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx";
        /// <summary>
        /// URL servicio FE de Prooduccion.
        /// </summary>
        const string DEFAULT_URLWSFEV1 = "https://servicios1.afip.gov.ar/wsfev1/service.asmx";

        public static Wsaa ObtenerDatosWsaa(string pPath, string pPathTa, bool IsTest, string _Cuit, string _PathLog)
        {
            string strUrlWassaTest = TEST_URLWSAAWSDL;
            string strUrlWsaaWsdl = DEFAULT_URLWSAAWSDL;
            string strIdServicioNegocio = DEFAULT_SERVICIO;
            string strRutaCertSigner = pPath;
            bool blnVerboseMode = DEFAULT_VERBOSE;
            //string strTApath = System.IO.Path.GetDirectoryName(pPath) + @"\TA.xml";
            //string strTApath = pPathTa + @"\TA.xml";
            string strTApath = pPathTa + @"\" + _Cuit + ".xml";

            Wsaa wa = new Wsaa();

            // Argumentos OK, entonces procesar normalmente... 

            Wsaa.LoginTicket objTicketRespuesta;
            string strTicketRespuesta;

            try
            {

                if (blnVerboseMode)
                {                    
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***Servicio a acceder: " + strIdServicioNegocio);
                    if(IsTest)
                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***URL del WSAA: " + strUrlWassaTest);
                    else
                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***URL del WSAA: " + strUrlWsaaWsdl);
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***Ruta del certificado: " + strRutaCertSigner);
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***Modo verbose: " + blnVerboseMode.ToString());
                }

                objTicketRespuesta = new Wsaa.LoginTicket();

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***Accediendo a " + strUrlWsaaWsdl);
                }

                if (IsTest)
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWassaTest, strRutaCertSigner, blnVerboseMode, _PathLog);
                else
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWsaaWsdl, strRutaCertSigner, blnVerboseMode, _PathLog);

                if (blnVerboseMode)
                {
                    //Ponemos los datos en el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***CONTENIDO DEL TICKET RESPUESTA:");
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " Token: " + objTicketRespuesta.Token);
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " Sign: " + objTicketRespuesta.Sign);
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " GenerationTime: " + Convert.ToString(objTicketRespuesta.GenerationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " ExpirationTime: " + Convert.ToString(objTicketRespuesta.ExpirationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " Service: " + objTicketRespuesta.Service);
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), " UniqueID: " + Convert.ToString(objTicketRespuesta.UniqueId));
                    //Pasamos los datos a la clase.
                    wa.Token = objTicketRespuesta.Token;
                    wa.Sign = objTicketRespuesta.Sign;
                    wa.ExpirationTime = objTicketRespuesta.ExpirationTime;
                    objTicketRespuesta.XmlLoginTicketResponse.Save(strTApath);
                }
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "***EXCEPCION AL OBTENER TICKET:" + excepcionAlObtenerTicket.Message);
            }

            return wa;
        }

        public static bool ValidoTA(string pPath, string pPathTa, Wsaa _cls, string _Cuit, string _PathLog)
        {
            //variable con la respuesta.
            bool booRta = false;
            //instanciamos el dataset
            System.Data.DataSet dtsTA = new System.Data.DataSet();
            //Vemos si ya tenemos el TA.xml
            //string strTApath = pPathTa + @"\TA.xml";
            string strTApath = pPathTa + @"\" + _Cuit +".xml";

            if (System.IO.File.Exists(strTApath))
            {
                System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                try
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Leemos el archivo: " + strTApath);
                    dtsTA.ReadXml(fsReadXml);
                    fsReadXml.Close();
                    //Vemos si esta activo.
                    DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    if (dttExpiration > DateTime.Now)
                    {
                        _cls.Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                        _cls.Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                        _cls.ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        booRta = true;
                    }
                    else
                    {
                        booRta = false;
                    }
                }
                catch (Exception ex)
                {
                    //Escribimos el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                    //Como se produjo un error lo vuelvo a pedir.
                    fsReadXml.Close();
                    booRta = false;
                }

            }
            else
            {
                booRta = false;
            }
            dtsTA.Dispose();
            return booRta;
        }

        public static int RecuperoUltimoComprobante(string pPathCert, string pPathTa, int PtoVta, int CbteTipo, string pCuit, bool IsTest, string _PathLog, out string pError)
        {
            string _Error = "";
            Wsaa _cls = new Wsaa();
            try
            {
                //strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////RECUPEROULTIMOCOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////Certificado->" + pPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////PTOVTA->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CBTETIPO->" + CbteTipo);
                //validamos que tenemos el TA.xml
                if (!wsAfip.ValidoTA(pPathCert, pPathTa, _cls, pCuit, _PathLog))
                    _cls = wsAfip.ObtenerDatosWsaa(pPathCert, pPathTa, IsTest, pCuit, _PathLog);
                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);
                wsAfipCae.Service srvCall = new wsAfipCae.Service();
                if (IsTest)
                    srvCall.Url = TEST_URLWSFEV1;
                else
                    srvCall.Url = DEFAULT_URLWSFEV1;
                //Consultamos el ultimo comprobante autorizado.
                IcgPlugInFacturacion.wsAfipCae.FERecuperaLastCbteResponse clsUltimoComprobante = srvCall.FECompUltimoAutorizado(Auth, PtoVta, CbteTipo);
                if (clsUltimoComprobante.Errors != null)
                {
                    if (clsUltimoComprobante.Errors.Length > 0)
                    {
                        for (int i = 0; clsUltimoComprobante.Errors.Length < 0; i++)
                        {
                            if (String.IsNullOrEmpty(_Error))
                                _Error = clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                            else
                                _Error = _Error + " | " + clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                        }
                    }
                }
                pError = _Error;
                //Recuperamos los datos del comprobante.
                return clsUltimoComprobante.CbteNro;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "*****ERROR AL TRAER EL ULTIMO AUTORIZADO*******");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "*****" + ex.Message + "*******");
                pError = ex.Message;
                return -1;
            }
        }

        public static clsCaeResponse ObtenerDatosCAE(clsCabReq _CabReq, clsDetReq _DetReq, List<clsTributos> _Trib, List<clsIVA> _Iva, 
            string pCerPath, string pPathTa, string pCUIT, bool IsTest, string _cbu, string _PathLog)
        {
            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            Wsaa _cls = new Wsaa();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CUIT->" + pCUIT);

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                return _clsRta;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                return _clsRta;
            }

            //validamos que tenemos el TA.xml
            if (!wsAfip.ValidoTA(strPathCert, pPathTa, _cls, pCUIT, _PathLog))
                _cls = wsAfip.ObtenerDatosWsaa(strPathCert, pPathTa, IsTest, pCUIT, _PathLog);

            try
            {
                if (_cls.Token != null)
                {

                    if (_cls.Token.Length > 0)
                    {
                        if (_cls.Sign.Length > 0)
                        {
                            if (_cls.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _cls.Token;
                                Auth.Sign = _cls.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos la clase que contiene los datos de la FC.
                                wsAfipCae.FECAERequest CAEReq = new wsAfipCae.FECAERequest();
                                //Instanciamos la clase de la cabecera de los datos de la FC. y la cargamos.
                                wsAfipCae.FECAECabRequest CabRequest = new wsAfipCae.FECAECabRequest();
                                CabRequest.CantReg = _CabReq.CantReg;
                                CabRequest.CbteTipo = _CabReq.CbteTipo;
                                CabRequest.PtoVta = _CabReq.PtoVta;
                                //Instanciamos la clase de los detalles de la FC (Array).
                                wsAfipCae.FECAEDetRequest[] arrCAEDetReq = new wsAfipCae.FECAEDetRequest[1];
                                
                                //Instanciamos la clase de los detalles de la FC que luego se cargara en el Array de detalles de FC.
                                wsAfipCae.FECAEDetRequest CAEDetReq = new wsAfipCae.FECAEDetRequest();
                                CAEDetReq.Concepto = _DetReq.Concepto;
                                CAEDetReq.DocTipo = _DetReq.DocTipo;
                                CAEDetReq.DocNro = _DetReq.DocNro;
                                CAEDetReq.CbteDesde = _DetReq.CbteDesde;
                                CAEDetReq.CbteHasta = _DetReq.CbteHasta;
                                CAEDetReq.CbteFch = _DetReq.CbteFch;
                                CAEDetReq.ImpTotal = _DetReq.ImpTotal;
                                CAEDetReq.ImpTotConc = _DetReq.ImpTotConc;
                                CAEDetReq.ImpNeto = _DetReq.ImpNeto;
                                CAEDetReq.ImpOpEx = _DetReq.ImpOpEx;
                                CAEDetReq.ImpTrib = _DetReq.ImpTrib;
                                CAEDetReq.ImpIVA = _DetReq.ImpIVA;
                                if (_DetReq.Concepto > 1)
                                {
                                    CAEDetReq.FchServDesde = _DetReq.FchServDesde;
                                    CAEDetReq.FchServHasta = _DetReq.FchServHasta;
                                    CAEDetReq.FchVtoPago = _DetReq.FchVtoPago;
                                }
                                CAEDetReq.MonId = _DetReq.MonID;
                                CAEDetReq.MonCotiz = _DetReq.MonCotiz;

                                if (_Trib.Count > 0)
                                {
                                    //Instanciamos el array de la clase de Tributos y le cargamos los datos.
                                    wsAfipCae.Tributo[] arrTrib = new wsAfipCae.Tributo[_Trib.Count];
                                    for (int i = 0; i < _Trib.Count; i++)
                                    {
                                        //Instanciamos la clase de tributo para llenar el array.
                                        wsAfipCae.Tributo Trib = new wsAfipCae.Tributo();
                                        Trib.Id = short.Parse(_Trib[i].ID.ToString());
                                        Trib.Desc = _Trib[i].Desc;
                                        Trib.BaseImp = _Trib[i].BaseImp;
                                        Trib.Alic = _Trib[i].Alic;
                                        Trib.Importe = _Trib[i].Importe;
                                        arrTrib[i] = Trib;
                                    }
                                    CAEDetReq.Tributos = arrTrib;
                                }

                                //Instanciamo el array de la clase de Alicuota de IVA.
                                if (_Iva.Count > 0)
                                {
                                    wsAfipCae.AlicIva[] arrAlic = new wsAfipCae.AlicIva[_Iva.Count];
                                    for (int x = 0; x < _Iva.Count; x++)
                                    {
                                        //Instanciamos la clase de Alicuotas de IVa para llenar el array.
                                        wsAfipCae.AlicIva Alic = new wsAfipCae.AlicIva();
                                        Alic.Id = _Iva[x].ID;
                                        Alic.BaseImp = _Iva[x].BaseImp;
                                        Alic.Importe = _Iva[x].Importe;
                                        arrAlic[x] = Alic;
                                    }
                                    CAEDetReq.Iva = arrAlic;
                                }

                                //Vemos si tenemos FActura MiPyME
                                if (_CabReq.CbteTipo == 201 || _CabReq.CbteTipo == 202 || _CabReq.CbteTipo == 203 ||
                                            _CabReq.CbteTipo == 206 || _CabReq.CbteTipo == 207 || _CabReq.CbteTipo == 208)
                                {
                                    List<wsAfipCae.Opcional> _opcio = new List<wsAfipCae.Opcional>();
                                    //CBU
                                    wsAfipCae.Opcional _op1 = new wsAfipCae.Opcional();
                                    _op1.Id = "2101";
                                    _op1.Valor = _cbu;
                                    _opcio.Add(_op1);
                                    CAEDetReq.Opcionales = _opcio.ToArray();
                                }

                                //Cargamos el array de detalles de FC.
                                arrCAEDetReq[0] = CAEDetReq;
                                //Cargamo la clase de los datos de las FC's.
                                CAEReq.FeCabReq = CabRequest;
                                CAEReq.FeDetReq = arrCAEDetReq;
                                //Instanciamos el Servicio.
                                wsAfipCae.Service srvCall = new wsAfipCae.Service();
                                //Pasamos la url del servicio.
                                if (IsTest)
                                    srvCall.Url = TEST_URLWSFEV1;
                                else
                                    srvCall.Url = DEFAULT_URLWSFEV1;
                                //Consumimos el servicio y cargamos la respuesta en la clase.
                                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), CAEReq.FeDetReq.ToString());
                                //
                                System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//Antes.xml";
                                System.IO.FileStream file = System.IO.File.Create(path);
                                _Antes.Serialize(file, CAEReq);
                                file.Close();
                                //
                                wsAfipCae.FECAEResponse CaeResponse = srvCall.FECAESolicitar(Auth, CAEReq);
                                //Vemos si el resultado es Aprobado.
                                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                //Recorremos los Errores.
                                if (CaeResponse.Errors != null)
                                {
                                    _clsRta.Resultado = "R";
                                    for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                    {
                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                               ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                        if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                        {
                                            _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                            _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                        }
                                        else
                                        {
                                            _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                            _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                        }
                                    }
                                }
                                if (CaeResponse.FeCabResp != null)
                                {
                                    switch (CaeResponse.FeCabResp.Resultado)
                                    {
                                        case "A":
                                            {
                                                _clsRta.Resultado = "A";
                                                _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;                                                

                                                break;
                                            }
                                        case "P":
                                            {
                                                _clsRta.Resultado = "P";
                                                _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                {
                                                    for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                    {
                                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                            ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                        if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                        {
                                                            _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                            _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                        }
                                                        else
                                                        {
                                                            _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                            _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        case "R":
                                            {
                                                _clsRta.Resultado = "R";
                                                if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                {
                                                    for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                    {
                                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                            ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                        if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                        {
                                                            _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                            _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                        }
                                                        else
                                                        {
                                                            _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                            _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                    }
                                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Resultado: " + _clsRta.Resultado +
                                               ". CAE: " + _clsRta.Cae + ". Fecha VTO: "+ _clsRta.FechaVto);
                                }
                                //Salimos de la Funcion.
                                return _clsRta;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj ="La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                return _clsRta;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj ="El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            return _clsRta;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        return _clsRta;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    return _clsRta;
                }
            }
            catch (Exception ex)
            {
                string _Error;
                //Recupero el nro del ultimo comprobante.
                int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, _PathLog, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                    //Recupero los datos del comprobante.
                //else
                    //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "R";
                return _clsRta;
            }
        }

        public static clsCaeRecupero ConsultaComprobante(string pPathCert, string pPathTa, int PtoVta, int CbteTipo, string pCuit, int CbteNro, bool IsTest, string _PathLog)
        {
            clsCaeRecupero _Rta = new clsCaeRecupero();
            try
            {
                string strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                Wsaa _cls = new Wsaa();
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CONSULTACOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////Certificado->" + strPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////PuntoVenta->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////CBTETIPO->" + CbteTipo);
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "//////Nro. Cbte.->" + CbteNro);
                //validamos que tenemos el TA.xml
                if (!wsAfip.ValidoTA(pPathCert, pPathTa, _cls, pCuit, _PathLog))
                    _cls = wsAfip.ObtenerDatosWsaa(pPathCert, pPathTa, IsTest, pCuit, _PathLog);
                if (_cls.Token != null)
                {
                    wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                    Auth.Token = _cls.Token;
                    Auth.Sign = _cls.Sign;
                    Auth.Cuit = long.Parse(pCuit);
                    wsAfipCae.FECompConsultaReq Cons = new wsAfipCae.FECompConsultaReq();
                    Cons.CbteTipo = CbteTipo;
                    Cons.CbteNro = long.Parse(CbteNro.ToString());
                    Cons.PtoVta = PtoVta;

                    wsAfipCae.Service srvCall = new wsAfipCae.Service();
                    if (IsTest)
                        srvCall.Url = TEST_URLWSFEV1;
                    else
                        srvCall.Url = DEFAULT_URLWSFEV1;

                    wsAfipCae.FECompConsultaResponse clsCompConsulta = srvCall.FECompConsultar(Auth, Cons);
                    //Vemos si tenemos errores.
                    if (clsCompConsulta.Errors != null)
                    {
                        _Rta.Resultado = "R";
                        for (int i = 0; i < clsCompConsulta.Errors.Length; i++)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "Error: " + clsCompConsulta.Errors[i].Code.ToString() +
                                   ". Descripción: " + clsCompConsulta.Errors[i].Msg.ToString());
                            if (String.IsNullOrEmpty(_Rta.ErrorMsj))
                            {
                                _Rta.ErrorMsj = clsCompConsulta.Errors[i].Msg.ToString();
                            }
                            else
                            {
                                _Rta.ErrorMsj = _Rta.ErrorMsj + " " + clsCompConsulta.Errors[i].Msg.ToString();
                            }
                        }
                    }
                    else
                    {
                        _Rta.Cae = clsCompConsulta.ResultGet.CodAutorizacion;
                        _Rta.FechaVto = clsCompConsulta.ResultGet.FchVto;
                        _Rta.ImporteTotal = clsCompConsulta.ResultGet.ImpTotal;
                        _Rta.CbteDesde = clsCompConsulta.ResultGet.CbteDesde;
                        _Rta.CbtTipo = clsCompConsulta.ResultGet.CbteTipo;
                        _Rta.DocNro = clsCompConsulta.ResultGet.DocNro;
                        _Rta.DocTipo = clsCompConsulta.ResultGet.DocTipo;
                        _Rta.MonCtz = clsCompConsulta.ResultGet.MonCotiz;
                        _Rta.MonId = clsCompConsulta.ResultGet.MonId;
                        _Rta.Resultado = "A";
                    }
                    
                }
                return _Rta;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "*****ERROR AL COMPROBAR EL COMPRONATE*******");
                LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "*****" + ex.Message + "*******");
                _Rta.Resultado = "R";
                _Rta.ErrorMsj = ex.Message;
                return _Rta;
            }
        }
    }
}
