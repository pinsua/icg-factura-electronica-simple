﻿namespace IcgPlugInFacturacion
{
    partial class frmSearchClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridEXLayout5 = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearchClientes));
            Janus.Windows.GridEX.GridEXLayout gridEXLayout6 = new Janus.Windows.GridEX.GridEXLayout();
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.grdClientes = new Janus.Windows.GridEX.GridEX();
            this.gbSeleccionados = new System.Windows.Forms.GroupBox();
            this.grdSeleccion = new Janus.Windows.GridEX.GridEX();
            this.btnPasar = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdClientes)).BeginInit();
            this.gbSeleccionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSeleccion)).BeginInit();
            this.SuspendLayout();
            // 
            // gbClientes
            // 
            this.gbClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbClientes.Controls.Add(this.grdClientes);
            this.gbClientes.Location = new System.Drawing.Point(5, 3);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(307, 370);
            this.gbClientes.TabIndex = 0;
            this.gbClientes.TabStop = false;
            this.gbClientes.Text = "Clientes";
            // 
            // grdClientes
            // 
            this.grdClientes.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdClientes.ColumnAutoResize = true;
            gridEXLayout5.LayoutString = resources.GetString("gridEXLayout5.LayoutString");
            this.grdClientes.DesignTimeLayout = gridEXLayout5;
            this.grdClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdClientes.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.grdClientes.FilterRowFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdClientes.GroupByBoxVisible = false;
            this.grdClientes.HideSelection = Janus.Windows.GridEX.HideSelection.Highlight;
            this.grdClientes.Location = new System.Drawing.Point(3, 16);
            this.grdClientes.Name = "grdClientes";
            this.grdClientes.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdClientes.Size = new System.Drawing.Size(301, 351);
            this.grdClientes.TabIndex = 0;
            this.grdClientes.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // gbSeleccionados
            // 
            this.gbSeleccionados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSeleccionados.Controls.Add(this.grdSeleccion);
            this.gbSeleccionados.Location = new System.Drawing.Point(398, 3);
            this.gbSeleccionados.Name = "gbSeleccionados";
            this.gbSeleccionados.Size = new System.Drawing.Size(307, 370);
            this.gbSeleccionados.TabIndex = 1;
            this.gbSeleccionados.TabStop = false;
            this.gbSeleccionados.Text = "Selección";
            // 
            // grdSeleccion
            // 
            this.grdSeleccion.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdSeleccion.ColumnAutoResize = true;
            gridEXLayout6.LayoutString = resources.GetString("gridEXLayout6.LayoutString");
            this.grdSeleccion.DesignTimeLayout = gridEXLayout6;
            this.grdSeleccion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSeleccion.GroupByBoxVisible = false;
            this.grdSeleccion.HideSelection = Janus.Windows.GridEX.HideSelection.Highlight;
            this.grdSeleccion.Location = new System.Drawing.Point(3, 16);
            this.grdSeleccion.Name = "grdSeleccion";
            this.grdSeleccion.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdSeleccion.Size = new System.Drawing.Size(301, 351);
            this.grdSeleccion.TabIndex = 0;
            this.grdSeleccion.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // btnPasar
            // 
            this.btnPasar.Location = new System.Drawing.Point(318, 81);
            this.btnPasar.Name = "btnPasar";
            this.btnPasar.Size = new System.Drawing.Size(75, 23);
            this.btnPasar.TabIndex = 2;
            this.btnPasar.Text = ">>";
            this.btnPasar.UseVisualStyleBackColor = true;
            this.btnPasar.Click += new System.EventHandler(this.btnPasar_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(318, 110);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 3;
            this.btnVolver.Text = "<<";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(317, 243);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(318, 272);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmSearchClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 377);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnPasar);
            this.Controls.Add(this.gbSeleccionados);
            this.Controls.Add(this.gbClientes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmSearchClientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Seleccione los clientes";
            this.Load += new System.EventHandler(this.frmSearchClientes_Load);
            this.gbClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdClientes)).EndInit();
            this.gbSeleccionados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSeleccion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbClientes;
        private Janus.Windows.GridEX.GridEX grdClientes;
        private System.Windows.Forms.GroupBox gbSeleccionados;
        private Janus.Windows.GridEX.GridEX grdSeleccion;
        private System.Windows.Forms.Button btnPasar;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
    }
}