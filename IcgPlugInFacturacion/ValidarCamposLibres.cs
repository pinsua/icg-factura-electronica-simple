﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgPlugInFacturacion
{
    public class ValidarCamposLibres
    {
        public static bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
        {
            string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
            bool _rta = false;

            using(SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _sqlConn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                _cmd.Parameters.AddWithValue("@table", _table);
                _cmd.Parameters.AddWithValue("column", _columna);

                if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                    _rta = true;
            }
            return _rta;
        }

        public static string ValidarColumnasCamposLibres(string _connection)
        {
            string _msj = "";
            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(_connection))
                {
                    _sqlConn.Open();

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "NC_RECHAZADA", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna NC_RECHAZADA en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "CAE", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "VTO_CAE", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna VTO_CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "COD_BARRAS", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna COD_BARRAS en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "ERROR_CAE", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna ERROR_CAE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "ESTADO_FE", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna ESTADO_FE en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "NC_RECHAZADA", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna NC_RECHAZADA en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Error validando CamposLibres. " + ex.Message);
            }
            return _msj;
        }
    }
}
