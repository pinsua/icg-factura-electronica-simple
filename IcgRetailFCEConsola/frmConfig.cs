﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailFCEConsola
{
    public partial class frmConfig : Form
    {
        private string _database;
        private string _serverSql;
        public frmConfig()
        {
            InitializeComponent();
            try
            {
                this.Text = this.Text + Application.ProductVersion;
                if (frmMain._server == "" || frmMain._database == "")
                    IcgVarios.IcgRegestry.Retail.GetDataBaseToConnect(out _serverSql, out _database);
                else
                {
                    _serverSql = frmMain._server;
                    _database = frmMain._database;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            txtCertificado.Text = frmMain._nombreCertificado;
            txtCuit.Text = frmMain._cuit;
            txtCaja.Text = frmMain._caja;
            //txtDatabase.Text = frmMain._database;
            txtDatabase.Text = _database;
            txtPassword.Text = frmMain._password;
            txtProVtaCAE.Text = frmMain._PtoVtaCAE;
            txtPtoVtaCAEA.Text = frmMain._PtoVtaCAEA;
            txtPtoVtaManual.Text = frmMain._PtoVtaManual;
            //txtServer.Text = frmMain._server;
            txtServer.Text = _serverSql;
            txtLicencia.Text = frmMain._licenciaIcg;
            chEsTest.Checked = frmMain._esTest;
            chSoloCAEA.Checked = frmMain._soloCaea;
            chDesdoblaPagos.Checked = frmMain._DesdoblaPagos;
            chMonotributista.Checked = frmMain._Monotributista;
            chMtxca.Checked = frmMain._mtxca;
            txtCantidadTiquets.Text = frmMain._cantidadTiquets.ToString();
            //Irsa
            txtContratoIrsa.Text = frmMain._irsa.contrato;
            txtLocalIrsa.Text = frmMain._irsa.local;
            txtPathIrsa.Text = frmMain._irsa.pathSalida;
            txtPosIrsa.Text = frmMain._irsa.pos;
            txtRubroIrsa.Text = frmMain._irsa.rubro;
            //Olmos
            txtOlmosPathSalida.Text = frmMain._olmosPathSalida;
            txtOlmosNroCliente.Text = frmMain._olmosNroCliente;
            txtOlmosNroPos.Text = frmMain._olmosNroPos;
            txtOlmosProceso.Text = frmMain._olmosProceso;
            txtOlmosRubro.Text = frmMain._olmosRubro;
            //Caballito
            txtCaballitoPathSalida.Text = frmMain._caballitoPathSalida;
            //Monto Maximo
            txtMontoMaximo.Text = frmMain._montoMaximo.ToString();
            //SiTef
            txtIdTiendaSiTef.Text = frmMain._IdTiendaSiTef;
            //PathSiTef
            txtPathSiTef.Text = frmMain._PathSiTef;
            //IdTermnalSiTef
            txtIdTerminalSiTef.Text = frmMain._IdTerminalSiTef;
        }

        private void btKey_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Para obtener una licencia, debe cargar correctamente los siguientes datos, caso contrario la licencia no será valida." +
                Environment.NewLine + "Punto de Venta, Nro de CUIT y el Nombre y el Nombre Comercial de la Empresa.",
                "ICG Argentina",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (!String.IsNullOrEmpty(txtServer.Text))
                {
                    if (!String.IsNullOrEmpty(txtDatabase.Text))
                    {
                        if (!String.IsNullOrEmpty(txtProVtaCAE.Text))
                        {
                            //Validamos licencia via WebService.
                            string _Msg = "";
                            string _Key = "";
                            IcgFceDll.RetailService.Licencia.GetKey(txtServer.Text,
                                txtDatabase.Text, txtCuit.Text, txtProVtaCAE.Text, out _Key, out _Msg);
                            if (!String.IsNullOrEmpty(_Key))
                            {
                                txtLicencia.Text = _Key;
                            }
                            else
                            {
                                txtLicencia.Text = "";
                            }
                            if(!String.IsNullOrEmpty(_Msg))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + Environment.NewLine +
                                                                _Msg, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Por favor ingrese el Punto de Venta CAE para obtener una licencia.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Por favor ingrese la Base de Datos, para obtener una licencia.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Por favor ingrese el Servidor, para obtener una licencia.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Seguro que desea salir SIN GUARDAR la información?.", 
                "ICG Argentina SA", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            //Guardamos los datos.
            Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
            //DataBase
            config.AppSettings.Settings["Server"].Value = txtServer.Text;
            config.AppSettings.Settings["DataBase"].Value = txtDatabase.Text;
            if (config.AppSettings.Settings.AllKeys.Contains("Caja"))
                config.AppSettings.Settings["Caja"].Value = txtCaja.Text;
            else
                config.AppSettings.Settings.Add("Caja", txtCaja.Text);
            //Facturación
            config.AppSettings.Settings["NombreCertificado"].Value = txtCertificado.Text;
            config.AppSettings.Settings["CUIT"].Value = txtCuit.Text;            
            config.AppSettings.Settings["PassWord"].Value = txtPassword.Text;
            config.AppSettings.Settings["PtoVtaManual"].Value = txtPtoVtaManual.Text;
            config.AppSettings.Settings["PtoVtaCAE"].Value = txtProVtaCAE.Text;
            config.AppSettings.Settings["PtoVtaCAEA"].Value = txtPtoVtaCAEA.Text;
            config.AppSettings.Settings["IsMtxca"].Value = chMtxca.Checked.ToString();
            config.AppSettings.Settings["SoloCAEA"].Value = chSoloCAEA.Checked.ToString();
            config.AppSettings.Settings["IsTest"].Value = chEsTest.Checked.ToString();
            config.AppSettings.Settings["Monotributista"].Value = chMonotributista.Checked.ToString();
            config.AppSettings.Settings["DesdoblaPagos"].Value = chDesdoblaPagos.Checked.ToString();
            if (config.AppSettings.Settings.AllKeys.Contains("CantidadTiquets"))
                config.AppSettings.Settings["CantidadTiquets"].Value = txtCantidadTiquets.Text;
            else
                config.AppSettings.Settings.Add("CantidadTiquets", txtCantidadTiquets.Text);
            //Licencia
            config.AppSettings.Settings["LicenciaIcg"].Value = txtLicencia.Text;
            //Irsa
            if (config.AppSettings.Settings.AllKeys.Contains("IrsaContrato"))
                config.AppSettings.Settings["IrsaContrato"].Value = txtContratoIrsa.Text;
            else
                config.AppSettings.Settings.Add("IrsaContrato", txtContratoIrsa.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("IrsaLocal"))
                config.AppSettings.Settings["IrsaLocal"].Value = txtLocalIrsa.Text;
            else
                config.AppSettings.Settings.Add("IrsaLocal", txtLocalIrsa.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("IrsaPathSalida"))
                config.AppSettings.Settings["IrsaPathSalida"].Value = txtPathIrsa.Text;
            else
                config.AppSettings.Settings.Add("IrsaPathSalida", txtPathIrsa.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("IrsaPos"))
                config.AppSettings.Settings["IrsaPos"].Value = txtPosIrsa.Text;
            else
                config.AppSettings.Settings.Add("IrsaPos", txtPosIrsa.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("IrsaRubro"))
                config.AppSettings.Settings["IrsaRubro"].Value = txtRubroIrsa.Text;
            else
                config.AppSettings.Settings.Add("IrsaRubro", txtRubroIrsa.Text);
            //Olmos
            if (config.AppSettings.Settings.AllKeys.Contains("OlmosPathSalida"))
                config.AppSettings.Settings["OlmosPathSalida"].Value = txtOlmosPathSalida.Text;
            else
                config.AppSettings.Settings.Add("OlmosPathSalida", txtOlmosPathSalida.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("OlmosNroCliente"))
                config.AppSettings.Settings["OlmosNroCliente"].Value = txtOlmosNroCliente.Text;
            else
                config.AppSettings.Settings.Add("OlmosNroCliente", txtOlmosNroCliente.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("OlmosNroPos"))
                config.AppSettings.Settings["OlmosNroPos"].Value = txtOlmosNroPos.Text;
            else
                config.AppSettings.Settings.Add("OlmosNroPos", txtOlmosNroPos.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("OlmosRubro"))
                config.AppSettings.Settings["OlmosRubro"].Value = txtOlmosRubro.Text;
            else
                config.AppSettings.Settings.Add("OlmosRubro", txtOlmosRubro.Text);

            if (config.AppSettings.Settings.AllKeys.Contains("OlmosProceso"))
                config.AppSettings.Settings["OlmosProceso"].Value = txtOlmosProceso.Text;
            else
                config.AppSettings.Settings.Add("OlmosProceso", txtOlmosProceso.Text);
            //Caballito
            if (config.AppSettings.Settings.AllKeys.Contains("CaballitoPathSalida"))
                config.AppSettings.Settings["CaballitoPathSalida"].Value = txtCaballitoPathSalida.Text;
            else
                config.AppSettings.Settings.Add("CaballitoPathSalida", txtCaballitoPathSalida.Text);
            //MontoMaximo
            if (config.AppSettings.Settings.AllKeys.Contains("MontoMaximo"))
                config.AppSettings.Settings["MontoMaximo"].Value = txtMontoMaximo.Text;
            else
                config.AppSettings.Settings.Add("MontoMaximo", txtMontoMaximo.Text);
            //IdTiendaSitef
            if (config.AppSettings.Settings.AllKeys.Contains("IdTiendaSitef"))
                config.AppSettings.Settings["IdTiendaSitef"].Value = txtIdTiendaSiTef.Text;
            else
                config.AppSettings.Settings.Add("IdTiendaSitef", txtIdTiendaSiTef.Text);
            //PathSitef
            //if (!String.IsNullOrEmpty(txtPathSiTef.Text))
            if (txtPathSiTef.Text.Trim().Length > 0)
            {
                if (!txtPathSiTef.Text.EndsWith(@"\"))
                    txtPathSiTef.Text = txtPathSiTef.Text + @"\";
            }
            if (config.AppSettings.Settings.AllKeys.Contains("PathSitef"))
                config.AppSettings.Settings["PathSitef"].Value = txtPathSiTef.Text;
            else
                config.AppSettings.Settings.Add("PathSitef", txtPathSiTef.Text);
            //IdTerminal
            if (config.AppSettings.Settings.AllKeys.Contains("IdTerminalSitef"))
                config.AppSettings.Settings["IdTerminalSitef"].Value = txtIdTerminalSiTef.Text;
            else
                config.AppSettings.Settings.Add("IdTerminalSitef", txtIdTerminalSiTef.Text);

            config.Save();

            MessageBox.Show("Para que la configuración tenga efecto debe reiniciar la consola",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Recuperar los datos de conexión a la Base de datos?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Recuperamos los datos de la conexion
                IcgVarios.IcgRegestry.Retail.GetDataBaseToConnect(out _serverSql, out _database);
                txtServer.Text = _serverSql;
                txtDatabase.Text = _database;
            }

            if (String.IsNullOrEmpty(txtServer.Text))
            {
                MessageBox.Show("No esta configurado el servidor de SQL." + Environment.NewLine +
                    "Ingreso y vuelva a probar.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (String.IsNullOrEmpty(txtDatabase.Text))
            {
                MessageBox.Show("No esta configurada la Base de datos SQL." + Environment.NewLine +
                    "Ingresela y vuelva a probar.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //Armamos el stringConnection.
            string strConnection = "Data Source=" + txtServer.Text.Trim() + ";Initial Catalog=" + txtDatabase.Text.Trim() + ";User Id=ICGAdmin;Password=masterkey;";
            try
            {
                //Conectamos.
                using (SqlConnection _connection = new SqlConnection(strConnection))
                {
                    _connection.Open();                    
                    
                    //Tabla TransaccionAFIP
                    if (MessageBox.Show("Desea crear la tabla de Transacciones con AFIP?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string _msj = IcgFceDll.CommonService.TransaccionAFIP.CrearTablaTransaccionAFIP(_connection);
                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //Tabla FCCAEA
                    if (MessageBox.Show("Desea crear la tabla FCCAE?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string _msj = IcgFceDll.CommonService.FcCAEA.CreateTableFcCAEA(_connection);
                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    //Vemos si tenemos el punto de venta para CAEA.
                    if (!String.IsNullOrEmpty(txtPtoVtaCAEA.Text))
                    {
                        if (MessageBox.Show("Desea validar los contadores de CAEA?.", "ICG Argentina",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SecureString strPasswordSecureString = new SecureString();
                            foreach (char c in txtPassword.Text.Trim()) strPasswordSecureString.AppendChar(c);
                            strPasswordSecureString.MakeReadOnly();
                            //Recuperamos los contadores existentes.
                            List<string> _contadores = IcgFceDll.RestService.Config.GetSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), _connection);
                            if (!_contadores.Contains("001"))
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 1, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "001", _ultimo.CbteNro, _connection);
                            }
                            else
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 1, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "001", _ultimo.CbteNro, _connection);
                            }

                            if (!_contadores.Contains("003"))
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 3, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "003", _ultimo.CbteNro, _connection);
                            }
                            else
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 3, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "003", _ultimo.CbteNro, _connection);
                            }

                            if (!_contadores.Contains("006"))
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 6, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "006", _ultimo.CbteNro, _connection);
                            }
                            else
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 6, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "006", _ultimo.CbteNro, _connection);
                            }

                            if (!_contadores.Contains("008"))
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 8, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "008", _ultimo.CbteNro, _connection);
                            }
                            else
                            {
                                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();
                                AfipDll.wsAfip.UltimoComprobante _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                    Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 8, txtCuit.Text.Trim(), chEsTest.Checked, strPasswordSecureString, out _lstErrores);
                                IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "008", _ultimo.CbteNro, _connection);
                            }
                        }
                    }

                    //Vemos si tenemos el punto de venta Manual.
                    if (!String.IsNullOrEmpty(txtPtoVtaManual.Text))
                    {
                        if (MessageBox.Show("Desea validar los contadores de los Talonarios Manuales?.", "ICG Argentina",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //Recuperamos los contadores existentes.
                            List<string> _contadores = IcgFceDll.RestService.Config.GetSeriesResolucionContadores(txtPtoVtaManual.Text.Trim(), _connection);
                            if (!_contadores.Contains("001"))
                            {
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaManual.Text.Trim(), "001", 0, _connection);
                            }
                            if (!_contadores.Contains("003"))
                            {
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaManual.Text.Trim(), "003", 0, _connection);
                            }
                            if (!_contadores.Contains("006"))
                            {
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaManual.Text.Trim(), "006", 0, _connection);
                            }
                            if (!_contadores.Contains("008"))
                            {
                                IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaManual.Text.Trim(), "008", 0, _connection);
                            }
                        }
                    }
                    //
                    if (MessageBox.Show("Desea validar los campos libres?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //Valido los campos libres
                        string _msj = IcgFceDll.RetailService.Common.ValidarColumnasCamposLibres(_connection);
                        if (!String.IsNullOrEmpty(_msj))
                        {
                            MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                            MessageBox.Show("Los campos libres están correctamente definidos.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error." + Environment.NewLine +
                    "Error: " + ex.Message,
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRegenerarIrsa_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(frmMain._irsa.pathSalida))
            {
                frmRegenerarInfo _frm = new frmRegenerarInfo("IRSA", frmMain._irsa,
                    frmMain._olmosPathSalida, frmMain._olmosNroCliente, frmMain._olmosNroPos, frmMain._olmosRubro, frmMain._olmosProceso, frmMain._cuit,
                    frmMain._caballitoPathSalida, frmMain._server, frmMain._database);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show("No esta definido el directorio de salida.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnRegenerarOlmos_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(frmMain._olmosPathSalida))
            {
                frmRegenerarInfo _frm = new frmRegenerarInfo("OLMOS", frmMain._irsa,
                    frmMain._olmosPathSalida, frmMain._olmosNroCliente, frmMain._olmosNroPos, frmMain._olmosRubro, frmMain._olmosProceso, frmMain._cuit,
                    frmMain._caballitoPathSalida, frmMain._server, frmMain._database);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show("No esta definido el directorio de salida.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnRegenerarCaballito_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(frmMain._caballitoPathSalida))
            {
                frmRegenerarInfo _frm = new frmRegenerarInfo("CABALLITO", frmMain._irsa,
                    frmMain._olmosPathSalida, frmMain._olmosNroCliente, frmMain._olmosNroPos, frmMain._olmosRubro, frmMain._olmosProceso, frmMain._cuit,
                    frmMain._caballitoPathSalida, frmMain._server, frmMain._database);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show("No esta definido el directorio de salida.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtMontoMaximo_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Permite 0-9, Espacios, y decimal
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }
    }
}
