﻿namespace IcgRetailFCEConsola
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDataBase = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCaja = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblDatebase = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.gbFacturacion = new System.Windows.Forms.GroupBox();
            this.txtMontoMaximo = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCantidadTiquets = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.chDesdoblaPagos = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chMonotributista = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chMtxca = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chSoloCAEA = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPtoVtaCAEA = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPtoVtaManual = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProVtaCAE = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chEsTest = new System.Windows.Forms.CheckBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCertificado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbLicencia = new System.Windows.Forms.GroupBox();
            this.txtLicencia = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btKey = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.tcShoppings = new System.Windows.Forms.TabControl();
            this.tpIrsa = new System.Windows.Forms.TabPage();
            this.btnRegenerarIrsa = new System.Windows.Forms.Button();
            this.txtPathIrsa = new System.Windows.Forms.TextBox();
            this.txtRubroIrsa = new System.Windows.Forms.TextBox();
            this.txtPosIrsa = new System.Windows.Forms.TextBox();
            this.txtLocalIrsa = new System.Windows.Forms.TextBox();
            this.txtContratoIrsa = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tpOlmos = new System.Windows.Forms.TabPage();
            this.btnRegenerarOlmos = new System.Windows.Forms.Button();
            this.txtOlmosProceso = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtOlmosRubro = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtOlmosNroPos = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtOlmosNroCliente = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtOlmosPathSalida = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tpCaballito = new System.Windows.Forms.TabPage();
            this.btnRegenerarCaballito = new System.Windows.Forms.Button();
            this.txtCaballitoPathSalida = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tpSiTef = new System.Windows.Forms.TabPage();
            this.txtIdTerminalSiTef = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtPathSiTef = new System.Windows.Forms.TextBox();
            this.txtIdTiendaSiTef = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.gbDataBase.SuspendLayout();
            this.gbFacturacion.SuspendLayout();
            this.gbLicencia.SuspendLayout();
            this.tcShoppings.SuspendLayout();
            this.tpIrsa.SuspendLayout();
            this.tpOlmos.SuspendLayout();
            this.tpCaballito.SuspendLayout();
            this.tpSiTef.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDataBase
            // 
            this.gbDataBase.Controls.Add(this.label24);
            this.gbDataBase.Controls.Add(this.txtCaja);
            this.gbDataBase.Controls.Add(this.button1);
            this.gbDataBase.Controls.Add(this.lblServer);
            this.gbDataBase.Controls.Add(this.txtServer);
            this.gbDataBase.Controls.Add(this.lblDatebase);
            this.gbDataBase.Controls.Add(this.txtDatabase);
            this.gbDataBase.Location = new System.Drawing.Point(18, 10);
            this.gbDataBase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDataBase.Name = "gbDataBase";
            this.gbDataBase.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDataBase.Size = new System.Drawing.Size(1076, 104);
            this.gbDataBase.TabIndex = 19;
            this.gbDataBase.TabStop = false;
            this.gbDataBase.Text = "Datos de Conexión";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 61);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 20);
            this.label24.TabIndex = 11;
            this.label24.Text = "Caja";
            // 
            // txtCaja
            // 
            this.txtCaja.Location = new System.Drawing.Point(134, 57);
            this.txtCaja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCaja.Name = "txtCaja";
            this.txtCaja.Size = new System.Drawing.Size(326, 26);
            this.txtCaja.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(632, 57);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 35);
            this.button1.TabIndex = 10;
            this.button1.Text = "Crear tablas de configuración";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(9, 25);
            this.lblServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(55, 20);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "Server";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(134, 21);
            this.txtServer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(326, 26);
            this.txtServer.TabIndex = 1;
            // 
            // lblDatebase
            // 
            this.lblDatebase.AutoSize = true;
            this.lblDatebase.Location = new System.Drawing.Point(507, 25);
            this.lblDatebase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDatebase.Name = "lblDatebase";
            this.lblDatebase.Size = new System.Drawing.Size(115, 20);
            this.lblDatebase.TabIndex = 2;
            this.lblDatebase.Text = "Base de Datos";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(632, 21);
            this.txtDatabase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(326, 26);
            this.txtDatabase.TabIndex = 2;
            // 
            // gbFacturacion
            // 
            this.gbFacturacion.Controls.Add(this.txtMontoMaximo);
            this.gbFacturacion.Controls.Add(this.label26);
            this.gbFacturacion.Controls.Add(this.txtCantidadTiquets);
            this.gbFacturacion.Controls.Add(this.label25);
            this.gbFacturacion.Controls.Add(this.chDesdoblaPagos);
            this.gbFacturacion.Controls.Add(this.label12);
            this.gbFacturacion.Controls.Add(this.chMonotributista);
            this.gbFacturacion.Controls.Add(this.label11);
            this.gbFacturacion.Controls.Add(this.chMtxca);
            this.gbFacturacion.Controls.Add(this.label10);
            this.gbFacturacion.Controls.Add(this.chSoloCAEA);
            this.gbFacturacion.Controls.Add(this.label8);
            this.gbFacturacion.Controls.Add(this.txtPtoVtaCAEA);
            this.gbFacturacion.Controls.Add(this.label7);
            this.gbFacturacion.Controls.Add(this.txtPtoVtaManual);
            this.gbFacturacion.Controls.Add(this.label6);
            this.gbFacturacion.Controls.Add(this.txtProVtaCAE);
            this.gbFacturacion.Controls.Add(this.label5);
            this.gbFacturacion.Controls.Add(this.label4);
            this.gbFacturacion.Controls.Add(this.chEsTest);
            this.gbFacturacion.Controls.Add(this.txtPassword);
            this.gbFacturacion.Controls.Add(this.label3);
            this.gbFacturacion.Controls.Add(this.txtCertificado);
            this.gbFacturacion.Controls.Add(this.label2);
            this.gbFacturacion.Controls.Add(this.txtCuit);
            this.gbFacturacion.Controls.Add(this.label1);
            this.gbFacturacion.Location = new System.Drawing.Point(18, 119);
            this.gbFacturacion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbFacturacion.Name = "gbFacturacion";
            this.gbFacturacion.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbFacturacion.Size = new System.Drawing.Size(1076, 208);
            this.gbFacturacion.TabIndex = 20;
            this.gbFacturacion.TabStop = false;
            this.gbFacturacion.Text = "Datos Facturación";
            // 
            // txtMontoMaximo
            // 
            this.txtMontoMaximo.Location = new System.Drawing.Point(632, 170);
            this.txtMontoMaximo.Name = "txtMontoMaximo";
            this.txtMontoMaximo.Size = new System.Drawing.Size(171, 26);
            this.txtMontoMaximo.TabIndex = 30;
            this.txtMontoMaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontoMaximo_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(361, 173);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(234, 20);
            this.label26.TabIndex = 29;
            this.label26.Text = "Monto máximo Consumidor final";
            // 
            // txtCantidadTiquets
            // 
            this.txtCantidadTiquets.Location = new System.Drawing.Point(237, 170);
            this.txtCantidadTiquets.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCantidadTiquets.Name = "txtCantidadTiquets";
            this.txtCantidadTiquets.Size = new System.Drawing.Size(96, 26);
            this.txtCantidadTiquets.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(14, 173);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(209, 20);
            this.label25.TabIndex = 27;
            this.label25.Text = "Informar cada tantos tiquets";
            // 
            // chDesdoblaPagos
            // 
            this.chDesdoblaPagos.AutoSize = true;
            this.chDesdoblaPagos.Location = new System.Drawing.Point(854, 133);
            this.chDesdoblaPagos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chDesdoblaPagos.Name = "chDesdoblaPagos";
            this.chDesdoblaPagos.Size = new System.Drawing.Size(22, 21);
            this.chDesdoblaPagos.TabIndex = 21;
            this.chDesdoblaPagos.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(722, 135);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Desdobla Pagos";
            // 
            // chMonotributista
            // 
            this.chMonotributista.AutoSize = true;
            this.chMonotributista.Location = new System.Drawing.Point(649, 131);
            this.chMonotributista.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chMonotributista.Name = "chMonotributista";
            this.chMonotributista.Size = new System.Drawing.Size(22, 21);
            this.chMonotributista.TabIndex = 19;
            this.chMonotributista.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(531, 133);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Monotributista";
            // 
            // chMtxca
            // 
            this.chMtxca.AutoSize = true;
            this.chMtxca.Location = new System.Drawing.Point(462, 133);
            this.chMtxca.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chMtxca.Name = "chMtxca";
            this.chMtxca.Size = new System.Drawing.Size(22, 21);
            this.chMtxca.TabIndex = 17;
            this.chMtxca.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(361, 135);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Usa MTXCA";
            // 
            // chSoloCAEA
            // 
            this.chSoloCAEA.AutoSize = true;
            this.chSoloCAEA.Location = new System.Drawing.Point(290, 134);
            this.chSoloCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chSoloCAEA.Name = "chSoloCAEA";
            this.chSoloCAEA.Size = new System.Drawing.Size(22, 21);
            this.chSoloCAEA.TabIndex = 8;
            this.chSoloCAEA.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(192, 136);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Solo CAEA";
            // 
            // txtPtoVtaCAEA
            // 
            this.txtPtoVtaCAEA.Location = new System.Drawing.Point(632, 96);
            this.txtPtoVtaCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVtaCAEA.Name = "txtPtoVtaCAEA";
            this.txtPtoVtaCAEA.Size = new System.Drawing.Size(326, 26);
            this.txtPtoVtaCAEA.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(507, 101);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pto. Vta. CAEA";
            // 
            // txtPtoVtaManual
            // 
            this.txtPtoVtaManual.Location = new System.Drawing.Point(632, 58);
            this.txtPtoVtaManual.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVtaManual.Name = "txtPtoVtaManual";
            this.txtPtoVtaManual.Size = new System.Drawing.Size(326, 26);
            this.txtPtoVtaManual.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(494, 63);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Pto. Vta. Manual";
            // 
            // txtProVtaCAE
            // 
            this.txtProVtaCAE.Location = new System.Drawing.Point(632, 21);
            this.txtProVtaCAE.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtProVtaCAE.Name = "txtProVtaCAE";
            this.txtProVtaCAE.Size = new System.Drawing.Size(326, 26);
            this.txtProVtaCAE.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(507, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pto. Vta. CAE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 136);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Es Test";
            // 
            // chEsTest
            // 
            this.chEsTest.AutoSize = true;
            this.chEsTest.Location = new System.Drawing.Point(134, 136);
            this.chEsTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chEsTest.Name = "chEsTest";
            this.chEsTest.Size = new System.Drawing.Size(22, 21);
            this.chEsTest.TabIndex = 7;
            this.chEsTest.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(134, 96);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(326, 26);
            this.txtPassword.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "PassWord";
            // 
            // txtCertificado
            // 
            this.txtCertificado.Location = new System.Drawing.Point(134, 58);
            this.txtCertificado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCertificado.Name = "txtCertificado";
            this.txtCertificado.Size = new System.Drawing.Size(326, 26);
            this.txtCertificado.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Certificado";
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(134, 21);
            this.txtCuit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.Size = new System.Drawing.Size(326, 26);
            this.txtCuit.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "CUIT";
            // 
            // gbLicencia
            // 
            this.gbLicencia.Controls.Add(this.txtLicencia);
            this.gbLicencia.Controls.Add(this.label9);
            this.gbLicencia.Controls.Add(this.btKey);
            this.gbLicencia.Location = new System.Drawing.Point(18, 337);
            this.gbLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbLicencia.Name = "gbLicencia";
            this.gbLicencia.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbLicencia.Size = new System.Drawing.Size(1076, 69);
            this.gbLicencia.TabIndex = 21;
            this.gbLicencia.TabStop = false;
            this.gbLicencia.Text = "Licencia ICG";
            // 
            // txtLicencia
            // 
            this.txtLicencia.Location = new System.Drawing.Point(134, 23);
            this.txtLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLicencia.Name = "txtLicencia";
            this.txtLicencia.ReadOnly = true;
            this.txtLicencia.Size = new System.Drawing.Size(518, 26);
            this.txtLicencia.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 28);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Licencia ICG";
            // 
            // btKey
            // 
            this.btKey.Location = new System.Drawing.Point(726, 20);
            this.btKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btKey.Name = "btKey";
            this.btKey.Size = new System.Drawing.Size(234, 35);
            this.btKey.TabIndex = 0;
            this.btKey.Text = "Generar Key";
            this.btKey.UseVisualStyleBackColor = true;
            this.btKey.Click += new System.EventHandler(this.btKey_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(31, 585);
            this.btSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(338, 35);
            this.btSave.TabIndex = 22;
            this.btSave.Text = "Guardar Configuración y Salir";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(767, 585);
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(338, 35);
            this.btClose.TabIndex = 23;
            this.btClose.Text = "Salir sin Guardar";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // tcShoppings
            // 
            this.tcShoppings.Controls.Add(this.tpIrsa);
            this.tcShoppings.Controls.Add(this.tpOlmos);
            this.tcShoppings.Controls.Add(this.tpCaballito);
            this.tcShoppings.Controls.Add(this.tpSiTef);
            this.tcShoppings.Location = new System.Drawing.Point(18, 415);
            this.tcShoppings.Name = "tcShoppings";
            this.tcShoppings.SelectedIndex = 0;
            this.tcShoppings.Size = new System.Drawing.Size(1076, 162);
            this.tcShoppings.TabIndex = 24;
            // 
            // tpIrsa
            // 
            this.tpIrsa.Controls.Add(this.btnRegenerarIrsa);
            this.tpIrsa.Controls.Add(this.txtPathIrsa);
            this.tpIrsa.Controls.Add(this.txtRubroIrsa);
            this.tpIrsa.Controls.Add(this.txtPosIrsa);
            this.tpIrsa.Controls.Add(this.txtLocalIrsa);
            this.tpIrsa.Controls.Add(this.txtContratoIrsa);
            this.tpIrsa.Controls.Add(this.label14);
            this.tpIrsa.Controls.Add(this.label13);
            this.tpIrsa.Controls.Add(this.label15);
            this.tpIrsa.Controls.Add(this.label16);
            this.tpIrsa.Controls.Add(this.label17);
            this.tpIrsa.Location = new System.Drawing.Point(4, 29);
            this.tpIrsa.Name = "tpIrsa";
            this.tpIrsa.Padding = new System.Windows.Forms.Padding(3);
            this.tpIrsa.Size = new System.Drawing.Size(1068, 129);
            this.tpIrsa.TabIndex = 0;
            this.tpIrsa.Text = "IRSA Shoppings";
            this.tpIrsa.UseVisualStyleBackColor = true;
            // 
            // btnRegenerarIrsa
            // 
            this.btnRegenerarIrsa.Location = new System.Drawing.Point(768, 69);
            this.btnRegenerarIrsa.Name = "btnRegenerarIrsa";
            this.btnRegenerarIrsa.Size = new System.Drawing.Size(234, 35);
            this.btnRegenerarIrsa.TabIndex = 20;
            this.btnRegenerarIrsa.Text = "Regenerar IRSA";
            this.btnRegenerarIrsa.UseVisualStyleBackColor = true;
            this.btnRegenerarIrsa.Click += new System.EventHandler(this.btnRegenerarIrsa_Click);
            // 
            // txtPathIrsa
            // 
            this.txtPathIrsa.Location = new System.Drawing.Point(167, 58);
            this.txtPathIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPathIrsa.Name = "txtPathIrsa";
            this.txtPathIrsa.Size = new System.Drawing.Size(481, 26);
            this.txtPathIrsa.TabIndex = 19;
            // 
            // txtRubroIrsa
            // 
            this.txtRubroIrsa.Location = new System.Drawing.Point(894, 15);
            this.txtRubroIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRubroIrsa.Name = "txtRubroIrsa";
            this.txtRubroIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtRubroIrsa.TabIndex = 18;
            // 
            // txtPosIrsa
            // 
            this.txtPosIrsa.Location = new System.Drawing.Point(631, 15);
            this.txtPosIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPosIrsa.Name = "txtPosIrsa";
            this.txtPosIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtPosIrsa.TabIndex = 17;
            // 
            // txtLocalIrsa
            // 
            this.txtLocalIrsa.Location = new System.Drawing.Point(358, 15);
            this.txtLocalIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLocalIrsa.Name = "txtLocalIrsa";
            this.txtLocalIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtLocalIrsa.TabIndex = 16;
            // 
            // txtContratoIrsa
            // 
            this.txtContratoIrsa.Location = new System.Drawing.Point(89, 15);
            this.txtContratoIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtContratoIrsa.Name = "txtContratoIrsa";
            this.txtContratoIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtContratoIrsa.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 63);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 20);
            this.label14.TabIndex = 14;
            this.label14.Text = "Ubicación Archivo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(831, 20);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 20);
            this.label13.TabIndex = 13;
            this.label13.Text = "Rubro";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(578, 20);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 20);
            this.label15.TabIndex = 12;
            this.label15.Text = "POS";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(300, 20);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 20);
            this.label16.TabIndex = 11;
            this.label16.Text = "Local";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 20);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 20);
            this.label17.TabIndex = 10;
            this.label17.Text = "Contrato";
            // 
            // tpOlmos
            // 
            this.tpOlmos.Controls.Add(this.btnRegenerarOlmos);
            this.tpOlmos.Controls.Add(this.txtOlmosProceso);
            this.tpOlmos.Controls.Add(this.label23);
            this.tpOlmos.Controls.Add(this.txtOlmosRubro);
            this.tpOlmos.Controls.Add(this.label22);
            this.tpOlmos.Controls.Add(this.txtOlmosNroPos);
            this.tpOlmos.Controls.Add(this.label21);
            this.tpOlmos.Controls.Add(this.txtOlmosNroCliente);
            this.tpOlmos.Controls.Add(this.label20);
            this.tpOlmos.Controls.Add(this.txtOlmosPathSalida);
            this.tpOlmos.Controls.Add(this.label18);
            this.tpOlmos.Location = new System.Drawing.Point(4, 29);
            this.tpOlmos.Name = "tpOlmos";
            this.tpOlmos.Padding = new System.Windows.Forms.Padding(3);
            this.tpOlmos.Size = new System.Drawing.Size(1068, 129);
            this.tpOlmos.TabIndex = 1;
            this.tpOlmos.Text = "Olmos Shopping";
            this.tpOlmos.UseVisualStyleBackColor = true;
            // 
            // btnRegenerarOlmos
            // 
            this.btnRegenerarOlmos.Location = new System.Drawing.Point(700, 84);
            this.btnRegenerarOlmos.Name = "btnRegenerarOlmos";
            this.btnRegenerarOlmos.Size = new System.Drawing.Size(234, 35);
            this.btnRegenerarOlmos.TabIndex = 30;
            this.btnRegenerarOlmos.Text = "Regenerar Olmos";
            this.btnRegenerarOlmos.UseVisualStyleBackColor = true;
            this.btnRegenerarOlmos.Click += new System.EventHandler(this.btnRegenerarOlmos_Click);
            // 
            // txtOlmosProceso
            // 
            this.txtOlmosProceso.Location = new System.Drawing.Point(167, 86);
            this.txtOlmosProceso.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOlmosProceso.Name = "txtOlmosProceso";
            this.txtOlmosProceso.Size = new System.Drawing.Size(311, 26);
            this.txtOlmosProceso.TabIndex = 29;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(55, 89);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 20);
            this.label23.TabIndex = 28;
            this.label23.Text = "Proceso";
            // 
            // txtOlmosRubro
            // 
            this.txtOlmosRubro.Location = new System.Drawing.Point(813, 50);
            this.txtOlmosRubro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOlmosRubro.Name = "txtOlmosRubro";
            this.txtOlmosRubro.Size = new System.Drawing.Size(178, 26);
            this.txtOlmosRubro.TabIndex = 27;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(736, 56);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 20);
            this.label22.TabIndex = 26;
            this.label22.Text = "Rubro";
            // 
            // txtOlmosNroPos
            // 
            this.txtOlmosNroPos.Location = new System.Drawing.Point(511, 50);
            this.txtOlmosNroPos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOlmosNroPos.Name = "txtOlmosNroPos";
            this.txtOlmosNroPos.Size = new System.Drawing.Size(178, 26);
            this.txtOlmosNroPos.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(420, 53);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 20);
            this.label21.TabIndex = 24;
            this.label21.Text = "Nro. POS";
            // 
            // txtOlmosNroCliente
            // 
            this.txtOlmosNroCliente.Location = new System.Drawing.Point(167, 50);
            this.txtOlmosNroCliente.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOlmosNroCliente.Name = "txtOlmosNroCliente";
            this.txtOlmosNroCliente.Size = new System.Drawing.Size(178, 26);
            this.txtOlmosNroCliente.TabIndex = 23;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(55, 53);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 20);
            this.label20.TabIndex = 22;
            this.label20.Text = "Nro. Cliente";
            // 
            // txtOlmosPathSalida
            // 
            this.txtOlmosPathSalida.Location = new System.Drawing.Point(167, 12);
            this.txtOlmosPathSalida.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOlmosPathSalida.Name = "txtOlmosPathSalida";
            this.txtOlmosPathSalida.Size = new System.Drawing.Size(481, 26);
            this.txtOlmosPathSalida.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 17);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(135, 20);
            this.label18.TabIndex = 20;
            this.label18.Text = "Ubicación Archivo";
            // 
            // tpCaballito
            // 
            this.tpCaballito.Controls.Add(this.btnRegenerarCaballito);
            this.tpCaballito.Controls.Add(this.txtCaballitoPathSalida);
            this.tpCaballito.Controls.Add(this.label19);
            this.tpCaballito.Location = new System.Drawing.Point(4, 29);
            this.tpCaballito.Name = "tpCaballito";
            this.tpCaballito.Size = new System.Drawing.Size(1068, 129);
            this.tpCaballito.TabIndex = 2;
            this.tpCaballito.Text = "Caballito Shopping";
            this.tpCaballito.UseVisualStyleBackColor = true;
            // 
            // btnRegenerarCaballito
            // 
            this.btnRegenerarCaballito.Location = new System.Drawing.Point(745, 76);
            this.btnRegenerarCaballito.Name = "btnRegenerarCaballito";
            this.btnRegenerarCaballito.Size = new System.Drawing.Size(234, 35);
            this.btnRegenerarCaballito.TabIndex = 24;
            this.btnRegenerarCaballito.Text = "Regenerar Caballito";
            this.btnRegenerarCaballito.UseVisualStyleBackColor = true;
            this.btnRegenerarCaballito.Click += new System.EventHandler(this.btnRegenerarCaballito_Click);
            // 
            // txtCaballitoPathSalida
            // 
            this.txtCaballitoPathSalida.Location = new System.Drawing.Point(156, 16);
            this.txtCaballitoPathSalida.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCaballitoPathSalida.Name = "txtCaballitoPathSalida";
            this.txtCaballitoPathSalida.Size = new System.Drawing.Size(481, 26);
            this.txtCaballitoPathSalida.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 21);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 20);
            this.label19.TabIndex = 22;
            this.label19.Text = "Ubicación Archivo";
            // 
            // tpSiTef
            // 
            this.tpSiTef.Controls.Add(this.txtIdTerminalSiTef);
            this.tpSiTef.Controls.Add(this.label29);
            this.tpSiTef.Controls.Add(this.txtPathSiTef);
            this.tpSiTef.Controls.Add(this.txtIdTiendaSiTef);
            this.tpSiTef.Controls.Add(this.label28);
            this.tpSiTef.Controls.Add(this.label27);
            this.tpSiTef.Location = new System.Drawing.Point(4, 29);
            this.tpSiTef.Name = "tpSiTef";
            this.tpSiTef.Size = new System.Drawing.Size(1068, 129);
            this.tpSiTef.TabIndex = 3;
            this.tpSiTef.Text = "IRSA SiTef";
            this.tpSiTef.UseVisualStyleBackColor = true;
            // 
            // txtIdTerminalSiTef
            // 
            this.txtIdTerminalSiTef.Location = new System.Drawing.Point(645, 19);
            this.txtIdTerminalSiTef.Name = "txtIdTerminalSiTef";
            this.txtIdTerminalSiTef.Size = new System.Drawing.Size(181, 26);
            this.txtIdTerminalSiTef.TabIndex = 5;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(495, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 20);
            this.label29.TabIndex = 4;
            this.label29.Text = "Id. Terminal";
            // 
            // txtPathSiTef
            // 
            this.txtPathSiTef.Location = new System.Drawing.Point(177, 55);
            this.txtPathSiTef.Name = "txtPathSiTef";
            this.txtPathSiTef.Size = new System.Drawing.Size(460, 26);
            this.txtPathSiTef.TabIndex = 3;
            // 
            // txtIdTiendaSiTef
            // 
            this.txtIdTiendaSiTef.Location = new System.Drawing.Point(177, 19);
            this.txtIdTiendaSiTef.Name = "txtIdTiendaSiTef";
            this.txtIdTiendaSiTef.Size = new System.Drawing.Size(181, 26);
            this.txtIdTiendaSiTef.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(27, 58);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(135, 20);
            this.label28.TabIndex = 1;
            this.label28.Text = "Ubicación Archivo";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(27, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 20);
            this.label27.TabIndex = 0;
            this.label27.Text = "Id. Tienda";
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 638);
            this.Controls.Add(this.tcShoppings);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.gbLicencia);
            this.Controls.Add(this.gbFacturacion);
            this.Controls.Add(this.gbDataBase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - Facturación Electrónica";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.gbDataBase.ResumeLayout(false);
            this.gbDataBase.PerformLayout();
            this.gbFacturacion.ResumeLayout(false);
            this.gbFacturacion.PerformLayout();
            this.gbLicencia.ResumeLayout(false);
            this.gbLicencia.PerformLayout();
            this.tcShoppings.ResumeLayout(false);
            this.tpIrsa.ResumeLayout(false);
            this.tpIrsa.PerformLayout();
            this.tpOlmos.ResumeLayout(false);
            this.tpOlmos.PerformLayout();
            this.tpCaballito.ResumeLayout(false);
            this.tpCaballito.PerformLayout();
            this.tpSiTef.ResumeLayout(false);
            this.tpSiTef.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDataBase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblDatebase;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.GroupBox gbFacturacion;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCertificado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chEsTest;
        private System.Windows.Forms.TextBox txtProVtaCAE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPtoVtaManual;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chSoloCAEA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPtoVtaCAEA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbLicencia;
        private System.Windows.Forms.TextBox txtLicencia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btKey;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.CheckBox chMtxca;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chMonotributista;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chDesdoblaPagos;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabControl tcShoppings;
        private System.Windows.Forms.TabPage tpIrsa;
        private System.Windows.Forms.TextBox txtPathIrsa;
        private System.Windows.Forms.TextBox txtRubroIrsa;
        private System.Windows.Forms.TextBox txtPosIrsa;
        private System.Windows.Forms.TextBox txtLocalIrsa;
        private System.Windows.Forms.TextBox txtContratoIrsa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tpOlmos;
        private System.Windows.Forms.TabPage tpCaballito;
        private System.Windows.Forms.TextBox txtOlmosPathSalida;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCaballitoPathSalida;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtOlmosNroCliente;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtOlmosNroPos;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtOlmosProceso;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtOlmosRubro;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnRegenerarIrsa;
        private System.Windows.Forms.Button btnRegenerarOlmos;
        private System.Windows.Forms.Button btnRegenerarCaballito;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCaja;
        private System.Windows.Forms.TextBox txtCantidadTiquets;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMontoMaximo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tpSiTef;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPathSiTef;
        private System.Windows.Forms.TextBox txtIdTiendaSiTef;
        private System.Windows.Forms.TextBox txtIdTerminalSiTef;
        private System.Windows.Forms.Label label29;
    }
}