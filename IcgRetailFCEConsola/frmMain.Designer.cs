﻿namespace IcgRetailFCEConsola
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpComprobantes = new System.Windows.Forms.TabPage();
            this.gbAcciones = new System.Windows.Forms.GroupBox();
            this.btnFcManuales = new System.Windows.Forms.Button();
            this.btConfig = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btReimprimir = new System.Windows.Forms.Button();
            this.grComprobantes = new System.Windows.Forms.DataGridView();
            this.N = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMFACTURA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMALBARAN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODVENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRECLIENTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALBRUTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALNETO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESTADO_FE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ERROR_CAE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMEROFISCAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIEFISCAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIEFISCAL2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIPCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtsMain = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataColumn35 = new System.Data.DataColumn();
            this.dataColumn36 = new System.Data.DataColumn();
            this.dataColumn37 = new System.Data.DataColumn();
            this.dataColumn38 = new System.Data.DataColumn();
            this.tpCaea = new System.Windows.Forms.TabPage();
            this.btSolicitarCAEA = new System.Windows.Forms.Button();
            this.grCaea = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaProcesoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDesdeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaHastaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaTopeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quincenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpInfomarCaea = new System.Windows.Forms.TabPage();
            this.btRefrescarSinInfomar = new System.Windows.Forms.Button();
            this.btCaeaInformar = new System.Windows.Forms.Button();
            this.grSinInformar = new System.Windows.Forms.DataGridView();
            this.fODataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eRRORCAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eSTADOFEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEAINFORMADODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vTOCAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuCAEA = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarNumeroFiscalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarFechaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCAEA = new System.Windows.Forms.Button();
            this.tcMain.SuspendLayout();
            this.tpComprobantes.SuspendLayout();
            this.gbAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grComprobantes)).BeginInit();
            this.contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            this.tpCaea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCaea)).BeginInit();
            this.tpInfomarCaea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grSinInformar)).BeginInit();
            this.contextMenuCAEA.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpComprobantes);
            this.tcMain.Controls.Add(this.tpCaea);
            this.tcMain.Controls.Add(this.tpInfomarCaea);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1200, 692);
            this.tcMain.TabIndex = 0;
            // 
            // tpComprobantes
            // 
            this.tpComprobantes.Controls.Add(this.gbAcciones);
            this.tpComprobantes.Controls.Add(this.grComprobantes);
            this.tpComprobantes.Location = new System.Drawing.Point(4, 29);
            this.tpComprobantes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpComprobantes.Name = "tpComprobantes";
            this.tpComprobantes.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpComprobantes.Size = new System.Drawing.Size(1192, 659);
            this.tpComprobantes.TabIndex = 0;
            this.tpComprobantes.Text = "Comprobantes Sin Fiscalizar";
            this.tpComprobantes.UseVisualStyleBackColor = true;
            // 
            // gbAcciones
            // 
            this.gbAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAcciones.BackColor = System.Drawing.Color.Transparent;
            this.gbAcciones.Controls.Add(this.btnCAEA);
            this.gbAcciones.Controls.Add(this.btnFcManuales);
            this.gbAcciones.Controls.Add(this.btConfig);
            this.gbAcciones.Controls.Add(this.btImprimir);
            this.gbAcciones.Controls.Add(this.btReimprimir);
            this.gbAcciones.Location = new System.Drawing.Point(12, 549);
            this.gbAcciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Name = "gbAcciones";
            this.gbAcciones.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Size = new System.Drawing.Size(1164, 86);
            this.gbAcciones.TabIndex = 15;
            this.gbAcciones.TabStop = false;
            this.gbAcciones.Text = "ACCIONES";
            // 
            // btnFcManuales
            // 
            this.btnFcManuales.Location = new System.Drawing.Point(621, 29);
            this.btnFcManuales.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnFcManuales.Name = "btnFcManuales";
            this.btnFcManuales.Size = new System.Drawing.Size(243, 35);
            this.btnFcManuales.TabIndex = 4;
            this.btnFcManuales.Text = "Activar FC Manuales";
            this.btnFcManuales.UseVisualStyleBackColor = true;
            this.btnFcManuales.Click += new System.EventHandler(this.btnFcManuales_Click);
            // 
            // btConfig
            // 
            this.btConfig.Location = new System.Drawing.Point(417, 29);
            this.btConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btConfig.Name = "btConfig";
            this.btConfig.Size = new System.Drawing.Size(177, 35);
            this.btConfig.TabIndex = 3;
            this.btConfig.Text = "Configuración";
            this.btConfig.UseVisualStyleBackColor = true;
            this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.Location = new System.Drawing.Point(213, 29);
            this.btImprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(177, 35);
            this.btImprimir.TabIndex = 1;
            this.btImprimir.Text = "Fiscalizar";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btReimprimir
            // 
            this.btReimprimir.Location = new System.Drawing.Point(9, 29);
            this.btReimprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btReimprimir.Name = "btReimprimir";
            this.btReimprimir.Size = new System.Drawing.Size(177, 35);
            this.btReimprimir.TabIndex = 0;
            this.btReimprimir.Text = "Refrescar Grilla";
            this.btReimprimir.UseVisualStyleBackColor = true;
            this.btReimprimir.Click += new System.EventHandler(this.btReimprimir_Click);
            // 
            // grComprobantes
            // 
            this.grComprobantes.AllowUserToAddRows = false;
            this.grComprobantes.AllowUserToDeleteRows = false;
            this.grComprobantes.AutoGenerateColumns = false;
            this.grComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.N,
            this.NUMSERIE,
            this.NUMFACTURA,
            this.NUMALBARAN,
            this.CODVENDEDOR,
            this.FECHA,
            this.NOMBRECLIENTE,
            this.TOTALBRUTO,
            this.TOTALNETO,
            this.ESTADO_FE,
            this.ERROR_CAE,
            this.NUMEROFISCAL,
            this.SERIEFISCAL,
            this.SERIEFISCAL2,
            this.DESCRIPCION,
            this.Z});
            this.grComprobantes.ContextMenuStrip = this.contextMenu;
            this.grComprobantes.DataMember = "Comprobantes";
            this.grComprobantes.DataSource = this.dtsMain;
            this.grComprobantes.Dock = System.Windows.Forms.DockStyle.Top;
            this.grComprobantes.Location = new System.Drawing.Point(4, 5);
            this.grComprobantes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grComprobantes.Name = "grComprobantes";
            this.grComprobantes.ReadOnly = true;
            this.grComprobantes.RowHeadersWidth = 62;
            this.grComprobantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grComprobantes.Size = new System.Drawing.Size(1184, 508);
            this.grComprobantes.TabIndex = 0;
            this.grComprobantes.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grComprobantes_CellFormatting);
            this.grComprobantes.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.grComprobantes_CellPainting);
            // 
            // N
            // 
            this.N.DataPropertyName = "N";
            this.N.HeaderText = "N";
            this.N.MinimumWidth = 8;
            this.N.Name = "N";
            this.N.ReadOnly = true;
            this.N.Width = 50;
            // 
            // NUMSERIE
            // 
            this.NUMSERIE.DataPropertyName = "NUMSERIE";
            this.NUMSERIE.HeaderText = "Serie";
            this.NUMSERIE.MinimumWidth = 8;
            this.NUMSERIE.Name = "NUMSERIE";
            this.NUMSERIE.ReadOnly = true;
            this.NUMSERIE.Width = 80;
            // 
            // NUMFACTURA
            // 
            this.NUMFACTURA.DataPropertyName = "NUMFACTURA";
            this.NUMFACTURA.HeaderText = "Numero";
            this.NUMFACTURA.MinimumWidth = 8;
            this.NUMFACTURA.Name = "NUMFACTURA";
            this.NUMFACTURA.ReadOnly = true;
            this.NUMFACTURA.Width = 80;
            // 
            // NUMALBARAN
            // 
            this.NUMALBARAN.DataPropertyName = "NUMALBARAN";
            this.NUMALBARAN.HeaderText = "Numero Alb";
            this.NUMALBARAN.MinimumWidth = 8;
            this.NUMALBARAN.Name = "NUMALBARAN";
            this.NUMALBARAN.ReadOnly = true;
            this.NUMALBARAN.Visible = false;
            this.NUMALBARAN.Width = 80;
            // 
            // CODVENDEDOR
            // 
            this.CODVENDEDOR.DataPropertyName = "CODVENDEDOR";
            this.CODVENDEDOR.HeaderText = "Cod. Vendedor";
            this.CODVENDEDOR.MinimumWidth = 8;
            this.CODVENDEDOR.Name = "CODVENDEDOR";
            this.CODVENDEDOR.ReadOnly = true;
            this.CODVENDEDOR.Width = 50;
            // 
            // FECHA
            // 
            this.FECHA.DataPropertyName = "FECHA";
            this.FECHA.HeaderText = "Fecha";
            this.FECHA.MinimumWidth = 8;
            this.FECHA.Name = "FECHA";
            this.FECHA.ReadOnly = true;
            this.FECHA.Width = 150;
            // 
            // NOMBRECLIENTE
            // 
            this.NOMBRECLIENTE.DataPropertyName = "NOMBRECLIENTE";
            this.NOMBRECLIENTE.HeaderText = "Cliente";
            this.NOMBRECLIENTE.MinimumWidth = 8;
            this.NOMBRECLIENTE.Name = "NOMBRECLIENTE";
            this.NOMBRECLIENTE.ReadOnly = true;
            this.NOMBRECLIENTE.Width = 150;
            // 
            // TOTALBRUTO
            // 
            this.TOTALBRUTO.DataPropertyName = "TOTALBRUTO";
            this.TOTALBRUTO.HeaderText = "Bruto";
            this.TOTALBRUTO.MinimumWidth = 8;
            this.TOTALBRUTO.Name = "TOTALBRUTO";
            this.TOTALBRUTO.ReadOnly = true;
            this.TOTALBRUTO.Width = 150;
            // 
            // TOTALNETO
            // 
            this.TOTALNETO.DataPropertyName = "TOTALNETO";
            this.TOTALNETO.HeaderText = "Neto";
            this.TOTALNETO.MinimumWidth = 8;
            this.TOTALNETO.Name = "TOTALNETO";
            this.TOTALNETO.ReadOnly = true;
            this.TOTALNETO.Width = 150;
            // 
            // ESTADO_FE
            // 
            this.ESTADO_FE.DataPropertyName = "ESTADO_FE";
            this.ESTADO_FE.HeaderText = "Estado FE";
            this.ESTADO_FE.MinimumWidth = 8;
            this.ESTADO_FE.Name = "ESTADO_FE";
            this.ESTADO_FE.ReadOnly = true;
            this.ESTADO_FE.ToolTipText = "Indica el estado de la factura con la AFIP";
            this.ESTADO_FE.Width = 150;
            // 
            // ERROR_CAE
            // 
            this.ERROR_CAE.DataPropertyName = "ERROR_CAE";
            this.ERROR_CAE.HeaderText = "Error AFIP";
            this.ERROR_CAE.MinimumWidth = 8;
            this.ERROR_CAE.Name = "ERROR_CAE";
            this.ERROR_CAE.ReadOnly = true;
            this.ERROR_CAE.Width = 250;
            // 
            // NUMEROFISCAL
            // 
            this.NUMEROFISCAL.DataPropertyName = "NUMEROFISCAL";
            this.NUMEROFISCAL.HeaderText = "Nro. Fiscal";
            this.NUMEROFISCAL.MinimumWidth = 8;
            this.NUMEROFISCAL.Name = "NUMEROFISCAL";
            this.NUMEROFISCAL.ReadOnly = true;
            this.NUMEROFISCAL.Visible = false;
            this.NUMEROFISCAL.Width = 150;
            // 
            // SERIEFISCAL
            // 
            this.SERIEFISCAL.DataPropertyName = "SERIEFISCAL";
            this.SERIEFISCAL.HeaderText = "Punto Venta";
            this.SERIEFISCAL.MinimumWidth = 8;
            this.SERIEFISCAL.Name = "SERIEFISCAL";
            this.SERIEFISCAL.ReadOnly = true;
            this.SERIEFISCAL.Visible = false;
            this.SERIEFISCAL.Width = 150;
            // 
            // SERIEFISCAL2
            // 
            this.SERIEFISCAL2.DataPropertyName = "SERIEFISCAL2";
            this.SERIEFISCAL2.HeaderText = "Tipo Cbte.";
            this.SERIEFISCAL2.MinimumWidth = 8;
            this.SERIEFISCAL2.Name = "SERIEFISCAL2";
            this.SERIEFISCAL2.ReadOnly = true;
            this.SERIEFISCAL2.Visible = false;
            this.SERIEFISCAL2.Width = 150;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.DataPropertyName = "DESCRIPCION";
            this.DESCRIPCION.HeaderText = "DESCRIPCION";
            this.DESCRIPCION.MinimumWidth = 8;
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.ReadOnly = true;
            this.DESCRIPCION.Width = 150;
            // 
            // Z
            // 
            this.Z.DataPropertyName = "Z";
            this.Z.HeaderText = "Z";
            this.Z.MinimumWidth = 8;
            this.Z.Name = "Z";
            this.Z.ReadOnly = true;
            this.Z.Width = 50;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFechaToolStripMenuItem,
            this.borrarToolStripMenuItem,
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(378, 100);
            // 
            // cambiarFechaToolStripMenuItem
            // 
            this.cambiarFechaToolStripMenuItem.Name = "cambiarFechaToolStripMenuItem";
            this.cambiarFechaToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.cambiarFechaToolStripMenuItem.Text = "Cambiar Fecha";
            this.cambiarFechaToolStripMenuItem.Click += new System.EventHandler(this.cambiarFechaToolStripMenuItem_Click);
            // 
            // borrarToolStripMenuItem
            // 
            this.borrarToolStripMenuItem.Name = "borrarToolStripMenuItem";
            this.borrarToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.borrarToolStripMenuItem.Text = "Borrar";
            this.borrarToolStripMenuItem.Click += new System.EventHandler(this.borrarToolStripMenuItem_Click);
            // 
            // consultarÚltimoComprobanteCAEAToolStripMenuItem
            // 
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem.Name = "consultarÚltimoComprobanteCAEAToolStripMenuItem";
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem.Text = "Consultar último comprobante CAEA";
            this.consultarÚltimoComprobanteCAEAToolStripMenuItem.Click += new System.EventHandler(this.consultarÚltimoComprobanteCAEAToolStripMenuItem_Click);
            // 
            // dtsMain
            // 
            this.dtsMain.DataSetName = "NewDataSet";
            this.dtsMain.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn1});
            this.dataTable1.TableName = "Comprobantes";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "SERIE";
            this.dataColumn2.ColumnName = "NUMSERIE";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "NUMERO";
            this.dataColumn3.ColumnName = "NUMALBARAN";
            this.dataColumn3.DataType = typeof(int);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "N";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "NUMFACTURA";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "TOTALBRUTO";
            this.dataColumn6.DataType = typeof(decimal);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "TOTALNETO";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "NOMBRECLIENTE";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "SERIEFISCAL";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "NUMEROFISCAL";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "DESCRIPCION";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "FECHA";
            this.dataColumn13.DataType = typeof(System.DateTime);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "Z";
            this.dataColumn14.DataType = typeof(int);
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "CODVENDEDOR";
            this.dataColumn15.DataType = typeof(int);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "CAE";
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "ESTADO_FE";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Error AFIP";
            this.dataColumn1.ColumnName = "ERROR_CAE";
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25});
            this.dataTable2.TableName = "Caea";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "ID";
            this.dataColumn18.DataType = typeof(int);
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "CAEA";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "FechaProceso";
            this.dataColumn20.DataType = typeof(System.DateTime);
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "FechaDesde";
            this.dataColumn21.DataType = typeof(System.DateTime);
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "FechaHasta";
            this.dataColumn22.DataType = typeof(System.DateTime);
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "FechaTope";
            this.dataColumn23.DataType = typeof(System.DateTime);
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "Periodo";
            this.dataColumn24.DataType = typeof(int);
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnName = "Quincena";
            this.dataColumn25.DataType = typeof(int);
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35,
            this.dataColumn36,
            this.dataColumn37,
            this.dataColumn38});
            this.dataTable3.TableName = "SinInformar";
            // 
            // dataColumn26
            // 
            this.dataColumn26.Caption = "SERIE";
            this.dataColumn26.ColumnName = "NUMSERIE";
            // 
            // dataColumn27
            // 
            this.dataColumn27.Caption = "NUMERO";
            this.dataColumn27.ColumnName = "NUMALBARAN";
            this.dataColumn27.DataType = typeof(int);
            // 
            // dataColumn28
            // 
            this.dataColumn28.ColumnName = "N";
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "FO";
            this.dataColumn29.DataType = typeof(int);
            // 
            // dataColumn30
            // 
            this.dataColumn30.ColumnName = "CAE";
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnName = "ERROR_CAE";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnName = "VTO_CAE";
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "ESTADO_FE";
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "CAEA_INFORMADO";
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "FECHA";
            this.dataColumn35.DataType = typeof(System.DateTime);
            // 
            // dataColumn36
            // 
            this.dataColumn36.ColumnName = "SERIEFISCAL";
            // 
            // dataColumn37
            // 
            this.dataColumn37.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn38
            // 
            this.dataColumn38.ColumnName = "NUMEROFISCAL";
            // 
            // tpCaea
            // 
            this.tpCaea.Controls.Add(this.btSolicitarCAEA);
            this.tpCaea.Controls.Add(this.grCaea);
            this.tpCaea.Location = new System.Drawing.Point(4, 29);
            this.tpCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCaea.Name = "tpCaea";
            this.tpCaea.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCaea.Size = new System.Drawing.Size(1192, 659);
            this.tpCaea.TabIndex = 1;
            this.tpCaea.Text = "Administración de CAEA";
            this.tpCaea.UseVisualStyleBackColor = true;
            // 
            // btSolicitarCAEA
            // 
            this.btSolicitarCAEA.Location = new System.Drawing.Point(393, 548);
            this.btSolicitarCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSolicitarCAEA.Name = "btSolicitarCAEA";
            this.btSolicitarCAEA.Size = new System.Drawing.Size(314, 35);
            this.btSolicitarCAEA.TabIndex = 1;
            this.btSolicitarCAEA.Text = "Solicitar Nuevo CAEA para el período";
            this.btSolicitarCAEA.UseVisualStyleBackColor = true;
            this.btSolicitarCAEA.Click += new System.EventHandler(this.btSolicitarCAEA_Click);
            // 
            // grCaea
            // 
            this.grCaea.AllowUserToAddRows = false;
            this.grCaea.AllowUserToDeleteRows = false;
            this.grCaea.AutoGenerateColumns = false;
            this.grCaea.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grCaea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grCaea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.cAEADataGridViewTextBoxColumn,
            this.fechaProcesoDataGridViewTextBoxColumn,
            this.fechaDesdeDataGridViewTextBoxColumn,
            this.fechaHastaDataGridViewTextBoxColumn,
            this.fechaTopeDataGridViewTextBoxColumn,
            this.periodoDataGridViewTextBoxColumn,
            this.quincenaDataGridViewTextBoxColumn});
            this.grCaea.DataMember = "Caea";
            this.grCaea.DataSource = this.dtsMain;
            this.grCaea.Dock = System.Windows.Forms.DockStyle.Top;
            this.grCaea.Location = new System.Drawing.Point(4, 5);
            this.grCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grCaea.MultiSelect = false;
            this.grCaea.Name = "grCaea";
            this.grCaea.ReadOnly = true;
            this.grCaea.RowHeadersWidth = 62;
            this.grCaea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grCaea.Size = new System.Drawing.Size(1184, 500);
            this.grCaea.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // cAEADataGridViewTextBoxColumn
            // 
            this.cAEADataGridViewTextBoxColumn.DataPropertyName = "CAEA";
            this.cAEADataGridViewTextBoxColumn.HeaderText = "CAEA";
            this.cAEADataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEADataGridViewTextBoxColumn.Name = "cAEADataGridViewTextBoxColumn";
            this.cAEADataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaProcesoDataGridViewTextBoxColumn
            // 
            this.fechaProcesoDataGridViewTextBoxColumn.DataPropertyName = "FechaProceso";
            this.fechaProcesoDataGridViewTextBoxColumn.HeaderText = "Fecha Proceso";
            this.fechaProcesoDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaProcesoDataGridViewTextBoxColumn.Name = "fechaProcesoDataGridViewTextBoxColumn";
            this.fechaProcesoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDesdeDataGridViewTextBoxColumn
            // 
            this.fechaDesdeDataGridViewTextBoxColumn.DataPropertyName = "FechaDesde";
            this.fechaDesdeDataGridViewTextBoxColumn.HeaderText = "Fecha Desde";
            this.fechaDesdeDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaDesdeDataGridViewTextBoxColumn.Name = "fechaDesdeDataGridViewTextBoxColumn";
            this.fechaDesdeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaHastaDataGridViewTextBoxColumn
            // 
            this.fechaHastaDataGridViewTextBoxColumn.DataPropertyName = "FechaHasta";
            this.fechaHastaDataGridViewTextBoxColumn.HeaderText = "Fecha Hasta";
            this.fechaHastaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaHastaDataGridViewTextBoxColumn.Name = "fechaHastaDataGridViewTextBoxColumn";
            this.fechaHastaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaTopeDataGridViewTextBoxColumn
            // 
            this.fechaTopeDataGridViewTextBoxColumn.DataPropertyName = "FechaTope";
            this.fechaTopeDataGridViewTextBoxColumn.HeaderText = "Fecha Tope";
            this.fechaTopeDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaTopeDataGridViewTextBoxColumn.Name = "fechaTopeDataGridViewTextBoxColumn";
            this.fechaTopeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // periodoDataGridViewTextBoxColumn
            // 
            this.periodoDataGridViewTextBoxColumn.DataPropertyName = "Periodo";
            this.periodoDataGridViewTextBoxColumn.HeaderText = "Periodo";
            this.periodoDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.periodoDataGridViewTextBoxColumn.Name = "periodoDataGridViewTextBoxColumn";
            this.periodoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quincenaDataGridViewTextBoxColumn
            // 
            this.quincenaDataGridViewTextBoxColumn.DataPropertyName = "Quincena";
            this.quincenaDataGridViewTextBoxColumn.HeaderText = "Quincena";
            this.quincenaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.quincenaDataGridViewTextBoxColumn.Name = "quincenaDataGridViewTextBoxColumn";
            this.quincenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tpInfomarCaea
            // 
            this.tpInfomarCaea.Controls.Add(this.btRefrescarSinInfomar);
            this.tpInfomarCaea.Controls.Add(this.btCaeaInformar);
            this.tpInfomarCaea.Controls.Add(this.grSinInformar);
            this.tpInfomarCaea.Location = new System.Drawing.Point(4, 29);
            this.tpInfomarCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpInfomarCaea.Name = "tpInfomarCaea";
            this.tpInfomarCaea.Size = new System.Drawing.Size(1192, 659);
            this.tpInfomarCaea.TabIndex = 2;
            this.tpInfomarCaea.Text = "CAEA Sin Informar";
            this.tpInfomarCaea.UseVisualStyleBackColor = true;
            // 
            // btRefrescarSinInfomar
            // 
            this.btRefrescarSinInfomar.Location = new System.Drawing.Point(48, 578);
            this.btRefrescarSinInfomar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btRefrescarSinInfomar.Name = "btRefrescarSinInfomar";
            this.btRefrescarSinInfomar.Size = new System.Drawing.Size(192, 35);
            this.btRefrescarSinInfomar.TabIndex = 2;
            this.btRefrescarSinInfomar.Text = "Refrescar Grilla";
            this.btRefrescarSinInfomar.UseVisualStyleBackColor = true;
            this.btRefrescarSinInfomar.Click += new System.EventHandler(this.btRefrescarSinInfomar_Click);
            // 
            // btCaeaInformar
            // 
            this.btCaeaInformar.Location = new System.Drawing.Point(264, 578);
            this.btCaeaInformar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCaeaInformar.Name = "btCaeaInformar";
            this.btCaeaInformar.Size = new System.Drawing.Size(192, 35);
            this.btCaeaInformar.TabIndex = 1;
            this.btCaeaInformar.Text = "Informar CAEA";
            this.btCaeaInformar.UseVisualStyleBackColor = true;
            this.btCaeaInformar.Click += new System.EventHandler(this.btCaeaInformar_Click2);
            // 
            // grSinInformar
            // 
            this.grSinInformar.AllowUserToAddRows = false;
            this.grSinInformar.AllowUserToDeleteRows = false;
            this.grSinInformar.AllowUserToResizeRows = false;
            this.grSinInformar.AutoGenerateColumns = false;
            this.grSinInformar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grSinInformar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fODataGridViewTextBoxColumn1,
            this.Serie,
            this.dataGridViewTextBoxColumn1,
            this.nDataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.cAEDataGridViewTextBoxColumn,
            this.eRRORCAEDataGridViewTextBoxColumn,
            this.eSTADOFEDataGridViewTextBoxColumn,
            this.cAEAINFORMADODataGridViewTextBoxColumn,
            this.vTOCAEDataGridViewTextBoxColumn});
            this.grSinInformar.ContextMenuStrip = this.contextMenuCAEA;
            this.grSinInformar.DataMember = "SinInformar";
            this.grSinInformar.DataSource = this.dtsMain;
            this.grSinInformar.Dock = System.Windows.Forms.DockStyle.Top;
            this.grSinInformar.Location = new System.Drawing.Point(0, 0);
            this.grSinInformar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grSinInformar.MultiSelect = false;
            this.grSinInformar.Name = "grSinInformar";
            this.grSinInformar.ReadOnly = true;
            this.grSinInformar.RowHeadersWidth = 62;
            this.grSinInformar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grSinInformar.Size = new System.Drawing.Size(1192, 552);
            this.grSinInformar.TabIndex = 0;
            // 
            // fODataGridViewTextBoxColumn1
            // 
            this.fODataGridViewTextBoxColumn1.DataPropertyName = "FO";
            this.fODataGridViewTextBoxColumn1.HeaderText = "Fo";
            this.fODataGridViewTextBoxColumn1.MinimumWidth = 8;
            this.fODataGridViewTextBoxColumn1.Name = "fODataGridViewTextBoxColumn1";
            this.fODataGridViewTextBoxColumn1.ReadOnly = true;
            this.fODataGridViewTextBoxColumn1.Width = 80;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "NUMSERIE";
            this.Serie.HeaderText = "Serie";
            this.Serie.MinimumWidth = 8;
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            this.Serie.Width = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NUMALBARAN";
            this.dataGridViewTextBoxColumn1.HeaderText = "Número";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 80;
            // 
            // nDataGridViewTextBoxColumn1
            // 
            this.nDataGridViewTextBoxColumn1.DataPropertyName = "N";
            this.nDataGridViewTextBoxColumn1.HeaderText = "N";
            this.nDataGridViewTextBoxColumn1.MinimumWidth = 8;
            this.nDataGridViewTextBoxColumn1.Name = "nDataGridViewTextBoxColumn1";
            this.nDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nDataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FECHA";
            this.dataGridViewTextBoxColumn2.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SERIEFISCAL";
            this.dataGridViewTextBoxColumn3.HeaderText = "Serie Fiscal";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "SERIEFISCAL2";
            this.dataGridViewTextBoxColumn4.HeaderText = "Serie Fiscal 2";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "NUMEROFISCAL";
            this.dataGridViewTextBoxColumn6.HeaderText = "Nro. Fiscal";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // cAEDataGridViewTextBoxColumn
            // 
            this.cAEDataGridViewTextBoxColumn.DataPropertyName = "CAE";
            this.cAEDataGridViewTextBoxColumn.HeaderText = "CAE";
            this.cAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEDataGridViewTextBoxColumn.Name = "cAEDataGridViewTextBoxColumn";
            this.cAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.cAEDataGridViewTextBoxColumn.Width = 150;
            // 
            // eRRORCAEDataGridViewTextBoxColumn
            // 
            this.eRRORCAEDataGridViewTextBoxColumn.DataPropertyName = "ERROR_CAE";
            this.eRRORCAEDataGridViewTextBoxColumn.HeaderText = "Error";
            this.eRRORCAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.eRRORCAEDataGridViewTextBoxColumn.Name = "eRRORCAEDataGridViewTextBoxColumn";
            this.eRRORCAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.eRRORCAEDataGridViewTextBoxColumn.Width = 350;
            // 
            // eSTADOFEDataGridViewTextBoxColumn
            // 
            this.eSTADOFEDataGridViewTextBoxColumn.DataPropertyName = "ESTADO_FE";
            this.eSTADOFEDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.eSTADOFEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.eSTADOFEDataGridViewTextBoxColumn.Name = "eSTADOFEDataGridViewTextBoxColumn";
            this.eSTADOFEDataGridViewTextBoxColumn.ReadOnly = true;
            this.eSTADOFEDataGridViewTextBoxColumn.Width = 150;
            // 
            // cAEAINFORMADODataGridViewTextBoxColumn
            // 
            this.cAEAINFORMADODataGridViewTextBoxColumn.DataPropertyName = "CAEA_INFORMADO";
            this.cAEAINFORMADODataGridViewTextBoxColumn.HeaderText = "CAEA Informado";
            this.cAEAINFORMADODataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEAINFORMADODataGridViewTextBoxColumn.Name = "cAEAINFORMADODataGridViewTextBoxColumn";
            this.cAEAINFORMADODataGridViewTextBoxColumn.ReadOnly = true;
            this.cAEAINFORMADODataGridViewTextBoxColumn.Width = 150;
            // 
            // vTOCAEDataGridViewTextBoxColumn
            // 
            this.vTOCAEDataGridViewTextBoxColumn.DataPropertyName = "VTO_CAE";
            this.vTOCAEDataGridViewTextBoxColumn.HeaderText = "Vto CAE";
            this.vTOCAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.vTOCAEDataGridViewTextBoxColumn.Name = "vTOCAEDataGridViewTextBoxColumn";
            this.vTOCAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.vTOCAEDataGridViewTextBoxColumn.Width = 150;
            // 
            // contextMenuCAEA
            // 
            this.contextMenuCAEA.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuCAEA.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarNumeroFiscalToolStripMenuItem,
            this.cambiarFechaToolStripMenuItem1});
            this.contextMenuCAEA.Name = "contextMenuCAEA";
            this.contextMenuCAEA.Size = new System.Drawing.Size(268, 68);
            // 
            // cambiarNumeroFiscalToolStripMenuItem
            // 
            this.cambiarNumeroFiscalToolStripMenuItem.Name = "cambiarNumeroFiscalToolStripMenuItem";
            this.cambiarNumeroFiscalToolStripMenuItem.Size = new System.Drawing.Size(267, 32);
            this.cambiarNumeroFiscalToolStripMenuItem.Text = "Cambiar Numero Fiscal";
            this.cambiarNumeroFiscalToolStripMenuItem.Click += new System.EventHandler(this.cambiarNumeroFiscalToolStripMenuItem_Click);
            // 
            // cambiarFechaToolStripMenuItem1
            // 
            this.cambiarFechaToolStripMenuItem1.Name = "cambiarFechaToolStripMenuItem1";
            this.cambiarFechaToolStripMenuItem1.Size = new System.Drawing.Size(267, 32);
            this.cambiarFechaToolStripMenuItem1.Text = "Cambiar Fecha";
            this.cambiarFechaToolStripMenuItem1.Click += new System.EventHandler(this.cambiarFechaToolStripMenuItem1_Click);
            // 
            // btnCAEA
            // 
            this.btnCAEA.Location = new System.Drawing.Point(891, 29);
            this.btnCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCAEA.Name = "btnCAEA";
            this.btnCAEA.Size = new System.Drawing.Size(224, 35);
            this.btnCAEA.TabIndex = 5;
            this.btnCAEA.Text = "Activar CAEA";
            this.btnCAEA.UseVisualStyleBackColor = true;
            this.btnCAEA.Click += new System.EventHandler(this.btnCAEA_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.tcMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tcMain.ResumeLayout(false);
            this.tpComprobantes.ResumeLayout(false);
            this.gbAcciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grComprobantes)).EndInit();
            this.contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            this.tpCaea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCaea)).EndInit();
            this.tpInfomarCaea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grSinInformar)).EndInit();
            this.contextMenuCAEA.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpComprobantes;
        private System.Windows.Forms.TabPage tpCaea;
        private System.Windows.Forms.DataGridView grComprobantes;
        private System.Windows.Forms.DataGridView grCaea;
        private System.Data.DataSet dtsMain;
        private System.Data.DataTable dataTable1;
        private System.Data.DataTable dataTable2;
        private System.Windows.Forms.GroupBox gbAcciones;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btReimprimir;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private System.Windows.Forms.TabPage tpInfomarCaea;
        private System.Windows.Forms.Button btSolicitarCAEA;
        private System.Windows.Forms.Button btCaeaInformar;
        private System.Windows.Forms.DataGridView grSinInformar;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataColumn dataColumn35;
        private System.Data.DataColumn dataColumn36;
        private System.Data.DataColumn dataColumn37;
        private System.Data.DataColumn dataColumn38;
        private System.Windows.Forms.Button btRefrescarSinInfomar;
        private System.Windows.Forms.Button btConfig;
        private System.Data.DataColumn dataColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaProcesoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDesdeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaHastaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaTopeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quincenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnFcManuales;
        private System.Windows.Forms.DataGridViewTextBoxColumn N;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMFACTURA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMALBARAN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODVENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRECLIENTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALBRUTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALNETO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESTADO_FE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ERROR_CAE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMEROFISCAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIEFISCAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIEFISCAL2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIPCION;
        private System.Windows.Forms.DataGridViewTextBoxColumn Z;
        private System.Windows.Forms.DataGridViewTextBoxColumn fODataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eRRORCAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eSTADOFEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEAINFORMADODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vTOCAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarÚltimoComprobanteCAEAToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuCAEA;
        private System.Windows.Forms.ToolStripMenuItem cambiarNumeroFiscalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaToolStripMenuItem1;
        private System.Windows.Forms.Button btnCAEA;
    }
}