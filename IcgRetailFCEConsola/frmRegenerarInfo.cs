﻿using IcgFceDll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailFCEConsola
{
    public partial class frmRegenerarInfo : Form
    {
        /// <summary>
        /// Nombre del Shopping.
        /// </summary>
        string _shoppingName;
        /// <summary>
        /// Clase con la información de IRSA.
        /// </summary>
        IcgFceDll.InfoIrsa _infoIrsa = new IcgFceDll.InfoIrsa();
        /// <summary>
        /// Directorio donde escribir el archivo para Olmos.
        /// </summary>
        string _olmosDirectorio;
        /// <summary>
        /// Numero de cliente del Shooping Olmos.
        /// </summary>
        string _olmosNroCliente;
        /// <summary>
        /// Numero de POS del shopping Olmos.
        /// </summary>
        string _olmosNroPOS;
        /// <summary>
        /// Nro de Rubro del shopping Olmos.
        /// </summary>
        string _olmosNroRubro;
        /// <summary>
        /// Nombre del proceso a ejecutar para enviar la info al shopping Olmos.
        /// </summary>
        string _olmosProceso;
        /// <summary>
        /// CUIT de la empresa.
        /// </summary>
        string _cuit;
        /// <summary>
        /// Directorio donde dejar la info para el shopping Caballito.
        /// </summary>
        string _caballitoDirectorio;
        /// <summary>
        /// Servidor
        /// </summary>
        string _server;
        string _database;
        public frmRegenerarInfo(string _shopping, IcgFceDll.InfoIrsa irsa,
            string _olmosPath, string _olmosCliente, string _olmosPos, string _olmosRubro, string _olmosExe, string _CUIT,
            string _caballitoPath, string _servidor, string _base)
        {
            InitializeComponent();
            //Pasamos los parametros a las variables.
            _shoppingName = _shopping;
            _infoIrsa = irsa;
            _olmosDirectorio = _olmosPath;
            _olmosNroCliente = _olmosCliente;
            _olmosNroPOS = _olmosPos;
            _olmosNroRubro = _olmosRubro;
            _olmosProceso = _olmosExe;
            _cuit = _CUIT;
            _caballitoDirectorio = _caballitoPath;
            _server = _servidor;
            _database = _base;
            this.Text = this.Text + " - " + _shoppingName;
        }

        private void frmRegenerarInfo_Load(object sender, EventArgs e)
        {

        }

        private void btnRegenerar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            //Recupero los datos de las fechas.
            DateTime _desde = dateTimePicker1.Value;
            DateTime _hasta = dateTimePicker2.Value;

            string _strCon = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=sa" +";Password=masterkey;";
            try
            {
                using (SqlConnection _conexion = new SqlConnection(_strCon))
                {
                    _conexion.Open();
                    //Recupero la lista de ticketsl
                    List<IcgFceDll.Irsa.Retail.Tickets> _lst = IcgFceDll.Irsa.Retail.GetTikets(_desde, _hasta, _conexion);
                    //Vemos si tenemos tickets
                    if (_lst.Count > 0)
                    {
                        foreach (IcgFceDll.Irsa.Retail.Tickets _tck in _lst)
                        {
                            if (_tck.NumeroFiscal > 0)
                                LanzarInfoShoppings(_tck, _conexion);
                        }
                        this.Cursor = Cursors.Default;
                        MessageBox.Show("El proceso termino correctamente.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show("El periodo ingresado no devuelve ningún comprobante.",
                     "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message + 
                    Environment.NewLine + "Comuníquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Metodo que lanza los procesos para informar a los shoppings
        /// </summary>
        /// <param name="_serie">Serie de la venta.</param>
        /// <param name="_numero">Numero de la venta.</param>
        /// <param name="_n">N de la venta.</param>        
        /// <param name="_connection">Conexión SQL.</param>
        private void LanzarInfoShoppings(IcgFceDll.Irsa.Retail.Tickets _ticket, SqlConnection _connection)
        {
            switch (_shoppingName)
            {
                case "IRSA":
                    {
                        try
                        {
                            Irsa.Retail.LanzarTrancomp(_ticket.NumSerie, _ticket.NumFactura, _ticket.N, _infoIrsa, _ticket.SerieFiscal2.Substring(0, 3), _ticket.SerieFiscal1 + "-" + _ticket.NumeroFiscal.ToString(), _connection);
                        }
                        catch (Exception ex)
                        {
                            if (!Directory.Exists(@"C:\ICG\IRSALOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\IRSALOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\IRSALOG\icgIrsa.log", true))
                            {
                                sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                sw.Flush();
                            }
                        }
                        break;
                    }
                case "CABALLITO":
                    {
                        try
                        {
                            ShoppingCaballitoService.Retail.GenerarShoppingCaballito(_ticket.NumSerie, _ticket.NumFactura.ToString(), _ticket.N, _connection, _caballitoDirectorio,
                                    _ticket.NumeroFiscal.ToString().PadLeft(8, '0'), Convert.ToInt32(_ticket.SerieFiscal1).ToString().PadLeft(4, '0'));
                        }
                        catch (Exception ex)
                        {
                            if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\CABALLITOLOG\icgCaballito.log", true))
                            {
                                sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                sw.Flush();
                            }
                        }
                        break;
                    }
                case "OLMOS":
                    {
                        try
                        {
                            ShoppingOlmosService.Retail.LanzarShoppingOlmos(_ticket.NumSerie, _ticket.NumFactura, _ticket.N, _olmosDirectorio, _ticket.NumeroFiscal.ToString(),
                                    Convert.ToInt32(_olmosNroCliente), Convert.ToInt32(_olmosNroPOS), Convert.ToInt32(_olmosNroRubro), Convert.ToInt64(_cuit),
                                    _olmosProceso, true, _connection);
                        }
                        catch (Exception ex)
                        {
                            if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                            {
                                Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                            }
                            using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                            {
                                sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                                sw.Flush();
                            }
                        }
                        break;
                    }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
