﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AfipDll;
using IcgFceDll;
using static IcgFceDll.CommonService;
using static IcgFceDll.RetailService.FceComun;

namespace IcgRetailFCEConsola
{
    public partial class frmMain : Form
    {
        public SqlConnection _connection = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;

        public string _ip = "";
        
        //public string _password = "";
        public string _serverConfig = "";
        public string _userConfig = "";
        public string _catalogConfig = "";
        public bool _monotributo = false;
        public bool _hasarLog = false;
        public string _terminal = "";
        public string _tktregalo1 = "";
        public string _tktregalo2 = "";
        public string _tktregalo3 = "";
        public string _pathIrsa = "";
        
        public string strConnection;
        //public bool _KeyIsOk = false;
        public bool _KeyIsOk = true;
        

        public string _pathApp;
        public static string _pathCertificado;
        public static string _pathLog;
        public static string _pathTaFC;
        public static string _pathTaFCE;
        //settings
        public static string _caja = "";
        public static int _codigoIVA;
        public static int _codigoIIBB;
        public static string _nombreCertificado;
        public static string _cuit;
        public static bool _esTest;
        public static string _password;
        public static string _PtoVtaCAE;
        public static string _PtoVtaCAEA;
        public static string _PtoVtaManual;
        public static string _server;
        public static string _database;
        public static string _user;
        public static bool _soloCaea;
        public static string _licenciaIcg;
        public static bool _generaXml = true;
        public static bool _mtxca = true;
        public static bool _EsTest = false;
        public static bool _DesdoblaPagos = false;
        public static bool _Monotributista = false;
        public static bool _soloManual = false;
        public static InfoIrsa _irsa = new InfoIrsa();
        /// <summary>
        /// Path de slida de la info para olmos
        /// </summary>
        public static string _olmosPathSalida;
        /// <summary>
        /// Numero de cliente asignado por OLMOS
        /// </summary>
        public static string _olmosNroCliente;
        /// <summary>
        /// Numero de POS asiganado por OLMOS
        /// </summary>
        public static string _olmosNroPos;
        /// <summary>
        /// Rubro asigando por OLMOS
        /// </summary>
        public static string _olmosRubro;
        /// <summary>
        /// Proceso a ejecutar para el envio de la información a OLMOS.
        /// </summary>
        public static string _olmosProceso;
        /// <summary>
        /// Path donde dejar al archivo de Caballito.
        /// </summary>
        public static string _caballitoPathSalida;
        /// <summary>
        /// Cantidad de tickets para informar.
        /// </summary>
        public static int _cantidadTiquets;
        /// <summary>
        /// Monto maximo para los clientes Consumidor Final.
        /// </summary>
        public static decimal _montoMaximo;
        /// <summary>
        /// Identificador de la tienda para SiTef.
        /// </summary>
        public static string _IdTiendaSiTef;
        /// <summary>
        /// Ubicación donde se genera el archivo para SiTef.
        /// </summary>
        public static string _PathSiTef;
        /// <summary>
        /// Identificador de la Termnal SiTef.
        /// </summary>
        public static string _IdTerminalSiTef;

        public frmMain()
        {
            InitializeComponent();
            //recupero la ubicacion del ejecutable.
            _pathApp = Application.StartupPath.ToString();
            //Cargo la ubicaciones.
            _pathCertificado = _pathApp + "\\Certificado";
            _pathLog = _pathApp + "\\Log";
            _pathTaFC = _pathApp + "\\TAFC";
            _pathTaFCE = _pathApp + "\\TAFCE";
            //Titulo.
            this.Text = "ICG Argentina - Consola de Facturación Electrónica RETAIL - V." + Application.ProductVersion;
            //setting
            //Lectura
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
                //Obtengo las key's
                String[] _keys = config.AppSettings.Settings.AllKeys;
                _codigoIVA = Convert.ToInt32(config.AppSettings.Settings["CodigoIVA"].Value);
                _codigoIIBB = Convert.ToInt32(config.AppSettings.Settings["CodigoIIBB"].Value);
                _nombreCertificado = config.AppSettings.Settings["NombreCertificado"].Value;
                _cuit = config.AppSettings.Settings["CUIT"].Value;
                _esTest = Convert.ToBoolean(config.AppSettings.Settings["IsTest"].Value);
                _password = config.AppSettings.Settings["PassWord"].Value;
                _PtoVtaManual = config.AppSettings.Settings["PtoVtaManual"].Value;
                _PtoVtaCAE = config.AppSettings.Settings["PtoVtaCAE"].Value;
                _PtoVtaCAEA = config.AppSettings.Settings["PtoVtaCAEA"].Value;
                _server = config.AppSettings.Settings["Server"].Value;
                _user = "ICGAdmin";
                _database = config.AppSettings.Settings["DataBase"].Value;
                _cantidadTiquets = _keys.Contains("CantidadTiquets") ? Convert.ToInt32(config.AppSettings.Settings["CantidadTiquets"].Value) : 5;
                if (_keys.Contains("SoloCAEA"))
                    _soloCaea = Convert.ToBoolean(config.AppSettings.Settings["SoloCAEA"].Value);
                else
                    _soloCaea = false;
                _licenciaIcg = config.AppSettings.Settings["LicenciaIcg"].Value;
                if (_keys.Contains("IsMtxca"))
                    _mtxca = Convert.ToBoolean(config.AppSettings.Settings["IsMtxca"].Value);
                else
                    _mtxca = false;
                if (_keys.Contains("GenaraXML"))
                    _generaXml = Convert.ToBoolean(config.AppSettings.Settings["GenaraXML"].Value);
                else
                    _generaXml = false;
                if (_keys.Contains("IsTest"))
                    _EsTest = Convert.ToBoolean(config.AppSettings.Settings["IsTest"].Value);
                else
                    _esTest = false;
                if (_keys.Contains("DesdoblaPagos"))
                    _DesdoblaPagos = Convert.ToBoolean(config.AppSettings.Settings["DesdoblaPagos"].Value);
                else
                    _DesdoblaPagos = false;
                if (_keys.Contains("Monotributista"))
                    _Monotributista = Convert.ToBoolean(config.AppSettings.Settings["Monotributista"].Value);
                else
                    _Monotributista = false;
                //Facturación Manual
                if (_keys.Contains("SoloManual"))
                    _soloManual = Convert.ToBoolean(config.AppSettings.Settings["SoloManual"].Value);
                else
                    _soloManual = false;
                //IRSA
                if (_keys.Contains("IrsaContrato"))
                    _irsa.contrato = config.AppSettings.Settings["IrsaContrato"].Value;
                else
                    _irsa.contrato = "";
                if (_keys.Contains("IrsaLocal"))
                    _irsa.local = config.AppSettings.Settings["IrsaLocal"].Value;
                else
                    _irsa.local = "";
                if (_keys.Contains("IrsaPathSalida"))
                    _irsa.pathSalida = config.AppSettings.Settings["IrsaPathSalida"].Value;
                else
                    _irsa.pathSalida = "";
                if (_keys.Contains("IrsaPos"))
                    _irsa.pos = config.AppSettings.Settings["IrsaPos"].Value;
                else
                    _irsa.pos = "";
                if (_keys.Contains("IrsaRubro"))
                    _irsa.rubro = config.AppSettings.Settings["IrsaRubro"].Value;
                else
                    _irsa.rubro = "";
                //Olmos
                if (_keys.Contains("OlmosPathSalida"))
                    _olmosPathSalida = config.AppSettings.Settings["OlmosPathSalida"].Value;
                else
                    _olmosPathSalida = "";
                //Caballito
                if (_keys.Contains("CaballitoPathSalida"))
                    _caballitoPathSalida = config.AppSettings.Settings["CaballitoPathSalida"].Value;
                else
                    _caballitoPathSalida = "";
                //Caja
                if (_keys.Contains("Caja"))
                    _caja = config.AppSettings.Settings["Caja"].Value;
                else
                    _caja = "";
                //Monto Maximo
                if (_keys.Contains("MontoMaximo"))
                    _montoMaximo = Convert.ToDecimal(config.AppSettings.Settings["MontoMaximo"].Value);
                else
                    _montoMaximo = 43010;
                //IdTienda SiTef
                if (_keys.Contains("IdTiendaSitef"))
                    _IdTiendaSiTef = config.AppSettings.Settings["IdTiendaSitef"].Value.PadLeft(8, '0');
                else
                    _IdTiendaSiTef = "";
                //Path SiTef
                if (_keys.Contains("PathSitef"))
                    _PathSiTef = config.AppSettings.Settings["PathSitef"].Value;
                else
                    _PathSiTef = "";
                //IdTerminal SiTef
                if (_keys.Contains("IdTerminalSitef"))
                    _IdTerminalSiTef = config.AppSettings.Settings["IdTerminalSitef"].Value;
                else
                    _IdTerminalSiTef = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(_server))
                {
                    string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                    _connection.ConnectionString = strConnection;
                    //
                    _connection.Open();
                    //Cargamos los datos de los comprobantes.
                    GetDataset();
                    //Cargamos los CAEA otrogados.
                    GetCAEAOtorgados();
                    //Cargmos los Comprobantes sin informar.
                    GetComprobantesSinInformar();
                    //boton FC Manuales
                    if (!_soloManual)
                    {
                        btnFcManuales.BackColor = Color.Transparent;
                        btnFcManuales.ForeColor = Color.Black;
                        btnFcManuales.Text = "Activar FC Manuales";
                    }
                    else
                    {
                        btnFcManuales.BackColor = Color.Red;
                        btnFcManuales.ForeColor = Color.White;
                        btnFcManuales.Text = "Desactivar FC Manuales";
                    }
                }
                else
                {
                    btCaeaInformar.Enabled = false;
                    btImprimir.Enabled = false;
                    btRefrescarSinInfomar.Enabled = false;
                    btReimprimir.Enabled = false;
                    btSolicitarCAEA.Enabled = false;
                }
                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCertificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCertificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCertificado + "). Por favor coloque el certificado en este directorio para poder continuar.");

                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(_nombreCertificado))
                    {
                        if (!File.Exists(_pathCertificado + "\\" + _nombreCertificado))
                        {
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                            return;
                        }
                        else
                            _pathCertificado = _pathCertificado + "\\" + _nombreCertificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);
                //Validamos la Licencia
                if (DateTime.Now.Day < 15)
                {
                    if (!ValidarKey())
                    {
                        MessageBox.Show("Su licencia no es valida. " + Environment.NewLine +
                                                    "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetComprobantesSinInformar()
        {
            try
            {
                string _sql;
                if (String.IsNullOrEmpty(_caja))
                {
                    _sql = @"SELECT FACTURASVENTA.NUMSERIE as NUMSERIE, FACTURASVENTA.NUMFACTURA  as NUMALBARAN, FACTURASVENTA.N as N, FACTURASVENTA.CODMONEDA as FO, 
                                FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE,
                                FACTURASVENTACAMPOSLIBRES.VTO_CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO,
                                FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1 as SERIEFISCAL, FACTURASVENTASERIESRESOL.SERIEFISCAL2, 
                                FACTURASVENTASERIESRESOL.NUMEROFISCAL
                                FROM FACTURASVENTA inner join FACTURASVENTASERIESRESOL on FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                                and FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                                INNER JOIN CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE 
                                INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC 
                                inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE 
                                and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                                INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE
                                Where (FACTURASVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR') and
                                (FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO is null OR FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO = '') 
                                and isnull(FACTURASVENTASERIESRESOL.NUMEROFISCAL,0) > 0
                                and SERIESCAMPOSLIBRES.ELECTRONICA = 'T'
                                ORDER BY FACTURASVENTASERIESRESOL.NUMEROFISCAL";
                }
                else
                {
                    _sql = @"SELECT FACTURASVENTA.NUMSERIE as NUMSERIE, FACTURASVENTA.NUMFACTURA  as NUMALBARAN, FACTURASVENTA.N as N, FACTURASVENTA.CODMONEDA as FO, 
                                FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE,
                                FACTURASVENTACAMPOSLIBRES.VTO_CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO,
                                FACTURASVENTA.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1 as SERIEFISCAL, FACTURASVENTASERIESRESOL.SERIEFISCAL2, 
                                FACTURASVENTASERIESRESOL.NUMEROFISCAL
                                FROM FACTURASVENTA inner join FACTURASVENTASERIESRESOL on FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE
                                and FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                                INNER JOIN CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE 
                                INNER JOIN TIPOSDOC ON FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC 
                                inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE 
                                and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                                INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTA.NUMSERIE = SERIESCAMPOSLIBRES.SERIE
                                Where (FACTURASVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR') and
                                (FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO is null OR FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO = '') 
                                and isnull(FACTURASVENTASERIESRESOL.NUMEROFISCAL,0) > 0
                                and SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND (FACTURASVENTA.CAJA = @Caja)
                                ORDER BY FACTURASVENTASERIESRESOL.NUMEROFISCAL";
                }

                dtsMain.Tables["SinInformar"].Clear();

                using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
                {
                    if (!String.IsNullOrEmpty(_caja))
                        _cmd.Parameters.AddWithValue("@Caja", _caja);

                    using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(dtsMain, "SinInformar");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetCAEAOtorgados()
        {
            try
            {
                string _sql = "SELECT ID, CAEA, FechaProceso, FechaDesde, FechaHasta, FechaTope, Periodo, Quincena FROM FCCAEA";

                //Limpio el dataset
                dtsMain.Tables["Caea"].Clear();

                using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
                {

                    using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(dtsMain, "Caea");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Revise que exista la tabla FCCAEA. " + ex.Message);
            }
        }

        private void GetDataset()
        {
            string _sql;

            if (String.IsNullOrEmpty(_caja))
            {
                _sql = @"SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA, ALBVENTACAB.CODVENDEDOR,
                    ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, 
                    FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL, 
                    TIPOSDOC.DESCRIPCION, FACTURASVENTA.FECHA, ALBVENTACAB.Z,
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE
                    from FACTURASVENTA 
                    inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    inner join ALBVENTACAB on FACTURASVENTACAMPOSLIBRES.NUMSERIE = ALBVENTACAB.NUMSERIEFAC AND 
                    FACTURASVENTACAMPOSLIBRES.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTACAMPOSLIBRES.N = ALBVENTACAB.NFAC 
                    left join FACTURASVENTASERIESRESOL on ALBVENTACAB.NUMSERIEFAC = FACTURASVENTASERIESRESOL.NUMSERIE 
                    and ALBVENTACAB.NUMFAC = FACTURASVENTASERIESRESOL.NUMFACTURA
                    and ALBVENTACAB.NFAC = FACTURASVENTASERIESRESOL.N
                    INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC 
                    INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE 
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTACAMPOSLIBRES.NUMSERIE = SERIESCAMPOSLIBRES.SERIE 
                    WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND (FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') 
                    AND (FACTURASVENTACAMPOSLIBRES.CAE is null) AND CLIENTES.REGIMFACT <> 'N' AND ALBVENTACAB.N = 'B' 
                    ORDER BY FACTURASVENTA.FECHA";
            }
            else
            {
                _sql = @"SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA, ALBVENTACAB.CODVENDEDOR,
                    ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, 
                    FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL, 
                    TIPOSDOC.DESCRIPCION, FACTURASVENTA.FECHA, ALBVENTACAB.Z,
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE
                    from FACTURASVENTA 
                    inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE and FACTURASVENTA.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTACAMPOSLIBRES.N
                    inner join ALBVENTACAB on FACTURASVENTACAMPOSLIBRES.NUMSERIE = ALBVENTACAB.NUMSERIEFAC AND 
                    FACTURASVENTACAMPOSLIBRES.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTACAMPOSLIBRES.N = ALBVENTACAB.NFAC 
                    left join FACTURASVENTASERIESRESOL on ALBVENTACAB.NUMSERIEFAC = FACTURASVENTASERIESRESOL.NUMSERIE 
                    and ALBVENTACAB.NUMFAC = FACTURASVENTASERIESRESOL.NUMFACTURA
                    and ALBVENTACAB.NFAC = FACTURASVENTASERIESRESOL.N
                    INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC 
                    INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE 
                    INNER JOIN SERIESCAMPOSLIBRES ON FACTURASVENTACAMPOSLIBRES.NUMSERIE = SERIESCAMPOSLIBRES.SERIE 
                    WHERE SERIESCAMPOSLIBRES.ELECTRONICA = 'T' AND (FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') 
                    AND (FACTURASVENTA.CAJA = @Caja)
                    AND (FACTURASVENTACAMPOSLIBRES.CAE is null) AND CLIENTES.REGIMFACT <> 'N' AND ALBVENTACAB.N = 'B' 
                    ORDER BY FACTURASVENTA.FECHA";
            }

            //Limpio el dataset
            dtsMain.Tables["Comprobantes"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
            {
                if (!String.IsNullOrEmpty(_caja))
                    _cmd.Parameters.AddWithValue("@Caja", _caja);
                
                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsMain, "Comprobantes");
                }
            }
        }

        private void btSolicitarCAEA_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(_PtoVtaCAEA))
            {
                try
                {
                    //Valido si existe el CAEA para el periodo en curso.
                    //1- recupero el CAEA.
                    int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                    int _quincena;
                    DateTime _fechaDesde;
                    DateTime _fechaHasta;

                    if (DateTime.Now.Day >= 15)
                    {
                        _quincena = 2;
                        _fechaDesde = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
                        DateTime _fechaUno = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        _fechaHasta = _fechaUno.AddMonths(1).AddDays(-1);
                    }
                    else
                    {
                        _quincena = 1;
                        _fechaDesde = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        _fechaHasta = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 15);
                    }

                    //2- recupero el CAEA de la base de datos
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);

                    if (_caea.Caea == null)
                    {
                        //No lo tengo lo solicito
                        if (_mtxca)
                            _caea = AfipDll.CAEA.ObtenerCAEAMtxca(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);
                        else
                            _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);

                        if (_caea.Caea == null)
                        {
                            //No lo tengo lo consulto.
                            if (_mtxca)
                                _caea = CAEA.ConsultarCAEAMtxca(_fechaDesde, _fechaHasta, _periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);
                            else
                                _caea = CAEA.ConsultarCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);
                        }

                        if (_caea.Caea != null)
                            CAEA.InsertCAEA(_caea, _connection);
                        else
                            throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
                    }

                    GetCAEAOtorgados();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No se ha definido un Punto de Venta para CAEA. Por favor comuniquese con ICG Argentina si desea configurarlo.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btReimprimir_Click(object sender, EventArgs e)
        {
            GetDataset();
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (_KeyIsOk)
                Fiscalizar();
            else
                MessageBox.Show("Su licencia no es valida. " + Environment.NewLine +
                                                "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                MessageBoxIcon.Information);
            Cursor.Current = Cursors.Default;
        }

        private void Fiscalizar()
        {
            string _dummy = "FALSE";

            if (grComprobantes.Rows.Count > 0)
            {
                if (grComprobantes.SelectedRows.Count > 0)
                {
                    //Tengo una fila seleccionada.
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows)
                    {
                        string _serie = rw.Cells["NUMSERIE"].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells["NUMFACTURA"].Value);
                        string _n = rw.Cells["N"].Value.ToString();
                        int _codigoVendedor = Convert.ToInt32(rw.Cells["CODVENDEDOR"].Value);
                        try
                        {
                            //Busco el comprobante en FacturasVentaSerieResol.
                            DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                            //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                            if (_fsr.NumeroFiscal == 0)
                            {
                                if (!_soloManual)
                                {
                                    //Factura Electronica comun.
                                    DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFacturaRetail(_serie, Convert.ToInt32(_numero), _n, _PtoVtaCAE, _connection);
                                    if (String.IsNullOrEmpty(_Fc.CAE))
                                    {
                                        //Password
                                        SecureString strPasswordSecureString = new SecureString();
                                        foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                        strPasswordSecureString.MakeReadOnly();
                                        //Vemos si vamos por MtxCa o no
                                        if (_mtxca)
                                        {
                                            long _ultimoICG = 0;
                                            List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                                            //Recupero el nro del último comprobante.
                                            Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTaFC, _pathLog,
                                                Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), _cuit, _EsTest,
                                                strPasswordSecureString, false, out _lstError);
                                            bool _estoyOkAfip = RetailService.Common.ValidarUltimoNroComprobante(_Fc.PtoVta, _ultimoComprobante, _Fc.cCodAfip, _connection, out _ultimoICG);
                                            if (!_estoyOkAfip)
                                            {
                                                string _error = "VAL-ICG: El ultimo Numero de AFIP no se corresponde con el ultimo en ICG." + Environment.NewLine +
                                                    "Ultimo informado por AFIP: " + _ultimoComprobante.ToString() +
                                                    Environment.NewLine + "Ultimo informado por ICG: " + _ultimoICG.ToString() +
                                                    Environment.NewLine + "Por favor Verifique que el comprobante no este anulado";
                                                MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Último AFIP ->" + _ultimoComprobante.ToString());
                                                LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Último ICG ->" + _ultimoICG.ToString());
                                                //Consulto el comprobante.
                                                WsMTXCA.ConsultaComprobanteResponse _consulta = new WsMTXCA.ConsultaComprobanteResponse();
                                                List<WsMTXCA.Errors> _lstErrorCons = new List<WsMTXCA.Errors>();
                                                WsMTXCA.ConsultarComprobanteAutorizado(_pathCertificado, _pathTaFC, _pathLog,
                                                    _cuit, _Fc.cCodAfip, _Fc.PtoVta, _ultimoComprobante, _EsTest, strPasswordSecureString,
                                                    _generaXml, out _consulta, out _lstErrorCons);
                                                if (_lstErrorCons.Count == 0)
                                                {
                                                    if (!String.IsNullOrEmpty(_consulta.observaciones))
                                                    {
                                                        if (_consulta.observaciones.Split('-').Length == 3)
                                                        {
                                                            string _serieCons = _consulta.observaciones.Split('-')[0];
                                                            string _numeroCons = _consulta.observaciones.Split('-')[1];
                                                            string _nCons = _consulta.observaciones.Split('-')[2];
                                                            //Recupero los datos de la Venta.
                                                            DataAccess.DatosFactura _fcCons = DataAccess.DatosFactura.GetDatosFactura(_serieCons, Convert.ToInt32(_numeroCons), _nCons, _connection);
                                                            if (_fcCons.NroFiscal == 0)
                                                            {
                                                                //Genero codigo de barra
                                                                string _codBarra = "";
                                                                string _fechaVto = _consulta.fechaVencimiento.Year.ToString() + _consulta.fechaVencimiento.Month.ToString().PadLeft(2, '0') + _consulta.fechaVencimiento.Day.ToString().PadLeft(2, '0');
                                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _fcCons.cCodAfip, _fcCons.PtoVta.ToString(), _consulta.codigoAutorizacion.ToString(), _fechaVto, out _codBarra);
                                                                //Genero el codigo del QR.
                                                                string _qr = QR.CrearJson(1, _fcCons.fecha, Convert.ToInt64(_cuit), _consulta.puntoVenta, _consulta.tipoComprobante,
                                                                    Convert.ToInt32(_consulta.numeroCbte), _consulta.importeTotal, _consulta.codigoMoneda, _consulta.cotizacionMoneda,
                                                                    _consulta.tipoDocumento, _consulta.numeroDocumento, "E", _consulta.codigoAutorizacion);
                                                                //Insertamos en FacturasVentasSeriesResol
                                                                DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_fcCons, Convert.ToInt32(_consulta.numeroCbte), _pathLog, _connection);
                                                                //Insertamos en FacturasCamposLibres
                                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serieCons, _numeroCons,
                                                                    _nCons, _consulta.codigoAutorizacion.ToString(), "", _fechaVto, _codBarra, _qr, _pathLog, _consulta.puntoVenta.ToString().PadLeft(5, '0'), _connection);
                                                            }
                                                            else
                                                            {
                                                                string _errorCons = "VAL-ICG: El comprobante faltante de la AFIP, ya posee número fiscal." + Environment.NewLine +
                                                                "Por favor comuniquese con ICG Argentina.";
                                                                MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string _errorCons = "VAL-ICG: El comprobante recuperado de AFIP, no posee los datos necesarios para insertarlo automáticamente." + Environment.NewLine +
                                                                            "Por favor comuniquese con ICG Argentina.";
                                                            MessageBox.Show(new Form { TopMost = true }, _errorCons, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        string _errorCons = "VAL-ICG: El comprobante recuperado de AFIP, no posee los datos necesarios para insertarlo automáticamente." + Environment.NewLine +
                                                                        "Por favor comuniquese con ICG Argentina.";
                                                        MessageBox.Show(new Form { TopMost = true }, _errorCons, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                string _erroMsj = "";
                                                bool _rta = RetailService.Mtxca.Fiscalizar(_Fc, _codigoIVA, _codigoIIBB, _Fc.PtoVta, _PtoVtaCAEA, _password, _soloCaea, _EsTest, _generaXml, _pathLog, _pathCertificado, _pathTaFC, _cuit, "", _connection, out _erroMsj);
                                                if (!_rta)
                                                {
                                                    string _tipoMsj = _erroMsj.Split('|')[0];
                                                    if (_tipoMsj.ToUpper() == "ERROR")
                                                    {
                                                        string _msj = _erroMsj.Split('|')[1];
                                                        MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error." + Environment.NewLine +
                                                                            "Error: " + _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //FCE Comun
                                            //Valido si hay comprobantes sin fiscalizar
                                            if (!RetailService.Common.ExistenComprobantesConFechaAnterior(_Fc.fecha, _caja, _connection))
                                            {
                                                if (CommonService.Validaciones.ValidarMontoMaximoConsumidorFinal(_montoMaximo, _Fc))
                                                {
                                                    List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                                    _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                                    //List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIIBB, _connection, _Fc.TotalBruto);
                                                    List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIIBB, _connection, _Fc.TotalBruto);
                                                    _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                                    List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                                    _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                                    //Validamos los datos de la FC.
                                                    DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);
                                                    //Vemos si utilizamos solo CAEA
                                                    if (_soloCaea)
                                                    {
                                                        _dummy = "FALSE";
                                                    }
                                                    else
                                                    {
                                                        //Vemos si esta disponible el servicio de AFIP.
                                                        _dummy = wsAfip.TestDummy(_pathLog, _esTest);
                                                    }
                                                    if (_dummy.ToUpper() == "OK")
                                                    {
                                                        RetailService.FceComun.Resultado resultado = new RetailService.FceComun.Resultado();
                                                        InfoTransaccionAFIP existeAnterior = CommonService.TransaccionAFIP.GetTransaccionAFIP(_serie, Convert.ToInt32(_numero), _connection);
                                                        if (existeAnterior.cae != null)
                                                        {
                                                            DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc.NumSerie, _Fc.NumFactura, _Fc.N, existeAnterior.cae, "", existeAnterior.fechavto, existeAnterior.codigobara, existeAnterior.codigoqr, _pathLog, existeAnterior.tipocae == "CAE" ? _PtoVtaCAE : _PtoVtaCAEA, _connection);
                                                            DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_Fc, existeAnterior.nrofiscal, _connection);
                                                            string _terminal = Environment.MachineName;
                                                            //Inserto en Rem_transacciones.
                                                            DataAccess.RemTransacciones.Retail_InsertRemTransacciones(_terminal, _Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _Fc.N, 1, _connection);
                                                            //armamos respuesta.
                                                            List<string> _msg = new List<string>();
                                                            resultado = new Resultado { afipOK = true, cae = existeAnterior.cae, fechaVto = existeAnterior.fechavto, message = _msg };
                                                        }
                                                        else
                                                        {
                                                            RetailService.FceComun.FiscalizarAFIP(_pathCertificado, _pathTaFC, _pathLog, _cuit,
                                                                _Fc, _Iva, _Tributos, _EsTest, strPasswordSecureString, _connection, out resultado);
                                                        }
                                                        //Analizmos el resultado.
                                                        if (String.IsNullOrEmpty(resultado.cae))
                                                        {
                                                            //Afip con Error, muestro el mensaje.
                                                            if (resultado.afipOK)
                                                            {
                                                                if (resultado.message.Count > 0)
                                                                {
                                                                    string _msj = "";
                                                                    foreach (string st in resultado.message)
                                                                    {
                                                                        if (String.IsNullOrEmpty(_msj))
                                                                            _msj = "Se produjo el siguiente error." + Environment.NewLine + st;
                                                                        else
                                                                            _msj = _msj + Environment.NewLine + st;
                                                                    }
                                                                    MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                //Transaccion CAEA
                                                                if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                                                {
                                                                    //Grabo todo con una transaccion.
                                                                    string message = "";
                                                                    if (!RetailService.FceComun.TransactionCAEA(_serie, _numero.ToString(), _n, _codigoVendedor.ToString(), _PtoVtaCAEA, _cuit, _pathLog, _Fc, _connection, out message))
                                                                        MessageBox.Show(message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                    //Recupero el contador para el CAEA
                                                                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.cCodAfip, _connection);
                                                                    if (_dtoContador.contador % _cantidadTiquets == 0)
                                                                    {
                                                                        string _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //Vemos si tenemos CAEA.
                                                        if (String.IsNullOrEmpty(_PtoVtaCAEA))
                                                        {
                                                            MessageBox.Show("No esta disponible la comunicación con la AFIP. Intente mas tarde.", "ICG Argentina",
                                                                 MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                        }
                                                        else
                                                        {
                                                            string message = "";
                                                            if (!RetailService.FceComun.TransactionCAEA(_serie, _numero.ToString(), _n, _codigoVendedor.ToString(), _PtoVtaCAEA, _cuit, _pathLog, _Fc, _connection, out message))
                                                                MessageBox.Show(message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                            //Recupero el contador para el CAEA
                                                            DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.cCodAfip, _connection);
                                                            if (_dtoContador.contador % _cantidadTiquets == 0)
                                                            {
                                                                string _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "Para Facturas B con Montos mayores a " + _montoMaximo.ToString() + Environment.NewLine +
                                                        "Por resolución de AFIP, es obligatorio identificar al comprador." + Environment.NewLine +
                                                        "Por favor ingrese los datos del cliente.", "ICG Argentina", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Error);
                                                }
                                            }
                                            else
                                            {

                                                MessageBox.Show("Existen comprobantes con fecha anterior sin fiscalizar." + Environment.NewLine +
                                                        "Debe fiscalizar estos comprobantes antes, para un correcto procesamiento en AFIP.", "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante ya posee CAE: " + _Fc.CAE);
                                        if (!String.IsNullOrEmpty(_Fc.Comprobante))
                                        {
                                            int _numeroFiscal = Convert.ToInt32(_Fc.Comprobante.Split('-')[1]);
                                            //Grabo FacturasVentaSerieResol.
                                            DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_Fc, _numeroFiscal, _pathLog, _connection);
                                        }
                                        else
                                        {
                                            InfoTransaccionAFIP aFIP = TransaccionAFIP.GetTransaccionAFIP(_Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _connection);
                                            //Grabo FacturasVentaSerieResol.
                                            DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_Fc, aFIP.nrofiscal, _pathLog, _connection);
                                        }
                                    }
                                }
                                else
                                {
                                    //FACTURAS MANUALES
                                    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero.ToString(), _n, _codigoVendedor.ToString(), _connection);
                                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaManual, _tipoComprobante.Substring(0, 3), _connection);
                                    //Vemos si tenemos contador.
                                    if (_dtoContador.serieresol == null)
                                    {
                                        string _rta = "No se encontro un contador definico para el punto de Venta: " + _PtoVtaManual + Environment.NewLine +
                                            "Por favor comuniquese con ICG Argentina.";
                                        MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        return;
                                    }
                                    frmIngresoManual frm = new frmIngresoManual(_tipoComprobante, _PtoVtaManual, _dtoContador.contador + 1);
                                    frm.ShowDialog();
                                    string _ptoVta = frm._ptoVta;
                                    int _nroCbte = frm._nroCbte;
                                    frm.Dispose();
                                    if (_nroCbte > 0)
                                    {
                                        bool _Ok = DataAccess.Transactions.Retail.TransactionManual(_serie, _numero.ToString(), _n, _codigoVendedor.ToString(), _ptoVta, _nroCbte,
                                            _cuit, _pathLog, _connection);
                                        if (!_Ok)
                                        {
                                            string _numeroError = _ptoVta.ToString().PadLeft(4, '0') + "-" + _nroCbte.ToString().PadLeft(8, '0');
                                            string _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                                            MessageBox.Show(new Form() { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                            {
                                if (ex.Message.StartsWith("ICG-VAL"))
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                        "Por favor revise los datos y luego fiscalice el comprobante desde la consola.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                                        "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                    {
                                        //Grabo todo con una transaccion.
                                        if (!DataAccess.Transactions.TransactionCAEA2(_serie, _numero.ToString(), _n, _codigoVendedor.ToString(), _PtoVtaCAEA, _cuit, _pathLog, _connection))
                                            MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la informacion en el sistema. Consulte el log.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente ERROR: " + ex.Message + Environment.NewLine +
                                                "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                MessageBoxIcon.Error);
                            }
                        }
                        LanzarInfoShoppings(_serie, _numero, _n, _connection);
                    }
                }
                GetDataset();
            }
        }


        private void btCaeaInformar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                {
                    //Valido si existe el CAEA para el periodo en curso.
                    //1- recupero el CAEA.
                    int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                    int _quincena = DateTime.Now.Day > 15 ? 2 : 1;

                    //1- recupero las FC con CAEA
                    MessageBox.Show("Quincena: " + _quincena.ToString());
                    MessageBox.Show("Periodo: " + _periodo.ToString());
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                    //VEmos si tenemos CAEA
                    if (_caea.Caea != null)
                    {
                        //Busco los comprobantes sin el mismo CAEA sin informar
                        List<DataAccess.ComprobantesConCAEA> _lst = DataAccess.IcgCAEA.GetComprobantesConCAEA(_caea.Caea, _connection);
                        //Recorro los datos de la lista.
                        foreach (DataAccess.ComprobantesConCAEA _comp in _lst)
                        {
                            if (_comp.Estado_FE.ToUpper() == "SININFORMAR")
                            {
                                //Ver como se genera el request en la FCE.
                                //Busco el Tiquet.
                                DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                if (_fsr.NumeroFiscal > 0)
                                {
                                    //Factura Electronica comun.
                                    DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                    // Array de IVA
                                    List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                    _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                    //Array de tributos
                                    List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIIBB, _connection, _Fc.TotalBruto);
                                    _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                    //Array de Importes no gravados.
                                    List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                    _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                    //Asigno el punto de venta de CAE.
                                    _Fc.PtoVta = _PtoVtaCAEA;
                                    //Validamos los datos de la FC.
                                    DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);
                                    //Armamos la password como segura
                                    SecureString strPasswordSecureString = new SecureString();
                                    foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                    strPasswordSecureString.MakeReadOnly();
                                    //Vemos si esta disponible el servicio de AFIP.
                                    string _dummy = wsAfip.TestDummy(_pathLog, _esTest);
                                    if (_dummy.ToUpper() == "OK")
                                    {
                                        List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                                        //Armo el string de la cabecera.
                                        AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                                        _cabReq.CantReg = 1;
                                        _cabReq.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                                        _cabReq.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                                        string strCabReg = "1|" + _Fc.cCodAfip.ToString() + "|" + _Fc.PtoVta.ToString();
                                        //Cargamos la clase Detalle.
                                        List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = RetailService.CargarDetalleCAEAAfip(_Fc, _fsr.NumeroFiscal, _caea);
                                        //Armo el string de IVA.
                                        List<AfipDll.wsAfipCae.AlicIva> _lstIva = RetailService.ArmarIVAAfip(_Iva);
                                        //Armos el string de Tributos.
                                        List<AfipDll.wsAfipCae.Tributo> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                                       //Agregamos el IVA
                                        _detReq[0].Iva = _lstIva.ToArray();
                                        //Agregamos los tributos.
                                        if (_TributosRequest.Count > 0)
                                            _detReq[0].Tributos = _TributosRequest.ToArray();
                                        //Armamos el Request.
                                        AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                                        _req.FeCabReq = _cabReq;
                                        _req.FeDetReq = _detReq.ToArray();
                                        try
                                        {
                                            List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest, strPasswordSecureString, _req, false, out _lstErrores);

                                            if (_Respuesta.Count() > 0)
                                            {
                                                //Recupero el nombre de la terminal.
                                                string _terminal = Environment.MachineName;
                                                foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                                                {
                                                    switch (cri.Resultado)
                                                    {
                                                        case "A":
                                                            {
                                                                //Grabo el dato en facturascamposlibres.

                                                                bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                if (!_rta)
                                                                {
                                                                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                }
                                                                //Inserto en Rem_transacciones.
                                                                DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                break;
                                                            }
                                                        case "P":
                                                            {
                                                                //Informo.
                                                                bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                if (!_rta)
                                                                {
                                                                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                }
                                                                else
                                                                {
                                                                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                            ") se informo con la observacion: " + cri.Observaciones);
                                                                }
                                                                //Inserto en Rem_transacciones.
                                                                DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                break;
                                                            }
                                                        case "R":
                                                            {
                                                                //Informo.
                                                                MessageBox.Show("El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N + Environment.NewLine +
                                                                    "Fue rechazado por AFIP. Error" + cri.Observaciones,
                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                    ") fue rechazado por la AFIP el ser infomado.");
                                                                break;
                                                            }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                    ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                                                {
                                                    MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                                        "Descripcion: " + _er.Msg);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                    ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                                        }
                                        //break;
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("No existe un CAEA para el periodo en curso.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("No se ha definido ningún Punto de Venta para CAEA. " + Environment.NewLine +
                                                "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                MessageBoxIcon.Information);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btCaeaInformar_Click2(object sender, EventArgs e)
        {
            try
            {
                if (_KeyIsOk)
                {
                    if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                    {
                        if (grSinInformar.Rows.Count > 0)
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            //Tengo una fila seleccionada.
                            foreach (DataGridViewRow rw in grSinInformar.Rows)
                            {
                                DateTime _fechaFc = Convert.ToDateTime(rw.Cells[4].Value.ToString());
                                string _estadoComp = rw.Cells[10].Value.ToString();
                                string _serieComp = rw.Cells[1].Value.ToString();
                                int _numeroComp = Convert.ToInt32(rw.Cells[2].Value.ToString());
                                string _nComp = rw.Cells[3].Value.ToString();
                                //Valido si existe el CAEA para el periodo en curso.
                                //1- recupero el CAEA.
                                int _periodo = Convert.ToInt32(_fechaFc.Year.ToString() + _fechaFc.Month.ToString().PadLeft(2, '0'));
                                int _quincena = 0;

                                if (_fechaFc.Day >= 16)
                                    _quincena = 2;
                                else
                                    _quincena = 1;

                                //1- recupero las FC con CAEA
                                AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                                //VEmos si tenemos CAEA
                                if (_caea.Caea != null)
                                {
                                    if (_estadoComp.ToUpper() == "SININFORMAR")
                                    {
                                        //Vemos si es MTXCA
                                        if (_mtxca)
                                        {
                                            //Busco el Tiquet.
                                            DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serieComp, _numeroComp, _nComp, _connection);
                                            //Vemos si tenemos nro Fiscal.
                                            if (_fsr.NumeroFiscal > 0)
                                            {
                                                //Factura Electronica MtxCa.
                                                DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(_serieComp, _numeroComp, _nComp, _connection);
                                                RetailService.Mtxca.InformarComprobanteCAEA(_Fc, _codigoIVA, _PtoVtaCAEA, _password, _soloCaea, _EsTest, true,
                                                    _pathLog, _pathCertificado, _pathTaFC,
                                                    _cuit, _connection);
                                            }
                                        }
                                        else
                                        {
                                            //Ver como se genera el request en la FCE.
                                            //Busco el Tiquet.
                                            DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serieComp, _numeroComp, _nComp, _connection);
                                            //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                            if (_fsr.NumeroFiscal > 0)
                                            {
                                                RetailService.FceComun.InformarComprobantesCAEA(_serieComp, _numeroComp, _nComp,
                                                    _codigoIVA, _codigoIIBB, _PtoVtaCAEA, _password, _cuit,
                                                    _pathLog, _pathCertificado, _pathTaFC,
                                                    _EsTest, false, _connection);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("No existe un CAEA para el periodo en curso.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            Cursor.Current = Cursors.Default;
                            //Refresco la grilla
                            GetComprobantesSinInformar();
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se ha definido ningún Punto de Venta para CAEA. " + Environment.NewLine +
                                                    "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);
                    }
                }
                else
                    MessageBox.Show("Licencia no Valida. " + Environment.NewLine +
                                                    "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private static List<AfipDll.wsAfip.ComprobanteAsoc> ArmarCbteAsoc(DataAccess.DatosFactura fc, string strCUIT, SqlConnection _connection)
        {
            List<AfipDll.wsAfip.ComprobanteAsoc> _lst = new List<AfipDll.wsAfip.ComprobanteAsoc>();
            try
            {
                List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _connection);

                foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                {
                    AfipDll.wsAfip.ComprobanteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocNew(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _connection);
                    _lst.Add(_cls);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }

        private void btRefrescarSinInfomar_Click(object sender, EventArgs e)
        {
            GetComprobantesSinInformar();
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _Key = _pass._Text;
            _pass.Dispose();
            if (_Key == "hoguera")
            {
                frmConfig _frm = new frmConfig();
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
        }

        private void grComprobantes_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

        }

        private void grComprobantes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow Myrow in grComprobantes.Rows)
            {            //Here 2 cell is target value and 1 cell is Volume
                if (Myrow.Cells[9].Value.ToString().ToUpper() == "ERROR" )// Or your condition 
                {
                    Myrow.DefaultCellStyle.BackColor = Color.Red;
                }
                if (Myrow.Cells[9].Value.ToString().ToUpper() == "PENDIENTE")// Or your condition 
                {
                    Myrow.DefaultCellStyle.BackColor = Color.Yellow;
                }
            }
        }

        private void btnFcManuales_Click(object sender, EventArgs e)
        {
            //Mostramos pantalla de password
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _Key = _pass._Text;
            _pass.Dispose();
            if (_Key == "4rg3nt1n4")
            {
                if (!_soloManual)
                {
                    btnFcManuales.BackColor = Color.Red;
                    btnFcManuales.ForeColor = Color.White;
                    btnFcManuales.Text = "Desactivar FC Manuales";
                    _soloManual = true;
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
                    config.AppSettings.Settings["SoloManual"].Value = _soloManual.ToString();
                    config.Save();
                    //Mensaje.
                    MessageBox.Show("Ya puede ingresar facturas Manuales." + Environment.NewLine +
                        "Recuerde que cuando finalice, debe volver a desactivar las Facturas Manuales",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    btnFcManuales.BackColor = Color.Transparent;
                    btnFcManuales.ForeColor = Color.Black;
                    btnFcManuales.Text = "Activar FC Manuales";
                    _soloManual = false;
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
                    config.AppSettings.Settings["SoloManual"].Value = _soloManual.ToString();
                    config.Save();
                    //Mensaje.
                    MessageBox.Show("Ha desactivado las facturas Manuales.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Password INCORRECTA.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Metodo que lanza los procesos para informar a los shoppings
        /// </summary>
        /// <param name="_serie">Serie de la venta.</param>
        /// <param name="_numero">Numero de la venta.</param>
        /// <param name="_n">N de la venta.</param>        
        /// <param name="_connection">Conexión SQL.</param>
        private static void LanzarInfoShoppings(string _serie, int _numero, string _n, SqlConnection _connection)
        {
            //Vemos si tenemos que lanzar IRSA.
            if (!String.IsNullOrEmpty(_irsa.pathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        Irsa.Retail.LanzarTrancomp(_serie, _numero, _n, _irsa, _fsr.SerieFiscal2.Substring(0, 3), _fsr.SerieFiscal1 + "-" + _fsr.NumeroFiscal.ToString(), _connection);
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\IRSALOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\IRSALOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\IRSALOG\icgIrsa.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que lanzar Shoppping Caballito.
            if (!String.IsNullOrEmpty(_caballitoPathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        ShoppingCaballitoService.Retail.GenerarShoppingCaballito(_serie, _numero.ToString(), _n, _connection, _caballitoPathSalida, 
                            _fsr.NumeroFiscal.ToString().PadLeft(8,'0'), Convert.ToInt32(_fsr.SerieFiscal1).ToString().PadLeft(4,'0'));
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\CABALLITOLOG\icgCaballito.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que lanzar Shoppping Caballito.
            if (!String.IsNullOrEmpty(_olmosPathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        ShoppingOlmosService.Retail.LanzarShoppingOlmos(_serie, Convert.ToInt32(_numero), _n, _olmosPathSalida, _fsr.NumeroFiscal.ToString(),
                            Convert.ToInt32(_olmosNroCliente), Convert.ToInt32(_olmosNroPos), Convert.ToInt32(_olmosRubro), Convert.ToInt64(_cuit),
                            _olmosProceso, true, _connection);
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que mandar a IRSA SITEF.
            if(!String.IsNullOrEmpty(_PathSiTef))
            {
                //Validamos que tenemos numero fiscal.
                DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                if (_fsr.NumeroFiscal > 0)
                {
                    if (!_esTest)
                    {
                        IcgFceDll.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _IdTiendaSiTef, _IdTerminalSiTef.PadLeft(8, '0'), _cuit, "30711277249", _PathSiTef, false, _connection);
                    }
                    else
                    {
                        IcgFceDll.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _IdTiendaSiTef, _IdTerminalSiTef.PadLeft(8, '0'), "30711277249", "30711277249", _PathSiTef, false, _connection);
                    }
                }
            }
        }

        private void cambiarFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grComprobantes.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows)
                    {
                        string _serie = rw.Cells["NUMSERIE"].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells["NUMFACTURA"].Value);
                        string _n = rw.Cells["N"].Value.ToString();
                        frmChangeDate _frm = new frmChangeDate(_serie, _numero, _n, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetDataset();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }

        private void borrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grComprobantes.SelectedRows.Count == 1)
                {
                    //Recupero las filas seleccionadas.
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows)
                    {
                        if (MessageBox.Show(new Form { TopMost = true },
                    "Desea BORRAR el comprobante seleccionado?.",
                    "ICG Argentina", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            try
                            {
                                string _serie = rw.Cells["NUMSERIE"].Value.ToString();
                                int _numero = Convert.ToInt32(rw.Cells["NUMFACTURA"].Value);
                                string _n = rw.Cells["N"].Value.ToString();
                                DataAccess.Transactions.Retail.TransaccionBlack2(_n, _serie, _numero, _connection);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Se produjo el siguiente Error: " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    GetDataset();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void consultarÚltimoComprobanteCAEAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                SecureString strPasswordSecureString = new SecureString();
                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                int _ptoVta = Convert.ToInt32(_PtoVtaCAEA);

                if (_mtxca)
                {
                    List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();
                    try
                    {
                        long _ultimoNro = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            1, _cuit, _EsTest, strPasswordSecureString, false, out _lstErrores);
                        if (_lstErrores.Count() == 0)
                            MessageBox.Show("Factura A Ultimo Nro: " + _ultimoNro.ToString());
                        else
                        {
                            string _message = "";
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                            {
                                _message = _message + Environment.NewLine + _er.Msg;
                            }
                            MessageBox.Show(_message);
                        }
                        _lstErrores.Clear();
                        long _ultimoNroNcA = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            3, _cuit, _EsTest, strPasswordSecureString, false, out _lstErrores);
                        if (_lstErrores.Count() == 0)
                            MessageBox.Show("NC A Ultimo Nro: " + _ultimoNroNcA.ToString());
                        else
                        {
                            string _message = "";
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                            {
                                _message = _message + Environment.NewLine + _er.Msg;
                            }
                            MessageBox.Show(_message);
                        }
                        _lstErrores.Clear();
                        long _ultimoNroFcB = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            6, _cuit, _EsTest, strPasswordSecureString, false, out _lstErrores);
                        if (_lstErrores.Count() == 0)
                            MessageBox.Show("FC B Ultimo Nro: " + _ultimoNroFcB.ToString());
                        else
                        {
                            string _message = "";
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                            {
                                _message = _message + Environment.NewLine + _er.Msg;
                            }
                            MessageBox.Show(_message);
                        }
                        _lstErrores.Clear();
                        long _ultimoNroNcB = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            8, _cuit, _EsTest, strPasswordSecureString, false, out _lstErrores);
                        if (_lstErrores.Count() == 0)
                            MessageBox.Show("NC B Ultimo Nro: " + _ultimoNroNcB.ToString());
                        else
                        {
                            string _message = "";
                            foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                            {
                                _message = _message + Environment.NewLine + _er.Msg;
                            }
                            MessageBox.Show(_message);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    //FCA
                    try
                    {
                        List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                        AfipDll.wsAfip.UltimoComprobante _ultimoFcA = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            1, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);

                        if (_lstErrores.Count() == 0)
                        {
                            MessageBox.Show("Nro. Comprobante: " + _ultimoFcA.CbteNro + Environment.NewLine +
                                "Comprobante Tipo: " + _ultimoFcA.CbteTipo + Environment.NewLine +
                                "Punto de Venta: " + _ultimoFcA.PtoVta);
                        }
                        else
                        {
                            MessageBox.Show("FC A Nro. Comprobante: " + _ultimoFcA.CbteNro);

                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                        }

                        //NCA
                        _lstErrores.Clear();
                        AfipDll.wsAfip.UltimoComprobante _ultimoNcA = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            3, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);

                        if (_lstErrores.Count() == 0)
                        {
                            MessageBox.Show("Nro. Comprobante: " + _ultimoNcA.CbteNro + Environment.NewLine +
                                "Comprobante Tipo: " + _ultimoNcA.CbteTipo + Environment.NewLine +
                                "Punto de Venta: " + _ultimoNcA.PtoVta);
                        }
                        else
                        {
                            MessageBox.Show("NC A Nro. Comprobante: " + _ultimoNcA.CbteNro);

                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                        }
                        //FCB
                        _lstErrores.Clear();
                        AfipDll.wsAfip.UltimoComprobante _ultimoFcB = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                            6, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);

                        if (_lstErrores.Count() == 0)
                        {
                            MessageBox.Show("Nro. Comprobante: " + _ultimoFcB.CbteNro + Environment.NewLine +
                                "Comprobante Tipo: " + _ultimoFcB.CbteTipo + Environment.NewLine +
                                "Punto de Venta: " + _ultimoFcB.PtoVta);
                        }
                        else
                        {
                            MessageBox.Show("FC B Nro. Comprobante: " + _ultimoFcB.CbteNro);

                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                        }
                        // NCB
                        _lstErrores.Clear();
                        AfipDll.wsAfip.UltimoComprobante _ultimoNcB = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                        8, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);

                        if (_lstErrores.Count() == 0)
                        {
                            MessageBox.Show("Nro. Comprobante: " + _ultimoNcB.CbteNro + Environment.NewLine +
                                "Comprobante Tipo: " + _ultimoNcB.CbteTipo + Environment.NewLine +
                                "Punto de Venta: " + _ultimoNcB.PtoVta);
                        }
                        else
                        {
                            MessageBox.Show("NC B Nro. Comprobante: " + _ultimoNcB.CbteNro);

                            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                            {
                                MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                    "Descripcion: " + _er.Msg);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        #region Menu CAEA
        private void cambiarNumeroFiscalToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grSinInformar.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grSinInformar.SelectedRows)
                    {
                        string _serie = rw.Cells[1].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells[2].Value);
                        string _n = rw.Cells[3].Value.ToString();
                        int _numeroFiscal = Convert.ToInt32(rw.Cells[7].Value);
                        frmChangeNroFiscalCAEA _frm = new frmChangeNroFiscalCAEA(_serie, _numero, _n, _numeroFiscal, _pathLog, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetComprobantesSinInformar();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }        

        private void cambiarFechaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grComprobantes.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grSinInformar.SelectedRows)
                    {
                        string _serie = rw.Cells[1].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells[2].Value);
                        string _n = rw.Cells[3].Value.ToString();
                        frmChangeDate _frm = new frmChangeDate(_serie, _numero, _n, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetComprobantesSinInformar();
                }
                else
                {
                    if (grSinInformar.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }
        #endregion

        private bool ValidarKey()
        {
            bool _rta = false;

            //Guardamos los datos.
            Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));

            //Validamos licencia via WebService.
            string _Msg = "";
            string _Key = "";
            IcgFceDll.RetailService.Licencia.GetKey(_server,
                _database, _cuit, _PtoVtaCAE, out _Key, out _Msg);
            if (!String.IsNullOrEmpty(_Key))
            {
                if (_Key == _licenciaIcg)
                    _rta = true;
                else
                    _rta = false;
                //Licencia
                //config.AppSettings.Settings["LicenciaIcg"].Value = _Key;
            }
            else
            {
                if (_Msg.StartsWith("-1 Error:"))
                    _rta = true;
                else
                    //Licencia
                    config.AppSettings.Settings["LicenciaIcg"].Value = "";
            }
            config.Save();
            if (!String.IsNullOrEmpty(_Msg))
            {
                if (!_Msg.StartsWith("-1 Error:"))
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + Environment.NewLine +
                                                _Msg, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _KeyIsOk = _rta;
            return _rta;
        }

        private void btnCAEA_Click(object sender, EventArgs e)
        {
            //Mostramos pantalla de password
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _Key = _pass._Text;
            _pass.Dispose();
            if (_Key == "4rg3nt1n4")
            {
                if (!_soloManual)
                {
                    btnFcManuales.BackColor = Color.Red;
                    btnFcManuales.ForeColor = Color.White;
                    btnFcManuales.Text = "Desactivar CAEA";
                    _soloCaea = true;
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
                    config.AppSettings.Settings["SoloCAEA"].Value = _soloCaea.ToString();
                    config.Save();
                    //Mensaje.
                    MessageBox.Show("Ha ACTIVADO la modalidad de Facturación CAEA.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    btnFcManuales.BackColor = Color.Transparent;
                    btnFcManuales.ForeColor = Color.Black;
                    btnFcManuales.Text = "Activar CAEA";
                    _soloCaea = false;
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRetailFCEConsola.exe"));
                    config.AppSettings.Settings["SoloCAEA"].Value = _soloCaea.ToString();
                    config.Save();
                    //Mensaje.
                    MessageBox.Show("Ha desactivado la modalidad de Facturación CAEA.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Password INCORRECTA.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
