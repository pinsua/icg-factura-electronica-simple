﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailFCEConsola
{
    public partial class frmIngresoManual : Form
    {
        public string _ptoVta;
        public int _nroCbte = 0;
        public string _tipoComp;

        public frmIngresoManual(string tComp, string ptovta, int _numero)
        {
            InitializeComponent();
            _tipoComp = tComp;
            _ptoVta = ptovta;
            _nroCbte = _numero;
        }

        private void frmIngresoManual_Load(object sender, EventArgs e)
        {
            lblTipoComprobante.Text = _tipoComp;
            txtPtoVta.Text = _ptoVta.ToString();
            txtNro.Text = _nroCbte.ToString();
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            if (txtPtoVta.Text.Length > 0 && txtNro.Text.Length > 0)
            {
                _ptoVta = txtPtoVta.Text;
                _nroCbte = Convert.ToInt32(txtNro.Text);

                this.Close();
            }
            else
                MessageBox.Show("Debe ingresar un valor en el Punto de Venta y en el Numero de Comprobante.!",
                                      "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            _nroCbte = 0;
            this.Close();
        }

        private void txtPtoVta_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}
