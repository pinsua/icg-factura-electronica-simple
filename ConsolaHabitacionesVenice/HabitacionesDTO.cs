﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaHabitacionesVenice
{
    public class HabitacionesDTO
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }
        [JsonProperty("departureDate")]
        public string departureDate { get; set; }
        [JsonProperty("locator")]
        public string locator { get; set; }
        [JsonProperty("agent")]
        public AgentDto agent { get; set; }
        [JsonProperty("company")]
        public CompanyDto company { get; set; }
        [JsonProperty("profile")]
        public ProfileDto profile { get; set; }
        [JsonProperty("reservationType")]
        public ReservationTypeDto reservationType { get; set; }
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("room")]
        public RoomDto room { get; set; }
    }

    public class AgentDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("documentNumber")]
        public string documentNomber { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("surName")]
        public string surName { get; set; }
    }

    public class CompanyDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("documentNumber")]
        public string documentNomber { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("surName")]
        public string surName { get; set; }
    }

    public class ProfileDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("documentNumber")]
        public string documentNomber { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("surName")]
        public string surName { get; set; }
    }

    public class ReservationTypeDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
    }

    public class RoomDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
    }
}
