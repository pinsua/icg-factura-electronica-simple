﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace ConsolaHabitacionesVenice
{
    public partial class frmMain : Form
    {
        string _apiUrl;
        string _apiKey;
        public frmMain()
        {
            InitializeComponent();
            _apiKey = ConfigurationManager.AppSettings.Get("ApiKey");
            _apiUrl = ConfigurationManager.AppSettings.Get("ApiUrl");
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                //Recupero las habitaciones.
                ApiVenice api = new ApiVenice();
                List<HabitacionesDTO> _lst = api.GetHabitaciones(_apiKey, _apiUrl);
                List<GridDto> _grid = new List<GridDto>();
                foreach (HabitacionesDTO hab in _lst)
                {
                    GridDto _cls = new GridDto();
                    _cls.Apellido = hab.profile.surName;
                    _cls.Nombre = hab.profile.name;
                    _cls.Habitacion = hab.room.name;
                    _grid.Add(_cls);
                }
                var source = new BindingSource();
                source.DataSource = _grid;
                gridHabitacion.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //Recupero las habitaciones.
                ApiVenice api = new ApiVenice();
                List<HabitacionesDTO> _lst = api.GetHabitaciones(_apiKey, _apiUrl);
                List<GridDto> _grid = new List<GridDto>();
                foreach (HabitacionesDTO hab in _lst)
                {
                    GridDto _cls = new GridDto();
                    _cls.Apellido = hab.profile.surName;
                    _cls.Nombre = hab.profile.name;
                    _cls.Habitacion = hab.room.name;
                    _grid.Add(_cls);
                }
                var source = new BindingSource();
                source.DataSource = _grid;
                gridHabitacion.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
