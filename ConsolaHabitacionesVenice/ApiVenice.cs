﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaHabitacionesVenice
{
    class ApiVenice
    {
        public List<HabitacionesDTO> GetHabitaciones(string _key, string _url)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<HabitacionesDTO> _lst = new List<HabitacionesDTO>();
            string apiUrl = _url + "/api/reservation/inhouse/list";
            var client = new RestClient(apiUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-API-KEY", _key);
            request.AddHeader("X-RESORT-CODE", "1");
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                _lst = JsonConvert.DeserializeObject<List<HabitacionesDTO>>(response.Content);
            }

            return _lst;
        }
    }
}
