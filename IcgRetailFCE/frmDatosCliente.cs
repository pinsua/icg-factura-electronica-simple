﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailFCE
{
    public partial class frmDatosCliente : Form
    {
        public int _tipoDoc;
        public string _nroDoc;
        public frmDatosCliente()
        {
            InitializeComponent();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (cboTipoDoc.SelectedIndex > -1)
            {
                if (String.IsNullOrEmpty(txtNroDoc.Text))
                    MessageBox.Show(new Form { TopMost = true }, "Debe ingresar un Número de Documento.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    _tipoDoc = Convert.ToInt32(cboTipoDoc.SelectedItem.ToString().Substring(0, 3));
                    _nroDoc = txtNroDoc.Text;
                    this.Close();
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "Debe seleccionar un Tipo de Documento.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtNroDoc_Validating(object sender, CancelEventArgs e)
        {
            if (cboTipoDoc.SelectedItem.ToString() == "80_CUIT" || cboTipoDoc.SelectedItem.ToString() == "86_CUIL")
            {
                if (cboTipoDoc.SelectedItem.ToString().Length == 11)
                {
                    try
                    {
                        int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                        char[] nums = txtNroDoc.Text.ToCharArray();
                        int total = 0;
                        for (int i = 0; i < mult.Length; i++)
                        {
                            total += int.Parse(nums[i].ToString()) * mult[i];
                        }
                        int resto = total % 11;
                        int _digito1 = resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
                        int _digitorecibido = Convert.ToInt16(txtNroDoc.Text.Substring(txtNroDoc.Text.Length - 1));

                        if (_digito1 != _digitorecibido)
                            e.Cancel = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al validar el CUIT/CUIL."
                            + Environment.NewLine + ex.Message, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "La longitud de un CUIT o CUIL es de 11 digítos."
                            + Environment.NewLine + "Por favor solucione el error y luego fiscalice el comprobante desde la consola.",
                        "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Si cancela el comprobante no se podrá fiscalizar." + Environment.NewLine + "Confirma que desea cancelar?", "ICG Argentina",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _tipoDoc = 0;
                _nroDoc = "";
                this.Close();
            }
        }
    }
}
