﻿using AfipDll;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using IcgFceDll;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;
using static IcgFceDll.DataAccess;
using static IcgFceDll.CommonService;
using static IcgFceDll.RetailService.FceComun;

namespace IcgRetailFCE
{
    static class Program
    {
        /// <summary>
        /// Path Appliction.
        /// </summary>
        public static string _pathApp;
        /// <summary>
        /// Path Certificado.
        /// </summary>
        public static string _pathCertificado;
        /// <summary>
        /// Path Log.
        /// </summary>
        public static string _pathLog;
        /// <summary>
        ///Path Ticket de Accesos. 
        /// </summary>
        public static string _pathTa;
        /// <summary>
        /// Cuit de la empresa.
        /// </summary>
        public static string strCUIT;
        /// <summary>
        /// Informacion de IRSA
        /// </summary>
        public static InfoIrsa _irsa = new InfoIrsa();
        /// <summary>
        /// Path de slida de la info para olmos
        /// </summary>
        public static string _olmosPathSalida;
        /// <summary>
        /// Numero de cliente asignado por OLMOS
        /// </summary>
        public static string _olmosNroCliente;
        /// <summary>
        /// Numero de POS asiganado por OLMOS
        /// </summary>
        public static string _olmosNroPos;
        /// <summary>
        /// Rubro asigando por OLMOS
        /// </summary>
        public static string _olmosRubro;
        /// <summary>
        /// Proceso a ejecutar para el envio de la información a OLMOS.
        /// </summary>
        public static string _olmosProceso;
        /// <summary>
        /// Path donde informar para CABALLITO
        /// </summary>
        public static string _caballitoPathSalida;
        /// <summary>
        /// Identificador de Tienda SiTef.
        /// </summary>
        public static string _IdTiendaSiTef;
        /// <summary>
        /// Path de escritura para SiTef.
        /// </summary>
        public static string _PathSiTef;
        /// <summary>
        /// Punto de venta CAE.
        /// </summary>
        public static string _PtoVtaCAE;

        public static bool _EsTest;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            int intCodigoIVA;
            int intCodigoIIBB;
            string _nombreCertificado;
            //bool _EsTest;
            string _password;
            string _PtoVtaManual;
            //string _PtoVtaCAE;
            string _PtoVtaCAEA;
            bool _soloCaea;
            bool _generaXml = true;
            bool _mtxca = true;
            bool _desdoblaPagos = false;
            string _fileNameIcgPlugin = "";
            bool _soloManual = false;
            string _caja = "";
            int _cantidadTiquets = 0;
            int pasoTef = 0;
            /// <summary>
            /// Monto maximo para los clientes Consumidor Final.
            /// </summary>
            decimal _montoMaximo;

        ExeConfigurationFileMap efm = new ExeConfigurationFileMap { ExeConfigFilename = "IcgRetailFCEConsola.exe.config" };
            Configuration _configuration = ConfigurationManager.OpenMappedExeConfiguration(efm, ConfigurationUserLevel.None);
            if (_configuration.HasFile)
            {
                //Obtengo las key's
                String[] _keys = _configuration.AppSettings.Settings.AllKeys;
                intCodigoIVA = Convert.ToInt32(_configuration.AppSettings.Settings["CodigoIVA"].Value);
                intCodigoIIBB = Convert.ToInt32(_configuration.AppSettings.Settings["CodigoIIBB"].Value);
                _nombreCertificado = _configuration.AppSettings.Settings["NombreCertificado"].Value;
                strCUIT = _configuration.AppSettings.Settings["CUIT"].Value;
                _EsTest = Convert.ToBoolean(_configuration.AppSettings.Settings["IsTest"].Value);
                _password = _configuration.AppSettings.Settings["PassWord"].Value;
                _PtoVtaManual = _configuration.AppSettings.Settings["PtoVtaManual"].Value;
                _PtoVtaCAE = _configuration.AppSettings.Settings["PtoVtaCAE"].Value;
                _PtoVtaCAEA = _configuration.AppSettings.Settings["PtoVtaCAEA"].Value;
                _soloCaea = Convert.ToBoolean(_configuration.AppSettings.Settings["SoloCAEA"].Value);
                _mtxca = Convert.ToBoolean(_configuration.AppSettings.Settings["IsMtxca"].Value);
                _generaXml = Convert.ToBoolean(_configuration.AppSettings.Settings["GenaraXML"].Value);
                _desdoblaPagos = Convert.ToBoolean(_configuration.AppSettings.Settings["DesdoblaPagos"].Value);
                //soloManual
                if (_keys.Contains("SoloManual"))
                    _soloManual = Convert.ToBoolean(_configuration.AppSettings.Settings["SoloManual"].Value);
                else
                    _soloManual = false;
                //IRSA
                if (_keys.Contains("IrsaContrato"))
                    _irsa.contrato = _configuration.AppSettings.Settings["IrsaContrato"].Value;
                else
                    _irsa.contrato = "";
                if (_keys.Contains("IrsaLocal"))
                    _irsa.local = _configuration.AppSettings.Settings["IrsaLocal"].Value;
                else
                    _irsa.local = "";
                if (_keys.Contains("IrsaPathSalida"))
                    _irsa.pathSalida = _configuration.AppSettings.Settings["IrsaPathSalida"].Value;
                else
                    _irsa.pathSalida = "";
                if (_keys.Contains("IrsaPos"))
                    _irsa.pos = _configuration.AppSettings.Settings["IrsaPos"].Value;
                else
                    _irsa.pos = "";
                if (_keys.Contains("IrsaRubro"))
                    _irsa.rubro = _configuration.AppSettings.Settings["IrsaRubro"].Value;
                else
                    _irsa.rubro = "";
                //Olmos
                if (_keys.Contains("OlmosPathSalida"))
                    _olmosPathSalida = _configuration.AppSettings.Settings["OlmosPathSalida"].Value;
                else
                    _olmosPathSalida = "";
                if (_keys.Contains("OlmosNroCliente"))
                    _olmosNroCliente = _configuration.AppSettings.Settings["OlmosNroCliente"].Value;
                else
                    _olmosNroCliente = "";
                if (_keys.Contains("OlmosNroPos"))
                    _olmosNroPos = _configuration.AppSettings.Settings["OlmosNroPos"].Value;
                else
                    _olmosNroPos = "";
                if (_keys.Contains("OlmosRubro"))
                    _olmosRubro = _configuration.AppSettings.Settings["OlmosRubro"].Value;
                else
                    _olmosRubro = "";
                if (_keys.Contains("OlmosProceso"))
                    _olmosProceso = _configuration.AppSettings.Settings["OlmosProceso"].Value;
                else
                    _olmosProceso = "";
                //Caballito
                if (_keys.Contains("CaballitoPathSalida"))
                    _caballitoPathSalida = _configuration.AppSettings.Settings["CaballitoPathSalida"].Value;
                else
                    _caballitoPathSalida = "";
                //Caja
                if (_keys.Contains("Caja"))
                    _caja = _configuration.AppSettings.Settings["Caja"].Value;
                else
                    _caja = "";
                //Cantidad de tickets
                if (_keys.Contains("CantidadTiquets"))
                    _cantidadTiquets = Convert.ToInt32(_configuration.AppSettings.Settings["CantidadTiquets"].Value);
                else
                    _cantidadTiquets = 5;
                //Monto Maximo
                if (_keys.Contains("MontoMaximo"))
                    _montoMaximo = Convert.ToDecimal(_configuration.AppSettings.Settings["MontoMaximo"].Value);
                else
                    _montoMaximo = 43010;
                //IdTienda SiTef
                if (_keys.Contains("IdTiendaSitef"))
                    _IdTiendaSiTef = _configuration.AppSettings.Settings["IdTiendaSitef"].Value.PadLeft(8, '0');
                else
                    _IdTiendaSiTef = "";
                //Path SiTef
                if (_keys.Contains("PathSitef"))
                    _PathSiTef = _configuration.AppSettings.Settings["PathSitef"].Value;
                else
                    _PathSiTef = "";
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }

            if (_desdoblaPagos)
            {
                _fileNameIcgPlugin = "DesdoblarMediosPagoFile.xml";
                //Lanzamos el programa de XaerSoft para el desdoble de pagos.
                if (File.Exists("icgretailplugins_5.exe"))
                {
                    Process p = new Process();
                    ProcessStartInfo psi = new ProcessStartInfo("icgretailplugins_5.exe");
                    p.StartInfo = psi;
                    p.Start();
                    p.WaitForExit();
                    //Espero 2 segundo.
                    System.Threading.Thread.Sleep(2000);
                    Application.DoEvents();
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "No se ha encontrado el plugin icgretailplugins_5.exe." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }
            }
            else
                _fileNameIcgPlugin = "fce.xml";

            //Datos XML
            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            //
            string _dummy = "FALSE";            

            try
            {
                DataAccess.FuncionesVarias.ValidarCarpetas(_nombreCertificado, out _pathApp, out _pathCertificado, out _pathLog, out _pathTa);

                //Consulta si existe el archivo.Caso contrario da error.
                if (File.Exists(_fileNameIcgPlugin))
                {
                    XmlDocument xDoc = new XmlDocument();
                    //xDoc.Load("DesdoblarMediosPagoFile.xml");
                    xDoc.Load(_fileNameIcgPlugin);

                    XmlNodeList _serverXml = xDoc.GetElementsByTagName("server");
                    _server = _serverXml[0].InnerText;

                    XmlNodeList _databaseXml = xDoc.GetElementsByTagName("database");
                    _database = _databaseXml[0].InnerText;

                    XmlNodeList _userXml = xDoc.GetElementsByTagName("user");
                    _user = _userXml[0].InnerText;

                    XmlNodeList _codVendedorXml = xDoc.GetElementsByTagName("codvendedor");
                    _codVendedor = _codVendedorXml[0].InnerText;

                    XmlNodeList _tipodocXml = xDoc.GetElementsByTagName("tipodoc");
                    _tipodoc = _tipodocXml[0].InnerText;

                    XmlNodeList serieXml = xDoc.GetElementsByTagName("serie");
                    _serie = serieXml[0].InnerText;

                    XmlNodeList numeroXml = xDoc.GetElementsByTagName("numero");
                    _numero = numeroXml[0].InnerText;

                    XmlNodeList nXml = xDoc.GetElementsByTagName("n");
                    _n = nXml[0].InnerText;

                    if (xDoc.GetElementsByTagName("GuardandoTef").Count > 0)
                    {
                        XmlNodeList guardandoTef = xDoc.GetElementsByTagName("GuardandoTef");
                        pasoTef = Convert.ToInt32(guardandoTef[0].InnerText);
                    }
                    else
                        pasoTef = 0;
                    if (pasoTef == 0)
                    {
                        if (xDoc.GetElementsByTagName("GuardandoTef2").Count > 0)
                        {
                            XmlNodeList guardandoTef2 = xDoc.GetElementsByTagName("GuardandoTef2");
                            pasoTef = Convert.ToInt32(guardandoTef2[0].InnerText);
                        }
                        else
                            pasoTef = 0;
                    }

                    //Salimos si es un pedido.
                    if (_tipodoc.ToUpper() == "PEDVENTA")
                    {
                        return;
                    }
                    //Salimos si es un remito.
                    if (_tipodoc.ToUpper() == "ALBCOMPRA")
                    {
                        return;
                    }
                    //Salimos si es un remito.
                    if (_tipodoc.ToUpper() == "ALBVENTA")
                    {
                        return;
                    }
                    //Salimos si es un Presupuesto.
                    if (_tipodoc.ToUpper() == "PRESUPUESTO")
                    {
                        return;
                    }
                    //Salimos si es un Factura de compra.
                    if (_tipodoc.ToUpper() == "FACCOMPRA")
                    {
                        return;
                    }
                    //Salimos si es un Pedido de compra.
                    if (_tipodoc.ToUpper() == "PEDCOMPRA")
                    {
                        return;
                    }

                    if (pasoTef == 0)
                    {
                        //Solo imprimimos las que N = B
                        if (_n.ToUpper() == "B")
                        {
                            //Armamos el stringConnection.
                            string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                            //Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                try
                                {
                                    //Abro la conexion al SQL.
                                    _connection.Open();
                                    //Busco el comprobante en FacturasVentaSerieResol.
                                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    //Busca los datos de la Factura. Si no posee CAE, avanza, caso contrario da error.
                                    DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFacturaRetail(_serie, Convert.ToInt32(_numero), _n, _PtoVtaCAE, _connection);
                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_fsr.NumeroFiscal == 0)
                                    {
                                        //Vemos si ya tiene CAE.
                                        if (String.IsNullOrEmpty(_Fc.CAE))
                                        {
                                            //Veo para 2019 si el regimen de facturación es distinto de N
                                            if (_Fc.regfacturacioncliente != "N")
                                            {
                                                if (!_soloManual)
                                                {
                                                    //Armamos la password como segura
                                                    SecureString strPasswordSecureString = new SecureString();
                                                    foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                                    strPasswordSecureString.MakeReadOnly();
                                                    //Vemos si vamos por MtxCa o no
                                                    if (_mtxca)
                                                    {
                                                        ///FACTURACION MTXCA
                                                        long _ultimoICG = 0;
                                                        List<AfipDll.WsMTXCA.Errors> _lstError = new List<WsMTXCA.Errors>();
                                                        //Recupero el nro del último comprobante.
                                                        Int64 _ultimoComprobante = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCertificado, _pathTa, _pathLog,
                                                            Convert.ToInt32(_Fc.PtoVta), Convert.ToInt32(_Fc.cCodAfip), strCUIT, _EsTest,
                                                            strPasswordSecureString, false, out _lstError);
                                                        //Vemos si tengo errores.
                                                        if (_lstError.Count() == 0)
                                                        {
                                                            bool _estoyOkAfip = RetailService.Common.ValidarUltimoNroComprobante(_Fc.PtoVta, _ultimoComprobante, _Fc.cCodAfip, _connection, out _ultimoICG);
                                                            if (!_estoyOkAfip)
                                                            {
                                                                string _error = "VAL-ICG: El último Número de AFIP no se corresponde con el ultimo en ICG." + Environment.NewLine +
                                                                    "Ultimo informado por AFIP: " + _ultimoComprobante.ToString() +
                                                                    Environment.NewLine + "Ultimo informado por ICG: " + _ultimoICG.ToString() +
                                                                    Environment.NewLine + "Por favor Verifique que el comprobante no este anulado";
                                                                MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Último AFIP ->" + _ultimoComprobante.ToString());
                                                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Último ICG ->" + _ultimoICG.ToString());
                                                                //Consulto el comprobante.
                                                                WsMTXCA.ConsultaComprobanteResponse _consulta = new WsMTXCA.ConsultaComprobanteResponse();
                                                                List<WsMTXCA.Errors> _lstErrorCons = new List<WsMTXCA.Errors>();
                                                                WsMTXCA.ConsultarComprobanteAutorizado(_pathCertificado, _pathTa, _pathLog,
                                                                    strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _ultimoComprobante, _EsTest, strPasswordSecureString,
                                                                    _generaXml, out _consulta, out _lstErrorCons);
                                                                if (_lstErrorCons.Count == 0)
                                                                {
                                                                    if (!String.IsNullOrEmpty(_consulta.observaciones))
                                                                    {
                                                                        if (_consulta.observaciones.Split('-').Length == 3)
                                                                        {
                                                                            string _serieCons = _consulta.observaciones.Split('-')[0];
                                                                            string _numeroCons = _consulta.observaciones.Split('-')[1];
                                                                            string _nCons = _consulta.observaciones.Split('-')[2];
                                                                            //Recupero los datos de la Venta.
                                                                            DataAccess.DatosFactura _fcCons = DataAccess.DatosFactura.GetDatosFactura(_serieCons, Convert.ToInt32(_numeroCons), _nCons, _connection);
                                                                            if (_fcCons.NroFiscal == 0)
                                                                            {
                                                                                //Genero codigo de barra
                                                                                string _codBarra = "";
                                                                                string _fechaVto = _consulta.fechaVencimiento.Year.ToString() + _consulta.fechaVencimiento.Month.ToString().PadLeft(2, '0') + _consulta.fechaVencimiento.Day.ToString().PadLeft(2, '0');
                                                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _fcCons.cCodAfip, _fcCons.PtoVta.ToString(), _consulta.codigoAutorizacion.ToString(), _fechaVto, out _codBarra);
                                                                                //Genero el codigo del QR.
                                                                                string _qr = QR.CrearJson(1, _fcCons.fecha, Convert.ToInt64(strCUIT), _consulta.puntoVenta, _consulta.tipoComprobante,
                                                                                    Convert.ToInt32(_consulta.numeroCbte), _consulta.importeTotal, _consulta.codigoMoneda, _consulta.cotizacionMoneda,
                                                                                    _consulta.tipoDocumento, _consulta.numeroDocumento, "E", _consulta.codigoAutorizacion);
                                                                                //Insertamos en FacturasVentasSeriesResol
                                                                                DataAccess.FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_fcCons, Convert.ToInt32(_consulta.numeroCbte), _pathLog, _connection);
                                                                                //Insertamos en FacturasCamposLibres
                                                                                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_serieCons, _numeroCons,
                                                                                    _nCons, _consulta.codigoAutorizacion.ToString(), "", _fechaVto, _codBarra, _qr, _pathLog, _fcCons.PtoVta.ToString().PadLeft(5, '0'), _connection);
                                                                                //Comprobante recuperado. Vemos si es el mismo o no
                                                                                if (_serie != _serieCons || _numero != _numeroCons || _n != _nCons)
                                                                                {
                                                                                    //Fiscalizo.
                                                                                    string _erroMsj = "";
                                                                                    bool _rta = RetailService.Mtxca.Fiscalizar(_Fc, intCodigoIVA, intCodigoIIBB, _PtoVtaCAE, _PtoVtaCAEA, _password, _soloCaea, _EsTest, _generaXml,
                                                                                            _pathLog, _pathCertificado, _pathTa, strCUIT, _codVendedor, _connection, out _erroMsj);
                                                                                    if (!_rta)
                                                                                    {
                                                                                        if (_erroMsj.StartsWith("Error AFIP"))
                                                                                        {
                                                                                            //Vemos si tenemos CAEA
                                                                                            if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                                                                            {
                                                                                                //Grabo todo con una transaccion.
                                                                                                if (!DataAccess.Transactions.TransactionCAEA2(_serie, _numero, _n, _codVendedor, _PtoVtaCAEA, strCUIT, _pathLog, _connection))
                                                                                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la informacion en el sistema. Consulte el log.",
                                                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                MessageBox.Show(new Form { TopMost = true }, "Hubo un error al conectar con el servicio de la AFIP. Intente mas tarde." + Environment.NewLine +
                                                                                                    "Error: " + _erroMsj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            MessageBox.Show(new Form { TopMost = true }, _erroMsj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                string _errorCons = "VAL-ICG: El comprobante faltante de la AFIP, ya posee número fiscal." + Environment.NewLine +
                                                                                "Por favor comuniquese con ICG Argentina.";
                                                                                MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            string _errorCons = "VAL-ICG: El comprobante recuperado de AFIP, no posee los datos necesarios para insertarlo automáticamente.." + Environment.NewLine +
                                                                                "Por favor comuniquese con ICG Argentina.";
                                                                            MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        string _errorCons = "VAL-ICG: El comprobante recuperado de AFIP, no posee los datos necesarios para insertarlo automáticamente.." + Environment.NewLine +
                                                                            "Por favor comuniquese con ICG Argentina.";
                                                                        MessageBox.Show(new Form { TopMost = true }, _error, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                string _erroMsj = "";
                                                                bool _rta = RetailService.Mtxca.Fiscalizar(_Fc, intCodigoIVA, intCodigoIIBB, _PtoVtaCAE, _PtoVtaCAEA, _password, _soloCaea, _EsTest, _generaXml,
                                                                        _pathLog, _pathCertificado, _pathTa, strCUIT, _codVendedor, _connection, out _erroMsj);
                                                                if (!_rta)
                                                                {
                                                                    string _tipoMsj = _erroMsj.Split('|')[0];
                                                                    if (_tipoMsj.ToUpper() == "ERROR")
                                                                    {
                                                                        string _msj = _erroMsj.Split('|')[1];
                                                                        MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        { }
                                                    }
                                                    else
                                                    {
                                                        //Valido si hay comprobantes sin fiscalizar
                                                        if (!RetailService.Common.ExistenComprobantesConFechaAnterior(_Fc.fecha, _caja, _connection))
                                                        {
                                                            if (CommonService.Validaciones.ValidarMontoMaximoConsumidorFinal(_montoMaximo, _Fc))
                                                            {
                                                                ///FACTURACION NORMAL
                                                                List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, _connection);
                                                                _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                                                //List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, _connection, _Fc.TotalBruto);
                                                                List<Modelos.FacturasVentasTot> _Tributos = RetailService.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, _connection, _Fc.TotalBruto);
                                                                _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                                                List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, _connection);
                                                                _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                                                //Asigno el punto de venta de CAE.
                                                                _Fc.PtoVta = _PtoVtaCAE;
                                                                //Validamos los datos de la FC.
                                                                DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);

                                                                //Veo si aplico Solo CAEA
                                                                if (_soloCaea)
                                                                {
                                                                    _dummy = "FALSE";
                                                                }
                                                                else
                                                                {
                                                                    //Vemos si esta disponible el servicio de AFIP.
                                                                    _dummy = wsAfip.TestDummy(_pathLog, _EsTest);
                                                                }
                                                                if (_dummy == "OK")
                                                                {
                                                                    RetailService.FceComun.Resultado resultado = new RetailService.FceComun.Resultado();
                                                                    InfoTransaccionAFIP existeAnterior = CommonService.TransaccionAFIP.GetTransaccionAFIP(_serie, Convert.ToInt32(_numero), _connection);
                                                                    if (existeAnterior.cae != null)
                                                                    {
                                                                        DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc.NumSerie, _Fc.NumFactura, _Fc.N, existeAnterior.cae, "", existeAnterior.fechavto, existeAnterior.codigobara, existeAnterior.codigoqr, _pathLog, existeAnterior.tipocae == "CAE" ? _PtoVtaCAE : _PtoVtaCAEA, _connection);
                                                                        DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_Fc, existeAnterior.nrofiscal, _connection);
                                                                        string _terminal = Environment.MachineName;
                                                                        //Inserto en Rem_transacciones.
                                                                        DataAccess.RemTransacciones.Retail_InsertRemTransacciones(_terminal, _Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _Fc.N, 1, _connection);
                                                                        //armamos respuesta.
                                                                        List<string> _msg = new List<string>();
                                                                        resultado = new Resultado { afipOK = true, cae = existeAnterior.cae, fechaVto = existeAnterior.fechavto, message = _msg };
                                                                    }
                                                                    else
                                                                    {
                                                                        RetailService.FceComun.FiscalizarAFIP(_pathCertificado, _pathTa, _pathLog, strCUIT,
                                                                            _Fc, _Iva, _Tributos, _EsTest, strPasswordSecureString, _connection, out resultado);
                                                                    }
                                                                    //Analizmos el resultado.
                                                                    if (String.IsNullOrEmpty(resultado.cae))
                                                                    {
                                                                        //Afip con Error, muestro el mensaje.
                                                                        if (resultado.afipOK)
                                                                        {
                                                                            if (resultado.message.Count > 0)
                                                                            {
                                                                                string _msj = "";
                                                                                foreach (string st in resultado.message)
                                                                                {
                                                                                    if (String.IsNullOrEmpty(_msj))
                                                                                        _msj = "Se produjo el siguiente error." + Environment.NewLine + st;
                                                                                    else
                                                                                        _msj = _msj + Environment.NewLine + st;
                                                                                }
                                                                                MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            //Transaccion CAEA
                                                                            if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                                                            {
                                                                                //Grabo todo con una transaccion.
                                                                                string message = "";
                                                                                if (!RetailService.FceComun.TransactionCAEA(_serie, _numero, _n, _codVendedor, _PtoVtaCAEA, strCUIT, _pathLog, _Fc, _connection, out message))
                                                                                    MessageBox.Show(message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                                //Recupero el contador para el CAEA
                                                                                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.cCodAfip, _connection);
                                                                                if (_dtoContador.contador % _cantidadTiquets == 0)
                                                                                {
                                                                                    string _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                                    MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                                                    {
                                                                        //Grabo todo con una transaccion.
                                                                        string message = "";
                                                                        if (!RetailService.FceComun.TransactionCAEA(_serie, _numero, _n, _codVendedor, _PtoVtaCAEA, strCUIT, _pathLog, _Fc, _connection, out message))
                                                                            MessageBox.Show(message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                        //Recupero el contador para el CAEA
                                                                        DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.cCodAfip, _connection);
                                                                        if (_dtoContador.contador % _cantidadTiquets == 0)
                                                                        {
                                                                            string _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                            MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                        }
                                                                    }
                                                                    else
                                                                        MessageBox.Show("El servicio de la AFIP no esta disponible en este momento. Intente mas tarde.",
                                                                                "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "Para Facturas B con Montos mayores a " + _montoMaximo.ToString() + Environment.NewLine +
                                                                    "Por resolución de AFIP, es obligatorio identificar al comprador." + Environment.NewLine +
                                                                    "Por favor ingrese los datos del cliente.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Error);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show("Existen comprobantes con fecha anterior sin fiscalizar." + Environment.NewLine +
                                                                "Debe fiscalizar estos comprobantes antes, para un correcto procesamiento en AFIP." + Environment.NewLine +
                                                                "Ingrese a la consola y fiscalice todos comprobantes con fecha anterior.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //FACTURAS MANUALES
                                                    string _tipoComprobante = DataAccess.FuncionesVarias.QueComprobanteEs(_serie, _numero, _n, _codVendedor, _connection);
                                                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaManual, _tipoComprobante.Substring(0, 3), _connection);
                                                    //Vemos si tenemos contador.
                                                    if (_dtoContador.serieresol == null)
                                                    {
                                                        string _rta = "No se encontro un contador definico para el punto de Venta: " + _PtoVtaManual + Environment.NewLine +
                                                            "Por favor comuniquese con ICG Argentina.";
                                                        MessageBox.Show(_rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                        return;
                                                    }
                                                    frmIngresoManual frm = new frmIngresoManual(_tipoComprobante, _PtoVtaManual, _dtoContador.contador + 1);
                                                    frm.ShowDialog();
                                                    string _ptoVta = frm._ptoVta;
                                                    int _nroCbte = frm._nroCbte;
                                                    frm.Dispose();
                                                    if (_nroCbte > 0)
                                                    {
                                                        bool _Ok = RetailService.FceComun.TransactionManual(_serie, _numero, _n, _codVendedor,
                                                            _ptoVta, _nroCbte, strCUIT, _pathLog, _connection);
                                                        if (!_Ok)
                                                        {
                                                            string _numeroError = _ptoVta.ToString().PadLeft(4, '0') + "-" + _nroCbte.ToString().PadLeft(8, '0');
                                                            string _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                                                            MessageBox.Show(_rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //Realizamos una transaccion en Negro.
                                                RetailService.FceComun.TransaccionBlack(_Fc, _connection);
                                            }
                                        }
                                        else
                                        {
                                            AfipDll.LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante ya posee CAE: " + _Fc.CAE);
                                            if (!String.IsNullOrEmpty(_Fc.Comprobante))
                                            {
                                                int _numeroFiscal = Convert.ToInt32(_Fc.Comprobante.Split('-')[1]);
                                                //Grabo FacturasVentaSerieResol.
                                                FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_Fc, _numeroFiscal, _pathLog, _connection);
                                            }
                                            else
                                            {
                                                InfoTransaccionAFIP aFIP = TransaccionAFIP.GetTransaccionAFIP(_Fc.NumSerie, Convert.ToInt32(_Fc.NumFactura), _connection);
                                                //Grabo FacturasVentaSerieResol.
                                                FacturasVentaSeriesResol.Grabar_FacturasVentaSerieResol(_Fc, aFIP.nrofiscal, _pathLog, _connection);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Si es una transaccion en negro 
                                        if (_fsr.NumeroFiscal < 0)
                                        {
                                            //Si es una ABONO
                                            if (_tipodoc.ToUpper() == "ABONO")
                                                RetailService.FceComun.TransaccionBlack(_Fc, _connection);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message != "Se produjo una excepción de tipo 'System.Exception'.")
                                        AfipDll.LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error: " + ex.Message);
                                    if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                    {
                                        if (ex.Message.StartsWith("ICG-VAL"))
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                                "Por favor revise los datos y luego fiscalice el comprobante desde la consola.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            //Grabo todo con una transaccion.
                                            if (!DataAccess.Transactions.TransactionCAEA2(_serie, _numero, _n, _codVendedor, _PtoVtaCAEA, strCUIT, _pathLog, _connection))
                                                MessageBox.Show(new Form { TopMost = true }, "Se produjo un error al grabar la informacion en el sistema. Consulte el log.",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                    else
                                        MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                                                "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                                }
                                LanzarInfoShoppings(_serie, Convert.ToInt32(_numero), _n, _connection);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el archivo fce.xml. Por favor comuniquese con ICG Argentina.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private static List<AfipDll.wsAfip.ComprobanteAsoc> ArmarCbteAsoc(DataAccess.DatosFactura fc, string strCUIT, SqlConnection _connection)
        {
            List<AfipDll.wsAfip.ComprobanteAsoc> _lst = new List<AfipDll.wsAfip.ComprobanteAsoc>();
            try
            {
                //using (SqlConnection _conexion = new SqlConnection(_connection))
                //{
                    List<DataAccess.ComprobanteAsociado> _icgCompAsoc = DataAccess.ComprobanteAsociado.GetComprobantesAsociados(fc.NumSerie, Convert.ToInt32(fc.NumFactura), fc.N, _connection);

                    foreach (DataAccess.ComprobanteAsociado _ca in _icgCompAsoc)
                    {
                        AfipDll.wsAfip.ComprobanteAsoc _cls = DataAccess.ComprobanteAsociado.GetAfipComprobanteAsocNew(_ca.numSerie, _ca.numAlbaran, _ca.n, strCUIT, _connection);
                        _lst.Add(_cls);
                    }
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
        /// <summary>
        /// Metodo que lanza los procesos para informar a los shoppings
        /// </summary>
        /// <param name="_serie">Serie de la venta.</param>
        /// <param name="_numero">Numero de la venta.</param>
        /// <param name="_n">N de la venta.</param>        
        /// <param name="_connection">Conexión SQL.</param>
        private static void LanzarInfoShoppings(string _serie, int _numero, string _n, SqlConnection _connection)
        {
            //Vemos si tenemos que lanzar IRSA.
            if (!String.IsNullOrEmpty(_irsa.pathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        Irsa.Retail.LanzarTrancomp(_serie, _numero, _n, _irsa, _fsr.SerieFiscal2.Substring(0, 3), _fsr.SerieFiscal1 + "-" + _fsr.NumeroFiscal.ToString(), _connection);
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\IRSALOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\IRSALOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\IRSALOG\icgIrsa.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que lanzar Shoppping Caballito.
            if (!String.IsNullOrEmpty(_caballitoPathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        ShoppingCaballitoService.Retail.GenerarShoppingCaballito(_serie, _numero.ToString(), _n, _connection, _caballitoPathSalida,
                            _fsr.NumeroFiscal.ToString().PadLeft(8, '0'), Convert.ToInt32(_fsr.SerieFiscal1).ToString().PadLeft(4, '0'));
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\CABALLITOLOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\CABALLITOLOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\CABALLITOLOG\icgCaballito.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que lanzar Shoppping Caballito.
            if (!String.IsNullOrEmpty(_olmosPathSalida))
            {
                try
                {
                    DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                    if (_fsr.NumeroFiscal > 0)
                    {
                        ShoppingOlmosService.Retail.LanzarShoppingOlmos(_serie, Convert.ToInt32(_numero), _n, _olmosPathSalida, _fsr.NumeroFiscal.ToString(),
                            Convert.ToInt32(_olmosNroCliente), Convert.ToInt32(_olmosNroPos), Convert.ToInt32(_olmosRubro), Convert.ToInt64(strCUIT),
                            _olmosProceso, true, _connection);
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(@"C:\ICG\OLMOSLOG"))
                    {
                        Directory.CreateDirectory(@"C:\ICG\OLMOSLOG");
                    }
                    using (StreamWriter sw = new StreamWriter(@"C:\ICG\OLMOSLOG\icgOlmos.log", true))
                    {
                        sw.WriteLine(DateTime.Today.ToShortDateString() + ex.Message);
                        sw.Flush();
                    }
                }
            }

            //Vemos si tenemos que mandar a IRSA SITEF.
            if (!String.IsNullOrEmpty(_PathSiTef))
            {
                //Validamos que tenemos numero fiscal.
                DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_serie, _numero, _n, _connection);
                if (_fsr.NumeroFiscal > 0)
                {
                    if (!_EsTest)
                    {
                        IcgFceDll.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _IdTiendaSiTef, _PtoVtaCAE.PadLeft(8, '0'), strCUIT, "30711277249", _PathSiTef, false, _connection);
                    }
                    else
                    {
                        IcgFceDll.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _IdTiendaSiTef, "ICG00000", "30711277249", "30711277249", _PathSiTef, false, _connection);
                    }
                }
            }
        }
    }
}
