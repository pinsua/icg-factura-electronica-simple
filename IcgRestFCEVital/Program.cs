﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using AfipDll;
using IcgFceDll;

namespace IcgRestFCEVital
{
    class Program
    {
        /// <summary>
        /// Path de la aplicación
        /// </summary>
        public static string _pathApp;
        /// <summary>
        /// Path del certificado
        /// </summary>
        public static string _pathCertificado;
        /// <summary>
        /// Path del Log.
        /// </summary>
        public static string _pathLog;
        /// <summary>
        /// Path del ticket de acceso.
        /// </summary>
        public static string _pathTa;
        /// <summary>
        /// Monotributista Si/No.
        /// </summary>
        public static bool _IsMonotributista;
        /// <summary>
        /// Numero de Caja
        /// </summary>
        public static int _caja;
        /// <summary>
        ///Punto de venta de CAE 
        /// </summary>
        public static string _PtoVtaCAE;
        /// <summary>
        /// Punto de venta de CAEA
        /// </summary>
        public static string _PtoVtaCAEA;
        /// <summary>
        /// Punto de venta Manual
        /// </summary>
        public static string _PtoVtaManual;
        /// <summary>
        /// Solo procesa por CAEA.
        /// </summary>
        public static bool _soloCaea;
        /// <summary>
        /// indica si procesa en producción u homologación.
        /// </summary>
        public static bool _EsTest;
        /// <summary>
        /// Nro de CUIT del Emisor.
        /// </summary>
        public static string strCUIT = "";
        /// <summary>
        /// Codigo interno de IVA
        /// </summary>
        public static int intCodigoIVA = 0;
        /// <summary>
        /// Codigo interno de IIBB
        /// </summary>
        public static int intCodigoIIBB = 0;
        /// <summary>
        /// Nombre del Certificado de AFIP.
        /// </summary>
        public static string _nombreCertificado = "";
        /// <summary>
        /// Password del certificado de AFIP.
        /// </summary>
        public static string _password = "";
        /// <summary>
        /// Monto maximo a permitir para CAEA cuando el cliente no esta identificado.
        /// </summary>
        public static decimal _montoMaximo = 0;
        /// <summary>
        /// Cantidad de tiquets para desplegar los msj.
        /// </summary>
        public static int _cantidadTiquets = 5;
        /// <summary>
        /// Clase para la información de Sitef.
        /// </summary>
        public static SiTefServiceRest.InfoSitef _siTef = new SiTefServiceRest.InfoSitef();
        //Irsa
        public static InfoIrsa _irsa = new InfoIrsa();

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string _Archivo = Path.Combine(Application.StartupPath, "IcgRestFCEConsola.exe");
            //Seteo las variables booleanas.
            _EsTest = true;
            _IsMonotributista = false;
            _soloCaea = false;
            //Cargo el archivo de configuración
            ExeConfigurationFileMap efm = new ExeConfigurationFileMap { ExeConfigFilename = "IcgRestFCEConsola.exe.config" };
            Configuration _configuration = ConfigurationManager.OpenMappedExeConfiguration(efm, ConfigurationUserLevel.None);
            if (_configuration.HasFile)
            {
                string[] _key = _configuration.AppSettings.Settings.AllKeys;
                intCodigoIVA = _key.Contains("CodigoIVA") ? Convert.ToInt32(_configuration.AppSettings.Settings["CodigoIVA"].Value) : 5;
                intCodigoIIBB = _key.Contains("CodigoIIBB") ? Convert.ToInt32(_configuration.AppSettings.Settings["CodigoIIBB"].Value) : 1;
                _nombreCertificado = _key.Contains("NombreCertificado") ? _configuration.AppSettings.Settings["NombreCertificado"].Value : "";
                strCUIT = _key.Contains("CUIT") ? _configuration.AppSettings.Settings["CUIT"].Value : "";
                _EsTest = _key.Contains("IsTest") ? Convert.ToBoolean(_configuration.AppSettings.Settings["IsTest"].Value) : true;
                _password = _key.Contains("PassWord") ? _configuration.AppSettings.Settings["PassWord"].Value : "";
                _PtoVtaManual = _key.Contains("PtoVtaManual") ? _configuration.AppSettings.Settings["PtoVtaManual"].Value : "";
                _PtoVtaCAE = _key.Contains("PtoVtaCAE") ? _configuration.AppSettings.Settings["PtoVtaCAE"].Value : "";
                _PtoVtaCAEA = _key.Contains("PtoVtaCAEA") ? _configuration.AppSettings.Settings["PtoVtaCAEA"].Value : "";
                _soloCaea = _key.Contains("SoloCAEA") ? Convert.ToBoolean(_configuration.AppSettings.Settings["SoloCAEA"].Value) : false;
                _caja = _key.Contains("Caja") ? Convert.ToInt32(_configuration.AppSettings.Settings["Caja"].Value) : 0;
                _IsMonotributista = _key.Contains("Monotributista") ? Convert.ToBoolean(_configuration.AppSettings.Settings["Monotributista"].Value) : false;
                _montoMaximo = _key.Contains("MontoMaximo") ? Convert.ToDecimal(_configuration.AppSettings.Settings["MontoMaximo"].Value) : 15000;
                _cantidadTiquets = _key.Contains("CantidadTiquets") ? Convert.ToInt32(_configuration.AppSettings.Settings["CantidadTiquets"].Value) : 5;
                //Clover
                _siTef.sitefIdTienda = _key.Contains("IdTienda") ? _configuration.AppSettings.Settings["IdTienda"].Value : "";
                _siTef.sitefIdTerminal = _key.Contains("IdTerminal") ? _configuration.AppSettings.Settings["IdTerminal"].Value:"";
                _siTef.sitefCuit = _key.Contains("CUIT") ? _configuration.AppSettings.Settings["CUIT"].Value:"";
                _siTef.sitefCuitIsv = _key.Contains("CuitIsv") ? _configuration.AppSettings.Settings["CuitIsv"].Value:"";
                _siTef.pathSendInvoice = _key.Contains("PathInvoice") ? _configuration.AppSettings.Settings["PathInvoice"].Value:"";
                _siTef.usaClover = _key.Contains("UsaClover") ? Convert.ToBoolean(_configuration.AppSettings.Settings["UsaClover"].Value) : false;
                //Irsa
                _irsa.contrato = _key.Contains("IrsaContrato") ? _configuration.AppSettings.Settings["IrsaContrato"].Value : "";
                _irsa.local = _key.Contains("IrsaLocal") ? _configuration.AppSettings.Settings["IrsaLocal"].Value:"" ;
                _irsa.pos = _key.Contains("IrsaPos") ? _configuration.AppSettings.Settings["IrsaPos"].Value : "";
                _irsa.rubro = _key.Contains("IrsaRubro") ? _configuration.AppSettings.Settings["IrsaRubro"].Value : "";
                _irsa.pathSalida = _key.Contains("IrsaPath") ? _configuration.AppSettings.Settings["IrsaPath"].Value : "";
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }

            //Definimos las variables.
            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            string _fo = "";
            string _dummy = "";

            try
            {
                DataAccess.FuncionesVarias.ValidarCarpetas(_nombreCertificado, out _pathApp, out _pathCertificado, out _pathLog, out _pathTa);

                if (File.Exists("fce.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("fce.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                    foreach (XmlNode xn in _List1)
                    {
                        _server = xn["server"].InnerText;
                        _database = xn["database"].InnerText;
                        _user = xn["user"].InnerText;
                    }

                    XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                    foreach (XmlNode xn in _List2)
                    {
                        _codVendedor = xn["codvendedor"].InnerText;
                        _tipodoc = xn["tipodoc"].InnerText;
                        _serie = xn["serie"].InnerText;
                        _numero = xn["numero"].InnerText;
                        _n = xn["n"].InnerText;
                        _fo = xn["fo"].InnerText;
                    }
                    //Recupero el nombre de la terminal.
                    string _terminal = Environment.MachineName;
                    //Salimos si es un pedido.
                    if (_tipodoc.ToUpper() == "PEDVENTA")
                    {
                        return;
                    }
                    //Salimos si es un remito.
                    if (_tipodoc.ToUpper() == "ALBCOMPRA")
                    {
                        return;
                    }
                    //Salimos si es un remito.
                    if (_tipodoc.ToUpper() == "ALBVENTA")
                    {
                        return;
                    }
                    //Solo imprimimos las que N = B
                    if (_n.ToUpper() == "B")
                    {
                        //Armamos el stringConnection.
                        string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                        //Obtenemos el primer digito de la serie.
                        string _digitoSerie = _serie.Substring(0, 1);
                        //Solo imprimimos las que poseen el primer digito de la serie = F
                        if (_digitoSerie != "G" && _digitoSerie != "S" && _digitoSerie != "I")
                        {
                            //Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                try
                                {
                                    _connection.Open();
                                    //Vemos si es un ABONO
                                    if (_tipodoc.ToUpper() == "ABONO")
                                    {
                                        //Limpio los datos del ABONO.
                                        RestService.LimpioDatosFiscalesAbono(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    }
                                    //Busco el Tiquet.
                                    Modelos.Tiquet _tiquet = IcgFceDll.RestService.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_tiquet.NumeroFiscal == 0)
                                    {
                                        //Factura Electronica comun.
                                        Modelos.DatosFactura _Fc = RestService.GetDatosFactura(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codVendedor), _connection);
                                        //Vemos si el total es cero. Si es así lo mandamos como Black.
                                        if (_Fc.totalbruto == 0)
                                        {
                                            //Mando la transaccion Black.
                                            RestService.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                                            //Salgo 
                                            return;
                                        }
                                        //Valido la fecha.
                                        if (!RestService.ExistenComprobantesConFechaAnterior(_Fc.fecha, _caja.ToString(), _connection))
                                        {
                                            //Vemos si es monotribitista.
                                            if (_IsMonotributista)
                                            {
                                                _Fc.codAfipComprobante = _Fc.totalneto > 0 ? 11 : 13;
                                                _Fc.serieFiscal = _Fc.codAfipComprobante == 11 ? "011_Factura_C" : "013_NCredito_C";
                                            }
                                            //Array de IVA
                                            List<Modelos.TiquetsTot> _Iva = RestService.GetTotalesIva(_Fc.serie, _Fc.n, _Fc.numero, intCodigoIVA, _connection);
                                            _Fc.totIVA = _Iva.Sum(x => x.TotIva);
                                            //Array de tributos
                                            List<Modelos.TiquetsTot> _Tributos = new List<Modelos.TiquetsTot>(); // DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIIBB, _connection, _Fc.TotalBruto);
                                            _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                            //Array de Importes no gravados.
                                            List<Modelos.TiquetsTot> _NoGravado = RestService.GetTotalesIvaNoGravado(_Fc.serie, _Fc.n, _Fc.numero, intCodigoIVA, _connection); // DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, intCodigoIVA, _connection);
                                            _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                            //Asigno el punto de venta de CAE.
                                            _Fc.ptoVta = Convert.ToInt32(_PtoVtaCAE);

                                            //Validamos los datos de la FC.
                                            RestService.ValidarDatosFactura(_Fc);
                                            //Armamos la password como segura
                                            SecureString strPasswordSecureString = new SecureString();
                                            foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                            strPasswordSecureString.MakeReadOnly();
                                            if (_soloCaea)
                                            {
                                                _dummy = "FALSE";
                                            }
                                            else
                                            {
                                                //Vemos si esta disponible el servicio de AFIP.
                                                _dummy = wsAfip.TestDummy(_pathLog, _EsTest);
                                            }
                                            if (_dummy.ToUpper() == "OK")
                                            {
                                                int intNroComprobante = 0;
                                                //string _Error;
                                                List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                                                //Recupero el nro del ultimo comprobante.
                                                wsAfip.UltimoComprobante _ultimo = wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTa, _pathLog,
                                                    _Fc.ptoVta, _Fc.codAfipComprobante, strCUIT, _EsTest, strPasswordSecureString, out _lstErrores);
                                                //Vemos si hubo error.
                                                if (_lstErrores.Count() == 0)
                                                {
                                                    long _ultimoICG = 0;
                                                    if (!RestService.Validaciones.ValidarUltimoNroComprobante(_ultimo.PtoVta.ToString(), _ultimo.CbteNro, _ultimo.CbteTipo.ToString().PadLeft(3, '0'), strConnection, out _ultimoICG))
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "Existe una diferencia entre AFIP e ICG." +
                                                            Environment.NewLine + "Ultimo comprobante en AFIP: " + _ultimo.CbteNro.ToString() +
                                                            Environment.NewLine + "Ultimo comprobante en ICG: " + _ultimoICG.ToString() +
                                                            Environment.NewLine + "Por favor Verifique que el comprobante no este anulado",
                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "****Comprobante Faltante en ICG.***");
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Punto de Venta: " + _ultimo.PtoVta.ToString()+ 
                                                            "    Tipo comprobante: " + _ultimo.CbteTipo.ToString() + "    Número Fiscal: " + _ultimo.CbteNro.ToString());
                                                    }
                                                    //incremento el nro de comprobante.
                                                    intNroComprobante = _ultimo.CbteNro + 1;
                                                    //Armo el string de la cabecera.
                                                    AfipDll.wsAfipCae.FECAECabRequest _feCab = new AfipDll.wsAfipCae.FECAECabRequest();
                                                    _feCab.CantReg = 1;
                                                    _feCab.CbteTipo = _Fc.codAfipComprobante;
                                                    _feCab.PtoVta = _Fc.ptoVta;
                                                    //Cargamos la clase Detalle.
                                                    List<AfipDll.wsAfipCae.FECAEDetRequest> _lstDetalle = new List<AfipDll.wsAfipCae.FECAEDetRequest>();
                                                    AfipDll.wsAfipCae.FECAEDetRequest _feDetalle = new AfipDll.wsAfipCae.FECAEDetRequest();
                                                    _feDetalle.CbteDesde = intNroComprobante;
                                                    _feDetalle.CbteHasta = intNroComprobante;
                                                    _feDetalle.CbteFch = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    _feDetalle.Concepto = 3;
                                                    _feDetalle.DocTipo = _Fc.clienteTipoDocumento;
                                                    _feDetalle.DocNro = Convert.ToInt64(_Fc.clienteDocumento.Replace("-", ""));
                                                    _feDetalle.FchServDesde = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    _feDetalle.FchServHasta = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    _feDetalle.FchVtoPago = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    _feDetalle.MonId = "PES";
                                                    _feDetalle.MonCotiz = 1;
                                                    _feDetalle.ImpTotal = Math.Abs(_Fc.totalneto);
                                                    if (_Fc.cargo == 0)
                                                        _feDetalle.ImpNeto = Math.Abs(_Fc.totalbruto);
                                                    else
                                                    {
                                                        double _cargo = (_Fc.totalbruto * Math.Abs(_Fc.cargo)) / 100;
                                                        if (_Fc.cargo > 0)
                                                            _feDetalle.ImpNeto = Math.Round(_Fc.totalbruto + _cargo, 2);
                                                        else
                                                            _feDetalle.ImpNeto = Math.Round(_Fc.totalbruto - _cargo, 2);
                                                    }
                                                    _feDetalle.ImpIVA = Math.Abs((double)_Fc.totIVA);
                                                    _feDetalle.ImpTrib = Math.Abs((double)_Fc.totTributos);
                                                    if (!_IsMonotributista)
                                                        _feDetalle.Iva = RestService.ArmarIVAAfip(_Iva).ToArray();
                                                    if (_Fc.totTributos > 0)
                                                        _feDetalle.Tributos = RestService.ArmaTributos(_Tributos).ToArray();
                                                    ///                                                    
                                                    //Recuperamos los Comprobantes Asociados.
                                                    List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RestService.GetCbteAsocs(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strCUIT, _connection);
                                                    //Verifico que tengo por lo menos 1 comprobante asociado.
                                                    if (_Fc.codAfipComprobante == 2 || _Fc.codAfipComprobante == 3
                                                        || _Fc.codAfipComprobante == 7 || _Fc.codAfipComprobante == 8
                                                        || _Fc.codAfipComprobante == 12 || _Fc.codAfipComprobante == 13)
                                                    {
                                                        if (_ComprobantesAsociados.Count() == 0)
                                                        {
                                                            frmAddComprobanteAsociado _frm = new frmAddComprobanteAsociado(_feCab.CbteTipo, _siTef.sitefCuit, false);
                                                            _frm.ShowDialog();
                                                            _ComprobantesAsociados = _frm._lstNewComun;
                                                            _frm.Dispose();
                                                            if (_ComprobantesAsociados.Count == 0)
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "Sin comprobante asociado el ticket no se puede fiscalizar." +
                                                                    Environment.NewLine + "Por favor solucionelo y fiscalicelo por la consola.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                                                return;
                                                            }
                                                        }
                                                        _feDetalle.CbtesAsoc = _ComprobantesAsociados.ToArray();
                                                    }
                                                    ////
                                                    _lstDetalle.Add(_feDetalle);
                                                    //Armo el request
                                                    AfipDll.wsAfipCae.FECAERequest _requestCAE = new AfipDll.wsAfipCae.FECAERequest();
                                                    _requestCAE.FeCabReq = _feCab;
                                                    _requestCAE.FeDetReq = _lstDetalle.ToArray();
                                                    try
                                                    {
                                                        AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                                                        //validamos que tenemos el TA.xml
                                                        if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCertificado, _pathTa, _pathLog, _wsaa, strCUIT))
                                                            _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCertificado, _pathTa, _pathLog, _EsTest, strCUIT, strPasswordSecureString);
                                                        if (_wsaa.Token != null)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "****Ticket Serie: " + _serie + ", Numero:" + _numero +", N: " + _n);
                                                            wsAfip.clsCaeResponse _rta = wsAfip.ObtenerDatosCAENew(_requestCAE, _pathCertificado, _pathTa, _pathLog, strCUIT, _EsTest, strPasswordSecureString, _wsaa);
                                                            //
                                                            string strCodBarra = "";
                                                            switch (_rta.Resultado)
                                                            {
                                                                case "A":
                                                                case "P":
                                                                    {
                                                                        //Genero el codigo de barra.
                                                                        DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _feCab.CbteTipo.ToString(), _feCab.PtoVta.ToString(), _rta.Cae, _rta.FechaVto, out strCodBarra);
                                                                        //Genero el los datos del QR.
                                                                        string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                                            _requestCAE.FeCabReq.PtoVta, _requestCAE.FeCabReq.CbteTipo, Convert.ToInt32(_requestCAE.FeDetReq[0].CbteDesde),
                                                                            Convert.ToDecimal(_requestCAE.FeDetReq[0].ImpTotal), _requestCAE.FeDetReq[0].MonId, Convert.ToDecimal(_requestCAE.FeDetReq[0].MonCotiz),
                                                                            _requestCAE.FeDetReq[0].DocTipo, _requestCAE.FeDetReq[0].DocNro, "E", Convert.ToInt64(_rta.Cae));
                                                                        //Grabo el nro. fiscal y los demas datos.
                                                                        RestService.GrabarNumeroTiquetFecha(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero),
                                                                            _n, _PtoVtaCAE, _Fc.serieFiscal, intNroComprobante, DateTime.Now, _rta.Cae,
                                                                            strCodBarra, _rta.ErrorMsj, "Facturado", _rta.FechaVto, qrEncode, _connection);
                                                                        //Grabo la tabla TransaccionAFIP
                                                                        CommonService.InfoTransaccionAFIP _dtoAfip = new CommonService.InfoTransaccionAFIP();
                                                                        _dtoAfip.cae = _rta.Cae;
                                                                        _dtoAfip.codigobara = strCodBarra;
                                                                        _dtoAfip.codigoqr = qrEncode;
                                                                        _dtoAfip.fechavto = _rta.FechaVto;
                                                                        _dtoAfip.n = _n;
                                                                        _dtoAfip.nrofiscal = intNroComprobante;
                                                                        _dtoAfip.numero = Convert.ToInt32(_numero);
                                                                        _dtoAfip.serie = _serie;
                                                                        _dtoAfip.tipocae = "CAE";
                                                                        try
                                                                        {
                                                                            CommonService.TransaccionAFIP.InsertTransaccionAFIP(_dtoAfip, _connection);
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al grabar info en la tabla TransaccionAfip. Error: " + ex.Message);
                                                                        }
                                                                        //SiTef
                                                                        if (!String.IsNullOrEmpty(_siTef.pathSendInvoice))
                                                                        {
                                                                            //IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                            //    _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                            IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                                _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                        }
                                                                        //Irsa
                                                                        if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                        {
                                                                            Irsa.Rest.LanzarTrancomp(_irsa, _feCab.CbteTipo.ToString(), intNroComprobante.ToString(), _feCab.PtoVta.ToString(), _connection);

                                                                        }
                                                                        break;
                                                                    }
                                                                case "R":
                                                                    {
                                                                        //Aca se debe armar el CAEA.
                                                                        MessageBox.Show(new Form { TopMost = true }, "El ticket fue rechazado por la AFIP." + Environment.NewLine +
                                                                            "Error Codigo: " + _rta.ErrorCode + Environment.NewLine +
                                                                            "Error Mensaje: " + _rta.ErrorMsj + Environment.NewLine +
                                                                            "Por favor solucionelo y fiscalicelo por la consola.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                        break;
                                                                    }
                                                                case "EX":
                                                                    {
                                                                        //Exception
                                                                        throw new Exception(_rta.ErrorMsj);
                                                                        break;
                                                                    }
                                                            }

                                                            //Inserto en Rem_transacciones.
                                                            DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, _connection);
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show(new Form { TopMost = true }, "No se pudo obtener una Tiquet de Acceso. Consulte el log.",
                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        if (intNroComprobante == 0)
                                                        {
                                                            if (String.IsNullOrEmpty(_PtoVtaCAEA))
                                                            {
                                                                //Si no tengo CAEA pido el nro manual.
                                                                //Recupero el contador para el Pto de Venta Manual
                                                                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaManual, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                                //Grabo la transaccion Manual
                                                                RestService.TransactionManual(_Fc, _terminal, _dtoContador.contador, _PtoVtaManual, strCUIT, _pathLog, _connection);
                                                            }
                                                            else
                                                            {
                                                                if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                                                    "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                {
                                                                    //Recupero el contador para el CAEA
                                                                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                                    //Obtengo el CAEA para el periodo en curso.
                                                                    CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                                    if (_caeaEnCurso.Caea != null)
                                                                    {
                                                                        if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                                        {
                                                                            MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                            "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Information);
                                                                        }
                                                                        else
                                                                        {
                                                                            //Genero la fecha de vto.
                                                                            string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                                                            //Genero el los datos del QR.
                                                                            string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                                                _requestCAE.FeCabReq.PtoVta, _requestCAE.FeCabReq.CbteTipo, Convert.ToInt32(_requestCAE.FeDetReq[0].CbteDesde),
                                                                                Convert.ToDecimal(_requestCAE.FeDetReq[0].ImpTotal), _requestCAE.FeDetReq[0].MonId, Convert.ToDecimal(_requestCAE.FeDetReq[0].MonCotiz),
                                                                                _requestCAE.FeDetReq[0].DocTipo, Convert.ToInt64(_requestCAE.FeDetReq[0].DocNro), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                            //Genero el codigo de barras.
                                                                            string _codBarra;
                                                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                            //Grabo la transaccion de CAEA. 
                                                                            RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra,
                                                                                _PtoVtaCAEA, strCUIT, _pathLog, qrEncode, _connection);
                                                                            //SiTef
                                                                            if (!String.IsNullOrEmpty(_siTef.pathSendInvoice))
                                                                            {
                                                                                //IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _siTef.sitefIdTienda, _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                                IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                                _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                            }
                                                                            //Irsa
                                                                            if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                            {
                                                                                Irsa.Rest.LanzarTrancomp(_irsa, _feCab.CbteTipo.ToString(), intNroComprobante.ToString(), _feCab.PtoVta.ToString(), _connection);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                                            "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Information);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    string _msj = "";
                                                    foreach (wsAfip.Errors er in _lstErrores)
                                                    {
                                                        AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Error: " + er.Code + ": " + er.Msg);
                                                        _msj = _msj + Environment.NewLine + "Error: " + er.Code + ": " + er.Msg;
                                                    }

                                                    if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + _msj + Environment.NewLine +
                                                        "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                    {
                                                        //Recupero el contador para el CAEA
                                                        DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                        //Obtengo el CAEA para el periodo en curso.
                                                        CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                        if (_caeaEnCurso.Caea != null)
                                                        {
                                                            if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information);
                                                            }
                                                            else
                                                            {
                                                                //Genero la fecha de vto.
                                                                string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                                                //Genero el codigo de barras.
                                                                string _codBarra;
                                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                //Genero el los datos del QR.
                                                                string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                                    Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                                    "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                //Grabo la transaccion de CAEA. 
                                                                RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                                    strCUIT, _pathLog, qrEncode, _connection);
                                                                //SiTef
                                                                if (!String.IsNullOrEmpty(_siTef.pathSendInvoice))
                                                                {
                                                                    //IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _siTef.sitefIdTienda, _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                    IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                                _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                                }
                                                                //Irsa
                                                                if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                {
                                                                    Irsa.Rest.LanzarTrancomp(_irsa, _Fc.codAfipComprobante.ToString(), intNroComprobante.ToString(), _PtoVtaCAEA, _connection);
                                                                }
                                                            }                                                                                                                        
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                                "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information);
                                                    }
                                                }
                                            }
                                            else
                                            {                                                
                                                //Obtengo el CAEA para el periodo en curso.
                                                CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                if (_caeaEnCurso.Caea != null)
                                                {
                                                    string _msj = "";
                                                    if (_soloCaea)
                                                    {
                                                        if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                        {
                                                            MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                            "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                            MessageBoxIcon.Information);
                                                        }
                                                        else
                                                        {
                                                            //Recupero el contador para el CAEA
                                                            DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                            if (_dtoContador.contador % _cantidadTiquets == 0)
                                                            {
                                                                _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                            }
                                                            //Genero la fecha de vto.
                                                            string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                                                            //Genero el codigo de barras.
                                                            string _codBarra;
                                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                            //Genero el los datos del QR.
                                                            string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                                Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                                "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                            //Grabo la transaccion de CAEA. 
                                                            RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                                strCUIT, _pathLog, qrEncode, _connection);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _msj = "No esta disponible el servicio de la AFIP para Fiscalizar." + Environment.NewLine +
                                                                "Desea ingresar el comprobante con CAEA?.";
                                                        if (MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                        {
                                                            if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information);
                                                            }
                                                            else
                                                            {
                                                                //Genero la fecha de vto.
                                                                string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                                                                //Genero el codigo de barras.
                                                                string _codBarra;
                                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                ////Genero el los datos del QR.
                                                                //string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                                //    Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                                //    "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                ////Grabo la transaccion de CAEA. 
                                                                //RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                                //    strCUIT, _pathLog, qrEncode, _connection);
                                                            }                                                            
                                                        }
                                                    }
                                                    //SiTef
                                                    if (!String.IsNullOrEmpty(_siTef.pathSendInvoice))
                                                    {
                                                        //IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _siTef.sitefIdTienda, _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                        IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                                _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                    }
                                                    //Irsa
                                                    if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                    {
                                                        //Irsa.Rest.LanzarTrancomp(_irsa, _Fc.codAfipComprobante.ToString(), _dtoContador.contador.ToString(), _PtoVtaCAEA, _connection);
                                                    }
                                                }
                                                else
                                                    MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                        "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Information);
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "Existen comprobantes con fecha anterior sin fiscalizar." + Environment.NewLine +
                                                    "Debe fiscalizar estos comprobantes antes, para un correcto procesamiento en AFIP." + Environment.NewLine +
                                                    "Ingrese a la consola y fiscalice todos comprobantes con fecha anterior.", "ICG Argentina", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);
                                        }
                                    }
                                    //Solo si el ticket fue impreso se envían mensajes
                                    _tiquet = IcgFceDll.RestService.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    if (_tiquet.NumeroFiscal > 0)
                                    {
                                        if (_tiquet.NumeroFiscal % _cantidadTiquets == 0)
                                        {
                                            if (RestService.Validaciones.ValidarComprobantesSinInformarCAEA(_caja, _connection))
                                            {
                                                MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes con CAEA sin informar a la AFIP." + Environment.NewLine +
                                                "Por favor informelos desde la consola.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information);
                                            }
                                            if (RestService.Validaciones.ValidarComprobantesSinFiscalizar(_caja, _connection))
                                            {
                                                MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar en la AFIP." + Environment.NewLine +
                                                "Por favor fiscalicelos desde la consola.", "ICG Argentina", MessageBoxButtons.OK,
                                                                MessageBoxIcon.Information);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Error: " + ex.Message);
                                    if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                                    {
                                        if (!ex.Message.StartsWith("ICG-VAL."))
                                        {
                                            if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                                "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                            {
                                                //Factura Electronica comun.
                                                Modelos.DatosFactura _Fc = RestService.GetDatosFactura(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codVendedor), _connection);
                                                //Recupero el contador para el CAEA
                                                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                //Obtengo el CAEA para el periodo en curso.
                                                CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                if (_caeaEnCurso.Caea != null)
                                                {
                                                    if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                        "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Information);
                                                    }
                                                    else
                                                    {
                                                        //Genero la fecha de vto.
                                                        string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                                        //Genero el codigo de barras.
                                                        string _codBarra;
                                                        DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                        //Genero el los datos del QR.
                                                        string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(strCUIT),
                                                            Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                            "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                        //Grabo la transaccion de CAEA. 
                                                        RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                            strCUIT, _pathLog, qrEncode, _connection);
                                                        //SiTef
                                                        if (!String.IsNullOrEmpty(_siTef.pathSendInvoice))
                                                        {
                                                            //IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _siTef.sitefIdTienda, _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                            IcgFceDll.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _siTef.sitefIdTienda,
                                                                                _siTef.sitefIdTerminal, _siTef.sitefCuit, _siTef.sitefCuitIsv, _siTef.pathSendInvoice, _siTef.usaClover, _connection);
                                                        }
                                                        //Irsa
                                                        if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                        {
                                                            Irsa.Rest.LanzarTrancomp(_irsa, _Fc.codAfipComprobante.ToString(), _dtoContador.contador.ToString(), _PtoVtaCAEA, _connection);
                                                        }
                                                    }
                                                }
                                                else
                                                    MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                        "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Information);
                                            }
                                        }
                                        else
                                            MessageBox.Show(new Form { TopMost = true }, "El comprobante no se fiscalizo." + Environment.NewLine +
                                            "Se produjo el siguiente error: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK,
                                                            MessageBoxIcon.Error);
                                    }
                                    else
                                        MessageBox.Show(new Form { TopMost = true }, "El comprobante no se fiscalizo." + Environment.NewLine +
                                            "Se produjo el siguiente error: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK,
                                                            MessageBoxIcon.Error);
                                }
                            }
                        }
                        else
                        {
                            //Ponemos el numerofiscal en -1 y la seiefiscal en 00000
                            //Invocamos la transaccion Black
                            RestService.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo fce.xml. Por favor comuniquese con ICG Argentina.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Error: " + ex.Message);
            }
        }

    }
}
