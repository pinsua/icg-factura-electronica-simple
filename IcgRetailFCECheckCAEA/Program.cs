﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AfipDll;

namespace IcgRetailFCECheckCAEA
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //REcupero los datos de la configuracion
            string _conexion = Properties.Settings.Default.Conection;
            string _cuit = Properties.Settings.Default.Cuit;
            string _certificado = Properties.Settings.Default.Certificado;
            string _password = Properties.Settings.Default.Password;
            bool _IsTest = Convert.ToBoolean(Properties.Settings.Default.IsTest);
            //Variables
            string _pathApp;
            string _pathLog;
            string _pathCertificado;
            string _pathTaFC;
            string _pathTaFCE;

            //VAlido las carpetas.
            //recupero la ubicacion del ejecutable.
            _pathApp = Application.StartupPath.ToString();
            //Cargo la ubicaciones.
            _pathCertificado = _pathApp + "\\Certificado";
            _pathLog = _pathApp + "\\Log";
            _pathTaFC = _pathApp + "\\TAFC";
            _pathTaFCE = _pathApp + "\\TAFCE";

            try
            {
                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCertificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCertificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCertificado + "). Por favor coloque el certificado en este directorio para poder continuar.");

                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(_certificado))
                    {
                        if (!File.Exists(_pathCertificado + "\\" + _certificado))
                        {
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                            return;
                        }
                        else
                            _pathCertificado = _pathCertificado + "\\" + _certificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);

                //Valido si existe el CAEA para el periodo en curso.
                //1- recupero el CAEA.
                int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                int _quincena;

                if (DateTime.Now.Day > 15)
                    _quincena = 2;
                else
                    _quincena = 1;

                using (SqlConnection _connection = new SqlConnection())
                {
                    _connection.ConnectionString = _conexion;
                    _connection.Open();
                    //2- recupero el CAEA de la base de datos
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);

                    if (_caea.Caea == null)
                    {
                        //No lo tengo lo solicito
                        _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _IsTest);

                        if(_caea.Caea == null)
                        {
                            //No lo tengo lo consulto.
                            _caea = CAEA.ConsultarCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _IsTest);
                        }

                        if (_caea.Caea != null)
                            CAEA.InsertCAEA(_caea, _connection);
                        else
                            throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
                    }                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
