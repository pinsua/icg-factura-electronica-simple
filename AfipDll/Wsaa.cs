﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace AfipDll
{
    public class Wsaa
    {
        public string Token { get; set; }
        public string Sign { get; set; }
        public DateTime ExpirationTime { get; set; }

        /// <summary>
        /// URL servicio Wasa de Test.
        /// </summary>
        const string TEST_URLWSAAWSDL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
        /// <summary>
        /// URL serviciop Waa de Produccion.
        /// </summary>
        const string DEFAULT_URLWSAAWSDL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";
        const bool DEFAULT_VERBOSE = true;

        /// <summary> 
        /// Clase para crear objetos Login Tickets 
        /// </summary> 
        /// <remarks> 
        /// Ver documentacion: 
        /// Especificacion Tecnica del Webservice de Autenticacion y Autorizacion 
        /// Version 1.0 
        /// Departamento de Seguridad Informatica - AFIP 
        /// </remarks> 
        public class LoginTicket
        {
            // Entero de 32 bits sin signo que identifica el requerimiento 
            public UInt32 UniqueId;
            // Momento en que fue generado el requerimiento 
            public DateTime GenerationTime;
            // Momento en el que exoira la solicitud 
            public DateTime ExpirationTime;
            // Identificacion del WSN para el cual se solicita el TA 
            public string Service;
            // Firma de seguridad recibida en la respuesta 
            public string Sign;
            // Token de seguridad recibido en la respuesta 
            public string Token;

            public XmlDocument XmlLoginTicketRequest = null;
            public XmlDocument XmlLoginTicketResponse = null;
            public string RutaDelCertificadoFirmante;
            public string XmlStrLoginTicketRequestTemplate = "<loginTicketRequest><header><uniqueId></uniqueId><generationTime></generationTime><expirationTime></expirationTime></header><service></service></loginTicketRequest>";

            private bool _verboseMode = true;

            // OJO! NO ES THREAD-SAFE 
            private static UInt32 _globalUniqueID = 0;

            /// <summary> 
            /// Construye un Login Ticket obtenido del WSAA 
            /// </summary> 
            /// <param name="argServicio">Servicio al que se desea acceder</param> 
            /// <param name="argUrlWsaa">URL del WSAA</param> 
            /// <param name="argRutaCertX509Firmante">Ruta del certificado X509 (con clave privada) usado para firmar</param> 
            /// <param name="argVerbose">Nivel detallado de descripcion? true/false</param> 
            /// <remarks></remarks> 
            public string ObtenerLoginTicketResponse(string argServicio, string argUrlWsaa, string argRutaCertX509Firmante, bool argVerbose, string pPathLog)
            {

                this.RutaDelCertificadoFirmante = argRutaCertX509Firmante;
                this._verboseMode = argVerbose;
                CertificadosX509Lib.VerboseMode = argVerbose;

                string cmsFirmadoBase64;
                string loginTicketResponse;

                XmlNode xmlNodoUniqueId;
                XmlNode xmlNodoGenerationTime;
                XmlNode xmlNodoExpirationTime;
                XmlNode xmlNodoService;

                // PASO 1: Genero el Login Ticket Request 
                try
                {
                    XmlLoginTicketRequest = new XmlDocument();
                    XmlLoginTicketRequest.LoadXml(XmlStrLoginTicketRequestTemplate);

                    xmlNodoUniqueId = XmlLoginTicketRequest.SelectSingleNode("//uniqueId");
                    xmlNodoGenerationTime = XmlLoginTicketRequest.SelectSingleNode("//generationTime");
                    xmlNodoExpirationTime = XmlLoginTicketRequest.SelectSingleNode("//expirationTime");
                    xmlNodoService = XmlLoginTicketRequest.SelectSingleNode("//service");

                    xmlNodoGenerationTime.InnerText = DateTime.Now.AddMinutes(-10).ToString("s");
                    xmlNodoExpirationTime.InnerText = DateTime.Now.AddMinutes(+10).ToString("s");
                    xmlNodoUniqueId.InnerText = Convert.ToString(_globalUniqueID);
                    xmlNodoService.InnerText = argServicio;
                    this.Service = argServicio;

                    _globalUniqueID += 1;

                    if (this._verboseMode)
                    {
                        Console.WriteLine(XmlLoginTicketRequest.OuterXml);
                    }
                }

                catch (Exception excepcionAlGenerarLoginTicketRequest)
                {
                    throw new Exception("***Error GENERANDO el LoginTicketRequest : " + excepcionAlGenerarLoginTicketRequest.Message);
                }

                // PASO 2: Firmo el Login Ticket Request 
                try
                {
                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Leyendo certificado: " + RutaDelCertificadoFirmante);
                    }

                    X509Certificate2 certFirmante = CertificadosX509Lib.ObtieneCertificadoDesdeArchivo(RutaDelCertificadoFirmante);

                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Firmando: ");
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), XmlLoginTicketRequest.OuterXml);
                    }

                    // Convierto el login ticket request a bytes, para firmar 
                    Encoding EncodedMsg = Encoding.UTF8;
                    byte[] msgBytes = EncodedMsg.GetBytes(XmlLoginTicketRequest.OuterXml);

                    // Firmo el msg y paso a Base64 
                    byte[] encodedSignedCms = CertificadosX509Lib.FirmaBytesMensaje(msgBytes, certFirmante, pPathLog);
                    cmsFirmadoBase64 = Convert.ToBase64String(encodedSignedCms);
                }

                catch (Exception excepcionAlFirmar)
                {
                    throw new Exception("***Error FIRMANDO el LoginTicketRequest : " + excepcionAlFirmar.Message);
                }

                // PASO 3: Invoco al WSAA para obtener el Login Ticket Response 
                try
                {
                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Llamando al WSAA en URL: " + argUrlWsaa.ToString());
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Argumento en el request:");
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), cmsFirmadoBase64);
                    }

                    //Invocamos al metodo de la Afi
                    wsWasa.LoginCMSClient _cl = new wsWasa.LoginCMSClient();
                    _cl.Endpoint.Address =  new System.ServiceModel.EndpointAddress(argUrlWsaa);
                    //wsWasa.LoginCMSService servicioWsaa = new wsWasa.LoginCMSService();
                    //servicioWsaa.Url = argUrlWsaa;
                    //loginTicketResponse = servicioWsaa.loginCms(cmsFirmadoBase64);
                    loginTicketResponse = _cl.loginCms(cmsFirmadoBase64);

                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***LoguinTicketResponse: ");
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), loginTicketResponse);
                    }
                }

                catch (Exception excepcionAlInvocarWsaa)
                {
                    throw new Exception("***Error INVOCANDO al servicio WSAA : " + excepcionAlInvocarWsaa.Message);
                }


                // PASO 4: Analizo el Login Ticket Response recibido del WSAA 
                try
                {
                    XmlLoginTicketResponse = new XmlDocument();
                    XmlLoginTicketResponse.LoadXml(loginTicketResponse);

                    this.UniqueId = UInt32.Parse(XmlLoginTicketResponse.SelectSingleNode("//uniqueId").InnerText);
                    this.GenerationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText);
                    this.ExpirationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText);
                    this.Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText;
                    this.Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText;
                }
                catch (Exception excepcionAlAnalizarLoginTicketResponse)
                {
                    throw new Exception("***Error ANALIZANDO el LoginTicketResponse : " + excepcionAlAnalizarLoginTicketResponse.Message);
                }

                return loginTicketResponse;
            }

            /// <summary> 
            /// Libreria de utilidades para manejo de certificados 
            /// </summary> 
            /// <remarks></remarks> 
            class CertificadosX509Lib
            {

                public static bool VerboseMode = false;

                /// <summary> 
                /// Firma mensaje 
                /// </summary> 
                /// <param name="argBytesMsg">Bytes del mensaje</param> 
                /// <param name="argCertFirmante">Certificado usado para firmar</param> 
                /// <returns>Bytes del mensaje firmado</returns> 
                /// <remarks></remarks> 
                public static byte[] FirmaBytesMensaje(byte[] argBytesMsg, X509Certificate2 argCertFirmante, string pPathLog)
                {
                    try
                    {
                        // Pongo el mensaje en un objeto ContentInfo (requerido para construir el obj SignedCms) 
                        ContentInfo infoContenido = new ContentInfo(argBytesMsg);
                        SignedCms cmsFirmado = new SignedCms(infoContenido);

                        // Creo objeto CmsSigner que tiene las caracteristicas del firmante 
                        CmsSigner cmsFirmante = new CmsSigner(argCertFirmante);
                        cmsFirmante.IncludeOption = X509IncludeOption.EndCertOnly;

                        if (VerboseMode)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Firmando bytes del mensaje...");
                        }
                        // Firmo el mensaje PKCS #7 
                        cmsFirmado.ComputeSignature(cmsFirmante);

                        if (VerboseMode)
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***OK mensaje firmado");
                        }

                        // Encodeo el mensaje PKCS #7. 
                        return cmsFirmado.Encode();
                    }
                    catch (Exception excepcionAlFirmar)
                    {
                        throw new Exception("***Error al firmar: " + excepcionAlFirmar.Message);
                    }
                }

                /// <summary> 
                /// Lee certificado de disco 
                /// </summary> 
                /// <param name="argArchivo">Ruta del certificado a leer.</param> 
                /// <returns>Un objeto certificado X509</returns> 
                /// <remarks></remarks> 
                public static X509Certificate2 ObtieneCertificadoDesdeArchivo(string argArchivo)
                {
                    X509Certificate2 objCert = new X509Certificate2();

                    try
                    {
                        objCert.Import(Microsoft.VisualBasic.FileIO.FileSystem.ReadAllBytes(argArchivo));
                        return objCert;
                    }
                    catch (Exception excepcionAlImportarCertificado)
                    {
                        throw new Exception("argArchivo=" + argArchivo + " excepcion=" + excepcionAlImportarCertificado.Message + " " + excepcionAlImportarCertificado.StackTrace);

                    }
                }

            }
        }

        public static Wsaa ObtenerDatosWsaa(string pServicio, string pPath, string pPathTa, string pPathLog, bool IsTest, string _Cuit)
        {
            string strUrlWassaTest = TEST_URLWSAAWSDL;
            string strUrlWsaaWsdl = DEFAULT_URLWSAAWSDL;
            string strIdServicioNegocio = pServicio;
            string strRutaCertSigner = pPath;
            bool blnVerboseMode = DEFAULT_VERBOSE;
            string strTApath = pPathTa + @"\" + _Cuit + ".xml";

            Wsaa wa = new Wsaa();

            // Argumentos OK, entonces procesar normalmente... 
            Wsaa.LoginTicket objTicketRespuesta;
            string strTicketRespuesta;

            try
            {

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Servicio a acceder: " + strIdServicioNegocio);
                    if (IsTest)
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWassaTest);
                    else
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWsaaWsdl);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Ruta del certificado: " + strRutaCertSigner);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Modo verbose: " + blnVerboseMode.ToString());
                }

                objTicketRespuesta = new Wsaa.LoginTicket();

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Accediendo a " + strUrlWsaaWsdl);
                }

                if (IsTest)
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWassaTest, strRutaCertSigner, blnVerboseMode, pPathLog);
                else
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWsaaWsdl, strRutaCertSigner, blnVerboseMode, pPathLog);

                if (blnVerboseMode)
                {
                    //Ponemos los datos en el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***CONTENIDO DEL TICKET RESPUESTA:");
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Token: " + objTicketRespuesta.Token);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Sign: " + objTicketRespuesta.Sign);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " GenerationTime: " + Convert.ToString(objTicketRespuesta.GenerationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " ExpirationTime: " + Convert.ToString(objTicketRespuesta.ExpirationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Service: " + objTicketRespuesta.Service);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " UniqueID: " + Convert.ToString(objTicketRespuesta.UniqueId));
                    //Pasamos los datos a la clase.
                    wa.Token = objTicketRespuesta.Token;
                    wa.Sign = objTicketRespuesta.Sign;
                    wa.ExpirationTime = objTicketRespuesta.ExpirationTime;
                    objTicketRespuesta.XmlLoginTicketResponse.Save(strTApath);
                }
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***EXCEPCION AL OBTENER TICKET:" + excepcionAlObtenerTicket.Message);
            }

            return wa;
        }
    }
}
