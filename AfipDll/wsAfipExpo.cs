﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Text;

namespace AfipDll
{
    public class wsAfipExpo
    {
        #region constantes
        /// <summary>
        /// URL Servicio Wasa TEST.
        /// </summary>
        const string TEST_URLWSAAWSDL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
        /// <summary>
        /// URL Servicio WsFEXV1 TEST.
        /// </summary>
        const string TEST_URLWSFEXV1 = "https://wswhomo.afip.gov.ar/wsfexv1/service.asmx";
        /// <summary>
        /// URL servicio WSAA PROD.
        /// </summary>
        const string PROD_URLWSAAWSDL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";
        /// <summary>
        /// URL servicio WSFEXV! PROD.
        /// </summary>
        const string PROD_URLWSFEXV1 = "https://servicios1.afip.gov.ar/wsfexv1/service.asmx";
        #endregion

        public class Cabecera
        {
            public int Tipo { get; set; }
            public string ptovta { get; set; }
            public string cuitemisor { get; set; }
            public string fechaemision { get; set; }
            public int idioma { get; set; }
            public int concepto { get; set; }
            public string ape { get; set; }
            public string moneda { get; set; }
            public string tipocambio { get; set; }
            public string destinocmp { get; set; }
            public string tipodocrecptor { get; set; }
            public string nrodocreceptor { get; set; }
            public string idimpositivoreceptor { get; set; }
            public string receptor { get; set; }
            public string domicilioreceptor { get; set; }
            public string importetotal { get; set; }
            public string formaspago { get; set; }
            public string incoterms { get; set; }
            public string detalles { get; set; }
            public string otrosdatosgenerales { get; set; }
            public string comprobante { get; set; }
            public int iD { get; set; }
            public string fechavto { get; set; }

            public static Cabecera ToDataTable(string _Path, out string _ErrorMessage)
            {
                string _Error = "";
                Cabecera cls = new Cabecera();
                System.Data.DataSet dtt = new System.Data.DataSet();
                try
                {
                    dtt.ReadXml(_Path);
                    foreach (System.Data.DataRow dr in dtt.Tables[0].Rows)
                    {
                        cls.ape = dr["ape"].ToString();
                        cls.concepto = Convert.ToInt32(dr["concepto"].ToString());
                        cls.cuitemisor = dr["cuitemisor"].ToString();
                        cls.destinocmp = dr["destinocmp"].ToString();
                        //cls.detalles = dr["detalles"].ToString();
                        cls.domicilioreceptor = dr["domicilioreceptor"].ToString();
                        cls.fechaemision = dr["fechaemision"].ToString();
                        cls.formaspago = dr["formapago"].ToString();
                        cls.idimpositivoreceptor = dr["idimpositivoreceptor"].ToString();
                        cls.idioma = Convert.ToInt32(dr["idioma"].ToString());
                        cls.importetotal = dr["importetotal"].ToString();
                        cls.incoterms = dr["incoterms"].ToString();
                        cls.moneda = dr["moneda"].ToString();
                        cls.nrodocreceptor = dr["nrodocreceptor"].ToString();
                        cls.otrosdatosgenerales = dr["otrosdatosgenerales"].ToString();
                        cls.ptovta = dr["ptovta"].ToString();
                        cls.receptor = dr["receptor"].ToString();
                        cls.Tipo = Convert.ToInt32(dr["Tipo"].ToString());
                        cls.tipocambio = dr["tipocambio"].ToString();
                        cls.comprobante = dr["comprobante"].ToString();
                        //cls.tipodocrecptor = dr["tipodocrecptor"].ToString();

                    }

                }
                catch (Exception ex)
                {
                    _Error = ex.Message;
                }
                dtt.Dispose();
                _ErrorMessage = _Error;
                return cls;
            }
        }

        public class Detalle
        {
            public string cod { get; set; }
            public string descrip { get; set; }
            public string unimed { get; set; }
            public string cant { get; set; }
            public string preciounit { get; set; }
            public string importe { get; set; }
            public string dto { get; set; }

            public static List<Detalle> ToDataTable(string _Path, out string _ErrorMessage)
            {
                string _Error = "";
                List<Detalle> lstDetalle = new List<Detalle>();
                System.Data.DataSet dtt = new System.Data.DataSet();
                try
                {
                    dtt.ReadXml(_Path);
                    foreach (System.Data.DataRow dr in dtt.Tables[0].Rows)
                    {
                        Detalle cls = new Detalle();
                        cls.cant = dr["cant"].ToString();
                        cls.cod = dr["cod"].ToString();
                        cls.descrip = dr["descr"].ToString();
                        cls.importe = dr["importe"].ToString();
                        cls.preciounit = dr["preciounit"].ToString();
                        cls.unimed = dr["unimed"].ToString();

                        lstDetalle.Add(cls);
                    }
                }
                catch (Exception ex)
                {
                    _Error = ex.Message;
                }
                dtt.Dispose();
                _ErrorMessage = _Error;
                return lstDetalle;
            }
        }

        /// <summary>
        /// Metodo para obtener el ultimo nro de comprobante utlizado en la AFIP
        /// </summary>
        /// <param name="_PathCertificado">Path del certificado</param>
        /// <param name="_PahtTa">Path donde se escribe el tiquet de Acceso</param>
        /// <param name="pPathLog">Path donde se escribe el Log</param>
        /// <param name="_Cuit">Cuit</param>
        /// <param name="_IsTest">Indica si se accede al ambiente de homologacion</param>
        /// <param name="_Resultado">Parametro de salido con el resultado</param>
        /// <param name="_ErrorMessage">Mesaje de error en caso de que exista</param>
        /// <param name="_PtoVta">Punto de venta.</param>
        /// <returns></returns>
        public static int GetLastId(string _PathCertificado, string _PathTa, string pPathLog, 
            string _Cuit, bool _IsTest, SecureString _pass, out string _Resultado, out string _ErrorMessage, out string _PtoVta)
        {
            //string _Error = "";
            //string _token;
            //string _sign;
            //DateTime _expiration;
            string _DefaultService = "wsfex";
            int _rtdo = 0;
            _ErrorMessage = "";
            _PtoVta = "";
            WsaaNew _cls = new WsaaNew();

            //if (!MetodosVarios.ValidoTA(_PathCertificado, _PahtTa, pPathLog, _Cuit, out _token, out _sign, out _expiration))
            //    MetodosVarios.ObtenerDatosWsaa(_DefaultService, _IsTest, _PathCertificado, _PahtTa, pPathLog, _Cuit, out _token, out _sign);
            if (!WsaaNew.Wsaa.ValidoTANew(_PathCertificado, _PathTa, pPathLog, _cls, _Cuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(_DefaultService, _PathCertificado, _PathTa, pPathLog, _IsTest, _Cuit, _pass);

            if (!String.IsNullOrEmpty(_cls.Token) && !String.IsNullOrEmpty(_cls.Sign))
            {
                using (wsExportacion.ServiceSoapClient _ws = new  wsExportacion.ServiceSoapClient())
                {
                    if (_IsTest)
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEXV1);
                    else
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWSFEXV1);

                    wsExportacion.ClsFEXAuthRequest _auth = new wsExportacion.ClsFEXAuthRequest();
                    _auth.Sign = _cls.Sign;
                    _auth.Token = _cls.Token;
                    _auth.Cuit = Convert.ToInt64(_Cuit);

                    wsExportacion.FEXResponse_LastID _rta = _ws.FEXGetLast_ID(_auth);

                    if(_rta.FEXErr.ErrCode == 0)
                    {
                        long pp = _rta.FEXResultGet.Id;
                        string _nroFc = pp.ToString().Substring(5,8);
                        _PtoVta = pp.ToString().Substring(0, 5);
                        _rtdo = Convert.ToInt32(_nroFc);
                        _Resultado = "OK";
                    }
                    else
                    {
                        _ErrorMessage = _rta.FEXErr.ErrMsg;
                        _Resultado = "Error";
                    }
                }
            }
            else
            {
                _Resultado = "R";
                _ErrorMessage = "Error al solicitar el ticket de acceso.";
            }

            return _rtdo;
        }

        public static int GetLastCmp(string _PathCertificado, string _PathTa, string pPathLog, 
            string _Cuit, bool _IsTest, string _PtoVta, int _cbteTipo, SecureString _pass, out string _Resultado, out string _ErrorMessage)
        {
            //string _Error = "";
            //string _token;
            //string _sign;
            //DateTime _expiration;
            string _DefaultService = "wsfex";
            int _rtdo = 0;
            _ErrorMessage = "";
            WsaaNew _cls = new WsaaNew();

            //if (!MetodosVarios.ValidoTA(_PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign, out _expiration))
            //    MetodosVarios.ObtenerDatosWsaa(_DefaultService, _IsTest, _PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign);
            if (!WsaaNew.Wsaa.ValidoTANew(_PathCertificado, _PathTa, pPathLog, _cls, _Cuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(_DefaultService, _PathCertificado, _PathTa, pPathLog, _IsTest, _Cuit, _pass);

            if (!String.IsNullOrEmpty(_cls.Token) && !String.IsNullOrEmpty(_cls.Sign))
            {
                using (wsExportacion.ServiceSoapClient _ws = new wsExportacion.ServiceSoapClient())
                {
                    if (_IsTest)
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEXV1);
                    else
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWSFEXV1);

                    //wsExportacion.ClsFEXAuthRequest _auth = new wsExportacion.ClsFEXAuthRequest();
                    //_auth.Sign = _sign;
                    //_auth.Token = _token;
                    //_auth.Cuit = Convert.ToInt64(_Cuit);

                    wsExportacion.ClsFEX_LastCMP _auth1 = new wsExportacion.ClsFEX_LastCMP();
                    _auth1.Sign = _cls.Sign;
                    _auth1.Token = _cls.Token;
                    _auth1.Cuit = Convert.ToInt64(_Cuit);
                    _auth1.Pto_venta = Convert.ToInt16(_PtoVta);
                    _auth1.Cbte_Tipo = Convert.ToInt16(_cbteTipo);

                    wsExportacion.FEXResponseLast_CMP _rta = _ws.FEXGetLast_CMP(_auth1);

                    if (_rta.FEXErr.ErrCode == 0)
                    {
                        long pp = _rta.FEXResult_LastCMP.Cbte_nro;
                        _rtdo = Convert.ToInt32(pp);
                        _Resultado = "OK";
                    }
                    else
                    {
                        _ErrorMessage = _rta.FEXErr.ErrMsg;
                        _Resultado = "Error";
                    }
                }
            }
            else
            {
                _Resultado = "R";
                _ErrorMessage = "Error al solicitar el ticket de acceso.";
            }

            return _rtdo;
        }

        public static bool AutorizarExpo(string _PathCertificado, string _PathTa, string _PathLog, string _Cuit, bool _IsTest,
            Cabecera _cabecera, List<Detalle> _detalles, List<wsExportacion.Cmp_asoc> _cmpAsoc, SecureString _pass, bool _generaXml,
            out string _Resultado, out string _Cae, out string _FechaVto, out string _ErrorMessage)
        {
            bool _rta = false;
            string _Error = "";
            //string _token;
            //string _sign;
            //DateTime _expiration;
            string _DefaultService = "wsfex";
            WsaaNew _cls = new WsaaNew();

            try
            {
                if (!WsaaNew.Wsaa.ValidoTANew(_PathCertificado, _PathTa, _PathLog, _cls, _Cuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(_DefaultService, _PathCertificado, _PathTa, _PathLog, _IsTest, _Cuit, _pass);

                if (!String.IsNullOrEmpty(_cls.Token) && !String.IsNullOrEmpty(_cls.Sign))
                {
                    //Cargamos la clase de autorizacion.
                    wsExportacion.ClsFEXAuthRequest _Auth = new wsExportacion.ClsFEXAuthRequest();
                    _Auth.Sign = _cls.Sign;
                    _Auth.Token = _cls.Token;
                    _Auth.Cuit = Convert.ToInt64(_Cuit);
                    //Cargamos la clase del request de la cabecera.
                    wsExportacion.ClsFEXRequest _Comp = Cargar_Comprobante(_cabecera, _cmpAsoc, out _Error);

                    //Cargamos los detalles de los items.
                    wsExportacion.Item[] _Items = Cargar_Items(_detalles, out _Error);
                    _Comp.Items = _Items;

                    System.Xml.Serialization.XmlSerializer reqAuth = new System.Xml.Serialization.XmlSerializer(typeof(wsExportacion.ClsFEXAuthRequest));
                    System.IO.StreamWriter file = new System.IO.StreamWriter(_PathLog + @"\AuthReq.xml");
                    reqAuth.Serialize(file, _Auth);
                    file.Dispose();
                    file.Close();

                    if (_generaXml)
                    {
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsExportacion.ClsFEXRequest));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\CompReq.xml";
                        System.IO.FileStream file1 = System.IO.File.Create(path);
                        _Antes.Serialize(file1, _Comp);
                        file1.Close();
                    }

                    //Invocamos el Ws de Autorizacion.
                    using (wsExportacion.ServiceSoapClient _service = new wsExportacion.ServiceSoapClient())
                    {
                        if (_IsTest)
                            _service.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEXV1);
                        else
                            _service.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWSFEXV1);

                        wsExportacion.FEXResponseAuthorize _Res = _service.FEXAuthorize(_Auth, _Comp);

                        if (_generaXml)
                        {
                            //Escribimos el xml del Request.
                            System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsExportacion.FEXResponseAuthorize));
                            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Response.xml";
                            System.IO.FileStream file2 = System.IO.File.Create(path);
                            _Antes.Serialize(file2, _Res);
                            file2.Close();
                        }

                        if (_Res.FEXErr.ErrCode == 0)
                        {
                            switch (_Res.FEXResultAuth.Resultado.ToUpper())
                            {
                                case "A":
                                    {
                                        //Aprobado.
                                        _Cae = _Res.FEXResultAuth.Cae;
                                        _FechaVto = _Res.FEXResultAuth.Fch_venc_Cae;
                                        _Resultado = _Res.FEXResultAuth.Resultado;
                                        _ErrorMessage = "";
                                        _rta = true;
                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "CAE Aprobado: " + _Cae);
                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "CAE FechaVto: " + _FechaVto);
                                        break;
                                    }
                                case "R":
                                    {
                                        //Rechazado.
                                        _Cae = "";
                                        _FechaVto = "";
                                        _Resultado = _Res.FEXResultAuth.Resultado;
                                        _ErrorMessage = _Res.FEXErr.ErrCode.ToString(_PathLog) + " - " + _Res.FEXErr.ErrMsg;
                                        LogFile.ErrorLog(LogFile.CreatePath(_PathLog), "CAE Rechazado: " + _ErrorMessage);
                                        break;
                                    }
                                case "P":
                                    {
                                        //Parcial con Observaciones.
                                        _Cae = _Res.FEXResultAuth.Cae;
                                        _FechaVto = _Res.FEXResultAuth.Fch_venc_Cae;
                                        _Resultado = _Res.FEXResultAuth.Resultado;
                                        _ErrorMessage = _Res.FEXErr.ErrCode.ToString() + " - " + _Res.FEXErr.ErrMsg;
                                        _rta = true;
                                        break;
                                    }
                                default:
                                    {
                                        //Cualquier otro.
                                        _Cae = _Res.FEXResultAuth.Cae;
                                        _FechaVto = _Res.FEXResultAuth.Fch_venc_Cae;
                                        _Resultado = _Res.FEXResultAuth.Resultado;
                                        _ErrorMessage = _Res.FEXErr.ErrCode.ToString() + " - " + _Res.FEXErr.ErrMsg;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            _Cae = "";
                            _FechaVto = "";
                            _Resultado = "R";
                            _ErrorMessage = _Res.FEXErr.ErrCode.ToString() + " - " + _Res.FEXErr.ErrMsg;
                        }
                    }
                }
                else
                {
                    _Cae = "";
                    _FechaVto = "";
                    _Resultado = "R";
                    _ErrorMessage = "Error al solicitar el ticket de acceso.";
                }
            }
            catch (Exception ex)
            {
                _Cae = "";
                _FechaVto = "";
                _Resultado = "R";
                _ErrorMessage = ex.Message;
            }

            return _rta;
        }

        #region uno
        public static decimal Cotizacion(string _PathCertificado, string _PathTa, string pPathLog, 
            string _Cuit, bool _IsTest, string _Moneda, out string _Fecha, out string _ErrorMessage)
        {
            decimal _rta = 0;
            string _token;
            string _sign;
            DateTime _expiration;
            string _DefaultService = "wsfex";
            if (!MetodosVarios.ValidoTA(_PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign, out _expiration))
                MetodosVarios.ObtenerDatosWsaa(_DefaultService, _IsTest, _PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign);
            if (!String.IsNullOrEmpty(_token) && !String.IsNullOrEmpty(_sign))
            {
                //Cargamos la clase de autorizacion.
                wsExportacion.ClsFEXAuthRequest _Auth = new wsExportacion.ClsFEXAuthRequest();
                _Auth.Sign = _sign;
                _Auth.Token = _token;
                _Auth.Cuit = Convert.ToInt64(_Cuit);

                //Invocamos el Ws de Autorizacion.
                using (wsExportacion.ServiceSoapClient _ws = new wsExportacion.ServiceSoapClient())
                {
                    if (_IsTest)
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEXV1);
                    else
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWSFEXV1);

                    wsExportacion.FEXResponse_Ctz _res = _ws.FEXGetPARAM_Ctz(_Auth, _Moneda);
                    //_ws.FEXResponse_Ctz _Res = _ws.FEXGetPARAM_Ctz(_Auth, _Moneda);

                    if (_res.FEXErr.ErrCode == 0)
                    {
                        _rta = _res.FEXResultGet.Mon_ctz;
                        _Fecha = _res.FEXResultGet.Mon_fecha;
                        _ErrorMessage = "";
                    }
                    else
                    {
                        _rta = 0;
                        _Fecha = "";
                        _ErrorMessage = _res.FEXErr.ErrCode.ToString() + " - " + _res.FEXErr.ErrMsg;
                    }

                }
            }
            else
            {
                _rta = 0;
                _Fecha = "";
                _ErrorMessage = "Error al solicitar el ticket de acceso.";
            }

            return _rta;
        }

        public static List<PuntosDeVenta> PuntosDeVenta(string _PathCertificado, string _PathTa, string pPathLog,
            string _Cuit, bool _IsTest, out string _ErrorMessage)
        {            
            string _token;
            string _sign;
            DateTime _expiration;
            string _DefaultService = "wsfex";
            _ErrorMessage = String.Empty;
            List<PuntosDeVenta> _lst = new List<PuntosDeVenta>();

            if (!MetodosVarios.ValidoTA(_PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign, out _expiration))
                MetodosVarios.ObtenerDatosWsaa(_DefaultService, _IsTest, _PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign);
            if (!String.IsNullOrEmpty(_token) && !String.IsNullOrEmpty(_sign))
            {
                //Cargamos la clase de autorizacion.
                wsExportacion.ClsFEXAuthRequest _Auth = new wsExportacion.ClsFEXAuthRequest();
                _Auth.Sign = _sign;
                _Auth.Token = _token;
                _Auth.Cuit = Convert.ToInt64(_Cuit);

                //Invocamos el Ws de Autorizacion.
                using (wsExportacion.ServiceSoapClient _ws = new wsExportacion.ServiceSoapClient())
                {
                    if (_IsTest)
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEXV1);
                    else
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWSFEXV1);

                    wsExportacion.FEXResponse_PtoVenta _res = _ws.FEXGetPARAM_PtoVenta(_Auth);
                    
                    if(_res.FEXResultGet.Count() > 0)
                    {
                        for (int i = 0; _res.FEXResultGet.Count() > i; i++)
                        {
                            PuntosDeVenta _cls = new PuntosDeVenta();
                            _cls.Bloqueado = _res.FEXResultGet[i].Pve_Bloqueado;
                            _cls.FechaBloqueo = _res.FEXResultGet[i].Pve_FchBaja;
                            _cls.PtoVta = _res.FEXResultGet[i].Pve_Nro;

                            _lst.Add(_cls);
                        }
                    }

                    if (_res.FEXErr.ErrCode > 0)
                    {
                        _ErrorMessage = _res.FEXErr.ErrCode.ToString() + " - " + _res.FEXErr.ErrMsg;
                    }
                    if (_res.FEXEvents.EventCode > 0)
                    {
                        _ErrorMessage = _res.FEXEvents.EventCode.ToString() + " - " + _res.FEXEvents.EventMsg;
                    }

                }
            }
            else
            {
                _ErrorMessage = "Error al solicitar el ticket de acceso.";
            }

            return _lst;
        }

        #endregion

        public static void Recuperar(string _PathCertificado, string _PathTa, string pPathLog, 
            string _Cuit, string _CbteTipo, string __PtoVta, string _CbteNro, bool _isTest)
        {
            //string _rta = "";
            //string _Error = "";
            string _token;
            string _sign;
            DateTime _expiration;
            string _DefaultService = "wsfex";
            if (!MetodosVarios.ValidoTA(_PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign, out _expiration))
                MetodosVarios.ObtenerDatosWsaa(_DefaultService, _isTest, _PathCertificado, _PathTa, pPathLog, _Cuit, out _token, out _sign);
            if (!String.IsNullOrEmpty(_token) && !String.IsNullOrEmpty(_sign))
            {
                //Cargamos la clase de autorizacion.
                wsExportacion.ClsFEXAuthRequest _Auth = new wsExportacion.ClsFEXAuthRequest();
                _Auth.Sign = _sign;
                _Auth.Token = _token;
                _Auth.Cuit = Convert.ToInt64(_Cuit);
                //Cargamos los datos del comprobante.
                wsExportacion.ClsFEXGetCMP _Cmp = new wsExportacion.ClsFEXGetCMP();
                _Cmp.Cbte_tipo = Convert.ToInt16(_CbteTipo);
                _Cmp.Punto_vta = Convert.ToInt16(__PtoVta);
                _Cmp.Cbte_nro = Convert.ToInt64(_CbteNro);
                //Invocamos el WS.
                //wsExportacion.Service _ws = new wsExportacion.Service();
                //wsExportacion.FEXGetCMPResponse _resp = _ws.FEXGetCMP(_Auth, _Cmp);

            }
            else
            {
                //_Cae = "";
                //_FechaVto = "";
                //_Resultado = "R";
                //_ErrorMessage = "Error al solicitar el ticket de acceso.";
            }
        }
        ///// <summary>
        ///// Metodo que carga los datos de la cabecera.
        ///// </summary>
        ///// <param name="_Cab">Clase cabecera.</param>
        ///// <param name="_ErrorMessage">Mensaje de error de salida.</param>
        ///// <returns>Clase del WebService.</returns>
        //private static wsExportacion.ClsFEXRequest Cargar_Comprobante(Cabecera _Cab, out string _ErrorMessage)
        //{
        //    string _Error = "";
        //    //Cargamos la clase del request de la cabecera.
        //    wsExportacion.ClsFEXRequest _Comp = new wsExportacion.ClsFEXRequest();
        //    try
        //    {
        //        _Comp.Id = Convert.ToInt64(_Cab.iD);
        //        _Comp.Cbte_Tipo = Convert.ToInt16(_Cab.Tipo);//codafip
        //        _Comp.Fecha_cbte = _Cab.fechaemision.Replace("-", "");
        //        _Comp.Punto_vta = Convert.ToInt16(_Cab.ptovta);
        //        string _Cbte = _Cab.comprobante.Remove(0, 5);
        //        _Comp.Cbte_nro = Convert.ToInt64(_Cbte);
        //        _Comp.Tipo_expo = Convert.ToInt16(_Cab.concepto);
        //        _Comp.Dst_cmp = Convert.ToInt16(_Cab.destinocmp);
        //        _Comp.Cliente = _Cab.receptor;
        //        _Comp.Domicilio_cliente = _Cab.domicilioreceptor;
        //        _Comp.Id_impositivo = _Cab.idimpositivoreceptor;
        //        _Comp.Moneda_Id = _Cab.moneda;
        //        _Comp.Moneda_ctz = Convert.ToDecimal(_Cab.tipocambio, CultureInfo.InvariantCulture.NumberFormat);
        //        _Comp.Imp_total = Convert.ToDecimal(_Cab.importetotal, CultureInfo.InvariantCulture.NumberFormat);
        //        _Comp.Obs = _Cab.otrosdatosgenerales;
        //        _Comp.Forma_pago = _Cab.formaspago;
        //        _Comp.Incoterms = _Cab.incoterms;
        //        _Comp.Idioma_cbte = Convert.ToInt16(_Cab.idioma);
        //        //Vemos el Permio Existente.
        //        if (_Comp.Cbte_Tipo == 20 || _Comp.Cbte_Tipo == 21)
        //            _Comp.Permiso_existente = "";
        //        else
        //        {
        //            if (_Comp.Cbte_Tipo == 19)
        //            {
        //                if (_Comp.Tipo_expo == 2 || _Comp.Tipo_expo == 4)
        //                    _Comp.Permiso_existente = "";
        //                else
        //                    _Comp.Permiso_existente = "N";
        //            }
        //            else
        //                _Comp.Permiso_existente = "N";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _Error = ex.Message;
        //        throw new Exception("Se produjo un error al cargar la cabecera de exportación (AFIP). Error: " + ex.Message);
        //    }
        //    _ErrorMessage = _Error;
        //    return _Comp;
        //}

        /// <summary>
        /// Metodo que carga los datos de la cabecera.
        /// </summary>
        /// <param name="_Cab">Clase cabecera.</param>
        /// <param name="_ErrorMessage">Mensaje de error de salida.</param>
        /// <returns>Clase del WebService.</returns>
        private static wsExportacion.ClsFEXRequest Cargar_Comprobante(Cabecera _Cab, List<wsExportacion.Cmp_asoc> _compAsoc, out string _ErrorMessage)
        {
            string _Error = "";
            //Cargamos la clase del request de la cabecera.
            wsExportacion.ClsFEXRequest _Comp = new wsExportacion.ClsFEXRequest();
            try
            {
                _Comp.Id = Convert.ToInt64(_Cab.iD);
                _Comp.Cbte_Tipo = Convert.ToInt16(_Cab.Tipo);//codafip
                _Comp.Fecha_cbte = _Cab.fechaemision.Replace("-", "");
                _Comp.Punto_vta = Convert.ToInt16(_Cab.ptovta);
                string _Cbte = _Cab.comprobante.Remove(0, 5);
                _Comp.Cbte_nro = Convert.ToInt64(_Cbte);
                _Comp.Tipo_expo = Convert.ToInt16(_Cab.concepto);
                _Comp.Dst_cmp = Convert.ToInt16(_Cab.destinocmp);
                _Comp.Cliente = _Cab.receptor;
                _Comp.Domicilio_cliente = _Cab.domicilioreceptor;
                _Comp.Id_impositivo = _Cab.idimpositivoreceptor;
                _Comp.Moneda_Id = _Cab.moneda;
                _Comp.Moneda_ctz = Convert.ToDecimal(_Cab.tipocambio, CultureInfo.InvariantCulture.NumberFormat);
                _Comp.Imp_total = Convert.ToDecimal(_Cab.importetotal, CultureInfo.InvariantCulture.NumberFormat);
                _Comp.Obs = _Cab.otrosdatosgenerales;
                _Comp.Forma_pago = _Cab.formaspago;
                _Comp.Incoterms = _Cab.incoterms;
                _Comp.Idioma_cbte = Convert.ToInt16(_Cab.idioma);
                //Vemos el Permio Existente.
                if (_Comp.Cbte_Tipo == 20 || _Comp.Cbte_Tipo == 21)
                    _Comp.Permiso_existente = "";
                else
                {
                    if (_Comp.Cbte_Tipo == 19)
                    {
                        if (_Comp.Tipo_expo == 2 || _Comp.Tipo_expo == 4)
                            _Comp.Permiso_existente = "";
                        else
                            _Comp.Permiso_existente = "N";
                    }
                    else
                        _Comp.Permiso_existente = "N";
                }
                //Cargo los comprobantes asociados si es nota de credito.
                if (_Cab.Tipo == 21)
                {
                    //veo si tengo comprobante asociado.
                    if (_compAsoc.Count > 0)
                        _Comp.Cmps_asoc = _compAsoc.ToArray();
                }
                //Vemos si es Servicios
                if (_Comp.Tipo_expo == 2 || _Comp.Tipo_expo == 4)
                {
                    _Comp.Fecha_pago = _Cab.fechavto;
                }

            }
            catch (Exception ex)
            {
                _Error = ex.Message;
                throw new Exception("Se produjo un error al cargar la cabecera de exportación (AFIP). Error: " + ex.Message);
            }
            _ErrorMessage = _Error;
            return _Comp;
        }
        /// <summary>
        /// Metodo que carga lo items en la clase del WebService.
        /// </summary>
        /// <param name="_Det">Lista de los detalles.</param>
        /// <param name="_ErrorMessage">Mensaje de error de salida.</param>
        /// <returns>Clase del Item del WebService.</returns>
        private static wsExportacion.Item[] Cargar_Items(List<Detalle> _Det, out string _ErrorMessage)
        {
            string _Error = "";
            wsExportacion.Item[] _Items = new wsExportacion.Item[_Det.Count];
            try
            {
                //Cargamos los detalles de los items.                
                for (int i = 0; _Det.Count > i; i++)
                {
                    wsExportacion.Item _it = new wsExportacion.Item();
                    _it.Pro_codigo = _Det[i].cod;
                    _it.Pro_ds = _Det[i].descrip;
                    _it.Pro_qty = Convert.ToDecimal(_Det[i].cant, CultureInfo.InvariantCulture.NumberFormat);
                    _it.Pro_umed = Convert.ToInt32(_Det[i].unimed);
                    _it.Pro_precio_uni = Convert.ToDecimal(_Det[i].preciounit, CultureInfo.InvariantCulture.NumberFormat);
                    _it.Pro_total_item = Convert.ToDecimal(_Det[i].importe, CultureInfo.InvariantCulture.NumberFormat);
                    _it.Pro_bonificacion = Convert.ToDecimal(_Det[i].dto.Replace(',','.'), CultureInfo.InvariantCulture.NumberFormat);
                    _Items[i] = _it;
                }
            }
            catch (Exception ex)
            {
                _Error = ex.Message;
            }
            _ErrorMessage = _Error;
            return _Items;
        }
        /// <summary>
        /// Elimana los archivos pasados como parametros.
        /// </summary>
        /// <param name="_Cabecera">Path del archivo de Cabecera.</param>
        /// <param name="_Detalle">Path del archivo de Detalle.</param>
        private static void EliminarXml(string _Cabecera, string _Detalle)
        {
            if (System.IO.File.Exists(_Cabecera))
                System.IO.File.Delete(_Cabecera);
            if (System.IO.File.Exists(_Detalle))
                System.IO.File.Delete(_Detalle);
        }

    }

    public class MetodosVarios
    {

        /// <summary>
        /// Metodo privado que valida que exista el TA.xml. Si existe y esta activo recuperamos los datos.
        /// Si no lo tenemos o no podemos leerlo respondemos false. Si lo tenemos y lo leemos respondemos True.
        /// </summary>
        /// <returns>Booleano</returns>
        public static bool ValidoTA(string _PathCertificado, string _pathTA, string pPathLog, string _Cuit, out string Token, out string Sign, out DateTime ExpirationTime)
        {
            Token = "";
            Sign = "";
            ExpirationTime = DateTime.Now;
            //variable con la respuesta.
            bool booRta = false;
            //instanciamos el dataset
            System.Data.DataSet dtsTA = new System.Data.DataSet();
            //Vemos si ya tenemos el TA.xml
            //string strTApath = SaberPathTA(_PathCertificado) + @"\TA.xml";
            //string strTApath = _pathTA + @"\TA.xml";
            string strTApath = _pathTA + @"\" + _Cuit + ".xml";

            if (System.IO.File.Exists(strTApath))
            {
                System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                try
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Leemos el archivo: " + strTApath);
                    dtsTA.ReadXml(fsReadXml);
                    fsReadXml.Close();
                    //Vemos si esta activo.
                    DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString());
                    if (dttExpiration > DateTime.Now)
                    {
                        Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                        Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                        ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString());
                        booRta = true;
                    }
                    else
                    {
                        booRta = false;
                    }
                }
                catch (Exception ex)
                {
                    //Escribimos el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                    //Como se produjo un error lo vuelvo a pedir.
                    fsReadXml.Close();
                    booRta = false;
                }

            }
            else
            {
                booRta = false;
            }
            dtsTA.Dispose();
            return booRta;
        }
        /// <summary>
        /// Metodo para saber el Path de la aplicacion.
        /// </summary>
        /// <returns></returns>
        private static string SaberPath()
        {
            return System.IO.Path.GetDirectoryName(LogFile.KnowPath());
        }
        /// <summary>
        /// Metodo que devuelve el Path donde se encuentra el certificado.
        /// Lo sacamos del path del certificado.
        /// </summary>
        /// <param name="pPath">Path del Certificado.</param>
        /// <returns>Path sin la extencion del certificado.</returns>
        public static string SaberPathTA(string pPath)
        {
            return System.IO.Path.GetDirectoryName(pPath);
        }
        /// <summary>
        /// Metodo privado que consume el servicio Wsaa para la obtencion de la autorizacion.
        /// </summary>
        public static void ObtenerDatosWsaa(string _defaultservicio, bool _IsTest, string _PathCertificado, string _PathTa, string pPathLog, 
            string _Cuit, out string _token, out string _sign)
        {
            string strUrlWsaaWsdl;
            string strIdServicioNegocio = _defaultservicio;
            //string strRutaCertSigner = DEFAULT_CERTSIGNER;
            //string strRutaCertSigner = strPathCert;
            bool blnVerboseMode = true;
            //string strTApath = SaberPathTA(_PathCertificado) + @"\TA.xml";
            //string strTApath = _PathTa + @"\TA.xml";
            string strTApath = _PathTa + @"\" + _Cuit+ ".xml";

            if (_IsTest)
                strUrlWsaaWsdl = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
            else
                strUrlWsaaWsdl = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";

            // Argumentos OK, entonces procesar normalmente... 

            Wsaa.LoginTicket objTicketRespuesta;
            string strTicketRespuesta;
            _token = "";
            _sign = "";
            try
            {

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Servicio a acceder: " + strIdServicioNegocio);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWsaaWsdl);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Ruta del certificado: " + _PathCertificado);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Modo verbose: " + blnVerboseMode.ToString());
                }

                objTicketRespuesta = new Wsaa.LoginTicket();

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Accediendo a " + strUrlWsaaWsdl);
                }

                strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWsaaWsdl, _PathCertificado, blnVerboseMode, pPathLog);

                if (blnVerboseMode)
                {
                    //Ponemos los datos en el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***CONTENIDO DEL TICKET RESPUESTA:");
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Token: " + objTicketRespuesta.Token);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Sign: " + objTicketRespuesta.Sign);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " GenerationTime: " + Convert.ToString(objTicketRespuesta.GenerationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " ExpirationTime: " + Convert.ToString(objTicketRespuesta.ExpirationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Service: " + objTicketRespuesta.Service);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " UniqueID: " + Convert.ToString(objTicketRespuesta.UniqueId));
                    //Pasamos los datos a las variables.
                    _token = objTicketRespuesta.Token;
                    _sign = objTicketRespuesta.Sign;
                    //dttExpirationTime = objTicketRespuesta.ExpirationTime;
                    objTicketRespuesta.XmlLoginTicketResponse.Save(strTApath);
                }
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***EXCEPCION AL OBTENER TICKET:" + excepcionAlObtenerTicket.Message);
                _token = "";
                _sign = "";
            }

        }
    }

    public class Monedas
    {
        public string _codigo { get; set; }
        public string _nombre { get; set; }

        public static List<Monedas> GetMonedas()
        {
            List<Monedas> _lst = new List<Monedas>();

            Monedas _pesos = new Monedas()
            {
                _codigo = "PES",
                _nombre = "Pesos"
            };

            Monedas _dolares = new Monedas()
            {
                _codigo = "DOL",
                _nombre = "Dolares"
            };

            _lst.Add(_pesos);
            _lst.Add(_dolares);

            return _lst;
        }
    }

    public class TiposExportacion
    {
        public string _codigo { get; set; }
        public string _nombre { get; set; }

        public static List<TiposExportacion> GetTiposExportacion()
        {
            List<TiposExportacion> _lst = new List<TiposExportacion>();

            TiposExportacion _tipo1 = new TiposExportacion()
            {
                _codigo = "1",
                _nombre = "Exportación definitiva de Bienes"
            };

            TiposExportacion _tipo2 = new TiposExportacion()
            {
                _codigo = "2",
                _nombre = "Servicios"
            };

            TiposExportacion _tipo4 = new TiposExportacion()
            {
                _codigo = "4",
                _nombre = "Otros"
            };

            _lst.Add(_tipo1);
            _lst.Add(_tipo2);
            _lst.Add(_tipo4);

            return _lst;
        }
    }

    public class Paises
    {
        public string _codigo { get; set; }
        public string _nombre { get; set; }

        public static List<Paises> GetPaises()
        {
            List<Paises> _lst = new List<Paises>();

            Paises _tipo1 = new Paises()
            {
                _codigo = "200",
                _nombre = "Argentina"
            };

            Paises _tipo2 = new Paises()
            {
                _codigo = "202",
                _nombre = "Bolivia"
            };

            Paises _tipo3 = new Paises()
            {
                _codigo = "203",
                _nombre = "Brasil"
            };

            Paises _tipo4 = new Paises()
            {
                _codigo = "205",
                _nombre = "Colombia"
            };

            Paises _tipo5 = new Paises()
            {
                _codigo = "208",
                _nombre = "Chile"
            };

            Paises _tipo6 = new Paises()
            {
                _codigo = "218",
                _nombre = "Mexico"
            };

            Paises _tipo7 = new Paises()
            {
                _codigo = "221",
                _nombre = "Paraguay"
            };

            Paises _tipo8 = new Paises()
            {
                _codigo = "222",
                _nombre = "Peru"
            };

            Paises _tipo9 = new Paises()
            {
                _codigo = "225",
                _nombre = "Uruguay"
            };

            Paises _tipo10 = new Paises()
            {
                _codigo = "250",
                _nombre = "Tierra del Fuego"
            };

            Paises _tipo11 = new Paises()
            {
                _codigo = "251",
                _nombre = "ZF La Plata"
            };



            _lst.Add(_tipo1);
            _lst.Add(_tipo2);
            _lst.Add(_tipo3);
            _lst.Add(_tipo4);
            _lst.Add(_tipo5);
            _lst.Add(_tipo6);
            _lst.Add(_tipo7);
            _lst.Add(_tipo8);
            _lst.Add(_tipo9);
            _lst.Add(_tipo10);
            _lst.Add(_tipo11);

            return _lst;
        }
    }

    public class Incoterms
    {
        public string _codigo { get; set; }
        public string _nombre { get; set; }

        public static List<Incoterms> GetIncoterms()
        {
            List<Incoterms> _lst = new List<Incoterms>();

            Incoterms _tipo1 = new Incoterms()
            {
                _codigo = "EXW",
                _nombre = "En fabrica"
            };

            Incoterms _tipo2 = new Incoterms()
            {
                _codigo = "FCA",
                _nombre = "Franco al transportista"
            };

            Incoterms _tipo3 = new Incoterms()
            {
                _codigo = "FAS",
                _nombre = "Franco al costado del buque"
            };

            Incoterms _tipo4 = new Incoterms()
            {
                _codigo = "FOB",
                _nombre = "Free on boar"
            };

            Incoterms _tipo5 = new Incoterms()
            {
                _codigo = "CFR",
                _nombre = "Costo y flete"
            };

            Incoterms _tipo6 = new Incoterms()
            {
                _codigo = "CIF",
                _nombre = "A bordo del buque"
            };

            Incoterms _tipo7 = new Incoterms()
            {
                _codigo = "CPT",
                _nombre = "Flete o porte pagado hasta"
            };

            Incoterms _tipo8 = new Incoterms()
            {
                _codigo = "CIP",
                _nombre = "Costo seguro y flete"
            };

            Incoterms _tipo9 = new Incoterms()
            {
                _codigo = "DAF",
                _nombre = "Delivered At Frontier"
            };

            Incoterms _tipo10 = new Incoterms()
            {
                _codigo = "DES",
                _nombre = "Entregada sobre Buque"
            };

            Incoterms _tipo11 = new Incoterms()
            {
                _codigo = "DEQ",
                _nombre = "Entregada en muelle"
            };

            Incoterms _tipo12 = new Incoterms()
            {
                _codigo = "DDU",
                _nombre = "Delivered Duty Unpaid"
            };

            Incoterms _tipo13 = new Incoterms()
            {
                _codigo = "DDP",
                _nombre = "Entregado derechos pagados"
            };



            _lst.Add(_tipo1);
            _lst.Add(_tipo2);
            _lst.Add(_tipo3);
            _lst.Add(_tipo4);
            _lst.Add(_tipo5);
            _lst.Add(_tipo6);
            _lst.Add(_tipo7);
            _lst.Add(_tipo8);
            _lst.Add(_tipo9);
            _lst.Add(_tipo10);
            _lst.Add(_tipo11);
            _lst.Add(_tipo12);
            _lst.Add(_tipo13);

            return _lst;
        }
    }

    public class CuitPais
    {
        public string cuit { get; set; }
        public string tipoCuit { get; set; }
        public string pais { get; set; }

        public static List<CuitPais> GetTiposExportacion()
        {
            List<CuitPais> _lst = new List<CuitPais>();

            CuitPais _tipo1 = new CuitPais()
            {
                cuit = "51600000016",
                tipoCuit = "OTRA TIPO DE ENTIDAD",
                pais = "URUGUAY"
            };

            CuitPais _tipo2 = new CuitPais()
            {
                cuit = "55000000018",
                tipoCuit = "JURIDICA",
                pais = "URUGUAY"
            };

            CuitPais _tipo3 = new CuitPais()
            {
                cuit = "50000000024",
                tipoCuit = "FISICA",
                pais = "PARAGUAY"
            };

            CuitPais _tipo4 = new CuitPais()
            {
                cuit = "51600000024",
                tipoCuit = "OTRA TIPO DE ENTIDAD",
                pais = "PARAGUAY"
            };

            CuitPais _tipo5 = new CuitPais()
            {
                cuit = "55000000026",
                tipoCuit = "JURIDICA",
                pais = "PARAGUAY"
            };

            _lst.Add(_tipo1);
            _lst.Add(_tipo2);
            _lst.Add(_tipo4);

            return _lst;
        }
    }

    public class PuntosDeVenta
    {
        public int PtoVta { get; set; }
        public string Bloqueado { get; set; }
        public string FechaBloqueo { get; set; }
    }
}
