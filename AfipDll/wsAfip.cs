﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.Threading;

namespace AfipDll
{
    public class wsAfip
    {
        /// <summary>
        /// Clase que define la cabecera.
        /// </summary>
        public class clsCabReq
        {
            /// <summary>
            /// Cantidad de facturas.
            /// </summary>
            public int CantReg
            {
                get;
                set;
            }
            /// <summary>
            /// Tipo de FC a imprimir.
            /// </summary>
            public int CbteTipo
            {
                get;
                set;
            }
            /// <summary>
            /// Punto de Venta del que se van imprimir las FC.
            /// </summary>
            public int PtoVta
            {
                get;
                set;
            }
        }

        public class clsDetReq
        {
            /// <summary>
            /// Concepto de la FC.
            /// </summary>
            public int Concepto
            {
                get;
                set;
            }
            /// <summary>
            /// Codigo del docuento del comprador.
            /// </summary>
            public int DocTipo
            {
                get;
                set;
            }
            /// <summary>
            /// Numero del documento del comprador.
            /// </summary>
            public long DocNro
            {
                get;
                set;
            }
            /// <summary>
            /// Nro. del comprobante desde.
            /// </summary>
            public long CbteDesde
            {
                get;
                set;
            }
            /// <summary>
            /// Nro. del comprobante hasta.
            /// </summary>
            public long CbteHasta
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha del comprobante.
            /// </summary>
            public string CbteFch
            {
                get;
                set;
            }
            /// <summary>
            /// Importe Total del comprobante.
            /// </summary>
            public double ImpTotal
            {
                get;
                set;
            }
            /// <summary>
            /// Importe noto no agravado.
            /// </summary>
            public double ImpTotConc
            {
                get;
                set;
            }
            /// <summary>
            /// Importe neto agravado.
            /// </summary>
            public double ImpNeto
            {
                get;
                set;
            }
            /// <summary>
            /// Importe de exentos.
            /// </summary>
            public double ImpOpEx
            {
                get;
                set;
            }
            /// <summary>
            /// Importe total de IVA.
            /// </summary>
            public double ImpIVA
            {
                get;
                set;
            }
            /// <summary>
            /// Importe Total de tributos.
            /// </summary>
            public double ImpTrib
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de inicio del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchServDesde
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de finalizacion del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchServHasta
            {
                get;
                set;
            }
            /// <summary>
            /// Fecha de vencimiento del servicio a facturar. Obligatorio para conceptos 2 y 3
            /// </summary>
            public string FchVtoPago
            {
                get;
                set;
            }
            /// <summary>
            /// Codigo de la moneda utilizada en el comprobante.
            /// </summary>
            public string MonID
            {
                get;
                set;
            }
            /// <summary>
            /// Cotizacion de la moneda informada. Para PES = 1.
            /// </summary>
            public double MonCotiz
            {
                get;
                set;
            }
        }

        public class clsIVA
        {
            /// <summary>
            /// Codigo del tipo de IVA.
            /// </summary>
            public int ID
            {
                get;
                set;
            }
            /// <summary>
            /// Base imponible para el codigo de IVA.
            /// </summary>
            public double BaseImp
            {
                get;
                set;
            }
            /// <summary>
            /// Sumatoria del importe de IVA para la alicuota informada.
            /// </summary>
            public double Importe
            {
                get;
                set;
            }
        }

        public class clsTributos
        {
            /// <summary>
            /// Codigo identificatorio del tributo.
            /// </summary>
            public int ID
            {
                get;
                set;
            }
            /// <summary>
            /// Descripcion del tributo.
            /// </summary>
            public string Desc
            {
                get;
                set;
            }
            /// <summary>
            /// Base imponible para el tributo
            /// </summary>
            public double BaseImp
            {
                get;
                set;
            }
            public double Alic
            {
                get;
                set;
            }
            public double Importe
            {
                get;
                set;
            }
        }

        public class clsCaeResponse
        {
            public string Resultado { get; set; }
            public string Cae { get; set; }
            public string FechaVto { get; set; }
            public string ErrorCode { get; set; }
            public string ErrorMsj { get; set; }
        }

        public class clsCaeRecupero
        {
            public string Resultado { get; set; }
            public string Cae { get; set; }
            public string FechaVto { get; set; }
            public double ImporteTotal { get; set; }
            public string ErrorMsj { get; set; }
        }

        public class PtoVenta
        {
            public int Nro { get; set; }
            public string EmisionTipo { get; set; }
            public string Bloqueado { get; set; }
            public string FechaBaja { get; set; }
        }

        public class Errors
        {
            public int Code { get; set; }
            public string Msg { get; set; }
        }

        public class CaeaSolicitar
        {
            public string CAEA { get; set; }
            public int Periodo { get; set; }
            public short Orden { get; set; }
            public string FchVigDesde { get; set; }
            public string FchVigHasta { get; set; }
            public string FchProceso { get; set; }
            public string FchTopeInf { get; set; }
        }

        public class UltimoComprobante
        {
            public int PtoVta { get; set; }
            public int CbteTipo { get; set; }
            public int CbteNro { get; set; }
        }

        public class CaeaResultadoInformar
        {
            public string Resultado { get; set; }
            public int DocTipo { get; set; }
            public long DocNro { get; set; }
            public long CbteDesde { get; set; }
            public string Caea { get; set; }
            public string Observaciones { get; set; }
        }

        public class ComprobanteAsoc
        {
            public int Tipo { get; set; }
            public int PtoVta { get; set; }
            public int Nro { get; set; }
            public string Cuit { get; set; }
            public string CbteFch { get; set; }
        }

        public class Opcionales
        {
            public string Id { get; set; }
            public string Valor { get; set; }
        }


        /// <summary>
        /// URL servicio Wasa de Test.
        /// </summary>
        const string TEST_URLWSAAWSDL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
        /// <summary>
        /// URL serviciop Waa de Produccion.
        /// </summary>
        const string DEFAULT_URLWSAAWSDL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";
        const string DEFAULT_SERVICIO = "wsfe";
        //const string DEFAULT_SERVICIO = "wsmtxca";
        const bool DEFAULT_VERBOSE = true;
        /// <summary>
        /// URL servicio FE de Test.
        /// </summary>
        const string TEST_URLWSFEV1 = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx";
        /// <summary>
        /// URL servicio FE de Prooduccion.
        /// </summary>
        const string DEFAULT_URLWSFEV1 = "https://servicios1.afip.gov.ar/wsfev1/service.asmx";

        public static Wsaa ObtenerDatosWsaa(string pPath, string pPathTa, string pPathLog, bool IsTest, string _Cuit)
        {
            string strUrlWassaTest = TEST_URLWSAAWSDL;
            string strUrlWsaaWsdl = DEFAULT_URLWSAAWSDL;
            string strIdServicioNegocio = DEFAULT_SERVICIO;
            string strRutaCertSigner = pPath;
            bool blnVerboseMode = DEFAULT_VERBOSE;
            string strTApath = pPathTa + @"\" + _Cuit + ".xml";

            Wsaa wa = new Wsaa();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            Wsaa.LoginTicket objTicketRespuesta;
            string strTicketRespuesta;

            try
            {

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Servicio a acceder: " + strIdServicioNegocio);
                    if (IsTest)
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWassaTest);
                    else
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWsaaWsdl);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Ruta del certificado: " + strRutaCertSigner);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Modo verbose: " + blnVerboseMode.ToString());
                }

                objTicketRespuesta = new Wsaa.LoginTicket();

                if (blnVerboseMode)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Accediendo a " + strUrlWsaaWsdl);
                }

                if (IsTest)
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWassaTest, strRutaCertSigner, blnVerboseMode, pPathLog);
                else
                    strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWsaaWsdl, strRutaCertSigner, blnVerboseMode, pPathLog);

                if (blnVerboseMode)
                {
                    //Ponemos los datos en el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***CONTENIDO DEL TICKET RESPUESTA:");
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Token: " + objTicketRespuesta.Token);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Sign: " + objTicketRespuesta.Sign);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " GenerationTime: " + Convert.ToString(objTicketRespuesta.GenerationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " ExpirationTime: " + Convert.ToString(objTicketRespuesta.ExpirationTime));
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Service: " + objTicketRespuesta.Service);
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " UniqueID: " + Convert.ToString(objTicketRespuesta.UniqueId));
                    //Pasamos los datos a la clase.
                    wa.Token = objTicketRespuesta.Token;
                    wa.Sign = objTicketRespuesta.Sign;
                    wa.ExpirationTime = objTicketRespuesta.ExpirationTime;
                    objTicketRespuesta.XmlLoginTicketResponse.Save(strTApath);
                }
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***EXCEPCION AL OBTENER TICKET:" + excepcionAlObtenerTicket.Message);
            }

            return wa;
        }

        public static bool ValidoTA(string pPath, string pPathTa, string pPathLog, Wsaa _cls, string _Cuit)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //variable con la respuesta.
            bool booRta = false;
            //instanciamos el dataset
            System.Data.DataSet dtsTA = new System.Data.DataSet();
            //Vemos si ya tenemos el TA.xml
            //string strTApath = pPathTa + @"\TA.xml";
            string strTApath = pPathTa + @"\" + _Cuit + ".xml";

            if (System.IO.File.Exists(strTApath))
            {
                System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                try
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Leemos el archivo: " + strTApath);
                    dtsTA.ReadXml(fsReadXml);
                    fsReadXml.Close();
                    //Vemos si esta activo.
                    DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    if (dttExpiration > DateTime.Now)
                    {
                        _cls.Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                        _cls.Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                        _cls.ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        booRta = true;
                    }
                    else
                    {
                        booRta = false;
                    }
                }
                catch (Exception ex)
                {
                    //Escribimos el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                    //Como se produjo un error lo vuelvo a pedir.
                    fsReadXml.Close();
                    booRta = false;
                }

            }
            else
            {
                booRta = false;
            }
            dtsTA.Dispose();
            return booRta;
        }

        public static bool ValidoTANew(string pPath, string pPathTa, string pPathLog, WsaaNew _cls, string _Cuit)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //variable con la respuesta.
            bool booRta = false;
            //instanciamos el dataset
            System.Data.DataSet dtsTA = new System.Data.DataSet();
            string strTApath = pPathTa + @"\" + _Cuit + ".xml";

            if (System.IO.File.Exists(strTApath))
            {
                System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                try
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Leemos el archivo: " + strTApath);
                    dtsTA.ReadXml(fsReadXml);
                    fsReadXml.Close();
                    //Vemos si esta activo.
                    DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    if (dttExpiration > DateTime.Now)
                    {
                        _cls.Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                        _cls.Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                        _cls.ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        booRta = true;
                    }
                    else
                    {
                        booRta = false;
                    }
                }
                catch (Exception ex)
                {
                    //Escribimos el Log.
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                    //Como se produjo un error lo vuelvo a pedir.
                    fsReadXml.Close();
                    booRta = false;
                }

            }
            else
            {
                booRta = false;
            }
            dtsTA.Dispose();
            return booRta;
        }

        public static int RecuperoUltimoComprobante(string pPathCert, string pPathTa, string pPathLog,
            int PtoVta, int CbteTipo, string pCuit, bool IsTest, out string pError)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string _Error = "";
            int _comprobNro = -1;
            Wsaa _cls = new Wsaa();
            try
            {
                //strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////RECUPEROULTIMOCOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PTOVTA->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                //validamos que tenemos el TA.xml
                //if (!wsAfip.ValidoTA(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                //    _cls = wsAfip.ObtenerDatosWsaa(pPathCert, pPathTa, pPathLog, IsTest, pCuit);
                if (!ValidoTA(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = Wsaa.ObtenerDatosWsaa(DEFAULT_SERVICIO, pPathCert, pPathTa, pPathLog, IsTest, pCuit);
                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    //Pongo la url dependiendo de si es o no Test.
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    //Consultamos el ultimo comprobante autorizado.
                    wsAfipCae.FERecuperaLastCbteResponse clsUltimoComprobante = _client.FECompUltimoAutorizado(Auth, PtoVta, CbteTipo);
                    
                    if (clsUltimoComprobante.Errors != null)
                    {
                        if (clsUltimoComprobante.Errors.Length > 0)
                        {
                            for (int i = 0; clsUltimoComprobante.Errors.Length < 0; i++)
                            {
                                if (String.IsNullOrEmpty(_Error))
                                    _Error = clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                                else
                                    _Error = _Error + " | " + clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                            }
                        }
                    }

                    _comprobNro = clsUltimoComprobante.CbteNro;
                }
                //wsAfipCae.Service srvCall = new wsAfipCae.Service();
                //if (IsTest)
                //    srvCall.Url = TEST_URLWSFEV1;
                //else
                //    srvCall.Url = DEFAULT_URLWSFEV1;
                ////Consultamos el ultimo comprobante autorizado.
                //IcgPlugInFacturacion.wsAfipCae.FERecuperaLastCbteResponse clsUltimoComprobante = srvCall.FECompUltimoAutorizado(Auth, PtoVta, CbteTipo);
                //if (clsUltimoComprobante.Errors != null)
                //{
                //    if (clsUltimoComprobante.Errors.Length > 0)
                //    {
                //        for (int i = 0; clsUltimoComprobante.Errors.Length < 0; i++)
                //        {
                //            if (String.IsNullOrEmpty(_Error))
                //                _Error = clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                //            else
                //                _Error = _Error + " | " + clsUltimoComprobante.Errors[i].Code.ToString() + " - " + clsUltimoComprobante.Errors[i].Msg;
                //        }
                //    }
                //}
                pError = _Error;
                //Recuperamos los datos del comprobante.
                return _comprobNro;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL TRAER EL ULTIMO AUTORIZADO*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");
                pError = ex.Message;
                return -1;
            }
        }

        public static UltimoComprobante RecuperoUltimoComprobanteNew(string pPathCert, string pPathTa, string pPathLog,
            int PtoVta, int CbteTipo, string pCuit, bool IsTest, SecureString _pass, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Errors> _errors = new List<Errors>();
            WsaaNew _cls = new WsaaNew();
            UltimoComprobante _ultimo = new UltimoComprobante();
            
            try
            {
                //strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////RECUPEROULTIMOCOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PTOVTA->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, pPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    //Pongo la url dependiendo de si es o no Test.
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    //Consultamos el ultimo comprobante autorizado.
                    wsAfipCae.FERecuperaLastCbteResponse clsUltimoComprobante = _client.FECompUltimoAutorizado(Auth, PtoVta, CbteTipo);

                    if (clsUltimoComprobante.Errors != null)
                    {
                            for (int i = 0; clsUltimoComprobante.Errors.Length < 0; i++)
                            {
                                Errors _er = new Errors();
                                _er.Code = clsUltimoComprobante.Errors[i].Code;
                                _er.Msg = clsUltimoComprobante.Errors[i].Msg;

                                _errors.Add(_er);
                            }
                    }
                    else
                    {
                        _ultimo.CbteNro = clsUltimoComprobante.CbteNro;
                        _ultimo.CbteTipo = clsUltimoComprobante.CbteTipo;
                        _ultimo.PtoVta = clsUltimoComprobante.PtoVta;
                    }
                    
                }
                _errores = _errors;
                //Recuperamos los datos del comprobante.
                return _ultimo;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL TRAER EL ULTIMO AUTORIZADO*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");

                Errors _er = new Errors();
                _er.Code = -1;
                _er.Msg = ex.Message;

                _errors.Add(_er);
                _errores = _errors;

                _ultimo.CbteNro = -1;
                _ultimo.CbteTipo = 0;
                _ultimo.PtoVta = 0;

                return _ultimo;
            }
        }


        public static UltimoComprobante RecuperoUltimoComprobanteNew(string pPathCert, string pPathTa, string pPathLog,
            int PtoVta, int CbteTipo, string pCuit, bool IsTest, WsaaNew _wsaa, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Errors> _errors = new List<Errors>();
            UltimoComprobante _ultimo = new UltimoComprobante();
            try
            {
                //strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////RECUPEROULTIMOCOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PTOVTA->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _wsaa.Token;
                Auth.Sign = _wsaa.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    //Pongo la url dependiendo de si es o no Test.
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    //Consultamos el ultimo comprobante autorizado.
                    wsAfipCae.FERecuperaLastCbteResponse clsUltimoComprobante = _client.FECompUltimoAutorizado(Auth, PtoVta, CbteTipo);

                    if (clsUltimoComprobante.Errors != null)
                    {
                        for (int i = 0; clsUltimoComprobante.Errors.Length < 0; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = clsUltimoComprobante.Errors[i].Code;
                            _er.Msg = clsUltimoComprobante.Errors[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                    else
                    {
                        _ultimo.CbteNro = clsUltimoComprobante.CbteNro;
                        _ultimo.CbteTipo = clsUltimoComprobante.CbteTipo;
                        _ultimo.PtoVta = clsUltimoComprobante.PtoVta;
                    }

                }
                _errores = _errors;
                //Recuperamos los datos del comprobante.
                return _ultimo;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL TRAER EL ULTIMO AUTORIZADO*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");

                Errors _er = new Errors();
                _er.Code = -1;
                _er.Msg = ex.Message;

                _errors.Add(_er);
                _errores = _errors;

                _ultimo.CbteNro = -1;
                _ultimo.CbteTipo = 0;
                _ultimo.PtoVta = 0;

                return _ultimo;
            }
        }

        /// <summary>
        /// Para Front Retail
        /// </summary>
        /// <param name="_CabReq"></param>
        /// <param name="_DetReq"></param>
        /// <param name="_Trib"></param>
        /// <param name="_Iva"></param>
        /// <param name="_ComprobantesAsociados"></param>
        /// <param name="pCerPath"></param>
        /// <param name="pPathTa"></param>
        /// <param name="pPathLog"></param>
        /// <param name="pCUIT"></param>
        /// <param name="IsTest"></param>
        /// <returns></returns>
        public static clsCaeResponse ObtenerDatosCAE(clsCabReq _CabReq, clsDetReq _DetReq, List<clsTributos> _Trib, List<clsIVA> _Iva,
            List<AfipDll.wsAfip.ComprobanteAsoc> _ComprobantesAsociados,
            string pCerPath, string pPathTa, string pPathLog, string pCUIT, bool IsTest)
        {
            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            Wsaa _cls = new Wsaa();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + pCUIT);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                return _clsRta;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                return _clsRta;
            }

            //validamos que tenemos el TA.xml
            if (!wsAfip.ValidoTA(strPathCert, pPathTa, pPathLog, _cls, pCUIT))
                _cls = wsAfip.ObtenerDatosWsaa(strPathCert, pPathTa, pPathLog, IsTest, pCUIT);

            try
            {
                if (_cls.Token != null)
                {

                    if (_cls.Token.Length > 0)
                    {
                        if (_cls.Sign.Length > 0)
                        {
                            if (_cls.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _cls.Token;
                                Auth.Sign = _cls.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos la clase que contiene los datos de la FC.
                                wsAfipCae.FECAERequest CAEReq = new wsAfipCae.FECAERequest();
                                //Instanciamos la clase de la cabecera de los datos de la FC. y la cargamos.
                                wsAfipCae.FECAECabRequest CabRequest = new wsAfipCae.FECAECabRequest();
                                CabRequest.CantReg = _CabReq.CantReg;
                                CabRequest.CbteTipo = _CabReq.CbteTipo;
                                CabRequest.PtoVta = _CabReq.PtoVta;
                                //Instanciamos la clase de los detalles de la FC (Array).
                                wsAfipCae.FECAEDetRequest[] arrCAEDetReq = new wsAfipCae.FECAEDetRequest[1];
                                //Instanciamos la clase de los detalles de la FC que luego se cargara en el Array de detalles de FC.
                                wsAfipCae.FECAEDetRequest CAEDetReq = new wsAfipCae.FECAEDetRequest();
                                CAEDetReq.Concepto = _DetReq.Concepto;
                                CAEDetReq.DocTipo = _DetReq.DocTipo;
                                if (_DetReq.DocTipo == 99)
                                    CAEDetReq.DocNro = 0;
                                else
                                    CAEDetReq.DocNro = _DetReq.DocNro;
                                CAEDetReq.CbteDesde = _DetReq.CbteDesde;
                                CAEDetReq.CbteHasta = _DetReq.CbteHasta;
                                CAEDetReq.CbteFch = _DetReq.CbteFch;
                                CAEDetReq.ImpTotal = _DetReq.ImpTotal;
                                CAEDetReq.ImpTotConc = _DetReq.ImpTotConc;
                                CAEDetReq.ImpNeto = _DetReq.ImpNeto;
                                CAEDetReq.ImpOpEx = _DetReq.ImpOpEx;
                                CAEDetReq.ImpTrib = _DetReq.ImpTrib;
                                CAEDetReq.ImpIVA = _DetReq.ImpIVA;
                                if (_DetReq.Concepto > 1)
                                {
                                    CAEDetReq.FchServDesde = _DetReq.FchServDesde;
                                    CAEDetReq.FchServHasta = _DetReq.FchServHasta;
                                    CAEDetReq.FchVtoPago = _DetReq.FchVtoPago;
                                }
                                CAEDetReq.MonId = _DetReq.MonID;
                                CAEDetReq.MonCotiz = _DetReq.MonCotiz;

                                if (_Trib.Count > 0)
                                {
                                    //Instanciamos el array de la clase de Tributos y le cargamos los datos.
                                    wsAfipCae.Tributo[] arrTrib = new wsAfipCae.Tributo[_Trib.Count];
                                    for (int i = 0; i < _Trib.Count; i++)
                                    {
                                        //Instanciamos la clase de tributo para llenar el array.
                                        wsAfipCae.Tributo Trib = new wsAfipCae.Tributo();
                                        Trib.Id = short.Parse(_Trib[i].ID.ToString());
                                        Trib.Desc = _Trib[i].Desc;
                                        Trib.BaseImp = _Trib[i].BaseImp;
                                        Trib.Alic = _Trib[i].Alic;
                                        Trib.Importe = _Trib[i].Importe;
                                        arrTrib[i] = Trib;
                                    }
                                    CAEDetReq.Tributos = arrTrib;
                                }

                                //Instanciamo el array de la clase de Alicuota de IVA.
                                if (_Iva.Count > 0)
                                {
                                    wsAfipCae.AlicIva[] arrAlic = new wsAfipCae.AlicIva[_Iva.Count];
                                    for (int x = 0; x < _Iva.Count; x++)
                                    {
                                        //Instanciamos la clase de Alicuotas de IVa para llenar el array.
                                        wsAfipCae.AlicIva Alic = new wsAfipCae.AlicIva();
                                        Alic.Id = _Iva[x].ID;
                                        Alic.BaseImp = _Iva[x].BaseImp;
                                        Alic.Importe = _Iva[x].Importe;
                                        arrAlic[x] = Alic;
                                    }
                                    CAEDetReq.Iva = arrAlic;
                                }

                                //Notas de credito
                                List<AfipDll.wsAfipCae.CbteAsoc> _lstCA = new List<AfipDll.wsAfipCae.CbteAsoc>();
                                if (_CabReq.CbteTipo == 3 || _CabReq.CbteTipo == 8 || _CabReq.CbteTipo == 13)
                                {
                                    //Recorro los comprobantes.
                                    foreach (AfipDll.wsAfip.ComprobanteAsoc _ca in _ComprobantesAsociados)
                                    {
                                        AfipDll.wsAfipCae.CbteAsoc _clsCA = new AfipDll.wsAfipCae.CbteAsoc();
                                        _clsCA.CbteFch = _ca.CbteFch;
                                        _clsCA.Cuit = _ca.Cuit;
                                        _clsCA.Nro = _ca.Nro;
                                        _clsCA.PtoVta = _ca.PtoVta;
                                        _clsCA.Tipo = _ca.Tipo;
                                        _lstCA.Add(_clsCA);
                                    }
                                    CAEDetReq.CbtesAsoc = _lstCA.ToArray();
                                }

                                //Cargamos el array de detalles de FC.
                                arrCAEDetReq[0] = CAEDetReq;
                                //Cargamo la clase de los datos de las FC's.
                                CAEReq.FeCabReq = CabRequest;
                                CAEReq.FeDetReq = arrCAEDetReq;
                                //Instanciamos el Servicio.
                                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                                {
                                    if (IsTest)
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                                    else
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                                    //Consumimos el servicio y cargamos la respuesta en la clase.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), CAEReq.FeDetReq.ToString());
                                    //Escribimos el xml del Request.
                                    System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeRequest.xml";
                                    System.IO.FileStream file = System.IO.File.Create(path);
                                    _Antes.Serialize(file, CAEReq);
                                    file.Close();
                                    //Solicitamos el CAE
                                    wsAfipCae.FECAEResponse CaeResponse = _client.FECAESolicitar(Auth, CAEReq);

                                    //Escribimos el xml del Response.
                                    System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEResponse));
                                    var pathD = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeResponse.xml";
                                    System.IO.FileStream fileD = System.IO.File.Create(pathD);
                                    _Despues.Serialize(fileD, CaeResponse);
                                    fileD.Close();

                                    //Vemos si el resultado es Aprobado.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                    //Recorremos los Errores.
                                    if (CaeResponse.Errors != null)
                                    {
                                        _clsRta.Resultado = "R";
                                        for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                            {
                                                _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                            }
                                            else
                                            {
                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                            }
                                        }
                                    }
                                    if (CaeResponse.FeCabResp != null)
                                    {
                                        switch (CaeResponse.FeCabResp.Resultado)
                                        {
                                            case "A":
                                                {
                                                    _clsRta.Resultado = "A";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;

                                                    break;
                                                }
                                            case "P":
                                                {
                                                    _clsRta.Resultado = "P";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            case "R":
                                                {
                                                    _clsRta.Resultado = "R";
                                                    _clsRta.Cae = "";
                                                    _clsRta.FechaVto = "";
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        if (_clsRta.Resultado != "R")
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Resultado: " + _clsRta.Resultado +
                                                       ". CAE: " + _clsRta.Cae + ". Fecha VTO: " + _clsRta.FechaVto);
                                    }

                                }

                                //Salimos de la Funcion.
                                return _clsRta;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj = "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                return _clsRta;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj = "El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            return _clsRta;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        return _clsRta;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    return _clsRta;
                }
            }
            catch (Exception ex)
            {
                string _Error;
                //Recupero el nro del ultimo comprobante.
                int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, pPathLog, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                //Recupero los datos del comprobante.
                //else
                //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "R";
                return _clsRta;
            }
        }

        public static clsCaeResponse ObtenerDatosCAE(clsCabReq _CabReq, clsDetReq _DetReq, List<clsTributos> _Trib, List<clsIVA> _Iva,
            List<Opcionales> _Opcionales, List<AfipDll.wsAfip.ComprobanteAsoc> _cbteAsoc, string pCerPath, string pPathTa, 
            string pPathLog, string pCUIT, bool IsTest,
            WsaaNew _wsaa, string _cbu, bool _generaXml)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + pCUIT);

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                return _clsRta;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                return _clsRta;
            }

            try
            {
                if (_wsaa.Token != null)
                {

                    if (_wsaa.Token.Length > 0)
                    {
                        if (_wsaa.Sign.Length > 0)
                        {
                            if (_wsaa.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _wsaa.Token;
                                Auth.Sign = _wsaa.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos la clase que contiene los datos de la FC.
                                wsAfipCae.FECAERequest CAEReq = new wsAfipCae.FECAERequest();
                                //Instanciamos la clase de la cabecera de los datos de la FC. y la cargamos.
                                wsAfipCae.FECAECabRequest CabRequest = new wsAfipCae.FECAECabRequest();
                                CabRequest.CantReg = _CabReq.CantReg;
                                CabRequest.CbteTipo = _CabReq.CbteTipo;
                                CabRequest.PtoVta = _CabReq.PtoVta;
                                //Instanciamos la clase de los detalles de la FC (Array).
                                wsAfipCae.FECAEDetRequest[] arrCAEDetReq = new wsAfipCae.FECAEDetRequest[1];
                                //Instanciamos la clase de los detalles de la FC que luego se cargara en el Array de detalles de FC.
                                wsAfipCae.FECAEDetRequest CAEDetReq = new wsAfipCae.FECAEDetRequest();                               
                                CAEDetReq.Concepto = _DetReq.Concepto;
                                CAEDetReq.DocTipo = _DetReq.DocTipo;
                                CAEDetReq.DocNro = _DetReq.DocNro;
                                CAEDetReq.CbteDesde = _DetReq.CbteDesde;
                                CAEDetReq.CbteHasta = _DetReq.CbteHasta;
                                CAEDetReq.CbteFch = _DetReq.CbteFch;
                                CAEDetReq.ImpTotal = _DetReq.ImpTotal;
                                CAEDetReq.ImpTotConc = _DetReq.ImpTotConc;
                                CAEDetReq.ImpNeto = _DetReq.ImpNeto;
                                CAEDetReq.ImpOpEx = _DetReq.ImpOpEx;
                                CAEDetReq.ImpTrib = _DetReq.ImpTrib;
                                CAEDetReq.ImpIVA = _DetReq.ImpIVA;
                                if (_DetReq.Concepto > 1)
                                {
                                    CAEDetReq.FchServDesde = _DetReq.FchServDesde;
                                    CAEDetReq.FchServHasta = _DetReq.FchServHasta;
                                    CAEDetReq.FchVtoPago = _DetReq.FchVtoPago;
                                }
                                CAEDetReq.MonId = _DetReq.MonID;
                                CAEDetReq.MonCotiz = _DetReq.MonCotiz;

                                if (_Trib.Count > 0)
                                {
                                    //Instanciamos el array de la clase de Tributos y le cargamos los datos.
                                    wsAfipCae.Tributo[] arrTrib = new wsAfipCae.Tributo[_Trib.Count];
                                    for (int i = 0; i < _Trib.Count; i++)
                                    {
                                        //Instanciamos la clase de tributo para llenar el array.
                                        wsAfipCae.Tributo Trib = new wsAfipCae.Tributo();
                                        Trib.Id = short.Parse(_Trib[i].ID.ToString());
                                        Trib.Desc = _Trib[i].Desc;
                                        Trib.BaseImp = _Trib[i].BaseImp;
                                        Trib.Alic = _Trib[i].Alic;
                                        Trib.Importe = _Trib[i].Importe;
                                        arrTrib[i] = Trib;
                                    }
                                    CAEDetReq.Tributos = arrTrib;
                                }

                                //Instanciamo el array de la clase de Alicuota de IVA.
                                if (_Iva.Count > 0)
                                {
                                    wsAfipCae.AlicIva[] arrAlic = new wsAfipCae.AlicIva[_Iva.Count];
                                    for (int x = 0; x < _Iva.Count; x++)
                                    {
                                        //Instanciamos la clase de Alicuotas de IVa para llenar el array.
                                        wsAfipCae.AlicIva Alic = new wsAfipCae.AlicIva();
                                        Alic.Id = _Iva[x].ID;
                                        Alic.BaseImp = _Iva[x].BaseImp;
                                        Alic.Importe = _Iva[x].Importe;
                                        arrAlic[x] = Alic;
                                    }
                                    CAEDetReq.Iva = arrAlic;
                                }

                                //Vemos si tenemos FActura MiPyME
                                if (_CabReq.CbteTipo == 201 || _CabReq.CbteTipo == 202 || _CabReq.CbteTipo == 203 ||
                                            _CabReq.CbteTipo == 206 || _CabReq.CbteTipo == 207 || _CabReq.CbteTipo == 208)
                                {
                                    List<wsAfipCae.Opcional> _opcio = new List<wsAfipCae.Opcional>();
                                    //CBU
                                    wsAfipCae.Opcional _op1 = new wsAfipCae.Opcional();
                                    _op1.Id = "2101";
                                    _op1.Valor = _cbu;
                                    _opcio.Add(_op1);
                                    CAEDetReq.Opcionales = _opcio.ToArray();
                                }
                                //Excepcion de computo de IVA.
                                if (_Opcionales.Count > 0)
                                {
                                    List<wsAfipCae.Opcional> _opcio = new List<wsAfipCae.Opcional>();
                                    foreach (Opcionales op in _Opcionales)
                                    {
                                        wsAfipCae.Opcional _op1 = new wsAfipCae.Opcional();
                                        _op1.Id = op.Id;
                                        _op1.Valor = op.Valor;
                                        _opcio.Add(_op1);
                                    }
                                    CAEDetReq.Opcionales = _opcio.ToArray();
                                }
                                //Comprobantes Asociados.                                
                                if (_CabReq.CbteTipo == 3 || _CabReq.CbteTipo == 8 || _CabReq.CbteTipo == 13 ||
                                    _CabReq.CbteTipo == 2 || _CabReq.CbteTipo == 7 || _CabReq.CbteTipo == 12)
                                {
                                    List<AfipDll.wsAfipCae.CbteAsoc> _lstCA = new List<AfipDll.wsAfipCae.CbteAsoc>();
                                    //Recorro los comprobantes.
                                    foreach (AfipDll.wsAfip.ComprobanteAsoc _ca in _cbteAsoc)
                                    {
                                        AfipDll.wsAfipCae.CbteAsoc _clsCA = new AfipDll.wsAfipCae.CbteAsoc();
                                        _clsCA.CbteFch = _ca.CbteFch;
                                        _clsCA.Cuit = _ca.Cuit;
                                        _clsCA.Nro = _ca.Nro;
                                        _clsCA.PtoVta = _ca.PtoVta;
                                        _clsCA.Tipo = _ca.Tipo;
                                        _lstCA.Add(_clsCA);
                                    }
                                    CAEDetReq.CbtesAsoc = _lstCA.ToArray();
                                }

                                //Cargamos el array de detalles de FC.
                                arrCAEDetReq[0] = CAEDetReq;
                                //Cargamo la clase de los datos de las FC's.
                                CAEReq.FeCabReq = CabRequest;
                                CAEReq.FeDetReq = arrCAEDetReq;
                                //Instanciamos el Servicio.
                                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                                {
                                    if (IsTest)
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                                    else
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                                    //Consumimos el servicio y cargamos la respuesta en la clase.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), CAEReq.FeDetReq.ToString());

                                    //Escribimos el xml del Request.
                                    if (_generaXml)
                                    {
                                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeRequest.xml";
                                        System.IO.FileStream file = System.IO.File.Create(path);
                                        _Antes.Serialize(file, CAEReq);
                                        file.Close();
                                    }
                                    //Solicitamos el CAE
                                    wsAfipCae.FECAEResponse CaeResponse = _client.FECAESolicitar(Auth, CAEReq);

                                    //Escribimos el xml del Response.
                                    if (_generaXml)
                                    {
                                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEResponse));
                                        var pathD = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeResponse.xml";
                                        System.IO.FileStream fileD = System.IO.File.Create(pathD);
                                        _Despues.Serialize(fileD, CaeResponse);
                                        fileD.Close();
                                    }

                                    //Vemos si el resultado es Aprobado.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                    //Recorremos los Errores.
                                    if (CaeResponse.Errors != null)
                                    {
                                        _clsRta.Resultado = "R";
                                        for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                            {
                                                _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                            }
                                            else
                                            {
                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                            }
                                        }
                                    }
                                    if (CaeResponse.FeCabResp != null)
                                    {
                                        switch (CaeResponse.FeCabResp.Resultado)
                                        {
                                            case "A":
                                                {
                                                    _clsRta.Resultado = "A";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;

                                                    break;
                                                }
                                            case "P":
                                                {
                                                    _clsRta.Resultado = "P";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            case "R":
                                                {
                                                    _clsRta.Resultado = "R";
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Resultado: " + _clsRta.Resultado +
                                                   ". CAE: " + _clsRta.Cae + ". Fecha VTO: " + _clsRta.FechaVto);
                                    }

                                }

                                //Salimos de la Funcion.
                                return _clsRta;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj = "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                return _clsRta;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj = "El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            return _clsRta;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        return _clsRta;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    return _clsRta;
                }
            }
            catch (Exception ex)
            {
                //string _Error;
                //Recupero el nro del ultimo comprobante.
                //int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, pPathLog, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                //Recupero los datos del comprobante.
                //else
                //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "R";
                return _clsRta;
            }
        }

        public static clsCaeResponse ObtenerDatosCAE(clsCabReq _CabReq, clsDetReq _DetReq, List<clsTributos> _Trib, List<clsIVA> _Iva, 
            List<AfipDll.wsAfip.ComprobanteAsoc> _ComprobantesAsociados,
           string pCerPath, string pPathTa, string pPathLog, string pCUIT, bool IsTest, WsaaNew _wsaa, string _cbu)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + pCUIT);

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                return _clsRta;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                return _clsRta;
            }

            try
            {
                if (_wsaa.Token != null)
                {

                    if (_wsaa.Token.Length > 0)
                    {
                        if (_wsaa.Sign.Length > 0)
                        {
                            if (_wsaa.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _wsaa.Token;
                                Auth.Sign = _wsaa.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos la clase que contiene los datos de la FC.
                                wsAfipCae.FECAERequest CAEReq = new wsAfipCae.FECAERequest();
                                //Instanciamos la clase de la cabecera de los datos de la FC. y la cargamos.
                                wsAfipCae.FECAECabRequest CabRequest = new wsAfipCae.FECAECabRequest();
                                CabRequest.CantReg = _CabReq.CantReg;
                                CabRequest.CbteTipo = _CabReq.CbteTipo;
                                CabRequest.PtoVta = _CabReq.PtoVta;
                                //Instanciamos la clase de los detalles de la FC (Array).
                                wsAfipCae.FECAEDetRequest[] arrCAEDetReq = new wsAfipCae.FECAEDetRequest[1];
                                //Instanciamos la clase de los detalles de la FC que luego se cargara en el Array de detalles de FC.
                                wsAfipCae.FECAEDetRequest CAEDetReq = new wsAfipCae.FECAEDetRequest();
                                CAEDetReq.Concepto = _DetReq.Concepto;
                                CAEDetReq.DocTipo = _DetReq.DocTipo;
                                CAEDetReq.DocNro = _DetReq.DocNro;
                                CAEDetReq.CbteDesde = _DetReq.CbteDesde;
                                CAEDetReq.CbteHasta = _DetReq.CbteHasta;
                                CAEDetReq.CbteFch = _DetReq.CbteFch;
                                CAEDetReq.ImpTotal = _DetReq.ImpTotal;
                                CAEDetReq.ImpTotConc = _DetReq.ImpTotConc;
                                CAEDetReq.ImpNeto = _DetReq.ImpNeto;
                                CAEDetReq.ImpOpEx = _DetReq.ImpOpEx;
                                CAEDetReq.ImpTrib = _DetReq.ImpTrib;
                                CAEDetReq.ImpIVA = _DetReq.ImpIVA;
                                if (_DetReq.Concepto > 1)
                                {
                                    CAEDetReq.FchServDesde = _DetReq.FchServDesde;
                                    CAEDetReq.FchServHasta = _DetReq.FchServHasta;
                                    CAEDetReq.FchVtoPago = _DetReq.FchVtoPago;
                                }
                                CAEDetReq.MonId = _DetReq.MonID;
                                CAEDetReq.MonCotiz = _DetReq.MonCotiz;

                                if (_Trib.Count > 0)
                                {
                                    //Instanciamos el array de la clase de Tributos y le cargamos los datos.
                                    wsAfipCae.Tributo[] arrTrib = new wsAfipCae.Tributo[_Trib.Count];
                                    for (int i = 0; i < _Trib.Count; i++)
                                    {
                                        //Instanciamos la clase de tributo para llenar el array.
                                        wsAfipCae.Tributo Trib = new wsAfipCae.Tributo();
                                        Trib.Id = short.Parse(_Trib[i].ID.ToString());
                                        Trib.Desc = _Trib[i].Desc;
                                        Trib.BaseImp = _Trib[i].BaseImp;
                                        Trib.Alic = _Trib[i].Alic;
                                        Trib.Importe = _Trib[i].Importe;
                                        arrTrib[i] = Trib;
                                    }
                                    CAEDetReq.Tributos = arrTrib;
                                }

                                //Instanciamo el array de la clase de Alicuota de IVA.
                                if (_Iva.Count > 0)
                                {
                                    wsAfipCae.AlicIva[] arrAlic = new wsAfipCae.AlicIva[_Iva.Count];
                                    for (int x = 0; x < _Iva.Count; x++)
                                    {
                                        //Instanciamos la clase de Alicuotas de IVa para llenar el array.
                                        wsAfipCae.AlicIva Alic = new wsAfipCae.AlicIva();
                                        Alic.Id = _Iva[x].ID;
                                        Alic.BaseImp = _Iva[x].BaseImp;
                                        Alic.Importe = _Iva[x].Importe;
                                        arrAlic[x] = Alic;
                                    }
                                    CAEDetReq.Iva = arrAlic;
                                }

                                //Vemos si tenemos FActura MiPyME
                                if (_CabReq.CbteTipo == 201 || _CabReq.CbteTipo == 202 || _CabReq.CbteTipo == 203 ||
                                            _CabReq.CbteTipo == 206 || _CabReq.CbteTipo == 207 || _CabReq.CbteTipo == 208)
                                {
                                    List<wsAfipCae.Opcional> _opcio = new List<wsAfipCae.Opcional>();
                                    //CBU
                                    wsAfipCae.Opcional _op1 = new wsAfipCae.Opcional();
                                    _op1.Id = "2101";
                                    _op1.Valor = _cbu;
                                    _opcio.Add(_op1);
                                    CAEDetReq.Opcionales = _opcio.ToArray();
                                }

                                //Notas de credito
                                List<AfipDll.wsAfipCae.CbteAsoc> _lstCA = new List<AfipDll.wsAfipCae.CbteAsoc>();
                                if (_CabReq.CbteTipo == 3 || _CabReq.CbteTipo == 8 || _CabReq.CbteTipo == 13)
                                {
                                    //Recorro los comprobantes.
                                    foreach (AfipDll.wsAfip.ComprobanteAsoc _ca in _ComprobantesAsociados)
                                    {
                                        AfipDll.wsAfipCae.CbteAsoc _cls = new AfipDll.wsAfipCae.CbteAsoc();
                                        _cls.CbteFch = _ca.CbteFch;
                                        _cls.Cuit = _ca.Cuit;
                                        _cls.Nro = _ca.Nro;
                                        _cls.PtoVta = _ca.PtoVta;
                                        _cls.Tipo = _ca.Tipo;
                                        _lstCA.Add(_cls);
                                    }
                                    CAEDetReq.CbtesAsoc = _lstCA.ToArray();
                                }                                

                                //Cargamos el array de detalles de FC.
                                arrCAEDetReq[0] = CAEDetReq;
                                //Cargamo la clase de los datos de las FC's.
                                CAEReq.FeCabReq = CabRequest;
                                CAEReq.FeDetReq = arrCAEDetReq;
                                //Instanciamos el Servicio.
                                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                                {
                                    if (IsTest)
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                                    else
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                                    //Consumimos el servicio y cargamos la respuesta en la clase.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), CAEReq.FeDetReq.ToString());
                                    //Escribimos el xml del Request.
                                    System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeRequest.xml";
                                    System.IO.FileStream file = System.IO.File.Create(path);
                                    _Antes.Serialize(file, CAEReq);
                                    file.Close();
                                    //Solicitamos el CAE
                                    wsAfipCae.FECAEResponse CaeResponse = _client.FECAESolicitar(Auth, CAEReq);

                                    //Escribimos el xml del Response.
                                    System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEResponse));
                                    var pathD = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeResponse.xml";
                                    System.IO.FileStream fileD = System.IO.File.Create(pathD);
                                    _Despues.Serialize(fileD, CaeResponse);
                                    fileD.Close();

                                    //Vemos si el resultado es Aprobado.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                    //Recorremos los Errores.
                                    if (CaeResponse.Errors != null)
                                    {
                                        _clsRta.Resultado = "R";
                                        for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                            {
                                                _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                            }
                                            else
                                            {
                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                            }
                                        }
                                    }
                                    if (CaeResponse.FeCabResp != null)
                                    {
                                        switch (CaeResponse.FeCabResp.Resultado)
                                        {
                                            case "A":
                                                {
                                                    _clsRta.Resultado = "A";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;

                                                    break;
                                                }
                                            case "P":
                                                {
                                                    _clsRta.Resultado = "P";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            case "R":
                                                {
                                                    _clsRta.Resultado = "R";
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Resultado: " + _clsRta.Resultado +
                                                   ". CAE: " + _clsRta.Cae + ". Fecha VTO: " + _clsRta.FechaVto);
                                    }

                                }

                                //Salimos de la Funcion.
                                return _clsRta;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj = "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                return _clsRta;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj = "El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            return _clsRta;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        return _clsRta;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    return _clsRta;
                }
            }
            catch (Exception ex)
            {
                string _Error;
                //Recupero el nro del ultimo comprobante.
                int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, pPathLog, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                //Recupero los datos del comprobante.
                //else
                //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "R";
                return _clsRta;
            }
        }

        public static clsCaeResponse ObtenerDatosCAENew(AfipDll.wsAfipCae.FECAERequest _request,
            string pCerPath, string pPathTa, string pPathLog, string pCUIT, bool IsTest, SecureString _pass, WsaaNew _cls)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            //WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + pCUIT);

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                return _clsRta;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                return _clsRta;
            }

           try
            {
                if (_cls.Token != null)
                {
                    if (_cls.Token.Length > 0)
                    {
                        if (_cls.Sign.Length > 0)
                        {
                            if (_cls.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _cls.Token;
                                Auth.Sign = _cls.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos el Servicio.
                                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                                {
                                    if (IsTest)
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                                    else
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                                    //Consumimos el servicio y cargamos la respuesta en la clase.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                    //Escribimos el xml del Request.
                                    System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeRequest.xml";
                                    System.IO.FileStream file = System.IO.File.Create(path);
                                    _Antes.Serialize(file, _request);
                                    file.Close();
                                    //Solicitamos el CAE
                                    wsAfipCae.FECAEResponse CaeResponse = _client.FECAESolicitar(Auth, _request);

                                    //Escribimos el xml del Response.
                                    System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEResponse));
                                    var pathD = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeResponse.xml";
                                    System.IO.FileStream fileD = System.IO.File.Create(pathD);
                                    _Despues.Serialize(fileD, CaeResponse);
                                    fileD.Close();

                                    //Vemos si el resultado es Aprobado.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                    //Recorremos los Errores.
                                    if (CaeResponse.Errors != null)
                                    {
                                        _clsRta.Resultado = "R";
                                        for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            //Vemos si algúno de los errores es por tema de infraestructura. En ese caso forzamos una excepcion.
                                            if (CaeResponse.Errors[i].Code == 500 || CaeResponse.Errors[i].Code == 501 ||
                                                CaeResponse.Errors[i].Code == 502 || CaeResponse.Errors[i].Code == 600 ||
                                                CaeResponse.Errors[i].Code == 601 || CaeResponse.Errors[i].Code == 602)
                                            {
                                                throw new Exception("Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            }
                                            
                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                            {
                                                _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                            }
                                            else
                                            {
                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                            }
                                        }
                                    }
                                    if (CaeResponse.FeCabResp != null)
                                    {
                                        switch (CaeResponse.FeCabResp.Resultado)
                                        {
                                            case "A":
                                                {
                                                    _clsRta.Resultado = "A";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;

                                                    break;
                                                }
                                            case "P":
                                                {
                                                    _clsRta.Resultado = "P";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            case "R":
                                                {
                                                    _clsRta.Resultado = "R";
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Resultado: " + _clsRta.Resultado +
                                                   ". CAE: " + _clsRta.Cae + ". Fecha VTO: " + _clsRta.FechaVto);
                                    }

                                }

                                //Salimos de la Funcion.
                                return _clsRta;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj = "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                return _clsRta;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj = "El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            return _clsRta;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        return _clsRta;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    return _clsRta;
                }
            }
            catch (Exception ex)
            {
                //string _Error;
                //Recupero el nro del ultimo comprobante.
                //int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                //Recupero los datos del comprobante.
                //else
                //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "EX";
                return _clsRta;
            }
        }

        public static void ObtenerDatosCAENew(AfipDll.wsAfipCae.FECAERequest _request,
            string pCerPath, string pPathTa, string pPathLog, string pCUIT, bool IsTest, SecureString _pass, WsaaNew _cls, out clsCaeResponse _rta)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // Variable publica para los strings de respuesta.
            clsCaeResponse _clsRta = new clsCaeResponse();
            //string strRta = "";
            string strPathCert = "";
            long lngCuit;
            //WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////OBTENERDATOSCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + pCerPath);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + pCUIT);

            if (pCerPath.Trim().Length > 0)
                strPathCert = pCerPath;
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el path al certificado.";
                _clsRta.ErrorCode = "1";
                _rta = _clsRta;
                return;
            }
            if (pCUIT.Trim().Length > 0)
                lngCuit = long.Parse(pCUIT);
            else
            {
                _clsRta.ErrorMsj = "No se proporciono el CUIT.";
                _clsRta.ErrorCode = "2";
                _rta = _clsRta;
                return;
            }
            Thread.Sleep(3000);
            try
            {
                if (_cls.Token != null)
                {
                    if (_cls.Token.Length > 0)
                    {
                        if (_cls.Sign.Length > 0)
                        {
                            if (_cls.ExpirationTime > DateTime.Now)
                            {
                                //Instanciamos la clase para los datos de la autorizacion y cargamos los datos.
                                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                                Auth.Token = _cls.Token;
                                Auth.Sign = _cls.Sign;
                                Auth.Cuit = long.Parse(pCUIT);
                                //Instanciamos el Servicio.
                                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                                {
                                    if (IsTest)
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                                    else
                                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                                    //Consumimos el servicio y cargamos la respuesta en la clase.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Invocamos al Metodo FECAESolicitar()******");
                                    //LogFile.ErrorLog(LogFile.CreatePath(pPathLog), _request.FeDetReq.ToString());
                                    //Escribimos el xml del Request.
                                    System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAERequest));
                                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeRequest.xml";
                                    System.IO.FileStream file = System.IO.File.Create(path);
                                    _Antes.Serialize(file, _request);
                                    file.Close();
                                    //Solicitamos el CAE
                                    wsAfipCae.FECAEResponse CaeResponse = _client.FECAESolicitar(Auth, _request);

                                    //Escribimos el xml del Response.
                                    System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEResponse));
                                    var pathD = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ObtenerCaeResponse.xml";
                                    System.IO.FileStream fileD = System.IO.File.Create(pathD);
                                    _Despues.Serialize(fileD, CaeResponse);
                                    fileD.Close();

                                    //Vemos si el resultado es Aprobado.
                                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "******Respuesta al Metodo FECAESolicitar()1******");
                                    //Recorremos los Errores.
                                    if (CaeResponse.Errors != null)
                                    {
                                        _clsRta.Resultado = "R";
                                        for (int i = 0; i < CaeResponse.Errors.Length; i++)
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + CaeResponse.Errors[i].Code.ToString() +
                                                   ". Descripción: " + CaeResponse.Errors[i].Msg.ToString());
                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                            {
                                                _clsRta.ErrorCode = CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = CaeResponse.Errors[i].Msg.ToString();
                                            }
                                            else
                                            {
                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.Errors[i].Code.ToString();
                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.Errors[i].Msg.ToString();
                                            }
                                        }
                                    }
                                    if (CaeResponse.FeCabResp != null)
                                    {
                                        switch (CaeResponse.FeCabResp.Resultado)
                                        {
                                            case "A":
                                                {
                                                    _clsRta.Resultado = "A";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;

                                                    break;
                                                }
                                            case "P":
                                                {
                                                    _clsRta.Resultado = "P";
                                                    _clsRta.Cae = CaeResponse.FeDetResp[0].CAE;
                                                    _clsRta.FechaVto = CaeResponse.FeDetResp[0].CAEFchVto;
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                            case "R":
                                                {
                                                    _clsRta.Resultado = "R";
                                                    if (CaeResponse.FeDetResp[0].Observaciones != null)
                                                    {
                                                        for (int i = 0; i < CaeResponse.FeDetResp[0].Observaciones.Length; i++)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Observacion: " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString() +
                                                                ". Descripción: " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString());
                                                            if (String.IsNullOrEmpty(_clsRta.ErrorCode))
                                                            {
                                                                _clsRta.ErrorCode = CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                            else
                                                            {
                                                                _clsRta.ErrorCode = _clsRta.ErrorCode + " - " + CaeResponse.FeDetResp[0].Observaciones[i].Code.ToString();
                                                                _clsRta.ErrorMsj = _clsRta.ErrorMsj + " " + CaeResponse.FeDetResp[0].Observaciones[i].Msg.ToString();
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Resultado: " + _clsRta.Resultado +
                                                   ". CAE: " + _clsRta.Cae + ". Fecha VTO: " + _clsRta.FechaVto);
                                    }

                                }

                                //Salimos de la Funcion.
                                _rta = _clsRta;
                                return;
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.");
                                _clsRta.ErrorMsj = "La fecha de expiracion otorgada por el Wsaa ha caducado, se debe invocar una autorización nueva.";
                                _clsRta.ErrorCode = "5";
                                _clsRta.Resultado = "R";
                                _rta = _clsRta;
                                return;
                            }
                        }
                        else
                        {
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Sign esta vacio. No se puede invocar al Wsfev1.");
                            _clsRta.ErrorCode = "6";
                            _clsRta.ErrorMsj = "El Sign esta vacio. No se puede invocar al Wsfev1.";
                            _clsRta.Resultado = "R";
                            _rta = _clsRta;
                            return;
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El Token esta vacio. No se puede invocar al Wsfev1.");
                        _clsRta.ErrorMsj = "El Token esta vacio. No se puede invocar al Wsfev1.";
                        _clsRta.ErrorCode = "7";
                        _clsRta.Resultado = "R";
                        _rta = _clsRta;
                        return;
                    }
                }
                else
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "El servicio Wsaa no se ejecuto correctamente.");
                    _clsRta.ErrorMsj = "El servicio Wsaa no se ejecuto correctamente.";
                    _clsRta.ErrorCode = "8";
                    _clsRta.Resultado = "R";
                    _rta = _clsRta;
                    return;
                }
            }
            catch (Exception ex)
            {
                //string _Error;
                //Recupero el nro del ultimo comprobante.
                //int intNroComprobante = wsAfip.RecuperoUltimoComprobante(pCerPath, pPathTa, _CabReq.PtoVta, _CabReq.CbteTipo, pCUIT, IsTest, out _Error);
                //if(intNroComprobante == _DetReq.CbteDesde)
                //Recupero los datos del comprobante.
                //else
                //No hago nada e informo.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "****Error:" + ex.Message.ToString());
                _clsRta.ErrorMsj = ex.Message;
                _clsRta.ErrorCode = "11";
                _clsRta.Resultado = "EX";
                _rta = _clsRta;
                return;
            }
        }

        public static clsCaeRecupero ConsultaComprobante(string pPathCert, string pPathTa, string pPathLog, 
            int PtoVta, int CbteTipo, string pCuit, int CbteNro, bool IsTest)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            clsCaeRecupero _Rta = new clsCaeRecupero();
            try
            {
                string strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                Wsaa _cls = new Wsaa();
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PuntoVenta->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Nro. Cbte.->" + CbteNro);
                //validamos que tenemos el TA.xml
                if (!wsAfip.ValidoTA(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = wsAfip.ObtenerDatosWsaa(strPathCert, pPathTa, pPathLog, IsTest, pCuit);
                if (_cls.Token != null)
                {
                    wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                    Auth.Token = _cls.Token;
                    Auth.Sign = _cls.Sign;
                    Auth.Cuit = long.Parse(pCuit);
                    wsAfipCae.FECompConsultaReq Cons = new wsAfipCae.FECompConsultaReq();
                    Cons.CbteTipo = CbteTipo;
                    Cons.CbteNro = long.Parse(CbteNro.ToString());
                    Cons.PtoVta = PtoVta;

                    using(wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                        wsAfipCae.FECompConsultaResponse clsCompConsulta = _client.FECompConsultar(Auth, Cons);
                        //Vemos si tenemos errores.
                        if (clsCompConsulta.Errors != null)
                        {
                            _Rta.Resultado = "R";
                            for (int i = 0; i < clsCompConsulta.Errors.Length; i++)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + clsCompConsulta.Errors[i].Code.ToString() +
                                       ". Descripción: " + clsCompConsulta.Errors[i].Msg.ToString());
                                if (String.IsNullOrEmpty(_Rta.ErrorMsj))
                                {
                                    _Rta.ErrorMsj = clsCompConsulta.Errors[i].Msg.ToString();
                                }
                                else
                                {
                                    _Rta.ErrorMsj = _Rta.ErrorMsj + " " + clsCompConsulta.Errors[i].Msg.ToString();
                                }
                            }
                        }
                        else
                        {
                            _Rta.Cae = clsCompConsulta.ResultGet.CodAutorizacion;
                            _Rta.FechaVto = clsCompConsulta.ResultGet.FchVto;
                            _Rta.ImporteTotal = clsCompConsulta.ResultGet.ImpTotal;
                            _Rta.Resultado = "A";
                        }
                    }                    
                }
                return _Rta;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL COMPROBAR EL COMPRONATE*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");
                _Rta.Resultado = "R";
                _Rta.ErrorMsj = ex.Message;
                return _Rta;
            }
        }

        public static clsCaeRecupero ConsultaComprobanteNew(string pPathCert, string pPathTa, string pPathLog, 
            int PtoVta, int CbteTipo, string pCuit, int CbteNro, bool IsTest, SecureString _pass)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            clsCaeRecupero _Rta = new clsCaeRecupero();
            try
            {
                string strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                WsaaNew _cls = new WsaaNew();
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PuntoVenta->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Nro. Cbte.->" + CbteNro);
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                    Auth.Token = _cls.Token;
                    Auth.Sign = _cls.Sign;
                    Auth.Cuit = long.Parse(pCuit);
                    wsAfipCae.FECompConsultaReq Cons = new wsAfipCae.FECompConsultaReq();
                    Cons.CbteTipo = CbteTipo;
                    Cons.CbteNro = long.Parse(CbteNro.ToString());
                    Cons.PtoVta = PtoVta;

                    using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                        wsAfipCae.FECompConsultaResponse clsCompConsulta = _client.FECompConsultar(Auth, Cons);
                        //Vemos si tenemos errores.
                        if (clsCompConsulta.Errors != null)
                        {
                            _Rta.Resultado = "R";
                            for (int i = 0; i < clsCompConsulta.Errors.Length; i++)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + clsCompConsulta.Errors[i].Code.ToString() +
                                       ". Descripción: " + clsCompConsulta.Errors[i].Msg.ToString());
                                if (String.IsNullOrEmpty(_Rta.ErrorMsj))
                                {
                                    _Rta.ErrorMsj = clsCompConsulta.Errors[i].Msg.ToString();
                                }
                                else
                                {
                                    _Rta.ErrorMsj = _Rta.ErrorMsj + " " + clsCompConsulta.Errors[i].Msg.ToString();
                                }
                            }
                        }
                        else
                        {
                            _Rta.Cae = clsCompConsulta.ResultGet.CodAutorizacion;
                            _Rta.FechaVto = clsCompConsulta.ResultGet.FchVto;
                            _Rta.ImporteTotal = clsCompConsulta.ResultGet.ImpTotal;
                            _Rta.Resultado = "A";
                        }
                    }
                }
                return _Rta;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL COMPROBAR EL COMPRONATE*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");
                _Rta.Resultado = "R";
                _Rta.ErrorMsj = ex.Message;
                return _Rta;
            }
        }

        public static string ConsultaComprobante2New(string pPathCert, string pPathTa, string pPathLog,
            int PtoVta, int CbteTipo, string pCuit, int CbteNro, bool IsTest, SecureString _pass)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string _Rta = "";
            try
            {
                string strPathCert = pPathCert;
                long lngCuit = long.Parse(pCuit);
                WsaaNew _cls = new WsaaNew();
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////PuntoVenta->" + PtoVta);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CBTETIPO->" + CbteTipo);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Nro. Cbte.->" + CbteNro);
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                    Auth.Token = _cls.Token;
                    Auth.Sign = _cls.Sign;
                    Auth.Cuit = long.Parse(pCuit);
                    wsAfipCae.FECompConsultaReq Cons = new wsAfipCae.FECompConsultaReq();
                    Cons.CbteTipo = CbteTipo;
                    Cons.CbteNro = long.Parse(CbteNro.ToString());
                    Cons.PtoVta = PtoVta;

                    using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                        wsAfipCae.FECompConsultaResponse clsCompConsulta = _client.FECompConsultar(Auth, Cons);
                        //Vemos si tenemos errores.
                        if (clsCompConsulta.Errors != null)
                        {
                            _Rta = "Rechazado";
                            for (int i = 0; i < clsCompConsulta.Errors.Length; i++)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error: " + clsCompConsulta.Errors[i].Code.ToString() +
                                       ". Descripción: " + clsCompConsulta.Errors[i].Msg.ToString());
                                _Rta = _Rta + Environment.NewLine + "Error: " + clsCompConsulta.Errors[i].Msg.ToString();

                            }
                        }
                        else
                        {
                            _Rta = "CAE: " + clsCompConsulta.ResultGet.CodAutorizacion + Environment.NewLine +
                       "Fecha: " + clsCompConsulta.ResultGet.CbteFch + Environment.NewLine +
                       "Tipo Comprobante: " + clsCompConsulta.ResultGet.CbteTipo.ToString() + Environment.NewLine +
                       "Concepto: " + clsCompConsulta.ResultGet.Concepto + Environment.NewLine +
                       "Tipo Documento: " + clsCompConsulta.ResultGet.DocTipo + Environment.NewLine +
                       "Nro Documento: " + clsCompConsulta.ResultGet.DocNro + Environment.NewLine +
                       "Fecha Proceso: " + clsCompConsulta.ResultGet.FchProceso + Environment.NewLine +
                       "Importe IVA: " + clsCompConsulta.ResultGet.ImpIVA.ToString() + Environment.NewLine +
                       "Importe Neto: " + clsCompConsulta.ResultGet.ImpNeto.ToString() + Environment.NewLine +
                       "Importe Operaciones Exentas: " + clsCompConsulta.ResultGet.ImpOpEx.ToString() + Environment.NewLine +
                       "Importe Total: " + clsCompConsulta.ResultGet.ImpTotal.ToString() + Environment.NewLine +
                       "Importe Otros Tributos: " + clsCompConsulta.ResultGet.ImpTrib.ToString() + Environment.NewLine +
                       "Punto de Venta: " + clsCompConsulta.ResultGet.PtoVta.ToString();
                            if (clsCompConsulta.ResultGet.Iva != null)
                            {
                                for (int i = 0; i < clsCompConsulta.ResultGet.Iva.Length; i++)
                                {
                                    _Rta = _Rta + Environment.NewLine +
                                        "IVA ID: " + clsCompConsulta.ResultGet.Iva[i].Id.ToString() + Environment.NewLine +
                                        "IVA Base imponible: " + clsCompConsulta.ResultGet.Iva[i].BaseImp.ToString() + Environment.NewLine +
                                        "IVA Importe : " + clsCompConsulta.ResultGet.Iva[i].Importe.ToString();
                                }
                            }
                            if(clsCompConsulta.ResultGet.Tributos != null)
                            { 
                                if (clsCompConsulta.ResultGet.Tributos != null)
                                {
                                    for (int i = 0; i < clsCompConsulta.ResultGet.Tributos.Length; i++)
                                    {
                                        _Rta = _Rta + Environment.NewLine +
                                            "Tributo ID: " + clsCompConsulta.ResultGet.Tributos[i].Id.ToString() + Environment.NewLine +
                                            "Tributo Descripcion : " + clsCompConsulta.ResultGet.Tributos[i].Desc + Environment.NewLine +
                                            "Tributo Base imponible: " + clsCompConsulta.ResultGet.Tributos[i].BaseImp.ToString() + Environment.NewLine +
                                            "Tributo Importe : " + clsCompConsulta.ResultGet.Tributos[i].Importe.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                return _Rta;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****ERROR AL COMPROBAR EL COMPRONATE*******");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "*****" + ex.Message + "*******");
                _Rta = ex.Message;
                return _Rta;
            }
        }

        public static List<PtoVenta> ConsultarPuntosVenta(string pPathCert, string pPathTa, string pPathLog, string pCuit, bool IsTest)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            Wsaa _cls = new Wsaa();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!wsAfip.ValidoTA(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = wsAfip.ObtenerDatosWsaa(strPathCert, pPathTa, pPathLog, IsTest, pCuit);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    wsAfipCae.FEPtoVentaResponse _rta = _client.FEParamGetPtosVenta(Auth);

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.PtoVenta[] _ptovta = _rta.ResultGet;
                        for (int i = 0; i < _ptovta.Length; i++)
                        {
                            PtoVenta _pto = new PtoVenta();
                            _pto.Bloqueado = _ptovta[i].Bloqueado;
                            _pto.EmisionTipo = _ptovta[i].EmisionTipo;
                            _pto.FechaBaja = _ptovta[i].FchBaja;
                            _pto.Nro = _ptovta[i].Nro;

                            _lst.Add(_pto);
                        }
                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
                return _lst;
        }

        public static List<PtoVenta> ConsultarPuntosVentaNew(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores )
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                if (_generaXML)
                {
                    //Escribimos el xml del Request.
                    System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                    var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                    System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                    _Auth.Serialize(fileAuth, Auth);
                    fileAuth.Close();
                }
                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasRequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, Auth);
                        file.Close();
                    }

                    wsAfipCae.FEPtoVentaResponse _rta = _client.FEParamGetPtosVenta(Auth);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Response.
                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEPtoVentaResponse));
                        var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasResponse.xml";
                        System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                        _Despues.Serialize(fileDes, _rta);
                        fileDes.Close();
                    }

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.PtoVenta[] _ptovta = _rta.ResultGet;
                        for (int i = 0; i < _ptovta.Length; i++)
                        {
                            PtoVenta _pto = new PtoVenta();
                            _pto.Bloqueado = _ptovta[i].Bloqueado;
                            _pto.EmisionTipo = _ptovta[i].EmisionTipo;
                            _pto.FechaBaja = _ptovta[i].FchBaja;
                            _pto.Nro = _ptovta[i].Nro;

                            _lst.Add(_pto);
                        }
                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _lst;
        }

        public static List<PtoVenta> ConsultarTiposComprobantes(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                if (_generaXML)
                {
                    //Escribimos el xml del Request.
                    System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                    var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                    System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                    _Auth.Serialize(fileAuth, Auth);
                    fileAuth.Close();
                }
                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasRequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, Auth);
                        file.Close();
                    }

                    wsAfipCae.CbteTipoResponse _rta = _client.FEParamGetTiposCbte(Auth);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Response.
                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEPtoVentaResponse));
                        var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasResponse.xml";
                        System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                        _Despues.Serialize(fileDes, _rta);
                        fileDes.Close();
                    }

                    //if (_rta.Errors == null)
                    //{
                    //    wsAfipCae.PtoVenta[] _ptovta = _rta.ResultGet;
                    //    for (int i = 0; i < _ptovta.Length; i++)
                    //    {
                    //        PtoVenta _pto = new PtoVenta();
                    //        _pto.Bloqueado = _ptovta[i].Bloqueado;
                    //        _pto.EmisionTipo = _ptovta[i].EmisionTipo;
                    //        _pto.FechaBaja = _ptovta[i].FchBaja;
                    //        _pto.Nro = _ptovta[i].Nro;

                    //        _lst.Add(_pto);
                    //    }
                    //}
                    //else
                    //{
                    //    wsAfipCae.Err[] _err = _rta.Errors;
                    //    for (int i = 0; i < _err.Length; i++)
                    //    {
                    //        Errors _er = new Errors();
                    //        _er.Code = _err[i].Code;
                    //        _er.Msg = _err[i].Msg;

                    //        _errors.Add(_er);
                    //    }
                    //}
                }
            }
            _errores = _errors;
            return _lst;
        }

        #region CAEA
        public static CaeaSolicitar SolicitarCAEANew(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, 
            int _periodo, short _orden, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            CaeaSolicitar _clsCAEA= new CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\\SolicitarCAEARequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, Auth);
                        file.Close();
                    }

                    wsAfipCae.FECAEAGetResponse _rta = _client.FECAEASolicitar(Auth, _periodo, _orden );

                    if (_generaXML)
                    {
                        //Escribimos el xml del Response.
                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEAGetResponse));
                        var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\SolicitarCAEResponse.xml";
                        System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                        _Despues.Serialize(fileDes, _rta);
                        fileDes.Close();
                    }

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.FECAEAGet _caea = _rta.ResultGet;

                        _clsCAEA.CAEA = _caea.CAEA;
                        _clsCAEA.FchProceso = _caea.FchProceso;
                        _clsCAEA.FchVigDesde = _caea.FchVigDesde;
                        _clsCAEA.FchVigHasta = _caea.FchVigHasta;
                        _clsCAEA.Orden = _caea.Orden;
                        _clsCAEA.Periodo = _caea.Periodo;
                        _clsCAEA.FchTopeInf = _caea.FchTopeInf;
                        
                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _clsCAEA;
        }

        public static CaeaSolicitar ConsultarCAEANew(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, int _periodo, short _orden, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            CaeaSolicitar _clsCAEA = new CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    wsAfipCae.FECAEAGetResponse _rta = _client.FECAEAConsultar(Auth, _periodo, _orden);

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.FECAEAGet _caea = _rta.ResultGet;

                        _clsCAEA.CAEA = _caea.CAEA;
                        _clsCAEA.FchProceso = _caea.FchProceso;
                        _clsCAEA.FchVigDesde = _caea.FchVigDesde;
                        _clsCAEA.FchVigHasta = _caea.FchVigHasta;
                        _clsCAEA.Orden = _caea.Orden;
                        _clsCAEA.Periodo = _caea.Periodo;
                        _clsCAEA.FchTopeInf = _caea.FchTopeInf;

                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _clsCAEA;
        }

        public static List<CaeaResultadoInformar> InformarMovimientosCAEANew(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, wsAfipCae.FECAEARequest _req, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<CaeaResultadoInformar> _clsCAEA = new List<CaeaResultadoInformar>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEARequest));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\InformarCAEARequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, _req);
                        file.Close();
                    }

                    wsAfipCae.FECAEAResponse _rta = _client.FECAEARegInformativo(Auth, _req);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Response.
                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FECAEAResponse));
                        var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\InformarCAEResponse.xml";
                        System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                        _Despues.Serialize(fileDes, _rta);
                        fileDes.Close();
                    }

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.FECAEADetResponse[] _det = _rta.FeDetResp;
                        for (int i = 0; i < _det.Length; i++)
                        {
                            CaeaResultadoInformar _er = new CaeaResultadoInformar();
                            _er.Caea = _det[i].CAEA;
                            _er.CbteDesde = Convert.ToInt64(_det[i].CbteDesde);
                            _er.DocNro = Convert.ToInt64(_det[i].DocNro);
                            _er.DocTipo = _det[i].DocTipo;
                            _er.Resultado = _det[i].Resultado;

                            wsAfipCae.Obs[] _obs = _det[i].Observaciones;
                            if (_obs != null)
                            {
                                for (int x = 0; x < _obs.Length; x++)
                                {
                                    _er.Observaciones = _er.Observaciones + _obs[x].Code + "|" + _obs[x].Msg;
                                }
                            }

                            _clsCAEA.Add(_er);
                        }
                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _clsCAEA;
        }

        #endregion

        public static List<wsAfipCae.OpcionalTipo> ConsultarOpcionales(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Errors> _errors = new List<Errors>();
            List<wsAfipCae.OpcionalTipo> _lst = new List<wsAfipCae.OpcionalTipo>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    //if (_generaXML)
                    //{
                    //    //Escribimos el xml del Request.
                    //    System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                    //    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasRequest.xml";
                    //    System.IO.FileStream file = System.IO.File.Create(path);
                    //    _Antes.Serialize(file, Auth);
                    //    file.Close();
                    //}

                    wsAfipCae.OpcionalTipoResponse _rta = _client.FEParamGetTiposOpcional(Auth);

                    //if (_generaXML)
                    //{
                    //    //Escribimos el xml del Response.
                    //    System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEPtoVentaResponse));
                    //    var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\PtosVtasResponse.xml";
                    //    System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                    //    _Despues.Serialize(fileDes, _rta);
                    //    fileDes.Close();
                    //}

                    if (_rta.Errors == null)
                    {
                        wsAfipCae.OpcionalTipo[] _ptovta = _rta.ResultGet;
                        for (int i = 0; i < _ptovta.Length; i++)
                        {
                            wsAfipCae.OpcionalTipo ot = new wsAfipCae.OpcionalTipo();
                            ot.Desc = _ptovta[i].Desc;
                            ot.ExtensionData = _ptovta[i].ExtensionData;
                            ot.FchDesde = _ptovta[i].FchDesde;
                            ot.FchHasta = _ptovta[i].FchHasta;
                            ot.Id = _ptovta[i].Id;
                            
                            _lst.Add(ot);
                        }
                    }
                    else
                    {
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }

            _errores = _errors;

            return _lst;
        }

        public static string TestDummy(string pPathLog, bool IsTest)
        {

            string _rta;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    wsAfipCae.DummyResponse _call = _client.FEDummy();

                    if (_call.DbServer.ToUpper() != "OK" || _call.AuthServer.ToUpper() != "OK" || _call.AppServer.ToUpper() != "OK")
                        _rta = "BAD";
                    else
                        _rta = "OK";

                }
                return _rta;
            }
            catch(Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "TestDummy - Error: " + ex.Message);
                return "BAD";
            }
        }

        public static string InformarCAEASinMovimiento(string pPathCert, string pPathTa, string pPathLog,
           string pCuit, bool IsTest, SecureString _pass, int _ptovta, string _caea, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Errors> _errors = new List<Errors>();
            string _respuesta = "";

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////InformarCAEASinMovimiento////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsAfipCae.FEAuthRequest Auth = new wsAfipCae.FEAuthRequest();
                Auth.Token = _cls.Token;
                Auth.Sign = _cls.Sign;
                Auth.Cuit = long.Parse(pCuit);

                using (wsAfipCae.ServiceSoapClient _client = new wsAfipCae.ServiceSoapClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWSFEV1);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(DEFAULT_URLWSFEV1);

                    wsAfipCae.FECAEASinMovResponse _rta = _client.FECAEASinMovimientoInformar(Auth, _ptovta, _caea);

                    if (_rta.Errors == null)
                    {
                        _respuesta = _rta.Resultado;
                    }
                    else
                    {
                        _respuesta = _rta.Resultado;
                        wsAfipCae.Err[] _err = _rta.Errors;
                        for (int i = 0; i < _err.Length; i++)
                        {
                            Errors _er = new Errors();
                            _er.Code = _err[i].Code;
                            _er.Msg = _err[i].Msg;

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _respuesta;
        }
    }


}
