﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace AfipDll
{
    public class WsMTXCA
    {
        /// <summary>
        /// URL servicio FE de Test.
        /// </summary>
        const string TEST_URLWS = "https://fwshomo.afip.gov.ar/wsmtxca/services/MTXCAService?wsdl";
        /// <summary>
        /// URL servicio FE de Prooduccion.
        /// </summary>
        const string PROD_URLWS = "https://serviciosjava.afip.gob.ar/wsmtxca/services/MTXCAService?wsdl";

        const string DEFAULT_SERVICIO = "wsmtxca";

        public class Codigos
        {
            public int codigo { get; set; }
            public string descripcion { get; set; }
        }

        public class Dummy
        {
            public string Autorizaciones { get; set; }
            public string Server { get; set; }
            public string BaseDatos { get; set; }
        }
        public class PtoVenta
        {
            public int Nro { get; set; }
            public string EmisionTipo { get; set; }
            public string Bloqueado { get; set; }
            public string FechaBaja { get; set; }
        }

        public class Errors
        {
            public int Code { get; set; }
            public string Msg { get; set; }
        }

        public class Comprobante
        {
            public Int16 tipoComprobante { get; set; }
            public Int64 numeroComprobante { get; set; }
            public int ptoVta { get; set; }
            public DateTime fechaEmision { get; set; }
            public Int16 tipoDocumento { get; set; }
            public Int64 numeroDocumento { get; set; }
            public decimal importeGravado { get; set; }
            public decimal importeNoGravado { get; set; }
            public decimal importeExento { get; set; }
            public string NcRechazo { get; set; }
            public decimal importeSubTotal { get; set; }
            public decimal importeTotal { get; set; }
            public string codigoMoneda { get; set; }
            public decimal cotizacionMoneda{ get; set; }
            public string obervaciones { get; set; }
            public Int16 concepto { get; set; }
            public decimal importeTributos { get; set; }
            public DateTime fechaDesde { get; set; }
            public DateTime fechaHasta { get; set; }
            public DateTime fechaPago { get; set; }
            public DateTime fechaVto { get; set; }
            public string cbu { get; set; }
            public Int16 tipoComprobanteAsoc { get; set; }
            public string numeroComprobanteAsoc { get; set; }
            public string cuit { get; set; }

        }

        public class CaeaSolicitar
        {
            public string Resultado { get; set; }
            public string CAEA { get; set; }
            public string FchProceso { get; set; }
            public string Descripcion { get; set; }
        }

        public class ConsultaComprobanteResponse
        {
            public long numeroCbte { get; set; }
            public int puntoVenta { get; set; }
            public short tipoComprobante { get; set; }
            public DateTime fechaEmision { get; set; }
            public string tipoAutorizacion { get; set; }
            public long codigoAutorizacion { get; set; }
            public int tipoDocumento { get; set; }
            public long numeroDocumento { get; set; }
            public decimal importeTotal { get; set; }
            public string observaciones { get; set; }
            public DateTime fechaVencimiento { get; set; }
            public string codigoMoneda { get; set; }
            public decimal cotizacionMoneda { get; set; }

        }

        public class ObternerCaeResponse
        {
            public string respuesta { get; set; }
            public DateTime fechaVto { get; set; }
            public DateTime fechaEmision { get; set; }
            public long cae { get; set; }
            public long numeroCbte { get; set; }
            public int tipoCbte { get; set; }
            public int puntoVta { get; set; }
            public string observacion { get; set; }
            public string error { get; set; }

        }

        public static List<PtoVenta> ConsultarPuntosVenta(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Consulta Puntos de venta////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.AuthRequestType));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();

                        wsMtxca.PuntoVentaType[] _rta = _client.consultarPuntosVenta(Auth, out _desType);

                        if (_rta.Count() > 0)
                        {
                            PtoVenta[] _ptovta = new PtoVenta[_rta.Count()];
                            for (int i = 0; i < _ptovta.Length; i++)
                            {
                                PtoVenta _pto = new PtoVenta();
                                _pto.Bloqueado = _rta[i].bloqueado.ToString();
                                //_pto.EmisionTipo = _rta[i]..EmisionTipo;
                                //_pto.FechaBaja = _rta[i].fechaBaja;
                                _pto.Nro = _rta[i].numeroPuntoVenta;

                                _lst.Add(_pto);
                            }
                        }
                    }

                }
                _errores = _errors;
                return _lst;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<Codigos> ConsultarUnidadesMedida(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Codigos> _lst = new List<Codigos>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTARUNIDADESMEDIDA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.AuthRequestType));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();

                        wsMtxca.CodigoDescripcionType[] _rta = _client.consultarUnidadesMedida(Auth, out _desType);

                        if (_generaXML)
                        {
                            //Escribimos el xml del Request.
                            System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.CodigoDescripcionType[]));
                            var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\MedidasResponse.xml";
                            System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                            _Auth.Serialize(fileAuth, _rta);
                            fileAuth.Close();
                        }

                        if (_rta.Count() > 0)
                        {
                            Codigos[] _ptovta = new Codigos[_rta.Count()];
                            for (int i = 0; i < _ptovta.Length; i++)
                            {
                                Codigos _pto = new Codigos();
                                _pto.codigo = _rta[i].codigo;
                                _pto.descripcion = _rta[i].descripcion;
                                
                                _lst.Add(_pto);
                            }
                        }
                    }

                }
                _errores = _errors;
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<Codigos> ConsultarTiposComprobantes(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Codigos> _lst = new List<Codigos>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTARTIPOCOMPROBANTES////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.AuthRequestType));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();

                        //wsMtxca.CodigoDescripcionType[] _rta = _client.consultarTiposComprobante(Auth, out _desType);
                        var _rta = _client.consultarTiposComprobante(Auth, out _desType);

                        if (_generaXML)
                        {
                            //Escribimos el xml del Request.
                            System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.CodigoDescripcionType[]));
                            var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\TiposDocReceptor.xml";
                            System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                            _Auth.Serialize(fileAuth, _rta);
                            fileAuth.Close();
                        }

                        if (_rta.Count() > 0)
                        {
                            Codigos[] _ptovta = new Codigos[_rta.Count()];
                            for (int i = 0; i < _ptovta.Length; i++)
                            {
                                Codigos _pto = new Codigos();
                                _pto.codigo = _rta[i].codigo;
                                _pto.descripcion = _rta[i].descripcion;

                                _lst.Add(_pto);
                            }
                        }
                    }

                }
                _errores = _errors;
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<PtoVenta> ConsultarPuntosVentaCAE(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();

                        //wsMtxca.PuntoVentaType[] _rta = _client.consultarPuntosVenta(Auth, out _desType);

                        wsMtxca.PuntoVentaType[] _rta = _client.consultarPuntosVentaCAE(Auth, out _desType);

                        if (_rta.Count() > 0)
                        {
                            PtoVenta[] _ptovta = new PtoVenta[_rta.Count()];
                            for (int i = 0; i < _ptovta.Length; i++)
                            {
                                PtoVenta _pto = new PtoVenta();
                                _pto.Bloqueado = _rta[i].bloqueado.ToString();
                                //_pto.EmisionTipo = _rta[i]..EmisionTipo;
                                //_pto.FechaBaja = _rta[i].FchBaja;
                                _pto.Nro = _rta[i].numeroPuntoVenta;

                                _lst.Add(_pto);
                            }
                        }
                    }

                }
                _errores = _errors;
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<PtoVenta> ConsultarPuntosVentaCAEA(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<PtoVenta> _lst = new List<PtoVenta>();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CONSULTACOMPROBANTE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsAfipCae.FEAuthRequest));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();

                        wsMtxca.PuntoVentaType[] _rta = _client.consultarPuntosVentaCAEA(Auth, out _desType);

                        if (_rta.Count() > 0)
                        {
                            PtoVenta[] _ptovta = new PtoVenta[_rta.Count()];
                            for (int i = 0; i < _ptovta.Length; i++)
                            {
                                PtoVenta _pto = new PtoVenta();
                                _pto.Bloqueado = _rta[i].bloqueado.ToString();
                                //_pto.EmisionTipo = _rta[i]..EmisionTipo;
                                //_pto.FechaBaja = _rta[i].fechaBaja;
                                _pto.Nro = _rta[i].numeroPuntoVenta;

                                _lst.Add(_pto);
                            }
                        }
                    }

                }
                _errores = _errors;
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static Dummy TestDummy(string pPathLog, bool IsTest)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            Dummy _dum = new Dummy();
            try
            {
                using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                    string _server = "";
                    string _dbserver = "";

                    string _call = _client.dummy(out _server, out _dbserver);

                    _dum.Autorizaciones = _call;
                    _dum.Server = _server;
                    _dum.BaseDatos = _dbserver;
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al ejecutar el Dummy. Error: " + ex.Message);
                _dum.Autorizaciones = "False";
                _dum.Server = "False";
                _dum.BaseDatos = "False";
            }
            return _dum;
        }

        public static long RecuperoUltimoComprobante(string pPathCert, string pPathTa, string pPathLog, int PtoVta, int CbteTipo,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            
            List<Errors> _errors = new List<Errors>();
            Int64 _rta = -1;

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit.Replace("-",""));
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Recupero Ultimo comprobante////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Punto de venta->" + PtoVta.ToString() + " TipoComprobante->" + CbteTipo.ToString());            
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit.Replace("-", ""));

                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Auth = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.AuthRequestType));
                        var pathAuth = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\AuthRequest.xml";
                        System.IO.FileStream fileAuth = System.IO.File.Create(pathAuth);
                        _Auth.Serialize(fileAuth, Auth);
                        fileAuth.Close();
                    }
                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType _desType = new wsMtxca.CodigoDescripcionType();
                        wsMtxca.CodigoDescripcionType[] _desError;
                        wsMtxca.ConsultaUltimoComprobanteAutorizadoRequestType _reqUltimoAutorizado = new wsMtxca.ConsultaUltimoComprobanteAutorizadoRequestType();
                        _reqUltimoAutorizado.codigoTipoComprobante = Convert.ToInt16(CbteTipo);
                        _reqUltimoAutorizado.numeroPuntoVenta = PtoVta;

                        _rta = _client.consultarUltimoComprobanteAutorizado(Auth, _reqUltimoAutorizado, out _desError, out _desType);
                    }
                }
                else
                {
                    throw new Exception("No tenemos Token del WASA");
                }
                _errores = _errors;
                return _rta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void ObtenerCAE(string pPathCert, string pPathTa, string pPathLog, int PtoVta, int CbteTipo,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML,
            List<AfipDll.wsAfip.clsIVA> _lstIva,
            List<AfipDll.wsMtxca.ItemType> _lstItems,
            List<AfipDll.wsAfip.clsTributos> _lstTributos,            
            Comprobante _compro,
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lstCompAsoc,
            string _tipoTransMiPyme,
            out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            
            List<Errors> _errors = new List<Errors>();
            //Int64 _rta = 0;

            string strPathCert = pPathCert;
            string _cuit = pCuit.Replace("-", "");
            long lngCuit = long.Parse(pCuit.Replace("-",""));
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////ObtenerCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Registro ICG->" + _compro.obervaciones);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, _cuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, _cuit, _pass);
                if (_cls.Token != null)
                {                   
                    //Instanciamos las clases de MTXCA.
                    wsMtxca.ComprobanteType ComprobanteCAERequest = new wsMtxca.ComprobanteType();
                    wsMtxca.ComprobanteCAEResponseType comprobanteResponse = new wsMtxca.ComprobanteCAEResponseType();

                    //Instanciamos el array de los items.
                    List<wsMtxca.ItemType> arrayOfItems = new List<wsMtxca.ItemType>();
                    //Cargamos los detalles en el array.
                    foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                    {
                        wsMtxca.ItemType item = new wsMtxca.ItemType();
                        item.unidadesMtx = _it.unidadesMtx;
                        item.unidadesMtxSpecified = true;
                        item.codigoMtx = _it.codigoMtx.ToString();
                        item.codigo = _it.codigo;
                        item.descripcion = _it.descripcion;
                        item.cantidad = _it.cantidad;
                        item.cantidadSpecified = true;
                        item.codigoUnidadMedida = Convert.ToInt16(_it.codigoUnidadMedida);
                        //Vemos si es factura A
                        if (_compro.tipoComprobante == 1 || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 3
                            || _compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203)
                        {
                            item.precioUnitario = Math.Round(_it.precioUnitario, 3);
                            item.precioUnitarioSpecified = true;
                            item.importeIVA = Math.Round(Convert.ToDecimal(_it.importeIVA), 2);
                            item.importeIVASpecified = true;
                        }
                        //Vemos si es factura B
                        if (_compro.tipoComprobante == 6 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 8
                            || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208)
                        {
                            item.precioUnitario = Math.Round(Convert.ToDecimal(_it.precioUnitario), 3);
                            item.precioUnitarioSpecified = true;
                            ////
                            //item.importeIVA = Math.Round(Convert.ToDecimal(_it.importeIVA), 2);
                            //item.importeIVASpecified = true;
                        }
                        item.importeBonificacion = _it.importeBonificacion;
                        item.importeBonificacionSpecified = true;
                        item.codigoCondicionIVA = _it.codigoCondicionIVA;
                        item.importeItem = Math.Round(_it.importeItem, 2);
                        arrayOfItems.Add(item);
                    }

                    //IVA
                    List<AfipDll.wsMtxca.SubtotalIVAType> _alicIVA = new List<AfipDll.wsMtxca.SubtotalIVAType>();
                    if (_compro.importeTotal == 0)
                    {
                        foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                        {
                            //AfipDll.wsMtxca.SubtotalIVAType _loTengo = _alicIVA.Any(x => x.codigo == _it.codigoCondicionIVA);

                            if (!_alicIVA.Any(x => x.codigo == _it.codigoCondicionIVA))
                            {
                                AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                                _al.codigo = _it.codigoCondicionIVA;
                                _al.importe = 0;
                                _alicIVA.Add(_al);
                            }
                        }
                    }
                    else
                    {
                        foreach (AfipDll.wsAfip.clsIVA _iva in _lstIva)
                        {
                            //Solo enviamos los IVA 4 ,5 o 6
                            if (_iva.ID == 4 || _iva.ID == 5 || _iva.ID == 6)
                            {
                                AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                                _al.codigo = Convert.ToInt16(_iva.ID);
                                _al.importe = Convert.ToDecimal(_iva.Importe);
                                _alicIVA.Add(_al);
                            }
                        }
                    }
                    wsMtxca.SubtotalIVAType[] arrayOfSubtotalesIVA = _alicIVA.ToArray();
                    //Tributos
                    List<AfipDll.wsMtxca.OtroTributoType> _tributos = new List<AfipDll.wsMtxca.OtroTributoType>();
                    foreach (AfipDll.wsAfip.clsTributos _tr in _lstTributos)
                    {
                        AfipDll.wsMtxca.OtroTributoType _t = new AfipDll.wsMtxca.OtroTributoType();
                        _t.codigo = Convert.ToInt16(_tr.ID);
                        _t.baseImponible = Convert.ToDecimal(_tr.BaseImp);
                        _t.descripcion = _tr.Desc;
                        _t.importe = Convert.ToDecimal(_tr.Importe);
                        _tributos.Add(_t);
                    }
                    wsMtxca.OtroTributoType[] arrayOfTributos = _tributos.ToArray();

                    //CArgamos el request a enviar.
                    ComprobanteCAERequest.codigoTipoComprobante = _compro.tipoComprobante;
                    ComprobanteCAERequest.numeroPuntoVenta = _compro.ptoVta;
                    ComprobanteCAERequest.numeroComprobante = _compro.numeroComprobante;
                    ComprobanteCAERequest.fechaEmision = _compro.fechaEmision;
                    ComprobanteCAERequest.fechaEmisionSpecified = true;
                    ComprobanteCAERequest.codigoTipoDocumento = _compro.tipoDocumento;
                    ComprobanteCAERequest.codigoTipoDocumentoSpecified = true;
                    ComprobanteCAERequest.numeroDocumento = _compro.numeroDocumento;
                    ComprobanteCAERequest.numeroDocumentoSpecified = true;
                    ComprobanteCAERequest.importeGravado = _compro.importeGravado;
                    ComprobanteCAERequest.importeGravadoSpecified = true;
                    ComprobanteCAERequest.importeNoGravado = _compro.importeNoGravado;
                    ComprobanteCAERequest.importeNoGravadoSpecified = true;
                    ComprobanteCAERequest.importeExento = _compro.importeExento;
                    ComprobanteCAERequest.importeSubtotal = _compro.importeSubTotal;
                    ComprobanteCAERequest.importeTotal = _compro.importeTotal;
                    ComprobanteCAERequest.codigoMoneda = _compro.codigoMoneda;
                    ComprobanteCAERequest.cotizacionMoneda = _compro.cotizacionMoneda;
                    ComprobanteCAERequest.observaciones = _compro.obervaciones;
                    ComprobanteCAERequest.codigoConcepto = _compro.concepto;
                    if (_compro.concepto == 3 || _compro.concepto == 2)
                    {
                        ComprobanteCAERequest.fechaServicioDesde = _compro.fechaDesde;
                        ComprobanteCAERequest.fechaServicioDesdeSpecified = true;
                        ComprobanteCAERequest.fechaServicioHasta = _compro.fechaHasta;
                        ComprobanteCAERequest.fechaServicioHastaSpecified = true;
                        ComprobanteCAERequest.fechaVencimientoPago = _compro.fechaPago;
                        ComprobanteCAERequest.fechaVencimientoPagoSpecified = true;
                        ComprobanteCAERequest.fechaVencimiento = _compro.fechaVto;
                    }

                    if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203
                           || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208
                           || _compro.tipoComprobante == 211 || _compro.tipoComprobante == 212 || _compro.tipoComprobante == 213
                           || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                           || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                    {
                        List<wsMtxca.DatoAdicionalType> _lstAdd = new List<wsMtxca.DatoAdicionalType>();
                        if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 211)
                        {
                            ComprobanteCAERequest.fechaVencimientoPago = _compro.fechaPago;
                            ComprobanteCAERequest.fechaVencimientoPagoSpecified = true;
                            //CBU                                
                            wsMtxca.DatoAdicionalType _ad1 = new wsMtxca.DatoAdicionalType();
                            _ad1.t = 21;
                            _ad1.c1 = _compro.cbu;
                            _lstAdd.Add(_ad1);

                            //Tipo Transferencia.                                
                            wsMtxca.DatoAdicionalType _ad2 = new wsMtxca.DatoAdicionalType();
                            _ad1.t = 27;
                            _ad1.c1 =_tipoTransMiPyme;
                            _lstAdd.Add(_ad1);
                        }

                        if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                    || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212
                                    || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                                    || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                        {
                            ComprobanteCAERequest.arrayComprobantesAsociados = _lstCompAsoc.ToArray(); 

                            if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                    || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212)
                            {
                                //Cargo el opcional como rechazado para sacar la NC.
                                wsMtxca.DatoAdicionalType _opNcRech = new wsMtxca.DatoAdicionalType();
                                _opNcRech.t = 22;
                                if (_compro.NcRechazo.ToUpper() == "T")
                                    _opNcRech.c1 = "S";
                                else
                                    _opNcRech.c1 = "N";
                                _lstAdd.Add(_opNcRech);
                            }
                        }
                        //Si tengo dato adicional lo incluyo.
                        if (_lstAdd.Count > 0)
                        {
                            ComprobanteCAERequest.arrayDatosAdicionales = _lstAdd.ToArray();
                        }
                    }

                    //Array's
                    if (arrayOfTributos.Length > 0)
                    {
                        ComprobanteCAERequest.importeOtrosTributos = _compro.importeTributos;
                        ComprobanteCAERequest.importeOtrosTributosSpecified = true;
                        ComprobanteCAERequest.arrayOtrosTributos = arrayOfTributos;
                    }
                    if (arrayOfItems != null)
                        ComprobanteCAERequest.arrayItems = arrayOfItems.ToArray();
                    if (arrayOfSubtotalesIVA.Length > 0)
                        ComprobanteCAERequest.arraySubtotalesIVA = arrayOfSubtotalesIVA;

                    //

                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(_cuit);


                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ComprobanteType));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEARequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, ComprobanteCAERequest);
                        file.Close();
                    }

                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType objCodigoDescripcionType = new wsMtxca.CodigoDescripcionType();
                        wsMtxca.CodigoDescripcionType[] arrayObservaciones;
                        wsMtxca.CodigoDescripcionType[] arrayErrores;
                        wsMtxca.CodigoDescripcionType evento;
                        wsMtxca.ResultadoSimpleType objResultadoSimpleType = new wsMtxca.ResultadoSimpleType();

                        objResultadoSimpleType = _client.autorizarComprobante(Auth, ComprobanteCAERequest,
                            out comprobanteResponse, out arrayObservaciones, out arrayErrores, out evento);
                        if (_generaXML)
                        {
                            //Escribimos el xml del Resultado.
                            System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ResultadoSimpleType));
                            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEResultado.xml";
                            System.IO.FileStream file = System.IO.File.Create(path);
                            _Antes.Serialize(file, objResultadoSimpleType);
                            file.Close();
                            //Escribimos el response
                            System.Xml.Serialization.XmlSerializer _Response = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ComprobanteCAEResponseType));
                            var pathResponse = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEResponse.xml";
                            System.IO.FileStream fResponse = System.IO.File.Create(pathResponse);
                            _Response.Serialize(fResponse, comprobanteResponse);
                            fResponse.Close();
                            //Escribimos el Error
                            System.Xml.Serialization.XmlSerializer _Errores = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.CodigoDescripcionType[]));
                            var pathError = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEError.xml";
                            System.IO.FileStream fError = System.IO.File.Create(pathError);
                            _Errores.Serialize(fError, arrayErrores);
                            fError.Close();
                        }

                        //Analizamos el resultado.
                        if (objResultadoSimpleType.ToString().Length > 0)
                        {
                            if (objResultadoSimpleType.ToString() == "O")
                            {
                                Errors _er = new Errors();
                                _er.Code = 1;
                                _er.Msg = "P";
                                _errors.Add(_er);
                            }
                            else
                            {
                                Errors _er = new Errors();
                                _er.Code = 1;
                                _er.Msg = objResultadoSimpleType.ToString();
                                _errors.Add(_er);
                            }
                            //Analizamos el comprobanteResponse.
                            if (comprobanteResponse != null)
                            {
                                Errors _er = new Errors();
                                _er.Code = 0;
                                _er.Msg = comprobanteResponse.CAE.ToString() + "|" + comprobanteResponse.fechaVencimientoCAE.Year.ToString() +
                                comprobanteResponse.fechaVencimientoCAE.Month.ToString().PadLeft(2, '0') + comprobanteResponse.fechaVencimientoCAE.Day.ToString().PadLeft(2, '0') +
                                "|" + comprobanteResponse.numeroComprobante.ToString(); ;
                                _errors.Add(_er);
                            }
                            //Analizamos las Observaciones.
                            if (arrayObservaciones != null)
                            {
                                for (int x = 0; x < arrayObservaciones.Length; x++)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 2;
                                    _er.Msg = "Codigo:" + arrayObservaciones[x].codigo.ToString() + " - Descripción:" + arrayObservaciones[x].descripcion;
                                    _errors.Add(_er);
                                }
                            }
                            //Analizamos los Errores
                            if (arrayErrores != null)
                            {
                                for (int x = 0; x < arrayErrores.Length; x++)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 3;
                                    _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                    _errors.Add(_er);
                                }
                            }
                            //Analizamos el evento.
                            if (evento != null)
                            {
                                Errors _er = new Errors();
                                _er.Code = 4;
                                _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                                _errors.Add(_er);
                            }
                        }
                    }
                }
                _errores = _errors;
                return;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "ObtenerCAE(MTXCA) Error: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public static void ObtenerCAE(string pPathCert, string pPathTa, string pPathLog, int PtoVta, int CbteTipo,
            string pCuit, bool IsTest, SecureString _pass, bool _generaXML,
            List<AfipDll.wsAfip.clsIVA> _lstIva,
            List<AfipDll.wsMtxca.ItemType> _lstItems,
            List<AfipDll.wsAfip.clsTributos> _lstTributos,
            Comprobante _compro,
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lstCompAsoc,
            out ObternerCaeResponse _reponse)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            List<Errors> _errors = new List<Errors>();
            ObternerCaeResponse _rta = new ObternerCaeResponse();
            //Int64 _rta = 0;

            string strPathCert = pPathCert;
            string _cuit = pCuit.Replace("-", "");
            long lngCuit = long.Parse(pCuit.Replace("-", ""));
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////ObtenerCAE////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Registro ICG->" + _compro.obervaciones);
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, _cuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, _cuit, _pass);
                if (_cls.Token != null)
                {
                    //Instanciamos las clases de MTXCA.
                    wsMtxca.ComprobanteType ComprobanteCAERequest = new wsMtxca.ComprobanteType();
                    wsMtxca.ComprobanteCAEResponseType comprobanteResponse = new wsMtxca.ComprobanteCAEResponseType();

                    //Instanciamos el array de los items.
                    List<wsMtxca.ItemType> arrayOfItems = new List<wsMtxca.ItemType>();
                    //Cargamos los detalles en el array.
                    foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                    {
                        wsMtxca.ItemType item = new wsMtxca.ItemType();
                        item.unidadesMtx = _it.unidadesMtx;
                        item.unidadesMtxSpecified = true;
                        item.codigoMtx = _it.codigoUnidadMedida.ToString();
                        item.codigo = _it.codigo;
                        item.descripcion = _it.descripcion;
                        item.cantidad = _it.cantidad;
                        item.cantidadSpecified = true;
                        item.codigoUnidadMedida = Convert.ToInt16(_it.codigoUnidadMedida);
                        //Vemos si es factura A
                        if (_compro.tipoComprobante == 1 || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 3
                            || _compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203)
                        {
                            item.precioUnitario = Math.Round(_it.precioUnitario, 3);
                            item.precioUnitarioSpecified = true;
                            item.importeIVA = Math.Round(Convert.ToDecimal(_it.importeIVA), 2);
                            item.importeIVASpecified = true;
                        }
                        //Vemos si es factura B
                        if (_compro.tipoComprobante == 6 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 8
                            || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208)
                        {
                            item.precioUnitario = Math.Round(Convert.ToDecimal(_it.precioUnitario), 3);
                            item.precioUnitarioSpecified = true;
                        }
                        item.importeBonificacion = Math.Round(_it.importeBonificacion, 3);
                        item.importeBonificacionSpecified = true;
                        item.codigoCondicionIVA = _it.codigoCondicionIVA;
                        item.importeItem = Math.Round(_it.importeItem, 2);
                        //Lo agrego al Array
                        arrayOfItems.Add(item);
                    }

                    //IVA
                    List<AfipDll.wsMtxca.SubtotalIVAType> _alicIVA = new List<AfipDll.wsMtxca.SubtotalIVAType>();
                    if (_compro.importeTotal == 0)
                    {
                        foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                        {
                            if (!_alicIVA.Any(x => x.codigo == _it.codigoCondicionIVA))
                            {
                                AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                                _al.codigo = _it.codigoCondicionIVA;
                                _al.importe = 0;
                                _alicIVA.Add(_al);
                            }
                        }
                    }
                    else
                    {
                        foreach (AfipDll.wsAfip.clsIVA _iva in _lstIva)
                        {
                            //Solo enviamos cuando el tipo iva es 4, 5 o 6
                            if (_iva.ID == 4 || _iva.ID == 5 || _iva.ID == 6)
                            {
                                AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                                _al.codigo = Convert.ToInt16(_iva.ID);
                                _al.importe = Math.Round(Convert.ToDecimal(_iva.Importe), 2);
                                _alicIVA.Add(_al);
                            }
                        }
                    }
                    wsMtxca.SubtotalIVAType[] arrayOfSubtotalesIVA = _alicIVA.ToArray();
                
                    //Tributos
                    List<AfipDll.wsMtxca.OtroTributoType> _tributos = new List<AfipDll.wsMtxca.OtroTributoType>();
                    foreach (AfipDll.wsAfip.clsTributos _tr in _lstTributos)
                    {
                        AfipDll.wsMtxca.OtroTributoType _t = new AfipDll.wsMtxca.OtroTributoType();
                        _t.codigo = Convert.ToInt16(_tr.ID);
                        _t.baseImponible = Convert.ToDecimal(_tr.BaseImp);
                        _t.descripcion = _tr.Desc;
                        _t.importe = Convert.ToDecimal(_tr.Importe);
                        _tributos.Add(_t);
                    }
                    wsMtxca.OtroTributoType[] arrayOfTributos = _tributos.ToArray();

                    //CArgamos el request a enviar.
                    ComprobanteCAERequest.codigoTipoComprobante = _compro.tipoComprobante;
                    ComprobanteCAERequest.numeroPuntoVenta = _compro.ptoVta;
                    ComprobanteCAERequest.numeroComprobante = _compro.numeroComprobante;
                    ComprobanteCAERequest.fechaEmision = _compro.fechaEmision;
                    ComprobanteCAERequest.fechaEmisionSpecified = true;
                    ComprobanteCAERequest.codigoTipoDocumento = _compro.tipoDocumento;
                    ComprobanteCAERequest.codigoTipoDocumentoSpecified = true;
                    ComprobanteCAERequest.numeroDocumento = _compro.numeroDocumento;
                    ComprobanteCAERequest.numeroDocumentoSpecified = true;
                    ComprobanteCAERequest.importeGravado = Math.Round(_compro.importeGravado, 2);
                    ComprobanteCAERequest.importeGravadoSpecified = true;
                    ComprobanteCAERequest.importeNoGravado = Math.Round(_compro.importeNoGravado, 2);
                    ComprobanteCAERequest.importeNoGravadoSpecified = true;
                    ComprobanteCAERequest.importeExento = Math.Round(_compro.importeExento, 2);
                    ComprobanteCAERequest.importeSubtotal = Math.Round(_compro.importeSubTotal, 2);
                    ComprobanteCAERequest.importeTotal = Math.Round(_compro.importeTotal, 2);
                    ComprobanteCAERequest.codigoMoneda = _compro.codigoMoneda;
                    ComprobanteCAERequest.cotizacionMoneda = _compro.cotizacionMoneda;
                    ComprobanteCAERequest.observaciones = _compro.obervaciones;
                    ComprobanteCAERequest.codigoConcepto = _compro.concepto;
                    if (_compro.concepto == 3 || _compro.concepto == 2)
                    {
                        ComprobanteCAERequest.fechaServicioDesde = _compro.fechaDesde;
                        ComprobanteCAERequest.fechaServicioDesdeSpecified = true;
                        ComprobanteCAERequest.fechaServicioHasta = _compro.fechaHasta;
                        ComprobanteCAERequest.fechaServicioHastaSpecified = true;
                        ComprobanteCAERequest.fechaVencimientoPago = _compro.fechaPago;
                        ComprobanteCAERequest.fechaVencimientoPagoSpecified = true;
                        ComprobanteCAERequest.fechaVencimiento = _compro.fechaVto;
                    }

                    if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203
                           || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208
                           || _compro.tipoComprobante == 211 || _compro.tipoComprobante == 212 || _compro.tipoComprobante == 213
                           || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                           || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                    {
                        List<wsMtxca.DatoAdicionalType> _lstAdd = new List<wsMtxca.DatoAdicionalType>();
                        if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 211)
                        {
                            ComprobanteCAERequest.fechaVencimientoPago = _compro.fechaPago;
                            ComprobanteCAERequest.fechaVencimientoPagoSpecified = true;
                            //CBU                                
                            wsMtxca.DatoAdicionalType _ad1 = new wsMtxca.DatoAdicionalType();
                            _ad1.t = 21;
                            _ad1.c1 = _compro.cbu;
                            _lstAdd.Add(_ad1);

                        }

                        if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                    || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212
                                    || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                                    || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                        {
                            ComprobanteCAERequest.arrayComprobantesAsociados = _lstCompAsoc.ToArray();

                            if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                    || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212)
                            {
                                //Cargo el opcional como rechazado para sacar la NC.
                                wsMtxca.DatoAdicionalType _opNcRech = new wsMtxca.DatoAdicionalType();
                                _opNcRech.t = 22;
                                if (_compro.NcRechazo.ToUpper() == "T")
                                    _opNcRech.c1 = "S";
                                else
                                    _opNcRech.c1 = "N";
                                _lstAdd.Add(_opNcRech);
                            }
                        }
                        //Si tengo dato adicional lo incluyo.
                        if (_lstAdd.Count > 0)
                        {
                            ComprobanteCAERequest.arrayDatosAdicionales = _lstAdd.ToArray();
                        }
                    }
                    //Array's
                    if (arrayOfTributos.Length > 0)
                    {
                        ComprobanteCAERequest.importeOtrosTributos = _compro.importeTributos;
                        ComprobanteCAERequest.importeOtrosTributosSpecified = true;
                        ComprobanteCAERequest.arrayOtrosTributos = arrayOfTributos;
                    }
                    if (arrayOfItems != null)
                        ComprobanteCAERequest.arrayItems = arrayOfItems.ToArray();
                    if (arrayOfSubtotalesIVA.Length > 0)
                        ComprobanteCAERequest.arraySubtotalesIVA = arrayOfSubtotalesIVA;
                    //Armo la autenticación
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(_cuit);
                    //Vemos si generamos los Xml
                    if (_generaXML)
                    {
                        //Escribimos el xml del Request.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ComprobanteType));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEARequest.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, ComprobanteCAERequest);
                        file.Close();
                    }

                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType objCodigoDescripcionType = new wsMtxca.CodigoDescripcionType();
                        wsMtxca.CodigoDescripcionType[] arrayObservaciones;
                        wsMtxca.CodigoDescripcionType[] arrayErrores;
                        wsMtxca.CodigoDescripcionType evento;
                        wsMtxca.ResultadoSimpleType objResultadoSimpleType = new wsMtxca.ResultadoSimpleType();

                        objResultadoSimpleType = _client.autorizarComprobante(Auth, ComprobanteCAERequest,
                            out comprobanteResponse, out arrayObservaciones, out arrayErrores, out evento);
                        if (_generaXML)
                        {
                            //Escribimos el xml del Resultado.
                            System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ResultadoSimpleType));
                            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEResultado.xml";
                            System.IO.FileStream file = System.IO.File.Create(path);
                            _Antes.Serialize(file, objResultadoSimpleType);
                            file.Close();
                            //Escribimos el response
                            System.Xml.Serialization.XmlSerializer _Response = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.ComprobanteCAEResponseType));
                            var pathResponse = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEResponse.xml";
                            System.IO.FileStream fResponse = System.IO.File.Create(pathResponse);
                            _Response.Serialize(fResponse, comprobanteResponse);
                            fResponse.Close();
                            //Escribimos el Error
                            System.Xml.Serialization.XmlSerializer _Errores = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.CodigoDescripcionType[]));
                            var pathError = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ComCAEError.xml";
                            System.IO.FileStream fError = System.IO.File.Create(pathError);
                            _Errores.Serialize(fError, arrayErrores);
                            fError.Close();
                        }

                        switch(objResultadoSimpleType.ToString().ToUpper())
                        {
                            case "A":
                                {
                                    _rta.numeroCbte = comprobanteResponse.numeroComprobante;
                                    _rta.cae = comprobanteResponse.CAE;
                                    _rta.fechaEmision = comprobanteResponse.fechaEmision;
                                    _rta.fechaVto = comprobanteResponse.fechaVencimientoCAE;
                                    _rta.numeroCbte = comprobanteResponse.numeroComprobante;
                                    _rta.puntoVta = comprobanteResponse.numeroPuntoVenta;
                                    _rta.respuesta = "A";
                                    _rta.tipoCbte = comprobanteResponse.codigoTipoComprobante;
                                    break;
                                }
                            case "O":
                                {
                                    string _observa = "";
                                    //Analizamos las Observaciones.
                                    if (arrayObservaciones != null)
                                    {
                                        for (int x = 0; x < arrayObservaciones.Length; x++)
                                        {
                                            _observa = "Codigo:" + arrayObservaciones[x].codigo.ToString() + " - Descripción:" + arrayObservaciones[x].descripcion;
                                        }
                                    }
                                    _rta.numeroCbte = comprobanteResponse.numeroComprobante;
                                    _rta.cae = comprobanteResponse.CAE;
                                    _rta.fechaEmision = comprobanteResponse.fechaEmision;
                                    _rta.fechaVto = comprobanteResponse.fechaVencimientoCAE;
                                    _rta.numeroCbte = comprobanteResponse.numeroComprobante;
                                    _rta.puntoVta = comprobanteResponse.numeroPuntoVenta;
                                    _rta.respuesta = "O";
                                    _rta.tipoCbte = comprobanteResponse.codigoTipoComprobante;
                                    _rta.observacion = _observa;
                                    break;
                                }
                            case "R":
                                {
                                    string _err = "";
                                    if (arrayErrores != null)
                                    {
                                        for (int x = 0; x < arrayErrores.Length; x++)
                                        {
                                            _err = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                        }
                                    }
                                    _rta.respuesta = "R";
                                    _rta.error = _err;
                                    break;
                                }
                        }
                    }
                }
                _reponse = _rta;
                return;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "ObtenerCAE(MTXCA) Error: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }


        public static AfipDll.wsAfip.CaeaSolicitar SolicitarCAEA(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass,
            int _periodo, short _orden, bool _generaXML, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            AfipDll.wsAfip.CaeaSolicitar _clsCAEA = new wsAfip.CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                Auth.token = _cls.Token;
                Auth.sign = _cls.Sign;
                Auth.cuitRepresentada = long.Parse(pCuit);

                using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);                    

                    wsMtxca.SolicitudCAEAType _solicitud = new wsMtxca.SolicitudCAEAType();
                    _solicitud.orden = _orden;
                    _solicitud.periodo = _periodo;

                    wsMtxca.CodigoDescripcionType objCodigoDescripcionType = new wsMtxca.CodigoDescripcionType();
                    wsMtxca.CodigoDescripcionType[] arrayErrores;
                    wsMtxca.CodigoDescripcionType evento;

                    if (_generaXML)
                    {
                        //Escribimos el xml del auth.
                        System.Xml.Serialization.XmlSerializer _Antes = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.AuthRequestType));
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\\SolicitarCAEAAuth.xml";
                        System.IO.FileStream file = System.IO.File.Create(path);
                        _Antes.Serialize(file, Auth);
                        file.Close();
                        //Escribimos el xml del Solicitud.
                        System.Xml.Serialization.XmlSerializer _Req = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.SolicitudCAEAType));
                        var pathReq = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\\SolicitarCAEARequest.xml";
                        System.IO.FileStream fileReq = System.IO.File.Create(pathReq);
                        _Req.Serialize(fileReq, _solicitud);
                        fileReq.Close();
                    }

                    wsMtxca.CAEAResponseType _rta = _client.solicitarCAEA(Auth, _solicitud, out arrayErrores, out evento);

                    if (_generaXML)
                    {
                        //Escribimos el xml del Response.
                        System.Xml.Serialization.XmlSerializer _Despues = new System.Xml.Serialization.XmlSerializer(typeof(wsMtxca.CAEAResponseType));
                        var pathDes = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\SolicitarCAEResponse.xml";
                        System.IO.FileStream fileDes = System.IO.File.Create(pathDes);
                        _Despues.Serialize(fileDes, _rta);
                        fileDes.Close();
                    }

                    if (arrayErrores == null)
                    {
                        _clsCAEA.CAEA = _rta.CAEA.ToString();
                        _clsCAEA.FchProceso = _rta.fechaProceso.Year.ToString() + _rta.fechaProceso.Month.ToString().PadLeft(2, '0') + _rta.fechaProceso.Day.ToString().PadLeft(2, '0');
                        _clsCAEA.FchVigDesde = _rta.fechaDesde.Year.ToString() + _rta.fechaDesde.Month.ToString().PadLeft(2, '0') + _rta.fechaDesde.Day.ToString().PadLeft(2, '0');
                        _clsCAEA.FchVigHasta = _rta.fechaHasta.Year.ToString() + _rta.fechaHasta.Month.ToString().PadLeft(2, '0') + _rta.fechaHasta.Day.ToString().PadLeft(2, '0');
                        _clsCAEA.Orden = _rta.orden;
                        _clsCAEA.Periodo = _rta.periodo;
                        _clsCAEA.FchTopeInf = _rta.fechaTopeInforme.Year.ToString() + _rta.fechaTopeInforme.Month.ToString().PadLeft(2, '0') + _rta.fechaTopeInforme.Day.ToString().PadLeft(2, '0');

                    }
                    else
                    {
                        //Analizamos los Errores
                        if (arrayErrores != null)
                        {
                            for (int x = 0; x < arrayErrores.Length; x++)
                            {
                                Errors _er = new Errors();
                                _er.Code = 3;
                                _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                _errors.Add(_er);
                            }
                        }
                        //Analizamos el evento.
                        if (evento != null)
                        {
                            Errors _er = new Errors();
                            _er.Code = 4;
                            _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _clsCAEA;
        }

        public static AfipDll.wsAfip.CaeaSolicitar ConsultarCAEA(string pPathCert, string pPathTa, string pPathLog,
            string pCuit, bool IsTest, SecureString _pass, DateTime _fechaDesde, DateTime _fechaHasta, out List<Errors> _errores)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            AfipDll.wsAfip.CaeaSolicitar _clsCAEA = new AfipDll.wsAfip.CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(pCuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////ConsultarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, pCuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, pCuit, _pass);
            if (_cls.Token != null)
            {
                wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                Auth.token = _cls.Token;
                Auth.sign = _cls.Sign;
                Auth.cuitRepresentada = long.Parse(pCuit);

                using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                    wsMtxca.CodigoDescripcionType[] arrayErrores;
                    wsMtxca.CodigoDescripcionType evento = new wsMtxca.CodigoDescripcionType();

                    wsMtxca.CAEAResponseType[] _rta = _client.consultarCAEAEntreFechas(Auth, _fechaDesde, _fechaHasta, out arrayErrores, out evento);

                    if (_rta.Count()  > 0)
                    {
                        for (int i = 0; i < _rta.Length; i++)
                        {
                            _clsCAEA.CAEA = _rta[i].CAEA.ToString();
                            _clsCAEA.FchProceso = _rta[i].fechaProceso.ToString();
                            _clsCAEA.FchVigDesde = _rta[i].fechaDesde.ToString();
                            _clsCAEA.FchVigHasta = _rta[i].fechaHasta.ToString();
                            _clsCAEA.Orden = _rta[i].orden;
                            _clsCAEA.Periodo = _rta[i].periodo;
                            _clsCAEA.FchTopeInf = _rta[i].fechaTopeInforme.ToString();
                        }
                    }
                    else
                    {
                        if (arrayErrores != null)
                        {
                            //wsAfipCae.Err[] _err = new wsAfipCae.Err[arrayErrores.Length];
                            for (int i = 0; i < arrayErrores.Length; i++)
                            {
                                Errors _er = new Errors();
                                _er.Code = arrayErrores[i].codigo;
                                _er.Msg = arrayErrores[i].descripcion;

                                _errors.Add(_er);
                            }
                        }
                        else
                        {
                            Errors _er = new Errors();
                            _er.Code = -1;
                            _er.Msg = "Error: La AFIP contesto de manera incorrecta. No otorgo CAEA, pero tampoco informo ERROR.";

                            _errors.Add(_er);
                        }
                    }
                }
            }
            _errores = _errors;
            return _clsCAEA;
        }

        public static void InformarComprobantesCAEA(string pPathCert, string pPathTa, string pPathLog,
            bool IsTest, SecureString _pass,
            List<AfipDll.wsAfip.clsIVA> _lstIva,
            List<AfipDll.wsMtxca.ItemType> _lstItems,
            List<AfipDll.wsAfip.clsTributos> _lstTributos,
            Comprobante _compro, string _caea,
            List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lstCompAsoc,bool _generaXML, out List<Errors> _errores, out CaeaSolicitar _resultado)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            CaeaSolicitar _clsCAEA = new CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(_compro.cuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, _compro.cuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, _compro.cuit, _pass);
            if (_cls.Token != null)
            {
                //Instanciamos el array de los items.
                List<wsMtxca.ItemType> arrayOfItems = new List<wsMtxca.ItemType>();
                //Cargamos los detalles en el array.
                foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                {
                    wsMtxca.ItemType item = new wsMtxca.ItemType();
                    item.unidadesMtx = _it.unidadesMtx;
                    item.unidadesMtxSpecified = true;
                    item.codigoMtx = _it.codigoUnidadMedida.ToString();
                    item.codigo = _it.codigo;
                    item.descripcion = _it.descripcion;
                    item.cantidad = _it.cantidad;
                    item.cantidadSpecified = true;
                    item.codigoUnidadMedida = Convert.ToInt16(_it.codigoUnidadMedida);
                    //Vemos si es factura A
                    if (_compro.tipoComprobante == 1 || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 3
                        || _compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203)
                    {
                        item.precioUnitario = Math.Round(_it.precioUnitario, 3);
                        item.precioUnitarioSpecified = true;
                        item.importeIVA = Math.Round(Convert.ToDecimal(_it.importeIVA), 2);
                        item.importeIVASpecified = true;
                    }
                    //Vemos si es factura B
                    if (_compro.tipoComprobante == 6 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 8
                        || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208)
                    {
                        item.precioUnitario = Math.Round(Convert.ToDecimal(_it.precioUnitario), 3);
                        item.precioUnitarioSpecified = true;
                    }
                    item.importeBonificacion = Math.Round(_it.importeBonificacion, 2);
                    item.importeBonificacionSpecified = true;
                    item.codigoCondicionIVA = _it.codigoCondicionIVA;
                    item.importeItem = Math.Round(_it.importeItem, 2);
                    arrayOfItems.Add(item);
                }

                //IVA
                //List<AfipDll.wsMtxca.SubtotalIVAType> _alicIVA = new List<AfipDll.wsMtxca.SubtotalIVAType>();
                //foreach (AfipDll.wsAfip.clsIVA _iva in _lstIva)
                //{
                //    AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                //    _al.codigo = Convert.ToInt16(_iva.ID);
                //    _al.importe = Convert.ToDecimal(_iva.Importe);
                //    _alicIVA.Add(_al);
                //}
                //wsMtxca.SubtotalIVAType[] arrayOfSubtotalesIVA = _alicIVA.ToArray();
                List<AfipDll.wsMtxca.SubtotalIVAType> _alicIVA = new List<AfipDll.wsMtxca.SubtotalIVAType>();
                if (_compro.importeTotal == 0)
                {
                    foreach (AfipDll.wsMtxca.ItemType _it in _lstItems)
                    {
                        if (!_alicIVA.Any(x => x.codigo == _it.codigoCondicionIVA))
                        {
                            AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                            _al.codigo = _it.codigoCondicionIVA;
                            _al.importe = 0;
                            _alicIVA.Add(_al);
                        }
                    }
                }
                else
                {
                    foreach (AfipDll.wsAfip.clsIVA _iva in _lstIva)
                    {
                        //Solo enviamos cuando el tipo iva es 4, 5 o 6
                        if (_iva.ID == 4 || _iva.ID == 5 || _iva.ID == 6)
                        {
                            AfipDll.wsMtxca.SubtotalIVAType _al = new AfipDll.wsMtxca.SubtotalIVAType();
                            _al.codigo = Convert.ToInt16(_iva.ID);
                            _al.importe = Math.Round(Convert.ToDecimal(_iva.Importe), 2);
                            _alicIVA.Add(_al);
                        }
                    }
                }
                wsMtxca.SubtotalIVAType[] arrayOfSubtotalesIVA = _alicIVA.ToArray();

                //Tributos
                List<AfipDll.wsMtxca.OtroTributoType> _tributos = new List<AfipDll.wsMtxca.OtroTributoType>();
                foreach (AfipDll.wsAfip.clsTributos _tr in _lstTributos)
                {
                    AfipDll.wsMtxca.OtroTributoType _t = new AfipDll.wsMtxca.OtroTributoType();
                    _t.codigo = Convert.ToInt16(_tr.ID);
                    _t.baseImponible = Math.Round(Convert.ToDecimal(_tr.BaseImp), 2);
                    _t.descripcion = _tr.Desc;
                    _t.importe = Math.Round(Convert.ToDecimal(_tr.Importe), 2);
                    _tributos.Add(_t);
                }
                wsMtxca.OtroTributoType[] arrayOfTributos = _tributos.ToArray();

                //Request de InformarCAEA
                wsMtxca.ComprobanteType comprobante = new wsMtxca.ComprobanteType();
                //CArgamos el request a enviar.
                comprobante.codigoTipoComprobante = _compro.tipoComprobante;
                comprobante.numeroPuntoVenta = _compro.ptoVta;
                comprobante.numeroComprobante = _compro.numeroComprobante;
                comprobante.fechaEmision = _compro.fechaEmision;
                comprobante.fechaEmisionSpecified = true;
                comprobante.fechaHoraGen = _compro.fechaEmision;
                comprobante.fechaHoraGenSpecified = true;
                comprobante.codigoTipoDocumento = _compro.tipoDocumento;
                comprobante.codigoTipoDocumentoSpecified = true;
                comprobante.numeroDocumento = _compro.numeroDocumento;
                comprobante.numeroDocumentoSpecified = true;
                comprobante.importeGravado = Math.Round(_compro.importeGravado, 2);
                comprobante.importeGravadoSpecified = true;
                comprobante.importeNoGravado = Math.Round(_compro.importeNoGravado, 2);
                comprobante.importeNoGravadoSpecified = true;
                comprobante.importeExento = Math.Round(_compro.importeExento, 2);
                comprobante.importeSubtotal = Math.Round(_compro.importeSubTotal, 2);
                comprobante.importeTotal = Math.Round(_compro.importeTotal, 2);
                comprobante.codigoMoneda = _compro.codigoMoneda;
                comprobante.cotizacionMoneda = _compro.cotizacionMoneda;
                comprobante.observaciones = _compro.obervaciones;
                comprobante.codigoConcepto = _compro.concepto;
                comprobante.codigoAutorizacionSpecified = true;
                comprobante.codigoAutorizacion = Convert.ToInt64(_caea);

                if (_compro.concepto == 3 || _compro.concepto == 2)
                {
                    comprobante.fechaServicioDesde = _compro.fechaDesde;
                    comprobante.fechaServicioDesdeSpecified = true;
                    comprobante.fechaServicioHasta = _compro.fechaHasta;
                    comprobante.fechaServicioHastaSpecified = true;
                    comprobante.fechaVencimientoPago = _compro.fechaPago;
                    comprobante.fechaVencimientoPagoSpecified = true;
                    comprobante.fechaVencimiento = _compro.fechaVto;
                }

                if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 203
                       || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 208
                       || _compro.tipoComprobante == 211 || _compro.tipoComprobante == 212 || _compro.tipoComprobante == 213
                       || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                       || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                {
                    List<wsMtxca.DatoAdicionalType> _lstAdd = new List<wsMtxca.DatoAdicionalType>();
                    if (_compro.tipoComprobante == 201 || _compro.tipoComprobante == 206 || _compro.tipoComprobante == 211)
                    {
                        comprobante.fechaVencimientoPago = _compro.fechaPago;
                        comprobante.fechaVencimientoPagoSpecified = true;
                        //CBU                                
                        wsMtxca.DatoAdicionalType _ad1 = new wsMtxca.DatoAdicionalType();
                        _ad1.t = 21;
                        _ad1.c1 = _compro.cbu;
                        _lstAdd.Add(_ad1);

                    }

                    if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212
                                || _compro.tipoComprobante == 3 || _compro.tipoComprobante == 8 || _compro.tipoComprobante == 13
                                || _compro.tipoComprobante == 2 || _compro.tipoComprobante == 7 || _compro.tipoComprobante == 12)
                    {
                        comprobante.arrayComprobantesAsociados = _lstCompAsoc.ToArray();

                        if (_compro.tipoComprobante == 203 || _compro.tipoComprobante == 208 || _compro.tipoComprobante == 213
                                || _compro.tipoComprobante == 202 || _compro.tipoComprobante == 207 || _compro.tipoComprobante == 212)
                        {
                            //Cargo el opcional como rechazado para sacar la NC.
                            wsMtxca.DatoAdicionalType _opNcRech = new wsMtxca.DatoAdicionalType();
                            _opNcRech.t = 22;
                            if (_compro.NcRechazo.ToUpper() == "T")
                                _opNcRech.c1 = "S";
                            else
                                _opNcRech.c1 = "N";
                            _lstAdd.Add(_opNcRech);
                        }
                    }
                    //Si tengo dato adicional lo incluyo.
                    if (_lstAdd.Count > 0)
                    {
                        comprobante.arrayDatosAdicionales = _lstAdd.ToArray();
                    }
                }

                //Array's
                if (arrayOfTributos.Length > 0)
                {
                    comprobante.importeOtrosTributos = _compro.importeTributos;
                    comprobante.importeOtrosTributosSpecified = true;
                    comprobante.arrayOtrosTributos = arrayOfTributos;
                }
                if (arrayOfItems != null)
                    comprobante.arrayItems = arrayOfItems.ToArray();
                if (arrayOfSubtotalesIVA.Length > 0)
                    comprobante.arraySubtotalesIVA = arrayOfSubtotalesIVA;

                wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                Auth.token = _cls.Token;
                Auth.sign = _cls.Sign;
                Auth.cuitRepresentada = long.Parse(_compro.cuit);

                using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                    DateTime fechaProceso ;
                    wsMtxca.ComprobanteCAEAResponseType responseType;
                    wsMtxca.CodigoDescripcionType[] arrayObservaciones;
                    wsMtxca.CodigoDescripcionType[] arrayErrores;
                    wsMtxca.CodigoDescripcionType evento;

                    wsMtxca.ResultadoSimpleType response = _client.informarComprobanteCAEA(Auth, comprobante,
                        out fechaProceso, out responseType, out arrayObservaciones, out arrayErrores, out evento);


                    switch (response.ToString())
                    {
                        case "A":
                            {
                                //Aprobado.
                                _clsCAEA.CAEA = responseType.CAEA.ToString();
                                _clsCAEA.FchProceso = fechaProceso.Year.ToString() + fechaProceso.Month.ToString().PadLeft(2, '0') + fechaProceso.Day.ToString().PadLeft(2, '0');
                                _clsCAEA.Resultado = "A";
                                _clsCAEA.Descripcion = "APROBADO";
                                break;
                            }
                        case "O":
                            {
                                //Aprobado con Observaciones
                                _clsCAEA.CAEA = responseType.CAEA.ToString();
                                _clsCAEA.FchProceso = fechaProceso.Year.ToString() + fechaProceso.Month.ToString().PadLeft(2, '0') + fechaProceso.Day.ToString().PadLeft(2, '0');
                                _clsCAEA.Resultado = "O";
                                //Mando las observaciones.
                                for (int x = 0; x < arrayObservaciones.Length; x++)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 3;
                                    _er.Msg = "Codigo:" + arrayObservaciones[x].codigo.ToString() + " - Descripción:" + arrayObservaciones[x].descripcion;
                                    _errors.Add(_er);
                                    _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                }
                                //Analizamos el evento.
                                if (evento != null)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 4;
                                    _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                                    _errors.Add(_er);
                                    _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                }
                                break;
                            }
                        case "R":
                            {
                                //Rechazado
                                _clsCAEA.CAEA = null;
                                _clsCAEA.FchProceso = null;
                                _clsCAEA.Resultado = "R";
                                //Mandamos los Errores
                                if (arrayErrores != null)
                                {
                                    for (int x = 0; x < arrayErrores.Length; x++)
                                    {
                                        Errors _er = new Errors();
                                        _er.Code = 3;
                                        _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                        _errors.Add(_er);
                                        _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                    }
                                }
                                //Analizamos el evento.
                                if (evento != null)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 4;
                                    _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                                    _errors.Add(_er);
                                    _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                }
                                break;
                            }
                    }                                            
                }
            }
            _errores = _errors;
            _resultado = _clsCAEA;
        }

        public static void InformarCAEANoUtilizadoPuntoVenta(string pPathCert, string pPathTa, string pPathLog,
            bool IsTest, SecureString _pass, bool _generaXML, string _cuit, string _caea, int _ptoVta,
            out List<Errors> _errores, out CaeaSolicitar _resultado)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            CaeaSolicitar _clsCAEA = new CaeaSolicitar();
            List<Errors> _errors = new List<Errors>();

            string strPathCert = pPathCert;
            long lngCuit = long.Parse(_cuit);
            WsaaNew _cls = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
            //validamos que tenemos el TA.xml
            if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, _cuit))
                _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, _cuit, _pass);
            if (_cls.Token != null)
            {
                wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                Auth.token = _cls.Token;
                Auth.sign = _cls.Sign;
                Auth.cuitRepresentada = long.Parse(_cuit);

                using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                {
                    if (IsTest)
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                    else
                        _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                    DateTime fechaProceso;

                    wsMtxca.CodigoDescripcionType[] arrayErrores;
                    wsMtxca.CodigoDescripcionType evento;

                    long caea = long.Parse(_caea);

                    wsMtxca.ResultadoSimpleType response = _client.informarCAEANoUtilizadoPtoVta(Auth, ref caea, ref _ptoVta,
                        out fechaProceso, out arrayErrores, out evento);


                    switch (response.ToString())
                    {
                        case "A":
                            {
                                //Aprobado.
                                _clsCAEA.CAEA = caea.ToString();
                                _clsCAEA.FchProceso = fechaProceso.Year.ToString() + fechaProceso.Month.ToString().PadLeft(2, '0') + fechaProceso.Day.ToString().PadLeft(2, '0');
                                _clsCAEA.Resultado = "A";
                                _clsCAEA.Descripcion = "APROBADO";
                                break;
                            }
                        case "R":
                            {
                                //Rechazado
                                _clsCAEA.CAEA = null;
                                _clsCAEA.FchProceso = null;
                                _clsCAEA.Resultado = "R";
                                //Mandamos los Errores
                                if (arrayErrores != null)
                                {
                                    for (int x = 0; x < arrayErrores.Length; x++)
                                    {
                                        Errors _er = new Errors();
                                        _er.Code = 3;
                                        _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                        _errors.Add(_er);
                                        _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                    }
                                }
                                //Analizamos el evento.
                                if (evento != null)
                                {
                                    Errors _er = new Errors();
                                    _er.Code = 4;
                                    _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                                    _errors.Add(_er);
                                    _clsCAEA.Descripcion = _clsCAEA.Descripcion + _er.Msg;
                                }
                                break;
                            }
                    }
                }
            }
            _errores = _errors;
            _resultado = _clsCAEA;
        }

        public static void ConsultarPtoVtaCAEANoInformado(string pPathCert, string pPathTa, string pPathLog,
            bool IsTest, SecureString _pass, bool _generaXML, string _cuit, string _caea,
            out List<Errors> _errores, out List<PtoVenta> _ptosVta)
        {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                List<PtoVenta> lst = new List<PtoVenta>();
                List<Errors> _errors = new List<Errors>();

                string strPathCert = pPathCert;
                long lngCuit = long.Parse(_cuit);
                WsaaNew _cls = new WsaaNew();
                //Escribo los detalles en el log.
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////SolicitarCAEA////");
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Certificado->" + strPathCert);
                LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////CUIT->" + lngCuit);
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _cls, _cuit))
                    _cls = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, strPathCert, pPathTa, pPathLog, IsTest, _cuit, _pass);
                if (_cls.Token != null)
                {
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _cls.Token;
                    Auth.sign = _cls.Sign;
                    Auth.cuitRepresentada = long.Parse(_cuit);

                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType[] arrayErrores;
                        wsMtxca.CodigoDescripcionType evento;

                        long caea = long.Parse(_caea);

                        wsMtxca.PuntoVentaType[] arrayPuntosVenta = _client.consultarPtosVtaCAEANoInformados(Auth, caea,
                            out arrayErrores, out evento);

                        if (arrayErrores == null)
                        {
                            for (int x = 0; x < arrayPuntosVenta.Length; x++)
                            {
                                PtoVenta cls = new PtoVenta();
                                cls.Nro = arrayPuntosVenta[x].numeroPuntoVenta;
                                cls.Bloqueado = arrayPuntosVenta[x].bloqueado.ToString();
                                if (arrayPuntosVenta[x].fechaBajaSpecified)
                                    cls.FechaBaja = arrayPuntosVenta[x].fechaBaja.ToShortDateString();
                                lst.Add(cls);
                            }
                        }
                        else
                        {
                            for (int x = 0; x < arrayErrores.Length; x++)
                            {
                                Errors _er = new Errors();
                                _er.Code = 3;
                                _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                _errors.Add(_er);
                            }
                            //Analizamos el evento.
                            if (evento != null)
                            {
                                Errors _er = new Errors();
                                _er.Code = 4;
                                _er.Msg = "Codigo:" + evento.codigo.ToString() + " - Descripción:" + evento.descripcion; ;
                                _errors.Add(_er);
                            }
                        }
                    }
                }
            _errores = _errors;
            _ptosVta = lst;
        }

        public static void ConsultarComprobanteAutorizado(string pPathCert, string pPathTa, string pPathLog, string pCuit,
            string pTipoCbte, string pPtoVta, long pNroCbte, bool IsTest, SecureString pPass, bool pGeneraXml,
            out ConsultaComprobanteResponse pCompro, out List<Errors> pErrores)
        {
            //Establecemos el TLS
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //
            List<Errors> _errors = new List<Errors>();
            ConsultaComprobanteResponse _cbte = new ConsultaComprobanteResponse();
            WsaaNew _clsWasa = new WsaaNew();
            //Escribo los detalles en el log.
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "**************************************************************************************************");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Recuperar Comprobante ya autorizado////");
            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "//////Comprobante-> Tipo: " + pTipoCbte + ", PtoVta: " + pPtoVta + ", Numero: " + pNroCbte.ToString());
            try
            {
                //validamos que tenemos el TA.xml
                if (!WsaaNew.Wsaa.ValidoTANew(pPathCert, pPathTa, pPathLog, _clsWasa, pCuit.Replace("-", "")))
                    _clsWasa = WsaaNew.Wsaa.ObtenerDatosWsaaNew(DEFAULT_SERVICIO, pPathCert, pPathTa, pPathLog, IsTest, pCuit.Replace("-", ""), pPass);
                if (_clsWasa.Token != null)
                {
                    //Armo la autenticacion
                    wsMtxca.AuthRequestType Auth = new wsMtxca.AuthRequestType();
                    Auth.token = _clsWasa.Token;
                    Auth.sign = _clsWasa.Sign;
                    Auth.cuitRepresentada = long.Parse(pCuit.Replace("-", ""));
                    //Armo la consulta.
                    wsMtxca.ConsultaComprobanteRequestType _consulta = new wsMtxca.ConsultaComprobanteRequestType();
                    _consulta.codigoTipoComprobante = Convert.ToInt16(pTipoCbte);
                    _consulta.numeroPuntoVenta = Convert.ToInt32(pPtoVta);
                    _consulta.numeroComprobante = pNroCbte;

                    using (wsMtxca.MTXCAServicePortTypeClient _client = new wsMtxca.MTXCAServicePortTypeClient())
                    {
                        if (IsTest)
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TEST_URLWS);
                        else
                            _client.Endpoint.Address = new System.ServiceModel.EndpointAddress(PROD_URLWS);

                        wsMtxca.CodigoDescripcionType[] arrayObservaciones;
                        wsMtxca.CodigoDescripcionType[] arrayErrores;
                        wsMtxca.CodigoDescripcionType evento;
                        wsMtxca.ComprobanteType comprobante = new wsMtxca.ComprobanteType();

                        wsMtxca.consultarComprobanteResponse res = new wsMtxca.consultarComprobanteResponse();
                        comprobante = _client.consultarComprobante(Auth, _consulta, out arrayObservaciones, out arrayErrores, out evento);
                        //vemos si hubo algún error
                        if (arrayErrores == null)
                        {
                            //Todo OK                            
                            _cbte.codigoAutorizacion = comprobante.codigoAutorizacion;
                            _cbte.codigoMoneda = comprobante.codigoMoneda;
                            _cbte.cotizacionMoneda = comprobante.cotizacionMoneda;
                            _cbte.fechaEmision = comprobante.fechaEmision;
                            _cbte.fechaVencimiento = comprobante.fechaVencimiento;
                            _cbte.importeTotal = comprobante.importeTotal;
                            _cbte.numeroCbte = comprobante.numeroComprobante;
                            _cbte.numeroDocumento = comprobante.numeroDocumento;
                            _cbte.observaciones = comprobante.observaciones;
                            _cbte.puntoVenta = comprobante.numeroPuntoVenta;
                            _cbte.tipoAutorizacion = comprobante.codigoTipoAutorizacion.ToString();
                            _cbte.tipoComprobante = comprobante.codigoTipoComprobante;
                            _cbte.tipoDocumento = comprobante.codigoTipoDocumento;
                        }
                        else
                        {
                            for (int x = 0; x < arrayErrores.Length; x++)
                            {
                                Errors _er = new Errors();
                                _er.Code = 3;
                                _er.Msg = "Codigo:" + arrayErrores[x].codigo.ToString() + " - Descripción:" + arrayErrores[x].descripcion;
                                _errors.Add(_er);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Errors _er = new Errors();
                _er.Code = 3;
                _er.Msg = "Codigo: Excepcion - Descripción:" + ex.Message;
                _errors.Add(_er);                
            }
            pCompro = _cbte;
            pErrores = _errors;
        }
    }
}
