﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace AfipDll
{
    public class CAEA
    {
        public int ID { get; set; }
        public string Caea { get; set; }
        public DateTime FechaProceso { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public DateTime FechaTope { get; set; }
        public int Periodo { get; set; }
        public int Quincena { get; set; }

        public static CAEA GetCaeaByPeriodoQuincena(int _periodo, int _quincena, SqlConnection _connection)
        {
            CAEA _cls = new CAEA();

            string _sql = @"SELECT ID, CAEA, FechaProceso, FechaDesde, FechaHasta, FechaTope, Periodo, Quincena 
                FROM FCCAEA WHERE Periodo = @Periodo AND Quincena = @Quincena";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _connection;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros.
                _cmd.Parameters.AddWithValue("@Periodo", _periodo);
                _cmd.Parameters.AddWithValue("@Quincena", _quincena);
                //Datareader
                using (SqlDataReader reader = _cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            _cls.Caea = reader["CAEA"].ToString();
                            _cls.FechaDesde = Convert.ToDateTime(reader["FechaDesde"]);
                            _cls.FechaHasta = Convert.ToDateTime(reader["FechaHasta"]);
                            _cls.FechaProceso = Convert.ToDateTime(reader["FechaProceso"]);
                            _cls.FechaTope = Convert.ToDateTime(reader["FechaTope"]);
                            _cls.ID = Convert.ToInt32(reader["ID"]);
                            _cls.Periodo = Convert.ToInt32(reader["Periodo"]);
                            _cls.Quincena = Convert.ToInt32(reader["Quincena"]);
                        }
                    }
                }
            }

            return _cls;
        }

        public static CAEA GetCaeaByID(int _id, SqlConnection _connection)
        {
            CAEA _cls = new CAEA();

            string _sql = @"SELECT ID, CAEA, FechaProceso, FechaDesde, FechaHasta, FechaTope, Periodo, Quincena 
                FROM FCCAEA WHERE ID = @id";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _connection;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros.
                _cmd.Parameters.AddWithValue("@id", _id);
                //Datareader
                using (SqlDataReader reader = _cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            _cls.Caea = reader["CAEA"].ToString();
                            _cls.FechaDesde = Convert.ToDateTime(reader["FechaDesde"]);
                            _cls.FechaHasta = Convert.ToDateTime(reader["FechaHasta"]);
                            _cls.FechaProceso = Convert.ToDateTime(reader["FechaProceso"]);
                            _cls.FechaTope = Convert.ToDateTime(reader["FechaTope"]);
                            _cls.ID = Convert.ToInt32(reader["ID"]);
                            _cls.Periodo = Convert.ToInt32(reader["Periodo"]);
                            _cls.Quincena = Convert.ToInt32(reader["Quincena"]);
                        }
                    }
                }
            }

            return _cls;
        }

        public static bool InsertCAEA(CAEA _cae, SqlConnection _connection)
        {
            bool _rta = false;
            CAEA _cls = new CAEA();

            string _sql = @"INSERT INTO FCCAEA (CAEA, FechaProceso, FechaDesde, FechaHasta, FechaTope, Periodo, Quincena)
                VALUES (@caea, @fechaproceso, @fechadesde, @fechahasta, @fechatope, @periodo, @quincena)";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _connection;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                //Parametros.
                _cmd.Parameters.AddWithValue("@caea", _cae.Caea);
                _cmd.Parameters.AddWithValue("@fechaproceso", _cae.FechaProceso);
                _cmd.Parameters.AddWithValue("@fechadesde", _cae.FechaDesde);
                _cmd.Parameters.AddWithValue("@fechahasta", _cae.FechaHasta);
                _cmd.Parameters.AddWithValue("@fechatope", _cae.FechaTope);
                _cmd.Parameters.AddWithValue("@periodo", _cae.Periodo);
                _cmd.Parameters.AddWithValue("@quincena", _cae.Quincena);
                //ExecuteNonQuery.
                int _insert = _cmd.ExecuteNonQuery();

                if (_insert > 0)
                    _rta = true;
                else
                    _rta = false;

            }

            return _rta;
        }

        public static CAEA ObtenerCAEA(int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest)
        {

            CAEA _caea = new CAEA();
            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                //_pathCertificado = _pathCertificado + @"\" + _nombreCertificado;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();


                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.wsAfip.SolicitarCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest,
                    strPasswordSecureString, _periodo, Convert.ToInt16(_quincena), false, out _lstErrores);

                if (_caeaWs.CAEA != null)
                {
                    string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
                    string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
                    string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
                    string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(fdesde);
                    _caea.FechaHasta = Convert.ToDateTime(fhasta);
                    _caea.FechaProceso = Convert.ToDateTime(fproce);
                    _caea.FechaTope = Convert.ToDateTime(ftope);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerCAEA Error: " + ex.Message);
            }
            return _caea;
        }

        public static CAEA ObtenerCAEA(int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest, out List<AfipDll.wsAfip.Errors> _lstErrores)
        {
            CAEA _caea = new CAEA();
            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.wsAfip.SolicitarCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest,
                    strPasswordSecureString, _periodo, Convert.ToInt16(_quincena), false, out _lstErrores);

                if (_caeaWs.CAEA != null)
                {
                    string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
                    string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
                    string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
                    string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(fdesde);
                    _caea.FechaHasta = Convert.ToDateTime(fhasta);
                    _caea.FechaProceso = Convert.ToDateTime(fproce);
                    _caea.FechaTope = Convert.ToDateTime(ftope);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerCAEA Error: " + ex.Message);
            }
            return _caea;
        }

        public static CAEA ObtenerCAEAMtxca(int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest)
        {

            CAEA _caea = new CAEA();
            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<WsMTXCA.Errors>();


                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.WsMTXCA.SolicitarCAEA(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest,
                    strPasswordSecureString, _periodo, Convert.ToInt16(_quincena), false, out _lstErrores);

                if (_caeaWs.CAEA != null)
                {
                    string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
                    string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
                    string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
                    string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(fdesde);
                    _caea.FechaHasta = Convert.ToDateTime(fhasta);
                    _caea.FechaProceso = Convert.ToDateTime(fproce);
                    _caea.FechaTope = Convert.ToDateTime(ftope);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerCAEA Error: " + ex.Message);
            }
            return _caea;
        }

        public static CAEA ConsultarCAEA(int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest)
        {
            CAEA _caea = new CAEA();

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                
                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                Int16 _orden = Convert.ToInt16(_quincena);

                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.wsAfip.ConsultarCAEANew(_pathCertificado, _pathTaFC, _pathLog,
                    _cuit, _esTest, strPasswordSecureString, _periodo, _orden, out _lstErrores);

                
                if (_caeaWs.CAEA != null)
                {
                    string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
                    string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
                    string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
                    string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(fdesde);
                    _caea.FechaHasta = Convert.ToDateTime(fhasta);
                    _caea.FechaProceso = Convert.ToDateTime(fproce);
                    _caea.FechaTope = Convert.ToDateTime(ftope);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                         "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ConsultarCAEA Error: " + ex.Message);
            }

            return _caea;
        }

        public static CAEA ConsultarCAEA(int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest, out List<AfipDll.wsAfip.Errors> _lstErrores)
        {
            CAEA _caea = new CAEA();

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                Int16 _orden = Convert.ToInt16(_quincena);

                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.wsAfip.ConsultarCAEANew(_pathCertificado, _pathTaFC, _pathLog,
                    _cuit, _esTest, strPasswordSecureString, _periodo, _orden, out _lstErrores);


                if (_caeaWs.CAEA != null)
                {
                    string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
                    string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
                    string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
                    string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(fdesde);
                    _caea.FechaHasta = Convert.ToDateTime(fhasta);
                    _caea.FechaProceso = Convert.ToDateTime(fproce);
                    _caea.FechaTope = Convert.ToDateTime(ftope);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                         "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ConsultarCAEA Error: " + ex.Message);
            }

            return _caea;
        }

        public static CAEA ConsultarCAEAMtxca(DateTime _fechaDesde, DateTime _fechaHasta, int _periodo, int _quincena,
            string _pathCertificado, string _pathTaFC, string _pathLog,
            string _cuit, string _password, string _nombreCertificado, bool _esTest)
        {
            CAEA _caea = new CAEA();

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();


                List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

                AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.WsMTXCA.ConsultarCAEA(_pathCertificado, _pathTaFC, _pathLog,
                    _cuit, _esTest, strPasswordSecureString, _fechaDesde, _fechaHasta, out _lstErrores);


                if (_caeaWs.CAEA != null)
                {                    
                    _caea.Caea = _caeaWs.CAEA;
                    _caea.FechaDesde = Convert.ToDateTime(_caeaWs.FchVigDesde);
                    _caea.FechaHasta = Convert.ToDateTime(_caeaWs.FchVigHasta);
                    _caea.FechaProceso = Convert.ToDateTime(_caeaWs.FchProceso);
                    _caea.FechaTope = Convert.ToDateTime(_caeaWs.FchTopeInf);
                    _caea.Periodo = _periodo;
                    _caea.Quincena = _quincena;
                }
                else
                {
                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
                         "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ConsultarCAEAMtxca Error: " + ex.Message);
            }

            return _caea;
        }

        //public static CAEA InformarCAEA(int _ptovta, string _caea,
        //    string _pathCertificado, string _pathTaFC, string _pathLog,
        //    string _cuit, string _password, string _nombreCertificado, bool _esTest, out List<AfipDll.wsAfip.Errors> _lstErrores)
        //{
            
        //    try
        //    {
        //        SecureString strPasswordSecureString = new SecureString();

        //        foreach (char c in _password) strPasswordSecureString.AppendChar(c);
        //        strPasswordSecureString.MakeReadOnly();
                
        //        AfipDll.wsAfip.CaeaSolicitar _caeaWs = AfipDll.wsAfip.ConsultarCAEANew(_pathCertificado, _pathTaFC, _pathLog,
        //            _cuit, _esTest, strPasswordSecureString, _periodo, _orden, out _lstErrores);


        //        if (_caeaWs.CAEA != null)
        //        {
        //            string fdesde = _caeaWs.FchVigDesde.Substring(6, 2) + "/" + _caeaWs.FchVigDesde.Substring(4, 2) + "/" + _caeaWs.FchVigDesde.Substring(0, 4);
        //            string fhasta = _caeaWs.FchVigHasta.Substring(6, 2) + "/" + _caeaWs.FchVigHasta.Substring(4, 2) + "/" + _caeaWs.FchVigHasta.Substring(0, 4);
        //            string fproce = _caeaWs.FchProceso.Substring(6, 2) + "/" + _caeaWs.FchProceso.Substring(4, 2) + "/" + _caeaWs.FchProceso.Substring(0, 4);
        //            string ftope = _caeaWs.FchTopeInf.Substring(6, 2) + "/" + _caeaWs.FchTopeInf.Substring(4, 2) + "/" + _caeaWs.FchTopeInf.Substring(0, 4);
        //            _caea.Caea = _caeaWs.CAEA;
        //            _caea.FechaDesde = Convert.ToDateTime(fdesde);
        //            _caea.FechaHasta = Convert.ToDateTime(fhasta);
        //            _caea.FechaProceso = Convert.ToDateTime(fproce);
        //            _caea.FechaTope = Convert.ToDateTime(ftope);
        //            _caea.Periodo = _periodo;
        //            _caea.Quincena = _quincena;
        //        }
        //        else
        //        {
        //            foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
        //            {
        //                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code +
        //                 "Descripcion: " + _er.Msg);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("ConsultarCAEA Error: " + ex.Message);
        //    }

        //    return _caea;
        //}
    }
}
