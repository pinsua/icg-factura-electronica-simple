﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace AfipDll
{
    public class WsaaNew
    {
        public string Token { get; set; }
        public string Sign { get; set; }
        public DateTime ExpirationTime { get; set; }

        public class LoginTicket
        {
            public UInt32 UniqueId; // Entero de 32 bits sin signo que identifica el requerimiento
            public DateTime GenerationTime; // Momento en que fue generado el requerimiento
            public DateTime ExpirationTime; // Momento en el que expira la solicitud
            public string Service; // Identificacion del WSN para el cual se solicita el TA
            public string Sign; // Firma de seguridad recibida en la respuesta
            public string Token; // Token de seguridad recibido en la respuesta
            public XmlDocument XmlLoginTicketRequest = null;
            public XmlDocument XmlLoginTicketResponse = null;
            public string RutaDelCertificadoFirmante;
            public string XmlStrLoginTicketRequestTemplate = "<loginTicketRequest><header><uniqueId></uniqueId><generationTime></generationTime><expirationTime></expirationTime></header><service></service></loginTicketRequest>";
            private bool _verboseMode = true;
            private static UInt32 _globalUniqueID = 0; // OJO! NO ES THREAD-SAFE

            /// <summary>
            /// Construye un Login Ticket obtenido del WSAA
            /// </summary>
            /// <param name="argServicio">Servicio al que se desea acceder</param>
            /// <param name="argUrlWsaa">URL del WSAA</param>
            /// <param name="argRutaCertX509Firmante">Ruta del certificado X509 (con clave privada) usado para firmar</param>
            /// <param name="argPassword">Password del certificado X509 (con clave privada) usado para firmar</param>
            /// <param name="argProxy">IP:port del proxy</param>
            /// <param name="argProxyUser">Usuario del proxy</param>''' 
            /// <param name="argProxyPassword">Password del proxy</param>
            /// <param name="argVerbose">Nivel detallado de descripcion? true/false</param>
            /// <remarks></remarks>
            public string ObtenerLoginTicketResponse(string argServicio, string argUrlWsaa, string argRutaCertX509Firmante,
                string pPathLog, SecureString argPassword, bool argVerbose)
            {
                const string ID_FNC = "[ObtenerLoginTicketResponse]";
                this.RutaDelCertificadoFirmante = argRutaCertX509Firmante;
                this._verboseMode = argVerbose;
                CertificadosX509Lib.VerboseMode = argVerbose;
                string cmsFirmadoBase64 = null;
                string loginTicketResponse = null;
                XmlNode xmlNodoUniqueId = default(XmlNode);
                XmlNode xmlNodoGenerationTime = default(XmlNode);
                XmlNode xmlNodoExpirationTime = default(XmlNode);
                XmlNode xmlNodoService = default(XmlNode);

                // PASO 1: Genero el Login Ticket Request
                try
                {
                    _globalUniqueID += 1;

                    XmlLoginTicketRequest = new XmlDocument();
                    XmlLoginTicketRequest.LoadXml(XmlStrLoginTicketRequestTemplate);

                    xmlNodoUniqueId = XmlLoginTicketRequest.SelectSingleNode("//uniqueId");
                    xmlNodoGenerationTime = XmlLoginTicketRequest.SelectSingleNode("//generationTime");
                    xmlNodoExpirationTime = XmlLoginTicketRequest.SelectSingleNode("//expirationTime");
                    xmlNodoService = XmlLoginTicketRequest.SelectSingleNode("//service");
                    xmlNodoGenerationTime.InnerText = DateTime.Now.AddMinutes(-10).ToString("s");
                    xmlNodoExpirationTime.InnerText = DateTime.Now.AddMinutes(+10).ToString("s");
                    xmlNodoUniqueId.InnerText = Convert.ToString(_globalUniqueID);
                    xmlNodoService.InnerText = argServicio;
                    this.Service = argServicio;

                    if (this._verboseMode) Console.WriteLine(XmlLoginTicketRequest.OuterXml);
                }
                catch (Exception excepcionAlGenerarLoginTicketRequest)
                {
                    throw new Exception(ID_FNC + "***Error GENERANDO el LoginTicketRequest : " + excepcionAlGenerarLoginTicketRequest.Message + excepcionAlGenerarLoginTicketRequest.StackTrace);
                }

                // PASO 2: Firmo el Login Ticket Request
                try
                {
                    if (this._verboseMode)
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***Leyendo certificado: " + RutaDelCertificadoFirmante);

                    X509Certificate2 certFirmante = CertificadosX509Lib.ObtieneCertificadoDesdeArchivo(RutaDelCertificadoFirmante, argPassword);

                    if (this._verboseMode)
                    {
                        Console.WriteLine(ID_FNC + "***Firmando: ");
                        Console.WriteLine(XmlLoginTicketRequest.OuterXml);
                    }

                    // Convierto el Login Ticket Request a bytes, firmo el msg y lo convierto a Base64
                    Encoding EncodedMsg = Encoding.UTF8;
                    byte[] msgBytes = EncodedMsg.GetBytes(XmlLoginTicketRequest.OuterXml);
                    byte[] encodedSignedCms = CertificadosX509Lib.FirmaBytesMensaje(msgBytes, certFirmante, pPathLog);
                    cmsFirmadoBase64 = Convert.ToBase64String(encodedSignedCms);
                }
                catch (Exception excepcionAlFirmar)
                {
                    throw new Exception(ID_FNC + "***Error FIRMANDO el LoginTicketRequest : " + excepcionAlFirmar.Message);
                }

                // PASO 3: Invoco al WSAA para obtener el Login Ticket Response
                try
                {
                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***Llamando al WSAA en URL: " + argUrlWsaa);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***Argumento en el request:" + cmsFirmadoBase64);
                    }

                    //Invocamos al metodo de la Afi
                    wsWasa.LoginCMSClient _cl = new wsWasa.LoginCMSClient();
                    _cl.Endpoint.Address = new System.ServiceModel.EndpointAddress(argUrlWsaa);
                    loginTicketResponse = _cl.loginCms(cmsFirmadoBase64);

                    if (this._verboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***LoguinTicketResponse: " + loginTicketResponse);
                    }

                }
                catch (Exception excepcionAlInvocarWsaa)
                {
                    throw new Exception(ID_FNC + "***Error INVOCANDO al servicio WSAA : " + excepcionAlInvocarWsaa.Message);
                }

                // PASO 4: Analizo el Login Ticket Response recibido del WSAA
                try
                {
                    XmlLoginTicketResponse = new XmlDocument();
                    XmlLoginTicketResponse.LoadXml(loginTicketResponse);

                    this.UniqueId = UInt32.Parse(XmlLoginTicketResponse.SelectSingleNode("//uniqueId").InnerText);
                    this.GenerationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText);
                    this.ExpirationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText);
                    this.Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText;
                    this.Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText;
                }
                catch (Exception excepcionAlAnalizarLoginTicketResponse)
                {
                    throw new Exception(ID_FNC + "***Error ANALIZANDO el LoginTicketResponse : " + excepcionAlAnalizarLoginTicketResponse.Message);
                }
                return loginTicketResponse;
            }
        }

        /// <summary>
        /// Libreria de utilidades para manejo de certificados
        /// </summary>
        /// <remarks></remarks>
        class CertificadosX509Lib
        {
            public static bool VerboseMode = false;

            /// <summary>
            /// Firma mensaje
            /// </summary>
            /// <param name="argBytesMsg">Bytes del mensaje</param>
            /// <param name="argCertFirmante">Certificado usado para firmar</param>
            /// <returns>Bytes del mensaje firmado</returns>
            /// <remarks></remarks>
            public static byte[] FirmaBytesMensaje(byte[] argBytesMsg, X509Certificate2 argCertFirmante, string pPathLog)
            {
                const string ID_FNC = "[FirmaBytesMensaje]";
                try
                {
                    // Pongo el mensaje en un objeto ContentInfo (requerido para construir el obj SignedCms)
                    ContentInfo infoContenido = new ContentInfo(argBytesMsg);
                    SignedCms cmsFirmado = new SignedCms(infoContenido);
                    //SignedCms cmsFirmado = new SignedCms(infoContenido, true);

                    // Creo objeto CmsSigner que tiene las caracteristicas del firmante
                    CmsSigner cmsFirmante = new CmsSigner(argCertFirmante);
                    cmsFirmante.IncludeOption = X509IncludeOption.EndCertOnly;
                    //var pk = argCertFirmante.GetPublicKeyString();
                    
                    if (VerboseMode)
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***Firmando bytes del mensaje...");

                    // Firmo el mensaje PKCS #7
                    cmsFirmado.ComputeSignature(cmsFirmante);

                    if (VerboseMode)
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), ID_FNC + "***OK mensaje firmado");

                    // Encodeo el mensaje PKCS #7.
                    return cmsFirmado.Encode();
                }
                catch (Exception excepcionAlFirmar)
                {
                    throw new Exception(ID_FNC + "***Error al firmar: " + excepcionAlFirmar.Message);
                }
            }

            /// <summary>
            /// Lee certificado de disco
            /// </summary>
            /// <param name="argArchivo">Ruta del certificado a leer.</param>
            /// <returns>Un objeto certificado X509</returns>
            /// <remarks></remarks>
            public static X509Certificate2 ObtieneCertificadoDesdeArchivo(string argArchivo, SecureString argPassword)
            {
                const string ID_FNC = "[ObtieneCertificadoDesdeArchivo]";
                X509Certificate2 objCert = new X509Certificate2();
                try
                {
                    if (argPassword.IsReadOnly())
                    {
                        objCert.Import(File.ReadAllBytes(argArchivo), argPassword, X509KeyStorageFlags.PersistKeySet);
                        //var kk = objCert.PrivateKey.ToXmlString(false);
                    }
                    else
                    {
                        objCert.Import(File.ReadAllBytes(argArchivo));
                    }
                    return objCert;
                }
                catch (Exception excepcionAlImportarCertificado)
                {
                    throw new Exception(ID_FNC + "***Error al leer certificado: " + excepcionAlImportarCertificado.Message);
                }
            }

            public static X509Certificate2 ObtieneCertificadoDesdeArchivo2(string argArchivo, SecureString argPassword)
            {
                const string ID_FNC = "[ObtieneCertificadoDesdeArchivo]";
                //            X509Certificate2 objCert = new X509Certificate2();
                try
                {
                    if (argPassword.IsReadOnly())
                    {
                        string pp = @"MIIDSzCCAjOgAwIBAgIISOV8I69N17AwDQYJKoZIhvcNAQENBQAwODEaMBgGA1UEAwwRQ29tcHV0YWRvcmVzIFRlc3QxDTALBgNVBAoMBEFGSVAxCzAJBgNVBAYTAkFSMB4XDTIxMDkyODE4NDk1N1oXDTIzMDkyODE4NDk1N1owMTEUMBIGA1UEAwwLcGtudWV2bzIwMjExGTAXBgNVBAUTEENVSVQgMjAyMjUwMDIxNTEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCp8qfDaVxO9RsHFRey76YMyQTNkghIv7XNl5krtTnrIAc5xPp+pGPiHPGGj2ZBoVwl1g83Rs/Q13jhxf+B2nm+yPio61utVY80TDp092BUO5koSH5nrft7PrI1eKAYxYFQigw14ghWgWeA2k0ktLCUAA19oPiJScydrWO1wTme87jaHc4u6Dp0w0E/5hWtN8bd7+fgV49RxRFvUyLoVNHmLrTClXrP8JVZHTPRJHzD8A6qm4IwUtoAKn1Tbas9P7GV7LttQfLmQtMklIAgPHvoLTJWmQIMIRVMCdb0HX0ozzJ1opGLM0F2f7xxy+X+aArcwJlxCZLzgGEr6sYwXpIZAgMBAAGjYDBeMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUs7LT//3put7eja8RIZzWIH3yT28wHQYDVR0OBBYEFJfBaybVNW3STL+0LgZzkaxkAOqcMA4GA1UdDwEB/wQEAwIF4DANBgkqhkiG9w0BAQ0FAAOCAQEADxWkfzIUgBPYPQOB+pHaVUnsF51GCJdPAV/1IM4bYGruN8pm0cYlqjKbCuqHVEB7ADo0SiPu05lQEwdwUJEYKfJ4P3CziIhZGKeeC/P0EPdTBKhNPS/2E5RK6v03b1TiipzEdRYAmym1u1uAriVLtKB3B+zzeJ8GsXK3qRAGs4IiauClwnKEI9uZUvNdoMKNLR9X6USI3fdPoqqA6n3BWKxn7iKx3O0G3NcDTQ10juv9V19ryGRodxWpMcous3MKhzFHvhcWkwABpOVdGLtHLqPTk/kiwuntkKb7oBm4cnGeNagYX2PdMYtXyIoFdI3MCR5PEIkVMqIM8OXMfLzogw==";
                        //string pp = @"MIIDSzCCAjOgAwIBAgIISOV8I69N17AwDQYJKoZIhvcNAQENBQAwODEaMBgGA1UEAwwRQ29tcHV0YWRvcmVzIFRlc3QxDTALBgNVBAoMBEFGSVAxCzAJBgNVBAYTAkFSMB4XDTIxMDkyODE4NDk1N1oXDTIzMDkyODE4NDk1N1owMTEUMBIGA1UEAwwLcGtudWV2bzIwMjExGTAXBgNVBAUTEENVSVQgMjAyMjUwMDIxNTEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCp8qfDaVxO9RsHFRey76YMyQTNkghIv7XNl5krtTnrIAc5xPp+pGPiHPGGj2ZBoVwl1g83Rs/Q13jhxf+B2nm+yPio61utVY80TDp092BUO5koSH5nrft7PrI1eKAYxYFQigw14ghWgWeA2k0ktLCUAA19oPiJScydrWO1wTme87jaHc4u6Dp0w0E/5hWtN8bd7+fgV49RxRFvUyLoVNHmLrTClXrP8JVZHTPRJHzD8A6qm4IwUtoAKn1Tbas9P7GV7LttQfLmQtMklIAgPHvoLTJWmQIMIRVMCdb0HX0ozzJ1opGLM0F2f7xxy+X+aArcwJlxCZLzgGEr6sYwXpIZAgMBAAGjYDBeMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUs7LT//3put7eja8RIZzWIH3yT28wHQYDVR0OBBYEFJfBaybVNW3STL+0LgZzkaxkAOqcMA4GA1UdDwEB/wQEAwIF4DANBgkqhkiG9w0BAQ0FAAOCAQEADxWkfzIUgBPYPQOB+pHaVUnsF51GCJdPAV/1IM4bYGruN8pm0cYlqjKbCuqHVEB7ADo0SiPu05lQEwdwUJEYKfJ4P3CziIhZGKeeC/P0EPdTBKhNPS/2E5RK6v03b1TiipzEdRYAmym1u1uAriVLtKB3B+zzeJ8GsXK3qRAGs4IiauClwnKEI9uZUvNdoMKNLR9X6USI3fdPoqqA6n3BWKxn7iKx3O0G3NcDTQ10juv9V19ryGRodxWpMcous3MKhzFHvhcWkwABpOVdGLtHLqPTk/kiwuntkKb7oBm4cnGeNagYX2PdMYtXyIoFdI3MCR5PEIkVMqIM8OXMfLzogw==";
                        X509Certificate2 objCert = new X509Certificate2(Convert.FromBase64String(pp), argPassword, X509KeyStorageFlags.Exportable);
                        //X509Certificate2 objCert = new X509Certificate2(Convert.FromBase64String(pp));
                        var cert = new X509Certificate2(argArchivo, argPassword, X509KeyStorageFlags.Exportable);
                        var certBytes = cert.RawData;
                        var certString = Convert.ToBase64String(certBytes);

                        var certBytes2 = objCert.RawData;
                        var certString2 = Convert.ToBase64String(certBytes2);

                        //var privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
                        //objCert.PrivateKey = privateKey;

                        return objCert;
                    }
                    else
                    {
                        X509Certificate2 objCert = new X509Certificate2(Convert.FromBase64String(argArchivo));
                        return objCert;
                    }
                }
                catch (Exception excepcionAlImportarCertificado)
                {
                    throw new Exception(ID_FNC + "***Error al leer certificado: " + excepcionAlImportarCertificado.Message);
                }
            }
        }


        public class Wsaa
        {
            public string Token { get; set; }
            public string Sign { get; set; }
            public DateTime ExpirationTime { get; set; }
            /// <summary>
            /// URL servicio Wasa de Test.
            /// </summary>
            const string TEST_URLWSAAWSDL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL";
            /// <summary>
            /// URL serviciop Waa de Produccion.
            /// </summary>
            const string DEFAULT_URLWSAAWSDL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl";
            const bool DEFAULT_VERBOSE = true;

            public static WsaaNew ObtenerDatosWsaaNew(string pServicio, string pPath, string pPathTa, string pPathLog, bool IsTest, string _Cuit, SecureString _pass)
            {
                string strUrlWassaTest = TEST_URLWSAAWSDL;
                string strUrlWsaaWsdl = DEFAULT_URLWSAAWSDL;
                string strIdServicioNegocio = pServicio;
                string strRutaCertSigner = pPath;
                bool blnVerboseMode = DEFAULT_VERBOSE;
                string strTApath = pPathTa + @"\" + _Cuit + ".xml";

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WsaaNew wa = new WsaaNew();

                WsaaNew.LoginTicket objTicketRespuesta;
                string strTicketRespuesta;

                try
                {
                    if (blnVerboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Servicio a acceder: " + strIdServicioNegocio);
                        if (IsTest)
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWassaTest);
                        else
                            LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***URL del WSAA: " + strUrlWsaaWsdl);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Ruta del certificado: " + strRutaCertSigner);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Modo verbose: " + blnVerboseMode.ToString());
                    }

                    objTicketRespuesta = new WsaaNew.LoginTicket();

                    if (blnVerboseMode)
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***Accediendo a " + strUrlWsaaWsdl);
                    }

                    if (IsTest)
                        strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWassaTest, strRutaCertSigner, pPathLog, _pass, blnVerboseMode);
                    else
                        strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(strIdServicioNegocio, strUrlWsaaWsdl, strRutaCertSigner, pPathLog, _pass, blnVerboseMode);

                    if (blnVerboseMode)
                    {
                        //Ponemos los datos en el Log.
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***CONTENIDO DEL TICKET RESPUESTA:");
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Token: " + objTicketRespuesta.Token);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Sign: " + objTicketRespuesta.Sign);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " GenerationTime: " + Convert.ToString(objTicketRespuesta.GenerationTime));
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " ExpirationTime: " + Convert.ToString(objTicketRespuesta.ExpirationTime));
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " Service: " + objTicketRespuesta.Service);
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), " UniqueID: " + Convert.ToString(objTicketRespuesta.UniqueId));
                        //Pasamos los datos a la clase.
                        wa.Token = objTicketRespuesta.Token;
                        wa.Sign = objTicketRespuesta.Sign;
                        wa.ExpirationTime = objTicketRespuesta.ExpirationTime;
                        objTicketRespuesta.XmlLoginTicketResponse.Save(strTApath);
                    }
                }
                catch (Exception excepcionAlObtenerTicket)
                {
                    LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "***EXCEPCION AL OBTENER TICKET:" + excepcionAlObtenerTicket.Message);
                }

                return wa;
            }

            public static bool ValidoTA(string pPath, string pPathTa, string pPathLog, Wsaa _cls, string _Cuit)
            {
                //variable con la respuesta.
                bool booRta = false;
                //instanciamos el dataset
                System.Data.DataSet dtsTA = new System.Data.DataSet();
                //Vemos si ya tenemos el TA.xml
                string strTApath = pPathTa + @"\" + _Cuit + ".xml";

                if (System.IO.File.Exists(strTApath))
                {
                    System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                    try
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Leemos el archivo: " + strTApath);
                        dtsTA.ReadXml(fsReadXml);
                        fsReadXml.Close();
                        //Vemos si esta activo.
                        DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        if (dttExpiration > DateTime.Now)
                        {
                            _cls.Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                            _cls.Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                            _cls.ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                            booRta = true;
                        }
                        else
                        {
                            booRta = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Escribimos el Log.
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                        //Como se produjo un error lo vuelvo a pedir.
                        fsReadXml.Close();
                        booRta = false;
                    }

                }
                else
                {
                    booRta = false;
                }
                dtsTA.Dispose();
                return booRta;
            }

            public static bool ValidoTANew(string pPath, string pPathTa, string pPathLog, WsaaNew _cls, string _Cuit)
            {
                //variable con la respuesta.
                bool booRta = false;
                //instanciamos el dataset
                System.Data.DataSet dtsTA = new System.Data.DataSet();
                string strTApath = pPathTa + @"\" + _Cuit + ".xml";

                if (System.IO.File.Exists(strTApath))
                {
                    System.IO.FileStream fsReadXml = new System.IO.FileStream(strTApath, System.IO.FileMode.Open);
                    try
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Leemos el archivo: " + strTApath);
                        dtsTA.ReadXml(fsReadXml);
                        fsReadXml.Close();
                        //Vemos si esta activo.
                        DateTime dttExpiration = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        if (dttExpiration > DateTime.Now)
                        {
                            _cls.Token = dtsTA.Tables["credentials"].Rows[0]["Token"].ToString();
                            _cls.Sign = dtsTA.Tables["credentials"].Rows[0]["Sign"].ToString();
                            _cls.ExpirationTime = DateTime.Parse(dtsTA.Tables["header"].Rows[0]["ExpirationTime"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                            booRta = true;
                        }
                        else
                        {
                            booRta = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Escribimos el Log.
                        LogFile.ErrorLog(LogFile.CreatePath(pPathLog), "Error al leer el archivo , " + strTApath + " Error: " + ex.Message.ToString());
                        //Como se produjo un error lo vuelvo a pedir.
                        fsReadXml.Close();
                        booRta = false;
                    }
                }
                else
                {
                    booRta = false;
                }
                dtsTA.Dispose();
                return booRta;
            }
        }
    }
 }

