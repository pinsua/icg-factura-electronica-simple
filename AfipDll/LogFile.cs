﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
//using System.Windows.Forms;

namespace AfipDll
{
    public class LogFile
    { 
        /// <summary>
        /// Metodo que devuelve un string con el nombre del archivo mas la fecha
        /// </summary>
        /// <param name="pFileName">archivo</param>
        /// <returns></returns>
        public static string CreatePath(string _pathLog)
        {
            string strFileName = _pathLog + "\\" + System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            //Ver que pongo
            //string strFileName = "AfipDll";
            return strFileName + DateTime.Now.ToShortDateString().Replace('/', '-') + ".log";
        }

        public static string KnowPath()
        {
            string strFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            return strFileName;
        }
        public static void ErrorLog(string pPathName, string pErrMsg)
        {
            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            StreamWriter sw = new StreamWriter(pPathName, true);
            sw.WriteLine(sLogFormat + pErrMsg);
            sw.Flush();
            sw.Close();
        }
    }
}
