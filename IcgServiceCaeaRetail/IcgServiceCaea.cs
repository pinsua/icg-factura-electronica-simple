﻿using AfipDll;
using IcgFceDll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IcgServiceCaea
{
    public partial class IcgServiceCaea : ServiceBase
    {
        private string _connectionStr;
        private string _cuit;
        private string _certificado;
        private string _password;
        private bool _istest;
        private int _intervalo;
        private bool _isMtxca;
        private string _codigoIVA;
        private string _codigoIIBB;
        private bool _ObtenerCAEA = false;
        private string _pathConsola;
        private string _server;
        private string _database;
        private string _user;
        private DateTime _fecha = DateTime.Now.AddDays(-1);
        private string _licenciaICG;
        private string _PtoVtaCAE;
        private bool _keyIsOk = false;
        private string _icgPlataforma = "";
        private int _caja;

        //Email
        private string _destinatarios;
        private string _emailEnvio;
        private string _emailAsunto;
        private bool _emailCredentials;
        private string _emailPassword;
        private int _emailPort;
        private string _emailSmtp;
        private bool _emailSSL;

        private System.Timers.Timer myTimer;

        public IcgServiceCaea()
        {
            InitializeComponent();
            Thread.Sleep(10000);
            EventoSistema = new EventLog();
            if(!EventLog.SourceExists("IcgServiceCaea"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "IcgServiceCaea", "Application");
            }
            EventoSistema.Source = "IcgServiceCaea";
            EventoSistema.Log = "Application";
            //Configuración
            try
            {
                //Recupero la plataforma
                _icgPlataforma = Properties.Settings.Default.IcgPlataforma;
                EventoSistema.WriteEntry("IcgServiceCaea. Plataforma." + _icgPlataforma);
                //Recupero el directorio de la consola.
                _pathConsola = Properties.Settings.Default.PathConsola;
                EventoSistema.WriteEntry("IcgServiceCaea. PathConsola." + _pathConsola);
                if (_icgPlataforma.ToUpper() == "RETAIL")
                {
                    //RETAIL
                    //Abrimos el archivo de configuración.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(_pathConsola, "IcgRetailFCEConsola.exe"));
                    _codigoIVA = config.AppSettings.Settings["CodigoIVA"].Value;
                    _codigoIIBB = config.AppSettings.Settings["CodigoIIBB"].Value;
                    _certificado = config.AppSettings.Settings["NombreCertificado"].Value;
                    _cuit = config.AppSettings.Settings["CUIT"].Value;
                    _istest = Convert.ToBoolean(config.AppSettings.Settings["IsTest"].Value);
                    _password = config.AppSettings.Settings["PassWord"].Value;
                    _PtoVtaCAE = config.AppSettings.Settings["PtoVtaCAE"].Value;
                    _server = config.AppSettings.Settings["Server"].Value;
                    //_user = "ICGAdmin";
                    _user = "sa";
                    _database = config.AppSettings.Settings["DataBase"].Value;
                    _isMtxca = Convert.ToBoolean(config.AppSettings.Settings["IsMtxca"].Value);
                    _licenciaICG = config.AppSettings.Settings["LicenciaIcg"].Value;
                }
                else
                {
                    //REST
                    //Abrimos el archivo de configuración.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(_pathConsola, "IcgRestFCEConsola.exe"));
                    //Obtengo las key's
                    String[] _keys = config.AppSettings.Settings.AllKeys;
                    _codigoIVA = _keys.Contains("CodigoIVA") ? config.AppSettings.Settings["CodigoIVA"].Value : "0";
                    _codigoIIBB = _keys.Contains("CodigoIIBB") ? config.AppSettings.Settings["CodigoIIBB"].Value : "=";
                    _certificado = _keys.Contains("NombreCertificado") ? config.AppSettings.Settings["NombreCertificado"].Value : "";
                    _cuit = _keys.Contains("CUIT") ? config.AppSettings.Settings["CUIT"].Value : "";
                    _istest = _keys.Contains("IsTest") ? Convert.ToBoolean(config.AppSettings.Settings["IsTest"].Value) : true;
                    _password = _keys.Contains("PassWord") ? config.AppSettings.Settings["PassWord"].Value : "";
                    _PtoVtaCAE = _keys.Contains("PtoVtaCAE") ? config.AppSettings.Settings["PtoVtaCAE"].Value : "";
                    _server = _keys.Contains("Server") ? config.AppSettings.Settings["Server"].Value : "";
                    _user = "ICGAdmin";
                    _database = _keys.Contains("Database") ? config.AppSettings.Settings["Database"].Value : "";
                    _isMtxca = false;
                    _caja = _keys.Contains("Caja") ? Convert.ToInt32(config.AppSettings.Settings["Caja"].Value) : 0;
                    _licenciaICG = _keys.Contains("LicenciaIcg") ? config.AppSettings.Settings["LicenciaIcg"].Value : "";
                }
                //
                _connectionStr = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                _intervalo = Properties.Settings.Default.Intervalo;
                _destinatarios = Properties.Settings.Default.Destinatarios;
                _emailEnvio = Properties.Settings.Default.EmailEnviador;
                _emailAsunto = Properties.Settings.Default.EmailAsunto;
                _emailCredentials = Properties.Settings.Default.EmailCredenciales;
                _emailPassword = Properties.Settings.Default.EmailPassword;
                _emailPort = Properties.Settings.Default.EmailPort;
                _emailSmtp = Properties.Settings.Default.EmailSmtp;
                _emailSSL = Properties.Settings.Default.EmailSSL;

                EventoSistema.WriteEntry("IcgServiceCaea. Configuración leída correctamente.");
            }
            catch (Exception ex)
            {
                EventoSistema.WriteEntry("Error en el Start del servicio. " + ex.Message);
                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                    "Se produjo el siguiente error al iniciar el servicio IcgServiceCaea." + Environment.NewLine + "Error: " + ex.Message,
                    _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                OnStop();
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                //Escribimos el evento.
                EventoSistema.WriteEntry("Iniciado el servicio IcgServiceCaea.");
                //inicio el Timer
                this.myTimer = new System.Timers.Timer(_intervalo);
                myTimer.Elapsed += new System.Timers.ElapsedEventHandler(myTimer_Elapsed);
                //Habilitar el Timer.
                myTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                EventoSistema.WriteEntry("Error en el Start del servicio. " + ex.Message);
                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                    "Se produjo el siguiente error al iniciar el servicio IcgServiceCaea." + Environment.NewLine + "Error: " + ex.Message,
                    _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                OnStop();
            }
        }

        protected override void OnStop()
        {
            //Paramos el timer.
            myTimer.Stop();
            //Escribimos el evento.
            EventoSistema.WriteEntry("Detenido el servicio IcgServiceCaea.");
        }

        private void myTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Detiene el Timer
            myTimer.Enabled = false;
            //Lanzamos el proceso para obtener CAEA.
            if (_fecha.Date < DateTime.Now.Date)
            {
                //if (ValidarKey())
                //{
                    if (!_ObtenerCAEA)
                    {
                        _ObtenerCAEA = ObtenerCAEA();
                    }
                //}
                _fecha = DateTime.Now;
            }
            //if (_keyIsOk)
            if (true)
            {
                if (_icgPlataforma.ToUpper() == "RETAIL")
                {
                    //Lanzamos el proceso para informar CAEA.
                    RetailInformaCAEA();
                }
                else
                {
                    RestInformaCAEA();
                }
            }
            else
            {
                EventoSistema.WriteEntry("Licencia no valida. Comuniquese con ICG Argentina.");
                //recupero la ubicacion del ejecutable.
                string _pathApp = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                //Cargo la ubicaciones.
                string _pathLog = _pathApp + "\\Log";
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Licencia no valida.Comuniquese con ICG Argentina.");
            }
            //habilita el Timer nuevamente.
            myTimer.Enabled = true;
        }

        private bool ObtenerCAEA()
        {
            //VAlido las carpetas.
            //recupero la ubicacion del ejecutable.
            string _pathApp = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            //Cargo la ubicaciones.
            string _pathCertificado = _pathConsola + "\\Certificado";
            string _pathLog = _pathApp + "\\Log";
            string _pathTaFC = _pathConsola + "\\TAFC";
            try
            {
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el Certificado
                if (!Directory.Exists(_pathCertificado))
                    Directory.CreateDirectory(_pathCertificado);

                if (!String.IsNullOrEmpty(_certificado))
                {
                    if (!File.Exists(_pathCertificado + "\\" + _certificado))
                    {
                        EventoSistema.WriteEntry("El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        return false;
                    }
                    else
                        _pathCertificado = _pathCertificado + "\\" + _certificado;
                }

                //Valido si existe el CAEA para el periodo en curso.
                //1- recupero el CAEA.
                int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                int _quincena = 0;

                if(DateTime.Now.Day >= 16)
                    _quincena = 2;
                else
                {
                    if(DateTime.Now.Day <= 15)
                        _quincena = 1;
                }
                //Si tengo quincena lo solicito.
                if (_quincena > 0)
                {
                    using (SqlConnection _connection = new SqlConnection())
                    {
                        _connection.ConnectionString = _connectionStr;
                        _connection.Open();
                        //2- recupero el CAEA de la base de datos
                        AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                        //Veo si ya tengo CAEA para la quincena.
                        if (_caea.Caea == null)
                        {
                            //No lo tengo lo solicito
                            if (_isMtxca)
                            {
                                _caea = AfipDll.CAEA.ObtenerCAEAMtxca(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _istest);
                            }
                            else
                            {
                                _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _istest);
                            }

                            if (_caea.Caea == null)
                            {
                                //No lo tengo lo consulto.
                                if (_isMtxca)
                                {
                                    DateTime _fDesde;
                                    DateTime _fHasta;

                                    if (_quincena == 1)
                                    {
                                        _fDesde = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                                        _fHasta = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 15);
                                    }
                                    else
                                    {
                                        //Asi obtenemos el primer dia del mes actual
                                        DateTime _PrimerDiaDelMes = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                                        _fDesde = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 16);
                                        _fHasta = _PrimerDiaDelMes.AddMonths(1).AddDays(-1);
                                    }
                                    _caea = CAEA.ConsultarCAEAMtxca(_fDesde, _fHasta, _periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _istest);
                                }
                                else
                                {
                                    _caea = CAEA.ConsultarCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _certificado, _istest);
                                }
                            }

                            if (_caea.Caea != null)
                                CAEA.InsertCAEA(_caea, _connection);
                            else
                                throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error: " + ex.Message, ex);
                EventoSistema.WriteEntry("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + "Para mas detalles revise el log.");
                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                    "Se produjo el siguiente error al iniciar el servicio IcgServiceCaea." + Environment.NewLine + "Error: " + ex.Message,
                    _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                return false;
            }
        }

        private void RetailInformaCAEA()
        {
            //recupero la ubicacion del ejecutable.
            string _pathApp = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            //string _pathApp = _pathConsola;
            //Cargo la ubicaciones.
            string _pathCertificado = _pathConsola + "\\Certificado\\" + _certificado;
            string _pathLog = _pathApp + "\\Log";
            string _pathTaFC = _pathConsola + "\\TAFC";
            List<String> _lstError = new List<string>();
            try
            {

                using (SqlConnection _connection = new SqlConnection())
                {
                    _connection.ConnectionString = _connectionStr;
                    _connection.Open();
                    //Recuperamos los CAEA sin informar
                    List<CaeaSI> _lst = CaeaSinInfomar.GetCompobantesSinInformar(_connection);
                    //Vemos si tenemos algo
                    if (_lst.Count > 0)
                    {
                        //Tengo una fila seleccionada.
                        foreach (CaeaSI csi in _lst)
                        {
                            //Valido si existe el CAEA para el periodo en curso.
                            //1- recupero el CAEA.
                            int _periodo = Convert.ToInt32(csi.fecha.Year.ToString() + csi.fecha.Month.ToString().PadLeft(2, '0'));
                            int _quincena = 0;

                            if (csi.fecha.Day >= 16)
                                _quincena = 2;
                            else
                                _quincena = 1;

                            //1- recupero las FC con CAEA
                            AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                            //VEmos si tenemos CAEA
                            if (_caea.Caea != null)
                            {
                                if (csi.estadoFE.ToUpper() == "SININFORMAR")
                                {
                                    //Vemos si es MTXCA
                                    if (_isMtxca)
                                    {
                                        //Busco el Tiquet.
                                        DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(csi.serie, csi.numero, csi.n, _connection);
                                        //Vemos si tenemos nro Fiscal.
                                        if (_fsr.NumeroFiscal > 0)
                                        {
                                            string _error = "";
                                            //Factura Electronica MTXCA.
                                            DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(csi.serie, csi.numero, csi.n, _connection);
                                            RetailService.Mtxca.InformarComprobanteCAEAPorServicio(_Fc, Convert.ToInt32(_codigoIVA), csi.seriefiscal, _password, _istest,
                                                _pathLog, _pathCertificado, _pathTaFC, _cuit, EventoSistema, _connection, out _error);
                                            //Si tengo error lo sumo a la lista.
                                            if (!String.IsNullOrEmpty(_error))
                                                _lstError.Add(_error);
                                        }
                                    }
                                    else
                                    {
                                        //Ver como se genera el request en la FCE.
                                        //Busco el Tiquet.
                                        DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(csi.serie, csi.numero, csi.n, _connection);
                                        //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                        if (_fsr.NumeroFiscal > 0)
                                        {
                                            //Factura Electronica comun.
                                            DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(csi.serie, csi.numero, csi.n, _connection);
                                            // Array de IVA
                                            List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, Convert.ToInt32(_codigoIVA), _connection);
                                            _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                            //Array de tributos
                                            List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, Convert.ToInt32(_codigoIIBB), _connection, _Fc.TotalBruto);
                                            _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                            //Array de Importes no gravados.
                                            List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, Convert.ToInt32(_codigoIVA), _connection);
                                            _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                            //Asigno el punto de venta de CAE.
                                            _Fc.PtoVta = csi.seriefiscal;
                                            //Validamos los datos de la FC.
                                            DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);
                                            //Armamos la password como segura
                                            SecureString strPasswordSecureString = new SecureString();
                                            foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                            strPasswordSecureString.MakeReadOnly();
                                            //Vemos si esta disponible el servicio de AFIP.
                                            string _dummy = wsAfip.TestDummy(_pathLog, _istest);
                                            if (_dummy.ToUpper() == "OK")
                                            {
                                                List<wsAfip.Errors> _lstErroresAFIP = new List<wsAfip.Errors>();
                                                //Armo el string de la cabecera.
                                                AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                                                _cabReq.CantReg = 1;
                                                _cabReq.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                                                _cabReq.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                                                string strCabReg = "1|" + _Fc.cCodAfip.ToString() + "|" + _Fc.PtoVta.ToString();
                                                //Cargamos la clase Detalle.
                                                List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = RetailService.CargarDetalleCAEAAfip(_Fc, _fsr.NumeroFiscal, _caea);
                                                //Armo el string de IVA.
                                                List<AfipDll.wsAfipCae.AlicIva> _lstIva = RetailService.ArmarIVAAfip(_Iva);
                                                //Armos el string de Tributos.
                                                List<AfipDll.wsAfipCae.Tributo> _TributosRequest = RetailService.ArmaTributos(_Tributos);
                                                //Agregamos el IVA
                                                _detReq[0].Iva = _lstIva.ToArray();
                                                //Agregamos los tributos.
                                                if (_TributosRequest.Count > 0)
                                                    _detReq[0].Tributos = _TributosRequest.ToArray();
                                                //Comprobante asociado
                                                //Verifico que si es una NC/ND.
                                                if (Convert.ToInt32(_Fc.cCodAfip) == 203 || Convert.ToInt32(_Fc.cCodAfip) == 208 || Convert.ToInt32(_Fc.cCodAfip) == 213
                                                    || Convert.ToInt32(_Fc.cCodAfip) == 202 || Convert.ToInt32(_Fc.cCodAfip) == 207 || Convert.ToInt32(_Fc.cCodAfip) == 212
                                                    || Convert.ToInt32(_Fc.cCodAfip) == 3 || Convert.ToInt32(_Fc.cCodAfip) == 8 || Convert.ToInt32(_Fc.cCodAfip) == 13
                                                    || Convert.ToInt32(_Fc.cCodAfip) == 2 || Convert.ToInt32(_Fc.cCodAfip) == 7 || Convert.ToInt32(_Fc.cCodAfip) == 12)
                                                {
                                                    //Armamos la lista de comprobantes Asociados.
                                                    List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RetailService.ArmarCbteAsocCAEA(_Fc, _cuit, _connection);
                                                    //Verifico que tengo por lo menos 1 comprobante asociado.
                                                    if (_ComprobantesAsociados.Count() == 0)
                                                    {
                                                        DateTime _dtDesde = _Fc.fecha.AddDays(-30);
                                                        string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                                                        string _hasta = _Fc.fecha.Year.ToString().PadLeft(2, '0') + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                        AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                                                        _detReq[0].PeriodoAsoc = _cls;
                                                    }
                                                    else
                                                    {
                                                        _detReq[0].CbtesAsoc = _ComprobantesAsociados.ToArray();
                                                    }
                                                }
                                                //Armamos el Request.
                                                AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                                                _req.FeCabReq = _cabReq;
                                                _req.FeDetReq = _detReq.ToArray();
                                                try
                                                {
                                                    List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _istest, strPasswordSecureString, _req, false, out _lstErroresAFIP);

                                                    if (_Respuesta.Count() > 0)
                                                    {
                                                        //Recupero el nombre de la terminal.
                                                        string _terminal = Environment.MachineName;
                                                        foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                                                        {
                                                            switch (cri.Resultado)
                                                            {
                                                                case "A":
                                                                    {
                                                                        //Grabo el dato en facturascamposlibres.
                                                                        bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                        if (!_rta)
                                                                        {
                                                                            EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                        }
                                                                        //Inserto en Rem_transacciones.
                                                                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, csi.serie, csi.numero, csi.n, _connection);
                                                                        break;
                                                                    }
                                                                case "P":
                                                                    {
                                                                        //Informo.
                                                                        bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                        if (!_rta)
                                                                        {
                                                                            EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                                ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                        }
                                                                        //Inserto en Rem_transacciones.
                                                                        DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, csi.serie, csi.numero, csi.n, _connection);
                                                                        break;
                                                                    }
                                                                case "R":
                                                                    {
                                                                        //Informo.
                                                                        EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                                ") Fue rechazado por AFIP.Error" + cri.Observaciones);
                                                                        //Agrego el error a la lista.
                                                                        _lstError.Add("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                                ") Fue rechazado por AFIP.Error" + cri.Observaciones);
                                                                        break;
                                                                    }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                        _lstError.Add("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                        foreach (AfipDll.wsAfip.Errors _er in _lstErroresAFIP)
                                                        {
                                                            LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code + Environment.NewLine +
                                                                "Descripcion: " + _er.Msg);
                                                            _lstError.Add("Codigo de error: " + _er.Code + Environment.NewLine +
                                                                "Descripcion: " + _er.Msg);

                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    EventoSistema.WriteEntry("Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() +
                                                        ", N: " + csi.n + ") fue rechazado por la AFIP el ser infomado." + Environment.NewLine + "Error: " + ex.Message, ex);
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    string _body = "No existe un CAEA para el periodo en curso.";
                                    //Envio Mail
                                    CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                                            _body, _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                                }
                                catch (Exception ex)
                                {
                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al enviar el mail: " + ex.Message, ex);
                                }
                            }
                        }
                        //Veo si tenggo algo que informar
                        if (_lstError.Count > 0)
                        {
                            string _body = "";
                            //Recorro la lista de errores.
                            foreach (String str in _lstError)
                            {
                                if (String.IsNullOrEmpty(_body))
                                    _body = "<p>" + str + "</p>";
                                else
                                    _body = _body + "<p>" + str + "</p>";
                            }
                            try
                            {
                                //Envio Mail
                                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                                        _body, _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                            }
                            catch(Exception ex)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al enviar el mail: " + ex.Message, ex);
                            }
                        }
                    }
                }               
            }
            catch (Exception ex)
            {                
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error: " + ex.Message, ex);
                EventoSistema.WriteEntry("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + "Para más detalle revise el log.");
                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                    "Se produjo el siguiente error al informar los CAEA." + Environment.NewLine + "Error: " + ex.Message,
                    _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
            }
        }

        private void RestInformaCAEA()
        {
            //recupero la ubicacion del ejecutable.
            string _pathApp = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            //string _pathApp = _pathConsola;
            //Cargo la ubicaciones.
            string _pathCertificado = _pathConsola + "\\Certificado\\" + _certificado;
            string _pathLog = _pathApp + "\\Log";
            string _pathTaFC = _pathConsola + "\\TAFC";
            List<String> _lstError = new List<string>();
            try
            {

                using (SqlConnection _connection = new SqlConnection())
                {
                    _connection.ConnectionString = _connectionStr;
                    _connection.Open();
                    //Recuperamos los CAEA sin informar
                    List<CaeaSI> _lst = Rest.GetCaeasSinInformar(_caja, _connection);
                    //Vemos si tenemos algo
                    if (_lst.Count > 0)
                    {
                        //Tengo una fila seleccionada.
                        foreach (CaeaSI csi in _lst)
                        {
                            //Valido si existe el CAEA para el periodo en curso.
                            //1- recupero el CAEA.
                            int _periodo = Convert.ToInt32(csi.fecha.Year.ToString() + csi.fecha.Month.ToString().PadLeft(2, '0'));
                            int _quincena = 0;

                            if (csi.fecha.Day >= 16)
                                _quincena = 2;
                            else
                                _quincena = 1;

                            AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                            //Vemos si tenemos CAEA
                            if (_caea.Caea != null)
                            {
                                if (csi.estadoFE.ToUpper() == "SININFORMAR")
                                {
                                    //Busco el Tiquet.
                                    Modelos.Tiquet _tiquet = IcgFceDll.RestService.GetTiquet(csi.serie, csi.numero, csi.n, _connection);
                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_tiquet.NumeroFiscal > 0)
                                    {
                                        Modelos.DatosFactura _Fc = RestService.GetDatosFactura(csi.fo, csi.serie, csi.numero, csi.n, csi.codVendedor, _connection);
                                        // Array de IVA
                                        List<Modelos.TiquetsTot> _Iva = RestService.GetTotalesIva(_Fc.serie, _Fc.n, _Fc.numero, Convert.ToInt32(_codigoIVA), _connection);
                                        _Fc.totIVA = _Iva.Sum(x => x.TotIva);
                                        //Array de tributos
                                        List<Modelos.TiquetsTot> _Tributos = new List<Modelos.TiquetsTot>();
                                        _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                        //Array de Importes no gravados.
                                        List<Modelos.TiquetsTot> _NoGravado = RestService.GetTotalesIvaNoGravado(_Fc.serie, _Fc.n, _Fc.numero, Convert.ToInt32(_codigoIVA), _connection);
                                        _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                        //Asigno el punto de venta de CAE.
                                        _Fc.ptoVta = Convert.ToInt32(csi.seriefiscal);
                                        //Validamos los datos de la FC.
                                        RestService.ValidarDatosFactura(_Fc);
                                        //Armamos la password como segura
                                        SecureString strPasswordSecureString = new SecureString();
                                        foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                        strPasswordSecureString.MakeReadOnly();
                                        //Vemos si esta disponible el servicio de AFIP.
                                        string _dummy = wsAfip.TestDummy(_pathLog, _istest);
                                        if (_dummy.ToUpper() == "OK")
                                        {
                                            List<wsAfip.Errors> _lstErroresAFIP = new List<wsAfip.Errors>();
                                            //Armo el string de la cabecera.
                                            AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                                            _cabReq.CantReg = 1;
                                            _cabReq.CbteTipo = Convert.ToInt16(_Fc.codAfipComprobante);
                                            _cabReq.PtoVta = Convert.ToInt16(_Fc.ptoVta);
                                            string strCabReg = "1|" + _Fc.codAfipComprobante.ToString() + "|" + _Fc.ptoVta.ToString();
                                            //Cargamos la clase Detalle.
                                            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = RestService.CargarDetalleCAEAAfip(_Fc, _tiquet.NumeroFiscal, _caea);
                                            //Armo el string de IVA.
                                            List<AfipDll.wsAfipCae.AlicIva> _lstIva = RestService.ArmarIVAAfip(_Iva);
                                            //Armos el string de Tributos.
                                            List<AfipDll.wsAfipCae.Tributo> _TributosRequest = RestService.ArmaTributos(_Tributos);
                                            //Agregamos el IVA
                                            _detReq[0].Iva = _lstIva.ToArray();
                                            //Agregamos los tributos.
                                            if (_TributosRequest.Count > 0)
                                                _detReq[0].Tributos = _TributosRequest.ToArray();
                                            //Comprobante asociado
                                            //Verifico que si es una NC/ND.
                                            if (_Fc.codAfipComprobante == 203 || _Fc.codAfipComprobante == 208 || _Fc.codAfipComprobante == 213
                                                || _Fc.codAfipComprobante == 202 || _Fc.codAfipComprobante == 207 || _Fc.codAfipComprobante == 212
                                                || _Fc.codAfipComprobante == 3 || _Fc.codAfipComprobante == 8 || _Fc.codAfipComprobante == 13
                                                || _Fc.codAfipComprobante == 2 || _Fc.codAfipComprobante == 7 || _Fc.codAfipComprobante == 12)
                                            {
                                                //Armamos la lista de comprobantes Asociados.
                                                List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RestService.GetCbteAsocs(csi.fo, csi.serie, csi.numero, csi.n, _cuit, _connection);
                                                //Verifico que tengo por lo menos 1 comprobante asociado.
                                                if (_ComprobantesAsociados.Count() == 0)
                                                {
                                                    DateTime _dtDesde = _Fc.fecha.AddDays(-30);
                                                    string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                                                    string _hasta = _Fc.fecha.Year.ToString().PadLeft(2, '0') + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                                                    _detReq[0].PeriodoAsoc = _cls;
                                                }
                                                else
                                                {
                                                    _detReq[0].CbtesAsoc = _ComprobantesAsociados.ToArray();
                                                }
                                            }
                                            //Armamos el Request.
                                            AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                                            _req.FeCabReq = _cabReq;
                                            _req.FeDetReq = _detReq.ToArray();
                                            try
                                            {
                                                List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _istest, strPasswordSecureString, _req, false, out _lstErroresAFIP);

                                                if (_Respuesta.Count() > 0)
                                                {
                                                    //Recupero el nombre de la terminal.
                                                    string _terminal = Environment.MachineName;
                                                    foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                                                    {
                                                        switch (cri.Resultado)
                                                        {
                                                            case "A":
                                                                {
                                                                    //Grabo el dato en facturascamposlibres.
                                                                    //bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                    bool _rta = RestService.TiquetsVentaCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, csi.serie, csi.numero, csi.n, _connection);
                                                                    break;
                                                                }
                                                            case "P":
                                                                {
                                                                    //Informo.
                                                                    //bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                    bool _rta = RestService.TiquetsVentaCamposLibresInformoCAEA(csi.serie, csi.numero, csi.n, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, csi.serie, csi.numero, csi.n, _connection);
                                                                    break;
                                                                }
                                                            case "R":
                                                                {
                                                                    //Informo.
                                                                    EventoSistema.WriteEntry("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") Fue rechazado por AFIP.Error" + cri.Observaciones);
                                                                    //Agrego el error a la lista.
                                                                    _lstError.Add("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                            ") Fue rechazado por AFIP.Error" + cri.Observaciones);
                                                                    break;
                                                                }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                        ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                    _lstError.Add("El comprobante (SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                        ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                    foreach (AfipDll.wsAfip.Errors _er in _lstErroresAFIP)
                                                    {
                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Codigo de error: " + _er.Code + Environment.NewLine +
                                                            "Descripcion: " + _er.Msg);
                                                        _lstError.Add("Codigo de error: " + _er.Code + Environment.NewLine +
                                                            "Descripcion: " + _er.Msg);

                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                EventoSistema.WriteEntry("Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() + ", N: " + csi.n +
                                                                        ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + csi.serie + ", NUMERO: " + csi.numero.ToString() +
                                                    ", N: " + csi.n + ") fue rechazado por la AFIP el ser infomado." + Environment.NewLine + "Error: " + ex.Message, ex);
                                            }
                                        }

                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    string _body = "No existe un CAEA para el periodo en curso.";
                                    //Envio Mail
                                    CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                                            _body, _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                                }
                                catch (Exception ex)
                                {
                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al enviar el mail: " + ex.Message, ex);
                                }
                            }
                        }
                        //Veo si tenggo algo que informar
                        if (_lstError.Count > 0)
                        {
                            string _body = "";
                            //Recorro la lista de errores.
                            foreach (String str in _lstError)
                            {
                                if (String.IsNullOrEmpty(_body))
                                    _body = "<p>" + str + "</p>";
                                else
                                    _body = _body + "<p>" + str + "</p>";
                            }
                            try
                            {
                                //Envio Mail
                                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                                        _body, _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
                            }
                            catch (Exception ex)
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al enviar el mail: " + ex.Message, ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error: " + ex.Message, ex);
                EventoSistema.WriteEntry("Se produjo el siguiente error: " + ex.Message + Environment.NewLine + "Para más detalle revise el log.");
                CommonService.Email.Enviar("", _destinatarios, "", _emailEnvio, "Servicio CAEA RETAIL", _emailAsunto,
                    "Se produjo el siguiente error al informar los CAEA." + Environment.NewLine + "Error: " + ex.Message,
                    _emailCredentials, _emailPassword, _emailPort, _emailSmtp, _emailSSL);
            }
        }

        private bool ValidarKey()
        {
            bool _rta = false;
            string _pathApp = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            //Cargo la ubicaciones.
            string _pathLog = _pathApp + "\\Log";
            
            //Validamos licencia via WebService.
            string _Msg = "";
            string _Key = "";
            if (_icgPlataforma.ToUpper() == "RETAIL")
                IcgFceDll.RetailService.Licencia.GetKey(_server, _database, _cuit, _PtoVtaCAE, out _Key, out _Msg);
            else
                IcgFceDll.RestService.Licencia.ValidarKey(_PtoVtaCAE, _pathConsola, _connectionStr, out _Key, out _Msg);
            if (!String.IsNullOrEmpty(_Key))
            {
                if (_Key == _licenciaICG)
                    _rta = true;
                else
                {
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(_pathConsola, "IcgRetailFCEConsola.exe"));
                    //Licencia
                    config.AppSettings.Settings["LicenciaIcg"].Value = "";
                    config.Save();
                    _rta = false;
                }
            }
            else
            {
                if (_Msg.StartsWith("-1 Error:"))
                    _rta = true;
                else
                {
                    //Guardamos los datos.
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(_pathConsola, "IcgRetailFCEConsola.exe"));
                    //Licencia
                    config.AppSettings.Settings["LicenciaIcg"].Value = "";
                    config.Save();
                    _rta = false;
                }
            }
            
            if (!String.IsNullOrEmpty(_Msg))
            {
                if (!_Msg.StartsWith("-1 Error:"))
                {
                    EventoSistema.WriteEntry("Se produjo el siguiente error: "  + _Msg);
                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error : " + _Msg);
                }
            }

            _keyIsOk = _rta;
            return _rta;
        }
    }
}
