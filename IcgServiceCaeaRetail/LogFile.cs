﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgServiceCaea
{
    class LogFile
    {
        private static bool Log = true;
        /// <summary>
        /// Metodo que devuelve un string con el nombre del archivo mas la fecha
        /// </summary>
        /// <param name="pFileName">archivo</param>
        /// <returns></returns>
        public static string CreatePath()
        {
            string strFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            return strFileName + DateTime.Now.ToShortDateString().Replace('/', '-') + ".log";
        }

        public static string CreatePath(string _pathLog)
        {
            string strFileName = _pathLog + "\\" + System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            //Ver que pongo
            //string strFileName = "AfipDll";
            return strFileName + DateTime.Now.ToShortDateString().Replace('/', '-') + ".log";
        }

        public static void ErrorLog(string pPathName, string pErrMsg)
        {
            if (!Log)
                return;
            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            StreamWriter sw = new StreamWriter(pPathName, true);
            sw.WriteLine(sLogFormat + pErrMsg);
            sw.Flush();
            sw.Close();
        }

        public static void ErrorLog(string pPathName, string pErrMsg, Exception ex)
        {
            if (!Log)
                return;
            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            StreamWriter sw = new StreamWriter(pPathName, true);
            sw.WriteLine(sLogFormat + pErrMsg);


            sw.WriteLine("-----------------------------------------------------------------------------");
            sw.WriteLine();

            while (ex != null)
            {
                sw.WriteLine(ex.GetType().FullName);
                sw.WriteLine("Message : " + ex.Message);
                sw.WriteLine("StackTrace : " + ex.StackTrace);

                ex = ex.InnerException;
            }

            sw.Flush();
            sw.Close();

        }
    }
}
