﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcgServiceCaea
{
    public class CaeaSI
    {
        public string serie { get; set; }
        public int numero { get; set; }
        public string n { get; set; }
        public int fo { get; set; }
        public string cae { get; set; }
        public string vtocae { set; get; }
        public string estadoFE { get; set; }
        public DateTime fecha { get; set; }
        public string seriefiscal { get; set; }
        public string seriefiscal2 { get; set; }
        public int numerofiscal { get; set; }
        public int codVendedor { get; set; }

    }

    public class CaeaSinInfomar
    {
        public static List<CaeaSI> GetCompobantesSinInformar(SqlConnection _connection)
        {
            List<CaeaSI> _lst = new List<CaeaSI>();

            string _sql = @"SELECT ALBVENTACAB.NUMSERIEFAC, ALBVENTACAB.NUMFAC, ALBVENTACAB.NFAC, ALBVENTACAB.FO, 
                    FACTURASVENTACAMPOSLIBRES.CAE, FACTURASVENTACAMPOSLIBRES.ERROR_CAE,
                    FACTURASVENTACAMPOSLIBRES.VTO_CAE, FACTURASVENTACAMPOSLIBRES.ESTADO_FE, FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO,
                    ALBVENTACAB.FECHA, FACTURASVENTASERIESRESOL.SERIEFISCAL1 as SERIEFISCAL, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL
                    FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND 
                    FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N 
                    INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE 
                    INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC 
                    inner join FACTURASVENTACAMPOSLIBRES on FACTURASVENTASERIESRESOL.NUMSERIE = FACTURASVENTACAMPOSLIBRES.NUMSERIE and FACTURASVENTASERIESRESOL.NUMFACTURA = FACTURASVENTACAMPOSLIBRES.NUMFACTURA and FACTURASVENTASERIESRESOL.N = FACTURASVENTACAMPOSLIBRES.N
                    Where (FACTURASVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR') and
					(FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO is null OR FACTURASVENTACAMPOSLIBRES.CAEA_INFORMADO = '') 
					and isnull(FACTURASVENTASERIESRESOL.NUMEROFISCAL,0) > 0
                    ORDER BY FACTURASVENTASERIESRESOL.NUMEROFISCAL";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _connection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    //Datareader
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                CaeaSI _cls = new CaeaSI();
                                _cls.cae = reader["CAE"].ToString();
                                _cls.estadoFE = reader["ESTADO_FE"].ToString();
                                _cls.fecha = Convert.ToDateTime(reader["FECHA"]);
                                _cls.fo = Convert.ToInt32(reader["FO"]);
                                _cls.n = reader["NFAC"].ToString();
                                _cls.numero = Convert.ToInt32(reader["NUMFAC"]);
                                _cls.numerofiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                _cls.serie = reader["NUMSERIEFAC"].ToString();
                                _cls.seriefiscal = reader["SERIEFISCAL"].ToString();
                                _cls.seriefiscal2 = reader["SERIEFISCAL2"].ToString();
                                _cls.vtocae = reader["VTO_CAE"].ToString();
                                _lst.Add(_cls);
                            }
                        }
                    }
                }
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error en el proceso GetCompobantesSinInformar." + 
                    Environment.NewLine + "ERROR: " + ex.Message);
            }
        }
    }

    public class Rest
    {
        public static List<CaeaSI> GetCaeasSinInformar(int _caja, SqlConnection _connection)
        {
            try
            {
                //string _sql = @"SELECT TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.FO, TIQUETSVENTACAMPOSLIBRES.CAE, TIQUETSVENTACAMPOSLIBRES.ERROR_CAE,
                //    TIQUETSVENTACAMPOSLIBRES.VTO_CAE, TIQUETSVENTACAMPOSLIBRES.ESTADO_FE, TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO,
                //    TIQUETSCAB.FECHA, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.CODVENDEDOR
                //    from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                //    and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                //    Where (TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR')
                //    and(TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO is null
                //    OR TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO = '') AND TIQUETSCAB.CAJA = @caja ORDER BY TIQUETSCAB.NUMEROFISCAL";
                string _sql = @"SELECT TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.FO, TIQUETSVENTACAMPOSLIBRES.CAE, TIQUETSVENTACAMPOSLIBRES.ERROR_CAE,
                    TIQUETSVENTACAMPOSLIBRES.VTO_CAE, TIQUETSVENTACAMPOSLIBRES.ESTADO_FE, TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO,
                    TIQUETSCAB.FECHA, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.CODVENDEDOR
                    from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                    and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                    Where (TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR')
                    AND TIQUETSCAB.CAJA = @caja ORDER BY TIQUETSCAB.NUMEROFISCAL";

                List<CaeaSI> _lst = new List<CaeaSI>();

                using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
                {
                    _cmd.Parameters.AddWithValue("@caja", _caja);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                CaeaSI _cls = new CaeaSI()
                                {
                                    cae = _reader["CAE"].ToString(),
                                    estadoFE = _reader["ESTADO_FE"].ToString(),
                                    fecha = Convert.ToDateTime(_reader["FECHA"].ToString()),
                                    fo = Convert.ToInt32(_reader["FO"].ToString()),
                                    n = _reader["N"].ToString(),
                                    numero = Convert.ToInt32(_reader["NUMERO"]),
                                    numerofiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]),
                                    serie = _reader["SERIE"].ToString(),
                                    seriefiscal = _reader["SERIEFISCAL"].ToString(),
                                    seriefiscal2 = _reader["SERIEFISCAL"].ToString(),
                                    vtocae = _reader["VTO_CAE"].ToString(),
                                    codVendedor = Convert.ToInt32(_reader["CODVENDEDOR"])
                                };
                                _lst.Add(_cls);
                            }
                        }
                    }
                }
                return _lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
