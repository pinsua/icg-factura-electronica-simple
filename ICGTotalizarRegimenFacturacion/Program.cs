﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ICGTotalizarRegimenFacturacion
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);            


            //Consulta si existe el archivo.Caso contrario da error.
            if (File.Exists("TotalRegimenFac.xml"))
            {
                //Leo el xml
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("TotalRegimenFac.xml");
                //Recupero el tag.
                XmlNodeList _serverXml = xDoc.GetElementsByTagName("regimenfac");
                string _regimen = _serverXml[0].InnerText;
                //Vemos que tenemos en el regimen de facturción
                if (_regimen != "N")
                {
                    //Abro el Form.                    
                    using (frmMain form = new frmMain())
                    {
                        DialogResult dr = form.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            //Modifico el arhchivo.
                            _serverXml[0].InnerText = "N";
                            xDoc.Save("TotalRegimenFac.xml");
                        }

                    }
                }
            }
        }
    }
}
