﻿
namespace ICGTotalizarRegimenFacturacion
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbQuestion = new System.Windows.Forms.GroupBox();
            this.btSi = new System.Windows.Forms.Button();
            this.btNo = new System.Windows.Forms.Button();
            this.gbQuestion.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbQuestion
            // 
            this.gbQuestion.Controls.Add(this.btNo);
            this.gbQuestion.Controls.Add(this.btSi);
            this.gbQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbQuestion.Location = new System.Drawing.Point(257, 153);
            this.gbQuestion.Name = "gbQuestion";
            this.gbQuestion.Size = new System.Drawing.Size(289, 135);
            this.gbQuestion.TabIndex = 0;
            this.gbQuestion.TabStop = false;
            this.gbQuestion.Text = "Desea Modificar?";
            // 
            // btSi
            // 
            this.btSi.Location = new System.Drawing.Point(38, 48);
            this.btSi.Name = "btSi";
            this.btSi.Size = new System.Drawing.Size(99, 39);
            this.btSi.TabIndex = 0;
            this.btSi.Text = "Si";
            this.btSi.UseVisualStyleBackColor = true;
            this.btSi.Click += new System.EventHandler(this.btSi_Click);
            // 
            // btNo
            // 
            this.btNo.Location = new System.Drawing.Point(160, 48);
            this.btNo.Name = "btNo";
            this.btNo.Size = new System.Drawing.Size(99, 39);
            this.btNo.TabIndex = 1;
            this.btNo.Text = "No";
            this.btNo.UseVisualStyleBackColor = true;
            this.btNo.Click += new System.EventHandler(this.btNo_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gbQuestion);
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina S.A.";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.gbQuestion.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbQuestion;
        private System.Windows.Forms.Button btNo;
        private System.Windows.Forms.Button btSi;
    }
}