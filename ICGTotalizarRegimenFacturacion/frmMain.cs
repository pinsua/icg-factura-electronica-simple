﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ICGTotalizarRegimenFacturacion
{
    public partial class frmMain : Form
    {
        public bool _rta = false;
        public frmMain()
        {
            InitializeComponent();            
        }

        private void btSi_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            gbQuestion.Left = (this.ClientRectangle.Width - gbQuestion.Width) / 2;
            gbQuestion.Top = (this.ClientRectangle.Height - gbQuestion.Height) / 2;            
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            gbQuestion.Left = (this.ClientRectangle.Width - gbQuestion.Width) / 2;
            gbQuestion.Top = (this.ClientRectangle.Height - gbQuestion.Height) / 2;
        }
    }
}
