﻿using AfipDll;
using IcgFceDll;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailFCEInformaCAEA
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //REcupero los datos de la configuracion
            string _conexion = Properties.Settings.Default.Conection;
            string _cuit = Properties.Settings.Default.Cuit;
            string _certificado = Properties.Settings.Default.Certificado;
            string _password = Properties.Settings.Default.Password;
            bool _IsTest = Convert.ToBoolean(Properties.Settings.Default.IsTest);
            string _ptoVtaCaea = Properties.Settings.Default.PtoVtaCaea;
            int _codigoIVA = Convert.ToInt32(Properties.Settings.Default.CodigoIVA);
            int _codigoIIBB = Convert.ToInt32(Properties.Settings.Default.CodigoIIBB);
            //Variables
            string _pathApp;
            string _pathLog;
            string _pathCertificado;
            string _pathTaFC;
            string _pathTaFCE;

            //VAlido las carpetas.
            //recupero la ubicacion del ejecutable.
            _pathApp = Application.StartupPath.ToString();
            //Cargo la ubicaciones.
            _pathCertificado = _pathApp + "\\Certificado";
            _pathLog = _pathApp + "\\Log";
            _pathTaFC = _pathApp + "\\TAFC";
            _pathTaFCE = _pathApp + "\\TAFCE";

            try
            {
                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCertificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCertificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCertificado + "). Por favor coloque el certificado en este directorio para poder continuar.");

                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(_certificado))
                    {
                        if (!File.Exists(_pathCertificado + "\\" + _certificado))
                        {
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                            return;
                        }
                        else
                            _pathCertificado = _pathCertificado + "\\" + _certificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);

                //Valido si existe el CAEA para el periodo en curso.
                //1- recupero el CAEA.
                int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                int _quincena = 0;

                if (DateTime.Now.Day >= 15)
                    _quincena = 2;
                else
                    _quincena = 1;

                using (SqlConnection _connection = new SqlConnection())
                {
                    _connection.ConnectionString = _conexion;
                    _connection.Open();
                    //1- recupero las FC con CAEA
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                    //VEmos si tenemos CAEA
                    if (_caea.Caea != null)
                    {
                        //Busco los comprobantes sin el mismo CAEA sin informar
                        List<DataAccess.ComprobantesConCAEA> _lst = DataAccess.IcgCAEA.GetComprobantesConCAEA(_caea.Caea, _connection);
                        //Recorro los datos de la lista.
                        foreach(DataAccess.ComprobantesConCAEA _comp in _lst)
                        {
                            //Ver como se genera el request en la FCE.
                            //Busco el comprobante en FacturasVentaSerieResol.
                            DataAccess.FacturasVentaSeriesResol _fsr = DataAccess.FacturasVentaSeriesResol.GetTiquet(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                            //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                            if (_fsr.NumeroFiscal > 0)
                            {
                                //Factura Electronica comun.
                                DataAccess.DatosFactura _Fc = DataAccess.DatosFactura.GetDatosFactura(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                List<DataAccess.FacturasVentasTot> _Iva = DataAccess.FacturasVentasTot.GetTotalesIva(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                _Fc.TotIva = _Iva.Sum(x => x.TotIva);
                                List<DataAccess.FacturasVentasTot> _Tributos = DataAccess.FacturasVentasTot.GetTotalesTributos(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIIBB, _connection, _Fc.TotalBruto);
                                _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                List<DataAccess.FacturasVentasTot> _NoGravado = DataAccess.FacturasVentasTot.GetTotalesNoGravado(_Fc.NumSerie, _Fc.N, _Fc.NumFactura, _codigoIVA, _connection);
                                _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                //Asigno el punto de venta de CAE.
                                _Fc.PtoVta = _ptoVtaCaea;
                                DataAccess.FuncionesVarias.ValidarDatosFactura(_Fc);
                                //Convierto la password
                                SecureString strPasswordSecureString = new SecureString();
                                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                strPasswordSecureString.MakeReadOnly();

                                switch (_Fc.Estado.ToUpper())
                                {
                                    case "FACTURADO":
                                        {
                                            List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                                            //Armo el string de la cabecera.
                                            AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                                            _cabReq.CantReg = 1;
                                            _cabReq.CbteTipo = Convert.ToInt16(_Fc.cCodAfip);
                                            _cabReq.PtoVta = Convert.ToInt16(_Fc.PtoVta);
                                                string strCabReg = "1|" + _Fc.cCodAfip + "|" + _Fc.PtoVta;
                                            //Cargamos la clase Detalle.
                                            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = DataAccess.FuncionesVarias.CargarDetalleCAEAAfip(_Fc, _fsr.NumeroFiscal, _caea);
                                            //Armo el string de IVA.
                                            List<AfipDll.wsAfipCae.AlicIva> _lstIva = DataAccess.FuncionesVarias.ArmarIVAAfip(_Iva);
                                            //Armos el string de Tributos.
                                            List<AfipDll.wsAfipCae.Tributo> _TributosRequest = DataAccess.FuncionesVarias.ArmaTributosAfip(_Tributos);
                                            //Agregamos el IVA
                                            _detReq[0].Iva = _lstIva.ToArray();
                                            //Agregamos los tributos.
                                            _detReq[0].Tributos = _TributosRequest.ToArray();
                                            //Armamos el Request.
                                            AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                                            _req.FeCabReq = _cabReq;
                                            _req.FeDetReq = _detReq.ToArray();

                                            try
                                            {
                                                List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _IsTest, strPasswordSecureString, _req, false, out _lstErrores);

                                                if (_Respuesta.Count() > 0)
                                                {
                                                    //Recupero el nombre de la terminal.
                                                    string _terminal = Environment.MachineName;
                                                    foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                                                    {
                                                        switch (cri.Resultado)
                                                        {
                                                            case "A":
                                                                {
                                                                    //Grabo el dato en facturascamposlibres.
                                                                    bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                    break;
                                                                }
                                                            case "P":
                                                                {
                                                                    //Informo.
                                                                    bool _rta = DataAccess.FacturasVentaCamposLibres.FacturasVentasCamposLibresInformoCAEA(_comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    else
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                                ") se informo con la observacion: " + cri.Observaciones);
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _comp.Numserie, _comp.Numfactura, _comp.N, _connection);
                                                                    break;
                                                                }
                                                            case "R":
                                                                {
                                                                    //Informo.
                                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") fue rechazado por la AFIP el ser infomado.");
                                                                    break;
                                                                }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") fue rechazado por la AFIP el ser infomado. A continuación se detallan los errores: ");
                                                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                                                    {
                                                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                                            "Descripcion: " + _er.Msg);
                                                    }
                                                }                                                
                                            }
                                            catch (Exception ex)
                                            {
                                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                                            }
                                            break;
                                        }
                                    case "RECUPERAR":
                                        {
                                            ////Recupero el nro grabado como pendiente.
                                            //DataAccess.FacturasVentaCamposLibres _fvcl = DataAccess.FacturasVentaCamposLibres.GetRecord(_connection, _Fc);
                                            //if (String.IsNullOrEmpty(_fvcl.CodBarras))
                                            //    _fvcl.CodBarras = "0";
                                            //if (Convert.ToInt32(_fvcl.CodBarras) >= 0)
                                            //{
                                            //    //Recuperamos el CAE.
                                            //    wsAfip.clsCaeRecupero _rec = wsAfip.ConsultaComprobanteNew(_pathCertificado, _pathTa, _pathLog, Convert.ToInt32(_Fc.PtoVta),
                                            //        Convert.ToInt32(_Fc.cCodAfip), strCUIT, Convert.ToInt32(_fvcl.CodBarras), _EsTest, strPasswordSecureString);
                                            //    //Analizamos la respuesta.
                                            //    switch (_rec.Resultado.ToUpper())
                                            //    {
                                            //        case "A":
                                            //            {
                                            //                string strCodBarra;
                                            //                DataAccess.FuncionesVarias.GenerarCodigoBarra(strCUIT, _Fc.cCodAfip, _Fc.PtoVta, _rec.Cae, _rec.FechaVto, out strCodBarra);
                                            //                //Modificamos el registro en FacturasCamposLibres
                                            //                DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc, _rec.Cae, _rec.ErrorMsj, _rec.FechaVto, strCodBarra, _connection);
                                            //                //Solo si tenemos CAE insertamos en FacturasVentasSeriesResol.
                                            //                if (!String.IsNullOrEmpty(_rec.Cae))
                                            //                {
                                            //                    DataAccess.FacturasVentaSeriesResol.Insertar_FacturasVentaSerieResol(_Fc, Convert.ToInt32(_fvcl.CodBarras.Trim()), _connection);
                                            //                }

                                            //                break;
                                            //            }
                                            //        case "R":
                                            //            {
                                            //                if (_rec.ErrorMsj == "No existen datos en nuestros registros para los parametros ingresados."
                                            //                    || _rec.ErrorMsj == "Campo CbteNro no lo ingreso o supera el tamaño permitido de 8 caracteres numericos.")
                                            //                {
                                            //                    //Modificamos el registro en FacturasCamposLibres.
                                            //                    DataAccess.FacturasVentaCamposLibres.ModificoFacturasVentaCampoLibres(_Fc, "", "", "", "", _connection);
                                            //                }
                                            //                break;
                                            //            }
                                            //    }
                                            //}
                                            break;
                                        }
                                    default:
                                        {
                                            break;
                                        }
                                }

                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante SERIE: " + _comp.Numserie + ", NUMERO: " + _comp.Numfactura.ToString() + ", N: " + _comp.N +
                                                                        ") No fue infomado porque no pose numerofiscal.");
                            }
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error: " + ex.Message);
            }
        }
    }
}
