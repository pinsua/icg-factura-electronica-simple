﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AfipDll;

namespace IcgRestFCEConsola
{
    public partial class frmAddComprobanteAsociado : Form
    {
        public List<AfipDll.wsMtxca.ComprobanteAsociadoType> _lstNew = new List<AfipDll.wsMtxca.ComprobanteAsociadoType>();
        public List<AfipDll.wsAfipCae.CbteAsoc> _lstNewComun = new List<AfipDll.wsAfipCae.CbteAsoc>();
        public string _Cuit;
        public int _tipoComprobante;
        bool isMTXCA;
        public frmAddComprobanteAsociado(int _tipoCompro, string _cuit, bool _isMTXCA)
        {
            InitializeComponent();
            _Cuit = _cuit;
            _tipoComprobante = _tipoCompro;
            isMTXCA = _isMTXCA;
        }

        private void frmAddComprobanteAsociado_Load(object sender, EventArgs e)
        {
            txtCuitComprobante.Enabled = false;
            txtCuitComprobante.Text = _Cuit;
        }

        private void cboTipoComprobante_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //int _tipoComprobante = Convert.ToInt32(cboTipoComprobante.SelectedItem.ToString().Split('_')[0]);
            //if (_tipoComprobante == 203 || _tipoComprobante == 208 || _tipoComprobante == 213 
            //    || _tipoComprobante == 202 || _tipoComprobante == 207 || _tipoComprobante == 212)
            //{
            //    txtCuitComprobante.Enabled = true;
            //}
        }
        

        private void txtCuitComprobante_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                char[] nums = txtCuitComprobante.Text.ToCharArray();
                int total = 0;
                for (int i = 0; i < mult.Length; i++)
                {
                    total += int.Parse(nums[i].ToString()) * mult[i];
                }
                int resto = total % 11;
                int _digito1 = resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
                int _digitorecibido = Convert.ToInt16(txtCuitComprobante.Text.Substring(txtCuitComprobante.Text.Length - 1));

                if (_digito1 != _digitorecibido)
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw new Exception("ValidarDigitoCuit. Error: " + ex.Message);
            }
        }

        private void txtPtoVtaComprobante_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void btnAcept_Click(object sender, EventArgs e)
        {
            if (isMTXCA)
            {
                AfipDll.wsMtxca.ComprobanteAsociadoType _cls = new AfipDll.wsMtxca.ComprobanteAsociadoType();
                _cls.codigoTipoComprobante = Convert.ToInt16(cboTipoComprobante.SelectedItem.ToString().Split('_')[0]);
                _cls.fechaEmision = dtpFechaComprobante.Value;
                _cls.fechaEmisionSpecified = true;
                _cls.numeroPuntoVenta = Convert.ToInt32(txtPtoVtaComprobante.Text);
                _cls.numeroComprobante = Convert.ToInt32(txtNumeroComprobante.Text);
                if (_tipoComprobante == 203 || _tipoComprobante == 208 || _tipoComprobante == 213
                    || _tipoComprobante == 202 || _tipoComprobante == 207 || _tipoComprobante == 212)
                {
                    _cls.cuit = Convert.ToInt64(txtCuitComprobante.Text);
                    _cls.cuitSpecified = true;
                }
                _lstNew.Add(_cls);
            }
            else
            {
                AfipDll.wsAfipCae.CbteAsoc _cls = new AfipDll.wsAfipCae.CbteAsoc();
                _cls.CbteFch = dtpFechaComprobante.Value.Year.ToString() + dtpFechaComprobante.Value.Month.ToString().PadLeft(2, '0') + dtpFechaComprobante.Value.Day.ToString().PadLeft(2, '0'); ;
                _cls.Cuit = _Cuit;
                _cls.Nro = Convert.ToInt32(txtNumeroComprobante.Text);
                _cls.PtoVta = Convert.ToInt32(txtPtoVtaComprobante.Text);
                _cls.Tipo = Convert.ToInt16(cboTipoComprobante.SelectedItem.ToString().Split('_')[0]);
                _lstNewComun.Add(_cls);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Si cancela el comprobante no se podrá fiscalizar." + Environment.NewLine + "Confirma que desea cancelar?", "ICG Argentina",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
