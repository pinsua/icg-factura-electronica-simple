﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRestFCEConsola
{
    public partial class frmChangeNroFiscalCAEA : Form
    {
        private string serie;
        private int numeroFac;
        private string N;
        private SqlConnection connection;
        private string pathLog;
        public frmChangeNroFiscalCAEA(string _serie, int _numero, string _n,
            int _numeroFiscalActual, string _pathLog, SqlConnection sqlConnection)
        {
            InitializeComponent();
            txtSerie.Text = _serie;
            serie = _serie;
            txtNumero.Text = _numero.ToString();
            numeroFac = _numero;
            txtFiscalActual.Text = _numeroFiscalActual.ToString();
            N = _n;
            connection = sqlConnection;
            pathLog = _pathLog;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //Vemos si tenemos nuevo numero.
            if (!String.IsNullOrEmpty(txtFiscalNuevo.Text))
            {
                int _nuevo = 0;
                //Vemos si es un entero.
                try 
                {
                    _nuevo = Convert.ToInt32(txtFiscalNuevo.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("El Nuevo Numero Fiscal debe ser un numero entero.",
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                try
                {
                    IcgFceDll.RestService.TransaccionChangeNroFiscal(serie, numeroFac, N, _nuevo, pathLog, connection);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Se produjo un error al modificar le Numero Fiscal.",
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AfipDll.LogFile.ErrorLog(pathLog, ex.Message);
                }
                //salgo
                this.Close();
            }
            else
                MessageBox.Show("Debe ingresar un Nuevo Numero Fiscal.",
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
