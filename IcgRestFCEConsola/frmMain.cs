﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AfipDll;
using IcgFceDll;
using Newtonsoft.Json;
using RestSharp;
//using IcgFceDll.SiTefService;
//using IcgFceDll.SiTefService;

namespace IcgRestFCEConsola
{
    public partial class frmMain : Form
    {
        public static SqlConnection _connection = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;

        public string _ip = "";
        
        public string _serverConfig = "";
        public string _userConfig = "";
        public string _catalogConfig = "";
        public bool _monotributo = false;
        public bool _hasarLog = false;
        public string _terminal = "";
        public string _tktregalo1 = "";
        public string _tktregalo2 = "";
        public string _tktregalo3 = "";
        public string _pathIrsa = "";
        public string _keyIcg = "";

        public string strConnection;
        public bool _KeyIsOk;
        public static InfoIrsa _irsa = new InfoIrsa();

        public static string _pathApp;
        public static string _pathCertificado;
        public static string _pathLog;
        public static string _pathTaFC;
        public static string _pathTaFCE;
        //settings
        public int _codigoIVA;
        public int _codigoIIBB;
        public static string _nombreCertificado;
        public static string _cuit;
        public static bool _esTest;
        public static string _password;
        public static string _PtoVtaManual;
        public static string _PtoVtaCAE;
        public static string _PtoVtaCAEA;
        public static string _server;
        public static string _database;
        public static string _user;
        public static bool _soloCaea;
        public static string _licenciaIcg;
        public static bool _IsMonotributista;
        public static string _caja = "";
        public static decimal _montoMaximo;
        public static int _cantidadTiquets;
        //Sitef
        public static SiTefService.Common.InfoSitef _sitef = new SiTefService.Common.InfoSitef();

        //Venice.
        public static string _veniceUrl = "";
        public static string _veniceKey = "";
        public static int _veniceCodPago = 0;
        public static int _venicePdv = 0;
        public static int _veniceArticulo = 0;

        public frmMain()
        {
            InitializeComponent();
            //Titulo.
            this.Text = "ICG Argentina - Consola de Facturación Electrónica Rest - V." + Application.ProductVersion;
            //recupero la ubicacion del ejecutable.
            _pathApp = Application.StartupPath.ToString();
            //Cargo la ubicaciones.
            _pathCertificado = _pathApp + "\\Certificado\\";
            _pathLog = _pathApp + "\\Log\\";
            _pathTaFC = _pathApp + "\\TAFC\\";
            _pathTaFCE = _pathApp + "\\TAFCE\\";
            _IsMonotributista = false;
            _terminal = Environment.MachineName;
            //setting
            grComprobantes.TopLeftHeaderCell.Value = "Todas";
            //Lectura
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRestFCEConsola.exe"));
                //Obtengo las key's
                String[] _keys = config.AppSettings.Settings.AllKeys;
                _codigoIVA = _keys.Contains("CodigoIVA") ? Convert.ToInt32(config.AppSettings.Settings["CodigoIVA"].Value) : 0;
                _codigoIIBB = _keys.Contains("CodigoIIBB") ? Convert.ToInt32(config.AppSettings.Settings["CodigoIIBB"].Value) : 0;
                _nombreCertificado = _keys.Contains("NombreCertificado") ? config.AppSettings.Settings["NombreCertificado"].Value : "";
                _cuit = _keys.Contains("CUIT") ? config.AppSettings.Settings["CUIT"].Value : "";
                _esTest = _keys.Contains("IsTest") ? Convert.ToBoolean(config.AppSettings.Settings["IsTest"].Value) : true;
                _password = _keys.Contains("PassWord") ? config.AppSettings.Settings["PassWord"].Value : "";
                _PtoVtaManual = _keys.Contains("PtoVtaManual") ? config.AppSettings.Settings["PtoVtaManual"].Value : "";
                _PtoVtaCAE = _keys.Contains("PtoVtaCAE") ? config.AppSettings.Settings["PtoVtaCAE"].Value : "";
                _PtoVtaCAEA = _keys.Contains("PtoVtaCAEA") ? config.AppSettings.Settings["PtoVtaCAEA"].Value : "";
                _server = _keys.Contains("Server") ? config.AppSettings.Settings["Server"].Value : "";
                _user = "ICGAdmin";
                _database = _keys.Contains("Database") ? config.AppSettings.Settings["Database"].Value : "";
                _caja = _keys.Contains("Caja") ? config.AppSettings.Settings["Caja"].Value : "0"; 
                _soloCaea = _keys.Contains("SoloCAEA") ? Convert.ToBoolean(config.AppSettings.Settings["SoloCAEA"].Value) : false;
                _licenciaIcg = _keys.Contains("LicenciaIcg") ? config.AppSettings.Settings["LicenciaIcg"].Value : "";
                _IsMonotributista = _keys.Contains("Monotributista") ? Convert.ToBoolean(config.AppSettings.Settings["Monotributista"].Value) : false;
                _montoMaximo = _keys.Contains("MontoMaximo") ? Convert.ToDecimal(config.AppSettings.Settings["MontoMaximo"].Value): Convert.ToDecimal("15000");
                _cantidadTiquets = _keys.Contains("CantidadTiquets") ? Convert.ToInt32(config.AppSettings.Settings["CantidadTiquets"].Value) : 5;
                //Sitef
                _sitef.sitefIdTienda = _keys.Contains("IdTienda") ? config.AppSettings.Settings["IdTienda"].Value : "";
                _sitef.sitefIdTerminal = _keys.Contains("IdTerminal") ? config.AppSettings.Settings["IdTerminal"].Value : "";
                _sitef.sitefCuitIsv = _keys.Contains("CuitIsv") ? config.AppSettings.Settings["CuitIsv"].Value : "";
                _sitef.pathSendInvoice = _keys.Contains("PathInvoice") ? config.AppSettings.Settings["PathInvoice"].Value :"" ;
                _sitef.sitefCuit = _keys.Contains("CUIT") ? config.AppSettings.Settings["CUIT"].Value : "";
                _sitef.usaClover = _keys.Contains("UsaClover") ? Convert.ToBoolean(config.AppSettings.Settings["UsaClover"].Value) : false;
                //Irsa
                _irsa.contrato = _keys.Contains("IrsaContrato") ? config.AppSettings.Settings["IrsaContrato"].Value : "";
                _irsa.local = _keys.Contains("IrsaLocal") ? config.AppSettings.Settings["IrsaLocal"].Value : "";
                _irsa.pos = _keys.Contains("IrsaPos") ? config.AppSettings.Settings["IrsaPos"].Value : "";
                _irsa.rubro = _keys.Contains("IrsaRubro") ? config.AppSettings.Settings["IrsaRubro"].Value : "";
                _irsa.pathSalida = _keys.Contains("IrsaPath") ? config.AppSettings.Settings["IrsaPath"].Value : "";
                //Venice
                if (_keys.Contains("VeniceCondPago"))
                    _veniceCodPago = String.IsNullOrEmpty(config.AppSettings.Settings["VeniceCondPago"].Value) ? 0 : Convert.ToInt32(config.AppSettings.Settings["VeniceCondPago"].Value);
                else
                    _veniceCodPago = 0;
                if (_keys.Contains("VeniceApiKey"))
                    _veniceKey = String.IsNullOrEmpty(config.AppSettings.Settings["VeniceApiKey"].Value) ? "" : config.AppSettings.Settings["VeniceApiKey"].Value;
                else
                    _veniceKey = "";
                if (_keys.Contains("VeniceApiUrl"))
                    _veniceUrl = String.IsNullOrEmpty(config.AppSettings.Settings["VeniceApiUrl"].Value) ? "" : config.AppSettings.Settings["VeniceApiUrl"].Value;
                else
                    _veniceUrl = "";
                if (_keys.Contains("VenicePDV"))
                    _venicePdv = String.IsNullOrEmpty(config.AppSettings.Settings["VenicePDV"].Value) ? 0 : Convert.ToInt32(config.AppSettings.Settings["VenicePDV"].Value);
                else
                    _venicePdv = 0;
                if (_keys.Contains("VeniceArticulo"))
                    _veniceArticulo = String.IsNullOrEmpty(config.AppSettings.Settings["VeniceArticulo"].Value) ? 0 : Convert.ToInt32(config.AppSettings.Settings["VeniceArticulo"].Value);
                else
                    _veniceArticulo = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                _connection.ConnectionString = strConnection;
                //
                _connection.Open();
                //Cargamos los datos de los comprobantes.
                GetDataset();
                //Cargamos los CAEA otrogados.
                GetCAEAOtorgados();
                //Cargmos los Comprobantes sin informar.
                GetComprobantesSinInformar();
                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCertificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCertificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show(new Form { TopMost = true }, "Se ha creado el Directorio para el certificado de la AFIP (" + _pathCertificado + "). Por favor coloque el certificado en este directorio para poder continuar.");

                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(_nombreCertificado))
                    {
                        if (!File.Exists(_pathCertificado + "\\" + _nombreCertificado))
                        {
                            MessageBox.Show(new Form { TopMost = true }, "El certificado no se encuentra en la directorio " + _pathCertificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                            return;
                        }
                        else
                            _pathCertificado = _pathCertificado + "\\" + _nombreCertificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);
                //Validamos la Key
                ValidateKey();
                //Validamos el CAEA del día.
                if (!String.IsNullOrEmpty(_PtoVtaCAEA))
                    ValidarCAEA();
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void GetComprobantesSinInformar()
        {
            try
            {
                string _sql = @"SELECT TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.FO, TIQUETSVENTACAMPOSLIBRES.CAE, TIQUETSVENTACAMPOSLIBRES.ERROR_CAE,
                    TIQUETSVENTACAMPOSLIBRES.VTO_CAE, TIQUETSVENTACAMPOSLIBRES.ESTADO_FE, TIQUETSVENTACAMPOSLIBRES.CAEA_INFORMADO,
                    TIQUETSCAB.FECHA, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.CODVENDEDOR
                    from TIQUETSCAB inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE AND TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO
                    and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N and TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO
                    Where (TIQUETSVENTACAMPOSLIBRES.ESTADO_FE = 'SININFORMAR') AND TIQUETSCAB.CAJA = @caja ORDER BY TIQUETSCAB.NUMEROFISCAL";

                dtsMain.Tables["SinInformar"].Clear();

                using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
                {
                    _cmd.Parameters.AddWithValue("@caja", _caja);

                    using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(dtsMain, "SinInformar");
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetCAEAOtorgados()
        {
            try
            {
                string _sql = "SELECT ID, CAEA, FechaProceso, FechaDesde, FechaHasta, FechaTope, Periodo, Quincena FROM FCCAEA";

                //Limpio el dataset
                dtsMain.Tables["Caea"].Clear();

                using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
                {

                    using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                    {
                        _sda.Fill(dtsMain, "Caea");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetDataset()
        {
            string _sql;

            _sql = "SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, TIQUETSCAB.TOTALBRUTO, TIQUETSCAB.TOTALNETO, "+
                   "CASE WHEN TIQUETSCAB.CODCLIENTE = 0 THEN 'Consumidor Final' ELSE CLIENTES.NOMBRECLIENTE END AS NOMBRECLIENTE, "+
                   "TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN "+
				   "CASE WHEN CLIENTES.REGIMFACT = '4' THEN '006 Factura' ELSE '001 Factura' END "+
				   "ELSE CASE WHEN CLIENTES.REGIMFACT = '4' THEN '008 Nota Credito' ELSE '003 Nota Credito' END END AS DESCRIPCION, "+
                   "TIQUETSCAB.FECHA, TIQUETSCAB.Z, TIQUETSCAB.CODVENDEDOR, 0 as ANULACION_IRSA, TIQUETSCAB.HORAFIN "+
                   "FROM TIQUETSCAB LEFT JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE "+
                   "WHERE (TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = 0) AND year(TIQUETSCAB.FECHAANULACION) = 1899 "+
                   "AND TIQUETSCAB.N = 'B' AND TIQUETSCAB.SUBTOTAL = 'F' AND TIQUETSCAB.CAJA = @caja ORDER BY TIQUETSCAB.FECHA";

            //Limpio el dataset
            dtsMain.Tables["Comprobantes"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _connection))
            {
                _cmd.Parameters.AddWithValue("@caja", _caja);

                int pp = _cmd.ExecuteNonQuery();

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsMain, "Comprobantes");
                }
            }
        }

        private void dtpSeleccion_ValueChanged(object sender, EventArgs e)
        {
            GetDataset();
        }

        private void btSolicitarCAEA_Click(object sender, EventArgs e)
        {
            bool nahue = true;
            if(nahue)
            //if (ValidarLicencia(_licenciaIcg))
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    //Valido si existe el CAEA para el periodo en curso.
                    //1- recupero el CAEA.
                    int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                    int _quincena;

                    if (DateTime.Now.Day > 15)
                        _quincena = 2;
                    else
                        _quincena = 1;

                    //2- recupero el CAEA de la base de datos
                    AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);

                    if (_caea.Caea == null)
                    {
                        //No lo tengo lo solicito
                        _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);

                        if (_caea.Caea == null)
                        {
                            //No lo tengo lo consulto.
                            _caea = CAEA.ConsultarCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);
                        }

                        if (_caea.Caea != null)
                            CAEA.InsertCAEA(_caea, _connection);
                        else
                            throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
                    }

                    GetCAEAOtorgados();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "Error: " + ex.Message);
                }
                this.Cursor = Cursors.Default;
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "Licencia no valida por favor comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                          MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btReimprimir_Click(object sender, EventArgs e)
        {
            GetDataset();
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            string _dummy = "FALSE";

            this.Cursor = Cursors.WaitCursor;
            if(grComprobantes.Rows.Count > 0)
            {
                if (grComprobantes.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection _collection = grComprobantes.SelectedRows;
                    //Tengo una fila seleccionada.
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows.Cast<DataGridViewRow>().Reverse())
                    {
                        bool nahue = true;
                        //if (ValidarLicencia(_licenciaIcg))
                        if (nahue)
                        {
                            try
                            {
                                int _fo = Convert.ToInt32(rw.Cells["FO"].Value);
                                string _serie = rw.Cells["SERIE"].Value.ToString();
                                int _numero = Convert.ToInt32(rw.Cells["NUMERO"].Value);
                                string _n = rw.Cells["N"].Value.ToString();
                                int _codigoVendedor = Convert.ToInt32(rw.Cells["CODVENDEDOR"].Value);
                                //Busco el Tiquet.
                                Modelos.Tiquet _tiquet = IcgFceDll.RestService.GetTiquet(_serie, _numero, _n, _connection);
                                //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                if (_tiquet.NumeroFiscal == 0)
                                {
                                    string _digitoSerie = _serie.Substring(0, 1);
                                    //Solo imprimimos las que poseen el primer digito de la serie = F
                                    if (_digitoSerie != "G" && _digitoSerie != "S" && _digitoSerie != "I")
                                    {
                                        //Factura Electronica comun.
                                        Modelos.DatosFactura _Fc = RestService.GetDatosFactura(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n,
                                        _codigoVendedor, _connection);
                                        //Valido el codigo de la Forma de Pago.
                                        if (!RestService.TarjetaFidelización.GetIsTarjetaFidelizacion(_fo, _serie, _numero, _n, _codigoVendedor, _connection))
                                        {
                                            //Validamos si el total es cero. 
                                            if (_Fc.totalbruto == 0)
                                            {
                                                //Mandamos transaccionBlack.
                                                RestService.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                                            }
                                            else
                                            {
                                                //Vemos si tenemos las credenciales de Venice.
                                                if (!String.IsNullOrEmpty(_veniceUrl) && !String.IsNullOrEmpty(_veniceKey))
                                                {
                                                    //Recupero la forma de pago
                                                    Modelos.FormaPago _fp = RestService.Venice.GetFormaPago(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codigoVendedor), _connection);
                                                    //Vemos si la formade pago es correcta.
                                                    if (_fp.codFormaPago == _veniceCodPago)
                                                    {
                                                        //Mostramos la consulta de habitaciones.
                                                        IcgFceDll.frmConsultaHabitaciones frm = new IcgFceDll.frmConsultaHabitaciones(_veniceKey, _veniceUrl);
                                                        //frmConsultaHabitaciones frm = new frmConsultaHabitaciones(_veniceKey, _veniceUrl);
                                                        frm.ShowDialog();
                                                        int _reservation = frm._reservation;
                                                        frm.Dispose();
                                                        if (_reservation > 0)
                                                        {
                                                            string _icgKey = _serie + "-" + _numero + "-" + _n;
                                                            if (_Fc.serieFiscal.StartsWith("003") || _Fc.serieFiscal.StartsWith("008"))
                                                            {
                                                                //Es un Abono y no se envía a Venice. Se hace la transacción y se muestra mensaje.
                                                                RestService.TransaccionHotel(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, _reservation.ToString(), strConnection);
                                                                //Mensaje.
                                                                MessageBox.Show(new Form { TopMost = true }, "El comprobante es ABONO. Los ABONOS no se pueden enviar al hotel." + Environment.NewLine +
                                                                    "Por favor registre el mismo para luego informalo al Hotel.", "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Information);
                                                            }
                                                            else
                                                            {
                                                                if (RestService.Venice.PutFactura(_veniceKey, _veniceUrl, _icgKey, _reservation, _Fc.totalneto, _veniceArticulo, _venicePdv))
                                                                {
                                                                    //Mando la transaccion Black.
                                                                    RestService.TransaccionHotel(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, _reservation.ToString(), strConnection);
                                                                }
                                                                else
                                                                {
                                                                    MessageBox.Show(new Form { TopMost = true }, "El comprobante no se pudo enviar al hotel." + Environment.NewLine +
                                                                    "Ingrese a la consola e intente nuevamente desde ahí.", "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Information);
                                                                }
                                                            }
                                                        }
                                                        GetDataset();
                                                        this.Cursor = Cursors.Default;
                                                        //Salgo
                                                        return;
                                                    }
                                                }

                                                if (!RestService.ExistenComprobantesConFechaAnterior(_Fc.fecha, _caja, _connection))
                                                {
                                                    if (_IsMonotributista)
                                                    {
                                                        _Fc.codAfipComprobante = _Fc.totalneto > 0 ? 11 : 13;
                                                        _Fc.serieFiscal = _Fc.codAfipComprobante == 11 ? "011_Factura_C" : "013_NCredito_C";
                                                    }
                                                    //Array de IVA
                                                    List<Modelos.TiquetsTot> _Iva = RestService.GetTotalesIva(_Fc.serie, _Fc.n, _Fc.numero, _codigoIVA, _connection);
                                                    _Fc.totIVA = _Iva.Sum(x => x.TotIva);
                                                    //Array de tributos
                                                    List<Modelos.TiquetsTot> _Tributos = new List<Modelos.TiquetsTot>();
                                                    _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                                    //Array de Importes no gravados.
                                                    List<Modelos.TiquetsTot> _NoGravado = RestService.GetTotalesIvaNoGravado(_Fc.serie, _Fc.n, _Fc.numero, _codigoIVA, _connection);
                                                    _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                                    //Asigno el punto de venta de CAE.
                                                    _Fc.ptoVta = Convert.ToInt32(_PtoVtaCAE);
                                                    //Validamos los datos de la FC.
                                                    RestService.ValidarDatosFactura(_Fc);
                                                    //Armamos la password como segura
                                                    SecureString strPasswordSecureString = new SecureString();
                                                    foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                                    strPasswordSecureString.MakeReadOnly();
                                                    if (_soloCaea)
                                                    {
                                                        _dummy = "FALSE";
                                                    }
                                                    else
                                                    {
                                                        //Vemos si esta disponible el servicio de AFIP.
                                                        _dummy = wsAfip.TestDummy(_pathLog, _esTest);
                                                    }
                                                    if (_dummy.ToUpper() == "OK")
                                                    {
                                                        int intNroComprobante = 0;
                                                        //string _Error;
                                                        List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                                                        //Recupero el nro del ultimo comprobante.
                                                        wsAfip.UltimoComprobante _ultimo = wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog,
                                                            _Fc.ptoVta, _Fc.codAfipComprobante, _cuit, _esTest, strPasswordSecureString, out _lstErrores);
                                                        //Vemos si hubo error.
                                                        if (_lstErrores.Count() == 0)
                                                        {
                                                            //incremento el nro de comprobante.
                                                            intNroComprobante = _ultimo.CbteNro + 1;
                                                            //Armo el string de la cabecera.
                                                            AfipDll.wsAfipCae.FECAECabRequest _feCab = new AfipDll.wsAfipCae.FECAECabRequest();
                                                            _feCab.CantReg = 1;
                                                            _feCab.CbteTipo = _Fc.codAfipComprobante;
                                                            _feCab.PtoVta = _Fc.ptoVta;
                                                            //Cargamos la clase Detalle.
                                                            List<AfipDll.wsAfipCae.FECAEDetRequest> _lstDetalle = new List<AfipDll.wsAfipCae.FECAEDetRequest>();
                                                            AfipDll.wsAfipCae.FECAEDetRequest _feDetalle = new AfipDll.wsAfipCae.FECAEDetRequest();
                                                            _feDetalle.CbteDesde = intNroComprobante;
                                                            _feDetalle.CbteHasta = intNroComprobante;
                                                            _feDetalle.CbteFch = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                            _feDetalle.Concepto = 3;
                                                            _feDetalle.DocTipo = _Fc.clienteTipoDocumento;
                                                            _feDetalle.DocNro = Convert.ToInt64(_Fc.clienteDocumento.Replace("-", ""));
                                                            _feDetalle.FchServDesde = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                            _feDetalle.FchServHasta = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                            _feDetalle.FchVtoPago = _Fc.fecha.Year.ToString() + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                            _feDetalle.MonId = "PES";
                                                            _feDetalle.MonCotiz = 1;
                                                            _feDetalle.ImpTotal = Math.Abs(_Fc.totalneto);
                                                            if (_Fc.cargo == 0)
                                                                _feDetalle.ImpNeto = Math.Abs(_Fc.totalbruto);
                                                            else
                                                            {
                                                                double _cargo = (_Fc.totalbruto * Math.Abs(_Fc.cargo)) / 100;
                                                                if (_Fc.cargo > 0)
                                                                    _feDetalle.ImpNeto = Math.Round(Math.Abs(_Fc.totalbruto + _cargo), 2);
                                                                else
                                                                    _feDetalle.ImpNeto = Math.Round(Math.Abs(_Fc.totalbruto - _cargo), 2);
                                                            }
                                                            _feDetalle.ImpIVA = Math.Abs((double)_Fc.totIVA);
                                                            _feDetalle.ImpTrib = Math.Abs((double)_Fc.totTributos);
                                                            if (!_IsMonotributista)
                                                                _feDetalle.Iva = RestService.ArmarIVAAfip(_Iva).ToArray();
                                                            if (_Fc.totTributos > 0)
                                                                _feDetalle.Tributos = RestService.ArmaTributos(_Tributos).ToArray();
                                                            //Verifico que tengo por lo menos 1 comprobante asociado.
                                                            if (_Fc.codAfipComprobante == 2 || _Fc.codAfipComprobante == 3
                                                                || _Fc.codAfipComprobante == 7 || _Fc.codAfipComprobante == 8
                                                                || _Fc.codAfipComprobante == 12 || _Fc.codAfipComprobante == 13)
                                                            {
                                                                //Recuperamos los Comprobantes Asociados.
                                                                List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RestService.GetCbteAsocs(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, _cuit, _connection);
                                                                //Vemos si tenemos comprobantes asociados.
                                                                if (_ComprobantesAsociados.Count() > 0)
                                                                {
                                                                    _feDetalle.CbtesAsoc = _ComprobantesAsociados.ToArray();
                                                                }
                                                                else
                                                                {
                                                                    //Vamos por el periodo asociado.
                                                                    DateTime _dtDesde = _Fc.fecha.AddDays(-30);
                                                                    string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                                                                    string _hasta = _Fc.fecha.Year.ToString().PadLeft(2, '0') + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                                    AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                                                                    _feDetalle.PeriodoAsoc = _cls;
                                                                }

                                                            }
                                                            _lstDetalle.Add(_feDetalle);
                                                            //Armo el request
                                                            AfipDll.wsAfipCae.FECAERequest _requestCAE = new AfipDll.wsAfipCae.FECAERequest();
                                                            _requestCAE.FeCabReq = _feCab;
                                                            _requestCAE.FeDetReq = _lstDetalle.ToArray();
                                                            try
                                                            {
                                                                AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                                                                //validamos que tenemos el TA.xml
                                                                if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCertificado, _pathTaFC, _pathLog, _wsaa, _cuit))
                                                                    _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCertificado, _pathTaFC, _pathLog, _esTest, _cuit, strPasswordSecureString);
                                                                if (_wsaa.Token != null)
                                                                {
                                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "****Ticket Serie: " + _serie + ", Numero:" + _numero.ToString() + ", N: " + _n);
                                                                    wsAfip.clsCaeResponse _rta = wsAfip.ObtenerDatosCAENew(_requestCAE, _pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest, strPasswordSecureString, _wsaa);
                                                                    //
                                                                    string strCodBarra = "";
                                                                    switch (_rta.Resultado)
                                                                    {
                                                                        case "A":
                                                                        case "P":
                                                                            {
                                                                                //Genero el codigo de barra.
                                                                                DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _feCab.CbteTipo.ToString(), _feCab.PtoVta.ToString(),
                                                                                    _rta.Cae, _rta.FechaVto, out strCodBarra);
                                                                                //Generamos la info del QR.
                                                                                string qrEncode = QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit), _feCab.PtoVta, _feCab.CbteTipo,
                                                                                    Convert.ToInt32(_feDetalle.CbteDesde), Convert.ToDecimal(_feDetalle.ImpTotal), "PES", 1,
                                                                                    _feDetalle.DocTipo, Convert.ToInt64(_feDetalle.DocNro), "E", Convert.ToInt64(_rta.Cae));
                                                                                //Grabo el nro. fiscal y los demas datos.
                                                                                RestService.GrabarNumeroTiquetFecha(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n,
                                                                                    _PtoVtaCAE, _Fc.serieFiscal, intNroComprobante, DateTime.Now,
                                                                                    _rta.Cae, strCodBarra, _rta.ErrorMsj, "Facturado", _rta.FechaVto, qrEncode, _connection);
                                                                                //Grabo la tabla TransaccionAFIP
                                                                                CommonService.InfoTransaccionAFIP _dtoAfip = new CommonService.InfoTransaccionAFIP();
                                                                                _dtoAfip.cae = _rta.Cae;
                                                                                _dtoAfip.codigobara = strCodBarra;
                                                                                _dtoAfip.codigoqr = qrEncode;
                                                                                _dtoAfip.fechavto = _rta.FechaVto;
                                                                                _dtoAfip.n = _n;
                                                                                _dtoAfip.nrofiscal = intNroComprobante;
                                                                                _dtoAfip.numero = Convert.ToInt32(_numero);
                                                                                _dtoAfip.serie = _serie;
                                                                                _dtoAfip.tipocae = "CAE";
                                                                                try
                                                                                {
                                                                                    CommonService.TransaccionAFIP.InsertTransaccionAFIP(_dtoAfip, _connection);
                                                                                }
                                                                                catch (Exception ex)
                                                                                {
                                                                                    LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Error al grabar info en la tabla TransaccionAfip. Error: " + ex.Message);
                                                                                }
                                                                                //SiTef
                                                                                if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                                                {
                                                                                    IcgFceDll.SiTefService.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _n, _sitef.sitefIdTienda, _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _connection);
                                                                                }
                                                                                //Irsa
                                                                                if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                                {
                                                                                    Irsa.Rest.LanzarTrancomp(_irsa, _feCab.CbteTipo.ToString(), intNroComprobante.ToString(), _feCab.PtoVta.ToString(), _connection);

                                                                                }
                                                                                break;
                                                                            }
                                                                        case "R":
                                                                            {
                                                                                //Aca se debe armar el CAEA.
                                                                                MessageBox.Show(new Form { TopMost = true }, "El ticket fue rechazado por la AFIP." + Environment.NewLine +
                                                                                    "Error Codigo: " + _rta.ErrorCode + Environment.NewLine +
                                                                                    "Error Mensaje: " + _rta.ErrorMsj + Environment.NewLine +
                                                                                    "Por favor solucionelo y fiscalicelo por la consola.",
                                                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                                break;
                                                                            }
                                                                    }

                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, Convert.ToInt32(_numero), _n, _connection);

                                                                }
                                                                else
                                                                {
                                                                    MessageBox.Show(new Form { TopMost = true }, "No se pudo obtener una Tiquet de Acceso. Consulte el log.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine +
                                                                    "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                {
                                                                    //Recupero el contador para el CAEA
                                                                    DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                                    //Obtengo el CAEA para el periodo en curso.
                                                                    CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                                    if (_caeaEnCurso.Caea != null)
                                                                    {
                                                                        if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                                        {
                                                                            MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                            "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Information);
                                                                        }
                                                                        else
                                                                        {
                                                                            //Genero la fecha de vto.
                                                                            string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                                                            //Genero el codigo de barras.
                                                                            string _codBarra;
                                                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                            //Generamos la info del QR.
                                                                            string qrEncode = QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit), _feCab.PtoVta, _feCab.CbteTipo,
                                                                                _dtoContador.contador, Convert.ToDecimal(_feDetalle.ImpTotal), "PES", 1,
                                                                                _feDetalle.DocTipo, Convert.ToInt64(_feDetalle.DocNro), "E", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                            //Grabo la transaccion de CAEA. 
                                                                            RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador,
                                                                                _codBarra, _PtoVtaCAEA, _cuit, _pathLog, qrEncode, _connection);
                                                                            //SiTef
                                                                            if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                                            {
                                                                                IcgFceDll.SiTefService.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _n, _sitef.sitefIdTienda, _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _connection);
                                                                            }
                                                                            //Irsa
                                                                            if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                            {
                                                                                Irsa.Rest.LanzarTrancomp(_irsa, _feCab.CbteTipo.ToString(), intNroComprobante.ToString(), _feCab.PtoVta.ToString(), _connection);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                                            "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Information);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string _msj = "";
                                                            foreach (wsAfip.Errors er in _lstErrores)
                                                            {
                                                                AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Error: " + er.Code + ": " + er.Msg);
                                                                _msj = _msj + Environment.NewLine + "Error: " + er.Code + ": " + er.Msg;
                                                            }

                                                            if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + _msj + Environment.NewLine +
                                                                "Desea ingresar el comprobante con CAEA?.", "ICG Argentina", MessageBoxButtons.YesNo,
                                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                            {
                                                                //Recupero el contador para el CAEA
                                                                DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                                //Obtengo el CAEA para el periodo en curso.
                                                                CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                                if (_caeaEnCurso.Caea != null)
                                                                {
                                                                    if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                                    {
                                                                        MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                        "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Information);
                                                                    }
                                                                    else
                                                                    {
                                                                        //Genero la fecha de vto.
                                                                        string _fechaVto = DateTime.Now.Day.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/" + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                                                        //Genero el codigo de barras.
                                                                        string _codBarra;
                                                                        DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                        //Generamos la info del QR.
                                                                        string qrEncode = QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit), Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante,
                                                                            _dtoContador.contador, Convert.ToDecimal(_Fc.totalneto), "PES", 1,
                                                                            _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                        //Grabo la transaccion de CAEA. 
                                                                        RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra,
                                                                            _PtoVtaCAEA, _cuit, _pathLog, qrEncode, _connection);
                                                                        //SiTef
                                                                        if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                                        {
                                                                            IcgFceDll.SiTefService.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _n, _sitef.sitefIdTienda, _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _connection);
                                                                        }
                                                                        //Irsa
                                                                        if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                        {
                                                                            Irsa.Rest.LanzarTrancomp(_irsa, _Fc.codAfipComprobante.ToString(), _dtoContador.contador.ToString(), _PtoVtaCAEA, _connection);
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                    MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                                        "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Information);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //Recupero el contador para el CAEA
                                                        DataAccess.SeriesResol _dtoContador = DataAccess.FuncionesVarias.GetContador(_PtoVtaCAEA, _Fc.serieFiscal.Substring(0, 3), _connection);
                                                        //Validamos que tenemos contador.
                                                        if (!String.IsNullOrEmpty(_dtoContador.serieresol))
                                                        {
                                                            //Obtengo el CAEA para el periodo en curso.
                                                            CAEA _caeaEnCurso = DataAccess.IcgCAEA.GetCAEAEnCurso(_connection);
                                                            if (_caeaEnCurso.Caea != null)
                                                            {
                                                                string _msj = "";
                                                                if (_soloCaea)
                                                                {
                                                                    //Vemos si el monto supera el maximo permitido.
                                                                    if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                                    {
                                                                        MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                        "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Information);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_dtoContador.contador % 5 == 0)
                                                                        {
                                                                            _msj = "Usted esta Fiscalizando SOLO con CAEA." + Environment.NewLine + "Para cambiar esta modalidad, hagalo desde la configuración de la consola.";
                                                                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                                        }
                                                                        //Genero la fecha de vto.
                                                                        string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                                                                        //Genero el codigo de barras.
                                                                        string _codBarra;
                                                                        DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                        //Genero el los datos del QR.
                                                                        string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit),
                                                                            Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                                            "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                        //Grabo la transaccion de CAEA. 
                                                                        RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                                            _cuit, _pathLog, qrEncode, _connection);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _msj = "No esta disponible el servicio de la AFIP para Fiscalizar." + Environment.NewLine +
                                                                            "Desea ingresar el comprobante con CAEA?.";
                                                                    if (MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                    {
                                                                        //Vemos si el monto supera el maximo permitido.
                                                                        if (_Fc.totalneto > Convert.ToDouble(_montoMaximo) && Convert.ToInt64(_Fc.clienteDocumento) == 0)
                                                                        {
                                                                            MessageBox.Show(new Form { TopMost = true }, "El monto del comprobante excede el máximo permitido para Clientes sin identificar." + Environment.NewLine +
                                                                            "Por favor ingrese un cliente con DNI/CUIT y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Information);
                                                                        }
                                                                        else
                                                                        {
                                                                            //Genero la fecha de vto.
                                                                            string _fechaVto = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                                                                            //Genero el codigo de barras.
                                                                            string _codBarra;
                                                                            DataAccess.FuncionesVarias.GenerarCodigoBarra(_cuit, _Fc.codAfipComprobante.ToString(), _PtoVtaCAEA, _caeaEnCurso.Caea, _fechaVto, out _codBarra);
                                                                            //Genero el los datos del QR.
                                                                            string qrEncode = IcgFceDll.QR.CrearJson(1, _Fc.fecha, Convert.ToInt64(_cuit),
                                                                                Convert.ToInt32(_PtoVtaCAEA), _Fc.codAfipComprobante, _dtoContador.contador, Convert.ToDecimal(_Fc.totalbruto),
                                                                                "PES", 1, _Fc.clienteTipoDocumento, Convert.ToInt64(_Fc.clienteDocumento), "A", Convert.ToInt64(_caeaEnCurso.Caea));
                                                                            //Grabo la transaccion de CAEA. 
                                                                            RestService.TransactionCAEA(_Fc, _caeaEnCurso.Caea, _terminal, _dtoContador.contador, _codBarra, _PtoVtaCAEA,
                                                                                _cuit, _pathLog, qrEncode, _connection);
                                                                        }
                                                                    }
                                                                }
                                                                //SiTef
                                                                if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                                {
                                                                    IcgFceDll.SiTefService.SiTefServiceRest.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _n, _sitef.sitefIdTienda, _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _connection);
                                                                }
                                                                //Irsa
                                                                if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                                {
                                                                    Irsa.Rest.LanzarTrancomp(_irsa, _Fc.codAfipComprobante.ToString(), _dtoContador.contador.ToString(), _PtoVtaCAEA, _connection);
                                                                }
                                                            }
                                                            else
                                                                MessageBox.Show(new Form { TopMost = true }, "No posee CAEA para el periodo en curso." + Environment.NewLine +
                                                                    "Por favor gestione un nuevo CAEA desde la consola y luego fiscalícelo.", "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Information);
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show(new Form { TopMost = true }, "No posee un contador definido para CAEA." + Environment.NewLine +
                                                                "El comprobante no se fiscalizará, y quedará pendiente." + Environment.NewLine +
                                                                    "Por favor comuníquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                                    MessageBoxIcon.Information);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "Existen comprobantes con fecha anterior sin fiscalizar." + Environment.NewLine +
                                                            "Debe fiscalizar estos comprobantes antes, para un correcto procesamiento en AFIP." + Environment.NewLine +
                                                            "Ingrese a la consola y fiscalice todos comprobantes con fecha anterior.", "ICG Argentina", MessageBoxButtons.OK,
                                                            MessageBoxIcon.Information);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Invocamos la transaccion Black
                                            RestService.TransaccionBlack(_fo, _serie, _numero, _n, strConnection);
                                        }
                                    }
                                    else
                                    {
                                        //Ponemos el numerofiscal en -1 y la seiefiscal en 00000
                                        //Invocamos la transaccion Black
                                        RestService.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente ERROR: " + ex.Message + Environment.NewLine +
                                                "Comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                                                MessageBoxIcon.Error);
                            }
                        }
                        else
                            MessageBox.Show(new Form { TopMost = true }, "Licencia no valida por favor comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                           MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    }
                }
            }
            GetDataset();
            this.Cursor = Cursors.Default;
        }

        private void btCaeaInformar_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            bool nahue = true;
            //if (ValidarLicencia(_licenciaIcg))
            if(nahue)
            {
                if (grSinInformar.Rows.Count > 0)
                {
                    try
                    {
                        //Tengo una fila seleccionada.
                        foreach (DataGridViewRow rw in grSinInformar.Rows)
                        {
                            //Valido si existe el CAEA para el periodo en curso.
                            //1- recupero el CAEA.
                            DateTime dttFecha = Convert.ToDateTime(rw.Cells["FECHASININF"].Value);
                            int _fo = Convert.ToInt32(rw.Cells["FOSININF"].Value);
                            string _serie = rw.Cells["SERIESININF"].Value.ToString();
                            int _numero = Convert.ToInt32(rw.Cells["NUMEROSININF"].Value);
                            string _n = rw.Cells["NSININF"].Value.ToString();
                            int _vendedor = Convert.ToInt32(rw.Cells["CODVENDEDORSININF"].Value);

                            int _periodo = Convert.ToInt32(dttFecha.Year.ToString() + dttFecha.Month.ToString().PadLeft(2, '0'));
                            int _quincena = 0;

                            if (dttFecha.Day > 15)
                                _quincena = 2;
                            else
                                _quincena = 1;

                            //1- recupero las FC con CAEA
                            AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);
                            //Vemos si tenemos CAEA
                            if (_caea.Caea != null)
                            {
                                //valido el estado de la fila
                                if (rw.Cells["ESTADO_FESININF"].Value.ToString().ToUpper() == "SININFORMAR")
                                {
                                    //Ver como se genera el request en la FCE.
                                    //Busco el Tiquet.
                                    Modelos.Tiquet _tiquet = IcgFceDll.RestService.GetTiquet(_serie, _numero, _n, _connection);
                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_tiquet.NumeroFiscal > 0)
                                    {
                                        //Factura Electronica comun.
                                        Modelos.DatosFactura _Fc = RestService.GetDatosFactura(_fo, _serie, _numero, _n,
                                           _vendedor, _connection);
                                        //Cambio para contemplar Monotributo.
                                        if (_IsMonotributista)
                                        {
                                            _Fc.codAfipComprobante = _Fc.totalneto > 0 ? 11 : 13;
                                            _Fc.serieFiscal = _Fc.codAfipComprobante == 11 ? "011_Factura_C" : "013_NCredito_C";
                                        }
                                        // Array de IVA
                                        List<Modelos.TiquetsTot> _Iva = RestService.GetTotalesIva(_Fc.serie, _Fc.n, _Fc.numero, _codigoIVA, _connection);
                                        _Fc.totIVA = _Iva.Sum(x => x.TotIva);
                                        //Array de tributos
                                        List<Modelos.TiquetsTot> _Tributos = new List<Modelos.TiquetsTot>();
                                        _Fc.totTributos = _Tributos.Sum(x => x.TotIva);
                                        //Array de Importes no gravados.
                                        List<Modelos.TiquetsTot> _NoGravado = RestService.GetTotalesIvaNoGravado(_Fc.serie, _Fc.n, _Fc.numero, _codigoIVA, _connection);
                                        _Fc.totNoGravado = _NoGravado.Sum(x => x.BaseImponible);
                                        //Asigno el punto de venta de CAE.
                                        _Fc.ptoVta = Convert.ToInt32(_PtoVtaCAEA);
                                        //Validamos los datos de la FC.
                                        RestService.ValidarDatosFactura(_Fc);
                                        //Armamos la password como segura
                                        SecureString strPasswordSecureString = new SecureString();
                                        foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                                        strPasswordSecureString.MakeReadOnly();
                                        //Vemos si esta disponible el servicio de AFIP.
                                        string _dummy = wsAfip.TestDummy(_pathLog, _esTest);
                                        if (_dummy.ToUpper() == "OK")
                                        {
                                            List<wsAfip.Errors> _lstErrores = new List<wsAfip.Errors>();
                                            //Armo el string de la cabecera.
                                            AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                                            _cabReq.CantReg = 1;
                                            _cabReq.CbteTipo = Convert.ToInt16(_Fc.codAfipComprobante);
                                            _cabReq.PtoVta = Convert.ToInt16(_Fc.ptoVta);
                                            string strCabReg = "1|" + _Fc.codAfipComprobante.ToString() + "|" + _Fc.ptoVta.ToString();
                                            //Cargamos la clase Detalle.
                                            List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = RestService.CargarDetalleCAEAAfip(_Fc, _tiquet.NumeroFiscal, _caea);
                                            //Armo el string de IVA.
                                            List<AfipDll.wsAfipCae.AlicIva> _lstIva = RestService.ArmarIVAAfip(_Iva);
                                            //Armos el string de Tributos.
                                            List<AfipDll.wsAfipCae.Tributo> _TributosRequest = RestService.ArmaTributos(_Tributos);
                                            //Agregamos el IVA
                                            if(!_IsMonotributista)
                                                _detReq[0].Iva = _lstIva.ToArray();
                                            //Agregamos los tributos.
                                            if (_TributosRequest.Count > 0)
                                                _detReq[0].Tributos = _TributosRequest.ToArray();
                                            //Agregamos los comprobantes asociados, si es una Nota de Credito/Debito.
                                            //Verifico que si es una NC/ND.
                                            if (_Fc.codAfipComprobante == 203 || _Fc.codAfipComprobante == 208 || _Fc.codAfipComprobante == 213
                                                || _Fc.codAfipComprobante == 202 || _Fc.codAfipComprobante == 207 || _Fc.codAfipComprobante == 212
                                                || _Fc.codAfipComprobante == 3 || _Fc.codAfipComprobante == 8 || _Fc.codAfipComprobante == 13
                                                || _Fc.codAfipComprobante == 2 || _Fc.codAfipComprobante == 7 || _Fc.codAfipComprobante == 12)
                                            {
                                                //Armamos la lista de comprobantes Asociados.
                                                List<AfipDll.wsAfipCae.CbteAsoc> _ComprobantesAsociados = RestService.GetCbteAsocs(_fo, _serie, _numero, _n, _cuit, _connection);
                                                //Verifico que tengo por lo menos 1 comprobante asociado.
                                                if (_ComprobantesAsociados.Count() == 0)
                                                {
                                                    DateTime _dtDesde = _Fc.fecha.AddDays(-30);
                                                    string _desde = _dtDesde.Year.ToString().PadLeft(2, '0') + _dtDesde.Month.ToString().PadLeft(2, '0') + _dtDesde.Day.ToString().PadLeft(2, '0');
                                                    string _hasta = _Fc.fecha.Year.ToString().PadLeft(2, '0') + _Fc.fecha.Month.ToString().PadLeft(2, '0') + _Fc.fecha.Day.ToString().PadLeft(2, '0');
                                                    AfipDll.wsAfipCae.Periodo _cls = new AfipDll.wsAfipCae.Periodo() { FchDesde = _desde, FchHasta = _hasta };
                                                    _detReq[0].PeriodoAsoc = _cls;
                                                }
                                                else
                                                {
                                                    _detReq[0].CbtesAsoc = _ComprobantesAsociados.ToArray();
                                                }
                                            }
                                            //Armamos el Request.
                                            AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                                            _req.FeCabReq = _cabReq;
                                            _req.FeDetReq = _detReq.ToArray();
                                            try
                                            {
                                                List<AfipDll.wsAfip.CaeaResultadoInformar> _Respuesta = wsAfip.InformarMovimientosCAEANew(_pathCertificado, _pathTaFC, _pathLog, _cuit, _esTest, strPasswordSecureString, _req, true, out _lstErrores);

                                                if (_Respuesta.Count() > 0)
                                                {
                                                    //Recupero el nombre de la terminal.
                                                    string _terminal = Environment.MachineName;
                                                    foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _Respuesta)
                                                    {
                                                        switch (cri.Resultado)
                                                        {
                                                            case "A":
                                                                {
                                                                    //Grabo el dato en facturascamposlibres.
                                                                    bool _rta = RestService.TiquetsVentaCamposLibresInformoCAEA(_serie, _numero, _n, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serie + ", NUMERO: " + _numero + ", N: " + _n +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection);
                                                                    break;
                                                                }
                                                            case "P":
                                                                {
                                                                    //Informo.
                                                                    bool _rta = RestService.TiquetsVentaCamposLibresInformoCAEA(_serie, _numero, _n, _connection);
                                                                    if (!_rta)
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serie + ", NUMERO: " + _numero.ToString() + ", N: " + _n +
                                                                            ") se informo correctamente, pero no se pudo grabar en la BBDD");
                                                                    }
                                                                    else
                                                                    {
                                                                        LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "El comprobante (SERIE: " + _serie + ", NUMERO: " + _numero.ToString() + ", N: " + _n +
                                                                                ") se informo con la observacion: " + cri.Observaciones);
                                                                    }
                                                                    //Inserto en Rem_transacciones.
                                                                    DataAccess.RemTransacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection);
                                                                    break;
                                                                }
                                                            case "R":
                                                                {
                                                                    //Informo.
                                                                    bool _rta = RestService.TiquetsVentaCamposLibresInformoCAEAError(_fo, _serie, _numero, _n, cri.Observaciones, _connection);
                                                                    MessageBox.Show(new Form { TopMost = true }, "El comprobante (SERIE: " + _serie + ", NUMERO: " + _numero.ToString() + ", N: " + _n + Environment.NewLine +
                                                                        "Fue rechazado por AFIP. Error" + cri.Observaciones,
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                                    string _error = "El comprobante (SERIE: " + _serie + ", NUMERO: " + _numero.ToString() + ", N: " + _n +
                                                                        ") fue rechazado por la AFIP el ser infomado. Error: " + cri.Observaciones;
                                                                    throw new Exception(_error);
                                                                    //break;
                                                                }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    string _msj = "";
                                                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                                                    {
                                                        if (String.IsNullOrEmpty(_msj))
                                                            _msj = "La AFIP reporto los siguientes Errores." + Environment.NewLine + "Codigo de error: " + _er.Code + Environment.NewLine + "Descripcion: " + _er.Msg;
                                                        else
                                                            _msj = _msj + Environment.NewLine + "Codigo de error: " + _er.Code + Environment.NewLine + "Descripcion: " + _er.Msg;
                                                    }
                                                    MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    throw new Exception(_msj);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Se produjo el siguiente error al infomar el CAEA del comprobante SERIE: " + _serie + ", NUMERO: " + _numero.ToString() + ", N: " + _n +
                                                                        ") fue rechazado por la AFIP el ser infomado. Error: " + ex.Message);
                                                throw new Exception();
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "No existe un CAEA para el periodo en curso.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch
                    { 
                    }
                }
                //Refresco la grilla
                GetComprobantesSinInformar();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "Licencia no valida por favor comuniquese con ICG Argentina.", "ICG Argentina", MessageBoxButtons.OK,
                          MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            this.Cursor = Cursors.Default;
        }

        private void btRefrescarSinInfomar_Click(object sender, EventArgs e)
        {
            GetComprobantesSinInformar();
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                frmConfig _frm = new frmConfig();
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
        }

        /// <summary>
        /// VAlida la licencia de ICG
        /// </summary>
        /// <returns></returns>
        private static bool ValidarLicencia(string _licenciaIcg)
        {
            string _value = IcgVarios.LicenciaIcg.Value();
            //string _value = IcgVarios.LicenciaIcg.Value(Convert.ToInt32(_PtoVtaCAE).ToString(), _cuit);
            string lic = IcgVarios.NuevaLicencia.Value(_value);
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Licencia Local: " + _licenciaIcg);
            //LogFile.ErrorLog(LogFile.CreatePath(_pathLog), "Licencia Generada: " + lic);
            if (_licenciaIcg == lic)
                return true;
            else
                return false;
        }

        private void ValidateKey()
        {
            if (DateTime.Now.Day < 5)
            {
                if (!String.IsNullOrEmpty(_cuit))
                {
                    if (!String.IsNullOrEmpty(_licenciaIcg))
                    {
                        try
                        {
                            string sql = "select NOMBRE, NOMBRECOMERCIAL, CIF from empresas";
                            IcgFceDll.Licencia lic = new IcgFceDll.Licencia();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = frmMain._connection;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = sql;

                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            if (String.IsNullOrEmpty(reader["NOMBRE"].ToString()) ||
                                                String.IsNullOrEmpty(reader["NOMBRECOMERCIAL"].ToString()) ||
                                                String.IsNullOrEmpty(reader["CIF"].ToString()))
                                            {
                                                throw new Exception("Los siguientes datos de la empresa deben estar completos." + Environment.NewLine +
                                                    "NOMBRE, NOMBRE COMERCIAL y CIF.");
                                            }
                                            lic.ClientCuit = reader["CIF"].ToString();
                                            lic.ClientName = reader["NOMBRE"].ToString();
                                            lic.ClientRazonSocial = reader["NOMBRECOMERCIAL"].ToString();
                                        }
                                    }
                                }
                            }

                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = frmMain._connection;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "select VALOR from PARAMETROS where TERMINAL = 'VersionBD' and CLAVE = 'VersionBD'";

                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            lic.Version = reader["VALOR"].ToString();
                                        }
                                    }
                                }
                            }
                            //No Aplico la nueva Key.
                            lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                            lic.password = "Pinsua.2730";
                            lic.Plataforma = "REST";
                            lic.Release = Application.ProductVersion;
                            lic.user = "pinsua@yahoo.com";
                            lic.Tipo = "FCE Comun";
                            lic.TerminalName = Environment.MachineName;
                            lic.PointOfSale =  Convert.ToInt32(_PtoVtaCAE);
                            var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                            var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
#else
                            var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");
#endif

                            client.Timeout = -1;
                            var request = new RestRequest(Method.POST);
                            request.AddHeader("Content-Type", "application/json");
                            request.AddParameter("application/json", json, ParameterType.RequestBody);
                            IRestResponse response = client.Execute(request);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                IcgFceDll.ResponcePostLicencia res = JsonConvert.DeserializeObject<IcgFceDll.ResponcePostLicencia>(response.Content);
                                if (String.IsNullOrEmpty(res.Key))
                                {
                                    //Recupero los datos.
                                    Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRestFCEConsola.exe"));
                                    //Obtengo las key's
                                    String[] _keys = config.AppSettings.Settings.AllKeys;
                                    //Licencia.
                                    if (_keys.Contains("LicenciaIcg"))
                                        config.AppSettings.Settings["LicenciaIcg"].Value = "";
                                    //Guardamos los datos.
                                    config.Save();
                                    //Muestro un Mensaje.
                                    MessageBox.Show(new Form { TopMost = true }, "Su licencia no esta habilitada." + Environment.NewLine +
                                        "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    this.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                                ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void ValidarCAEA()
        {
            try
            {
                //Valido si existe el CAEA para el periodo en curso.
                //1- recupero el CAEA.
                int _periodo = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
                int _quincena;

                if (DateTime.Now.Day > 15)
                    _quincena = 2;
                else
                    _quincena = 1;

                //2- recupero el CAEA de la base de datos
                AfipDll.CAEA _caea = CAEA.GetCaeaByPeriodoQuincena(_periodo, _quincena, _connection);

                if (_caea.Caea == null)
                {
                    //No lo tengo lo solicito
                    _caea = AfipDll.CAEA.ObtenerCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);

                    if (_caea.Caea == null)
                    {
                        //No lo tengo lo consulto.
                        _caea = CAEA.ConsultarCAEA(_periodo, _quincena, _pathCertificado, _pathTaFC, _pathLog, _cuit, _password, _nombreCertificado, _esTest);
                    }

                    if (_caea.Caea != null)
                        CAEA.InsertCAEA(_caea, _connection);
                    else
                        throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo Obtener un CAEA. Comuniquese con ICG ARGENTINA.");
            }
        }

        #region MenuContextual
        private void cambiarFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grComprobantes.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows)
                    {
                        string _serie = rw.Cells["SERIE"].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells["NUMERO"].Value);
                        string _n = rw.Cells["N"].Value.ToString();
                        frmChangeDate _frm = new frmChangeDate(_serie, _numero, _n, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetDataset();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }

        private void borrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grComprobantes.SelectedRows.Count == 1)
                {
                    //Recupero las filas seleccionadas.
                    foreach (DataGridViewRow rw in grComprobantes.SelectedRows)
                    {
                        if (MessageBox.Show(new Form { TopMost = true },
                    "Desea BORRAR el comprobante seleccionado?.",
                    "ICG Argentina", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            try
                            {
                                int _fo = Convert.ToInt32(rw.Cells["FO"].Value);
                                string _serie = rw.Cells["SERIE"].Value.ToString();
                                int _numero = Convert.ToInt32(rw.Cells["NUMERO"].Value);
                                string _n = rw.Cells["N"].Value.ToString();
                                int _codigoVendedor = Convert.ToInt32(rw.Cells["CODVENDEDOR"].Value);
                                RestService.TransaccionBlack2(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, 1, strConnection);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Se produjo el siguiente Error: " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    GetDataset();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void consultarUltimoComprobanteCAEAToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                SecureString strPasswordSecureString = new SecureString();
                foreach (char c in _password) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                int _ptoVta = Convert.ToInt32(_PtoVtaCAEA);

                //FCA
                try
                {
                    List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                    AfipDll.wsAfip.UltimoComprobante _ultimoFcA = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                        1, _cuit, _esTest, strPasswordSecureString, out _lstErrores);

                    if (_lstErrores.Count() == 0)
                    {
                        MessageBox.Show("Nro. Comprobante: " + _ultimoFcA.CbteNro + Environment.NewLine +
                            "Comprobante Tipo: " + _ultimoFcA.CbteTipo + Environment.NewLine +
                            "Punto de Venta: " + _ultimoFcA.PtoVta);
                    }
                    else
                    {
                        MessageBox.Show("FC A Nro. Comprobante: " + _ultimoFcA.CbteNro);

                        foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                        {
                            MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                "Descripcion: " + _er.Msg);
                        }
                    }

                    //NCA
                    _lstErrores.Clear();
                    AfipDll.wsAfip.UltimoComprobante _ultimoNcA = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                        3, _cuit, _esTest, strPasswordSecureString, out _lstErrores);

                    if (_lstErrores.Count() == 0)
                    {
                        MessageBox.Show("Nro. Comprobante: " + _ultimoNcA.CbteNro + Environment.NewLine +
                            "Comprobante Tipo: " + _ultimoNcA.CbteTipo + Environment.NewLine +
                            "Punto de Venta: " + _ultimoNcA.PtoVta);
                    }
                    else
                    {
                        MessageBox.Show("NC A Nro. Comprobante: " + _ultimoNcA.CbteNro);

                        foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                        {
                            MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                "Descripcion: " + _er.Msg);
                        }
                    }
                    //FCB
                    _lstErrores.Clear();
                    AfipDll.wsAfip.UltimoComprobante _ultimoFcB = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                        6, _cuit, _esTest, strPasswordSecureString, out _lstErrores);

                    if (_lstErrores.Count() == 0)
                    {
                        MessageBox.Show("Nro. Comprobante: " + _ultimoFcB.CbteNro + Environment.NewLine +
                            "Comprobante Tipo: " + _ultimoFcB.CbteTipo + Environment.NewLine +
                            "Punto de Venta: " + _ultimoFcB.PtoVta);
                    }
                    else
                    {
                        MessageBox.Show("FC B Nro. Comprobante: " + _ultimoFcB.CbteNro);

                        foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                        {
                            MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                "Descripcion: " + _er.Msg);
                        }
                    }
                    // NCB
                    _lstErrores.Clear();
                    AfipDll.wsAfip.UltimoComprobante _ultimoNcB = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCertificado, _pathTaFC, _pathLog, _ptoVta,
                    8, _cuit, _esTest, strPasswordSecureString, out _lstErrores);

                    if (_lstErrores.Count() == 0)
                    {
                        MessageBox.Show("Nro. Comprobante: " + _ultimoNcB.CbteNro + Environment.NewLine +
                            "Comprobante Tipo: " + _ultimoNcB.CbteTipo + Environment.NewLine +
                            "Punto de Venta: " + _ultimoNcB.PtoVta);
                    }
                    else
                    {
                        MessageBox.Show("NC B Nro. Comprobante: " + _ultimoNcB.CbteNro);

                        foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                        {
                            MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                                "Descripcion: " + _er.Msg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        #endregion

        #region Menu CAEA
        private void modificarNumeroFiscalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grSinInformar.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grSinInformar.SelectedRows)
                    {
                        string _serie = rw.Cells[1].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells[2].Value);
                        string _n = rw.Cells[3].Value.ToString();
                        int _numeroFiscal = Convert.ToInt32(rw.Cells[7].Value);
                        frmChangeNroFiscalCAEA _frm = new frmChangeNroFiscalCAEA(_serie, _numero, _n, _numeroFiscal, _pathLog, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetComprobantesSinInformar();
                }
                else
                {
                    if (grComprobantes.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }

        private void cambiarFechaComprobanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grSinInformar.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grSinInformar.SelectedRows)
                    {
                        string _serie = rw.Cells[1].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells[2].Value);
                        string _n = rw.Cells[3].Value.ToString();                        
                        frmChangeDate _frm = new frmChangeDate(_serie, _numero, _n, _connection);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetComprobantesSinInformar();
                }
                else
                {
                    if (grSinInformar.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();
        }

        #endregion


    }
}
