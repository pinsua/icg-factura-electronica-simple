﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRestFCEConsola
{
    public partial class frmConfig : Form
    {
        private string _database;
        private string _serverSql;

        public frmConfig()
        {
            InitializeComponent();
            this.Text = this.Text + Application.ProductVersion;
            _database = frmMain._database;
            _serverSql = frmMain._server;
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            txtCertificado.Text = frmMain._nombreCertificado;
            txtCuit.Text = frmMain._cuit;
            txtDatabase.Text = _database;
            txtPassword.Text = frmMain._password;
            txtProVtaCAE.Text = frmMain._PtoVtaCAE;
            txtPtoVtaCAEA.Text = frmMain._PtoVtaCAEA;
            txtPtoVtaManual.Text = frmMain._PtoVtaManual;
            txtServer.Text = _serverSql;
            txtLicencia.Text = frmMain._licenciaIcg;
            chkIsTest.Checked = frmMain._esTest;
            chkSoloCAEA.Checked = frmMain._soloCaea;
            chkMonotributista.Checked = frmMain._IsMonotributista;
            txtNroCaja.Text = frmMain._caja;
            txtMontoMaximo.Text = frmMain._montoMaximo.ToString();
            txtCantidadTiquets.Text = frmMain._cantidadTiquets.ToString();
            //Irsa
            txtIrsaContrato.Text = frmMain._irsa.contrato;
            txtIrsaLocal.Text = frmMain._irsa.local;
            txtIrsaPath.Text = frmMain._irsa.pathSalida;
            txtIrsaPos.Text = frmMain._irsa.pos;
            txtIrsaRubro.Text = frmMain._irsa.rubro;
            //Sitef
            txtSiTefIdTienda.Text = frmMain._sitef.sitefIdTienda;
            txtSiTefIdTerminal.Text = frmMain._sitef.sitefIdTerminal;
            txtSitefCuit.Text = frmMain._sitef.sitefCuit;
            txtSiTefCuitIsv.Text = frmMain._sitef.sitefCuitIsv;
            txtSiTefPath.Text = frmMain._sitef.pathSendInvoice;
            chkClover.Checked = frmMain._sitef.usaClover;
        }

        private void btKey_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Para obtener una licencia, debe cargar correctamente los siguientes datos, caso contrario la licencia no será valida." +
                Environment.NewLine + "Punto de Venta, Nro de CUIT y el Nombre y el Nombre Comercial de la Empresa.",
                "ICG Argentina", 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (!String.IsNullOrEmpty(txtProVtaCAE.Text))
                {
                    //Validamos licencia via WebService.
                    try
                    {
                        string sql = "select NOMBRE, NOMBRECOMERCIAL, CIF from empresas";
                        IcgFceDll.Licencia lic = new IcgFceDll.Licencia();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = frmMain._connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = sql;

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        if (String.IsNullOrEmpty(reader["NOMBRE"].ToString()) ||
                                            String.IsNullOrEmpty(reader["NOMBRECOMERCIAL"].ToString()) ||
                                            String.IsNullOrEmpty(reader["CIF"].ToString()))
                                        {
                                            throw new Exception("Los siguientes datos de la empresa deben estar completos." + Environment.NewLine +
                                                "NOMBRE, NOMBRE COMERCIAL y CIF.");
                                        }
                                        lic.ClientCuit = reader["CIF"].ToString();
                                        lic.ClientName = reader["NOMBRE"].ToString();
                                        lic.ClientRazonSocial = reader["NOMBRECOMERCIAL"].ToString();
                                    }
                                }
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = frmMain._connection;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "select VALOR from PARAMETROS where TERMINAL = 'VersionBD' and CLAVE = 'VersionBD'";

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        lic.Version = reader["VALOR"].ToString();
                                    }
                                }
                            }
                        }
                        //No Aplico la nueva Key.
                        //lic.ClientKey = IcgVarios.LicenciaIcg.Value(Convert.ToInt32(txtProVtaCAE.Text).ToString(), lic.ClientCuit);
                        lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                        lic.password = "Pinsua.2730";
                        lic.Plataforma = "REST";
                        lic.Release = Application.ProductVersion;
                        lic.user = "pinsua@yahoo.com";
                        lic.Tipo = "FCE Comun";
                        lic.TerminalName = Environment.MachineName;
                        lic.PointOfSale = Convert.ToInt32(txtProVtaCAE.Text);
                        //

                        var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                        var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");                        
#else                        
                        var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");
#endif
                        client.Timeout = -1;
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddParameter("application/json", json, ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            IcgFceDll.ResponcePostLicencia res = JsonConvert.DeserializeObject<IcgFceDll.ResponcePostLicencia>(response.Content);
                            txtLicencia.Text = res.Key;
                        }
                        else
                        {
                            MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                            response.Content, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                            ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Por favor ingrese el Punto de Venta CAE para obtener una licencia.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Guardamos los datos.
                Configuration config = ConfigurationManager.OpenExeConfiguration(Path.Combine(Application.StartupPath, "IcgRestFCEConsola.exe"));
                //Obtengo las key's
                String[] _keys = config.AppSettings.Settings.AllKeys;
                //Nombre del certificado.
                if (_keys.Contains("NombreCertificado"))
                    config.AppSettings.Settings["NombreCertificado"].Value = txtCertificado.Text;
                else
                    config.AppSettings.Settings.Add("NombreCertificado", txtCertificado.Text);
                //CUIT Emisor
                if (_keys.Contains("CUIT"))
                    config.AppSettings.Settings["CUIT"].Value = txtCuit.Text;
                else
                    config.AppSettings.Settings.Add("CUIT", txtCuit.Text);
                //Test o Produccion
                if (_keys.Contains("IsTest"))
                    config.AppSettings.Settings["IsTest"].Value = chkIsTest.Checked.ToString();
                else
                    config.AppSettings.Settings.Add("IsTest", chkIsTest.Checked.ToString());
                //Password Certificado
                if (_keys.Contains("PassWord"))
                    config.AppSettings.Settings["PassWord"].Value = txtPassword.Text;
                else
                    config.AppSettings.Settings.Add("PassWord", txtPassword.Text);
                //Pto Vta Manual
                if (_keys.Contains("PtoVtaManual"))
                    config.AppSettings.Settings["PtoVtaManual"].Value = txtPtoVtaManual.Text;
                else
                    config.AppSettings.Settings.Add("PtoVtaManual", txtPtoVtaManual.Text);
                //Pto Vta CAE
                if (_keys.Contains("PtoVtaCAE"))
                    config.AppSettings.Settings["PtoVtaCAE"].Value = txtProVtaCAE.Text;
                else
                    config.AppSettings.Settings.Add("PtoVtaCAE", txtProVtaCAE.Text);
                //Pto vta CAEA
                if (_keys.Contains("PtoVtaCAEA"))
                    config.AppSettings.Settings["PtoVtaCAEA"].Value = txtPtoVtaCAEA.Text;
                else
                    config.AppSettings.Settings.Add("PtoVtaCAEA", txtPtoVtaCAEA.Text);
                //Server
                if (_keys.Contains("Server"))
                    config.AppSettings.Settings["Server"].Value = txtServer.Text;
                else
                    config.AppSettings.Settings.Add("Server", txtServer.Text);
                //Database
                if (_keys.Contains("Database"))
                    config.AppSettings.Settings["Database"].Value = txtDatabase.Text;
                else
                    config.AppSettings.Settings.Add("Database", txtDatabase.Text);
                //SoloCAEA
                if (_keys.Contains("SoloCAEA"))
                    config.AppSettings.Settings["SoloCAEA"].Value = chkSoloCAEA.Checked.ToString();
                else
                    config.AppSettings.Settings.Add("SoloCAEA", chkSoloCAEA.Checked.ToString());
                //Licencia.
                if (_keys.Contains("LicenciaIcg"))
                    config.AppSettings.Settings["LicenciaIcg"].Value = txtLicencia.Text;
                else
                    config.AppSettings.Settings.Add("LicenciaIcg", txtLicencia.Text);
                //Monotributista
                if (_keys.Contains("Monotributista"))
                    config.AppSettings.Settings["Monotributista"].Value = chkMonotributista.Checked.ToString();
                else
                    config.AppSettings.Settings.Add("Monotributista", chkMonotributista.Checked.ToString());
                //Caja
                if (_keys.Contains("Caja"))
                    config.AppSettings.Settings["Caja"].Value = txtNroCaja.Text;
                else
                    config.AppSettings.Settings.Add("Caja", txtNroCaja.Text);
                //Monto Maximo
                if (_keys.Contains("MontoMaximo"))
                    config.AppSettings.Settings["MontoMaximo"].Value = txtMontoMaximo.Text;
                else
                    config.AppSettings.Settings.Add("MontoMaximo", txtMontoMaximo.Text);
                //Cantidad de Tiquets
                if (_keys.Contains("CantidadTiquets"))
                    config.AppSettings.Settings["CantidadTiquets"].Value = txtCantidadTiquets.Text;
                else
                    config.AppSettings.Settings.Add("CantidadTiquets", txtCantidadTiquets.Text);
                #region Trancomp
                //Irsa contrato
                if (_keys.Contains("IrsaContrato"))
                    config.AppSettings.Settings["IrsaContrato"].Value = txtIrsaContrato.Text;
                else
                    config.AppSettings.Settings.Add("IrsaContrato", txtIrsaContrato.Text);
                //Irsa Local
                if (_keys.Contains("IrsaLocal"))
                    config.AppSettings.Settings["IrsaLocal"].Value = txtIrsaLocal.Text;
                else
                    config.AppSettings.Settings.Add("IrsaLocal", txtIrsaLocal.Text);
                //IrsaPOS
                if (_keys.Contains("IrsaPos"))
                    config.AppSettings.Settings["IrsaPos"].Value = txtIrsaPos.Text;
                else
                    config.AppSettings.Settings.Add("IrsaPos", txtIrsaPos.Text);
                //Irsa Rubro
                if (_keys.Contains("IrsaRubro"))
                    config.AppSettings.Settings["IrsaRubro"].Value = txtIrsaRubro.Text;
                else
                    config.AppSettings.Settings.Add("IrsaRubro", txtIrsaRubro.Text);
                //Irsa Path
                if (_keys.Contains("IrsaPath"))
                    config.AppSettings.Settings["IrsaPath"].Value = txtIrsaPath.Text;
                else
                    config.AppSettings.Settings.Add("IrsaPath", txtIrsaPath.Text);
                #endregion
                #region Sitef
                if (_keys.Contains("IdTienda"))
                    config.AppSettings.Settings["IdTienda"].Value = txtSiTefIdTienda.Text;
                else
                    config.AppSettings.Settings.Add("IdTienda", txtSiTefIdTienda.Text);

                if (_keys.Contains("IdTerminal"))
                    config.AppSettings.Settings["IdTerminal"].Value = txtSiTefIdTerminal.Text;
                else
                    config.AppSettings.Settings.Add("IdTerminal", txtSiTefIdTerminal.Text);

                if (_keys.Contains("CuitIsv"))
                    config.AppSettings.Settings["CuitIsv"].Value = txtSiTefCuitIsv.Text;
                else
                    config.AppSettings.Settings.Add("CuitIsv", txtSiTefCuitIsv.Text);

                if (_keys.Contains("CUIT"))
                    config.AppSettings.Settings["CUIT"].Value = txtSitefCuit.Text;
                else
                    config.AppSettings.Settings.Add("CUIT", txtSitefCuit.Text);

                if (_keys.Contains("PathInvoice"))
                    config.AppSettings.Settings["PathInvoice"].Value = txtSiTefPath.Text;
                else
                    config.AppSettings.Settings.Add("PathInvoice", txtSiTefPath.Text);

                if (_keys.Contains("UsaClover"))
                    config.AppSettings.Settings["UsaClover"].Value = chkClover.Checked.ToString();
                else
                    config.AppSettings.Settings.Add("UsaClover", chkClover.Checked.ToString());
                #endregion
                //Guardamos los datos.
                config.Save();

                MessageBox.Show("Para que la configuración tenga efecto debe reiniciar la consola",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo un error al guardar la configuración" + Environment.NewLine +
                    "Error: " + ex.Message, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCajaCreaTabla_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Recuperar los datos de conexión a la Base de datos?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Recuperamos los datos de la conexion
                IcgVarios.IcgRegestry.Rest.GetDataBaseToConnect(out _serverSql, out _database);
                txtServer.Text = _serverSql;
                txtDatabase.Text = _database;
            }

            if (String.IsNullOrEmpty(txtServer.Text))
            {
                MessageBox.Show("No esta configurado el servidor de SQL." + Environment.NewLine +
                    "Ingreso y vuelva a probar.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (String.IsNullOrEmpty(txtDatabase.Text))
            {
                MessageBox.Show("No esta configurada la Base de datos SQL." + Environment.NewLine +
                    "Ingresela y vuelva a probar.",
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //Armamos el stringConnection.
            string strConnection = "Data Source=" + txtServer.Text.Trim() + ";Initial Catalog=" + txtDatabase.Text.Trim() + ";User Id=ICGAdmin;Password=masterkey;";
            try
            {
                //Conectamos.
                using (SqlConnection _connection = new SqlConnection(strConnection))
                {
                    _connection.Open();
                    if (MessageBox.Show("Desea Recuperar el CUIT de la Base de datos?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //Recupero el CUIT
                        long _nroCuit = IcgFceDll.RestService.Config.GetCUIT(_connection);
                        if (_nroCuit == 0)
                        {
                            MessageBox.Show("No se ha encontrado un CUIT definido en tabla de Empresas." + Environment.NewLine +
                                    "Por favor ingréselo y reintente nuevamente.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        txtCuit.Text = _nroCuit.ToString();
                    }
                    if (MessageBox.Show("Desea Recuperar la Caja de la Base de datos?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //Recupero la caja.
                        int _nroCaja = IcgFceDll.RestService.Config.GetCaja(_connection);
                        if (_nroCaja == 0)
                        {
                            MessageBox.Show("No se ha encontrado una caja definida para está terminal." + Environment.NewLine +
                                    "Ingresela manualmente.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        txtNroCaja.Text = _nroCaja.ToString();
                    }
                    //Tabla TransaccionAFIP
                    if (MessageBox.Show("Desea crear la tabla de Transacciones con AFIP?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string _msj = IcgFceDll.CommonService.TransaccionAFIP.CrearTablaTransaccionAFIP(_connection);
                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //Tabla FCCAEA
                    if (MessageBox.Show("Desea crear la tabla FCCAE?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string _msj = IcgFceDll.CommonService.FcCAEA.CreateTableFcCAEA(_connection);
                        MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //Vemos si tenemos el punto de venta para CAEA.
                    if (!String.IsNullOrEmpty(txtPtoVtaCAEA.Text))
                    { 
                        if (MessageBox.Show("Desea validar los contadores de CAEA?.", "ICG Argentina",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (!IcgFceDll.RestService.Config.TengoPendientesDeInformarCAEA(txtNroCaja.Text.Trim(), _connection))
                            {
                                //Recuperamos los contadores existentes.
                                List<string> _contadores = IcgFceDll.RestService.Config.GetSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), _connection);
                                if (!_contadores.Contains("001"))
                                {
                                    string _error = "";
                                    int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 1, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                    IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "001", _ultimo, _connection);
                                }
                                else
                                {
                                    if (MessageBox.Show("Ya posee un contador CAEA para el tipo de comprobante Factura A." + Environment.NewLine +
                                        "Desea recuperar el último número de AFIP?.", "ICG Argentina",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string _error = "";
                                        int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 1, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                        IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "001", _ultimo, _connection);
                                    }
                                }

                                if (!_contadores.Contains("003"))
                                {
                                    string _error = "";
                                    int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 3, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                    IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "003", _ultimo, _connection);
                                }
                                else
                                {
                                    if (MessageBox.Show("Ya posee un contador CAEA para el tipo de comprobante Nota de Crédito A." + Environment.NewLine +
                                        "Desea recuperar el último número de AFIP?.", "ICG Argentina",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string _error = "";
                                        int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 3, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                        IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "003", _ultimo, _connection);
                                    }
                                }

                                if (!_contadores.Contains("006"))
                                {
                                    string _error = "";
                                    int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                        Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 6, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                    IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "006", _ultimo, _connection);
                                }
                                else
                                {
                                    if (MessageBox.Show("Ya posee un contador CAEA para el tipo de comprobante Factura B." + Environment.NewLine +
                                        "Desea recuperar el último número de AFIP?.", "ICG Argentina",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string _error = "";
                                        int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 6, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                        IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "006", _ultimo, _connection);
                                    }
                                }

                                if (!_contadores.Contains("008"))
                                {
                                    string _error = "";
                                    int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog,
                                        Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 8, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                    IcgFceDll.RestService.Config.InsertSeriesResolucionContadores(txtPtoVtaCAEA.Text.Trim(), "008", _ultimo, _connection);
                                }
                                else
                                {
                                    if (MessageBox.Show("Ya posee un contador CAEA para el tipo de comprobante Nota de Crédito B." + Environment.NewLine +
                                        "Desea recuperar el último número de AFIP?.", "ICG Argentina",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string _error = "";
                                        int _ultimo = AfipDll.wsAfip.RecuperoUltimoComprobante(frmMain._pathCertificado, frmMain._pathTaFC, frmMain._pathLog, Convert.ToInt32(txtPtoVtaCAEA.Text.Trim()), 8, txtCuit.Text.Trim(), chkIsTest.Checked, out _error);
                                        IcgFceDll.RestService.Config.UpdateContadorSeriesResolucion(txtPtoVtaCAEA.Text.Trim(), "008", _ultimo, _connection);
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Posee comprobantes con CAEA sin informar." + Environment.NewLine +
                                    "Informelos a la AFIP y luego reintente.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    //
                    if (MessageBox.Show("Desea validar los campos libres?.", "ICG Argentina",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //Valido los campos libres
                        string _msj = IcgFceDll.RestService.ValidarCamposLibres.ValidarColumnasCamposLibres(_connection);
                        if(!String.IsNullOrEmpty(_msj))
                        {
                            MessageBox.Show(_msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente error." + Environment.NewLine +
                    "Error: " + ex.Message ,
                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtMontoMaximo_KeyPress(object sender, KeyPressEventArgs e)
        {
            CultureInfo cc = System.Threading.Thread.CurrentThread.CurrentCulture;
            if (!(char.IsNumber(e.KeyChar)) && e.KeyChar.ToString() != cc.NumberFormat.NumberDecimalSeparator && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void btnRegenerarTrancomp_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtIrsaPath.Text.Trim()))
            {
                frmRegeneraTrancomp frm = new frmRegeneraTrancomp();
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }
    }
}
