﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRestFCEConsola
{
    public partial class frmRegeneraTrancomp : Form
    {
        public frmRegeneraTrancomp()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor; 
            List<IcgFceDll.Regenerar> _lst = IcgFceDll.Irsa.Rest.GetRegenerar(dtpDesde.Value.Date, dtpHasta.Value.Date, frmMain._connection);
            foreach (IcgFceDll.Regenerar reg in _lst)
            {
                string _tipoCbte = reg.SerieFiscal2.Substring(0, 3);
                IcgFceDll.Irsa.Rest.LanzarTrancomp(frmMain._irsa, _tipoCbte, reg.NumeroFiscal.ToString(), reg.PuntoVenta, frmMain._connection);
            }
            this.Cursor = Cursors.Default;
        }

    }
}
