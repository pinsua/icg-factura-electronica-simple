﻿namespace IcgRestFCEConsola
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDataBase = new System.Windows.Forms.GroupBox();
            this.btnCajaCreaTabla = new System.Windows.Forms.Button();
            this.lblCaja = new System.Windows.Forms.Label();
            this.txtNroCaja = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblDatebase = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.gbFacturacion = new System.Windows.Forms.GroupBox();
            this.txtCantidadTiquets = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMontoMaximo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIsTest = new System.Windows.Forms.CheckBox();
            this.chkMonotributista = new System.Windows.Forms.CheckBox();
            this.chkSoloCAEA = new System.Windows.Forms.CheckBox();
            this.txtPtoVtaCAEA = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPtoVtaManual = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProVtaCAE = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCertificado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbLicencia = new System.Windows.Forms.GroupBox();
            this.txtLicencia = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btKey = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tcShoping = new System.Windows.Forms.TabControl();
            this.tpTranComp = new System.Windows.Forms.TabPage();
            this.btnRegenerarTrancomp = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtIrsaPath = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtIrsaPos = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIrsaRubro = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIrsaContrato = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIrsaLocal = new System.Windows.Forms.TextBox();
            this.tpSitef = new System.Windows.Forms.TabPage();
            this.chkClover = new System.Windows.Forms.CheckBox();
            this.txtSiTefPath = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSiTefCuitIsv = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSitefCuit = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSiTefIdTerminal = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSiTefIdTienda = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gbDataBase.SuspendLayout();
            this.gbFacturacion.SuspendLayout();
            this.gbLicencia.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tcShoping.SuspendLayout();
            this.tpTranComp.SuspendLayout();
            this.tpSitef.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDataBase
            // 
            this.gbDataBase.Controls.Add(this.btnCajaCreaTabla);
            this.gbDataBase.Controls.Add(this.lblCaja);
            this.gbDataBase.Controls.Add(this.txtNroCaja);
            this.gbDataBase.Controls.Add(this.lblServer);
            this.gbDataBase.Controls.Add(this.txtServer);
            this.gbDataBase.Controls.Add(this.lblDatebase);
            this.gbDataBase.Controls.Add(this.txtDatabase);
            this.gbDataBase.Location = new System.Drawing.Point(18, 18);
            this.gbDataBase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDataBase.Name = "gbDataBase";
            this.gbDataBase.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbDataBase.Size = new System.Drawing.Size(1076, 124);
            this.gbDataBase.TabIndex = 19;
            this.gbDataBase.TabStop = false;
            this.gbDataBase.Text = "Datos de Conexión";
            // 
            // btnCajaCreaTabla
            // 
            this.btnCajaCreaTabla.Location = new System.Drawing.Point(632, 74);
            this.btnCajaCreaTabla.Name = "btnCajaCreaTabla";
            this.btnCajaCreaTabla.Size = new System.Drawing.Size(326, 35);
            this.btnCajaCreaTabla.TabIndex = 12;
            this.btnCajaCreaTabla.Text = "Recuperar Caja y crear Tabla";
            this.btnCajaCreaTabla.UseVisualStyleBackColor = true;
            this.btnCajaCreaTabla.Click += new System.EventHandler(this.btnCajaCreaTabla_Click);
            // 
            // lblCaja
            // 
            this.lblCaja.AutoSize = true;
            this.lblCaja.Location = new System.Drawing.Point(9, 74);
            this.lblCaja.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCaja.Name = "lblCaja";
            this.lblCaja.Size = new System.Drawing.Size(41, 20);
            this.lblCaja.TabIndex = 10;
            this.lblCaja.Text = "Caja";
            // 
            // txtNroCaja
            // 
            this.txtNroCaja.Location = new System.Drawing.Point(134, 70);
            this.txtNroCaja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNroCaja.Name = "txtNroCaja";
            this.txtNroCaja.Size = new System.Drawing.Size(326, 26);
            this.txtNroCaja.TabIndex = 11;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(9, 38);
            this.lblServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(55, 20);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "Server";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(134, 34);
            this.txtServer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(326, 26);
            this.txtServer.TabIndex = 1;
            // 
            // lblDatebase
            // 
            this.lblDatebase.AutoSize = true;
            this.lblDatebase.Location = new System.Drawing.Point(507, 38);
            this.lblDatebase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDatebase.Name = "lblDatebase";
            this.lblDatebase.Size = new System.Drawing.Size(115, 20);
            this.lblDatebase.TabIndex = 2;
            this.lblDatebase.Text = "Base de Datos";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(632, 34);
            this.txtDatabase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(326, 26);
            this.txtDatabase.TabIndex = 9;
            // 
            // gbFacturacion
            // 
            this.gbFacturacion.Controls.Add(this.txtCantidadTiquets);
            this.gbFacturacion.Controls.Add(this.label8);
            this.gbFacturacion.Controls.Add(this.txtMontoMaximo);
            this.gbFacturacion.Controls.Add(this.label4);
            this.gbFacturacion.Controls.Add(this.chkIsTest);
            this.gbFacturacion.Controls.Add(this.chkMonotributista);
            this.gbFacturacion.Controls.Add(this.chkSoloCAEA);
            this.gbFacturacion.Controls.Add(this.txtPtoVtaCAEA);
            this.gbFacturacion.Controls.Add(this.label7);
            this.gbFacturacion.Controls.Add(this.txtPtoVtaManual);
            this.gbFacturacion.Controls.Add(this.label6);
            this.gbFacturacion.Controls.Add(this.txtProVtaCAE);
            this.gbFacturacion.Controls.Add(this.label5);
            this.gbFacturacion.Controls.Add(this.txtPassword);
            this.gbFacturacion.Controls.Add(this.label3);
            this.gbFacturacion.Controls.Add(this.txtCertificado);
            this.gbFacturacion.Controls.Add(this.label2);
            this.gbFacturacion.Controls.Add(this.txtCuit);
            this.gbFacturacion.Controls.Add(this.label1);
            this.gbFacturacion.Location = new System.Drawing.Point(18, 152);
            this.gbFacturacion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbFacturacion.Name = "gbFacturacion";
            this.gbFacturacion.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbFacturacion.Size = new System.Drawing.Size(1076, 216);
            this.gbFacturacion.TabIndex = 20;
            this.gbFacturacion.TabStop = false;
            this.gbFacturacion.Text = "Datos Facturación";
            // 
            // txtCantidadTiquets
            // 
            this.txtCantidadTiquets.Location = new System.Drawing.Point(773, 173);
            this.txtCantidadTiquets.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCantidadTiquets.Name = "txtCantidadTiquets";
            this.txtCantidadTiquets.Size = new System.Drawing.Size(184, 26);
            this.txtCantidadTiquets.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(550, 176);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(209, 20);
            this.label8.TabIndex = 25;
            this.label8.Text = "Informar cada tantos tiquets";
            // 
            // txtMontoMaximo
            // 
            this.txtMontoMaximo.Location = new System.Drawing.Point(773, 138);
            this.txtMontoMaximo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMontoMaximo.Name = "txtMontoMaximo";
            this.txtMontoMaximo.Size = new System.Drawing.Size(184, 26);
            this.txtMontoMaximo.TabIndex = 24;
            this.txtMontoMaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontoMaximo_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(470, 143);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Monto máximo para ventas sin identificar";
            // 
            // chkIsTest
            // 
            this.chkIsTest.AutoSize = true;
            this.chkIsTest.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkIsTest.Location = new System.Drawing.Point(369, 141);
            this.chkIsTest.Name = "chkIsTest";
            this.chkIsTest.Size = new System.Drawing.Size(89, 24);
            this.chkIsTest.TabIndex = 22;
            this.chkIsTest.Text = "Es Test";
            this.chkIsTest.UseVisualStyleBackColor = true;
            // 
            // chkMonotributista
            // 
            this.chkMonotributista.AutoSize = true;
            this.chkMonotributista.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkMonotributista.Location = new System.Drawing.Point(195, 141);
            this.chkMonotributista.Name = "chkMonotributista";
            this.chkMonotributista.Size = new System.Drawing.Size(136, 24);
            this.chkMonotributista.TabIndex = 21;
            this.chkMonotributista.Text = "Monotributista";
            this.chkMonotributista.UseVisualStyleBackColor = true;
            // 
            // chkSoloCAEA
            // 
            this.chkSoloCAEA.AutoSize = true;
            this.chkSoloCAEA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSoloCAEA.Location = new System.Drawing.Point(39, 141);
            this.chkSoloCAEA.Name = "chkSoloCAEA";
            this.chkSoloCAEA.Size = new System.Drawing.Size(115, 24);
            this.chkSoloCAEA.TabIndex = 19;
            this.chkSoloCAEA.Text = "Solo CAEA";
            this.chkSoloCAEA.UseVisualStyleBackColor = true;
            // 
            // txtPtoVtaCAEA
            // 
            this.txtPtoVtaCAEA.Location = new System.Drawing.Point(632, 104);
            this.txtPtoVtaCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVtaCAEA.Name = "txtPtoVtaCAEA";
            this.txtPtoVtaCAEA.Size = new System.Drawing.Size(326, 26);
            this.txtPtoVtaCAEA.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(507, 109);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pto. Vta. CAEA";
            // 
            // txtPtoVtaManual
            // 
            this.txtPtoVtaManual.Location = new System.Drawing.Point(632, 67);
            this.txtPtoVtaManual.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVtaManual.Name = "txtPtoVtaManual";
            this.txtPtoVtaManual.Size = new System.Drawing.Size(326, 26);
            this.txtPtoVtaManual.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(494, 72);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Pto. Vta. Manual";
            // 
            // txtProVtaCAE
            // 
            this.txtProVtaCAE.Location = new System.Drawing.Point(632, 31);
            this.txtProVtaCAE.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtProVtaCAE.Name = "txtProVtaCAE";
            this.txtProVtaCAE.Size = new System.Drawing.Size(326, 26);
            this.txtProVtaCAE.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(507, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pto. Vta. CAE";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(134, 104);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(326, 26);
            this.txtPassword.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 109);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "PassWord";
            // 
            // txtCertificado
            // 
            this.txtCertificado.Location = new System.Drawing.Point(134, 67);
            this.txtCertificado.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCertificado.Name = "txtCertificado";
            this.txtCertificado.Size = new System.Drawing.Size(326, 26);
            this.txtCertificado.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Certificado";
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(134, 31);
            this.txtCuit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.ReadOnly = true;
            this.txtCuit.Size = new System.Drawing.Size(326, 26);
            this.txtCuit.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "CUIT";
            // 
            // gbLicencia
            // 
            this.gbLicencia.Controls.Add(this.txtLicencia);
            this.gbLicencia.Controls.Add(this.label9);
            this.gbLicencia.Controls.Add(this.btKey);
            this.gbLicencia.Location = new System.Drawing.Point(18, 561);
            this.gbLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbLicencia.Name = "gbLicencia";
            this.gbLicencia.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbLicencia.Size = new System.Drawing.Size(1076, 82);
            this.gbLicencia.TabIndex = 21;
            this.gbLicencia.TabStop = false;
            this.gbLicencia.Text = "Licencia ICG";
            // 
            // txtLicencia
            // 
            this.txtLicencia.Location = new System.Drawing.Point(134, 32);
            this.txtLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLicencia.Name = "txtLicencia";
            this.txtLicencia.ReadOnly = true;
            this.txtLicencia.Size = new System.Drawing.Size(518, 26);
            this.txtLicencia.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Licencia ICG";
            // 
            // btKey
            // 
            this.btKey.Location = new System.Drawing.Point(726, 29);
            this.btKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btKey.Name = "btKey";
            this.btKey.Size = new System.Drawing.Size(234, 35);
            this.btKey.TabIndex = 0;
            this.btKey.Text = "Generar Key";
            this.btKey.UseVisualStyleBackColor = true;
            this.btKey.Click += new System.EventHandler(this.btKey_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(130, 655);
            this.btSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(338, 35);
            this.btSave.TabIndex = 22;
            this.btSave.Text = "Guardar Configuración y Salir";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(664, 655);
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(338, 35);
            this.btClose.TabIndex = 23;
            this.btClose.Text = "Salir sin Guardar";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tcShoping);
            this.groupBox1.Location = new System.Drawing.Point(18, 378);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1076, 178);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos IRSA (Dejar en blanco si no se usan)";
            // 
            // tcShoping
            // 
            this.tcShoping.Controls.Add(this.tpTranComp);
            this.tcShoping.Controls.Add(this.tpSitef);
            this.tcShoping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcShoping.Location = new System.Drawing.Point(4, 24);
            this.tcShoping.Name = "tcShoping";
            this.tcShoping.SelectedIndex = 0;
            this.tcShoping.Size = new System.Drawing.Size(1068, 149);
            this.tcShoping.TabIndex = 0;
            // 
            // tpTranComp
            // 
            this.tpTranComp.Controls.Add(this.btnRegenerarTrancomp);
            this.tpTranComp.Controls.Add(this.label15);
            this.tpTranComp.Controls.Add(this.txtIrsaPath);
            this.tpTranComp.Controls.Add(this.label13);
            this.tpTranComp.Controls.Add(this.txtIrsaPos);
            this.tpTranComp.Controls.Add(this.label14);
            this.tpTranComp.Controls.Add(this.txtIrsaRubro);
            this.tpTranComp.Controls.Add(this.label11);
            this.tpTranComp.Controls.Add(this.txtIrsaContrato);
            this.tpTranComp.Controls.Add(this.label12);
            this.tpTranComp.Controls.Add(this.txtIrsaLocal);
            this.tpTranComp.Location = new System.Drawing.Point(4, 29);
            this.tpTranComp.Name = "tpTranComp";
            this.tpTranComp.Padding = new System.Windows.Forms.Padding(3);
            this.tpTranComp.Size = new System.Drawing.Size(1060, 116);
            this.tpTranComp.TabIndex = 0;
            this.tpTranComp.Text = "Trancomp";
            this.tpTranComp.UseVisualStyleBackColor = true;
            // 
            // btnRegenerarTrancomp
            // 
            this.btnRegenerarTrancomp.Location = new System.Drawing.Point(771, 75);
            this.btnRegenerarTrancomp.Name = "btnRegenerarTrancomp";
            this.btnRegenerarTrancomp.Size = new System.Drawing.Size(234, 35);
            this.btnRegenerarTrancomp.TabIndex = 27;
            this.btnRegenerarTrancomp.Text = "Regenerar Info";
            this.btnRegenerarTrancomp.UseVisualStyleBackColor = true;
            this.btnRegenerarTrancomp.Click += new System.EventHandler(this.btnRegenerarTrancomp_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(56, 82);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(135, 20);
            this.label15.TabIndex = 25;
            this.label15.Text = "Ubicación Archivo";
            // 
            // txtIrsaPath
            // 
            this.txtIrsaPath.Location = new System.Drawing.Point(200, 78);
            this.txtIrsaPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaPath.Name = "txtIrsaPath";
            this.txtIrsaPath.Size = new System.Drawing.Size(483, 26);
            this.txtIrsaPath.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(56, 46);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 20);
            this.label13.TabIndex = 21;
            this.label13.Text = "POS";
            // 
            // txtIrsaPos
            // 
            this.txtIrsaPos.Location = new System.Drawing.Point(181, 42);
            this.txtIrsaPos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaPos.Name = "txtIrsaPos";
            this.txtIrsaPos.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaPos.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(554, 46);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 20);
            this.label14.TabIndex = 23;
            this.label14.Text = "Rubro";
            // 
            // txtIrsaRubro
            // 
            this.txtIrsaRubro.Location = new System.Drawing.Point(679, 42);
            this.txtIrsaRubro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaRubro.Name = "txtIrsaRubro";
            this.txtIrsaRubro.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaRubro.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(56, 12);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 20);
            this.label11.TabIndex = 17;
            this.label11.Text = "Contrato";
            // 
            // txtIrsaContrato
            // 
            this.txtIrsaContrato.Location = new System.Drawing.Point(181, 8);
            this.txtIrsaContrato.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaContrato.Name = "txtIrsaContrato";
            this.txtIrsaContrato.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaContrato.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(554, 12);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 20);
            this.label12.TabIndex = 19;
            this.label12.Text = "Local";
            // 
            // txtIrsaLocal
            // 
            this.txtIrsaLocal.Location = new System.Drawing.Point(679, 8);
            this.txtIrsaLocal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaLocal.Name = "txtIrsaLocal";
            this.txtIrsaLocal.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaLocal.TabIndex = 20;
            // 
            // tpSitef
            // 
            this.tpSitef.Controls.Add(this.chkClover);
            this.tpSitef.Controls.Add(this.txtSiTefPath);
            this.tpSitef.Controls.Add(this.label22);
            this.tpSitef.Controls.Add(this.txtSiTefCuitIsv);
            this.tpSitef.Controls.Add(this.label21);
            this.tpSitef.Controls.Add(this.txtSitefCuit);
            this.tpSitef.Controls.Add(this.label20);
            this.tpSitef.Controls.Add(this.txtSiTefIdTerminal);
            this.tpSitef.Controls.Add(this.label19);
            this.tpSitef.Controls.Add(this.txtSiTefIdTienda);
            this.tpSitef.Controls.Add(this.label10);
            this.tpSitef.Location = new System.Drawing.Point(4, 29);
            this.tpSitef.Name = "tpSitef";
            this.tpSitef.Padding = new System.Windows.Forms.Padding(3);
            this.tpSitef.Size = new System.Drawing.Size(1060, 116);
            this.tpSitef.TabIndex = 1;
            this.tpSitef.Text = "SiTef";
            this.tpSitef.UseVisualStyleBackColor = true;
            // 
            // chkClover
            // 
            this.chkClover.AutoSize = true;
            this.chkClover.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkClover.Location = new System.Drawing.Point(864, 64);
            this.chkClover.Name = "chkClover";
            this.chkClover.Size = new System.Drawing.Size(112, 24);
            this.chkClover.TabIndex = 26;
            this.chkClover.Text = "Usa Clover";
            this.chkClover.UseVisualStyleBackColor = true;
            // 
            // txtSiTefPath
            // 
            this.txtSiTefPath.Location = new System.Drawing.Point(160, 64);
            this.txtSiTefPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSiTefPath.Name = "txtSiTefPath";
            this.txtSiTefPath.Size = new System.Drawing.Size(407, 26);
            this.txtSiTefPath.TabIndex = 25;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 70);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(135, 20);
            this.label22.TabIndex = 24;
            this.label22.Text = "Ubicación Archivo";
            // 
            // txtSiTefCuitIsv
            // 
            this.txtSiTefCuitIsv.Location = new System.Drawing.Point(682, 64);
            this.txtSiTefCuitIsv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSiTefCuitIsv.Name = "txtSiTefCuitIsv";
            this.txtSiTefCuitIsv.ReadOnly = true;
            this.txtSiTefCuitIsv.Size = new System.Drawing.Size(148, 26);
            this.txtSiTefCuitIsv.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(595, 67);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 20);
            this.label21.TabIndex = 22;
            this.label21.Text = "CUIT ISV";
            // 
            // txtSitefCuit
            // 
            this.txtSitefCuit.Location = new System.Drawing.Point(723, 26);
            this.txtSitefCuit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSitefCuit.Name = "txtSitefCuit";
            this.txtSitefCuit.Size = new System.Drawing.Size(148, 26);
            this.txtSitefCuit.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(614, 29);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 20);
            this.label20.TabIndex = 20;
            this.label20.Text = "CUIT Tienda";
            // 
            // txtSiTefIdTerminal
            // 
            this.txtSiTefIdTerminal.Location = new System.Drawing.Point(438, 26);
            this.txtSiTefIdTerminal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSiTefIdTerminal.Name = "txtSiTefIdTerminal";
            this.txtSiTefIdTerminal.Size = new System.Drawing.Size(148, 26);
            this.txtSiTefIdTerminal.TabIndex = 19;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(336, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 20);
            this.label19.TabIndex = 18;
            this.label19.Text = "Id. Terminal";
            // 
            // txtSiTefIdTienda
            // 
            this.txtSiTefIdTienda.Location = new System.Drawing.Point(160, 26);
            this.txtSiTefIdTienda.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSiTefIdTienda.Name = "txtSiTefIdTienda";
            this.txtSiTefIdTienda.Size = new System.Drawing.Size(148, 26);
            this.txtSiTefIdTienda.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(73, 29);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Id. Tienda";
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 718);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.gbLicencia);
            this.Controls.Add(this.gbFacturacion);
            this.Controls.Add(this.gbDataBase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - Facturación Electrónica";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.gbDataBase.ResumeLayout(false);
            this.gbDataBase.PerformLayout();
            this.gbFacturacion.ResumeLayout(false);
            this.gbFacturacion.PerformLayout();
            this.gbLicencia.ResumeLayout(false);
            this.gbLicencia.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tcShoping.ResumeLayout(false);
            this.tpTranComp.ResumeLayout(false);
            this.tpTranComp.PerformLayout();
            this.tpSitef.ResumeLayout(false);
            this.tpSitef.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDataBase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblDatebase;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.GroupBox gbFacturacion;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCertificado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProVtaCAE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPtoVtaManual;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPtoVtaCAEA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbLicencia;
        private System.Windows.Forms.TextBox txtLicencia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btKey;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCajaCreaTabla;
        private System.Windows.Forms.Label lblCaja;
        private System.Windows.Forms.TextBox txtNroCaja;
        private System.Windows.Forms.CheckBox chkSoloCAEA;
        private System.Windows.Forms.CheckBox chkIsTest;
        private System.Windows.Forms.CheckBox chkMonotributista;
        private System.Windows.Forms.TextBox txtMontoMaximo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCantidadTiquets;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tcShoping;
        private System.Windows.Forms.TabPage tpTranComp;
        private System.Windows.Forms.Button btnRegenerarTrancomp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtIrsaPath;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtIrsaPos;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIrsaRubro;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtIrsaContrato;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIrsaLocal;
        private System.Windows.Forms.TabPage tpSitef;
        private System.Windows.Forms.CheckBox chkClover;
        private System.Windows.Forms.TextBox txtSiTefPath;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtSiTefCuitIsv;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtSitefCuit;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtSiTefIdTerminal;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtSiTefIdTienda;
        private System.Windows.Forms.Label label10;
    }
}