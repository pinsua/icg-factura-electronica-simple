﻿namespace IcgRestFCEConsola
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpComprobantes = new System.Windows.Forms.TabPage();
            this.gbAcciones = new System.Windows.Forms.GroupBox();
            this.btConfig = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btReimprimir = new System.Windows.Forms.Button();
            this.grComprobantes = new System.Windows.Forms.DataGridView();
            this.FO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.N = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRECLIENTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALBRUTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALNETO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMEROFISCAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIEFISCAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIEFISCAL2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIPCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODVENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANULACION_IRSA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HORAFIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMFACTURA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarUltimoComprobanteCAEAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtsMain = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataColumn35 = new System.Data.DataColumn();
            this.dataColumn36 = new System.Data.DataColumn();
            this.dataColumn37 = new System.Data.DataColumn();
            this.dataColumn38 = new System.Data.DataColumn();
            this.dataColumn39 = new System.Data.DataColumn();
            this.tpCaea = new System.Windows.Forms.TabPage();
            this.btSolicitarCAEA = new System.Windows.Forms.Button();
            this.grCaea = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaProcesoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDesdeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaHastaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaTopeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quincenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpInfomarCaea = new System.Windows.Forms.TabPage();
            this.btRefrescarSinInfomar = new System.Windows.Forms.Button();
            this.btCaeaInformar = new System.Windows.Forms.Button();
            this.grSinInformar = new System.Windows.Forms.DataGridView();
            this.FOSININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIESININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMEROSININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NSININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHASININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eRRORCAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESTADO_FESININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAEAINFORMADODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vTOCAEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODVENDEDORSININF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuCaea = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modificarNumeroFiscalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarFechaComprobanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcMain.SuspendLayout();
            this.tpComprobantes.SuspendLayout();
            this.gbAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grComprobantes)).BeginInit();
            this.MenuContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            this.tpCaea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCaea)).BeginInit();
            this.tpInfomarCaea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grSinInformar)).BeginInit();
            this.MenuCaea.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpComprobantes);
            this.tcMain.Controls.Add(this.tpCaea);
            this.tcMain.Controls.Add(this.tpInfomarCaea);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1200, 692);
            this.tcMain.TabIndex = 0;
            // 
            // tpComprobantes
            // 
            this.tpComprobantes.Controls.Add(this.gbAcciones);
            this.tpComprobantes.Controls.Add(this.grComprobantes);
            this.tpComprobantes.Location = new System.Drawing.Point(4, 29);
            this.tpComprobantes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpComprobantes.Name = "tpComprobantes";
            this.tpComprobantes.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpComprobantes.Size = new System.Drawing.Size(1192, 659);
            this.tpComprobantes.TabIndex = 0;
            this.tpComprobantes.Text = "Comprobantes Sin Fiscalizar";
            this.tpComprobantes.UseVisualStyleBackColor = true;
            // 
            // gbAcciones
            // 
            this.gbAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAcciones.BackColor = System.Drawing.Color.Transparent;
            this.gbAcciones.Controls.Add(this.btConfig);
            this.gbAcciones.Controls.Add(this.btImprimir);
            this.gbAcciones.Controls.Add(this.btReimprimir);
            this.gbAcciones.Location = new System.Drawing.Point(12, 549);
            this.gbAcciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Name = "gbAcciones";
            this.gbAcciones.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Size = new System.Drawing.Size(1164, 86);
            this.gbAcciones.TabIndex = 15;
            this.gbAcciones.TabStop = false;
            this.gbAcciones.Text = "ACCIONES";
            // 
            // btConfig
            // 
            this.btConfig.Location = new System.Drawing.Point(456, 29);
            this.btConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btConfig.Name = "btConfig";
            this.btConfig.Size = new System.Drawing.Size(177, 35);
            this.btConfig.TabIndex = 2;
            this.btConfig.Text = "Configuración";
            this.btConfig.UseVisualStyleBackColor = true;
            this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.Location = new System.Drawing.Point(224, 29);
            this.btImprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(177, 35);
            this.btImprimir.TabIndex = 1;
            this.btImprimir.Text = "Fiscalizar";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btReimprimir
            // 
            this.btReimprimir.Location = new System.Drawing.Point(9, 29);
            this.btReimprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btReimprimir.Name = "btReimprimir";
            this.btReimprimir.Size = new System.Drawing.Size(177, 35);
            this.btReimprimir.TabIndex = 0;
            this.btReimprimir.Text = "Refrescar Grilla";
            this.btReimprimir.UseVisualStyleBackColor = true;
            this.btReimprimir.Click += new System.EventHandler(this.btReimprimir_Click);
            // 
            // grComprobantes
            // 
            this.grComprobantes.AllowUserToAddRows = false;
            this.grComprobantes.AllowUserToDeleteRows = false;
            this.grComprobantes.AutoGenerateColumns = false;
            this.grComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FO,
            this.SERIE,
            this.NUMERO,
            this.N,
            this.FECHA,
            this.NOMBRECLIENTE,
            this.TOTALBRUTO,
            this.TOTALNETO,
            this.NUMEROFISCAL,
            this.SERIEFISCAL,
            this.SERIEFISCAL2,
            this.DESCRIPCION,
            this.Z,
            this.CODVENDEDOR,
            this.ANULACION_IRSA,
            this.HORAFIN,
            this.NUMFACTURA});
            this.grComprobantes.ContextMenuStrip = this.MenuContext;
            this.grComprobantes.DataMember = "Comprobantes";
            this.grComprobantes.DataSource = this.dtsMain;
            this.grComprobantes.Dock = System.Windows.Forms.DockStyle.Top;
            this.grComprobantes.Location = new System.Drawing.Point(4, 5);
            this.grComprobantes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grComprobantes.Name = "grComprobantes";
            this.grComprobantes.ReadOnly = true;
            this.grComprobantes.RowHeadersWidth = 62;
            this.grComprobantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grComprobantes.ShowCellToolTips = false;
            this.grComprobantes.ShowEditingIcon = false;
            this.grComprobantes.Size = new System.Drawing.Size(1184, 508);
            this.grComprobantes.TabIndex = 0;
            // 
            // FO
            // 
            this.FO.DataPropertyName = "FO";
            this.FO.HeaderText = "FO";
            this.FO.MinimumWidth = 8;
            this.FO.Name = "FO";
            this.FO.ReadOnly = true;
            this.FO.Width = 50;
            // 
            // SERIE
            // 
            this.SERIE.DataPropertyName = "SERIE";
            this.SERIE.HeaderText = "SERIE";
            this.SERIE.MinimumWidth = 8;
            this.SERIE.Name = "SERIE";
            this.SERIE.ReadOnly = true;
            this.SERIE.Width = 60;
            // 
            // NUMERO
            // 
            this.NUMERO.DataPropertyName = "NUMERO";
            this.NUMERO.HeaderText = "NUMERO";
            this.NUMERO.MinimumWidth = 8;
            this.NUMERO.Name = "NUMERO";
            this.NUMERO.ReadOnly = true;
            this.NUMERO.Width = 60;
            // 
            // N
            // 
            this.N.DataPropertyName = "N";
            this.N.HeaderText = "N";
            this.N.MinimumWidth = 8;
            this.N.Name = "N";
            this.N.ReadOnly = true;
            this.N.Width = 50;
            // 
            // FECHA
            // 
            this.FECHA.DataPropertyName = "FECHA";
            this.FECHA.HeaderText = "FECHA";
            this.FECHA.MinimumWidth = 8;
            this.FECHA.Name = "FECHA";
            this.FECHA.ReadOnly = true;
            this.FECHA.Width = 150;
            // 
            // NOMBRECLIENTE
            // 
            this.NOMBRECLIENTE.DataPropertyName = "NOMBRECLIENTE";
            this.NOMBRECLIENTE.HeaderText = "NOMBRECLIENTE";
            this.NOMBRECLIENTE.MinimumWidth = 8;
            this.NOMBRECLIENTE.Name = "NOMBRECLIENTE";
            this.NOMBRECLIENTE.ReadOnly = true;
            this.NOMBRECLIENTE.Width = 150;
            // 
            // TOTALBRUTO
            // 
            this.TOTALBRUTO.DataPropertyName = "TOTALBRUTO";
            this.TOTALBRUTO.HeaderText = "TOTALBRUTO";
            this.TOTALBRUTO.MinimumWidth = 8;
            this.TOTALBRUTO.Name = "TOTALBRUTO";
            this.TOTALBRUTO.ReadOnly = true;
            this.TOTALBRUTO.Width = 150;
            // 
            // TOTALNETO
            // 
            this.TOTALNETO.DataPropertyName = "TOTALNETO";
            this.TOTALNETO.HeaderText = "TOTALNETO";
            this.TOTALNETO.MinimumWidth = 8;
            this.TOTALNETO.Name = "TOTALNETO";
            this.TOTALNETO.ReadOnly = true;
            this.TOTALNETO.Width = 150;
            // 
            // NUMEROFISCAL
            // 
            this.NUMEROFISCAL.DataPropertyName = "NUMEROFISCAL";
            this.NUMEROFISCAL.HeaderText = "NUMEROFISCAL";
            this.NUMEROFISCAL.MinimumWidth = 8;
            this.NUMEROFISCAL.Name = "NUMEROFISCAL";
            this.NUMEROFISCAL.ReadOnly = true;
            this.NUMEROFISCAL.Width = 150;
            // 
            // SERIEFISCAL
            // 
            this.SERIEFISCAL.DataPropertyName = "SERIEFISCAL";
            this.SERIEFISCAL.HeaderText = "SERIEFISCAL";
            this.SERIEFISCAL.MinimumWidth = 8;
            this.SERIEFISCAL.Name = "SERIEFISCAL";
            this.SERIEFISCAL.ReadOnly = true;
            this.SERIEFISCAL.Width = 150;
            // 
            // SERIEFISCAL2
            // 
            this.SERIEFISCAL2.DataPropertyName = "SERIEFISCAL2";
            this.SERIEFISCAL2.HeaderText = "SERIEFISCAL2";
            this.SERIEFISCAL2.MinimumWidth = 8;
            this.SERIEFISCAL2.Name = "SERIEFISCAL2";
            this.SERIEFISCAL2.ReadOnly = true;
            this.SERIEFISCAL2.Width = 150;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.DataPropertyName = "DESCRIPCION";
            this.DESCRIPCION.HeaderText = "DESCRIPCION";
            this.DESCRIPCION.MinimumWidth = 8;
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.ReadOnly = true;
            this.DESCRIPCION.Width = 150;
            // 
            // Z
            // 
            this.Z.DataPropertyName = "Z";
            this.Z.HeaderText = "Z";
            this.Z.MinimumWidth = 8;
            this.Z.Name = "Z";
            this.Z.ReadOnly = true;
            this.Z.Width = 50;
            // 
            // CODVENDEDOR
            // 
            this.CODVENDEDOR.DataPropertyName = "CODVENDEDOR";
            this.CODVENDEDOR.HeaderText = "CODVENDEDOR";
            this.CODVENDEDOR.MinimumWidth = 8;
            this.CODVENDEDOR.Name = "CODVENDEDOR";
            this.CODVENDEDOR.ReadOnly = true;
            this.CODVENDEDOR.Width = 150;
            // 
            // ANULACION_IRSA
            // 
            this.ANULACION_IRSA.DataPropertyName = "ANULACION_IRSA";
            this.ANULACION_IRSA.HeaderText = "ANULACION_IRSA";
            this.ANULACION_IRSA.MinimumWidth = 8;
            this.ANULACION_IRSA.Name = "ANULACION_IRSA";
            this.ANULACION_IRSA.ReadOnly = true;
            this.ANULACION_IRSA.Width = 150;
            // 
            // HORAFIN
            // 
            this.HORAFIN.DataPropertyName = "HORAFIN";
            this.HORAFIN.HeaderText = "HORAFIN";
            this.HORAFIN.MinimumWidth = 8;
            this.HORAFIN.Name = "HORAFIN";
            this.HORAFIN.ReadOnly = true;
            this.HORAFIN.Width = 150;
            // 
            // NUMFACTURA
            // 
            this.NUMFACTURA.DataPropertyName = "NUMFACTURA";
            this.NUMFACTURA.HeaderText = "NUMFACTURA";
            this.NUMFACTURA.MinimumWidth = 8;
            this.NUMFACTURA.Name = "NUMFACTURA";
            this.NUMFACTURA.ReadOnly = true;
            this.NUMFACTURA.Width = 150;
            // 
            // MenuContext
            // 
            this.MenuContext.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MenuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFechaToolStripMenuItem,
            this.borrarToolStripMenuItem,
            this.consultarUltimoComprobanteCAEAToolStripMenuItem});
            this.MenuContext.Name = "MenuContext";
            this.MenuContext.Size = new System.Drawing.Size(378, 100);
            // 
            // cambiarFechaToolStripMenuItem
            // 
            this.cambiarFechaToolStripMenuItem.Name = "cambiarFechaToolStripMenuItem";
            this.cambiarFechaToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.cambiarFechaToolStripMenuItem.Text = "Cambiar Fecha";
            this.cambiarFechaToolStripMenuItem.Click += new System.EventHandler(this.cambiarFechaToolStripMenuItem_Click);
            // 
            // borrarToolStripMenuItem
            // 
            this.borrarToolStripMenuItem.Name = "borrarToolStripMenuItem";
            this.borrarToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.borrarToolStripMenuItem.Text = "Borrar";
            this.borrarToolStripMenuItem.Click += new System.EventHandler(this.borrarToolStripMenuItem_Click);
            // 
            // consultarUltimoComprobanteCAEAToolStripMenuItem
            // 
            this.consultarUltimoComprobanteCAEAToolStripMenuItem.Name = "consultarUltimoComprobanteCAEAToolStripMenuItem";
            this.consultarUltimoComprobanteCAEAToolStripMenuItem.Size = new System.Drawing.Size(377, 32);
            this.consultarUltimoComprobanteCAEAToolStripMenuItem.Text = "Consultar ultimo comprobante CAEA";
            this.consultarUltimoComprobanteCAEAToolStripMenuItem.Click += new System.EventHandler(this.consultarUltimoComprobanteCAEAToolStripMenuItem_Click);
            // 
            // dtsMain
            // 
            this.dtsMain.DataSetName = "NewDataSet";
            this.dtsMain.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17});
            this.dataTable1.TableName = "Comprobantes";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "FO";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "SERIE";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "NUMERO";
            this.dataColumn3.DataType = typeof(int);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "N";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "NUMFACTURA";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "TOTALBRUTO";
            this.dataColumn6.DataType = typeof(decimal);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "TOTALNETO";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "NOMBRECLIENTE";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "SERIEFISCAL";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "NUMEROFISCAL";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "DESCRIPCION";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "FECHA";
            this.dataColumn13.DataType = typeof(System.DateTime);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "Z";
            this.dataColumn14.DataType = typeof(int);
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "CODVENDEDOR";
            this.dataColumn15.DataType = typeof(int);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "ANULACION_IRSA";
            this.dataColumn16.DataType = typeof(int);
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "HORAFIN";
            this.dataColumn17.DataType = typeof(System.DateTime);
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25});
            this.dataTable2.TableName = "Caea";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "ID";
            this.dataColumn18.DataType = typeof(int);
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "CAEA";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "FechaProceso";
            this.dataColumn20.DataType = typeof(System.DateTime);
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "FechaDesde";
            this.dataColumn21.DataType = typeof(System.DateTime);
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "FechaHasta";
            this.dataColumn22.DataType = typeof(System.DateTime);
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "FechaTope";
            this.dataColumn23.DataType = typeof(System.DateTime);
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "Periodo";
            this.dataColumn24.DataType = typeof(int);
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnName = "Quincena";
            this.dataColumn25.DataType = typeof(int);
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35,
            this.dataColumn36,
            this.dataColumn37,
            this.dataColumn38,
            this.dataColumn39});
            this.dataTable3.TableName = "SinInformar";
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "SERIE";
            // 
            // dataColumn27
            // 
            this.dataColumn27.ColumnName = "NUMERO";
            this.dataColumn27.DataType = typeof(int);
            // 
            // dataColumn28
            // 
            this.dataColumn28.ColumnName = "N";
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "FO";
            this.dataColumn29.DataType = typeof(int);
            // 
            // dataColumn30
            // 
            this.dataColumn30.ColumnName = "CAE";
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnName = "ERROR_CAE";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnName = "VTO_CAE";
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "ESTADO_FE";
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "CAEA_INFORMADO";
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "FECHA";
            this.dataColumn35.DataType = typeof(System.DateTime);
            // 
            // dataColumn36
            // 
            this.dataColumn36.ColumnName = "SERIEFISCAL";
            // 
            // dataColumn37
            // 
            this.dataColumn37.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn38
            // 
            this.dataColumn38.ColumnName = "NUMEROFISCAL";
            // 
            // dataColumn39
            // 
            this.dataColumn39.ColumnName = "CODVENDEDOR";
            this.dataColumn39.DataType = typeof(int);
            this.dataColumn39.ReadOnly = true;
            // 
            // tpCaea
            // 
            this.tpCaea.Controls.Add(this.btSolicitarCAEA);
            this.tpCaea.Controls.Add(this.grCaea);
            this.tpCaea.Location = new System.Drawing.Point(4, 29);
            this.tpCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCaea.Name = "tpCaea";
            this.tpCaea.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCaea.Size = new System.Drawing.Size(1192, 659);
            this.tpCaea.TabIndex = 1;
            this.tpCaea.Text = "Administración de CAEA";
            this.tpCaea.UseVisualStyleBackColor = true;
            // 
            // btSolicitarCAEA
            // 
            this.btSolicitarCAEA.Location = new System.Drawing.Point(393, 548);
            this.btSolicitarCAEA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSolicitarCAEA.Name = "btSolicitarCAEA";
            this.btSolicitarCAEA.Size = new System.Drawing.Size(314, 35);
            this.btSolicitarCAEA.TabIndex = 1;
            this.btSolicitarCAEA.Text = "Solicitar Nuevo CAEA para el período";
            this.btSolicitarCAEA.UseVisualStyleBackColor = true;
            this.btSolicitarCAEA.Click += new System.EventHandler(this.btSolicitarCAEA_Click);
            // 
            // grCaea
            // 
            this.grCaea.AllowUserToAddRows = false;
            this.grCaea.AllowUserToDeleteRows = false;
            this.grCaea.AutoGenerateColumns = false;
            this.grCaea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grCaea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.cAEADataGridViewTextBoxColumn,
            this.fechaProcesoDataGridViewTextBoxColumn,
            this.fechaDesdeDataGridViewTextBoxColumn,
            this.fechaHastaDataGridViewTextBoxColumn,
            this.fechaTopeDataGridViewTextBoxColumn,
            this.periodoDataGridViewTextBoxColumn,
            this.quincenaDataGridViewTextBoxColumn});
            this.grCaea.DataMember = "Caea";
            this.grCaea.DataSource = this.dtsMain;
            this.grCaea.Dock = System.Windows.Forms.DockStyle.Top;
            this.grCaea.Location = new System.Drawing.Point(4, 5);
            this.grCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grCaea.MultiSelect = false;
            this.grCaea.Name = "grCaea";
            this.grCaea.ReadOnly = true;
            this.grCaea.RowHeadersWidth = 62;
            this.grCaea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grCaea.Size = new System.Drawing.Size(1184, 500);
            this.grCaea.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 150;
            // 
            // cAEADataGridViewTextBoxColumn
            // 
            this.cAEADataGridViewTextBoxColumn.DataPropertyName = "CAEA";
            this.cAEADataGridViewTextBoxColumn.HeaderText = "CAEA";
            this.cAEADataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEADataGridViewTextBoxColumn.Name = "cAEADataGridViewTextBoxColumn";
            this.cAEADataGridViewTextBoxColumn.ReadOnly = true;
            this.cAEADataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaProcesoDataGridViewTextBoxColumn
            // 
            this.fechaProcesoDataGridViewTextBoxColumn.DataPropertyName = "FechaProceso";
            this.fechaProcesoDataGridViewTextBoxColumn.HeaderText = "FechaProceso";
            this.fechaProcesoDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaProcesoDataGridViewTextBoxColumn.Name = "fechaProcesoDataGridViewTextBoxColumn";
            this.fechaProcesoDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaProcesoDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaDesdeDataGridViewTextBoxColumn
            // 
            this.fechaDesdeDataGridViewTextBoxColumn.DataPropertyName = "FechaDesde";
            this.fechaDesdeDataGridViewTextBoxColumn.HeaderText = "FechaDesde";
            this.fechaDesdeDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaDesdeDataGridViewTextBoxColumn.Name = "fechaDesdeDataGridViewTextBoxColumn";
            this.fechaDesdeDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaDesdeDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaHastaDataGridViewTextBoxColumn
            // 
            this.fechaHastaDataGridViewTextBoxColumn.DataPropertyName = "FechaHasta";
            this.fechaHastaDataGridViewTextBoxColumn.HeaderText = "FechaHasta";
            this.fechaHastaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaHastaDataGridViewTextBoxColumn.Name = "fechaHastaDataGridViewTextBoxColumn";
            this.fechaHastaDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaHastaDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaTopeDataGridViewTextBoxColumn
            // 
            this.fechaTopeDataGridViewTextBoxColumn.DataPropertyName = "FechaTope";
            this.fechaTopeDataGridViewTextBoxColumn.HeaderText = "FechaTope";
            this.fechaTopeDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaTopeDataGridViewTextBoxColumn.Name = "fechaTopeDataGridViewTextBoxColumn";
            this.fechaTopeDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaTopeDataGridViewTextBoxColumn.Width = 150;
            // 
            // periodoDataGridViewTextBoxColumn
            // 
            this.periodoDataGridViewTextBoxColumn.DataPropertyName = "Periodo";
            this.periodoDataGridViewTextBoxColumn.HeaderText = "Periodo";
            this.periodoDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.periodoDataGridViewTextBoxColumn.Name = "periodoDataGridViewTextBoxColumn";
            this.periodoDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodoDataGridViewTextBoxColumn.Width = 150;
            // 
            // quincenaDataGridViewTextBoxColumn
            // 
            this.quincenaDataGridViewTextBoxColumn.DataPropertyName = "Quincena";
            this.quincenaDataGridViewTextBoxColumn.HeaderText = "Quincena";
            this.quincenaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.quincenaDataGridViewTextBoxColumn.Name = "quincenaDataGridViewTextBoxColumn";
            this.quincenaDataGridViewTextBoxColumn.ReadOnly = true;
            this.quincenaDataGridViewTextBoxColumn.Width = 150;
            // 
            // tpInfomarCaea
            // 
            this.tpInfomarCaea.Controls.Add(this.btRefrescarSinInfomar);
            this.tpInfomarCaea.Controls.Add(this.btCaeaInformar);
            this.tpInfomarCaea.Controls.Add(this.grSinInformar);
            this.tpInfomarCaea.Location = new System.Drawing.Point(4, 29);
            this.tpInfomarCaea.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpInfomarCaea.Name = "tpInfomarCaea";
            this.tpInfomarCaea.Size = new System.Drawing.Size(1192, 659);
            this.tpInfomarCaea.TabIndex = 2;
            this.tpInfomarCaea.Text = "CAEA Sin Informar";
            this.tpInfomarCaea.UseVisualStyleBackColor = true;
            // 
            // btRefrescarSinInfomar
            // 
            this.btRefrescarSinInfomar.Location = new System.Drawing.Point(48, 578);
            this.btRefrescarSinInfomar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btRefrescarSinInfomar.Name = "btRefrescarSinInfomar";
            this.btRefrescarSinInfomar.Size = new System.Drawing.Size(192, 35);
            this.btRefrescarSinInfomar.TabIndex = 2;
            this.btRefrescarSinInfomar.Text = "Refrescar Grilla";
            this.btRefrescarSinInfomar.UseVisualStyleBackColor = true;
            this.btRefrescarSinInfomar.Click += new System.EventHandler(this.btRefrescarSinInfomar_Click);
            // 
            // btCaeaInformar
            // 
            this.btCaeaInformar.Location = new System.Drawing.Point(264, 578);
            this.btCaeaInformar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCaeaInformar.Name = "btCaeaInformar";
            this.btCaeaInformar.Size = new System.Drawing.Size(192, 35);
            this.btCaeaInformar.TabIndex = 1;
            this.btCaeaInformar.Text = "Informar CAEA";
            this.btCaeaInformar.UseVisualStyleBackColor = true;
            this.btCaeaInformar.Click += new System.EventHandler(this.btCaeaInformar_Click);
            // 
            // grSinInformar
            // 
            this.grSinInformar.AllowUserToAddRows = false;
            this.grSinInformar.AllowUserToDeleteRows = false;
            this.grSinInformar.AllowUserToResizeRows = false;
            this.grSinInformar.AutoGenerateColumns = false;
            this.grSinInformar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grSinInformar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FOSININF,
            this.SERIESININF,
            this.NUMEROSININF,
            this.NSININF,
            this.FECHASININF,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.cAEDataGridViewTextBoxColumn,
            this.eRRORCAEDataGridViewTextBoxColumn,
            this.ESTADO_FESININF,
            this.cAEAINFORMADODataGridViewTextBoxColumn,
            this.vTOCAEDataGridViewTextBoxColumn,
            this.CODVENDEDORSININF});
            this.grSinInformar.ContextMenuStrip = this.MenuCaea;
            this.grSinInformar.DataMember = "SinInformar";
            this.grSinInformar.DataSource = this.dtsMain;
            this.grSinInformar.Dock = System.Windows.Forms.DockStyle.Top;
            this.grSinInformar.Location = new System.Drawing.Point(0, 0);
            this.grSinInformar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grSinInformar.MultiSelect = false;
            this.grSinInformar.Name = "grSinInformar";
            this.grSinInformar.ReadOnly = true;
            this.grSinInformar.RowHeadersWidth = 62;
            this.grSinInformar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grSinInformar.Size = new System.Drawing.Size(1192, 552);
            this.grSinInformar.TabIndex = 0;
            // 
            // FOSININF
            // 
            this.FOSININF.DataPropertyName = "FO";
            this.FOSININF.HeaderText = "FO";
            this.FOSININF.MinimumWidth = 8;
            this.FOSININF.Name = "FOSININF";
            this.FOSININF.ReadOnly = true;
            this.FOSININF.Width = 80;
            // 
            // SERIESININF
            // 
            this.SERIESININF.DataPropertyName = "SERIE";
            this.SERIESININF.HeaderText = "SERIE";
            this.SERIESININF.MinimumWidth = 8;
            this.SERIESININF.Name = "SERIESININF";
            this.SERIESININF.ReadOnly = true;
            this.SERIESININF.Width = 80;
            // 
            // NUMEROSININF
            // 
            this.NUMEROSININF.DataPropertyName = "NUMERO";
            this.NUMEROSININF.HeaderText = "NUMERO";
            this.NUMEROSININF.MinimumWidth = 8;
            this.NUMEROSININF.Name = "NUMEROSININF";
            this.NUMEROSININF.ReadOnly = true;
            this.NUMEROSININF.Width = 150;
            // 
            // NSININF
            // 
            this.NSININF.DataPropertyName = "N";
            this.NSININF.HeaderText = "N";
            this.NSININF.MinimumWidth = 8;
            this.NSININF.Name = "NSININF";
            this.NSININF.ReadOnly = true;
            this.NSININF.Width = 80;
            // 
            // FECHASININF
            // 
            this.FECHASININF.DataPropertyName = "FECHA";
            this.FECHASININF.HeaderText = "FECHA";
            this.FECHASININF.MinimumWidth = 8;
            this.FECHASININF.Name = "FECHASININF";
            this.FECHASININF.ReadOnly = true;
            this.FECHASININF.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SERIEFISCAL";
            this.dataGridViewTextBoxColumn2.HeaderText = "SERIEFISCAL";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SERIEFISCAL2";
            this.dataGridViewTextBoxColumn3.HeaderText = "SERIEFISCAL2";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NUMEROFISCAL";
            this.dataGridViewTextBoxColumn4.HeaderText = "NUMEROFISCAL";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 8;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // cAEDataGridViewTextBoxColumn
            // 
            this.cAEDataGridViewTextBoxColumn.DataPropertyName = "CAE";
            this.cAEDataGridViewTextBoxColumn.HeaderText = "CAE";
            this.cAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEDataGridViewTextBoxColumn.Name = "cAEDataGridViewTextBoxColumn";
            this.cAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.cAEDataGridViewTextBoxColumn.Width = 150;
            // 
            // eRRORCAEDataGridViewTextBoxColumn
            // 
            this.eRRORCAEDataGridViewTextBoxColumn.DataPropertyName = "ERROR_CAE";
            this.eRRORCAEDataGridViewTextBoxColumn.HeaderText = "ERROR_CAE";
            this.eRRORCAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.eRRORCAEDataGridViewTextBoxColumn.Name = "eRRORCAEDataGridViewTextBoxColumn";
            this.eRRORCAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.eRRORCAEDataGridViewTextBoxColumn.Width = 150;
            // 
            // ESTADO_FESININF
            // 
            this.ESTADO_FESININF.DataPropertyName = "ESTADO_FE";
            this.ESTADO_FESININF.HeaderText = "ESTADO_FE";
            this.ESTADO_FESININF.MinimumWidth = 8;
            this.ESTADO_FESININF.Name = "ESTADO_FESININF";
            this.ESTADO_FESININF.ReadOnly = true;
            this.ESTADO_FESININF.Width = 150;
            // 
            // cAEAINFORMADODataGridViewTextBoxColumn
            // 
            this.cAEAINFORMADODataGridViewTextBoxColumn.DataPropertyName = "CAEA_INFORMADO";
            this.cAEAINFORMADODataGridViewTextBoxColumn.HeaderText = "CAEA_INFORMADO";
            this.cAEAINFORMADODataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cAEAINFORMADODataGridViewTextBoxColumn.Name = "cAEAINFORMADODataGridViewTextBoxColumn";
            this.cAEAINFORMADODataGridViewTextBoxColumn.ReadOnly = true;
            this.cAEAINFORMADODataGridViewTextBoxColumn.Width = 150;
            // 
            // vTOCAEDataGridViewTextBoxColumn
            // 
            this.vTOCAEDataGridViewTextBoxColumn.DataPropertyName = "VTO_CAE";
            this.vTOCAEDataGridViewTextBoxColumn.HeaderText = "VTO_CAE";
            this.vTOCAEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.vTOCAEDataGridViewTextBoxColumn.Name = "vTOCAEDataGridViewTextBoxColumn";
            this.vTOCAEDataGridViewTextBoxColumn.ReadOnly = true;
            this.vTOCAEDataGridViewTextBoxColumn.Width = 150;
            // 
            // CODVENDEDORSININF
            // 
            this.CODVENDEDORSININF.DataPropertyName = "CODVENDEDOR";
            this.CODVENDEDORSININF.HeaderText = "CODVENDEDOR";
            this.CODVENDEDORSININF.MinimumWidth = 8;
            this.CODVENDEDORSININF.Name = "CODVENDEDORSININF";
            this.CODVENDEDORSININF.ReadOnly = true;
            this.CODVENDEDORSININF.Visible = false;
            this.CODVENDEDORSININF.Width = 150;
            // 
            // MenuCaea
            // 
            this.MenuCaea.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MenuCaea.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFechaComprobanteToolStripMenuItem,
            this.modificarNumeroFiscalToolStripMenuItem});
            this.MenuCaea.Name = "MenuCaea";
            this.MenuCaea.Size = new System.Drawing.Size(317, 68);
            // 
            // modificarNumeroFiscalToolStripMenuItem
            // 
            this.modificarNumeroFiscalToolStripMenuItem.Name = "modificarNumeroFiscalToolStripMenuItem";
            this.modificarNumeroFiscalToolStripMenuItem.Size = new System.Drawing.Size(316, 32);
            this.modificarNumeroFiscalToolStripMenuItem.Text = "Modificar Numero Fiscal";
            this.modificarNumeroFiscalToolStripMenuItem.Click += new System.EventHandler(this.modificarNumeroFiscalToolStripMenuItem_Click);
            // 
            // cambiarFechaComprobanteToolStripMenuItem
            // 
            this.cambiarFechaComprobanteToolStripMenuItem.Name = "cambiarFechaComprobanteToolStripMenuItem";
            this.cambiarFechaComprobanteToolStripMenuItem.Size = new System.Drawing.Size(316, 32);
            this.cambiarFechaComprobanteToolStripMenuItem.Text = "Cambiar Fecha Comprobante";
            this.cambiarFechaComprobanteToolStripMenuItem.Click += new System.EventHandler(this.cambiarFechaComprobanteToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.tcMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tcMain.ResumeLayout(false);
            this.tpComprobantes.ResumeLayout(false);
            this.gbAcciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grComprobantes)).EndInit();
            this.MenuContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            this.tpCaea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCaea)).EndInit();
            this.tpInfomarCaea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grSinInformar)).EndInit();
            this.MenuCaea.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpComprobantes;
        private System.Windows.Forms.TabPage tpCaea;
        private System.Windows.Forms.DataGridView grComprobantes;
        private System.Windows.Forms.DataGridView grCaea;
        private System.Data.DataSet dtsMain;
        private System.Data.DataTable dataTable1;
        private System.Data.DataTable dataTable2;
        private System.Windows.Forms.GroupBox gbAcciones;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btReimprimir;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaProcesoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDesdeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaHastaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaTopeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quincenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tpInfomarCaea;
        private System.Windows.Forms.Button btSolicitarCAEA;
        private System.Windows.Forms.Button btCaeaInformar;
        private System.Windows.Forms.DataGridView grSinInformar;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataColumn dataColumn35;
        private System.Data.DataColumn dataColumn36;
        private System.Data.DataColumn dataColumn37;
        private System.Data.DataColumn dataColumn38;
        private System.Windows.Forms.Button btRefrescarSinInfomar;
        private System.Windows.Forms.Button btConfig;
        private System.Data.DataColumn dataColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn FO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn N;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRECLIENTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALBRUTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALNETO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMEROFISCAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIEFISCAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIEFISCAL2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIPCION;
        private System.Windows.Forms.DataGridViewTextBoxColumn Z;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODVENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANULACION_IRSA;
        private System.Windows.Forms.DataGridViewTextBoxColumn HORAFIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMFACTURA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOSININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIESININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMEROSININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn NSININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHASININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eRRORCAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESTADO_FESININF;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAEAINFORMADODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vTOCAEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODVENDEDORSININF;
        private System.Windows.Forms.ContextMenuStrip MenuContext;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MenuCaea;
        private System.Windows.Forms.ToolStripMenuItem modificarNumeroFiscalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarUltimoComprobanteCAEAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaComprobanteToolStripMenuItem;
    }
}