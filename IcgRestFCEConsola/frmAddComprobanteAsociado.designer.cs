﻿namespace IcgRestFCEConsola
{
    partial class frmAddComprobanteAsociado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboTipoComprobante = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFechaComprobante = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPtoVtaComprobante = new System.Windows.Forms.TextBox();
            this.txtNumeroComprobante = new System.Windows.Forms.TextBox();
            this.txtCuitComprobante = new System.Windows.Forms.TextBox();
            this.btnAcept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(41, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(699, 30);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Por disposición de la AFIP, toda Nota de Crédito o Debito debe ser informada con " +
    "el comprobante asociado.\r\nPor favor ingrese los datos del mismo, caso contrario " +
    "no se podrá fiscalizar el comprobante.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tipo de comprobante ";
            // 
            // cboTipoComprobante
            // 
            this.cboTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoComprobante.FormattingEnabled = true;
            this.cboTipoComprobante.Items.AddRange(new object[] {
            "001_FACTURA ELECTRÓNICA A",
            "006_FACTURA ELECTRÓNICA B",
            "011_FACTURA ELECTRÓNICA C",
            "201_FACTURA DE CRÉDITO ELECTRÓNICA MiPyME (FCE) A",
            "206_FACTURA DE CRÉDITO ELECTRÓNICA MiPyME (FCE) B",
            "211_FACTURA DE CRÉDITO ELECTRÓNICA MiPyME (FCE) C"});
            this.cboTipoComprobante.Location = new System.Drawing.Point(298, 66);
            this.cboTipoComprobante.Name = "cboTipoComprobante";
            this.cboTipoComprobante.Size = new System.Drawing.Size(365, 21);
            this.cboTipoComprobante.TabIndex = 2;
            this.cboTipoComprobante.SelectionChangeCommitted += new System.EventHandler(this.cboTipoComprobante_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Fecha del Comprobante";
            // 
            // dtpFechaComprobante
            // 
            this.dtpFechaComprobante.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaComprobante.Location = new System.Drawing.Point(298, 101);
            this.dtpFechaComprobante.Name = "dtpFechaComprobante";
            this.dtpFechaComprobante.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaComprobante.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Punto de Venta del Comprobante";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Numero del Comprobante";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(106, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "CUIT del reseptor del Comprobante";
            // 
            // txtPtoVtaComprobante
            // 
            this.txtPtoVtaComprobante.Location = new System.Drawing.Point(298, 135);
            this.txtPtoVtaComprobante.Name = "txtPtoVtaComprobante";
            this.txtPtoVtaComprobante.Size = new System.Drawing.Size(200, 20);
            this.txtPtoVtaComprobante.TabIndex = 8;
            this.txtPtoVtaComprobante.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPtoVtaComprobante_KeyPress);
            // 
            // txtNumeroComprobante
            // 
            this.txtNumeroComprobante.Location = new System.Drawing.Point(298, 169);
            this.txtNumeroComprobante.Name = "txtNumeroComprobante";
            this.txtNumeroComprobante.Size = new System.Drawing.Size(200, 20);
            this.txtNumeroComprobante.TabIndex = 9;
            this.txtNumeroComprobante.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPtoVtaComprobante_KeyPress);
            // 
            // txtCuitComprobante
            // 
            this.txtCuitComprobante.Location = new System.Drawing.Point(298, 203);
            this.txtCuitComprobante.Name = "txtCuitComprobante";
            this.txtCuitComprobante.Size = new System.Drawing.Size(200, 20);
            this.txtCuitComprobante.TabIndex = 10;
            this.txtCuitComprobante.Validating += new System.ComponentModel.CancelEventHandler(this.txtCuitComprobante_Validating);
            // 
            // btnAcept
            // 
            this.btnAcept.Location = new System.Drawing.Point(262, 246);
            this.btnAcept.Name = "btnAcept";
            this.btnAcept.Size = new System.Drawing.Size(128, 23);
            this.btnAcept.TabIndex = 11;
            this.btnAcept.Text = "Aceptar";
            this.btnAcept.UseVisualStyleBackColor = true;
            this.btnAcept.Click += new System.EventHandler(this.btnAcept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(406, 246);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(128, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmAddComprobanteAsociado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 301);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAcept);
            this.Controls.Add(this.txtCuitComprobante);
            this.Controls.Add(this.txtNumeroComprobante);
            this.Controls.Add(this.txtPtoVtaComprobante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFechaComprobante);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboTipoComprobante);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddComprobanteAsociado";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - Por favor ingrese el comprobante asociado.";
            this.Load += new System.EventHandler(this.frmAddComprobanteAsociado_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboTipoComprobante;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFechaComprobante;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPtoVtaComprobante;
        private System.Windows.Forms.TextBox txtNumeroComprobante;
        private System.Windows.Forms.TextBox txtCuitComprobante;
        private System.Windows.Forms.Button btnAcept;
        private System.Windows.Forms.Button btnCancel;
    }
}