﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IcgVarios;

namespace IcgRestFCEConsola
{
    public partial class frmKey : Form
    {
        public frmKey()
        {
            InitializeComponent();
            textBox1.Text = IcgVarios.LicenciaIcg.Value();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
