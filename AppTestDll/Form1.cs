﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AppTestDll
{
    public partial class Form1 : Form
    {
        int intCodigoIVA;
        int intCodigoIIBB;
        public static string _nombreCertificado;
        public static string strConnectionString;
        public static string strCUIT;
        public static bool _EsTest = false;
        public static string _pathApp;
        public static string _pathLog;
        public static string _pathCerificado;
        public static string _pathTaFC;
        public static string _pathTaFCE;
        public static string _pathTaFCMTXCA;
        public static string _appName;
        public static int _codigoEmpresa;
        public static string _razonSocial;

        public string _certificado;
        public string _password;
        public string _pruebaCert;



        public Form1()
        {
            InitializeComponent();

            //_nombreCertificado = System.Configuration.ConfigurationManager.AppSettings["NombreCertificado"];
            intCodigoIVA = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["CodigoIVA"]);
            intCodigoIIBB = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["CodigoIIBB"]);
            //strConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["IcgConnection"].ConnectionString;
            strCUIT = System.Configuration.ConfigurationManager.AppSettings["CUIT"];
            _certificado = System.Configuration.ConfigurationManager.AppSettings["Certificado"];
            _password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            _EsTest = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsTest"]);
            _appName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ValidarCarpetas();

            txtCertificado.Text = _certificado;
            txtCuit.Text = strCUIT;
            txtPassword.Text = _password;

            decimal _dec = Convert.ToDecimal("10,5");
            
            decimal b = _dec - Math.Floor(_dec);
            //double c = a - b;
            int _int = (int)(b);

            
            
        }

        private void ValidarCarpetas()
        {
            try
            {
                //recupero la ubicacion del ejecutable.
                _pathApp = Application.StartupPath.ToString();
                //Cargo la ubicaciones.
                _pathCerificado = _pathApp + "\\Certificado";
                _pathLog = _pathApp + "\\Log";
                _pathTaFC = _pathApp + "\\TAFC";
                _pathTaFCE = _pathApp + "\\TAFCE";
                _pathTaFCMTXCA = _pathApp + "\\TAFCMTXCA";

                //Validamos que existan los Directorios
                if (!Directory.Exists(_pathCerificado))
                {
                    //Si no existe la creamos.
                    Directory.CreateDirectory(_pathCerificado);
                    //Informamos que debe instalar el certificado en la ruta.
                    MessageBox.Show("Se ha creado el Directorio para el certificado de la AFIP (" + _pathCerificado + "). Por favor coloque el certificado en este directorio para poder continuar.");
                }
                else
                {
                    if (!String.IsNullOrEmpty(_nombreCertificado))
                    {
                        if (!File.Exists(_pathCerificado + "\\" + _nombreCertificado))
                            MessageBox.Show("El certificado no se encuentra en la directorio " + _pathCerificado + ". Por favor Por favor coloque el certificado en este directorio para poder continuar.");
                        else
                            _pathCerificado = _pathCerificado + "\\" + _nombreCertificado;
                    }
                }
                //Validamos el log.
                if (!Directory.Exists(_pathLog))
                    Directory.CreateDirectory(_pathLog);
                //Validamos el FC
                if (!Directory.Exists(_pathTaFC))
                    Directory.CreateDirectory(_pathTaFC);
                //Validamos el FCE.
                if (!Directory.Exists(_pathTaFCE))
                    Directory.CreateDirectory(_pathTaFCE);
                //Validamos el FCEMTXCA.
                if (!Directory.Exists(_pathTaFCMTXCA))
                    Directory.CreateDirectory(_pathTaFCMTXCA);
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error: " + ex.Message + " validando los directorios.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if(!_pathCerificado.Contains(txtCertificado.Text))
                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

            List<AfipDll.wsAfip.PtoVenta> _ptoVta = AfipDll.wsAfip.ConsultarPuntosVentaNew(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text, _EsTest,
                strPasswordSecureString, false, out _lstErrores);

            if (_ptoVta.Count > 0)
            {
                foreach (AfipDll.wsAfip.PtoVenta _pv in _ptoVta)
                {
                    MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                        "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                        "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                        "Fecha de Baja: " + _pv.FechaBaja);
                }
            }
            else
            {
                MessageBox.Show("No se recivieron puntos de venta");

                foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                {
                    MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                        "Descripcion: " + _er.Msg);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPeriodo.Text))
            {
                MessageBox.Show("Debe ingresar el Periodo");
                return;
            }

            if (String.IsNullOrEmpty(txtOrden.Text))
            {
                MessageBox.Show("Debe ingresar el Orden");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                int _periodo = Convert.ToInt32(txtPeriodo.Text);
                Int16 _orden = Convert.ToInt16(txtOrden.Text);

                AfipDll.wsAfip.CaeaSolicitar _caea = AfipDll.wsAfip.SolicitarCAEANew(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text, _EsTest, 
                    strPasswordSecureString, _periodo, _orden, false, out _lstErrores);

                if(_caea.CAEA != null)
                {
                    MessageBox.Show("CAEA: " + _caea.CAEA + Environment.NewLine +
                        "Fecha Proceso: " + _caea.FchProceso + Environment.NewLine +
                        "Fecha Tope de Información: " + _caea.FchTopeInf + Environment.NewLine +
                        "Fecha Vig. Desde: " + _caea.FchVigDesde + Environment.NewLine +
                        "Fecha Vig. Hasta: " + _caea.FchVigHasta);
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPeriodo.Text))
            {
                MessageBox.Show("Debe ingresar el Periodo");
                return;
            }

            if (String.IsNullOrEmpty(txtOrden.Text))
            {
                MessageBox.Show("Debe ingresar el Orden");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                int _periodo = Convert.ToInt32(txtPeriodo.Text);
                Int16 _orden = Convert.ToInt16(txtOrden.Text);

                AfipDll.wsAfip.CaeaSolicitar _caea = AfipDll.wsAfip.ConsultarCAEANew(_pathCerificado, _pathTaFC, _pathLog, 
                    txtCuit.Text, _EsTest, strPasswordSecureString, _periodo, _orden, out _lstErrores);

                if (_caea.CAEA != null)
                {
                    MessageBox.Show("CAEA: " + _caea.CAEA + Environment.NewLine +
                        "Fecha Proceso: " + _caea.FchProceso + Environment.NewLine +
                        "Fecha Tope de Información: " + _caea.FchTopeInf + Environment.NewLine +
                        "Fecha Vig. Desde: " + _caea.FchVigDesde + Environment.NewLine +
                        "Fecha Vig. Hasta: " + _caea.FchVigHasta);
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "CAEA: " + _caea.CAEA);
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Fecha Proceso: " + _caea.FchProceso);
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Fecha Tope de Información: " + _caea.FchTopeInf);
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Fecha Vig. Desde: " + _caea.FchVigDesde);
                    AfipDll.LogFile.ErrorLog(AfipDll.LogFile.CreatePath(_pathLog), "Fecha Vig. Hasta: " + _caea.FchVigHasta);
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPeriodo.Text))
            {
                MessageBox.Show("Debe ingresar el Periodo");
                return;
            }

            if (String.IsNullOrEmpty(txtOrden.Text))
            {
                MessageBox.Show("Debe ingresar el Orden");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                int _periodo = Convert.ToInt32(txtPeriodo.Text);
                Int16 _orden = Convert.ToInt16(txtOrden.Text);

                List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();
                AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                _al.BaseImp = 100;
                _al.Id = 5;
                _al.Importe = 21;
                _alicIVA.Add(_al);

                List<AfipDll.wsAfipCae.FECAEADetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEADetRequest>();
                AfipDll.wsAfipCae.FECAEADetRequest _det = new AfipDll.wsAfipCae.FECAEADetRequest();
                _det.CAEA = "";
                _det.CbteDesde = 8;
                _det.CbteFch = "20190610";
                _det.CbteHasta = 8;
                _det.Concepto = 3;
                _det.DocNro = 11111111;
                _det.DocTipo = 1;
                _det.FchServDesde = "20190610";
                _det.FchServHasta = "20190610";
                _det.FchVtoPago = "20190610";
                _det.ImpIVA = 0;
                _det.ImpNeto = 121;
                _det.ImpOpEx = 0;
                _det.ImpTotal = 121;
                _det.ImpTotConc = 0;
                _det.ImpTrib = 0;
                //_det.Iva = _alicIVA.ToArray();
                _det.MonCotiz = 1;
                _det.MonId = "PES";
                _det.CAEA = "29244098519667";

                _detReq.Add(_det);

                AfipDll.wsAfipCae.FECAEACabRequest _cabReq = new AfipDll.wsAfipCae.FECAEACabRequest();
                _cabReq.CantReg = 1;
                _cabReq.CbteTipo = 11;
                _cabReq.PtoVta = 4;

                AfipDll.wsAfipCae.FECAEARequest _req = new AfipDll.wsAfipCae.FECAEARequest();
                _req.FeCabReq = _cabReq;

                _req.FeDetReq = _detReq.ToArray();
                //

                List<AfipDll.wsAfip.CaeaResultadoInformar> _call = AfipDll.wsAfip.InformarMovimientosCAEANew(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text, 
                    true, strPasswordSecureString, _req, _EsTest, out _lstErrores);

                if (_call.Count() > 0)
                {
                    foreach (AfipDll.wsAfip.CaeaResultadoInformar cri in _call)
                    {
                        MessageBox.Show("CAEA: " + cri.Caea + Environment.NewLine +
                            "Comprobante: " + cri.CbteDesde + Environment.NewLine +
                            "Comprobante Nro.: " + cri.DocNro + Environment.NewLine +
                            "Comprobante Tipo: " + cri.DocTipo + Environment.NewLine +
                            "Resultado: " + cri.Resultado + Environment.NewLine +
                            "Observaciones: " + cri.Observaciones);
                    }
                }
                else
                {
                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
                if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
            {
                MessageBox.Show("Debe ingresar el PtoVta.");
                return;
            }

            

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                //int _periodo = Convert.ToInt32(txtPeriodo.Text);
                //Int16 _orden = Convert.ToInt16(txtOrden.Text);
                int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
                string _cuit = txtCuit.Text;
                int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));

                AfipDll.wsAfip.UltimoComprobante call = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pathCerificado, _pathTaFC, _pathLog, _ptoVta,
                    _tipoConpro, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);
                //AfipDll.wsAfip.UltimoComprobante call = AfipDll.wsAfip.RecuperoUltimoComprobanteNew(_pruebaCert, _pathTaFC, _pathLog, _ptoVta,
                //    _tipoConpro, _cuit, _EsTest, strPasswordSecureString, out _lstErrores);

                if (call.CbteNro > 0)
                {
                    MessageBox.Show("Nro. Comprobante: " + call.CbteNro + Environment.NewLine +
                        "Comprobante Tipo: " + call.CbteTipo + Environment.NewLine +
                        "Punto de Venta: " + call.PtoVta);
                }
                else
                {
                    MessageBox.Show("Nro. Comprobante: " + call.CbteNro);

                    foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPeriodo.Text))
            {
                MessageBox.Show("Debe ingresar el Periodo");
                return;
            }

            if (String.IsNullOrEmpty(txtOrden.Text))
            {
                MessageBox.Show("Debe ingresar el Orden");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                //validamos que tenemos el TA.xml
                if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCerificado, _pathTaFC, _pathLog, _wsaa, strCUIT))
                    _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCerificado, _pathTaFC, _pathLog, _EsTest, strCUIT, strPasswordSecureString);
                if (_wsaa.Token != null)
                {
                    List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                    int _periodo = Convert.ToInt32(txtPeriodo.Text);
                    Int16 _orden = Convert.ToInt16(txtOrden.Text);

                    List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();
                    AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                    _al.BaseImp = 5000000;
                    _al.Id = 5;
                    _al.Importe = 1050000;
                    _alicIVA.Add(_al);

                    List<AfipDll.wsAfipCae.FECAEDetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEDetRequest>();
                    AfipDll.wsAfipCae.FECAEDetRequest _det = new AfipDll.wsAfipCae.FECAEDetRequest();
                    //_det.CAEA = "";
                    _det.CbteDesde = Convert.ToInt32(txtNroComprobante.Text);
                    _det.CbteFch = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                    _det.CbteHasta = Convert.ToInt32(txtNroComprobante.Text);
                    _det.Concepto = 3;
                    _det.DocNro = 20180532499;
                    _det.DocTipo = 80;
                    _det.FchServDesde = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0')+ DateTime.Now.Day.ToString().PadLeft(2, '0');
                    _det.FchServHasta = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                    _det.FchVtoPago = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.AddDays(1).Day.ToString().PadLeft(2, '0');
                    _det.ImpIVA = 1050000;
                    _det.ImpNeto = 5000000;
                    _det.ImpOpEx = 0;
                    _det.ImpTotal = 6050000;
                    _det.ImpTotConc = 0;
                    _det.ImpTrib = 0;
                    _det.Iva = _alicIVA.ToArray();
                    _det.MonCotiz = 1;
                    _det.MonId = "PES";
                    //_det.CAEA = "29244098519667";

                    _detReq.Add(_det);

                    AfipDll.wsAfipCae.FECAECabRequest _cabReq = new AfipDll.wsAfipCae.FECAECabRequest();
                    _cabReq.CantReg = 1;
                    _cabReq.CbteTipo = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));
                    _cabReq.PtoVta = Convert.ToInt32(txtPtoVta.Text);

                    AfipDll.wsAfipCae.FECAERequest _req = new AfipDll.wsAfipCae.FECAERequest();
                    _req.FeCabReq = _cabReq;

                    _req.FeDetReq = _detReq.ToArray();
                    //

                    AfipDll.wsAfip.clsCaeResponse _call = AfipDll.wsAfip.ObtenerDatosCAENew(_req, _pathCerificado, _pathTaFC, _pathLog, txtCuit.Text,
                        _EsTest, strPasswordSecureString, _wsaa);

                    if (!String.IsNullOrEmpty(_call.Cae))
                    {
                        MessageBox.Show("CAE: " + _call.Cae + Environment.NewLine +
                            "Fecha Vto.: " + _call.FechaVto + Environment.NewLine +
                            "Resultado: " + _call.Resultado);
                    }
                    else
                    {
                        {
                            MessageBox.Show("Codigo de error: " + _call.ErrorCode + Environment.NewLine +
                        "Descripcion: " + _call.ErrorMsj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            if (!_pathCerificado.Contains(txtCertificado.Text))
                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

            List<AfipDll.wsAfipCae.OpcionalTipo> op = AfipDll.wsAfip.ConsultarOpcionales(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text,
                _EsTest, strPasswordSecureString, false, out _lstErrores);

            foreach (AfipDll.wsAfipCae.OpcionalTipo cri in op)
            {
                MessageBox.Show("DESC: " + cri.Desc+ Environment.NewLine +
                    
                    "Desde: " + cri.FchDesde + Environment.NewLine +
                    "Hasta: " + cri.FchHasta+ Environment.NewLine +
                    "ID: " + cri.Id + Environment.NewLine );
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPeriodo.Text))
            {
                MessageBox.Show("Debe ingresar el Periodo");
                return;
            }

            if (String.IsNullOrEmpty(txtOrden.Text))
            {
                MessageBox.Show("Debe ingresar el Orden");
                return;
            }

            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                AfipDll.WsaaNew _wsaa = new AfipDll.WsaaNew();
                //validamos que tenemos el TA.xml
                if (!AfipDll.WsaaNew.Wsaa.ValidoTANew(_pathCerificado, _pathTaFC, _pathLog, _wsaa, strCUIT))
                    _wsaa = AfipDll.WsaaNew.Wsaa.ObtenerDatosWsaaNew("wsfe", _pathCerificado, _pathTaFC, _pathLog, _EsTest, strCUIT, strPasswordSecureString);
                if (_wsaa.Token != null)
                {

                    List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                    int _periodo = Convert.ToInt32(txtPeriodo.Text);
                    Int16 _orden = Convert.ToInt16(txtOrden.Text);

                    List<AfipDll.wsAfipCae.AlicIva> _alicIVA = new List<AfipDll.wsAfipCae.AlicIva>();
                    AfipDll.wsAfipCae.AlicIva _al = new AfipDll.wsAfipCae.AlicIva();
                    _al.BaseImp = 5000000;
                    _al.Id = 5;
                    _al.Importe = 1050000;
                    _alicIVA.Add(_al);

                    //Opcionales
                    List<AfipDll.wsAfipCae.Opcional> _opcio = new List<AfipDll.wsAfipCae.Opcional>();
                    //Referencia Comercial
                    //AfipDll.wsAfipCae.Opcional _op0 = new AfipDll.wsAfipCae.Opcional();
                    //_op0.Id = "23";
                    //_op0.Valor = "Pablo Insua";
                    //_opcio.Add(_op0);
                    //CBU
                    AfipDll.wsAfipCae.Opcional _op1 = new AfipDll.wsAfipCae.Opcional();
                    _op1.Id = "2101";
                    _op1.Valor = "0123456789012345678901";
                    _opcio.Add(_op1);

                    List<AfipDll.wsAfipCae.FECAEDetRequest> _detReq = new List<AfipDll.wsAfipCae.FECAEDetRequest>();
                    AfipDll.wsAfipCae.FECAEDetRequest _det = new AfipDll.wsAfipCae.FECAEDetRequest();
                    //_det.CAEA = "";
                    _det.CbteDesde = Convert.ToInt32(txtNroComprobante.Text);
                    _det.CbteFch = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0'); // "20190704";
                    _det.CbteHasta = Convert.ToInt32(txtNroComprobante.Text);
                    _det.Concepto = 3;
                    _det.DocNro = 20180532499;
                    _det.DocTipo = 80;
                    _det.FchServDesde = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
                    _det.FchServHasta = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0'); // "20190704";
                    _det.FchVtoPago = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0'); // "20190705";
                    _det.ImpIVA = 1050000;
                    _det.ImpNeto = 5000000;
                    _det.ImpOpEx = 0;
                    _det.ImpTotal = 6050000;
                    _det.ImpTotConc = 0;
                    _det.ImpTrib = 0;
                    _det.Iva = _alicIVA.ToArray();
                    _det.MonCotiz = 1;
                    _det.MonId = "PES";
                    //_det.CAEA = "29244098519667";
                    _det.Opcionales = _opcio.ToArray();

                    _detReq.Add(_det);

                    AfipDll.wsAfipCae.FECAECabRequest _cabReq = new AfipDll.wsAfipCae.FECAECabRequest();
                    _cabReq.CantReg = 1;
                    _cabReq.CbteTipo = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));// 201;
                    _cabReq.PtoVta = Convert.ToInt32(txtPtoVta.Text);

                    AfipDll.wsAfipCae.FECAERequest _req = new AfipDll.wsAfipCae.FECAERequest();
                    _req.FeCabReq = _cabReq;

                    _req.FeDetReq = _detReq.ToArray();
                    //

                    AfipDll.wsAfip.clsCaeResponse _call = AfipDll.wsAfip.ObtenerDatosCAENew(_req, _pathCerificado, _pathTaFC, _pathLog, txtCuit.Text,
                        _EsTest, strPasswordSecureString, _wsaa);

                    if (!String.IsNullOrEmpty(_call.Cae))
                    {
                        MessageBox.Show("CAE: " + _call.Cae + Environment.NewLine +
                            "Fecha Vto.: " + _call.FechaVto + Environment.NewLine +
                            "Resultado: " + _call.Resultado);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de error: " + _call.ErrorCode + Environment.NewLine +
                            "Descripcion: " + _call.ErrorMsj);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                string _call = AfipDll.wsAfip.TestDummy(_pathLog, _EsTest);

                MessageBox.Show("Respuesta Dummy: " + _call);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

            try
            {
                List<AfipDll.WsMTXCA.PtoVenta> _ptoVta = AfipDll.WsMTXCA.ConsultarPuntosVenta(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    strPasswordSecureString, true, out _lstErrores);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.WsMTXCA.PtoVenta _pv in _ptoVta)
                    {
                        MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                            "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                            "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                            "Fecha de Baja: " + _pv.FechaBaja);
                    }
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                AfipDll.WsMTXCA.Dummy _call = AfipDll.WsMTXCA.TestDummy(_pathLog, _EsTest);

                MessageBox.Show("Respuesta Dummy: " + _call);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

            try
            {
                List<AfipDll.WsMTXCA.PtoVenta> _ptoVta = AfipDll.WsMTXCA.ConsultarPuntosVentaCAE(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    strPasswordSecureString, false, out _lstErrores);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.WsMTXCA.PtoVenta _pv in _ptoVta)
                    {
                        MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                            "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                            "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                            "Fecha de Baja: " + _pv.FechaBaja);
                    }
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();
            int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
            string _cuit = txtCuit.Text;
            int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));

            try
            {
                long _ultimoNro = AfipDll.WsMTXCA.RecuperoUltimoComprobante(_pathCerificado, _pathTaFCMTXCA, _pathLog,_ptoVta ,
                    _tipoConpro, txtCuit.Text, _EsTest,
                    strPasswordSecureString, false, out _lstErrores);
                if (_lstErrores.Count() == 0)
                    MessageBox.Show("Ultimo Nro: " + _ultimoNro.ToString());
                else
                {
                    string _message = "";
                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        _message = _message + Environment.NewLine + _er.Msg;
                    }
                    MessageBox.Show(_message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
            

        private void button13_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

            try
            {
                List<AfipDll.WsMTXCA.PtoVenta> _ptoVta = AfipDll.WsMTXCA.ConsultarPuntosVentaCAEA(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    strPasswordSecureString, false, out _lstErrores);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.WsMTXCA.PtoVenta _pv in _ptoVta)
                    {
                        MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                            "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                            "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                            "Fecha de Baja: " + _pv.FechaBaja);
                    }
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            string p = "tmontan.yw5n2x+2-ogq4dinjvga4tsnzx@mail.mercadolibre.com";
            int uno = p.IndexOf('.');
            int dos = p.IndexOf('@');
            string o = p.Substring(uno, (dos - uno));
            MessageBox.Show(o);
            MessageBox.Show(p.Replace(o, ""));
        }

        #region Exportacion
        private void button16_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            //List<AfipDll.wsExportacion.ClsFEXErr> _lstErrores = new List<AfipDll.wsExportacion.ClsFEXErr>();
            string _error = "";

            try
            {
                List<AfipDll.PuntosDeVenta> _ptoVta = AfipDll.wsAfipExpo.PuntosDeVenta(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    out _error);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.PuntosDeVenta _pv in _ptoVta)
                    {
                        MessageBox.Show("Punto de Venta: " + _pv.PtoVta.ToString() + Environment.NewLine +
                            "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                            "Fecha Bloqueo: " + _pv.FechaBloqueo + Environment.NewLine );
                    }
                }
                else
                {
                    MessageBox.Show("No se recivieron puntos de venta");

                        MessageBox.Show("Codigo de error: " + _error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            string _ptoVta = txtPtoVta.Text;

            int _tipoConpro = 19; // Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));

            string _error = "";
            string _rtdo = "";

            try
            {
                int _Nro = AfipDll.wsAfipExpo.GetLastCmp(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest, _ptoVta, _tipoConpro,strPasswordSecureString,
                    out _rtdo,out _error);

                MessageBox.Show("Punto de Venta: " + _Nro.ToString());
                MessageBox.Show("Error: " + _error);
                MessageBox.Show("Resultado: " + _rtdo);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        private void button18_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))
                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

            List<AfipDll.wsAfip.PtoVenta> _ptoVta = AfipDll.wsAfip.ConsultarTiposComprobantes(_pathCerificado, _pathTaFC, _pathLog, txtCuit.Text, _EsTest,
                strPasswordSecureString, false, out _lstErrores);

            if (_ptoVta.Count > 0)
            {
                foreach (AfipDll.wsAfip.PtoVenta _pv in _ptoVta)
                {
                    MessageBox.Show("Punto de Venta: " + _pv.Nro.ToString() + Environment.NewLine +
                        "Bloqueado: " + _pv.Bloqueado + Environment.NewLine +
                        "Emision Tipo: " + _pv.EmisionTipo + Environment.NewLine +
                        "Fecha de Baja: " + _pv.FechaBaja);
                }
            }
            else
            {
                MessageBox.Show("No se recivieron puntos de venta");

                foreach (AfipDll.wsAfip.Errors _er in _lstErrores)
                {
                    MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                        "Descripcion: " + _er.Msg);
                }
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCuit.Text))
            {
                MessageBox.Show("Debe ingresar el CUIT");
                return;
            }

            if (String.IsNullOrEmpty(txtCertificado.Text))
            {
                MessageBox.Show("Debe ingresar el Certificado");
                return;
            }

            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Debe ingresar el Password");
                return;
            }

            if (String.IsNullOrEmpty(txtPtoVta.Text))
            {
                MessageBox.Show("Debe ingresar el PtoVta.");
                return;
            }

            if (String.IsNullOrEmpty(txtNroComprobante.Text))
            {
                MessageBox.Show("Debe ingresar el Nro del comprobante.");
                return;
            }



            try
            {
                SecureString strPasswordSecureString = new SecureString();

                foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
                strPasswordSecureString.MakeReadOnly();

                if (!_pathCerificado.Contains(txtCertificado.Text))
                    _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

                List<AfipDll.wsAfip.Errors> _lstErrores = new List<AfipDll.wsAfip.Errors>();

                //int _periodo = Convert.ToInt32(txtPeriodo.Text);
                //Int16 _orden = Convert.ToInt16(txtOrden.Text);
                int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
                string _cuit = txtCuit.Text;
                int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));
                int _Nro = Convert.ToInt32(txtNroComprobante.Text);

                string call = AfipDll.wsAfip.ConsultaComprobante2New(_pathCerificado, _pathTaFC, _pathLog, _ptoVta,
                    _tipoConpro, _cuit, _Nro, _EsTest, strPasswordSecureString);

                MessageBox.Show(call);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

            try
            {
                List<AfipDll.WsMTXCA.Codigos> _ptoVta = AfipDll.WsMTXCA.ConsultarUnidadesMedida(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    strPasswordSecureString, true, out _lstErrores);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.WsMTXCA.Codigos _pv in _ptoVta)
                    {
                        MessageBox.Show("Codigo: " + _pv.codigo.ToString() + Environment.NewLine +
                            "Descripción: " + _pv.descripcion + Environment.NewLine);
                    }
                }
                else
                {
                    MessageBox.Show("No se recibieron Unidades de Medida.");

                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();
            int _ptoVta = Convert.ToInt32(txtPtoVta.Text);
            string _cuit = txtCuit.Text;
            int _tipoConpro = Convert.ToInt32(cboNroCompro.SelectedItem.ToString().Substring(0, 3));
            long _nroCbte = Convert.ToInt64(txtNroComprobante.Text);

            AfipDll.WsMTXCA.ConsultaComprobanteResponse _cbte = new AfipDll.WsMTXCA.ConsultaComprobanteResponse();
            try
            {
                AfipDll.WsMTXCA.ConsultarComprobanteAutorizado(_pathCerificado, _pathTaFCMTXCA, _pathLog, _cuit,
                _tipoConpro.ToString(), _ptoVta.ToString(), _nroCbte, _EsTest, strPasswordSecureString, true, out _cbte, out _lstErrores);
                if (_lstErrores.Count() == 0)
                {
                    string _message = "Resultado Del Comprobante" + Environment.NewLine +
                        "Numero Comprobante: " + _cbte.numeroCbte + Environment.NewLine +
                        "Punto de Venta: " + _cbte.puntoVenta + Environment.NewLine +
                        "Tipo de Comprobante: " + _cbte.tipoComprobante + Environment.NewLine +
                        "Fecha Emisión: " + _cbte.fechaEmision + Environment.NewLine +
                        "Tipo de Documento del cliente: " + _cbte.tipoDocumento + Environment.NewLine +
                        "Numero Documento del cliente: " + _cbte.numeroDocumento + Environment.NewLine +
                        "Importe Total del comprobante: " + _cbte.importeTotal + Environment.NewLine +
                        "Código Moneda: " + _cbte.codigoMoneda + Environment.NewLine +
                        "Código autorización (CAE): " + _cbte.codigoAutorizacion + Environment.NewLine +
                        "Fecha Vencimiento: " + _cbte.fechaVencimiento + Environment.NewLine +
                        "Observaciones: " + _cbte.observaciones + Environment.NewLine +
                        "Tipo Autorización: " + _cbte.tipoAutorizacion;
                    MessageBox.Show(_message);
                }
                else
                {
                    string _message = "";
                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        _message = _message + Environment.NewLine + _er.Msg;
                    }
                    MessageBox.Show(_message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            bool pp = IcgFceDll.DataAccess.FuncionesVarias.ValidarDigitoCuit(txtCuit.Text.Trim().Replace("-", ""));

            MessageBox.Show(pp.ToString());

            ////Invoco armado de cabecera del reTail IRSA.
            //string serie = "011T";
            //string numero = "49";
            //string n = "B";
            //string idTienda = "00IDTIENDA";
            //string idTerminal = "IDTERMINAL";
            //string cuit = "20180532499";
            //string pathSend = "C:\\PP\\";
            ////Armamos el stringConnection.
            //string strConnection = "Data Source=PKICGAPP01\\Demo;Initial Catalog=FRONTMARIACHER;User Id=sa;Password=masterkey;";
            //using (SqlConnection _con = new SqlConnection(strConnection))
            //{
            //    IcgFceDll.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql12(serie, numero, n, idTienda, idTerminal, cuit, strCUIT, pathSend, false, _con);
            //}

            //string startFolder = @"c:\";

            //// Take a snapshot of the file system.  
            //System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);

            //// This method assumes that the application has discovery permissions  
            //// for all folders under the specified path.  
            //IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles("invoice_T00196074B.txt", System.IO.SearchOption.AllDirectories);

            ////Create the query  
            //IEnumerable<System.IO.FileInfo> fileQuery =
            //    from file in fileList
            //    where file.Extension == ".txt"
            //    orderby file.Name
            //    select file;
            ////Execute the query. This might write out a lot of files!  
            //foreach (System.IO.FileInfo fi in fileQuery)
            //{
            //    MessageBox.Show(fi.FullName);
            //}

        }

        private void button23_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    //Recupero el string del control
            //    string Qr = "httpsÑ--www.afip.gob.ar - fe - qr - _p¿eyJ2ZXIiOjEsImZlY2hhIjoyMDIyLTA2LTIyLCJjdWl0IjozMDY3Nzg4MzYxNCwicHRvVnRhIjowMDAwLCJ0aXBvQ21wIjowMSwibnJvQ21wIjo3NjU3LCJpbXBvcnRlIjozNTQyODIuODAsIm1vbmVkYSI6UEVTLCJjdHoiOjEsInRpcG9Eb2NSZWMiOiBudWxsLCJucm9Eb2NSZWMiOiBudWxsLCJ0aXBvQ29kQXV0IjpFLCJjb2RBdXQiOjcyMjU0OTE2NDU0MDk5fQ¿¿";
            //    //Separo el parte de la URL del resto.
            //    //string urlAfip = "https://www.afip.gob.ar/fe/qr/?p=";
            //    string urlAfip = "httpsÑ--www.afip.gob.ar - fe - qr - _p¿";
            //    string dato = Qr.Replace(urlAfip, "");
            //    //decodifico
            //    string rta = DecodeBase64(dato);
            //    //Json
            //    SrvFCAE dll = new SrvFCAE();
            //    AfipWebService.Clases.QR.QRjson json = JsonConvert.DeserializeObject<AfipWebService.Clases.QR.QRjson>(rta);
            //    //Muestro en ventana
            //    MessageBox.Show("Fecha Comprobante: " + json.fecha + Environment.NewLine +
            //        "Tipo Comprobante: " + json.tipoCmp + Environment.NewLine +
            //        "Punto de Venta: " + json.ptoVta + Environment.NewLine +
            //        "Numero comprobante: " + json.nroCmp + Environment.NewLine +
            //        "Importe Total: " + json.importe + Environment.NewLine +
            //        "Moneda: " + json.moneda + Environment.NewLine +
            //        "Cotización: " + json.ctz + Environment.NewLine +
            //        "CUIT Emisor:" + json.cuit + Environment.NewLine +
            //        "Tipo Doc Receptor: " + json.tipoDocRec + Environment.NewLine +
            //        "Nro. Doc Receptor: " + json.nroDocRec + Environment.NewLine +
            //        "CAE: " + json.codAut);
            //}
            //catch (Exception ex)
            //{ MessageBox.Show("Error: " + ex.Message); }
        }
        public static string DecodeBase64(string value)
        {
            var valueBytes = System.Convert.FromBase64String(value);
            return Encoding.UTF8.GetString(valueBytes);
        }

        private void button25_Click(object sender, EventArgs e)
        {
            //Consulta si existe el archivo.Caso contrario da error.
            if (File.Exists("total.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                //xDoc.Load("DesdoblarMediosPagoFile.xml");
                xDoc.Load("total.xml");

                XmlNodeList _serverXml = xDoc.GetElementsByTagName("regimenfac");
                string _regimen = _serverXml[0].InnerText;
                if (_regimen == "4")
                {
                    if (MessageBox.Show("Desea cambiar el regimen de Facturación?.", "ICG Argentina", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _serverXml[0].InnerText = "N";
                        xDoc.Save("total.xml");
                    }
                }
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            SecureString strPasswordSecureString = new SecureString();

            if (!_pathCerificado.Contains(txtCertificado.Text))

                _pathCerificado = _pathCerificado + @"\" + txtCertificado.Text;

            foreach (char c in txtPassword.Text) strPasswordSecureString.AppendChar(c);
            strPasswordSecureString.MakeReadOnly();

            List<AfipDll.WsMTXCA.Errors> _lstErrores = new List<AfipDll.WsMTXCA.Errors>();

            try
            {
                List<AfipDll.WsMTXCA.Codigos> _ptoVta = AfipDll.WsMTXCA.ConsultarTiposComprobantes(_pathCerificado, _pathTaFCMTXCA, _pathLog, txtCuit.Text, _EsTest,
                    strPasswordSecureString, true, out _lstErrores);

                if (_ptoVta.Count > 0)
                {
                    foreach (AfipDll.WsMTXCA.Codigos _pv in _ptoVta)
                    {
                        MessageBox.Show("Codigo: " + _pv.codigo.ToString() + Environment.NewLine +
                            "Descripción: " + _pv.descripcion + Environment.NewLine);
                    }
                }
                else
                {
                    MessageBox.Show("No se recibieron Unidades de Medida.");

                    foreach (AfipDll.WsMTXCA.Errors _er in _lstErrores)
                    {
                        MessageBox.Show("Codigo de error: " + _er.Code + Environment.NewLine +
                            "Descripcion: " + _er.Msg);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
